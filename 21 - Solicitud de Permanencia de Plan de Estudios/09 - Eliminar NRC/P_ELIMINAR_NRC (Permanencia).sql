/*
SET SERVEROUTPUT ON
DECLARE P_MESSAGE VARCHAR2(4000);
BEGIN
    P_ELIMINAR_NRC(87313, '201810', P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
END;
*/

CREATE OR REPLACE PROCEDURE P_ELIMINAR_NRC (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_MESSAGE         OUT VARCHAR2
)
AS
        V_MAT               NUMBER := 0;
        V_STP               NUMBER := 0;
        V_INI               NUMBER := 0;
BEGIN

    -- #######################################################################
    ---ELIMINAR  MATRICULA EN EL PERIODO
    -- #######################################################################
    SELECT COUNT(*)
    INTO V_MAT
    FROM SFRSTCR
    WHERE SFRSTCR_PIDM = P_PIDM
    AND SFRSTCR_TERM_CODE = P_TERM_CODE
    AND SFRSTCR_RSTS_CODE in ('RW','RE','DD');
    
    IF V_MAT > 0 THEN
        DELETE SFRSTCR
        WHERE SFRSTCR_PIDM = P_PIDM
        AND SFRSTCR_TERM_CODE = P_TERM_CODE
        AND SFRSTCR_RSTS_CODE in ('RW','RE','DD');
    END IF;
    
    -- #######################################################################
    -- ELIMINAR - STUDY PATH -> SFAREGS
    -- #######################################################################
    SELECT COUNT(*)
    INTO V_STP
    FROM SFRENSP 
    WHERE SFRENSP_PIDM = P_PIDM
    AND SFRENSP_TERM_CODE = P_TERM_CODE;

    IF V_STP > 0 THEN
        DELETE SFRENSP 
        WHERE SFRENSP_PIDM = P_PIDM
        AND SFRENSP_TERM_CODE = P_TERM_CODE;
    END IF;
    
    -- #######################################################################
    -- ELIMINAR - INFORMACION DE INGRESO -> SFAREGS  ||  registro que determina al alumno Elegible 
    -- #######################################################################
    SELECT COUNT(*)
    INTO V_INI
    FROM SFBETRM
    WHERE SFBETRM_PIDM = P_PIDM
    AND SFBETRM_TERM_CODE = P_TERM_CODE;

    IF V_INI > 0 THEN
        DELETE SFBETRM
        WHERE SFBETRM_PIDM = P_PIDM
        AND SFBETRM_TERM_CODE = P_TERM_CODE;
    END IF;
    
    COMMIT;
    
    P_MESSAGE := 'OK';
    
EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_ELIMINAR_NRC;