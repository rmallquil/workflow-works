
/*
drop procedure p_get_usuario_regaca;
GRANT EXECUTE ON p_get_usuario_regaca TO wfobjects;
GRANT EXECUTE ON p_get_usuario_regaca TO wfauto;

set serveroutput on
DECLARE
-- OUT
p_nombre                  VARCHAR2(255);
p_correo_regaca           VARCHAR2(255);
p_usuario_regaca          VARCHAR2(255);
P_ROL_SEDE                VARCHAR2(255);
p_error                   VARCHAR2(255);
begin
  WFK_CONTISRIR.p_get_usuario_regacad('03','adminisiones',P_NOMBRE,P_CORREO_REGACA,P_USUARIO_REGACA,P_ROL_SEDE,P_ERROR);
  DBMS_OUTPUT.PUT_LINE(P_NOMBRE || P_CORREO_REGACA || P_USUARIO_REGACA || '---' || p_error);
end;

*/

---------------------------------------------------------------------------------------------------------------------------------------------------------------------



create or replace PROCEDURE p_get_usuario_regacad (
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
      P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
      P_CORREO_REGACA       OUT VARCHAR2,
      P_ROL_SEDE            OUT VARCHAR2,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : p_get_usuario_regacad
  FECHA     : 04/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento LOS CORREOS deL(os) responsable(s) 
              de Registros Académicos de esa sede.
  =================================================================================================================== */
AS
      -- @PARAMETERS
      P_ROLE_ID                 NUMBER;
      P_ORG_ID                  NUMBER;
      P_USER_ID                 NUMBER;
      P_ROLE_ASSIGNMENT_ID      NUMBER;
      P_Email_Address           VARCHAR2(100);
    
      P_SECCION_EXCEPT          VARCHAR2(50);
      
      CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = P_ORG_ID AND ROLE_ID = P_ROLE_ID;
      
      V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
BEGIN 
--
      P_ROL_SEDE := P_ROL || P_COD_SEDE;
      
      -- Obtener el ROL_ID 
      P_SECCION_EXCEPT := 'ROLES';
      SELECT ID INTO P_ROLE_ID FROM WORKFLOW.ROLE WHERE NAME = P_ROL_SEDE;
      P_SECCION_EXCEPT := '';
      
      -- Obtener el ORG_ID 
      P_SECCION_EXCEPT := 'ORGRANIZACION';
      SELECT ID INTO P_ORG_ID FROM WORKFLOW.ORGANIZATION WHERE NAME = 'Root';
      P_SECCION_EXCEPT := '';
     
      -- Obtener los datos de usuarios que relaciona rol y usuario
      P_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
       -- #######################################################################
      OPEN C_ROLE_ASSIGNMENT;
      LOOP
          FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
          EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
                      
                      -- Obtener Datos Usuario
                      SELECT Email_Address INTO P_Email_Address FROM WORKFLOW.WFUSER WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;
          
                      P_CORREO_REGACA := P_CORREO_REGACA || P_Email_Address || ',';
      END LOOP;
      CLOSE C_ROLE_ASSIGNMENT;
      P_SECCION_EXCEPT := '';
      
      -- Extraer el ultimo digito en caso sea un "coma"(,)
      SELECT SUBSTR(P_CORREO_REGACA,1,LENGTH(P_CORREO_REGACA) -1) INTO P_CORREO_REGACA
      FROM DUAL
      WHERE SUBSTR(P_CORREO_REGACA,-1,1) = ',';
      
EXCEPTION
  WHEN TOO_MANY_ROWS THEN 
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              -- --DBMS_OUTPUT.PUT_LINE ();
              P_ERROR := 'A ocurrido un problema, se encontraron mas de un ROL con el mismo nombre: ' || P_ROL || P_COD_SEDE;
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_ERROR := 'A ocurrido un problema, se encontraron mas de una ORGANIZACIÒN con el mismo nombre.';
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := 'A ocurrido un problema, Se encontraron mas de un usuario con el mismo ROL.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
  WHEN NO_DATA_FOUND THEN
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              P_ERROR := 'A ocurrido un problema, NO se encontrò el nombre del ROL: ' || P_ROL || P_COD_SEDE;
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_ERROR := 'A ocurrido un problema, NO se encontrò el nombre de la ORGANIZACION.';
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := 'A ocurrido un problema, NO  se encontrò ningun usuario con esas caracteristicas.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
  WHEN OTHERS THEN
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END p_get_usuario_regacad;


       
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------