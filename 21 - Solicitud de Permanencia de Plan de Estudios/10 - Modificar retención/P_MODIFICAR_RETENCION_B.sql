PROCEDURE P_MODIFICAR_RETENCION_B (
      P_PIDM                IN  SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_STVHLDD_CODE1       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_STVHLDD_CODE2       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_STVHLDD_CODE3       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_STVHLDD_CODE4       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_MESSAGE             OUT VARCHAR2
) 
/* ===================================================================================================================
  NOMBRE    : P_MODIFICAR_RETENCION_B
  FECHA     : 13/07/18
  AUTOR     : Flores Vilcapoma, Brian 
  OBJETIVO  : Modifica la fecha limite de las retenciones solicitadas a un dia antes de su fecha actual.

  MODIFICACIONES
  NRO     FECHA         USUARIO       MODIFICACION  
  =================================================================================================================== */
AS
      
      V_INDICADOR               NUMBER;
      V_STATUS_DATE             SVRSVPR.SVRSVPR_STATUS_DATE%TYPE;
      V_INVALID_COD_RETENCION   EXCEPTION;
      
BEGIN
      
      -- VALIDAR si EXISTE codigo del TIPO DE RETENCION - STVHLDD
      SELECT COUNT(*)
      INTO V_INDICADOR 
      FROM STVHLDD
      WHERE STVHLDD_CODE IN ( P_STVHLDD_CODE1, P_STVHLDD_CODE2, P_STVHLDD_CODE3, P_STVHLDD_CODE4 );
      
      -- GET FECHA de estado APROVADO DE LA SOLICITUD
      SELECT SVRSVPR_STATUS_DATE
      INTO V_STATUS_DATE
      FROM SVRSVPR 
      WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
      AND SVRSVPR_PIDM = P_PIDM
      AND SVRSVPR_SRVS_CODE = 'AP'; -- ESTADO APROVADO
      
      IF ( V_INDICADOR < 4 ) THEN
      
            RAISE V_INVALID_COD_RETENCION;
      
      ELSE
            -- INSERTAR REGISTRO DE FECHA DE RETENCION DOCUMENTOS - SOAHOLD      
            UPDATE SPRHOLD
            SET   
                  SPRHOLD_TO_DATE = V_STATUS_DATE - 1,
                  SPRHOLD_ACTIVITY_DATE = SYSDATE
            WHERE SPRHOLD_PIDM = P_PIDM
            AND SPRHOLD_TO_DATE > V_STATUS_DATE
            AND SPRHOLD_HLDD_CODE IN (P_STVHLDD_CODE1, P_STVHLDD_CODE2, P_STVHLDD_CODE3, P_STVHLDD_CODE4);
            
            COMMIT;
      END IF;

EXCEPTION
  WHEN V_INVALID_COD_RETENCION THEN
          P_MESSAGE  := '- El "CÓDIGO DE RETENCIÓN" enviado es inválido.';
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_MODIFICAR_RETENCION_B;