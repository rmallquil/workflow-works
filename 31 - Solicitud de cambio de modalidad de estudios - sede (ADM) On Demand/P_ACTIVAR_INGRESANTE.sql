/*
SET SERVEROUTPUT ON
DECLARE P_MESSAGE VARCHAR2(4000);
BEGIN
    P_ACTIVAR_INGRESANTE (641423, '73632340','UREG','UREG','S01','F02','F02', '201910', 'SE', P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
END;
*/
/* =============================================================================
  NOMBRE    : P_ACTIVAR_INGRESANTE
  FECHA     : 19/11/2018
  AUTOR     : Flores Vilcapoma Brian
  OBJETIVO  : Activa el ingreso del estudiante segun su nueva modalidad.
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =========================================================================== */
CREATE OR REPLACE PROCEDURE P_ACTIVAR_INGRESANTE (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE      	IN STVDEPT.STVDEPT_CODE%TYPE,
    P_DEPT_CODE_NEW 	IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_CODE_NEW     IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_CODE_DEST    IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_CAMBIO_ADM        IN VARCHAR2,
    P_MESSAGE			OUT VARCHAR2
)
AS
    V_APEC_TERM         VARCHAR2(10);
    V_APEC_CAMP	        VARCHAR2(10);
    V_APEC_DEPT         VARCHAR2(10);
    
    V_APEC_CAMP_NEW     VARCHAR2(10);
    V_APEC_DEPT_NEW     VARCHAR2(10);
    V_APEC_CAMP_DEST    VARCHAR2(10);

    V_RESULT            INTEGER;

    V_CONTADOR_ING_ANT  INTEGER;
    V_CONTADOR_ING_NEW  INTEGER;

    V_SARADAP_APPL_NO   SARADAP.SARADAP_APPL_NO%TYPE;

    V_N1                DECIMAL(18,2); --NOTA N1 DE GQT Y VIR
    V_N2                DECIMAL(18,2); --NOTA N2 DE GQT Y VIR
    V_N3                DECIMAL(18,2); --NOTA N3 DE GQT Y VIR
    V_N4                DECIMAL(18,2); --NOTA N4 DE GQT Y VIR
    V_N5                DECIMAL(18,2); --NOTA N5 DE GQT Y VIR
    V_N6                DECIMAL(18,2); --NOTA N6 DE GQT Y VIR
    V_N7                DECIMAL(18,2); --NOTA N7 DE GQT Y VIR
    V_RM                DECIMAL(18,2); --NOTA RM DE CR
    V_RV                DECIMAL(18,2); --NOTA RV DE CR
    V_CO                DECIMAL(18,2); --NOTA CO DE CR
    V_PUNTAJE           DECIMAL(18,2); --NOTA DEL PUNTAJE FINAL
    V_FEC_EXAMEN        TIMESTAMP;
    
BEGIN
    --dbms_session.set_nls('nls_date_format', 'mm/dd/yyyy');
    
    -- PERIODO
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM 
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;
    
    -- CAMPUS 
    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP
    FROM CZRCAMP 
    WHERE CZRCAMP_CODE = P_CAMP_CODE;

    -- DEPARTAMENTO
    SELECT CZRDEPT_DEPT_BDUCCI
        INTO V_APEC_DEPT
    FROM CZRDEPT
    WHERE CZRDEPT_CODE = P_DEPT_CODE;

    -- NEW CAMPUS 
    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP_NEW
    FROM CZRCAMP 
    WHERE CZRCAMP_CODE = P_CAMP_CODE_NEW;

    -- NEW CAMPUS DEST (CASOS DE VIRTUAL)
    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP_DEST
    FROM CZRCAMP 
    WHERE CZRCAMP_CODE = P_CAMP_CODE_DEST;

    -- NEW DEPARTAMENTO
    SELECT CZRDEPT_DEPT_BDUCCI
        INTO V_APEC_DEPT_NEW
    FROM CZRDEPT
    WHERE CZRDEPT_CODE = P_DEPT_CODE_NEW;

    IF P_CAMBIO_ADM = 'SE' THEN
        --INDICADOR SI ESTA VIGENTE EL CAMBIO ANTERIOR
        SELECT COUNT(*)
            INTO V_CONTADOR_ING_ANT
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDEscuelaADM" = V_APEC_DEPT
        AND "IDAlumno" = P_ID
        AND "Ingresante" = '1'
        AND "Renuncia" = '0';
        
        IF V_CONTADOR_ING_ANT > 0 THEN
            -- DESACTIVAR EL INGRESO EN LA MODALIDAD PARA VOLVER A ACTIVAR EL REGISTRO
            UPDATE tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
                SET "Ingresante" = 0
            WHERE "IDDependencia" = 'UCCI'
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDEscuelaADM" = V_APEC_DEPT
            AND "IDAlumno" = P_ID
            AND "Ingresante" = '1'
            AND "Renuncia" = '0';
            
            COMMIT;
            
        END IF;
              
    ELSE
        
        SELECT COUNT(*)
            INTO V_CONTADOR_ING_ANT
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDEscuelaADM" = V_APEC_DEPT
            AND "Ingresante" = '1'
            AND "Renuncia" = '0'
            AND "IDAlumno" = P_ID;

        IF V_CONTADOR_ING_ANT > 0 THEN
            
            SELECT COUNT(*)
                INTO V_CONTADOR_ING_NEW
            FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
            WHERE "IDDependencia" = 'UCCI'
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDEscuelaADM" = V_APEC_DEPT_NEW
            AND "IDAlumno" = P_ID;
            --AND "Ingresante" = 0;
            
            IF V_CONTADOR_ING_NEW > 0 THEN
            
                IF V_APEC_DEPT = 'ADM' THEN
                
                    SELECT "RM", "RV", "CO", "Puntaje"
                        INTO V_RM, V_RV, V_CO, V_PUNTAJE
                    FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
                    WHERE "IDDependencia" = 'UCCI'
                        AND "IDPerAcad" = V_APEC_TERM
                        AND "IDEscuelaADM" = V_APEC_DEPT
                        AND "Ingresante" = '1'
                        AND "IDAlumno" = P_ID;
                    
                    UPDATE tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
                        SET "N1" = V_CO, "N2" = V_CO, "N3" = V_CO, "N4" = V_CO, "N5" = V_CO, "N6" = V_RM, "N7" = V_RV, "Puntaje" = V_PUNTAJE
                    WHERE "IDDependencia" = 'UCCI'
                        AND "IDPerAcad" = V_APEC_TERM
                        AND "IDEscuelaADM" = V_APEC_DEPT_NEW
                        AND "Ingresante" = '0'
                        AND "IDAlumno" = P_ID;
                    
                    COMMIT;
                    
                ELSIF V_APEC_DEPT_NEW = 'ADM' THEN
                
                    SELECT "N1", "N2", "N3", "N4", "N5", "N6", "N7", "Puntaje"
                        INTO V_N1, V_N2, V_N3, V_N4, V_N5, V_N6, V_N7, V_PUNTAJE
                    FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
                    WHERE "IDDependencia" = 'UCCI'
                        AND "IDPerAcad" = V_APEC_TERM
                        AND "IDEscuelaADM" = V_APEC_DEPT
                        AND "Ingresante" = '1'
                        AND "IDAlumno" = P_ID;
                    
                    UPDATE tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
                        SET "RM" = V_N6, "RV" = V_N7, "CO" = (V_N1 + V_N2 + V_N3 + V_N4 + V_N5)/5, "Puntaje" = V_PUNTAJE
                    WHERE "IDDependencia" = 'UCCI'
                        AND "IDPerAcad" = V_APEC_TERM
                        AND "IDEscuelaADM" = V_APEC_DEPT_NEW
                        AND "IDAlumno" = P_ID;
                    COMMIT;
                    
                ELSE
                
                    SELECT "N1", "N2", "N3", "N4", "N5", "N6", "N7", "Puntaje"
                        INTO V_N1, V_N2, V_N3, V_N4, V_N5, V_N6, V_N7, V_PUNTAJE
                    FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
                    WHERE "IDDependencia" = 'UCCI'
                        AND "IDPerAcad" = V_APEC_TERM
                        AND "IDEscuelaADM" = V_APEC_DEPT
                        AND "Ingresante" = '1'
                        AND "IDAlumno" = P_ID;
                    
                    UPDATE tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
                        SET "N1" = V_N1, "N2" = V_N2, "N3" = V_N3, "N4" = V_N4, "N5" = V_N5, "N6" = V_N6, "N7" = V_N7, "Puntaje" = V_PUNTAJE
                    WHERE "IDDependencia" = 'UCCI'
                        AND "IDPerAcad" = V_APEC_TERM
                        AND "IDEscuelaADM" = V_APEC_DEPT_NEW
                        AND "IDAlumno" = P_ID;
                    COMMIT;
                    
                END IF;
                
            END IF;

            UPDATE tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
                SET "Renuncia" = 1
            WHERE "IDDependencia" = 'UCCI'
                AND "IDPerAcad" = V_APEC_TERM
                AND "IDEscuelaADM" = V_APEC_DEPT
                AND "Ingresante" = '1'
                AND "Renuncia" = '0'
                AND "IDAlumno" = P_ID;
            COMMIT;

        END IF;
        
    END IF;

    -- Obtener registro maximo del SARADAP
    SELECT MAX(SARADAP_APPL_NO)
        INTO V_SARADAP_APPL_NO
    FROM SARADAP
    WHERE SARADAP_PIDM = P_PIDM
    AND SARADAP_TERM_CODE_ENTRY = P_TERM_CODE
    AND SARADAP_LEVL_CODE = 'PG';

    -- Actualización del SARADAP
    UPDATE SARADAP
        SET SARADAP_APPL_DATE = SARADAP_APPL_DATE-1
    WHERE SARADAP_PIDM = P_PIDM
    AND SARADAP_TERM_CODE_ENTRY = P_TERM_CODE
    AND SARADAP_LEVL_CODE = 'PG'
    AND SARADAP_APPL_NO = V_SARADAP_APPL_NO;
    
    COMMIT;

    IF V_APEC_DEPT_NEW = 'ADM' THEN

        SELECT "RM", "RV", "CO", "Puntaje"
            INTO V_RM, V_RV, V_CO, V_PUNTAJE
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDEscuelaADM" = V_APEC_DEPT_NEW
        AND "IDAlumno" = P_ID
        AND "Ingresante" = '0'
        AND "Renuncia" = '0';

        SELECT TO_DATE("IDExamen",'dd/mm/yy hh24:mi:ss')
            INTO V_FEC_EXAMEN
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDEscuelaADM" = V_APEC_DEPT_NEW
        AND "IDAlumno" = P_ID
        AND "Ingresante" = '0'
        AND "Renuncia" = '0';
        
--        V_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
--        'ADM.sp_ActivarIngresante "'
--        || 'UCCI" , "'|| V_APEC_CAMP_NEW || '" , "' || V_APEC_TERM || '" , "' || TO_CHAR(V_FEC_EXAMEN, 'mm/dd/yyyy') ||'" , "'|| P_ID ||'" , "'
--        ||  REPLACE(V_RM, ',', '.') ||'" , "'|| REPLACE(V_RV, ',', '.') || '" , "'|| REPLACE(V_CO, ',', '.') || '" , "'|| REPLACE(V_PUNTAJE, ',', '.') || '" , "'|| 1 || '" , "'|| 1 || '" '
--        );
        COMMIT;
  
    ELSIF V_APEC_DEPT_NEW = 'ADG' THEN
   
        SELECT "N1", "N2", "N3", "N4", "N5", "N6", "N7", "Puntaje"
            INTO V_N1, V_N2, V_N3, V_N4, V_N5, V_N6, V_N7, V_PUNTAJE
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDEscuelaADM" = V_APEC_DEPT_NEW
            AND "IDAlumno" = P_ID
            AND "Ingresante" = '0'
            AND "Renuncia" = '0';

        SELECT TO_DATE("IDExamen",'dd/mm/yy hh24:mi:ss')
            INTO V_FEC_EXAMEN
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDEscuelaADM" = V_APEC_DEPT_NEW
        AND "IDAlumno" = P_ID
        AND "Ingresante" = '0'
        AND "Renuncia" = '0';
        
--        V_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
--        'ADM.sp_ActivarIngresante_gqt "UCCI" , "'|| V_APEC_CAMP_NEW || '" , "' || V_APEC_TERM || '" , "' || TO_CHAR(V_FEC_EXAMEN, 'mm/dd/yyyy') ||'" , "'|| P_ID ||'" , "'
--        ||  REPLACE(V_N1, ',', '.') ||'" , "'|| REPLACE(V_N2, ',', '.') || '" , "'|| REPLACE(V_N3, ',', '.') || '" , "'|| REPLACE(V_N4, ',', '.') ||
--        '" , "'|| REPLACE(V_N5, ',', '.') ||'" , "'|| REPLACE(V_N6, ',', '.') || '" , "'|| REPLACE(V_N7, ',', '.') || '" , "'|| REPLACE(V_PUNTAJE, ',', '.') || '" , "'|| 1 || '" , "'|| 1 || '" '
--        );
        COMMIT;

    ELSE

        SELECT NVL("N1",0), NVL("N2",0), NVL("N3",0), NVL("N4",0), NVL("N5",0), NVL("N6",0), NVL("N7",0), NVL("Puntaje",0)
            INTO V_N1, V_N2, V_N3, V_N4, V_N5, V_N6, V_N7, V_PUNTAJE
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDEscuelaADM" = V_APEC_DEPT_NEW
            AND "IDAlumno" = P_ID;

        SELECT TO_DATE("IDExamen",'dd/mm/yy hh24:mi:ss')
            INTO V_FEC_EXAMEN
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
        AND "IDPerAcad" = V_APEC_TERM
        AND "IDEscuelaADM" = V_APEC_DEPT_NEW
        AND "IDAlumno" = P_ID
        AND "Ingresante" = '0'
        AND "Renuncia" = '0';

--        V_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
--        'ADM.sp_ActivarIngresante_Vir "'
--        || 'UCCI" , "'|| V_APEC_CAMP_NEW || '" , "'|| V_APEC_CAMP_DEST || '" , "' || V_APEC_TERM || '" , "' || TO_CHAR(V_FEC_EXAMEN, 'mm/dd/yyyy') ||'" , "'|| P_ID ||'" , "'
--        ||  REPLACE(V_N1, ',', '.') ||'" , "'|| REPLACE(V_N2, ',', '.') || '" , "'|| REPLACE(V_N3, ',', '.') || '" , "'|| REPLACE(V_N4, ',', '.') ||
--        '" , "'|| REPLACE(V_N5, ',', '.') ||'" , "'|| REPLACE(V_N6, ',', '.') || '" , "'|| REPLACE(V_N7, ',', '.') || '" , "'|| REPLACE(V_PUNTAJE, ',', '.') || '" , "'|| 1 || '" , "'|| 1 || '" '
--        );
        COMMIT;

    END IF;

    -- Actualización del SARADAP
    UPDATE SARADAP
        SET SARADAP_APPL_DATE = SARADAP_APPL_DATE+1
    WHERE SARADAP_PIDM = P_PIDM
    AND SARADAP_TERM_CODE_ENTRY = P_TERM_CODE
    AND SARADAP_LEVL_CODE = 'PG'
    AND SARADAP_APPL_NO = V_SARADAP_APPL_NO;

    COMMIT;

EXCEPTION
WHEN OTHERS THEN
    ROLLBACK;
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_ACTIVAR_INGRESANTE;