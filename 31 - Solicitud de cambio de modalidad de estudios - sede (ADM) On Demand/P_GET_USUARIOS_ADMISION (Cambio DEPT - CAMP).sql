/*
SET SERVEROUTPUT ON
DECLARE P_CORREO_RRAA VARCHAR2(4000);
 P_ROL_DOC_RRAA VARCHAR2(4000);
 P_CORREO_DOC_RRAA VARCHAR2(4000);
 P_CORREO_BU VARCHAR2(4000);
 P_CORREO_ADM VARCHAR2(4000);
 P_CORREO_CAU VARCHAR2(4000);
 P_ROL_CAJA VARCHAR2(4000);
 P_CORREO_CAJA VARCHAR2(4000);
 P_MESSAGE VARCHAR2(4000);
BEGIN
    P_GET_USUARIOS_ADMISION ('F02','S01', P_CORREO_RRAA, P_ROL_DOC_RRAA, P_CORREO_DOC_RRAA, P_CORREO_BU, P_CORREO_ADM, P_ROL_CAJA, P_CORREO_CAJA, P_CORREO_CAU, P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_CORREO_RRAA);
    DBMS_OUTPUT.PUT_LINE(P_ROL_DOC_RRAA);
    DBMS_OUTPUT.PUT_LINE(P_CORREO_DOC_RRAA);
    DBMS_OUTPUT.PUT_LINE(P_CORREO_BU);
    DBMS_OUTPUT.PUT_LINE(P_CORREO_ADM);
    DBMS_OUTPUT.PUT_LINE(P_CORREO_CAU);
    DBMS_OUTPUT.PUT_LINE(P_ROL_CAJA);
    DBMS_OUTPUT.PUT_LINE(P_CORREO_CAJA);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
END;
*/

CREATE OR REPLACE PROCEDURE P_GET_USUARIOS_ADMISION (
    P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_CODE_NEW         IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CORREO_RRAA           OUT VARCHAR2,
    P_ROL_DOC_RRAA          OUT VARCHAR2,
    P_CORREO_DOC_RRAA       OUT VARCHAR2,
    P_CORREO_BU             OUT VARCHAR2,
    P_CORREO_ADM            OUT VARCHAR2,
    P_ROL_CAJA              OUT VARCHAR2,
    P_CORREO_CAJA           OUT VARCHAR2,
    P_CORREO_CAU            OUT VARCHAR2,
    P_MESSAGE               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_USUARIO
  FECHA     : 04/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento LOS CORREOS deL(os) responsable(s) 
              de Registros Academicos de esa sede.
  =================================================================================================================== */
AS
    -- @PARAMETERS
    V_ROLE_ID                   NUMBER;
    V_ROLE_ID_NEW               NUMBER;
    V_ORG_ID                    NUMBER;
    V_Email_Address             VARCHAR2(100);
    V_ROL_SEDE                  VARCHAR2(100);
    V_ROL_SEDE_NEW              VARCHAR2(100);
    V_ROL                       VARCHAR2(100);
    V_CORREO                    VARCHAR2(4000);
    
    V_CAMP_CODE                 VARCHAR2(4000);
    V_CAMP_CODE_NEW             VARCHAR2(4000);
    
    V_SECCION_EXCEPT            VARCHAR2(50);
    
    CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * 
        FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = V_ORG_ID
        AND ROLE_ID in (V_ROLE_ID,V_ROLE_ID_NEW);
        
    CURSOR C_OFFICE IS
        SELECT 'bienestar' FROM DUAL
        UNION
        SELECT 'registro' FROM DUAL
        UNION
        SELECT 'cau' FROM DUAL
        UNION
        SELECT 'caja' FROM DUAL
        UNION
        SELECT 'requisito' FROM DUAL
        UNION
        SELECT 'admision' FROM DUAL
        ;
      
      V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
BEGIN 
--
    OPEN C_OFFICE;
    LOOP
        FETCH C_OFFICE INTO V_ROL;
        EXIT WHEN C_OFFICE%NOTFOUND;
            
            V_CAMP_CODE := P_CAMP_CODE;
            V_CAMP_CODE_NEW := P_CAMP_CODE_NEW;
            
            IF V_ROL = 'caja' THEN
                V_CAMP_CODE := 'S01';
                V_CAMP_CODE_NEW := 'S01';
            END IF;
            
            V_CORREO := NULL;
            V_Email_Address := NULL;
            
            V_ROL_SEDE := V_ROL || V_CAMP_CODE;
            V_ROL_SEDE_NEW := V_ROL || V_CAMP_CODE_NEW;
            -- Obtener el ROL_ID 
            V_SECCION_EXCEPT := 'ROLES';
            
            IF V_ROL IN ('cau', 'requisito', 'admision') THEN
                SELECT DISTINCT ID 
                    INTO V_ROLE_ID
                FROM WORKFLOW.ROLE
                WHERE NAME = V_ROL_SEDE;
                
                SELECT DISTINCT ID 
                    INTO V_ROLE_ID_NEW
                FROM WORKFLOW.ROLE
                WHERE NAME = V_ROL_SEDE_NEW;
            ELSE
                SELECT DISTINCT ID 
                    INTO V_ROLE_ID
                FROM WORKFLOW.ROLE
                WHERE NAME = V_ROL_SEDE_NEW;
                
                V_ROLE_ID_NEW := '';
            END IF;
            
            V_SECCION_EXCEPT := '';
            
            -- Obtener el ORG_ID 
            V_SECCION_EXCEPT := 'ORGRANIZACION';
            
            SELECT ID
                INTO V_ORG_ID
            FROM WORKFLOW.ORGANIZATION
            WHERE NAME = 'Root';
            
            V_SECCION_EXCEPT := '';
            
            -- Obtener los datos de usuarios que relaciona rol y usuario
            V_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
            -- #######################################################################
            OPEN C_ROLE_ASSIGNMENT;
            LOOP
                FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
                EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
                    -- Obtener Datos Usuario
                    SELECT Email_Address
                        INTO V_Email_Address
                    FROM WORKFLOW.WFUSER
                    WHERE ID IN (V_ROLE_ASSIGNMENT_REC.USER_ID) ;
                    
                    IF V_Email_Address IS NOT NULL OR V_Email_Address <> '' THEN
                        IF V_CORREO IS NULL OR V_CORREO = '' THEN
                            V_CORREO := V_Email_Address;
                        ELSE
                            V_CORREO := V_CORREO || ',' || V_Email_Address;
                        END IF;
                    END IF;
            
                    IF V_ROL = 'bienestar' THEN
                        P_CORREO_BU := V_CORREO;
                    ELSIF V_ROL = 'registro' THEN
                        P_CORREO_RRAA := V_CORREO;
                    ELSIF V_ROL = 'requisito' THEN
                        P_CORREO_DOC_RRAA := V_CORREO;
                        P_ROL_DOC_RRAA := V_ROL_SEDE;
                    ELSIF V_ROL = 'cau' THEN
                        P_CORREO_CAU := V_CORREO;
                    ELSIF V_ROL = 'admision' THEN
                        P_CORREO_ADM := V_CORREO;
                    ELSIF V_ROL = 'caja' THEN
                        P_CORREO_CAJA := V_CORREO;
                        P_ROL_CAJA := V_ROL_SEDE;
                    END IF;
                    
            END LOOP;
            CLOSE C_ROLE_ASSIGNMENT;
            V_SECCION_EXCEPT := '';

    END LOOP;
    CLOSE C_OFFICE;
          
EXCEPTION
WHEN TOO_MANY_ROWS THEN 
    IF (V_SECCION_EXCEPT = 'ROLES') THEN
        P_MESSAGE := '- Se encontraron mas de un ROL con el mismo nombre: ' || V_ROL || P_CAMP_CODE;
    ELSIF (V_SECCION_EXCEPT = 'ORGRANIZACION') THEN
        P_MESSAGE := '- Se encontraron mas de una ORGANIZACIÓN con el mismo nombre.';
    ELSIF (V_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
        P_MESSAGE := '- Se encontraron mas de un usuario con el mismo ROL.';
    ELSE
        P_MESSAGE := SQLERRM;
    END IF;
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
    
WHEN NO_DATA_FOUND THEN
    IF (V_SECCION_EXCEPT = 'ROLES') THEN
        P_MESSAGE := '- NO se encontró el nombre del ROL: ' || V_ROL || P_CAMP_CODE;
    ELSIF (V_SECCION_EXCEPT = 'ORGRANIZACION') THEN
        P_MESSAGE := '- NO se encontró el nombre de la ORGANIZACION.';
    ELSIF (V_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
        P_MESSAGE := '- NO  se encontró ningun usuario con esas caracteristicas.';
    ELSE
        P_MESSAGE := SQLERRM;
    END IF; 
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
    
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_USUARIOS_ADMISION;