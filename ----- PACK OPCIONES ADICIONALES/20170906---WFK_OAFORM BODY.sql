CREATE OR REPLACE PACKAGE BODY WFK_OAFORM AS
/*******************************************************************************
 WFK_OAFORM:
       Conti Package OPCIONES ADICIONALES FORMA
*******************************************************************************/
-- FILE NAME..: WFK_OAFORM.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_OAFORM
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -  
  DESCRIPTION END 
*******************************************************************************/


FUNCTION F_RSS_SRIR (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_SRIR
              "Regla Solicitud de Servicio de SOLICITUD RECTIFICACIÓN DE INSCRIPCIÓN REZAGADOS"
  FECHA     : 20/02/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 

  NRO     FECHA         USUARIO       MODIFICACION
  001     09/02/2017    RMALLQUI      Agrego validacion solo para matriculados (CON NRC)   
  002     26/04/2017    RMALLQUI      Se agrego validacion para el segundo modulo de UVIR y UPGT
  003     13/06/2017    RMALLQUI      Se prioriza y obtiene el periodo mas antiguo en caso de cruce de fechas.
  =================================================================================================================== */
 IS
      P_CODIGO_SOL          SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL003';
      V_PRIORIDAD           NUMBER;
      P_INDICADOR           NUMBER;
      V_CODE_DEPT           STVDEPT.STVDEPT_CODE%TYPE; 
      V_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
      V_CODE_CAMP           STVCAMP.STVCAMP_CODE%TYPE;

      P_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;

      -- Parte-de-Periodo
      V_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_PTRM_CODE           SOBPTRM.SOBPTRM_PTRM_CODE%TYPE;
      
      -- Calculando parte PERIODO (sub PTRM)
      CURSOR C_SFRRSTS_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = V_CODE_DEPT AND STVCAMP_CODE = V_CODE_CAMP;      
      
      -- OBTENER PERIODO por modulo de UVIR y UPGT
      CURSOR C_SFRRSTS IS
      SELECT SFRRSTS_TERM_CODE, SFRRSTS_PTRM_CODE FROM (
          -- Forma SFARSTS  ---> fechas para las partes de periodo
          SELECT DISTINCT SFRRSTS_TERM_CODE, SFRRSTS_PTRM_CODE
          FROM SFRRSTS
          WHERE SFRRSTS_PTRM_CODE IN (V_SUB_PTRM || '1', V_SUB_PTRM || '2')
          AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
          AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SFRRSTS_END_DATE
          ORDER BY SFRRSTS_TERM_CODE, SFRRSTS_PTRM_CODE 
      ) WHERE ROWNUM <= 1;    
BEGIN  
--    
    -- DATOS ALUMNO
    P_GETDATA_ALUMN(P_PIDM,V_CODE_DEPT,V_CODE_TERM,V_CODE_CAMP);

    
    ------------------------------------------------------------
    -- >> calculando SUB PARTE PERIODO  --
    OPEN C_SFRRSTS_PTRM;
    LOOP
      FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
      EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
    END LOOP;
    CLOSE C_SFRRSTS_PTRM;

    /************************************************************************************************************/
    -- Segun regla para UPGT y UVIR, los que tramitan reserva el mismo periodo que ingresaron 
    /************************************************************************************************************/
    IF  (V_CODE_DEPT = 'UPGT' OR V_CODE_DEPT = 'UVIR') THEN

          OPEN C_SFRRSTS;
            LOOP
              FETCH C_SFRRSTS INTO P_CODE_TERM, V_PTRM_CODE;
              IF C_SFRRSTS%FOUND THEN
                  V_CODE_TERM := P_CODE_TERM;
              ELSE EXIT;
            END IF;
            END LOOP;
          CLOSE C_SFRRSTS;

    END IF;


    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD (P_PIDM, P_CODIGO_SOL, V_CODE_DEPT, NULL, NULL, V_PRIORIDAD);

    -- VERIFICAR QUE YA ESTE MATRICULADO
    ---A VERIFICAR QUE TENGA NRC's
      SELECT COUNT(*) INTO P_INDICADOR FROM SFRSTCR 
      WHERE SFRSTCR_PIDM = P_PIDM
      AND SFRSTCR_PTRM_CODE = (CASE WHEN V_PTRM_CODE IS NULL THEN (V_SUB_PTRM || '1') ELSE V_PTRM_CODE END) 
      AND SFRSTCR_TERM_CODE = V_CODE_TERM; 
    
    IF V_PRIORIDAD > 0 AND P_INDICADOR > 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
--
END F_RSS_SRIR;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_RSS_SRM (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_SRM
              "Regla Solicitud de Servicio de SOLICITUD RESERVA DE MATRICULA"
  FECHA     : 20/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 

  NRO     FECHA         USUARIO       MODIFICACION
  001     26/04/2017    RMALLQUI      Se agrego validacion para el segundo modulo de UVIR y UPGT
  002     13/06/2017    RMALLQUI      Se prioriza y obtiene el periodo mas antiguo en caso de cruce de fechas.
  =================================================================================================================== */
 IS
      V_CODIGO_SOL      SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL005';
      V_CODE_DEPT       STVDEPT.STVDEPT_CODE%TYPE;
      V_CODE_TERM       STVTERM.STVTERM_CODE%TYPE;
      V_CODE_CAMP       STVCAMP.STVCAMP_CODE%TYPE;
      V_PRIORIDAD       NUMBER;
      V_TERM_ADMI       NUMBER;
      P_PERIODOCTLG     SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';
      
      V_INGRESANTE      BOOLEAN := FALSE;
      V_NCURSOS         NUMBER;
      V_FECHA_EXM       DATE;
      V_FECHA_VALIDA    BOOLEAN;

      P_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
      V_PART_PERIODO        VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      
      -- Calculando parte PERIODO (sub PTRM)
      CURSOR C_SFRRSTS_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = V_CODE_DEPT AND STVCAMP_CODE = V_CODE_CAMP;
      
      -- OBTENER PERIODO por modulo de UVIR y UPGT
      CURSOR C_SFRRSTS IS
      SELECT SFRRSTS_TERM_CODE FROM (
          -- Forma SFARSTS  ---> fechas para las partes de periodo
          SELECT DISTINCT SFRRSTS_TERM_CODE 
          FROM SFRRSTS
          WHERE SFRRSTS_PTRM_CODE IN (V_PART_PERIODO || '1', V_PART_PERIODO || '2')
          AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
          AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SFRRSTS_END_DATE
          ORDER BY SFRRSTS_TERM_CODE
      ) WHERE ROWNUM <= 1;   
BEGIN  
--    
    -- DATOS ALUMNO
    P_GETDATA_ALUMN(P_PIDM,V_CODE_DEPT,V_CODE_TERM,V_CODE_CAMP);

    ------------------------------------------------------------
    -- >> calculando SUB PARTE PERIODO  --
    OPEN C_SFRRSTS_PTRM;
    LOOP
      FETCH C_SFRRSTS_PTRM INTO V_PART_PERIODO;
      EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
    END LOOP;
    CLOSE C_SFRRSTS_PTRM;
    
      
    IF  (V_CODE_DEPT = 'UPGT' OR V_CODE_DEPT = 'UVIR') THEN

          OPEN C_SFRRSTS;
            LOOP
              FETCH C_SFRRSTS INTO P_CODE_TERM;
              IF C_SFRRSTS%FOUND THEN
                  V_CODE_TERM := P_CODE_TERM;
              ELSE EXIT;
            END IF;
            END LOOP;
          CLOSE C_SFRRSTS;
        
    END IF;

    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD(P_PIDM, V_CODIGO_SOL, V_CODE_DEPT, V_CODE_TERM, V_CODE_CAMP, V_PRIORIDAD);

    /************************************************************************************************************/
    -- Segun regla para UPGT y UVIR, los que tramitan reserva el mismo periodo que ingresaron 
    /************************************************************************************************************/
    IF  (V_CODE_DEPT = 'UPGT' AND V_PRIORIDAD = 4) OR     -- se les RESERVA las prioridades "4" 
        (V_CODE_DEPT = 'UVIR' AND V_PRIORIDAD = 7)        -- se les RESERVA las prioridades "7"
        THEN

        -- VALIDAR QUE EL ALUMNO SEA INGRESANTE EN EL MISMO PERIODO DEL TRAMITE. (PERIODO DE INGRESO = PERIODO ACTUAL)
        SELECT  COUNT(*) INTO V_TERM_ADMI
        FROM SORLCUR        INNER JOIN SORLFOS
        ON    SORLCUR_PIDM          = SORLFOS_PIDM 
        AND   SORLCUR_SEQNO         = SORLFOS.SORLFOS_LCUR_SEQNO
        WHERE SORLCUR_PIDM          =  P_PIDM
        AND SORLCUR_LMOD_CODE       = 'LEARNER' /*#Estudiante*/ 
        AND SORLCUR_CACT_CODE       = 'ACTIVE' 
        AND SORLCUR_CURRENT_CDE     = 'Y'
        AND SORLCUR_TERM_CODE_ADMIT = V_CODE_TERM -- VALIDACION (PERIODO DE INGRESO = PERIODO ACTUAL)
        AND SORLFOS_DEPT_CODE       = V_CODE_DEPT;

    /************************************************************************************************************/
    -- para casos de INGREASANTES solo regular huancayo - VALIDACIONES POR FECHA DE EXAMEN
    /************************************************************************************************************/
    ELSIF  (V_CODE_DEPT = 'UREG' AND V_PRIORIDAD = 2 AND V_CODE_CAMP = 'S01')  THEN     -- Ingresanter periodo activo regular  con prioridad '2'

        P_DATOS_INGRESO(P_PIDM , V_CODE_CAMP,V_CODE_TERM,V_CODE_DEPT,V_INGRESANTE,V_FECHA_EXM);
        
        SELECT COUNT(*) INTO V_NCURSOS FROM SFRSTCR 
        WHERE SFRSTCR_PIDM = P_PIDM
        AND SFRSTCR_TERM_CODE = V_CODE_TERM;
        
        V_FECHA_VALIDA := CASE WHEN TO_CHAR(V_FECHA_EXM,'dd/mm/yyyy') = '26/03/2017' THEN TRUE
                               WHEN TO_CHAR(V_FECHA_EXM,'dd/mm/yyyy') = '02/04/2017' THEN TRUE
                               WHEN V_NCURSOS = 0 THEN TRUE ELSE FALSE END;
         
        IF (V_INGRESANTE AND V_FECHA_VALIDA) THEN
            V_TERM_ADMI := 1;
        ELSE 
            V_TERM_ADMI := 0;
        END IF;

    ELSE
        V_TERM_ADMI := 1;
    END IF;

    
    IF (V_PRIORIDAD > 0 AND V_TERM_ADMI > 0) THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';

--
END F_RSS_SRM;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_RSS_CPE (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_CPE
              "Regla Solicitud de Servicio de CAMBIO de PLAN ESTUDIO"
  FECHA     : 07/12/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 

  MODIFICACIONES
  NRO     FECHA         USUARIO       MODIFICACION
  001     16/06/2017    RMALLQUI      Se agregaron filtros: Especificados en el acta 006-WF-2017 con fecha 07-Jun-2017
  =================================================================================================================== */
 IS

      V_CODIGO_SOL            SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL004';
      V_PERIODOCTLG           SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';
      V_PERIODOCTLG_MAX       SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';
      V_CODE_DEPT             STVDEPT.STVDEPT_CODE%TYPE;
      V_CODE_TERM             STVTERM.STVTERM_CODE%TYPE;
      V_CODE_CAMP             STVCAMP.STVCAMP_CODE%TYPE;
      V_PROGRAM               SORLCUR.SORLCUR_PROGRAM%TYPE;
      
      V_SUB_PTRM              VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_PTRM_CODE             STVPTRM.STVPTRM_CODE%TYPE;
      V_MATRIC_FECHA          SFRRSTS.SFRRSTS_START_DATE%TYPE; --- Fecha de una semana antes de matricula
      
      V_PRIORIDAD             NUMBER;
      V_HAS_RETENCION         NUMBER;
      V_HAS_NRC               NUMBER;
      V_TERM_PREVIUS          STVTERM.STVTERM_CODE%TYPE;
      
      -- OBTENER PERIODO DE CAMPUS, DEPT, CATALOGO y la CARRERA
      CURSOR C_SORLCUR IS
      SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
      FROM(
          SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
          FROM SORLCUR 
          INNER JOIN SORLFOS
              ON SORLCUR_PIDM         = SORLFOS_PIDM 
              AND SORLCUR_SEQNO       = SORLFOS_LCUR_SEQNO
          WHERE SORLCUR_PIDM          = P_PIDM 
              AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
              AND SORLCUR_CACT_CODE   = 'ACTIVE' 
              AND SORLCUR_CURRENT_CDE = 'Y'
          ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM = 1;
      
      -- Calculando parte PERIODO (sub PTRM)
      CURSOR C_SFRRSTS_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = V_CODE_DEPT AND STVCAMP_CODE = V_CODE_CAMP;     
      
      
      /******************************************************************************************************
      GET TERM (PERIODO - Solo usando parte de periodo 1) 
           A LA FECHA QUE ESTE DENTRO DE LA FECHA DE INICIO DE MATRICULA(SFRRSTS_START_DATE) y 
           - y UNA SEMANA ANTES DE INICIO DE CLASES(SOBPTRM_START_DATE-7) -- LOS Q NO ESTUDIARON EL PERIODO ANTERIOR
           - HASTA 1 mes depsues de inicio de clases (SOBPTRM_START_DATE) -- ALUMNO NORMAL 
           - Hasta 1 mes depsues para calcular el PERIODO Devido a las constantes ampliaciones de fechas.
      */
      CURSOR C_STUDN_TERM IS
      SELECT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE, FECHA_MATRICULA FROM (
          SELECT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE, FECHA_MATRICULA FROM (
              -- Forma SOATERM - SFARSTS  ---> fechas para las partes de periodo
              SELECT DISTINCT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE, TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') FECHA_MATRICULA
              FROM SOBPTRM
              INNER JOIN SFRRSTS
                ON SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
                AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
              WHERE SFRRSTS_RSTS_CODE = 'RW'
              AND SOBPTRM_PTRM_CODE IN (V_SUB_PTRM || '1' ,  CASE WHEN V_CODE_DEPT='UVIR' OR V_CODE_DEPT='UPGT' THEN (V_SUB_PTRM||'2') ELSE '-' END ) 
              AND SOBPTRM_PTRM_CODE NOT LIKE '%0'          
              AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE-7,'dd/mm/yyyy'),'dd/mm/yyyy')  AND (ADD_MONTHS(SOBPTRM_START_DATE,1))
              ORDER BY SOBPTRM_TERM_CODE 
          ) ORDER BY SOBPTRM_TERM_CODE ,SOBPTRM_PTRM_CODE 
      ) WHERE ROWNUM <= 1;
      
      -- GET - PREVIUS TERM(Periodo)
      CURSOR C_PREVIUS_TERM IS
      SELECT PREVIUS_TERM 
        FROM (
            SELECT  STVTERM_CODE, 
                    LAG(STVTERM_CODE, 1, 0) OVER (ORDER BY STVTERM_CODE) PREVIUS_TERM, 
                    LEAD(STVTERM_CODE, 1, 0) OVER (ORDER BY STVTERM_CODE) NEXT_TERM
            FROM STVTERM ORDER BY STVTERM_CODE DESC
        ) WHERE STVTERM_CODE = V_CODE_TERM AND ROWNUM = 1;
--      
BEGIN  
--    
      OPEN C_SORLCUR;
      LOOP
        FETCH C_SORLCUR INTO V_CODE_CAMP, V_CODE_DEPT, V_PERIODOCTLG, V_PROGRAM;
        EXIT WHEN C_SORLCUR%NOTFOUND;
        END LOOP;
      CLOSE C_SORLCUR;
      
      
      -- >> calculando PARTE PERIODO  --
      OPEN C_SFRRSTS_PTRM;
      LOOP
        FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
        EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
      END LOOP;
      CLOSE C_SFRRSTS_PTRM;
      
  
      -- VERIFICAR QUE EL ALUMNO CUMPLA CON LAS RETENCIONES: '07' y/o '08' (Forma SOAHOLD)
      SELECT COUNT(*) INTO V_HAS_RETENCION FROM SPRHOLD
      WHERE SPRHOLD_PIDM = P_PIDM
      AND SPRHOLD_HLDD_CODE IN ('07','08')
      AND (SYSDATE > SPRHOLD_FROM_DATE AND SPRHOLD_TO_DATE > SYSDATE);
      
      
      -- VALIDACION DE RESTRICCIONES
      IF /* TENER RETENCION 07 ó 08 */ (V_HAS_RETENCION = 0) OR 
         /* PERTENECER A UREG de la CARRERA 309 ó 308*/ (V_CODE_DEPT = 'UREG' AND (V_PROGRAM = '309' OR V_PROGRAM = '308')) OR
         /* PERTENECER A UPGT de la CARRERA 309 ó 318*/ (V_CODE_DEPT = 'UPGT' AND (V_PROGRAM = '309' OR V_PROGRAM = '318')) THEN
         
         RETURN 'N';
         
      END IF;
      
      
      /******************************************************************************************************
      GET TERM (PERIODO - Solo usando parte de periodo 1) */
      OPEN C_STUDN_TERM;
      LOOP
        FETCH C_STUDN_TERM INTO V_CODE_TERM, V_PTRM_CODE, V_MATRIC_FECHA;
        EXIT WHEN C_STUDN_TERM%NOTFOUND;
        END LOOP;
      CLOSE C_STUDN_TERM;
      
      
      /******************************************************************************/
      -- PARA SEGUNDO MODULO: verificar que no tenga NRC primer modulo
      IF( V_PTRM_CODE = (V_SUB_PTRM||'2')) THEN

          SELECT COUNT(*) INTO V_HAS_NRC FROM SFRSTCR  
          WHERE SFRSTCR_TERM_CODE = V_CODE_TERM
          AND SFRSTCR_PIDM = P_PIDM
          AND SFRSTCR_PTRM_CODE = (V_SUB_PTRM||'1');
          
          IF  (V_HAS_NRC > 0 OR  -- No haber estudiado en el primer modulo
              SYSDATE < (V_MATRIC_FECHA) OR -- En segundo modulo: desde inicio de matricula <-> inicio de clases 
              V_CODE_TERM IS NULL) THEN
              
              RETURN 'N';
              
          END IF;
      
      -- PARA PRIMER MODULO O UN CASO NORMAL: verificar si estudió el periodo anterior.
      ELSE
          
          OPEN C_PREVIUS_TERM;
          LOOP
              FETCH C_PREVIUS_TERM INTO V_TERM_PREVIUS;
              EXIT WHEN C_PREVIUS_TERM%NOTFOUND;
              END LOOP;
          CLOSE C_PREVIUS_TERM;
          
          SELECT COUNT(*) INTO V_HAS_NRC FROM SFRSTCR  
          WHERE SFRSTCR_TERM_CODE = V_TERM_PREVIUS
          AND SFRSTCR_PIDM = P_PIDM;
          
          IF  (V_HAS_NRC > 0 AND SYSDATE < V_MATRIC_FECHA) OR  -- DE haber estudiado el periodo anterior: matricula <-> inicio de clases            
              V_CODE_TERM IS NULL OR V_TERM_PREVIUS IS NULL THEN
              
              RETURN 'N';
              
          END IF;
          
      END IF;
      
      -- VALIDAR MODALIDAD Y PRIORIDAD
      P_CHECK_DEPT_PRIORIDAD (P_PIDM, V_CODIGO_SOL, NULL,NULL,NULL, V_PRIORIDAD);
            
      IF(V_PERIODOCTLG < V_PERIODOCTLG_MAX) AND V_PRIORIDAD > 0 THEN
          RETURN 'Y';
      ELSE
          RETURN 'N';
      END IF;
--
END F_RSS_CPE;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_RSS_SRD (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_SRD
              "Regla Solicitud de Servicio de SOLICITUD DE SOBREPASO DE RETENCION DE DOCUMENTOS"
  FECHA     : 20/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 
  =================================================================================================================== */
 IS
      P_CODIGO_SOL             SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL006';
      P_PRIORIDAD              NUMBER;
      
BEGIN  
--    
    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD (P_PIDM, P_CODIGO_SOL, NULL,NULL,NULL, P_PRIORIDAD);
    
    IF P_PRIORIDAD > 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
--
END F_RSS_SRD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_RSS_SMCI (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_SMCI
              "Regla Solicitud de Servicio 
              de MODIFICACIÒN de la CUOTA INICIAL (MATRICULA ESPECIAL)  SOL007"
  FECHA     : 19/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 
  =================================================================================================================== */
 IS
      P_SEQNO                   SORLCUR.SORLCUR_SEQNO%TYPE := 0;
      P_DEPARTAMENTO            SORLFOS.SORLFOS_DEPT_CODE%TYPE := '-';
      P_CODIGO_SOL              SVRRSRV.SVRRSRV_SRVC_CODE%TYPE := 'SOL007';
      P_PRIORIDAD               NUMBER := 0;
      
      -- GET datos de alumno 
      CURSOR C_SORLCUR IS
      SELECT    SORLCUR_SEQNO, SORLFOS_DEPT_CODE  
      FROM (
              SELECT    SORLCUR_SEQNO,      SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE =   'Y'
              ORDER BY SORLCUR_TERM_CODE DESC,SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM <= 1;
      
BEGIN  
--
    -- #######################################################################
    -- GET datos de alumno 
    OPEN C_SORLCUR;
    LOOP
      FETCH C_SORLCUR INTO P_SEQNO, P_DEPARTAMENTO;
      EXIT WHEN C_SORLCUR%NOTFOUND;
      END LOOP;
    CLOSE C_SORLCUR;
    IF P_DEPARTAMENTO = '-' THEN RETURN 'N'; END IF;
    
    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD(P_PIDM, P_CODIGO_SOL, P_DEPARTAMENTO,NULL,NULL, P_PRIORIDAD);
    
    IF P_PRIORIDAD > 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
        
END F_RSS_SMCI;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_RSS_SR (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_SR
              "Regla Solicitud de Servicio de SOLICITUD DE REINCORPORACIÒN"
  FECHA     : 20/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 
  =================================================================================================================== */
 IS
      P_CODIGO_SOL             SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL009';
      P_PRIORIDAD              NUMBER;
      
BEGIN  
--    
    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD (P_PIDM, P_CODIGO_SOL, NULL,NULL,NULL, P_PRIORIDAD);
    
    IF P_PRIORIDAD > 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
--
END F_RSS_SR;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_RSS_SESM (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_SESM
              "Regla Solicitud de Servicio de SOLICITUD DE EXONERACIÓN DE SEGURO DE SALUD"
  FECHA     : 20/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 
  =================================================================================================================== */
 IS
      P_CODIGO_SOL             SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL012';
      P_PRIORIDAD              NUMBER;
      
BEGIN  
--    
    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD (P_PIDM, P_CODIGO_SOL, NULL,NULL,NULL, P_PRIORIDAD);
    
    IF P_PRIORIDAD > 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
--
END F_RSS_SESM;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_RSS_STI (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_STI
              "Regla Solicitud de Servicio de SOLICITUD DE TRASLADO INTERNO "
  FECHA     : 08/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : - para alumnos con mayor a 22 creditos
              - No estar estudiando ni tener asignaturas en el presente periodo
              - El periodo se determina desde inicio de matricula y una semana antes de inicio de clases.
              

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  001   27/07/2017    RMALLQUI      Se agrego la VALIDACION de retencion ('01'-deuda pendiente)
  002   02/08/2017    RMALLQUI      Se agrego la validacion de que la solicitud solo se puede realizar una vez por periodo.
  =================================================================================================================== */
IS
      P_CODIGO_SOL          SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL013';
      V_CODE_DEPT           STVDEPT.STVDEPT_CODE%TYPE; 
      V_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
      V_CODE_CAMP           STVCAMP.STVCAMP_CODE%TYPE;

      P_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
      V_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_1          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_2          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_PTRM                SOBPTRM.SOBPTRM_PTRM_CODE%TYPE;
      V_CREDITOS            INTEGER;
      V_CICLO               INTEGER;
      V_NRC_N               INTEGER;
      V_PRIORIDAD           NUMBER;
      V_INDICADOR           NUMBER;

      -- Calculando parte PERIODO (sub PTRM)
      CURSOR C_SFRRSTS_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = V_CODE_DEPT AND STVCAMP_CODE = V_CODE_CAMP;


      /******************************************************************************************************
        GET TERM (PERIODO - Solo usando parte de periodo 1) 
             A LA FECHA QUE ESTE DENTRO DE LA FECHA DE INICIO DE MATRICULA(SFRRSTS_START_DATE) y 
             y UNA SEMANA despues DE INICIO DE CLASES(SOBPTRM_START_DATE+7)
      */
      CURSOR C_SOBPTRM IS
      SELECT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE FROM (
          -- Forma SOATERM - SFARSTS  ---> fechas para las partes de periodo
          SELECT DISTINCT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE
          FROM SOBPTRM
          INNER JOIN SFRRSTS
            ON SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
            AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
          WHERE SFRRSTS_RSTS_CODE = 'RW'
          AND SOBPTRM_PTRM_CODE = V_SUB_PTRM_1 -- Solo parte de periodo '%1'
          AND SOBPTRM_PTRM_CODE NOT LIKE '%0'          
          AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')  AND (ADD_MONTHS(SOBPTRM_START_DATE,1))
          ORDER BY SOBPTRM_TERM_CODE DESC
      ) WHERE ROWNUM <= 1;


      -- Validar si tiene NRC activo en un determinado periodo (*** No se esta filtrando parte de periodo para los casos de modulos de UVIR Y UPGT)
      CURSOR C_SFRSTCR_N IS
      SELECT COUNT(*) NRC_ON FROM SFRSTCR
      WHERE SFRSTCR_TERM_CODE = V_CODE_TERM
      AND SFRSTCR_PIDM = P_PIDM
      AND SFRSTCR_RSTS_CODE IN ('RW','RE');
BEGIN  
--   
    -- VALIDACIÓN QUE NO TENGA RETENCION DE DEUDA "01" : (Se esta reduciendo la fecha SPRHOLD_TO_DATE a las 00:00:00 horas)
    SELECT COUNT(*) INTO V_INDICADOR 
    FROM SPRHOLD
    WHERE SPRHOLD_PIDM = P_PIDM 
    AND SPRHOLD_HLDD_CODE = '01' -- RETENCION Deuda pendiente
    AND (SPRHOLD_TO_DATE <= SYSDATE AND TO_DATE(TO_CHAR(SPRHOLD_TO_DATE, 'dd/mm/yyyy'),'dd/mm/yyyy') > SYSDATE); 
    IF V_INDICADOR > 0 THEN
        RETURN 'N';
    END IF;

    -- DATOS ALUMNO
    P_GETDATA_ALUMN(P_PIDM,V_CODE_DEPT,V_CODE_TERM,V_CODE_CAMP);


    ------------------------------------------------------------
    -- >> calculando SUB PARTE PERIODO  --
    OPEN C_SFRRSTS_PTRM;
    LOOP
      FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
      EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
    END LOOP;
    CLOSE C_SFRRSTS_PTRM;


    -- PARTE DE PERIODOS
    V_SUB_PTRM_1 := V_SUB_PTRM || '1';
    V_SUB_PTRM_2 := V_SUB_PTRM || CASE WHEN (V_CODE_DEPT ='UVIR' OR V_CODE_DEPT ='UPGT') THEN '2' ELSE '-' END;


    ------------------------------------------------------------
    -- >> Obteniendo PERIODO ACTIVO Y LA PARTE PERIODO  --
    V_CODE_TERM := NULL;
    OPEN C_SOBPTRM;
      LOOP
        FETCH C_SOBPTRM INTO P_CODE_TERM, V_PTRM;
        IF C_SOBPTRM%FOUND THEN
            V_CODE_TERM := P_CODE_TERM;
        ELSE EXIT;
      END IF;
      END LOOP;
    CLOSE C_SOBPTRM;
    IF V_CODE_TERM IS NULL THEN
        RETURN 'N';
    END IF;
    
    
    ------------------------------------------------------------
    -- >> VALIDACION:  SOLO se puede un solo TRASLADO INTERNO por PERIODO.
    SELECT COUNT(SVRSVPR_PROTOCOL_SEQ_NO) INTO V_INDICADOR FROM SVRSVPR 
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_SRVC_CODE = P_CODIGO_SOL
        AND SVRSVPR_SRVS_CODE = 'CO'; -- Estado 'CO'-Completado
    IF V_INDICADOR > 0 THEN
        RETURN 'N';
    END IF;


    ---------------------------------------------------------------------------------------------
    -- >> Validar si el estudiante esta estudiando en el periodo actual activo (TAMBIEN VALIDA por modulo PARTE PERIODO)
    OPEN C_SFRSTCR_N;
    LOOP
      FETCH C_SFRSTCR_N INTO V_NRC_N;
      EXIT WHEN C_SFRSTCR_N%NOTFOUND;
    END LOOP;
    CLOSE C_SFRSTCR_N;    


    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD(P_PIDM, P_CODIGO_SOL, V_CODE_DEPT,NULL,V_CODE_CAMP, V_PRIORIDAD);
    IF (V_PRIORIDAD <>  3 AND V_PRIORIDAD <> 5 AND V_PRIORIDAD <> 8) THEN
        RETURN 'N';
    END IF;


    -----------------------------------------------
    -- GET CREDITOS CICLO STUDN
    P_CREDITOS_CICLO(P_PIDM,V_CREDITOS,V_CICLO);
    

    IF V_CREDITOS >= 22 AND V_NRC_N = 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
--
END F_RSS_STI;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_RSS_SPGA (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_UA_RSS_SPGA
              "Regla Solicitud de Servicio de SOLICITUD DE PRORROGA GASTOS ADMINISTRATIVOS "
  FECHA     : 06/07/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : La solicitud disponible solo:
              - Disponible desde el 20 de cada mes hasta el 7 del siguiente mes.
  =================================================================================================================== */
IS
      P_CODIGO_SOL          SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL013';
      V_RETUR               BOOLEAN := FALSE;
      V_CODE_DEPT           STVDEPT.STVDEPT_CODE%TYPE; 
      V_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
      P_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
      V_CODE_CAMP           STVCAMP.STVCAMP_CODE%TYPE;
      V_PROGRAM             SORLCUR.SORLCUR_PROGRAM%TYPE;
      
      V_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_PTRM                SOBPTRM.SOBPTRM_PTRM_CODE%TYPE;
      V_NRC_N               INTEGER;

      V_FEC_PRORRG_INI      DATE;
      V_FEC_PRORRG_FIN      DATE;
      
      V_APEC_CAMP           VARCHAR2(9);
      V_APEC_TERM           VARCHAR2(9);
      V_ROWNUM              NUMBER;
      V_IDSECCIONC          VARCHAR2(18);
      V_CONCEPTO            VARCHAR2(9);
      V_FECCARGO            DATE;
      V_PRORROGA            VARCHAR2(50);

      -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
      CURSOR GET_DATACURR_C IS
      SELECT    SORLCUR_CAMP_CODE,
                SORLFOS_DEPT_CODE,
                SORLCUR_PROGRAM
      FROM (
              SELECT    SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_PROGRAM 
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE =   'Y'
              ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM = 1;

      -- Calculando parte PERIODO (sub PTRM --> SFRRSTS)
      CURSOR GET_PTRM_C IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = V_CODE_DEPT AND STVCAMP_CODE = V_CODE_CAMP;


      -- OBTENER PERIODO ACTIVO (SOATERM) inicio a fin
      CURSOR SOBPTRM_C IS
      SELECT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE FROM
      (
        SELECT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE, MIN(SOBPTRM_START_DATE) SOBPTRM_START_DATE, MAX(SOBPTRM_END_DATE) SOBPTRM_END_DATE
        FROM SOBPTRM
        WHERE SOBPTRM_PTRM_CODE IN (V_SUB_PTRM||'1' , (V_SUB_PTRM || CASE WHEN (V_CODE_DEPT ='UVIR' OR V_CODE_DEPT ='UPGT') THEN '2' ELSE '-' END) ) 
        AND SOBPTRM_PTRM_CODE NOT LIKE '%0'
        GROUP BY SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE
      ) WHERE ROWNUM = 1
      AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SOBPTRM_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SOBPTRM_END_DATE
      ORDER BY SOBPTRM_TERM_CODE DESC;  


      -- Validar si tiene NRC activo en un determinado periodo (***Se esta filtrando parte de periodo para los casos de modulos de UVIR Y UPGT)
      CURSOR SFRSTCR_N_C IS
      SELECT COUNT(*) NRC_ON FROM SFRSTCR
      WHERE SFRSTCR_TERM_CODE = V_CODE_TERM
            AND SFRSTCR_PIDM = P_PIDM
            AND SFRSTCR_PTRM_CODE = V_PTRM
            AND SFRSTCR_RSTS_CODE IN ('RW','RE');

      -- GET FECHAS DE cARGO Y los conceptos validos para las ProrrogaS
        -- UREG : C02,C03,C04
        -- UPGT : C02,C03
      CURSOR GET_FCARGO_C IS
      SELECT rownum, IDSeccionC, IDConcepto, FecCargo, Prorroga
      FROM (
          WITH 
              CTE_tblSeccionC AS (
                      -- GET SECCIONC
                      SELECT  "IDSeccionC" IDSeccionC,
                              "FecInic" FecInic
                      FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                      WHERE "IDDependencia"='UCCI'
                      AND "IDsede"    = V_APEC_CAMP
                      AND "IDPerAcad" = V_APEC_TERM
                      AND "IDEscuela" = V_PROGRAM
                      AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                      AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                      AND "IDSeccionC" <> '15NEX1A'
                      AND SUBSTRB("IDSeccionC",-2,2) IN (
                          -- PARTE PERIODO           
                          SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE = V_PTRM
                      )
              ),
              CTE_tblPersonaAlumno AS (
                      SELECT "IDAlumno" IDAlumno 
                      FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
                      WHERE "IDPersona" IN ( 
                          SELECT "IDPersona" FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM
                      )
              )
          SELECT "IDSeccionC" IDSeccionC,"IDConcepto" IDConcepto, "FecCargo" FecCargo, "Prorroga" Prorroga 
          FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia" = 'UCCI'
          AND "IDAlumno"     IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
          AND "IDSede"      = V_APEC_CAMP
          AND "IDPerAcad"   = V_APEC_TERM
          AND "IDEscuela"   = V_PROGRAM
          AND "IDSeccionC"  IN (SELECT IDSeccionC FROM CTE_tblSeccionC)
          AND "Prorroga"    = '0'
          AND "IDConcepto"  IN ('C02','C03', CASE WHEN V_CODE_DEPT = 'UREG' THEN 'C04' ELSE '-' END) 
      );

BEGIN  
--    
    -- >> GET DEPARTAMENTO, CAMPUS y PROGRAM --
    OPEN GET_DATACURR_C;
    LOOP
        FETCH GET_DATACURR_C INTO V_CODE_CAMP,V_CODE_DEPT,V_PROGRAM ;
        EXIT WHEN GET_DATACURR_C%NOTFOUND;
    END LOOP;
    CLOSE GET_DATACURR_C;

    -- GET DATOS ALUMNO
    -- P_GETDATA_ALUMN(P_PIDM,V_CODE_DEPT,V_CODE_TERM,V_CODE_CAMP);


    ------------------------------------------------------------
    -- >> calculando SUB PARTE PERIODO  --
    OPEN GET_PTRM_C;
    LOOP
        FETCH GET_PTRM_C INTO V_SUB_PTRM;
        EXIT WHEN GET_PTRM_C%NOTFOUND;
    END LOOP;
    CLOSE GET_PTRM_C;


    ------------------------------------------------------------
    -- >> Obteniendo PERIODO ACTIVO Y LA PARTE PERIODO  --
    OPEN SOBPTRM_C;
      LOOP
        FETCH SOBPTRM_C INTO P_CODE_TERM, V_PTRM;
        IF SOBPTRM_C%FOUND THEN
            V_CODE_TERM := P_CODE_TERM;
        ELSE EXIT;
      END IF;
      END LOOP;
    CLOSE SOBPTRM_C;


    ---------------------------------------------------------------------------------------------
    -- >> Validar si el estudiante esta estudiando en el periodo actual activo (TAMBIEN VALIDA por modulo PARTE PERIODO)
    OPEN SFRSTCR_N_C;
    LOOP
      FETCH SFRSTCR_N_C INTO V_NRC_N;
      EXIT WHEN SFRSTCR_N_C%NOTFOUND;
    END LOOP;
    CLOSE SFRSTCR_N_C;   
  
    IF(V_NRC_N = 0) THEN RETURN 'N'; END IF;


    ----------------------------------------------------------------------------------
    -- GET FECHAS DE cARGO Y los conceptos validos para las ProrrogaS
        -- UREG : C02,C03,C04
        -- UPGT : C02,C03

        SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = V_CODE_CAMP;-- CAMPUS 
        SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = V_CODE_TERM;-- PERIODO
        DBMS_OUTPUT.PUT_LINE(SYSDATE-30);
        OPEN GET_FCARGO_C;
            LOOP 
                FETCH GET_FCARGO_C INTO V_ROWNUM, V_IDSECCIONC, V_CONCEPTO, V_FECCARGO, V_PRORROGA;
                IF GET_FCARGO_C%FOUND THEN
                    ------------------ LAS REGLAS PARA LOS DEPARTAMENTOS YA SE FILTRARON EN LA CONSULTA -----------------
                    -- En la regla de caja y bienestar la fecha de vencimiento de pago es el 4 de cada mes a una pension.
                    V_FEC_PRORRG_INI := TO_DATE(('20/' || TO_CHAR(ADD_MONTHS(V_FECCARGO, -1), 'mm/yyyy')),'dd/mm/yyyy');
                    V_FEC_PRORRG_FIN := TO_DATE(('07/' || TO_CHAR(V_FECCARGO,'mm/yyyy')),'dd/mm/yyyy') + 1;
                    IF (V_FEC_PRORRG_INI <= (SYSDATE) AND (SYSDATE) < V_FEC_PRORRG_FIN) THEN
                        RETURN 'Y';
                    END IF;
                    ------------------------------------------------------------
                ELSE EXIT;
            END IF;
            END LOOP;
        CLOSE GET_FCARGO_C;

    RETURN 'N';
--
END F_RSS_SPGA;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CHECK_DEPT_PRIORIDAD(
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       IN SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_CODE_DEPT       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CODE_TERM       IN STVTERM.STVTERM_CODE%TYPE,
    P_CODE_CAMP       IN STVCAMP.STVCAMP_CODE%TYPE,
    P_PRIORIDAD       OUT NUMBER
) 
/* ===================================================================================================================
  NOMBRE    : P_CHECK_DEPT_PRIORIDAD
  FECHA     : 20/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida la fecha de hoy este dentro de las fechas para generar una solicitud, 
              incluyendo el departamento que le corresponde.
  =================================================================================================================== */
 IS
      V_INDICADOR               NUMBER := -1;
      V_TERMADMIN               NUMBER := -1;
      V_RETURN                  NUMBER := -1;
      V_CODE_DEPT               STVDEPT.STVDEPT_CODE%TYPE;
      V_SVRRSRV_SEQ_NO          SVRRSRV.SVRRSRV_SEQ_NO%TYPE;
      V_SVRRSRV_WEB_IND         SVRRSRV.SVRRSRV_WEB_IND%TYPE;
      
      -- GET DEPT en caso no se envie entre los parametros
      CURSOR C_SORLCUR IS
      SELECT    SORLFOS_DEPT_CODE  
      FROM (
              SELECT    SORLCUR_SEQNO,      SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
              ORDER BY SORLCUR_TERM_CODE DESC,SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM <= 1;

      -- GET prioridad
      CURSOR C_SVRRSRV IS
      SELECT SVRRSRV_SEQ_NO, SVRRSRV_WEB_IND FROM SVRRSRV 
      WHERE SVRRSRV_SRVC_CODE = P_SRVC_CODE
      AND ( 
            TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE(TO_CHAR(SVRRSRV_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')
            AND
            TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE(TO_CHAR(SVRRSRV_END_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')
          )
      AND SVRRSRV_SEQ_NO IN ( (0 + V_INDICADOR) , (1 + V_INDICADOR), (2 + V_INDICADOR) )
      ORDER BY SVRRSRV_SEQ_NO ASC; 
      
BEGIN  
--    
      P_PRIORIDAD := 0;
      V_CODE_DEPT := P_CODE_DEPT; 
      
      -- en caso se envie DEPT = null o vacio
      IF NVL(TRIM(V_CODE_DEPT),'TRUE') = 'TRUE'  THEN
      
              -- GET datos de alumno 
              OPEN C_SORLCUR;
              LOOP
                FETCH C_SORLCUR INTO V_CODE_DEPT;
                EXIT WHEN C_SORLCUR%NOTFOUND;
                END LOOP;
              CLOSE C_SORLCUR;
              
      END IF;
      
      SELECT CASE V_CODE_DEPT 
                  WHEN 'UREG' THEN 1
                  WHEN 'UPGT' THEN 4
                  WHEN 'UVIR' THEN 7
                  ELSE -99 END INTO V_INDICADOR
      FROM DUAL;
      
      -- GET COINCIDENCIAS A LOS FILTROS 
      -- UREG 1,2,3
      -- UPGT 4,5,6
      -- UVIR 7,8,9    
      -- GET DEPARTAMENTO, SEDE
      OPEN C_SVRRSRV;
      LOOP
        FETCH C_SVRRSRV INTO V_SVRRSRV_SEQ_NO, V_SVRRSRV_WEB_IND;
        IF C_SVRRSRV%FOUND THEN
            P_PRIORIDAD := CASE WHEN V_SVRRSRV_WEB_IND = 'N' THEN 0 ELSE V_SVRRSRV_SEQ_NO END;
        ELSE EXIT;
      END IF;
      END LOOP;
      CLOSE C_SVRRSRV;
      
--
END P_CHECK_DEPT_PRIORIDAD;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GETDATA_ALUMN (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_CODE_DEPT       OUT STVDEPT.STVDEPT_CODE%TYPE, 
    P_CODE_TERM       OUT STVTERM.STVTERM_CODE%TYPE,
    P_CODE_CAMP       OUT STVCAMP.STVCAMP_CODE%TYPE
) 
/* ===================================================================================================================
  NOMBRE    : P_GETDATA_ALUMN
  FECHA     : 14/03/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Retorn el periodo, Departamento y la sede del alumno

  NRO     FECHA         USUARIO       MODIFICACION
  =================================================================================================================== */
 AS
      P_INDICADOR               NUMBER;
      P_PART_PERIODO            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SEQNO                   SORLCUR.SORLCUR_SEQNO%TYPE;
      
      -- GET DEPARTAMENTO
      CURSOR C_CAMPDEPT IS
      SELECT    SORLCUR_CAMP_CODE,
                SORLFOS_DEPT_CODE                
      FROM (
              SELECT    SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
              ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM = 1;
      
      -- Calculando parte PERIODO (sub PTRM)
      CURSOR C_SFRRSTS_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = P_CODE_DEPT AND STVCAMP_CODE = P_CODE_CAMP;    
      
BEGIN  
--    
      P_CODE_TERM   := NULL;
      P_CODE_DEPT   := NULL;
      P_CODE_CAMP   := NULL;
      
      -- GET DEPARTAMENTO, SEDE
      OPEN C_CAMPDEPT;
      LOOP
        FETCH C_CAMPDEPT INTO P_CODE_CAMP,P_CODE_DEPT;
        EXIT WHEN C_CAMPDEPT%NOTFOUND;
      END LOOP;
      CLOSE C_CAMPDEPT;
      
      -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
      IF (P_CODE_DEPT IS NULL OR P_CODE_CAMP IS NULL) THEN
          P_CODE_TERM := NULL;
      ELSE
            -----------------------------------------------------------
            -- >> calculando SUB PARTE PERIODO  --
            OPEN C_SFRRSTS_PTRM;
            LOOP
              FETCH C_SFRRSTS_PTRM INTO P_PART_PERIODO;
              EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
            END LOOP;
            CLOSE C_SFRRSTS_PTRM;
            
            -- GET PERIODO ACTIVO --- FORMA: SFARSTS
            SELECT COUNT(SFRRSTS_TERM_CODE) INTO P_INDICADOR FROM SFRRSTS WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  ) 
            AND SFRRSTS_RSTS_CODE = 'RW' AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SFRRSTS_END_DATE;
            IF P_INDICADOR = 0 THEN
                  P_CODE_TERM := NULL;
            ELSE
                  SELECT SFRRSTS_TERM_CODE INTO  P_CODE_TERM FROM (
                      -- Forma SFARSTS  ---> fechas para las partes de periodo
                      SELECT DISTINCT SFRRSTS_TERM_CODE 
                      FROM SFRRSTS
                      WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  )
                      AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                      AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SFRRSTS_END_DATE
                      ORDER BY SFRRSTS_TERM_CODE DESC
                  ) WHERE ROWNUM <= 1;                    
            END IF;
      END IF;
      
--
END P_GETDATA_ALUMN;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_DATOS_INGRESO (
    P_PIDM           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_CAMP           IN STVCAMP.STVCAMP_CODE%type,
    P_TERM           IN STVTERM.STVTERM_CODE%TYPE,
    P_DEPT           IN STVDEPT.STVDEPT_CODE%type,
    P_INGRESANTE     OUT BOOLEAN,
    P_FECHA          OUT DATE
)
/* ===================================================================================================================
  NOMBRE    : F_INGRESANTE_TERM
  FECHA     : 03/04/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida si el alumno es ingresante para un periodo,sede,dept especifico.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
       
      P_APEC_CAMP      VARCHAR2(9);
      P_APEC_DEPT      VARCHAR2(9);
      P_APEC_TERM      VARCHAR2(10);
      
      V_C              INTEGER;
      V_ROW1           INTEGER;
      V_OUT            VARCHAR2(15);
      V_QUERY_INGRE    VARCHAR2(4000);
      V_CONTADOR       INTEGER := 0;
      V_FECHA          VARCHAR2(150);
BEGIN
--
      P_INGRESANTE := FALSE;
      SELECT CZRCAMP_CAMP_BDUCCI INTO P_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP;-- CAMPUS 
      SELECT CZRTERM_TERM_BDUCCI INTO P_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM;-- PERIODO
      SELECT CZRDEPT_DEPT_BDUCCI INTO P_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = P_DEPT;-- DEPARTAMENTO

      V_QUERY_INGRE := '
      --CALCULAR A LOS MATRICULADOS BECA 18
      SELECT IDPersonaN, convert(NVARCHAR, max(IDExamen), 103) IDExamen
      FROM tblPersona PP
      INNER JOIN tblPersonaAlumno  PA
      ON PP.IDPersona = PA.IDPersona
      INNER JOIN (
          select distinct p.idalumno,p.IDExamen
          from tblalumnoestado a 
          INNER JOIN tblpostulante p
            ON a.IDDependencia = P.IDDependencia
            and a.IDPerAcad = P.IDPerAcad
            and a.IDAlumno = P.IDAlumno
            and a.FecInic = p.IDExamen
            and a.idescuela = p.idescuelaadm
          INNER JOIN tblEscuela 
            ON tblEscuela.IDEscuela = P.IDEscuela
                  and tblEscuela.IDDependencia =p .IDDependencia
          inner join tblModalidadPostu mpost 
            on mpost.IDModalidadPostu = P.IDModalidadPostu
                  and mpost.IDPerAcad = P.IDPerAcad
                  and mpost.IDDependencia = P.IDDependencia
                  and mpost.IDEscuelaADM = A.IDEscuela
          where p.iddependencia = ''UCCI''
          and p.idperacad = ''' || P_APEC_TERM || '''
          and a.idescuela = ''' || P_APEC_DEPT || '''
          and p.ingresante = ''1''
          and a.idsede = ''' || P_APEC_CAMP || '''
          and Renuncia=''0''
          and tblEscuela.Activo=''1''
          and p.IDEscuela not in (''506'',''510'',''601'')
      ) TM
      ON  PA.IDAlumno = TM.IDAlumno 
      AND IDPersonaN = ' || TO_CHAR(P_PIDM) || ' 
      GROUP BY IDPersonaN ';
     V_C := DBMS_HS_PASSTHROUGH.OPEN_CURSOR@BDUCCI.CONTINENTAL.EDU.PE;
     DBMS_HS_PASSTHROUGH.PARSE@BDUCCI.CONTINENTAL.EDU.PE(V_C,V_QUERY_INGRE);
     LOOP
          V_ROW1 := DBMS_HS_PASSTHROUGH.FETCH_ROW@BDUCCI.CONTINENTAL.EDU.PE(V_C);
          EXIT WHEN V_ROW1 = 0;
          DBMS_HS_PASSTHROUGH.GET_VALUE@BDUCCI.CONTINENTAL.EDU.PE (V_C,2, V_OUT);
          P_INGRESANTE := TRUE;    
          V_FECHA      := V_OUT;--TO_DATE(V_OUT, 'dd/mm/yyyy');
          P_FECHA      := TO_DATE(V_FECHA,'dd/mm/yyyy');
     END LOOP;
     DBMS_HS_PASSTHROUGH.CLOSE_CURSOR@BDUCCI.CONTINENTAL.EDU.PE(V_C);

END P_DATOS_INGRESO;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_CREDITOS_CICLO (
      P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_CREDITOS        OUT INTEGER,
      P_CICLO           OUT INTEGER
)
/* ===================================================================================================================
  NOMBRE    : P_CREDITOS_CICLO
  FECHA     : 08/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Calcula  los creditos y ciclo del estudiante.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
    
    V_CRED_OBLIG          INTEGER := 0;  
    V_CRED_TOTAL          INTEGER := 0;
    V_CICLO_ACTL          INTEGER := 0;

    -- GET CREDITOS ACUMULADOS Y CICLO DEL ESTUDIANTE
    CURSOR C_CRED_CICLO IS
    SELECT UNIQUE
             NVL(CREDITOS_OBLIGATORIO,'0') AS CREDITOS_OBLIGATORIO,
             NVL(CREDITOS_ACTUAL,'0') AS CREDITOS_TOTAL,
    
             CASE WHEN CREDITOS_ACTUAL IS NULL THEN '1'
             ELSE
             NVL(CASE   WHEN CREDITOS_ACTUAL <= 22 AND EFF = '201510' THEN '1'
                        WHEN CREDITOS_ACTUAL > 22 AND CREDITOS_ACTUAL <= 44 AND EFF = '201510' THEN '2'
                        WHEN CREDITOS_ACTUAL > 44 AND CREDITOS_ACTUAL <= 66 AND EFF = '201510' THEN '3'
                        WHEN CREDITOS_ACTUAL > 66 AND CREDITOS_ACTUAL <= 88 AND EFF = '201510' THEN '4'
                        WHEN CREDITOS_ACTUAL > 88 AND CREDITOS_ACTUAL <= 110 AND EFF = '201510' THEN '5'
              
                        WHEN CREDITOS_ACTUAL > 110 AND CREDITOS_ACTUAL <= 135 AND EFF = '201510' THEN '6'
                        WHEN CREDITOS_ACTUAL > 135 AND CREDITOS_ACTUAL <= 160 AND EFF = '201510' THEN '7'
              
                        WHEN CREDITOS_ACTUAL > 160 AND CREDITOS_ACTUAL <= 182 AND EFF = '201510' THEN '8'
                        WHEN CREDITOS_ACTUAL > 182 AND CREDITOS_ACTUAL <= 204 AND EFF = '201510' THEN '9'
                        WHEN CREDITOS_ACTUAL > 204 AND CREDITOS_ACTUAL <= 226 AND EFF = '201510' THEN '10'
                        WHEN CREDITOS_ACTUAL > 226 AND CREDITOS_ACTUAL <= 248 AND EFF = '201510' AND PROGRAMA IN ('312','502') THEN '11'
                        WHEN CREDITOS_ACTUAL > 248 AND CREDITOS_ACTUAL <= 270 AND EFF = '201510' AND PROGRAMA IN ('312','502') THEN '12'
                        WHEN CREDITOS_ACTUAL > 270 AND CREDITOS_ACTUAL <= 314 AND EFF = '201510' AND PROGRAMA IN ('502') THEN '13'
                        WHEN CREDITOS_ACTUAL > 314 AND PROGRAMA IN ('502') THEN '14'
                        ELSE REPLACE(NVL(CASE   WHEN CREDITOS_ACTUAL <= 22 AND EFF = '200710' THEN '1'
                                                WHEN CREDITOS_ACTUAL > 22 AND CREDITOS_ACTUAL <= 44 AND EFF = '200710' THEN '2'
                                                WHEN CREDITOS_ACTUAL > 44 AND CREDITOS_ACTUAL <= 66 AND EFF = '200710' THEN '3'
                                                WHEN CREDITOS_ACTUAL > 66 AND CREDITOS_ACTUAL <= 88 AND EFF = '200710' THEN '4'
                                                WHEN CREDITOS_ACTUAL > 88 AND CREDITOS_ACTUAL <= 110 AND EFF = '200710' THEN '5'
                                                
                                                WHEN CREDITOS_ACTUAL > 110 AND CREDITOS_ACTUAL <= 132 AND EFF = '200710' THEN '6'
                                                WHEN CREDITOS_ACTUAL > 132 AND CREDITOS_ACTUAL <= 154 AND EFF = '200710' THEN '7'
                                                
                                                WHEN CREDITOS_ACTUAL > 154 AND CREDITOS_ACTUAL <= 176 AND EFF = '200710' THEN '8'
                                                WHEN CREDITOS_ACTUAL > 176 AND CREDITOS_ACTUAL <= 198 AND EFF = '200710' THEN '9'
                                                WHEN CREDITOS_ACTUAL > 198 AND CREDITOS_ACTUAL <= 220 AND EFF = '200710' THEN '10'
                                                WHEN CREDITOS_ACTUAL > 220 AND CREDITOS_ACTUAL <= 242 AND EFF = '200710' AND PROGRAMA IN ('312','502') THEN '11'
                                                WHEN CREDITOS_ACTUAL > 242 AND CREDITOS_ACTUAL <= 264 AND EFF = '200710' AND PROGRAMA IN ('312','502') THEN '12'
                                                WHEN CREDITOS_ACTUAL > 264 AND CREDITOS_ACTUAL <= 286 AND EFF = '200710' AND PROGRAMA IN ('502') THEN '13'
                                                WHEN CREDITOS_ACTUAL > 286 AND PROGRAMA IN ('502') THEN '14'
                                                ELSE REPLACE(NVL(CASE   WHEN CREDITOS_ACTUAL <= 22 AND EFF = '201710' THEN '1'
                                                                        WHEN CREDITOS_ACTUAL > 22 AND CREDITOS_ACTUAL <= 44 AND EFF = '201710' THEN '2'
                                                                        WHEN CREDITOS_ACTUAL > 44 AND CREDITOS_ACTUAL <= 66 AND EFF = '201710' THEN '3'
                                                                        WHEN CREDITOS_ACTUAL > 66 AND CREDITOS_ACTUAL <= 88 AND EFF = '201710' THEN '4'
                                                                        WHEN CREDITOS_ACTUAL > 88 AND CREDITOS_ACTUAL <= 110 AND EFF = '201710' THEN '5'
                                                                        
                                                                        WHEN CREDITOS_ACTUAL > 110 AND CREDITOS_ACTUAL <= 135 AND EFF = '201710' THEN '6'
                                                                        WHEN CREDITOS_ACTUAL > 135 AND CREDITOS_ACTUAL <= 160 AND EFF = '201710' THEN '7'
                                                                        
                                                                        WHEN CREDITOS_ACTUAL > 160 AND CREDITOS_ACTUAL <= 182 AND EFF = '201710' THEN '8'
                                                                        WHEN CREDITOS_ACTUAL > 182 AND CREDITOS_ACTUAL <= 204 AND EFF = '201710' THEN '9'
                                                                        WHEN CREDITOS_ACTUAL > 204 AND CREDITOS_ACTUAL <= 226 AND EFF = '201710' THEN '10'
                                                                        WHEN CREDITOS_ACTUAL > 226 AND CREDITOS_ACTUAL <= 248 AND EFF = '201710' AND PROGRAMA IN ('312','502') THEN '11'
                                                                        WHEN CREDITOS_ACTUAL > 248 AND CREDITOS_ACTUAL <= 270 AND EFF = '201710' AND PROGRAMA IN ('312','502') THEN '12'
                                                                        WHEN CREDITOS_ACTUAL > 270 AND CREDITOS_ACTUAL <= 314 AND EFF = '201710' AND PROGRAMA IN ('502') THEN '13'
                                                                        WHEN CREDITOS_ACTUAL > 314 AND PROGRAMA IN ('502') THEN '14' END
                                                ,''),'','')
                        END,''),'','')
             END,'10') END AS CICLO_ACTUAL
    FROM (
    /*------------------------------- INIT WITH --------------------------------*/
        WITH T1 AS (
                    SELECT    SORLCUR_PIDM        CODIGO,        
                              SORLCUR_TERM_CODE   PERIODO,
                              SORLCUR_PROGRAM     PROGRAMA,
                              SORLCUR_LEVL_CODE   NIVEL,
                              SORLCUR_CAMP_CODE   CAMPUS,  
                              SORLFOS_DEPT_CODE   DEPARTAMENTO
                    FROM (
                            SELECT    SORLCUR_PIDM,         SORLCUR_TERM_CODE,    SORLCUR_PROGRAM,  
                                      SORLCUR_LEVL_CODE,    SORLCUR_CAMP_CODE,    SORLFOS_DEPT_CODE
                            FROM SORLCUR        INNER JOIN SORLFOS
                                  ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                                  AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
                            WHERE   SORLCUR_PIDM        =   P_PIDM
                                AND SORLCUR_LMOD_CODE   =   'LEARNER'
                                AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                                AND SORLCUR_CURRENT_CDE =   'Y'
                            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
                    ) WHERE ROWNUM <= 1
        ), T2 AS (
                SELECT UNIQUE SMBPOGN_PIDM,
                     SMBPOGN_REQUEST_NO,
                     SMRDOUS_CRSE_SOURCE,
                     SMBPOGN_TERM_CODE_EFF,
                     CASE
                       WHEN SMBPOGN_TERM_CODE_EFF >= '200710' AND SMBPOGN_TERM_CODE_EFF < '201510' THEN '200710'
                       WHEN SMBPOGN_TERM_CODE_EFF >= '201510' AND SMBPOGN_TERM_CODE_EFF < '201710' THEN '201510'
                       --WHEN SMBPOGN_TERM_CODE_EFF >= '201710' AND SMBPOGN_TERM_CODE_EFF < '' THEN ''
                       ELSE REPLACE(NVL(SMBPOGN_TERM_CODE_EFF,''),'','')
                     END EFF,
                     SMBPOGN_REQ_CREDITS_OVERALL,
                     SMBPOGN_ACT_CREDITS_OVERALL,
                     SMBPOGN_CAMP_CODE
                FROM SMBPOGN LEFT JOIN SMRDOUS
                ON SMBPOGN_PIDM = SMRDOUS_PIDM
                AND SMBPOGN_REQUEST_NO = SMRDOUS_REQUEST_NO
                INNER JOIN (  SELECT SMBPOGN_PIDM AS PIDPOGN,
                              MAX(SMBPOGN_REQUEST_NO) AS RQPOGN
                              FROM SMBPOGN
                              GROUP BY SMBPOGN_PIDM) 
                ON SMBPOGN_PIDM=PIDPOGN
                AND SMBPOGN_REQUEST_NO=RQPOGN
                WHERE SMBPOGN_PIDM = P_PIDM
        ),T3 AS (
                SELECT  SMBPOGN_PIDM,
                        SMRDRRQ_REQUEST_NO,
                        
                        --ORIGINAL
                        SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) AS CREDITOS_ELECTIVO,
                        --SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) AS CREDITOS_ELECTIVO,
                        
                        --ORIGINAL
                        SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,SMRDOUS_CREDIT_HOURS)) -
                        --SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) -
                        SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) -
                        SUM(CASE WHEN SUBSTR(SMRDOUS_AREA,4,1) = 'D' THEN SMRDOUS_CREDIT_HOURS ELSE 0 END) AS CREDITOS_OBLIGATORIO,
                        
                        SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) AS DIPLOMADO,
                        
                        CASE
                             WHEN SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) > SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0))
                                  THEN SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) + (SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,SMRDOUS_CREDIT_HOURS)) - SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) - SUM(CASE WHEN SUBSTR(SMRDOUS_AREA,4,1) = 'D' THEN SMRDOUS_CREDIT_HOURS ELSE 0 END))
                             WHEN SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) > SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS))
                                  THEN SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) + (SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,SMRDOUS_CREDIT_HOURS)) - SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) - SUM(CASE WHEN SUBSTR(SMRDOUS_AREA,4,1) = 'D' THEN SMRDOUS_CREDIT_HOURS ELSE 0 END))
                             WHEN SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) = SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0))
                                  THEN SUM(DECODE(SUBSTR(SMRDOUS_AREA,4,1),'D',SMRDOUS_CREDIT_HOURS)) + (SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,SMRDOUS_CREDIT_HOURS)) - SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) - SUM(CASE WHEN SUBSTR(SMRDOUS_AREA,4,1) = 'D' THEN SMRDOUS_CREDIT_HOURS ELSE 0 END))
                             ELSE SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) +
                                  (
                                   SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,SMRDOUS_CREDIT_HOURS)) -
                                   SUM(DECODE(SUBSTR(SMRDOUS_KEY_RULE,1,4),'ELEC',SMRDOUS_CREDIT_HOURS,0)) -
                                   SUM(CASE WHEN SUBSTR(SMRDOUS_AREA,4,1) = 'D' THEN SMRDOUS_CREDIT_HOURS ELSE 0 END)
                                  ) END AS CREDITOS_ACTUAL
                FROM SMRDOUS
                LEFT JOIN SMRDRRQ
                    ON SMRDRRQ_PIDM = SMRDOUS_PIDM
                    AND SMRDRRQ_REQUEST_NO = SMRDOUS_REQUEST_NO
                    AND SMRDOUS_AREA = SMRDRRQ_AREA
                    AND SMRDOUS_CAA_SEQNO = SMRDRRQ_CAA_SEQNO
                    AND SMRDOUS_KEY_RULE = SMRDRRQ_KEY_RULE
                    AND SMRDOUS_TERM_CODE_EFF = SMRDRRQ_TERM_CODE_EFF
                    AND SMRDOUS_RUL_SEQNO = SMRDRRQ_RUL_SEQNO
                LEFT JOIN SMBPOGN 
                    ON SMBPOGN_PIDM = SMRDOUS_PIDM
                    AND SMBPOGN_REQUEST_NO = SMRDOUS_REQUEST_NO
                WHERE SMRDOUS_CRSE_SOURCE IN ('H','T')
                AND SMRDOUS_PIDM = P_PIDM
                GROUP BY SMBPOGN_PIDM,SMRDRRQ_REQUEST_NO
        )
        SELECT DISTINCT * FROM T1
        LEFT JOIN T2 ON T2.SMBPOGN_PIDM = T1.CODIGO
        LEFT JOIN T3 ON T2.SMBPOGN_PIDM = T3.SMBPOGN_PIDM
                     AND T2.SMBPOGN_REQUEST_NO = T3.SMRDRRQ_REQUEST_NO
    /*------------------------------- FIN WITH ---------------------------------*/
    );

BEGIN
    
    OPEN C_CRED_CICLO;
    LOOP
      FETCH C_CRED_CICLO INTO V_CRED_OBLIG, V_CRED_TOTAL, V_CICLO_ACTL;
      EXIT WHEN C_CRED_CICLO%NOTFOUND;
    END LOOP;
    CLOSE C_CRED_CICLO;

    P_CREDITOS     := V_CRED_TOTAL;
    P_CICLO        := V_CICLO_ACTL;

END P_CREDITOS_CICLO;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


END WFK_OAFORM;