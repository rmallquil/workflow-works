CREATE OR REPLACE PACKAGE BODY WFK_OAFORM AS
/*******************************************************************************
 WFK_OAFORM:
       Conti Package OPCIONES ADICIONALES FORMA
*******************************************************************************/
-- FILE NAME..: WFK_OAFORM.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_OAFORM
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -  
  DESCRIPTION END 
*******************************************************************************/


FUNCTION F_RSS_SRIR (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_SRIR
              "Regla Solicitud de Servicio de SOLICITUD RECTIFICACIÓN DE INSCRIPCIÓN REZAGADOS"
  FECHA     : 20/02/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 

  NRO     FECHA         USUARIO       MODIFICACION
  001     09/02/2017    RMALLQUI      Agrego validacion solo para amtrioculados (CON NRC)   
  =================================================================================================================== */
 IS
      P_CODIGO_SOL              SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL003';
      P_RESP                    NUMBER;
      P_INDICADOR               NUMBER;
      P_PERIODO                 SFBETRM.SFBETRM_TERM_CODE%TYPE;
      P_MODALIDAD_ALUMNO        SGBSTDN.SGBSTDN_DEPT_CODE%TYPE; -- Departamento
      P_COD_SEDE                STVCAMP.STVCAMP_CODE%TYPE;  -- SGBSTDN.SGBSTDN_CAMP_CODE%TYPE;
      P_PART_PERIODO            SOBPTRM.SOBPTRM_PTRM_CODE%TYPE; --------------- Parte-de-Periodo
BEGIN  
--    
    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD (P_PIDM, P_CODIGO_SOL, P_RESP);

    -- VERIFICAR QUE YA ESTE MATRICULADO
    -- >> GET DEPARTAMENTO y CAMPUS --
      SELECT COUNT(*) INTO P_INDICADOR FROM SORLCUR INNER JOIN SORLFOS
      ON SORLCUR_PIDM = SORLFOS_PIDM AND SORLCUR_SEQNO = SORLFOS.SORLFOS_LCUR_SEQNO
      WHERE SORLCUR_PIDM = P_PIDM AND SORLCUR_LMOD_CODE = 'LEARNER' 
      AND SORLCUR_CACT_CODE   = 'ACTIVE' AND SORLCUR_CURRENT_CDE = 'Y';
      
      IF P_INDICADOR = 0 THEN
            P_MODALIDAD_ALUMNO    := '-';
            P_COD_SEDE            := '-';
      ELSE  
            -- GET DEPARTAMENTO y CAMPUS mas reciente del alumno 
            SELECT SORLFOS_DEPT_CODE , SORLCUR_CAMP_CODE INTO P_MODALIDAD_ALUMNO, P_COD_SEDE 
            FROM(
                  SELECT    SORLCUR_SEQNO,      
                            SORLCUR_CAMP_CODE,    
                            SORLFOS_DEPT_CODE
                  FROM SORLCUR        
                  INNER JOIN SORLFOS
                      ON SORLCUR_PIDM         = SORLFOS_PIDM 
                      AND SORLCUR_SEQNO       = SORLFOS.SORLFOS_LCUR_SEQNO
                  WHERE SORLCUR_PIDM          = P_PIDM 
                      AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                      AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                      AND SORLCUR_CURRENT_CDE = 'Y'
                  ORDER BY SORLCUR_SEQNO DESC 
            ) WHERE ROWNUM <= 1;
      END IF;
      
      -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
      IF (P_MODALIDAD_ALUMNO = '-' OR P_COD_SEDE = '-') THEN
          P_PERIODO := '-';
      ELSE
            -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
            SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                      WHEN 'UPGT' THEN 'W' 
                                      WHEN 'UREG' THEN 'R' 
                                      WHEN 'UPOS' THEN '-' 
                                      WHEN 'ITEC' THEN '-' 
                                      WHEN 'UCIC' THEN '-' 
                                      WHEN 'UCEC' THEN '-' 
                                      WHEN 'ICEC' THEN '-' 
                                      ELSE '1' END ||
                    CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                      WHEN 'F01' THEN 'A' 
                                      WHEN 'F02' THEN 'L' 
                                      WHEN 'F03' THEN 'C' 
                                      WHEN 'V00' THEN 'V' 
                                      ELSE '9' END
            INTO P_PART_PERIODO
            FROM STVCAMP,STVDEPT 
            WHERE STVDEPT_CODE = P_MODALIDAD_ALUMNO AND STVCAMP_CODE = P_COD_SEDE;
            
            -- GET PERIODO ACTIVO 
            SELECT COUNT(SFRRSTS_TERM_CODE) INTO P_INDICADOR FROM SFRRSTS WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  ) 
            AND SFRRSTS_RSTS_CODE = 'RW' AND SYSDATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE;
            IF P_INDICADOR = 0 THEN
                  P_PERIODO := '-';
            ELSE
                  SELECT SFRRSTS_TERM_CODE INTO  P_PERIODO FROM (
                      -- Forma SFARSTS  ---> fechas para las partes de periodo
                      SELECT DISTINCT SFRRSTS_TERM_CODE 
                      FROM SFRRSTS
                      WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  )
                      AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                      AND SYSDATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE
                      ORDER BY SFRRSTS_TERM_CODE DESC
                  ) WHERE ROWNUM <= 1;                    
            END IF;
      END IF;
    ---A VERIFICAR QUE TENGA NRC's
      SELECT COUNT(*) INTO P_INDICADOR FROM SFRSTCR 
      WHERE SFRSTCR_PIDM = P_PIDM
      AND SFRSTCR_TERM_CODE = P_PERIODO; 
    
    IF P_RESP > 0 AND P_INDICADOR > 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
--
END F_RSS_SRIR;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_RSS_SRM (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_SRM
              "Regla Solicitud de Servicio de SOLICITUD RESERVA DE MATRICULA"
  FECHA     : 20/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 
  =================================================================================================================== */
 IS
      P_CODIGO_SOL             SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL005';
      P_RESP                   NUMBER;
      
BEGIN  
--    
    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD (P_PIDM, P_CODIGO_SOL, P_RESP);
    
    IF P_RESP > 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
--
END F_RSS_SRM;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_RSS_CPE (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_CPE
              "Regla Solicitud de Servicio de CAMBIO de PLAN ESTUDIO"
  FECHA     : 07/12/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 
  =================================================================================================================== */
 IS
      P_PERIODOCTLG            SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';
      P_PERIODOCTLG_MAX        SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';
      P_CODIGO_SOL             SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL004';
      P_RESP                   NUMBER;
      
      -- OBTENER PERIODO DE CATALOGO
      CURSOR C_SORLCUR IS
      SELECT SORLCUR_TERM_CODE_CTLG
      FROM(
        SELECT SORLCUR_TERM_CODE_CTLG 
        FROM SORLCUR 
        WHERE SORLCUR_PIDM      =  P_PIDM
        AND SORLCUR_CACT_CODE   = 'ACTIVE' 
        AND SORLCUR_LMOD_CODE   = 'LEARNER'
        AND SORLCUR_CURRENT_CDE = 'Y'
        ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM = 1;

BEGIN  
--    
    OPEN C_SORLCUR;
    LOOP
      FETCH C_SORLCUR INTO P_PERIODOCTLG;
      EXIT WHEN C_SORLCUR%NOTFOUND;
      END LOOP;
    CLOSE C_SORLCUR;
    
    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD (P_PIDM, P_CODIGO_SOL, P_RESP);
    
    IF(P_PERIODOCTLG < P_PERIODOCTLG_MAX) AND P_RESP > 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
--
END F_RSS_CPE;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_RSS_SRD (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_SRD
              "Regla Solicitud de Servicio de SOLICITUD DE SOBREPASO DE RETENCION DE DOCUMENTOS"
  FECHA     : 20/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 
  =================================================================================================================== */
 IS
      P_CODIGO_SOL             SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL006';
      P_RESP                   NUMBER;
      
BEGIN  
--    
    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD (P_PIDM, P_CODIGO_SOL, P_RESP);
    
    IF P_RESP > 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
--
END F_RSS_SRD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_RSS_SMCI (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_SMCI
              "Regla Solicitud de Servicio 
              de MODIFICACIÒN de la CUOTA INICIAL (MATRICULA ESPECIAL)  SOL007"
  FECHA     : 19/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : en base a una sede y un rol, el procedimiento obtenga los datos del responsable 
              de Registro acadèmico de esa sede.

  =================================================================================================================== */
 IS
      P_SEQNO                   SORLCUR.SORLCUR_SEQNO%TYPE := 0;
      P_DEPARTAMENTO            SORLFOS.SORLFOS_DEPT_CODE%TYPE := '-';
      P_DEPARTAMENTO_VALIDO     SORLFOS.SORLFOS_DEPT_CODE%TYPE := 'UREG';
      P_CODIGO_SOL              SVRRSRV.SVRRSRV_SRVC_CODE%TYPE := 'SOL007';
      P_RESP                    NUMBER := 0;
      
      -- GET datos de alumno 
      CURSOR C_SORLCUR IS
      SELECT    SORLCUR_SEQNO, SORLFOS_DEPT_CODE  
      FROM (
              SELECT    SORLCUR_SEQNO,      SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  -- AND SORLCUR_TERM_CODE   =   P_PERIODO 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
              ORDER BY SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM <= 1;
      
BEGIN  
--
    -- #######################################################################
    -- GET datos de alumno 
    OPEN C_SORLCUR;
    LOOP
      FETCH C_SORLCUR INTO P_SEQNO, P_DEPARTAMENTO;
      EXIT WHEN C_SORLCUR%NOTFOUND;
      END LOOP;
    CLOSE C_SORLCUR;
    
    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD (P_PIDM, P_CODIGO_SOL, P_RESP);
    
    IF P_DEPARTAMENTO = P_DEPARTAMENTO_VALIDO AND P_RESP > 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
        
END F_RSS_SMCI;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_RSS_SR (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_SR
              "Regla Solicitud de Servicio de SOLICITUD DE REINCORPORACIÒN"
  FECHA     : 20/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 
  =================================================================================================================== */
 IS
      P_CODIGO_SOL             SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL009';
      P_RESP                   NUMBER;
      
BEGIN  
--    
    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD (P_PIDM, P_CODIGO_SOL, P_RESP);
    
    IF P_RESP > 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
--
END F_RSS_SR;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

FUNCTION F_RSS_SESM (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_SESM
              "Regla Solicitud de Servicio de SOLICITUD DE EXONERACIÓN DE SEGURO DE SALUD"
  FECHA     : 20/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 
  =================================================================================================================== */
 IS
      P_CODIGO_SOL             SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL012';
      P_RESP                   NUMBER;
      
BEGIN  
--    
    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD (P_PIDM, P_CODIGO_SOL, P_RESP);
    
    IF P_RESP > 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
--
END F_RSS_SESM;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CHECK_DEPT_PRIORIDAD(
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       IN SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_RESP            OUT NUMBER
) 
/* ===================================================================================================================
  NOMBRE    : 
  FECHA     : 20/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida la fecha de hoy este dentro de las fechas para generar una solicitud, 
              incluyendo el departamento que le corresponde.
  =================================================================================================================== */
 IS
      V_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%TYPE;
      V_SEQNO                   SORLCUR.SORLCUR_SEQNO%TYPE;
      V_INDICADOR               NUMBER := -1;
      V_RETURN                  NUMBER := -1;
      
      -- GET DEPARTAMENTO
      CURSOR C_SORLCUR_SORLFOS IS
      SELECT    SORLCUR_SEQNO,       
                SORLFOS_DEPT_CODE   
      FROM (
              SELECT    SORLCUR_SEQNO, SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
              ORDER BY SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM <= 1;

BEGIN  
--    
      -- GET DEPARTAMENTO
      OPEN C_SORLCUR_SORLFOS;
      LOOP
        FETCH C_SORLCUR_SORLFOS INTO V_SEQNO, V_ALUM_DEPARTAMENTO;
        EXIT WHEN C_SORLCUR_SORLFOS%NOTFOUND;
      END LOOP;
      CLOSE C_SORLCUR_SORLFOS;
      
      SELECT CASE V_ALUM_DEPARTAMENTO 
                  WHEN 'UREG' THEN 1
                  WHEN 'UPGT' THEN 2
                  WHEN 'UVIR' THEN 3
                  ELSE -1 END INTO V_INDICADOR
      FROM DUAL;

      -- GET COINCIDENCIAS A LOS FILTROS 
      -- UREG 1,2
      -- UPGT 3,4
      -- UVIR 5,6      
      SELECT COUNT(*) INTO P_RESP
      FROM SVRRSRV 
      WHERE SVRRSRV_SRVC_CODE = P_SRVC_CODE
      AND ( 
            TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE(TO_CHAR(SVRRSRV_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')
            AND
            TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE(TO_CHAR(SVRRSRV_END_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')
          )
      AND SVRRSRV_SEQ_NO IN ( (0 + V_INDICADOR) , (1 + V_INDICADOR) ); 

--
END P_CHECK_DEPT_PRIORIDAD;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


END WFK_OAFORM;