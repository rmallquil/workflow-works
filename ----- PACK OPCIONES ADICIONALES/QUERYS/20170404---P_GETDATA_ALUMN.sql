
  /*
  SET SERVEROUTPUT ON
  DECLARE   
      P_CODE_DEPT       SGBSTDN.SGBSTDN_DEPT_CODE%TYPE;
      P_CODE_CAMP       STVCAMP.STVCAMP_CODE%TYPE;
      P_CODE_TERM       SFBETRM.SFBETRM_TERM_CODE%TYPE;
  BEGIN
      P_GETDATA_ALUMN(123544,P_CODE_DEPT,P_CODE_TERM,P_CODE_CAMP);
      DBMS_OUTPUT.PUT_LINE(P_CODE_DEPT || ' & ' || P_CODE_TERM || ' & ' || P_CODE_CAMP);
  END;
  */

  CREATE OR REPLACE PROCEDURE P_GETDATA_ALUMN (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_CODE_DEPT       OUT STVDEPT.STVDEPT_CODE%TYPE, 
    P_CODE_TERM       OUT STVTERM.STVTERM_CODE%TYPE,
    P_CODE_CAMP       OUT STVCAMP.STVCAMP_CODE%TYPE
) 
/* ===================================================================================================================
  NOMBRE    : P_GETDATA_ALUMN
  FECHA     : 14/03/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Retorn el periodo, Departamento y la sede del alumno

  NRO     FECHA         USUARIO       MODIFICACION
  =================================================================================================================== */
 AS
      P_INDICADOR               NUMBER;
      P_PART_PERIODO            SOBPTRM.SOBPTRM_PTRM_CODE%TYPE; --------------- Parte-de-Periodo
      V_SEQNO                   SORLCUR.SORLCUR_SEQNO%TYPE;
      
      -- GET DEPARTAMENTO
      CURSOR C_CAMPDEPT IS
      SELECT    SORLCUR_CAMP_CODE,
                SORLFOS_DEPT_CODE                
      FROM (
              SELECT    SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
              ORDER BY SORLCUR_TERM_CODE DESC 
      ) WHERE ROWNUM = 1;
      
BEGIN  
--    
      P_CODE_TERM   := NULL;
      P_CODE_DEPT   := NULL;
      P_CODE_CAMP   := NULL;
      
      -- GET DEPARTAMENTO, SEDE
      OPEN C_CAMPDEPT;
      LOOP
        FETCH C_CAMPDEPT INTO P_CODE_CAMP,P_CODE_DEPT;
        EXIT WHEN C_CAMPDEPT%NOTFOUND;
      END LOOP;
      CLOSE C_CAMPDEPT;
      
      -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
      IF (P_CODE_DEPT IS NULL OR P_CODE_CAMP IS NULL) THEN
          P_CODE_TERM := NULL;
      ELSE
            -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
            SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                      WHEN 'UPGT' THEN 'W' 
                                      WHEN 'UREG' THEN 'R' 
                                      WHEN 'UPOS' THEN '-' 
                                      WHEN 'ITEC' THEN '-' 
                                      WHEN 'UCIC' THEN '-' 
                                      WHEN 'UCEC' THEN '-' 
                                      WHEN 'ICEC' THEN '-' 
                                      ELSE '1' END ||
                    CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                      WHEN 'F01' THEN 'A' 
                                      WHEN 'F02' THEN 'L' 
                                      WHEN 'F03' THEN 'C' 
                                      WHEN 'V00' THEN 'V' 
                                      ELSE '9' END
            INTO P_PART_PERIODO
            FROM STVCAMP,STVDEPT 
            WHERE STVDEPT_CODE = P_CODE_DEPT AND STVCAMP_CODE = P_CODE_CAMP;
            
            -- GET PERIODO ACTIVO --- FORMA: SFARESTS
            SELECT COUNT(SFRRSTS_TERM_CODE) INTO P_INDICADOR FROM SFRRSTS WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  ) 
            AND SFRRSTS_RSTS_CODE = 'RW' AND SYSDATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE;
            IF P_INDICADOR = 0 THEN
                  P_CODE_TERM := NULL;
            ELSE
                  SELECT SFRRSTS_TERM_CODE INTO  P_CODE_TERM FROM (
                      -- Forma SFARSTS  ---> fechas para las partes de periodo
                      SELECT DISTINCT SFRRSTS_TERM_CODE 
                      FROM SFRRSTS
                      WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  )
                      AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                      AND SYSDATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE
                      ORDER BY SFRRSTS_TERM_CODE DESC
                  ) WHERE ROWNUM <= 1;                    
            END IF;
      END IF;
      
--
END P_GETDATA_ALUMN;