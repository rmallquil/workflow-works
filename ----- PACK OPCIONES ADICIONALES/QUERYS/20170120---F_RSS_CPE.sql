
create or replace FUNCTION F_RSS_CPE (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_CPE
              "Regla Solicitud de Servicio de CAMBIO de PLAN ESTUDIO"
  FECHA     : 07/12/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 
  =================================================================================================================== */
 IS
      P_PERIODOCTLG            SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';
      P_PERIODOCTLG_MAX        SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';
      P_CODIGO_SOL             SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL004';
      P_RESP                   NUMBER;
      
      -- OBTENER PERIODO DE CATALOGO
      CURSOR C_SORLCUR IS
      SELECT SORLCUR_TERM_CODE_CTLG
      FROM(
        SELECT SORLCUR_TERM_CODE_CTLG 
        FROM SORLCUR 
        WHERE SORLCUR_PIDM      =  P_PIDM
        AND SORLCUR_CACT_CODE   = 'ACTIVE' 
        AND SORLCUR_LMOD_CODE   = 'LEARNER'
        AND SORLCUR_CURRENT_CDE = 'Y'
        ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM = 1;

BEGIN  
--    
    OPEN C_SORLCUR;
    LOOP
      FETCH C_SORLCUR INTO P_PERIODOCTLG;
      EXIT WHEN C_SORLCUR%NOTFOUND;
      END LOOP;
    CLOSE C_SORLCUR;
    
    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD (P_PIDM, P_CODIGO_SOL, P_RESP);
    
    IF(P_PERIODOCTLG < P_PERIODOCTLG_MAX) AND P_RESP > 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
--
END F_RSS_CPE;