CREATE OR REPLACE PACKAGE WFK_OAFORM AS
/*******************************************************************************
 WFK_OAFORM:
       Conti Package OPCIONES ADICIONALES FORMA
*******************************************************************************/
-- FILE NAME..: WFK_OAFORM.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_OAFORM
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/

/******************************************************************************************************************+
 * F_RSS_SRIR : "Regla Solicitud de Servicio de RECTIFICACIÓN DE INSCRIPCIÓN REZAGADOS"
 */ 
FUNCTION F_RSS_SRIR (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SRM : "Regla Solicitud de Servicio de SOLICITUD RESERVA DE MATRICULA"
 */ 
FUNCTION F_RSS_SRM (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_CPE : "Regla Solicitud de Servicio de CAMBIO de PLAN ESTUDIO"
 */ 
FUNCTION F_RSS_CPE (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SRD : "Regla Solicitud de Servicio de SOLICITUD DE SOBREPASO DE RETENCION DE DOCUMENTOS"
 */ 
FUNCTION F_RSS_SRD (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SMCI : "Regla Solicitud de Servicio de MODIFICACIÓN de la CUOTA INICIAL"
 */ 
FUNCTION F_RSS_SMCI (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SR : "Regla Solicitud de Servicio de SOLICITUD DE REINCORPORACIÒN"
 */ 
FUNCTION F_RSS_SR (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;

/******************************************************************************************************************+
 * F_RSS_SESM : "Regla Solicitud de Servicio de SOLICITUD DE EXONERACIÓN DE SEGURO DE SALUD"
 */ 
FUNCTION F_RSS_SESM (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2;


-----------------------------------------------------------------------------------------------------------------

PROCEDURE P_CHECK_DEPT_PRIORIDAD(
            P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_SRVC_CODE       IN SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
            P_CODE_DEPT       IN SORLFOS.SORLFOS_DEPT_CODE%TYPE,
            P_CODE_TERM       IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
            P_CODE_CAMP       IN STVCAMP.STVCAMP_CODE%TYPE,
            P_PRIORIDAD       OUT NUMBER
        );
-----------------------------------------------------------------------------------------------------------------
PROCEDURE P_GETDATA_ALUMN (
            P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_CODE_DEPT       OUT SGBSTDN.SGBSTDN_DEPT_CODE%TYPE, 
            P_CODE_TERM       OUT SFBETRM.SFBETRM_TERM_CODE%TYPE,
            P_CODE_CAMP       OUT STVCAMP.STVCAMP_CODE%TYPE
        );
-----------------------------------------------------------------------------------------------------------------
PROCEDURE P_INGRESANTE_TERM (
            P_PIDM           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_CAMP           IN STVCAMP.STVCAMP_CODE%type,
            P_TERM           IN STVTERM.STVTERM_CODE%TYPE,
            P_DEPT           IN STVDEPT.STVDEPT_CODE%type,
            P_INGRESANTE     OUT BOOLEAN,
            P_FECHA          OUT DATE
        );

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--    
END WFK_OAFORM;
