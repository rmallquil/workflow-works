
/*
SET SERVEROUTPUT ON
DECLARE   P_RESP                 VARCHAR2(100);
BEGIN
    P_CHECK_DEPT_PRIORIDAD(241106,'SOL003',P_RESP);
    DBMS_OUTPUT.PUT_LINE( '--' || P_RESP);
END;
*/

CREATE OR REPLACE PROCEDURE P_CHECK_DEPT_PRIORIDAD(
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       IN SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_RESP            OUT NUMBER
) 
/* ===================================================================================================================
  NOMBRE    : 
  FECHA     : 20/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida la fecha de hoy este dentro de las fechas para generar una solicitud, 
              incluyendo el departamento que le corresponde.
  =================================================================================================================== */
 IS
      V_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%TYPE;
      V_SEQNO                   SORLCUR.SORLCUR_SEQNO%TYPE;
      V_INDICADOR               NUMBER := -1;
      V_RETURN                  NUMBER := -1;
      
      -- GET DEPARTAMENTO
      CURSOR C_SORLCUR_SORLFOS IS
      SELECT    SORLCUR_SEQNO,       
                SORLFOS_DEPT_CODE   
      FROM (
              SELECT    SORLCUR_SEQNO, SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
              ORDER BY SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM <= 1;

BEGIN  
--    
      -- GET DEPARTAMENTO
      OPEN C_SORLCUR_SORLFOS;
      LOOP
        FETCH C_SORLCUR_SORLFOS INTO V_SEQNO, V_ALUM_DEPARTAMENTO;
        EXIT WHEN C_SORLCUR_SORLFOS%NOTFOUND;
      END LOOP;
      CLOSE C_SORLCUR_SORLFOS;
      
      SELECT CASE V_ALUM_DEPARTAMENTO 
                  WHEN 'UREG' THEN 1
                  WHEN 'UPGT' THEN 2
                  WHEN 'UVIR' THEN 3
                  ELSE -1 END INTO V_INDICADOR
      FROM DUAL;

      -- GET COINCIDENCIAS A LOS FILTROS 
      -- UREG 1,2
      -- UPGT 3,4
      -- UVIR 5,6      
      SELECT COUNT(*) INTO P_RESP
      FROM SVRRSRV 
      WHERE SVRRSRV_SRVC_CODE = P_SRVC_CODE
      AND ( 
            TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE(TO_CHAR(SVRRSRV_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')
            AND
            TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE(TO_CHAR(SVRRSRV_END_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')
          )
      AND SVRRSRV_SEQ_NO IN ( (0 + V_INDICADOR) , (1 + V_INDICADOR) ); 

--
END P_CHECK_DEPT_PRIORIDAD;