SET SERVEROUTPUT ON
DECLARE BOOL VARCHAR2(1);
BEGIN
        BOOL :=  WFK_OAFORM.F_RSS_CPE('428093','0','0');
        --DBMS_OUTPUT.PUT_LINE(CASE WHEN BOOL = TRUE THEN 'TRUE' ELSE 'FALSE' END);
        DBMS_OUTPUT.PUT_LINE(BOOL);
END;




SELECT * FROM SPRIDEN WHERE SPRIDEN_ID = '41564482'; -- 169
SELECT * FROM SPRIDEN WHERE SPRIDEN_ID = '42371785'; -- 169
SELECT * FROM SPRIDEN WHERE SPRIDEN_ID = '70400270'; -- 342961
select * from SVRSVPR where SVRSVPR_ACTIVITY_DATE > sysdate -2 and SVRSVPR_SRVC_CODE = 'SOL003';


SET SERVEROUTPUT ON
DECLARE -- P_GETDATA_ALUMN
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE := 299187;
    P_CODE_DEPT       SGBSTDN.SGBSTDN_DEPT_CODE%TYPE;
    P_CODE_TERM       SFBETRM.SFBETRM_TERM_CODE%TYPE;
    P_CODE_CAMP       STVCAMP.STVCAMP_CODE%TYPE;
      P_INDICADOR               NUMBER;
      P_PART_PERIODO            SOBPTRM.SOBPTRM_PTRM_CODE%TYPE; --------------- Parte-de-Periodo
      V_SEQNO                   SORLCUR.SORLCUR_SEQNO%TYPE;
      
      -- GET DEPARTAMENTO
      CURSOR C_SORLCUR_SORLFOS IS
      SELECT    SORLCUR_SEQNO,       
                SORLFOS_DEPT_CODE,
                SORLCUR_CAMP_CODE
      FROM (
              SELECT    SORLCUR_SEQNO, SORLFOS_DEPT_CODE, SORLCUR_CAMP_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
              ORDER BY SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM <= 1;
      
BEGIN  
--    
      P_CODE_TERM   := NULL;
      P_CODE_DEPT   := NULL;
      P_CODE_CAMP   := NULL;
      
      -- GET DEPARTAMENTO, SEDE
      OPEN C_SORLCUR_SORLFOS;
      LOOP
        FETCH C_SORLCUR_SORLFOS INTO V_SEQNO, P_CODE_DEPT, P_CODE_CAMP;
        EXIT WHEN C_SORLCUR_SORLFOS%NOTFOUND;
      END LOOP;
      CLOSE C_SORLCUR_SORLFOS;
      
      -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
      IF (P_CODE_DEPT IS NULL OR P_CODE_CAMP IS NULL) THEN
          P_CODE_TERM := NULL;
      ELSE
            -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
            SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                      WHEN 'UPGT' THEN 'W' 
                                      WHEN 'UREG' THEN 'R' 
                                      WHEN 'UPOS' THEN '-' 
                                      WHEN 'ITEC' THEN '-' 
                                      WHEN 'UCIC' THEN '-' 
                                      WHEN 'UCEC' THEN '-' 
                                      WHEN 'ICEC' THEN '-' 
                                      ELSE '1' END ||
                    CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                      WHEN 'F01' THEN 'A' 
                                      WHEN 'F02' THEN 'L' 
                                      WHEN 'F03' THEN 'C' 
                                      WHEN 'V00' THEN 'V' 
                                      ELSE '9' END
            INTO P_PART_PERIODO
            FROM STVCAMP,STVDEPT 
            WHERE STVDEPT_CODE = P_CODE_DEPT AND STVCAMP_CODE = P_CODE_CAMP;
            
            -- GET PERIODO ACTIVO 
            SELECT COUNT(SFRRSTS_TERM_CODE) INTO P_INDICADOR FROM SFRRSTS WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  ) 
            AND SFRRSTS_RSTS_CODE = 'RW' AND SYSDATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE;
            IF P_INDICADOR = 0 THEN
                  P_CODE_TERM := NULL;
            ELSE
                  SELECT SFRRSTS_TERM_CODE INTO  P_CODE_TERM FROM (
                      -- Forma SFARSTS  ---> fechas para las partes de periodo
                      SELECT DISTINCT SFRRSTS_TERM_CODE 
                      FROM SFRRSTS
                      WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  )
                      AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                      AND SYSDATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE
                      ORDER BY SFRRSTS_TERM_CODE DESC
                  ) WHERE ROWNUM <= 1;                    
            END IF;
      END IF;
      DBMS_OUTPUT.PUT_LINE(P_CODE_DEPT || ' & ' || P_CODE_TERM || ' & ' || P_CODE_CAMP);
--
END;

/*************************************************************************************************/

SET SERVEROUTPUT ON
DECLARE  -- P_CHECK_DEPT_PRIORIDAD
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE := 299187;
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE := 'SOL003';
    P_CODE_DEPT       SORLFOS.SORLFOS_DEPT_CODE%TYPE := 'UREG';
    P_CODE_TERM       SFBETRM.SFBETRM_TERM_CODE%TYPE;
    P_CODE_CAMP       STVCAMP.STVCAMP_CODE%TYPE;
    P_PRIORIDAD       NUMBER;

      V_INDICADOR               NUMBER := -1;
      V_TERMADMIN               NUMBER := -1;
      V_RETURN                  NUMBER := -1;
      V_CODE_DEPT               SORLFOS.SORLFOS_DEPT_CODE%TYPE;
      V_SVRRSRV_SEQ_NO          SVRRSRV.SVRRSRV_SEQ_NO%TYPE;
      
      -- GET DEPARTAMENTO
      CURSOR C_SVRRSRV IS
      SELECT SVRRSRV_SEQ_NO FROM SVRRSRV 
      WHERE SVRRSRV_SRVC_CODE = P_SRVC_CODE
      AND ( 
            TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE(TO_CHAR(SVRRSRV_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')
            AND
            TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE(TO_CHAR(SVRRSRV_END_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')
          )
      AND SVRRSRV_SEQ_NO IN ( (0 + V_INDICADOR) , (1 + V_INDICADOR), (2 + V_INDICADOR) )
      ORDER BY SVRRSRV_SEQ_NO ASC; 
      
      -- GET DEPT en caso no se envie entre los parametros
      CURSOR C_SORLCUR IS
      SELECT    SORLFOS_DEPT_CODE  
      FROM (
              SELECT    SORLCUR_SEQNO,      SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  -- AND SORLCUR_TERM_CODE   =   P_PERIODO 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
              ORDER BY SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM <= 1;
BEGIN  
--    
      P_PRIORIDAD := 0;
      V_CODE_DEPT := P_CODE_DEPT; 
      
      -- en caso se envie DEPT = null o vacio
      IF NVL(TRIM(V_CODE_DEPT),'TRUE') = 'TRUE'  THEN
      
              -- GET datos de alumno 
              OPEN C_SORLCUR;
              LOOP
                FETCH C_SORLCUR INTO V_CODE_DEPT;
                EXIT WHEN C_SORLCUR%NOTFOUND;
                END LOOP;
              CLOSE C_SORLCUR;
              
      END IF;
      
      SELECT CASE V_CODE_DEPT 
                  WHEN 'UREG' THEN 1
                  WHEN 'UPGT' THEN 4
                  WHEN 'UVIR' THEN 7
                  ELSE -99 END INTO V_INDICADOR
      FROM DUAL;
      
      -- GET COINCIDENCIAS A LOS FILTROS 
      -- UREG 1,2,3
      -- UPGT 4,5,6
      -- UVIR 7,8,9    
      -- GET DEPARTAMENTO, SEDE
      OPEN C_SVRRSRV;
      LOOP
        FETCH C_SVRRSRV INTO V_SVRRSRV_SEQ_NO;
        IF C_SVRRSRV%FOUND THEN
            P_PRIORIDAD := V_SVRRSRV_SEQ_NO;
        ELSE EXIT;
      END IF;
      END LOOP;
      CLOSE C_SVRRSRV;
    
    DBMS_OUTPUT.PUT_LINE(P_PRIORIDAD);
--
END ;

/***************************************************************************************************/

SET SERVEROUTPUT ON
DECLARE  -- FUNCTION F_RSS_SRM (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE := 342961;
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE := '';
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE := '';
    RESP  VARCHAR2(500);

      V_CODIGO_SOL      SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL005';
      V_CODE_DEPT       SORLFOS.SORLFOS_DEPT_CODE%TYPE      := 'UREG';
      V_CODE_TERM       SFBETRM.SFBETRM_TERM_CODE%TYPE      := '201710';
      V_CODE_CAMP       STVCAMP.STVCAMP_CODE%TYPE           := 'S01';
      V_PRIORIDAD       NUMBER  := 3;
      V_TERM_ADMI       NUMBER;
      
BEGIN  
--    
    -- DATOS ALUMNO
    --P_GETDATA_ALUMN(P_PIDM,V_CODE_DEPT,V_CODE_TERM,V_CODE_CAMP);

    -- VALIDAR MODALIDAD Y PRIORIDAD
    --P_CHECK_DEPT_PRIORIDAD (P_PIDM, V_CODIGO_SOL, V_CODE_DEPT, V_CODE_TERM, V_CODE_CAMP, V_PRIORIDAD);

    -- Segun regla para UPGT y UVIR, los que tramitan reserva el mismo periodo que ingresaron 
    -- se les RESERVA las prioridades "4"  y "7"
    IF (V_CODE_DEPT = 'UPGT' OR V_CODE_DEPT = 'UVIR') AND (V_PRIORIDAD = 4 OR V_PRIORIDAD = 7) THEN

        -- VALIDAR QUE EL ALUMNO SEA INGRESANTE EN EL MISMO PERIODO DEL TRAMITE. (PERIODO DE INGRESO = PERIODO ACTUAL)
        SELECT  COUNT(*) INTO V_TERM_ADMI
        FROM SORLCUR        INNER JOIN SORLFOS
        ON    SORLCUR_PIDM          = SORLFOS_PIDM 
        AND   SORLCUR_SEQNO         = SORLFOS.SORLFOS_LCUR_SEQNO
        WHERE SORLCUR_PIDM          =  P_PIDM
        AND SORLCUR_LMOD_CODE       = 'LEARNER' /*#Estudiante*/ 
        AND SORLCUR_CACT_CODE       = 'ACTIVE' 
        AND SORLCUR_CURRENT_CDE     = 'Y'
        AND SORLCUR_TERM_CODE_ADMIT = V_CODE_TERM -- VALIDACION (PERIODO DE INGRESO = PERIODO ACTUAL)
        AND SORLFOS_DEPT_CODE       = V_CODE_DEPT;

    ELSE
        V_TERM_ADMI := 1;
    END IF;

    IF (V_PRIORIDAD > 0 AND V_TERM_ADMI > 0)THEN
        DBMS_OUTPUT.PUT_LINE('Y');
    END IF;
        DBMS_OUTPUT.PUT_LINE('N');
    
--
END ;

/*******************************************************************************************/

 SET SERVEROUTPUT ON
DECLARE -- FUNCTION F_RSS_SRIR (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE := 299187;
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE;
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE;

      P_CODIGO_SOL          SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL003';
      V_PRIORIDAD           NUMBER := 3;
      P_INDICADOR           NUMBER;
      V_CODE_DEPT           SORLFOS.SORLFOS_DEPT_CODE%TYPE;
      V_CODE_TERM           SFBETRM.SFBETRM_TERM_CODE%TYPE := '201710';
      V_CODE_CAMP           STVCAMP.STVCAMP_CODE%TYPE;
BEGIN  
--    
    -- DATOS ALUMNO
    --P_GETDATA_ALUMN(P_PIDM,V_CODE_DEPT,V_CODE_TERM,V_CODE_CAMP);

    -- VALIDAR MODALIDAD Y PRIORIDAD
    --P_CHECK_DEPT_PRIORIDAD (P_PIDM, P_CODIGO_SOL, V_CODE_DEPT, NULL, NULL, V_PRIORIDAD);

    -- VERIFICAR QUE YA ESTE MATRICULADO
    ---A VERIFICAR QUE TENGA NRC's
      SELECT COUNT(*) INTO P_INDICADOR FROM SFRSTCR 
      WHERE SFRSTCR_PIDM = P_PIDM
      AND SFRSTCR_TERM_CODE = V_CODE_TERM; 
      
    IF V_PRIORIDAD > 0 AND P_INDICADOR > 0 THEN
        DBMS_OUTPUT.PUT_LINE('Y');
    END IF;
        DBMS_OUTPUT.PUT_LINE('N');
--  
    DBMS_OUTPUT.PUT_LINE(P_INDICADOR);
END ;
