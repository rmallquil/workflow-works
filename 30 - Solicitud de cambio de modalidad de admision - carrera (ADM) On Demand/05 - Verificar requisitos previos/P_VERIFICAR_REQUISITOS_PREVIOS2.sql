/*
SET SERVEROUTPUT ON
DECLARE P_ARCHIVADOR_APEC VARCHAR2(4000);
 P_REQUISITOS_APEC VARCHAR2(4000);
 P_ARCHIVADOR_BANNER VARCHAR2(4000);
 P_REQUISITOS_BANNER VARCHAR2(4000);
 P_MESSAGE VARCHAR2(4000);
BEGIN
    P_VERIFICAR_REQUISITOS_PREVIOS (635781, '45958636','201910',P_ARCHIVADOR_APEC,P_REQUISITOS_APEC,P_ARCHIVADOR_BANNER,P_REQUISITOS_BANNER, P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_ARCHIVADOR_APEC);
    DBMS_OUTPUT.PUT_LINE(P_REQUISITOS_APEC);
    DBMS_OUTPUT.PUT_LINE(P_ARCHIVADOR_BANNER);
    DBMS_OUTPUT.PUT_LINE(P_REQUISITOS_BANNER);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
END;
*/
CREATE OR REPLACE PROCEDURE P_VERIFICAR_REQUISITOS_PREVIOS (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_TERM_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
    P_ARCHIVADOR_APEC   OUT VARCHAR2,
    P_REQUISITOS_APEC   OUT VARCHAR2,
    P_ARCHIVADOR_BANNER OUT VARCHAR2,
    P_REQUISITOS_BANNER OUT VARCHAR2,
    P_MESSAGE           OUT VARCHAR2
)
AS
    V_APEC_TERM         VARCHAR2(10);
    V_FLAG_ARCH_APEC    BOOLEAN := FALSE;
    V_FLAG_ARCH_BANNER  BOOLEAN := FALSE;
    V_FLAG_REQ_APEC     BOOLEAN := FALSE;
    V_FLAG_REQ_BANNER   BOOLEAN := FALSE;
    
    V_ARCH_APEC         VARCHAR2(1000) := '- Sin Registros';
    V_ARCH_BANNER       VARCHAR2(1000) := '- Sin Registros';
    V_REQ_APEC          VARCHAR2(1000) := 'Sin registro de requisitos previos';
    V_REQ_BANNER        VARCHAR2(1000) := 'Sin registro de requisitos previos';

BEGIN

    -- PERIODO
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM 
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;

    -- OBTENER LOS REGISTROS DE ARCHIVADOR DE APEC
    WITH CTE AS(
        SELECT "Archivador" ARCHIVADORAPEC, ROWNUM AS N
        FROM tblPostulanteRequisitos@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDAlumno" = P_ID
            AND ("Archivador" IS NOT NULL OR "Archivador" <> '')
    ),
    CTE2(ARCHIVADORAPEC, N, ARCHIVADORAPEC_ULT) AS
    (
        SELECT ARCHIVADORAPEC, N, ARCHIVADORAPEC
        FROM CTE
        WHERE N=1
        
        UNION ALL
        
        SELECT a.ARCHIVADORAPEC, a.N, B.ARCHIVADORAPEC_ULT || '<br />' || a.ARCHIVADORAPEC
        FROM CTE a
        INNER JOIN CTE2 b ON 
            a.N = b.N + 1
    )
    SELECT NVL(MAX(ARCHIVADORAPEC_ULT),'- Sin Registro')
        INTO P_ARCHIVADOR_APEC
    FROM CTE2
    ;

    -- OBTENER LOS REGISTROS DE ARCHIVADOR DE BANNER
    WITH cte AS(
        SELECT SARCHKL_CODE_VALUE ArchivadorBANNER, rownum as n
        FROM SARCHKL
        WHERE SARCHKL_TERM_CODE_ENTRY = P_TERM_CODE
            AND (SARCHKL_CODE_VALUE IS NOT NULL OR SARCHKL_CODE_VALUE <> '')
            AND SARCHKL_PIDM = P_PIDM
    ),
    cte2(ArchivadorBANNER, n, ArchivadorBANNER_Ult) AS
    (
        SELECT ArchivadorBANNER, n, ArchivadorBANNER
        FROM cte
        WHERE  n=1
        
        UNION ALL
        
        SELECT a.ArchivadorBANNER, a.n, b.ArchivadorBANNER_Ult || '<br />' || a.ArchivadorBANNER
        FROM cte a
        INNER JOIN cte2 b
            ON a.n = b.n + 1
    )
    SELECT NVL(MAX(ArchivadorBANNER_Ult),'- Sin Registro')
        INTO P_ARCHIVADOR_BANNER
    FROM cte2
    ;

    -- OBTENER LOS REQUISITOS DE APEC
     WITH CTE AS (
        SELECT CASE 
            WHEN pr."Observacion" IS NOT NULL THEN '- ' || re."Nombre" || ' - Obs: ' || pr."Observacion"
            WHEN pr."Observacion" <> '' THEN '- ' || re."Nombre" || ' - Obs: ' || pr."Observacion"
            ELSE '- ' || re."Nombre"
            END RequisitoAPEC, rownum as n
        FROM tblPostulanteRequisitos@BDUCCI.CONTINENTAL.EDU.PE pr
        INNER JOIN tblRequisitos@BDUCCI.CONTINENTAL.EDU.PE re ON
            pr."IDRequisito" = re."IDRequisito"
        WHERE pr."IDDependencia" = 'UCCI'
            AND pr."IDPerAcad" = V_APEC_TERM
            AND pr."IDAlumno" = P_ID
            AND pr."CantEntregada" > 0
        ORDER BY re."Nombre"
    ),
    CTE2(RequisitoAPEC, n, RequisitoAPEC_ULT) AS (
        SELECT RequisitoAPEC, n, RequisitoAPEC
        FROM CTE
        WHERE n=1
        
        UNION ALL
        
        SELECT a.RequisitoAPEC, a.n, b.RequisitoAPEC_ULT || '<br />' || a.RequisitoAPEC
        FROM CTE a
        INNER JOIN cte2 b ON
            a.n = b.n + 1
    )
    SELECT NVL(MAX(RequisitoAPEC_ULT),'- Sin registro de requisitos previos')
        INTO P_REQUISITOS_APEC
    FROM CTE2
    ;

    -- OBTENER LOS REQUISITOS DE BANNER
    with cte as(
        SELECT CASE 
            WHEN SARCHKL_COMMENT IS NOT NULL THEN '- ' || STVADMR_DESC || ' - Obs: ' || SARCHKL_COMMENT 
            WHEN SARCHKL_COMMENT <> '' THEN '- ' || STVADMR_DESC || ' - Obs: ' || SARCHKL_COMMENT 
            ELSE '- ' || STVADMR_DESC
            END RequisitoBANNER, rownum as n
        FROM SARCHKL
        INNER JOIN STVADMR ON
            SARCHKL_ADMR_CODE = STVADMR_CODE
        WHERE SARCHKL_TERM_CODE_ENTRY = P_TERM_CODE
        AND SARCHKL_PIDM = P_PIDM
        AND SARCHKL_CKST_CODE = 'ENTREGADO'
    ),
    cte2(RequisitoBANNER, n, RequisitoBANNER_Ult) as
    (
        SELECT RequisitoBANNER, n, RequisitoBANNER
        FROM cte
        WHERE n=1
        UNION ALL
        SELECT a.RequisitoBANNER, a.n, b.RequisitoBANNER_Ult || '<br />' || a.RequisitoBANNER
        FROM cte a
        INNER JOIN cte2 b ON
            a.n = b.n + 1
    )
    SELECT NVL(MAX(RequisitoBANNER_Ult),'- Sin registro de requisitos previos')
        INTO P_REQUISITOS_BANNER
    FROM cte2
    ;

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_REQUISITOS_PREVIOS;