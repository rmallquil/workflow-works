/******************************************************************************/
/* BWZKACOD.sql                                                               */
/******************************************************************************/
/*                                                                            */
/* Descripción corta: Script para generar el Paquete relacionado a los        */
/*                    procedimientos de Cambio de modalidad de admisión y/o   */
/*                    E.A.P. en Admisión.                                     */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                 INI    FECHA    */
/* ---------------------------------------------------------- --- ----------- */
/* 1. Creación del código.                                    BFV 10/ENE/2019 */
/*    Creación del paquete de Permanencia de Plan de Estudios                 */
/*                                                                            */
/*    Procedure P_VALID_ID: Verifica si el DNI ingresado existe en la DB      */
/*    Procedure P_GET_INFO_INGRESANTE: El objetivo es que se obtenga los      */
/*      datos del estudiante.                                                 */
/*    Procedure P_GET_USUARIOS_ADMISION: Obtienes el correo de los usuarios   */
/*      de las oficinas involucradas                                          */
/*    Procedure P_VERIFICAR_APTO: El objetivo es que se saber si el           */
/*      estudiante cumple con las verificaciones necesarias para acceder al   */
/*      servicio del Workflow.                                                */
/*    Procedure P_VERIFICAR_REQUISITOS_PREVIOS: Se verifica si el estudiante  */
/*      ha traido sus requisitos de admision                                  */
/*    Procedure P_GET_CAMBIO_ADM: Obtiene el cambio realizado en RECRUITER    */
/*    Procedure P_VALIDAR_CONVA: El objetivo es verificar si tiene registrado */
/*      una convalidacion                                                     */
/*    Procedure P_VERIFICAR_MATRICULA: Verifica si tiene matricula activa.    */
/*    Procedure P_ELIMINAR_NRC: Elimina la matricula del estudiante en el     */
/*      periodo solicitado.                                                   */
/*    Procedure P_VERIFICAR_CAMBIO_PLAN: El objetivo es verificar si en el    */
/*      cambio se esta dando un cambio de plan.                               */
/*    Procedure P_SET_CAMBIAR_ADM: Crea el registro con los datos del cambio  */
/*      y se inactiva el anterior registro.                                   */
/*    Procedure P_VERIFICAR_CURSOS_ADM: Crea el registro con los datos del    */
/*      cambio.                                                               */
/*    Procedure P_EXECCAPP: El objetivo es ejecutar el CAPP                   */
/*    Procedure P_EXECPROY: El objetivo es ejecutar la PROYECCION             */

/*    Procedure P_VERIFICAR_PAGOS_CTA: Se verifica si el estudiante tiene     */
/*      pagos en su cuenta anterior                                           */
/*    Procedure P_VERIFICAR_BENEFICIO_PREVIO: Elimina la cuenta anterior      */
/*      del estudiante                                                        */
/*    Procedure P_REGISTRAR_CAMBIO_ADM: Registra en la BD el cambio del       */
/*      estudiante solicitante                                                */
/*    Procedure P_SET_CAMBIAR_SGASTDN: Registra el cambio en SGASTDN          */
/*    Procedure P_SET_CAMBIAR_SAAADMS: Registra el cambio en SAAADMS          */
/* -------------------------------------------------------------------------- */
/*                                                                            */
/* FIN DEL SEGUIMIENTO                                                        */
/*                                                                            */
/**********************************************************************/
-- Creación BANNIS  Y PERMISOS
/**********************************************************************/
--     CREATE OR REPLACE PUBLIC SYNONYM "BWZKACOD" FOR "BANINST1"."BWZKACOD";
--     GRANT EXECUTE ON BANINST1.BWZKACOD TO SATURN;
--------------------------------------------------------------------------
--------------------------------------------------------------------------

CREATE OR REPLACE PACKAGE BODY BANINST1.BWZKACOD AS
/*
  BWZKACOD:
       Paquete Web _ Desarrollo Propio _ Paquete _ Cambio Mod Admision Carrera On Demand (ADM)
*/
-- FILE NAME..: BWZKACOD.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKACOD
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2019
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              

PROCEDURE P_VALID_ID (
    P_ID            IN  SPRIDEN.SPRIDEN_ID%TYPE,
    P_DNI_VALID     OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_VALID_ID (P_ID, P_DNI_VALID);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VALID_ID;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_INFO_INGRESANTE (
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_PIDM              OUT SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         OUT SOBPTRM.SOBPTRM_TERM_CODE%TYPE, 
    P_DEPT_CODE         OUT STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         OUT STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_DESC         OUT STVCAMP.STVCAMP_DESC%TYPE,
    P_PROGRAM           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_PROGRAM_DESC      OUT SMRPRLE.SMRPRLE_PROGRAM_DESC%TYPE,
    P_MOD_ADM           OUT VARCHAR2,
    P_MOD_ADM_DESC      OUT VARCHAR2,
    P_CATALOGO          OUT VARCHAR2,
    P_EMAIL             OUT GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE,
    P_NAME              OUT VARCHAR2,
    P_FECHA_SOL         OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_GET_INFO_INGRESANTE (P_ID, P_PIDM, P_TERM_CODE, P_DEPT_CODE, P_CAMP_CODE, P_CAMP_DESC, P_PROGRAM, P_PROGRAM_DESC, P_MOD_ADM, P_MOD_ADM_DESC, P_CATALOGO, P_EMAIL, P_NAME, P_FECHA_SOL);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_INFO_INGRESANTE;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_USUARIOS_ADMISION (
    P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CORREO_RRAA           OUT VARCHAR2,
    P_ROL_DOC_RRAA          OUT VARCHAR2,
    P_CORREO_DOC_RRAA       OUT VARCHAR2,
    P_CORREO_BU             OUT VARCHAR2,
    P_CORREO_ADM            OUT VARCHAR2,
    P_ROL_CAJA              OUT VARCHAR2,
    P_CORREO_CAJA           OUT VARCHAR2,
    P_CORREO_CAU            OUT VARCHAR2,
    P_MESSAGE               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_USUARIO
  FECHA     : 04/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento LOS CORREOS deL(os) responsable(s) 
              de Registros Academicos de esa sede.
  =================================================================================================================== */
AS
    -- @PARAMETERS
    V_ROLE_ID                   NUMBER;
    V_ORG_ID                    NUMBER;
    V_Email_Address             VARCHAR2(100);
    V_ROL_SEDE                  VARCHAR2(100);
    V_ROL                       VARCHAR2(100);
    V_CORREO                    VARCHAR2(4000);
    
    V_SECCION_EXCEPT            VARCHAR2(50);
    
    V_CAMP_CODE                 STVCAMP.STVCAMP_CODE%TYPE;
    
    CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * 
        FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = V_ORG_ID
        AND ROLE_ID = V_ROLE_ID;
        
    CURSOR C_OFFICE IS
        SELECT 'ADMbienestar' FROM DUAL
        UNION
        SELECT 'ADMregistro' FROM DUAL
        UNION
        SELECT 'cau' FROM DUAL
        UNION
        SELECT 'ADMcaja' FROM DUAL
        UNION
        SELECT 'requisito' FROM DUAL
        UNION
        SELECT 'admision' FROM DUAL
        ;
      
      V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
BEGIN 
--
    OPEN C_OFFICE;
    LOOP
        FETCH C_OFFICE INTO V_ROL;
        EXIT WHEN C_OFFICE%NOTFOUND;
            
            V_CORREO := NULL;
            V_Email_Address := NULL;

            IF V_ROL = 'ADMcaja' THEN
                V_CAMP_CODE := 'S01';
            ELSE
                V_CAMP_CODE := P_CAMP_CODE;
            END IF;

            V_ROL_SEDE := V_ROL || V_CAMP_CODE;
            -- Obtener el ROL_ID 
            V_SECCION_EXCEPT := 'ROLES';
            
            SELECT ID 
                INTO V_ROLE_ID
            FROM WORKFLOW.ROLE
            WHERE NAME = V_ROL_SEDE;
            
            V_SECCION_EXCEPT := '';
            
            -- Obtener el ORG_ID 
            V_SECCION_EXCEPT := 'ORGRANIZACION';
            
            SELECT ID
                INTO V_ORG_ID
            FROM WORKFLOW.ORGANIZATION
            WHERE NAME = 'Root';
            
            V_SECCION_EXCEPT := '';
            
            -- Obtener los datos de usuarios que relaciona rol y usuario
            V_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
            -- #######################################################################
            OPEN C_ROLE_ASSIGNMENT;
            LOOP
                FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
                EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
                    -- Obtener Datos Usuario
                    SELECT Email_Address
                        INTO V_Email_Address
                    FROM WORKFLOW.WFUSER
                    WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;
                    
                    IF V_Email_Address IS NOT NULL OR V_Email_Address <> '' THEN
                        IF V_CORREO IS NULL OR V_CORREO = '' THEN
                            V_CORREO := V_Email_Address;
                        ELSE
                            V_CORREO := V_CORREO || ',' || V_Email_Address;
                        END IF;
                    END IF;
            
                    IF V_ROL = 'bienestar' OR V_ROL = 'ADMbienestar' THEN
                        P_CORREO_BU := V_CORREO;
                    ELSIF V_ROL = 'ADMregistro' THEN
                        P_CORREO_RRAA := V_CORREO;
                    ELSIF V_ROL = 'requisito' THEN
                        P_CORREO_DOC_RRAA := V_CORREO;
                        P_ROL_DOC_RRAA := V_ROL_SEDE;
                    ELSIF V_ROL = 'cau' THEN
                        P_CORREO_CAU := V_CORREO;
                    ELSIF V_ROL = 'admision' THEN
                        P_CORREO_ADM := V_CORREO;
                    ELSIF V_ROL = 'ADMcaja' THEN
                        P_CORREO_CAJA := V_CORREO;
                        P_ROL_CAJA := V_ROL_SEDE;
                    END IF;
                    
            END LOOP;
            CLOSE C_ROLE_ASSIGNMENT;
            V_SECCION_EXCEPT := '';

    END LOOP;
    CLOSE C_OFFICE;
          
EXCEPTION
WHEN TOO_MANY_ROWS THEN 
    IF ( V_SECCION_EXCEPT = 'ROLES') THEN
      P_MESSAGE := '- Se encontraron mas de un ROL con el mismo nombre: ' || V_ROL || V_CAMP_CODE;
    ELSIF (V_SECCION_EXCEPT = 'ORGRANIZACION') THEN
      P_MESSAGE := '- Se encontraron mas de una ORGANIZACIÓN con el mismo nombre.';
    ELSIF (V_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
      P_MESSAGE := '- Se encontraron mas de un usuario con el mismo ROL.';
    ELSE  P_MESSAGE := SQLERRM;
    END IF; 
RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
WHEN NO_DATA_FOUND THEN
    IF ( V_SECCION_EXCEPT = 'ROLES') THEN
      P_MESSAGE := '- NO se encontró el nombre del ROL: ' || V_ROL || V_CAMP_CODE;
    ELSIF (V_SECCION_EXCEPT = 'ORGRANIZACION') THEN
      P_MESSAGE := '- NO se encontró el nombre de la ORGANIZACION.';
    ELSIF (V_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
      P_MESSAGE := '- NO  se encontró ningun usuario con esas caracteristicas.';
    ELSE  P_MESSAGE := SQLERRM;
    END IF; 
RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
WHEN OTHERS THEN
RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_USUARIOS_ADMISION;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_APTO (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_STATUS_APTO     OUT VARCHAR2,
    P_MESSAGE         OUT VARCHAR2
)
AS
    V_SOLTRA            INTEGER := 0; --VERIFICA SOL. TRASLADO INTERNO
    V_SOLPER            INTEGER := 0; --VERIFICA SOL. PERMANENCIA DE PLAN
    V_SOLRES            INTEGER := 0; --VERIFICA SOL. RESERVA
    V_SOLREC            INTEGER := 0; --VERIFICA SOL. RECTIFICACION DE MATRICULA
    V_SOLCAM            INTEGER := 0; --VERIFICA SOL. CAMBIO DE PLAN
    V_SOLCUO            INTEGER := 0; --VERIFICA SOL. CAMBIO DE CUOTA INICIAL
    V_SOLDIR            INTEGER := 0; --VERIFICA SOL. ASIGNTURA DIRIGIDA
    V_SOLCON            INTEGER := 0; --VERIFICA SOL. DESCUENTO POR CONVENIO
    V_PROC_DUP          INTEGER := 0; --VERIFICA SI EL ESTUDIANTE YA TIENE UN PROCESO DE CAMBIO FINALIZADO
    V_FEC_ADM           INTEGER := 0; --VERIFICA SI ESTA VIGENTE EL PERIODO DE ADMISION DEL ESTUDIANTE PARA EL CAMBIO
    V_RESPUESTA         VARCHAR2(4000) := NULL; --RESPUESTA ANIDADA DE LAS VALIDACIONES
    V_FLAG              INTEGER := 0; --BANDERA DE VALIDACIÓN

    V_CODE_DEPT         STVDEPT.STVDEPT_CODE%TYPE;
    V_CODE_CAMP         STVCAMP.STVCAMP_CODE%TYPE;
    V_PROGRAM           SORLCUR.SORLCUR_PROGRAM%TYPE;

    --################################################################################################
    -- OBTENER PERIODO DE CATALOGO, CAMPUS, DEPT y CARRERA
    --################################################################################################
    CURSOR C_SORLCUR IS
        SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_PROGRAM
        FROM(
            SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_PROGRAM
            FROM SORLCUR 
            INNER JOIN SORLFOS ON
                SORLCUR_PIDM = SORLFOS_PIDM 
                AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
            WHERE SORLCUR_PIDM = P_PIDM 
                AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y'
                AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
        ) WHERE ROWNUM = 1;

BEGIN
    ---- P_VERIFICAR_APTO PARA SOLICITUD DE CAMBIO DE CARRERA (ADMISION) ON DEMAND
    --################################################################################################
    -->> Obtener SEDE, DEPT, TERM CATALOGO Y PROGRAMA (CARRERA)
    --################################################################################################
    OPEN C_SORLCUR;
    LOOP
        FETCH C_SORLCUR INTO V_CODE_CAMP, V_CODE_DEPT, V_PROGRAM;
        EXIT WHEN C_SORLCUR%NOTFOUND;
        END LOOP;
    CLOSE C_SORLCUR;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RECTIFICACIÓN DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLREC
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL003'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');

    IF V_SOLREC > 0 THEN
        V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RESERVA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLRES
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL005'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
    
    IF V_SOLRES > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE TRASLADO INTERNO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLTRA
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL013'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
    
    IF V_SOLTRA > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE CAMBIO DE PLAN DE ESTUDIO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLCAM
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL004'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCAM > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE PERMANENCIA DE PLAN DE ESTUDIOS ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLPER
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL015'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLPER > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Permanencia de Plan de Estudios activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Permanencia de Plan de Estudios activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE CAMBIO DE CUOTA INICIAL ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLCUO
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL007'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCUO > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Cambio de cuota inicial activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Cambio de cuota inicial activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE DIRIGIDO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLDIR
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL014'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLDIR > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        END IF;
        V_FLAG := 1;
    END IF;
    
    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE DESCUENTO POR CONVENIO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLCON
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL019'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
    
    IF V_SOLCON > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Descuento por convenio activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Descuento pot convenio activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE PERIODO DE INGRESO
    --################################################################################################
    SELECT COUNT(*)
        INTO V_PROC_DUP
    FROM ADM.tblCambiosADM@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "pidm" = P_PIDM
    AND "term" = P_TERM_CODE;

    IF V_PROC_DUP > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Ya presenta un cambio de admisión. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Ya presenta un cambio de admisión. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE FECHAS DE ADMISIÓN
    --################################################################################################
    SELECT COUNT(*)
        INTO V_FEC_ADM
    FROM ADM.tblFechaAdmisionWF@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "term" = P_TERM_CODE
    AND "campus" = V_CODE_CAMP
    AND "dept" = V_CODE_DEPT
    AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE("FechaInicio",'dd/mm/yy')
    AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE("FechaFin",'dd/mm/yy');

    IF V_FEC_ADM = 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- No esta dentro de las fechas de admisión vigentes al periodo '|| P_TERM_CODE || '. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- No esta dentro de las fechas de admisión vigentes al periodo '|| P_TERM_CODE || '. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN FINAL
    --################################################################################################
    IF V_FLAG > 0 THEN
        P_STATUS_APTO := 'FALSE';
        P_MESSAGE := V_RESPUESTA;
    ELSE
        P_STATUS_APTO := 'TRUE';
        P_MESSAGE := 'OK';
    END IF;

EXCEPTION 
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_APTO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_REQUISITOS_PREVIOS (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_TERM_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
    P_ARCHIVADOR_APEC   OUT VARCHAR2,
    P_REQUISITOS_APEC   OUT VARCHAR2,
    P_ARCHIVADOR_BANNER OUT VARCHAR2,
    P_REQUISITOS_BANNER OUT VARCHAR2,
    P_MESSAGE           OUT VARCHAR2
)
AS
    V_APEC_TERM         VARCHAR2(10);
    V_FLAG_ARCH_APEC    BOOLEAN := FALSE;
    V_FLAG_ARCH_BANNER  BOOLEAN := FALSE;
    V_FLAG_REQ_APEC     BOOLEAN := FALSE;
    V_FLAG_REQ_BANNER   BOOLEAN := FALSE;
    
    V_ARCH_APEC         VARCHAR2(1000) := '- Sin Registros';
    V_ARCH_BANNER       VARCHAR2(1000) := '- Sin Registros';
    V_REQ_APEC          VARCHAR2(1000) := 'Sin registro de requisitos previos';
    V_REQ_BANNER        VARCHAR2(1000) := 'Sin registro de requisitos previos';

BEGIN

    -- PERIODO
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM 
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;

    -- OBTENER LOS REGISTROS DE ARCHIVADOR DE APEC
    WITH CTE AS(
        SELECT "Archivador" ARCHIVADORAPEC, ROWNUM AS N
        FROM tblPostulanteRequisitos@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
            AND "IDPerAcad" = V_APEC_TERM
            AND "IDAlumno" = P_ID
            AND ("Archivador" IS NOT NULL OR "Archivador" <> '')
    ),
    CTE2(ARCHIVADORAPEC, N, ARCHIVADORAPEC_ULT) AS
    (
        SELECT ARCHIVADORAPEC, N, ARCHIVADORAPEC
        FROM CTE
        WHERE N=1
        
        UNION ALL
        
        SELECT a.ARCHIVADORAPEC, a.N, B.ARCHIVADORAPEC_ULT || '<br />' || a.ARCHIVADORAPEC
        FROM CTE a
        INNER JOIN CTE2 b ON 
            a.N = b.N + 1
    )
    SELECT NVL(MAX(ARCHIVADORAPEC_ULT),'- Sin Registro')
        INTO P_ARCHIVADOR_APEC
    FROM CTE2
    ;

    -- OBTENER LOS REGISTROS DE ARCHIVADOR DE BANNER
    WITH cte AS(
        SELECT SARCHKL_CODE_VALUE ArchivadorBANNER, rownum as n
        FROM SARCHKL
        WHERE SARCHKL_TERM_CODE_ENTRY = P_TERM_CODE
            AND (SARCHKL_CODE_VALUE IS NOT NULL OR SARCHKL_CODE_VALUE <> '')
            AND SARCHKL_PIDM = P_PIDM
    ),
    cte2(ArchivadorBANNER, n, ArchivadorBANNER_Ult) AS
    (
        SELECT ArchivadorBANNER, n, ArchivadorBANNER
        FROM cte
        WHERE  n=1
        
        UNION ALL
        
        SELECT a.ArchivadorBANNER, a.n, b.ArchivadorBANNER_Ult || '<br />' || a.ArchivadorBANNER
        FROM cte a
        INNER JOIN cte2 b
            ON a.n = b.n + 1
    )
    SELECT NVL(MAX(ArchivadorBANNER_Ult),'- Sin Registro')
        INTO P_ARCHIVADOR_BANNER
    FROM cte2
    ;

    -- OBTENER LOS REQUISITOS DE APEC
     WITH CTE AS (
        SELECT CASE 
            WHEN pr."Observacion" IS NOT NULL THEN '- ' || re."Nombre" || ' - Obs: ' || pr."Observacion"
            WHEN pr."Observacion" <> '' THEN '- ' || re."Nombre" || ' - Obs: ' || pr."Observacion"
            ELSE '- ' || re."Nombre"
            END RequisitoAPEC, rownum as n
        FROM tblPostulanteRequisitos@BDUCCI.CONTINENTAL.EDU.PE pr
        INNER JOIN tblRequisitos@BDUCCI.CONTINENTAL.EDU.PE re ON
            pr."IDRequisito" = re."IDRequisito"
        WHERE pr."IDDependencia" = 'UCCI'
            AND pr."IDPerAcad" = V_APEC_TERM
            AND pr."IDAlumno" = P_ID
            AND pr."CantEntregada" > 0
        ORDER BY re."Nombre"
    ),
    CTE2(RequisitoAPEC, n, RequisitoAPEC_ULT) AS (
        SELECT RequisitoAPEC, n, RequisitoAPEC
        FROM CTE
        WHERE n=1
        
        UNION ALL
        
        SELECT a.RequisitoAPEC, a.n, b.RequisitoAPEC_ULT || '<br />' || a.RequisitoAPEC
        FROM CTE a
        INNER JOIN cte2 b ON
            a.n = b.n + 1
    )
    SELECT NVL(MAX(RequisitoAPEC_ULT),'- Sin registro de requisitos previos')
        INTO P_REQUISITOS_APEC
    FROM CTE2
    ;

    -- OBTENER LOS REQUISITOS DE BANNER
    with cte as(
        SELECT CASE 
            WHEN SARCHKL_COMMENT IS NOT NULL THEN '- ' || STVADMR_DESC || ' - Obs: ' || SARCHKL_COMMENT 
            WHEN SARCHKL_COMMENT <> '' THEN '- ' || STVADMR_DESC || ' - Obs: ' || SARCHKL_COMMENT 
            ELSE '- ' || STVADMR_DESC
            END RequisitoBANNER, rownum as n
        FROM SARCHKL
        INNER JOIN STVADMR ON
            SARCHKL_ADMR_CODE = STVADMR_CODE
        WHERE SARCHKL_TERM_CODE_ENTRY = P_TERM_CODE
        AND SARCHKL_PIDM = P_PIDM
        AND SARCHKL_CKST_CODE = 'ENTREGADO'
    ),
    cte2(RequisitoBANNER, n, RequisitoBANNER_Ult) as
    (
        SELECT RequisitoBANNER, n, RequisitoBANNER
        FROM cte
        WHERE n=1
        UNION ALL
        SELECT a.RequisitoBANNER, a.n, b.RequisitoBANNER_Ult || '<br />' || a.RequisitoBANNER
        FROM cte a
        INNER JOIN cte2 b ON
            a.n = b.n + 1
    )
    SELECT NVL(MAX(RequisitoBANNER_Ult),'- Sin registro de requisitos previos')
        INTO P_REQUISITOS_BANNER
    FROM cte2
    ;

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_REQUISITOS_PREVIOS;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_CAMBIO_ADM (
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_DEPT_CODE			IN STVDEPT.STVDEPT_CODE%TYPE,
    P_PROGRAM           IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_MOD_ADM           IN VARCHAR2,
    P_MOD_ADM_DESC      IN VARCHAR2,
    P_CAMBIO_ADM        OUT VARCHAR2,
    P_MOD_ADM_NEW       OUT VARCHAR2,
    P_MOD_ADM_NEW_DESC  OUT VARCHAR2,
    P_PROGRAM_NEW       OUT VARCHAR2,
    P_PROGRAM_NEW_DESC  OUT VARCHAR2,
    P_MESSAGE           OUT VARCHAR2
)
AS
	V_CONTADOR          NUMBER;
    V_FLAG_PROGRAM      NUMBER := 0;
    V_FLAG_MOD_ADM      NUMBER := 0;
	V_APEC_TERM         VARCHAR2(10);
    V_APEC_DEPT         VARCHAR2(10);
    V_APEC_MOD_ADM      VARCHAR(10);
    V_APEC_MOD_ADM_NEW  VARCHAR(10);
    V_PROGRAM_DESC      VARCHAR2(100);
    
    V_CODE_DEPT         STVDEPT.STVDEPT_CODE%TYPE;
    V_CODE_CAMP         STVCAMP.STVCAMP_CODE%TYPE;
    V_PROGRAM           SORLCUR.SORLCUR_PROGRAM%TYPE;

BEGIN
    
    -- PERIODO
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;
    
    -- DEPARTAMENTO
    SELECT CZRDEPT_DEPT_BDUCCI
    INTO V_APEC_DEPT
    FROM CZRDEPT 
    WHERE CZRDEPT_CODE = P_DEPT_CODE;

    -- MODALIDAD DE ADMISION
    SELECT "IDModalidadPostu"
        INTO V_APEC_MOD_ADM
    FROM dbo.tblModalidadPostuRecruiter@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "IDModRecruiter" = P_MOD_ADM
    AND "departament" = V_APEC_DEPT;
    
    --INDICADOR QUE EXISTE EL CAMBIO
    SELECT COUNT(*)
        INTO V_CONTADOR
    FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
    WHERE TRIM("IDDependencia") = 'UCCI'
    AND TRIM("IDPerAcad") = V_APEC_TERM
    AND TRIM("IDEscuelaADM") = V_APEC_DEPT
    AND "IDAlumno" = P_ID
    AND (TRIM("IDEscuela") <> P_PROGRAM
        OR "IDModalidadPostu" <> V_APEC_MOD_ADM)
    AND TRIM("Ingresante") = '1'
    AND TRIM("Renuncia") = '0';
    
    IF V_CONTADOR > 0 THEN
        SELECT DISTINCT TRIM("IDEscuela"), "IDModalidadPostu"
            INTO P_PROGRAM_NEW, V_APEC_MOD_ADM_NEW
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE TRIM("IDDependencia") = 'UCCI'
        AND TRIM("IDPerAcad") = V_APEC_TERM
        AND TRIM("IDEscuelaADM") = V_APEC_DEPT
        AND "IDAlumno" = P_ID
        AND (TRIM("IDEscuela") <> P_PROGRAM
            OR "IDModalidadPostu" <> V_APEC_MOD_ADM)
        AND TRIM("Ingresante") = '1'
        AND TRIM("Renuncia") = '0';
        
        IF P_PROGRAM <> P_PROGRAM_NEW THEN
            -- >> DESC PROGRAM NEW
            SELECT SMRPRLE_PROGRAM_DESC
            INTO P_PROGRAM_NEW_DESC
            FROM SMRPRLE
            WHERE SMRPRLE_PROGRAM = P_PROGRAM_NEW;
            
            V_FLAG_PROGRAM := 1;
        ELSE
            -- >> DESC PROGRAM
            SELECT SMRPRLE_PROGRAM_DESC
            INTO P_PROGRAM_NEW_DESC
            FROM SMRPRLE
            WHERE SMRPRLE_PROGRAM = P_PROGRAM;
        END IF;
        
        IF V_APEC_MOD_ADM_NEW <> V_APEC_MOD_ADM THEN
            -- >> MODALIDAD DE ADMISION NUEVO
            SELECT "IDModRecruiter"
                INTO P_MOD_ADM_NEW
            FROM dbo.tblModalidadPostuRecruiter@BDUCCI.CONTINENTAL.EDU.PE
            WHERE "IDModalidadPostu" = V_APEC_MOD_ADM_NEW
            AND "departament" = V_APEC_DEPT;
            
            -- >> DESC MODALIDAD DE ADMISION
            SELECT STVADMT_DESC
                INTO P_MOD_ADM_NEW_DESC
            FROM STVADMT
            WHERE STVADMT_CODE = P_MOD_ADM_NEW;
            
            V_FLAG_MOD_ADM := 1;
        ELSE
            P_MOD_ADM_NEW := P_MOD_ADM;
            P_MOD_ADM_NEW_DESC := P_MOD_ADM_DESC;
        END IF;
        
        -- >> VALIDACIÓN DEL CAMBIO
        IF V_FLAG_MOD_ADM = 1 AND V_FLAG_PROGRAM = 0 THEN
            P_MESSAGE := 'Cambio de modalidad de admisión';
            P_CAMBIO_ADM := 'MA';
        ELSIF V_FLAG_MOD_ADM = 0 AND V_FLAG_PROGRAM = 1 THEN
            IF P_PROGRAM <> '502' AND P_PROGRAM_NEW = '502' THEN
                P_MESSAGE := 'ERROR! El estudiante no puede cambiarse a la carrera de Medicina Humana. Por favor, verificar el cambio en RECRUITER.';
                P_CAMBIO_ADM := 'NO';
                P_MOD_ADM_NEW := '-';
                P_MOD_ADM_NEW_DESC := '-';
                P_PROGRAM_NEW := '-';
                P_PROGRAM_NEW_DESC := '-';
            ELSIF P_PROGRAM = '506' OR P_PROGRAM = '510' THEN
                P_MESSAGE := 'ERROR! El ingresante de la Segunda Especialidad Ortodoncia y Ortopedia Maxilar o Segunda Especialidad Odontopediatría no pueden usar este flujo. Por favor, verificar el cambio en RECRUITER.';
                P_CAMBIO_ADM := 'NO';
                P_MOD_ADM_NEW := '-';
                P_MOD_ADM_NEW_DESC := '-';
                P_PROGRAM_NEW := '-';
                P_PROGRAM_NEW_DESC := '-';
            ELSE
                P_MESSAGE := 'Cambio de carrera';
                P_CAMBIO_ADM := 'CA';
            END IF;
        ELSIF V_FLAG_MOD_ADM = 1 AND V_FLAG_PROGRAM = 1 THEN
            IF P_PROGRAM <> '502' AND P_PROGRAM_NEW = '502' THEN
                P_MESSAGE := 'ERROR! El estudiante no puede cambiarse a la carrera de Medicina Humana. Por favor, verificar el caso.';
                P_CAMBIO_ADM := 'NO';
                P_MOD_ADM_NEW := '-';
                P_MOD_ADM_NEW_DESC := '-';
                P_PROGRAM_NEW := '-';
                P_PROGRAM_NEW_DESC := '-';
            ELSIF P_PROGRAM = '506' OR P_PROGRAM = '510' THEN
                P_MESSAGE := 'ERROR! El ingresante de la Segunda Especialidad Ortodoncia y Ortopedia Maxilar o Segunda Especialidad Odontopediatría no pueden usar este flujo. Por favor, verificar el caso.';
                P_CAMBIO_ADM := 'NO';
                P_MOD_ADM_NEW := '-';
                P_MOD_ADM_NEW_DESC := '-';
                P_PROGRAM_NEW := '-';
                P_PROGRAM_NEW_DESC := '-';
            ELSE
                P_MESSAGE := 'Cambio de modalidad de admision y carrera';
                P_CAMBIO_ADM := 'MC';
            END IF;
        ELSE
            P_MESSAGE := 'ERROR! El estudiante no tiene registrado su cambio de carrera y/o modalidad de admisión. Por favor, verificar el cambio en RECRUITER.';
            P_CAMBIO_ADM := 'NO';
            P_MOD_ADM_NEW := '-';
            P_MOD_ADM_NEW_DESC := '-';
            P_PROGRAM_NEW := '-';
            P_PROGRAM_NEW_DESC := '-';
        END IF;
    ELSE
        P_MESSAGE := 'ERROR! El estudiante no tiene registrado su cambio de carrera y/o modalidad de admisión. Por favor, verificar su cambio en RECRUITER.';
        P_CAMBIO_ADM := 'NO';
        P_MOD_ADM_NEW := '-';
        P_MOD_ADM_NEW_DESC := '-';
        P_PROGRAM_NEW := '-';
        P_PROGRAM_NEW_DESC := '-';
    END IF;

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_GET_CAMBIO_ADM;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VALIDAR_CONVA (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
    P_STATUS_CONVA      OUT VARCHAR2,
    P_MESSAGE           OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_VALIDAR_CONVA (P_PIDM, P_TERM_CODE, P_STATUS_CONVA, P_MESSAGE);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VALIDAR_CONVA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_MATRICULA (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_STATUS_MAT      OUT VARCHAR2,
    P_MESSAGE         OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_VERIFICAR_MATRICULA (P_PIDM, P_TERM_CODE, P_STATUS_MAT, P_MESSAGE);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_MATRICULA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_ELIMINAR_NRC (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_MESSAGE         OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_ELIMINAR_NRC (P_PIDM, P_TERM_CODE, P_MESSAGE);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_ELIMINAR_NRC;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_CAMBIO_PLAN (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_DEPT_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
    P_MOD_ADM_NEW       IN VARCHAR2,
    P_CATALOGO          IN VARCHAR2,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_CAMBIO_PLAN       OUT VARCHAR2,
    P_CATALOGO_NEW      OUT VARCHAR2,
    P_ATTS_CODE_NEW     OUT VARCHAR2,
    P_MESSAGE           OUT VARCHAR2
)
AS
    V_FLAG_CATALOGO     NUMBER := 0;
	V_APEC_TERM         VARCHAR2(10);
    V_APEC_DEPT         VARCHAR2(10);
    V_APEC_MOD_ADM_NEW  VARCHAR(10);
    
    V_CAMP_CODE         VARCHAR(10);
    
BEGIN

-- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
    SELECT SORLCUR_CAMP_CODE
        INTO V_CAMP_CODE
    FROM (
        SELECT SORLCUR_CAMP_CODE
        FROM SORLCUR
        INNER JOIN SORLFOS ON
            SORLCUR_PIDM = SORLFOS_PIDM 
            AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
        WHERE SORLCUR_PIDM = P_PIDM
            AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
            AND SORLCUR_CACT_CODE = 'ACTIVE' 
            AND SORLCUR_CURRENT_CDE = 'Y'
        ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
    ) WHERE ROWNUM <= 1;

    -- PERIODO
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;
    
    -- DEPARTAMENTO
    SELECT CZRDEPT_DEPT_BDUCCI
    INTO V_APEC_DEPT
    FROM CZRDEPT 
    WHERE CZRDEPT_CODE = P_DEPT_CODE;

    -- MODALIDAD DE ADMISION
    SELECT "IDModalidadPostu"
        INTO V_APEC_MOD_ADM_NEW
    FROM dbo.tblModalidadPostuRecruiter@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "IDModRecruiter" = P_MOD_ADM_NEW
    AND "departament" = V_APEC_DEPT;
    
    -- OBTENER SI LA MODALIDAD ES DEL PLAN NUEVO O NO
    SELECT "PrimerCiclo"
        INTO V_FLAG_CATALOGO
    FROM tblModalidadPostu@BDUCCI.CONTINENTAL.EDU.PE
    WHERE TRIM("IDDependencia") = 'UCCI'
    AND TRIM("IDPerAcad") = V_APEC_TERM
    AND TRIM("IDEscuelaADM") = V_APEC_DEPT
    AND "IDModalidadPostu" = V_APEC_MOD_ADM_NEW;
    
    -- COMO NO HAY PLAN ANTIGUO EN LIMA, EN ELLOS SIEMPRE ESTARAN EN EL PLAN 2018
    IF V_CAMP_CODE = 'F02' THEN
        V_FLAG_CATALOGO := 1;
    END IF;
    
    --VALIDAR Y OBTENER LOS NUEVOS DATOS DE CAMBIO
    IF V_FLAG_CATALOGO = 0 THEN
        IF P_CATALOGO < '201810' THEN
            P_CAMBIO_PLAN := 'FALSE';
            P_CATALOGO_NEW := P_CATALOGO;
            P_MESSAGE := 'El estudiante no cambia de plan de estudios y se mantiene en el plan 2015.';
        ELSE
            P_CAMBIO_PLAN := 'TRUE';
            P_CATALOGO_NEW := '201710';
            P_MESSAGE := 'El estudiante cambia de plan de estudios 2015 y su periodo de catalogo ahora sera el 201710.';
        END IF;
        P_ATTS_CODE_NEW := 'P015';
    ELSE
        -- SI EL PLAN ES DESPUES DEL 2018
        IF P_CATALOGO > '201800' THEN
            P_CAMBIO_PLAN := 'FALSE';
            P_CATALOGO_NEW := P_CATALOGO;
            P_MESSAGE := 'El estudiante no cambia de plan de estudios y se mantiene en el plan 2018.';
        ELSE
            P_CAMBIO_PLAN := 'TRUE';
            P_CATALOGO_NEW := P_TERM_CODE;
            P_MESSAGE := 'El estudiante cambia de plan de estudios al 2018 y su periodo de catalogo ahora sera el ' || P_TERM_CODE || '.';
        END IF;
        P_ATTS_CODE_NEW := 'P018';
    END IF;

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VERIFICAR_CAMBIO_PLAN;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_CAMBIAR_ADM (
    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_CAMBIO_ADM            IN VARCHAR2,
    P_PROGRAM_NEW           IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_CATALOGO_NEW          IN VARCHAR2,
    P_ATTS_CODE             IN VARCHAR2,
    P_MOD_ADM_NEW           IN VARCHAR2,
    P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
    P_MESSAGE               OUT VARCHAR2
)
AS
    V_SARADAP_APPL_NO       SARADAP.SARADAP_APPL_NO%TYPE;
    V_ATRIB                 SGRSATT.SGRSATT_ATTS_CODE%TYPE;
BEGIN
    IF P_CAMBIO_ADM = 'CA' THEN
        P_SET_CAMBIAR_SAAADMS(P_PIDM, P_PROGRAM_NEW, P_DEPT_CODE, P_CAMP_CODE, P_TERM_CODE, P_CATALOGO_NEW, P_MOD_ADM_NEW, P_MESSAGE);

        P_SET_CAMBIAR_SGASTDN(P_PIDM, P_PROGRAM_NEW, P_DEPT_CODE, P_CAMP_CODE, P_TERM_CODE, P_CATALOGO_NEW, P_MOD_ADM_NEW, P_CAMBIO_ADM, P_MESSAGE);
    ELSE
        -- #######################################################################
        -- Obtener registro maximo del SARADAP
        SELECT MAX(SARADAP_APPL_NO)
            INTO V_SARADAP_APPL_NO
        FROM SARADAP
        WHERE SARADAP_PIDM = P_PIDM
        AND SARADAP_TERM_CODE_ENTRY = P_TERM_CODE
        AND SARADAP_CAMP_CODE = P_CAMP_CODE
        AND SARADAP_DEPT_CODE = P_DEPT_CODE
        AND SARADAP_LEVL_CODE = 'PG';
        
        -- Actualización del SARADAP
        UPDATE SARADAP
            SET SARADAP_ADMT_CODE = P_MOD_ADM_NEW
        WHERE SARADAP_PIDM = P_PIDM
        AND SARADAP_TERM_CODE_ENTRY = P_TERM_CODE
        AND SARADAP_CAMP_CODE = P_CAMP_CODE
        AND SARADAP_DEPT_CODE = P_DEPT_CODE
        AND SARADAP_LEVL_CODE = 'PG'
        AND SARADAP_APPL_NO = V_SARADAP_APPL_NO;
        
        SELECT SGRSATT_ATTS_CODE
            INTO V_ATRIB
        FROM (
            SELECT SGRSATT_ATTS_CODE 
            FROM SGRSATT
            WHERE SGRSATT_PIDM = P_PIDM
                AND SUBSTR(SGRSATT_ATTS_CODE,1,2) = 'P0'
            ORDER BY SGRSATT_TERM_CODE_EFF DESC
        )WHERE ROWNUM = 1;
        
        P_SET_CAMBIAR_SAAADMS(P_PIDM, P_PROGRAM_NEW, P_DEPT_CODE, P_CAMP_CODE, P_TERM_CODE, P_CATALOGO_NEW, P_MOD_ADM_NEW, P_MESSAGE);
        
        P_SET_CAMBIAR_SGASTDN(P_PIDM, P_PROGRAM_NEW, P_DEPT_CODE, P_CAMP_CODE, P_TERM_CODE, P_CATALOGO_NEW, P_MOD_ADM_NEW, P_CAMBIO_ADM, P_MESSAGE);
        
        BWZKPSPG.P_SET_ATRIBUTO_PLAN(P_PIDM, P_TERM_CODE, P_ATTS_CODE, P_MESSAGE);
    END IF;
    
    COMMIT;
    
EXCEPTION
WHEN OTHERS THEN
    ROLLBACK;
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CAMBIAR_ADM;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_CURSOS_ADM (
    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
    P_PROGRAM_NEW           IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
    P_CATALOGO_NEW          IN VARCHAR2,    
    P_MESSAGE               OUT VARCHAR2
)
AS
    V_SARADAP_APPL_NO       SARADAP.SARADAP_APPL_NO%TYPE;
    V_SARADAP_APPL_DATE     SARADAP.SARADAP_APPL_DATE%TYPE;
    V_COLL_CODE             SOBCURR.SOBCURR_COLL_CODE%TYPE;
BEGIN
    
    -- Obtener registro maximo del SARADAP
    SELECT MAX(SARADAP_APPL_NO)
        INTO V_SARADAP_APPL_NO
    FROM SARADAP
    WHERE SARADAP_PIDM = P_PIDM
        AND SARADAP_TERM_CODE_ENTRY = P_TERM_CODE
        AND SARADAP_CAMP_CODE = P_CAMP_CODE
        AND SARADAP_DEPT_CODE = P_DEPT_CODE
        AND SARADAP_LEVL_CODE = 'PG';
    
    -- Actualización del SARADAP
    SELECT SARADAP_APPL_DATE
        INTO V_SARADAP_APPL_DATE
    FROM SARADAP
    WHERE SARADAP_PIDM = P_PIDM
        AND SARADAP_TERM_CODE_ENTRY = P_TERM_CODE
        AND SARADAP_CAMP_CODE = P_CAMP_CODE
        AND SARADAP_DEPT_CODE = P_DEPT_CODE
        AND SARADAP_LEVL_CODE = 'PG'
        AND SARADAP_APPL_NO = V_SARADAP_APPL_NO;
            
    -- Get Reglas de Curriculum (CURR_RULE)
    SELECT SOBCURR_COLL_CODE
        INTO V_COLL_CODE
    FROM (
        SELECT *
        FROM SOBCURR
        INNER JOIN (
            SELECT  SOBCURR_CURR_RULE CURR_RULE, SOBCURR_CAMP_CODE CAMP_CODE, SOBCURR_COLL_CODE COLL_CODE, 
                    SOBCURR_DEGC_CODE DEGC_CODE, SOBCURR_PROGRAM CPROGRAM, MAX(SOBCURR_TERM_CODE_INIT) TERM_CODE_INIT
            FROM SOBCURR 
            WHERE SOBCURR_LEVL_CODE = 'PG' 
            GROUP BY SOBCURR_CURR_RULE, SOBCURR_CAMP_CODE, SOBCURR_COLL_CODE, SOBCURR_DEGC_CODE, SOBCURR_PROGRAM
        )ON SOBCURR_CURR_RULE = CURR_RULE
            AND SOBCURR_CAMP_CODE = CAMP_CODE
            AND SOBCURR_COLL_CODE = COLL_CODE
            AND SOBCURR_DEGC_CODE = DEGC_CODE
            AND SOBCURR_PROGRAM = CPROGRAM
            AND SOBCURR_TERM_CODE_INIT = TERM_CODE_INIT
    ) 
    INNER JOIN (
        SELECT * 
        FROM SORCMJR
        INNER JOIN (
            SELECT SORCMJR_CURR_RULE CURR_RULE, SORCMJR_MAJR_CODE MAJR_CODE, SORCMJR_DEPT_CODE DEPT_CODE, MAX(SORCMJR_TERM_CODE_EFF) TERM_CODE_EFF
            FROM SORCMJR 
            GROUP BY SORCMJR_CURR_RULE, SORCMJR_MAJR_CODE, SORCMJR_DEPT_CODE
        )ON SORCMJR_CURR_RULE = CURR_RULE
            AND SORCMJR_MAJR_CODE = MAJR_CODE
            AND SORCMJR_DEPT_CODE = DEPT_CODE
            AND SORCMJR_TERM_CODE_EFF = TERM_CODE_EFF
    )ON SOBCURR_CURR_RULE = SORCMJR_CURR_RULE
    WHERE SOBCURR_CAMP_CODE = P_CAMP_CODE
        AND SOBCURR_LEVL_CODE = 'PG'
        AND SORCMJR_DEPT_CODE = P_DEPT_CODE
        AND SOBCURR_PROGRAM = P_PROGRAM_NEW;

    IF P_CATALOGO_NEW > '201800' THEN
        SZKUADM.P_INSERT_RM_RV(P_PIDM, P_TERM_CODE, V_SARADAP_APPL_DATE, V_COLL_CODE);
        P_MESSAGE := 'Se registro los cursos respectivos al plan de estudios 2018';
    ELSE
        SZKUADM.P_DELETE_HISTORY(P_PIDM,P_TERM_CODE);
        P_MESSAGE := 'Se eliminaron los registros respectivos debido al cambio al plan de estudios 2015';
    END IF;
        
    COMMIT;
    
EXCEPTION
WHEN OTHERS THEN
    ROLLBACK;
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_CURSOS_ADM;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_EXECCAPP (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_MESSAGE           OUT VARCHAR2
)
AS
    -- @PARAMETERS
    LV_OUT            VARCHAR2(4000);
    P_EXCEPTION       EXCEPTION;
BEGIN
    --EJECUCIÓN CAPP
    SZKECAP.P_EXEC_CAPP(P_PIDM,P_PROGRAM_NEW,P_TERM_CODE,LV_OUT);
    
    IF (LV_OUT <> '0') THEN
        P_MESSAGE := LV_OUT;
        RAISE P_EXCEPTION;
    END IF;
  
EXCEPTION
WHEN P_EXCEPTION THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_EXECCAPP;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_EXECPROY (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_MESSAGE           OUT VARCHAR2
)
AS
    -- @PARAMETERS
    LV_OUT            VARCHAR2(4000);
    P_EXCEPTION       EXCEPTION;
BEGIN
    --EJECUCIÓN PROY
    SZKECAP.P_EXEC_PROY(P_PIDM,P_PROGRAM_NEW,P_TERM_CODE,LV_OUT);
    
    IF (LV_OUT <> '0') THEN
        P_MESSAGE := LV_OUT;
        RAISE P_EXCEPTION;
    END IF;
  
EXCEPTION
WHEN P_EXCEPTION THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_EXECPROY;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_PAGOS_CTA (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE			IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_PROGRAM_NEW       IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_CAMBIO_ADM        IN VARCHAR2,
    P_STATUS_CTA        OUT VARCHAR2,
    P_CTA_DESC			OUT VARCHAR2
)
AS
    V_ERROR             EXCEPTION;
    V_CONTADOR          NUMBER;
    V_ATTS_CODE         SGRSATT.SGRSATT_ATTS_CODE%TYPE;
    V_PERCAT            SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE;
    V_NEW_PLAN          VARCHAR2(10);

BEGIN

    -- ATRIBUTO DE PLAN
    SELECT SGRSATT_ATTS_CODE
        INTO V_ATTS_CODE
    FROM (
        SELECT SGRSATT_ATTS_CODE 
        FROM SGRSATT
        WHERE SGRSATT_PIDM = P_PIDM
            AND SUBSTR(SGRSATT_ATTS_CODE,1,2) = 'P0'
            ORDER BY SGRSATT_TERM_CODE_EFF DESC
    )WHERE ROWNUM = 1;

    -- PERIODO DE CATALOGO DE ESTUDIOS
    SELECT SORLCUR_TERM_CODE_CTLG 
        INTO V_PERCAT
    FROM( 
        SELECT SORLCUR_TERM_CODE_CTLG 
        FROM SORLCUR
        WHERE SORLCUR_PIDM = P_PIDM
            AND SORLCUR_LMOD_CODE = 'LEARNER'
            AND SORLCUR_ROLL_IND = 'Y'
            AND SORLCUR_CACT_CODE = 'ACTIVE'
            AND SORLCUR_TERM_CODE_END IS NULL
        ORDER BY SORLCUR_SEQNO DESC
    ) WHERE ROWNUM = 1;
    
    --VALIDACION DE PERIODO DE CATALOGO CON EL ATRIBUTO DE PLAN
    IF V_ATTS_CODE = 'P018' AND V_PERCAT > '201800' THEN
        V_NEW_PLAN := 'TRUE';
    ELSIF V_ATTS_CODE = 'P015' AND V_PERCAT > '201500' AND V_PERCAT < '201810' THEN
        V_NEW_PLAN := 'FALSE';
    ELSIF V_ATTS_CODE = 'P002' AND V_PERCAT < '201510' THEN
        V_NEW_PLAN := 'FALSE';
    ELSIF V_ATTS_CODE = 'P003' AND V_PERCAT < '201400' AND P_DEPT_CODE = 'UPGT' THEN
        V_NEW_PLAN := 'FALSE';
    ELSE
        RAISE V_ERROR;
    END IF;
    
    --LLAMA AL SP DEL PAQUETE GENERICO
    BWZKPSPG.P_VERIFICAR_PAGOS_CTA_DUP (P_PIDM, P_ID, P_DEPT_CODE, P_CAMP_CODE, P_TERM_CODE, P_PROGRAM_NEW, V_NEW_PLAN, P_CAMBIO_ADM, P_STATUS_CTA, P_CTA_DESC);

EXCEPTION
WHEN V_ERROR THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| 'El periodo de catálogo no coincide con el atributo de plan de estudios');
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_PAGOS_CTA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_BENEFICIO_PREVIO (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE			IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_CAMBIO_ADM        IN VARCHAR2,
    P_STATUS_BEN        OUT VARCHAR2,
    P_MESSAGE			OUT VARCHAR2
)
AS
BEGIN

    --LLAMA AL SP DEL PAQUETE GENERICO
    BWZKPSPG.P_VERIFICAR_BENEFICIO_PREVIO (P_PIDM, P_ID, P_DEPT_CODE, P_CAMP_CODE, P_TERM_CODE, P_CAMBIO_ADM, P_STATUS_BEN, P_MESSAGE);
    
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_BENEFICIO_PREVIO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_REGISTRAR_CAMBIO_ADM (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_CAMBIO_ADM        IN VARCHAR2,
    P_MOD_ADM           IN VARCHAR2,
    P_MOD_ADM_NEW       IN VARCHAR2,
    P_PROGRAM           IN VARCHAR2,
    P_PROGRAM_NEW       IN VARCHAR2,
    P_MESSAGE			OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_REGISTRAR_CAMBIO_ADM (P_PIDM, P_TERM_CODE, P_CAMBIO_ADM, '-', '-', '-', '-', P_MOD_ADM, P_MOD_ADM_NEW, P_PROGRAM, P_PROGRAM_NEW, P_MESSAGE);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_REGISTRAR_CAMBIO_ADM;

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--        

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_CAMBIAR_SGASTDN ( 
    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PROGRAM_NEW           IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
    P_CATALOGO_NEW          IN VARCHAR2,
    P_MOD_ADM_NEW           IN VARCHAR2,
    P_CAMBIO_ADM            IN VARCHAR2,
    P_MESSAGE               OUT VARCHAR2
)
AS
    V_TERM_CODE_LAST          SFBETRM.SFBETRM_TERM_CODE%TYPE;
    V_CURR_RULE               SOBCURR.SOBCURR_CURR_RULE%TYPE;
    V_COLL_CODE               SOBCURR.SOBCURR_COLL_CODE%TYPE;
    V_DEGC_CODE               SOBCURR.SOBCURR_DEGC_CODE%TYPE;
    V_CMJR_RULE               SORCMJR.SORCMJR_CMJR_RULE%TYPE;
    V_INDICADOR               NUMBER;
    V_MESSAGE                 EXCEPTION;
    
    V_ATTS_CODE               STVATTS.STVATTS_CODE%TYPE;      -------- COD atributos
        
    /*----------------------------------------------------------------------------------
    -- INSERTAR: Plan Estudio(SGRSTSP)   -- No confundir con PERIODO CATALOGO.
          - Nuevo Registro como Evidencia que alumno realizo TRAMITE
    */
    CURSOR C_SGRSTSP IS
        SELECT *
        FROM (
            --Ultimo registro cercano al P_TERM_CODE
            SELECT *
            FROM SGRSTSP 
            WHERE SGRSTSP_PIDM = P_PIDM
                AND SGRSTSP_TERM_CODE_EFF <= P_TERM_CODE
            ORDER BY SGRSTSP_KEY_SEQNO DESC
        ) WHERE ROWNUM = 1;
    
    -- AGREGAR NUEVO REGISTRO
    CURSOR C_SGBSTDN IS
        SELECT *
        FROM (
            SELECT *
            FROM SGBSTDN
            WHERE SGBSTDN_PIDM = P_PIDM
            ORDER BY SGBSTDN_TERM_CODE_EFF DESC
         ) WHERE ROWNUM = 1
         AND NOT EXISTS (
            SELECT *
            FROM SGBSTDN
            WHERE SGBSTDN_PIDM = P_PIDM
                AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE
         );
    
    -- AGREGAR NUEVO REGISTRO
    CURSOR C_SORLCUR IS
        SELECT *
        FROM (
            SELECT *
            FROM SORLCUR 
            WHERE SORLCUR_PIDM = P_PIDM
                AND SORLCUR_LMOD_CODE = 'LEARNER'
                AND SORLCUR_CURRENT_CDE = 'Y' --------------------- CURRENT CURRICULUM
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
        )WHERE ROWNUM = 1;
    
    V_SGRSTSP_KEY_SEQNO         SGRSTSP.SGRSTSP_KEY_SEQNO%TYPE;
    V_SGRSTSP_KEY_SEQNO_NEW     SGRSTSP.SGRSTSP_KEY_SEQNO%TYPE;
    V_SGRSTSP_REC               SGRSTSP%ROWTYPE;
    V_SGBSTDN_REC               SGBSTDN%ROWTYPE;
    V_SORLCUR_REC               SORLCUR%ROWTYPE;
    V_SORLCUR_SEQNO_OLD         SORLCUR.SORLCUR_SEQNO%TYPE;
    V_SORLCUR_SEQNO_INC         SORLCUR.SORLCUR_SEQNO%TYPE;
    V_SORLCUR_SEQNO_NEW         SORLCUR.SORLCUR_SEQNO%TYPE;
    
    V_INDICADOR             NUMBER;
    
BEGIN
          
    /* #######################################################################
    -- INSERTAR: Plan Estudio(SGRSTSP)   -- No confundir con PERIODO CATALOGO.
      - Nuevo Registro como Evidencia que alumno realizo TRAMITE
    */
    OPEN C_SGRSTSP;
    LOOP
        FETCH C_SGRSTSP INTO V_SGRSTSP_REC;
        EXIT WHEN C_SGRSTSP%NOTFOUND;
        
        -- GET SGRSTSP_KEY_SEQNO NUEVO
        SELECT SGRSTSP_KEY_SEQNO
            INTO V_SGRSTSP_KEY_SEQNO
        FROM (
            SELECT SGRSTSP_KEY_SEQNO
            FROM SGRSTSP 
            WHERE SGRSTSP_PIDM = P_PIDM
            ORDER BY SGRSTSP_KEY_SEQNO DESC
            ) WHERE ROWNUM = 1;
            
        -- GET SGRSTSP_KEY_SEQNO NUEVO
        SELECT SGRSTSP_KEY_SEQNO + 1
            INTO V_SGRSTSP_KEY_SEQNO_NEW
        FROM (
            SELECT SGRSTSP_KEY_SEQNO
            FROM SGRSTSP 
            WHERE SGRSTSP_PIDM = P_PIDM
            ORDER BY SGRSTSP_KEY_SEQNO DESC
            ) WHERE ROWNUM = 1;

        IF P_CAMBIO_ADM <> 'MA' THEN
            INSERT INTO SGRSTSP (
                  SGRSTSP_PIDM,       SGRSTSP_TERM_CODE_EFF,  SGRSTSP_KEY_SEQNO,
                  SGRSTSP_STSP_CODE,  SGRSTSP_ACTIVITY_DATE,  SGRSTSP_DATA_ORIGIN,
                  SGRSTSP_USER_ID,    SGRSTSP_FULL_PART_IND,  SGRSTSP_SESS_CODE,
                  SGRSTSP_RESD_CODE,  SGRSTSP_ORSN_CODE,      SGRSTSP_PRAC_CODE,
                  SGRSTSP_CAPL_CODE,  SGRSTSP_EDLV_CODE,      SGRSTSP_INCM_CODE,
                  SGRSTSP_EMEX_CODE,  SGRSTSP_APRN_CODE,      SGRSTSP_TRCN_CODE,
                  SGRSTSP_GAIN_CODE,  SGRSTSP_VOED_CODE,      SGRSTSP_BLCK_CODE,
                  SGRSTSP_EGOL_CODE,  SGRSTSP_BSKL_CODE,      SGRSTSP_ASTD_CODE,
                  SGRSTSP_PREV_CODE,  SGRSTSP_CAST_CODE ) 
            SELECT 
                  V_SGRSTSP_REC.SGRSTSP_PIDM,       P_TERM_CODE,                          V_SGRSTSP_KEY_SEQNO_NEW,
                  'AS',                             SYSDATE,                              'WorkFlow',
                  USER,                             V_SGRSTSP_REC.SGRSTSP_FULL_PART_IND,  V_SGRSTSP_REC.SGRSTSP_SESS_CODE,
                  V_SGRSTSP_REC.SGRSTSP_RESD_CODE,  V_SGRSTSP_REC.SGRSTSP_ORSN_CODE,      V_SGRSTSP_REC.SGRSTSP_PRAC_CODE,
                  V_SGRSTSP_REC.SGRSTSP_CAPL_CODE,  V_SGRSTSP_REC.SGRSTSP_EDLV_CODE,      V_SGRSTSP_REC.SGRSTSP_INCM_CODE,
                  V_SGRSTSP_REC.SGRSTSP_EMEX_CODE,  V_SGRSTSP_REC.SGRSTSP_APRN_CODE,      V_SGRSTSP_REC.SGRSTSP_TRCN_CODE,
                  V_SGRSTSP_REC.SGRSTSP_GAIN_CODE,  V_SGRSTSP_REC.SGRSTSP_VOED_CODE,      V_SGRSTSP_REC.SGRSTSP_BLCK_CODE,
                  V_SGRSTSP_REC.SGRSTSP_EGOL_CODE,  V_SGRSTSP_REC.SGRSTSP_BSKL_CODE,      V_SGRSTSP_REC.SGRSTSP_ASTD_CODE,
                  V_SGRSTSP_REC.SGRSTSP_PREV_CODE,  V_SGRSTSP_REC.SGRSTSP_CAST_CODE
            FROM DUAL;
        END IF;
        
    END LOOP;
    CLOSE C_SGRSTSP;
      
    -- #######################################################################
    -- Get Reglas de Curriculum (CURR_RULE)
    SELECT  SOBCURR_CURR_RULE,  SOBCURR_COLL_CODE,  SOBCURR_DEGC_CODE,  SORCMJR_CMJR_RULE
    INTO    V_CURR_RULE,        V_COLL_CODE,        V_DEGC_CODE,        V_CMJR_RULE
    FROM (
        SELECT *
        FROM SOBCURR
        INNER JOIN (
            SELECT  SOBCURR_CURR_RULE CURR_RULE, SOBCURR_CAMP_CODE CAMP_CODE, SOBCURR_COLL_CODE COLL_CODE, 
                    SOBCURR_DEGC_CODE DEGC_CODE, SOBCURR_PROGRAM CPROGRAM, MAX(SOBCURR_TERM_CODE_INIT) TERM_CODE_INIT
            FROM SOBCURR 
            WHERE SOBCURR_LEVL_CODE = 'PG' 
            GROUP BY SOBCURR_CURR_RULE, SOBCURR_CAMP_CODE, SOBCURR_COLL_CODE, SOBCURR_DEGC_CODE, SOBCURR_PROGRAM
        )ON SOBCURR_CURR_RULE = CURR_RULE
            AND SOBCURR_CAMP_CODE = CAMP_CODE
            AND SOBCURR_COLL_CODE = COLL_CODE
            AND SOBCURR_DEGC_CODE = DEGC_CODE
            AND SOBCURR_PROGRAM = CPROGRAM
            AND SOBCURR_TERM_CODE_INIT = TERM_CODE_INIT
    ) 
    INNER JOIN (
        SELECT *
        FROM SORCMJR
        INNER JOIN (
            SELECT SORCMJR_CURR_RULE CURR_RULE, SORCMJR_MAJR_CODE MAJR_CODE, SORCMJR_DEPT_CODE DEPT_CODE, MAX(SORCMJR_TERM_CODE_EFF) TERM_CODE_EFF
            FROM SORCMJR 
            GROUP BY SORCMJR_CURR_RULE, SORCMJR_MAJR_CODE, SORCMJR_DEPT_CODE
        )ON SORCMJR_CURR_RULE = CURR_RULE
            AND SORCMJR_MAJR_CODE = MAJR_CODE
            AND SORCMJR_DEPT_CODE = DEPT_CODE
            AND SORCMJR_TERM_CODE_EFF = TERM_CODE_EFF
    )
    ON SOBCURR_CURR_RULE = SORCMJR_CURR_RULE
    WHERE SOBCURR_CAMP_CODE = P_CAMP_CODE
        AND SOBCURR_LEVL_CODE = 'PG'
        AND SORCMJR_DEPT_CODE = P_DEPT_CODE
        AND SOBCURR_PROGRAM = P_PROGRAM_NEW;

    -- UPDATE SGASTDN
    UPDATE SGBSTDN
    SET  SGBSTDN_STST_CODE = 'AS'  ------------------ ESTADOS STVSTST : 'AS' - Activo
      ,SGBSTDN_COLL_CODE_1 = V_COLL_CODE
      ,SGBSTDN_DEGC_CODE_1 = V_DEGC_CODE
      ,SGBSTDN_MAJR_CODE_1 = P_PROGRAM_NEW
      ,SGBSTDN_ACTIVITY_DATE = SYSDATE
      ,SGBSTDN_PROGRAM_1 = P_PROGRAM_NEW
      ,SGBSTDN_TERM_CODE_CTLG_1 = P_CATALOGO_NEW
      ,SGBSTDN_CURR_RULE_1 = V_CURR_RULE
      ,SGBSTDN_CMJR_RULE_1_1 = V_CMJR_RULE
      ,SGBSTDN_DATA_ORIGIN = 'WorkFlow'
      ,SGBSTDN_USER_ID = USER
    WHERE SGBSTDN_PIDM = P_PIDM
        AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE;
    
    -- #######################################################################
    -- INSERT  ------> SGBSTDN -
    OPEN C_SGBSTDN;
    LOOP
        FETCH C_SGBSTDN INTO V_SGBSTDN_REC;
        EXIT WHEN C_SGBSTDN%NOTFOUND;
    
        INSERT INTO SGBSTDN (
                  SGBSTDN_PIDM,                 SGBSTDN_TERM_CODE_EFF,          SGBSTDN_STST_CODE,
                  SGBSTDN_LEVL_CODE,            SGBSTDN_STYP_CODE,              SGBSTDN_TERM_CODE_MATRIC,
                  SGBSTDN_TERM_CODE_ADMIT,      SGBSTDN_EXP_GRAD_DATE,          SGBSTDN_CAMP_CODE,
                  SGBSTDN_FULL_PART_IND,        SGBSTDN_SESS_CODE,              SGBSTDN_RESD_CODE,
                  SGBSTDN_COLL_CODE_1,          SGBSTDN_DEGC_CODE_1,            SGBSTDN_MAJR_CODE_1,
                  SGBSTDN_MAJR_CODE_MINR_1,     SGBSTDN_MAJR_CODE_MINR_1_2,     SGBSTDN_MAJR_CODE_CONC_1,
                  SGBSTDN_MAJR_CODE_CONC_1_2,   SGBSTDN_MAJR_CODE_CONC_1_3,     SGBSTDN_COLL_CODE_2,
                  SGBSTDN_DEGC_CODE_2,          SGBSTDN_MAJR_CODE_2,            SGBSTDN_MAJR_CODE_MINR_2,
                  SGBSTDN_MAJR_CODE_MINR_2_2,   SGBSTDN_MAJR_CODE_CONC_2,       SGBSTDN_MAJR_CODE_CONC_2_2,
                  SGBSTDN_MAJR_CODE_CONC_2_3,   SGBSTDN_ORSN_CODE,              SGBSTDN_PRAC_CODE,
                  SGBSTDN_ADVR_PIDM,            SGBSTDN_GRAD_CREDIT_APPR_IND,   SGBSTDN_CAPL_CODE,
                  SGBSTDN_LEAV_CODE,            SGBSTDN_LEAV_FROM_DATE,         SGBSTDN_LEAV_TO_DATE,
                  SGBSTDN_ASTD_CODE,            SGBSTDN_TERM_CODE_ASTD,         SGBSTDN_RATE_CODE,
                  SGBSTDN_ACTIVITY_DATE,        SGBSTDN_MAJR_CODE_1_2,          SGBSTDN_MAJR_CODE_2_2,
                  SGBSTDN_EDLV_CODE,            SGBSTDN_INCM_CODE,              SGBSTDN_ADMT_CODE,
                  SGBSTDN_EMEX_CODE,            SGBSTDN_APRN_CODE,              SGBSTDN_TRCN_CODE,
                  SGBSTDN_GAIN_CODE,            SGBSTDN_VOED_CODE,              SGBSTDN_BLCK_CODE,
                  SGBSTDN_TERM_CODE_GRAD,       SGBSTDN_ACYR_CODE,              SGBSTDN_DEPT_CODE,
                  SGBSTDN_SITE_CODE,            SGBSTDN_DEPT_CODE_2,            SGBSTDN_EGOL_CODE,
                  SGBSTDN_DEGC_CODE_DUAL,       SGBSTDN_LEVL_CODE_DUAL,         SGBSTDN_DEPT_CODE_DUAL,
                  SGBSTDN_COLL_CODE_DUAL,       SGBSTDN_MAJR_CODE_DUAL,         SGBSTDN_BSKL_CODE,
                  SGBSTDN_PRIM_ROLL_IND,        SGBSTDN_PROGRAM_1,              SGBSTDN_TERM_CODE_CTLG_1,
                  SGBSTDN_DEPT_CODE_1_2,        SGBSTDN_MAJR_CODE_CONC_121,     SGBSTDN_MAJR_CODE_CONC_122,
                  SGBSTDN_MAJR_CODE_CONC_123,   SGBSTDN_SECD_ROLL_IND,          SGBSTDN_TERM_CODE_ADMIT_2,
                  SGBSTDN_ADMT_CODE_2,          SGBSTDN_PROGRAM_2,              SGBSTDN_TERM_CODE_CTLG_2,
                  SGBSTDN_LEVL_CODE_2,          SGBSTDN_CAMP_CODE_2,            SGBSTDN_DEPT_CODE_2_2,
                  SGBSTDN_MAJR_CODE_CONC_221,   SGBSTDN_MAJR_CODE_CONC_222,     SGBSTDN_MAJR_CODE_CONC_223,
                  SGBSTDN_CURR_RULE_1,          SGBSTDN_CMJR_RULE_1_1,          SGBSTDN_CCON_RULE_11_1,
                  SGBSTDN_CCON_RULE_11_2,       SGBSTDN_CCON_RULE_11_3,         SGBSTDN_CMJR_RULE_1_2,
                  SGBSTDN_CCON_RULE_12_1,       SGBSTDN_CCON_RULE_12_2,         SGBSTDN_CCON_RULE_12_3,
                  SGBSTDN_CMNR_RULE_1_1,        SGBSTDN_CMNR_RULE_1_2,          SGBSTDN_CURR_RULE_2,
                  SGBSTDN_CMJR_RULE_2_1,        SGBSTDN_CCON_RULE_21_1,         SGBSTDN_CCON_RULE_21_2,
                  SGBSTDN_CCON_RULE_21_3,       SGBSTDN_CMJR_RULE_2_2,          SGBSTDN_CCON_RULE_22_1,
                  SGBSTDN_CCON_RULE_22_2,       SGBSTDN_CCON_RULE_22_3,         SGBSTDN_CMNR_RULE_2_1,
                  SGBSTDN_CMNR_RULE_2_2,        SGBSTDN_PREV_CODE,              SGBSTDN_TERM_CODE_PREV,
                  SGBSTDN_CAST_CODE,            SGBSTDN_TERM_CODE_CAST,         SGBSTDN_DATA_ORIGIN,
                  SGBSTDN_USER_ID,              SGBSTDN_SCPC_CODE ) 
        VALUES (  V_SGBSTDN_REC.SGBSTDN_PIDM,                 P_TERM_CODE,                                 'AS', /*AS-Activo*/ 
                  V_SGBSTDN_REC.SGBSTDN_LEVL_CODE,            V_SGBSTDN_REC.SGBSTDN_STYP_CODE,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_MATRIC,
                  V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT,      V_SGBSTDN_REC.SGBSTDN_EXP_GRAD_DATE,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE,
                  V_SGBSTDN_REC.SGBSTDN_FULL_PART_IND,        V_SGBSTDN_REC.SGBSTDN_SESS_CODE,              V_SGBSTDN_REC.SGBSTDN_RESD_CODE,
                  V_COLL_CODE,                                V_DEGC_CODE,                                  P_PROGRAM_NEW,
                  V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1_2,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1,
                  V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_3,     V_SGBSTDN_REC.SGBSTDN_COLL_CODE_2,
                  V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2,
                  V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_2,
                  V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_3,   V_SGBSTDN_REC.SGBSTDN_ORSN_CODE,              V_SGBSTDN_REC.SGBSTDN_PRAC_CODE,
                  V_SGBSTDN_REC.SGBSTDN_ADVR_PIDM,            V_SGBSTDN_REC.SGBSTDN_GRAD_CREDIT_APPR_IND,   V_SGBSTDN_REC.SGBSTDN_CAPL_CODE,
                  V_SGBSTDN_REC.SGBSTDN_LEAV_CODE,            V_SGBSTDN_REC.SGBSTDN_LEAV_FROM_DATE,         V_SGBSTDN_REC.SGBSTDN_LEAV_TO_DATE,
                  V_SGBSTDN_REC.SGBSTDN_ASTD_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ASTD,         V_SGBSTDN_REC.SGBSTDN_RATE_CODE,
                  SYSDATE,                                    V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2_2,
                  V_SGBSTDN_REC.SGBSTDN_EDLV_CODE,            V_SGBSTDN_REC.SGBSTDN_INCM_CODE,              P_MOD_ADM_NEW,
                  V_SGBSTDN_REC.SGBSTDN_EMEX_CODE,            V_SGBSTDN_REC.SGBSTDN_APRN_CODE,              V_SGBSTDN_REC.SGBSTDN_TRCN_CODE,
                  V_SGBSTDN_REC.SGBSTDN_GAIN_CODE,            V_SGBSTDN_REC.SGBSTDN_VOED_CODE,              V_SGBSTDN_REC.SGBSTDN_BLCK_CODE,
                  V_SGBSTDN_REC.SGBSTDN_TERM_CODE_GRAD,       V_SGBSTDN_REC.SGBSTDN_ACYR_CODE,              V_SGBSTDN_REC.SGBSTDN_DEPT_CODE,
                  V_SGBSTDN_REC.SGBSTDN_SITE_CODE,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2,            V_SGBSTDN_REC.SGBSTDN_EGOL_CODE,
                  V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_DUAL,
                  V_SGBSTDN_REC.SGBSTDN_COLL_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_BSKL_CODE,
                  V_SGBSTDN_REC.SGBSTDN_PRIM_ROLL_IND,        P_PROGRAM_NEW,                                P_CATALOGO_NEW,
                  V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_1_2,        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_121,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_122,
                  V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_123,   V_SGBSTDN_REC.SGBSTDN_SECD_ROLL_IND,          V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT_2,
                  V_SGBSTDN_REC.SGBSTDN_ADMT_CODE_2,          V_SGBSTDN_REC.SGBSTDN_PROGRAM_2,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CTLG_2,
                  V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_2,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE_2,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2_2,
                  V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_221,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_222,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_223,
                  V_CURR_RULE,                                V_CMJR_RULE,                                  V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_1,
                  V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_3,         V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_2,
                  V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_1,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_2,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_3,
                  V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_1,        V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_2,          V_SGBSTDN_REC.SGBSTDN_CURR_RULE_2,
                  V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_1,        V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_1,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_2,
                  V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_3,       V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_2,          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_1,
                  V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_3,         V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_1,
                  V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_2,        V_SGBSTDN_REC.SGBSTDN_PREV_CODE,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_PREV,
                  V_SGBSTDN_REC.SGBSTDN_CAST_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CAST,         'WorkFlow',
                  USER,                                       V_SGBSTDN_REC.SGBSTDN_SCPC_CODE );
    END LOOP;
    CLOSE C_SGBSTDN;
      
    -- #######################################################################
    -- INSERT  ------> SORLCUR -
    OPEN C_SORLCUR;
    LOOP
        FETCH C_SORLCUR INTO V_SORLCUR_REC;
        EXIT WHEN C_SORLCUR%NOTFOUND;    
        
        -- GET SORLCUR_SEQNO DEL REGISTRO INACTIVO 
        SELECT MAX(SORLCUR_SEQNO + 1)
            INTO V_SORLCUR_SEQNO_INC
        FROM SORLCUR 
        WHERE SORLCUR_PIDM = V_SORLCUR_REC.SORLCUR_PIDM;
        
        -- GET SORLCUR_SEQNO NUEVO 
        SELECT MAX(SORLCUR_SEQNO + 2)
            INTO V_SORLCUR_SEQNO_NEW
        FROM SORLCUR
        WHERE SORLCUR_PIDM = V_SORLCUR_REC.SORLCUR_PIDM;
        
        -- GET SORLCUR_SEQNO ANTERIOR 
        V_SORLCUR_SEQNO_OLD := V_SORLCUR_REC.SORLCUR_SEQNO;

        -- INACTIVE --- SORLCUR - (1 DE 2)
        INSERT INTO SORLCUR (
                  SORLCUR_PIDM,             SORLCUR_SEQNO,                SORLCUR_LMOD_CODE,
                  SORLCUR_TERM_CODE,        SORLCUR_KEY_SEQNO,            SORLCUR_PRIORITY_NO,
                  SORLCUR_ROLL_IND,         SORLCUR_CACT_CODE,            SORLCUR_USER_ID,
                  SORLCUR_DATA_ORIGIN,      SORLCUR_ACTIVITY_DATE,        SORLCUR_LEVL_CODE,
                  SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,            SORLCUR_TERM_CODE_CTLG,
                  SORLCUR_TERM_CODE_END,    SORLCUR_TERM_CODE_MATRIC,     SORLCUR_TERM_CODE_ADMIT,
                  SORLCUR_ADMT_CODE,        SORLCUR_CAMP_CODE,            SORLCUR_PROGRAM,
                  SORLCUR_START_DATE,       SORLCUR_END_DATE,             SORLCUR_CURR_RULE,
                  SORLCUR_ROLLED_SEQNO,     SORLCUR_STYP_CODE,            SORLCUR_RATE_CODE,
                  SORLCUR_LEAV_CODE,        SORLCUR_LEAV_FROM_DATE,       SORLCUR_LEAV_TO_DATE,
                  SORLCUR_EXP_GRAD_DATE,    SORLCUR_TERM_CODE_GRAD,       SORLCUR_ACYR_CODE,
                  SORLCUR_SITE_CODE,        SORLCUR_APPL_SEQNO,           SORLCUR_APPL_KEY_SEQNO,
                  SORLCUR_USER_ID_UPDATE,   SORLCUR_ACTIVITY_DATE_UPDATE, SORLCUR_GAPP_SEQNO,
                  SORLCUR_CURRENT_CDE ) 
        VALUES (  V_SORLCUR_REC.SORLCUR_PIDM,             V_SORLCUR_SEQNO_INC,                        V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                  P_TERM_CODE,                            V_SORLCUR_REC.SORLCUR_KEY_SEQNO,            V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                  V_SORLCUR_REC.SORLCUR_ROLL_IND,         'INACTIVE',/*SORLCUR_CACT_CODE*/            USER,
                  V_SORLCUR_REC.SORLCUR_DATA_ORIGIN,      SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                  V_SORLCUR_REC.SORLCUR_COLL_CODE,        V_SORLCUR_REC.SORLCUR_DEGC_CODE,            V_SORLCUR_REC.SORLCUR_TERM_CODE_CTLG,
                  P_TERM_CODE,                            V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                  V_SORLCUR_REC.SORLCUR_ADMT_CODE,        V_SORLCUR_REC.SORLCUR_CAMP_CODE,            V_SORLCUR_REC.SORLCUR_PROGRAM,
                  V_SORLCUR_REC.SORLCUR_START_DATE,       V_SORLCUR_REC.SORLCUR_END_DATE,             V_SORLCUR_REC.SORLCUR_CURR_RULE,
                  V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,     V_SORLCUR_REC.SORLCUR_STYP_CODE,            V_SORLCUR_REC.SORLCUR_RATE_CODE,
                  V_SORLCUR_REC.SORLCUR_LEAV_CODE,        V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                  V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,    V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                  V_SORLCUR_REC.SORLCUR_SITE_CODE,        V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                  USER,                                   SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                  NULL /*SORLCUR_CURRENT_CDE*/ );
        
        -- ACTIVE --- SORLCUR - (2 DE 2)
        INSERT INTO SORLCUR (
                  SORLCUR_PIDM,             SORLCUR_SEQNO,                SORLCUR_LMOD_CODE,
                  SORLCUR_TERM_CODE,        SORLCUR_KEY_SEQNO,            
                  SORLCUR_PRIORITY_NO,
                  SORLCUR_ROLL_IND,         SORLCUR_CACT_CODE,            SORLCUR_USER_ID,
                  SORLCUR_DATA_ORIGIN,      SORLCUR_ACTIVITY_DATE,        SORLCUR_LEVL_CODE,
                  SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,            SORLCUR_TERM_CODE_CTLG,
                  SORLCUR_TERM_CODE_END,    SORLCUR_TERM_CODE_MATRIC,     SORLCUR_TERM_CODE_ADMIT,
                  SORLCUR_ADMT_CODE,        SORLCUR_CAMP_CODE,            SORLCUR_PROGRAM,
                  SORLCUR_START_DATE,       SORLCUR_END_DATE,             SORLCUR_CURR_RULE,
                  SORLCUR_ROLLED_SEQNO,     SORLCUR_STYP_CODE,            SORLCUR_RATE_CODE,
                  SORLCUR_LEAV_CODE,        SORLCUR_LEAV_FROM_DATE,       SORLCUR_LEAV_TO_DATE,
                  SORLCUR_EXP_GRAD_DATE,    SORLCUR_TERM_CODE_GRAD,       SORLCUR_ACYR_CODE,
                  SORLCUR_SITE_CODE,        SORLCUR_APPL_SEQNO,           SORLCUR_APPL_KEY_SEQNO,
                  SORLCUR_USER_ID_UPDATE,   SORLCUR_ACTIVITY_DATE_UPDATE, SORLCUR_GAPP_SEQNO,
                  SORLCUR_CURRENT_CDE ) 
        VALUES (  V_SORLCUR_REC.SORLCUR_PIDM,             V_SORLCUR_SEQNO_NEW,                        V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                  P_TERM_CODE,                            CASE P_CAMBIO_ADM WHEN 'MA' THEN V_SORLCUR_REC.SORLCUR_KEY_SEQNO ELSE V_SGRSTSP_KEY_SEQNO_NEW END,/*SGRSTSP*/
                  V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                  V_SORLCUR_REC.SORLCUR_ROLL_IND,         V_SORLCUR_REC.SORLCUR_CACT_CODE,            USER,
                  'WorkFlow',                             SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                  V_COLL_CODE,                            V_DEGC_CODE,                                P_CATALOGO_NEW /*SORLCUR_TERM_CODE_CTLG*/,
                  NULL,                                   V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                  P_MOD_ADM_NEW,                          V_SORLCUR_REC.SORLCUR_CAMP_CODE,            P_PROGRAM_NEW,
                  V_SORLCUR_REC.SORLCUR_START_DATE,       V_SORLCUR_REC.SORLCUR_END_DATE,             V_CURR_RULE,
                  V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,     V_SORLCUR_REC.SORLCUR_STYP_CODE,            V_SORLCUR_REC.SORLCUR_RATE_CODE,
                  V_SORLCUR_REC.SORLCUR_LEAV_CODE,        V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                  V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,    V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                  V_SORLCUR_REC.SORLCUR_SITE_CODE,        V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                  USER,                                   SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                  V_SORLCUR_REC.SORLCUR_CURRENT_CDE );
        
        
        -- UPDATE TERM_CODE_END (vigencia curriculum del Registro ANTERIOR) 
        UPDATE SORLCUR 
            SET SORLCUR_TERM_CODE_END = P_TERM_CODE, SORLCUR_ACTIVITY_DATE_UPDATE = SYSDATE
        WHERE   SORLCUR_PIDM = P_PIDM 
        AND     SORLCUR_LMOD_CODE = 'LEARNER'
        AND     SORLCUR_SEQNO = V_SORLCUR_SEQNO_OLD;
        
        -- UPDATE SORLCUR_CURRENT_CDE(curriculum activo)  -- Solo 1 curricula activa por PERIODO
        UPDATE SORLCUR 
            SET   SORLCUR_CURRENT_CDE = NULL
        WHERE   SORLCUR_PIDM = P_PIDM 
        AND     SORLCUR_LMOD_CODE = 'LEARNER'
        AND     SORLCUR_TERM_CODE_END = P_TERM_CODE -- En caso el registro sea del mismo Periodo
        AND     SORLCUR_SEQNO = V_SORLCUR_SEQNO_OLD;    
      
    END LOOP;
    CLOSE C_SORLCUR;
      
    -- #######################################################################
    -- INSERT --- SORLFOS - (1 DE 2) 
    INSERT INTO SORLFOS (
        SORLFOS_PIDM,             SORLFOS_LCUR_SEQNO,         SORLFOS_SEQNO,
        SORLFOS_LFST_CODE,        SORLFOS_TERM_CODE,          SORLFOS_PRIORITY_NO,
        SORLFOS_CSTS_CODE,        SORLFOS_CACT_CODE,          SORLFOS_DATA_ORIGIN,
        SORLFOS_USER_ID,          SORLFOS_ACTIVITY_DATE,      SORLFOS_MAJR_CODE,
        SORLFOS_TERM_CODE_CTLG,   SORLFOS_TERM_CODE_END,      SORLFOS_DEPT_CODE,
        SORLFOS_MAJR_CODE_ATTACH, SORLFOS_LFOS_RULE,          SORLFOS_CONC_ATTACH_RULE,
        SORLFOS_START_DATE,       SORLFOS_END_DATE,           SORLFOS_TMST_CODE,
        SORLFOS_ROLLED_SEQNO,     SORLFOS_USER_ID_UPDATE,     SORLFOS_ACTIVITY_DATE_UPDATE,
        SORLFOS_CURRENT_CDE) 
    SELECT 
        V_SORLFOS_REC.SORLFOS_PIDM,             V_SORLCUR_SEQNO_INC,                      1,/*V_SORLFOS_REC.SORLFOS_SEQNO*/
        V_SORLFOS_REC.SORLFOS_LFST_CODE,        P_TERM_CODE,                              V_SORLFOS_REC.SORLFOS_PRIORITY_NO,
        'CHANGED'/*SORLFOS_CSTS_CODE*/,         'INACTIVE'/*SORLFOS_CACT_CODE*/,          V_SORLFOS_REC.SORLFOS_DATA_ORIGIN,
        USER,                                   SYSDATE,                                  V_SORLFOS_REC.SORLFOS_MAJR_CODE,
        V_SORLFOS_REC.SORLFOS_TERM_CODE_CTLG,   V_SORLFOS_REC.SORLFOS_TERM_CODE_END,      V_SORLFOS_REC.SORLFOS_DEPT_CODE,
        V_SORLFOS_REC.SORLFOS_MAJR_CODE_ATTACH, V_SORLFOS_REC.SORLFOS_LFOS_RULE,          V_SORLFOS_REC.SORLFOS_CONC_ATTACH_RULE,
        V_SORLFOS_REC.SORLFOS_START_DATE,       V_SORLFOS_REC.SORLFOS_END_DATE,           V_SORLFOS_REC.SORLFOS_TMST_CODE,
        V_SORLFOS_REC.SORLFOS_ROLLED_SEQNO,     USER,                                     SYSDATE,
        NULL /*SORLFOS_CURRENT_CDE*/
    FROM SORLFOS V_SORLFOS_REC
    WHERE SORLFOS_PIDM = P_PIDM
        AND SORLFOS_LCUR_SEQNO = V_SORLCUR_SEQNO_OLD
        AND ROWNUM = 1;
    
    -- INSERT --- SORLFOS - (2 DE 2)
    INSERT INTO SORLFOS (
        SORLFOS_PIDM,             SORLFOS_LCUR_SEQNO,         SORLFOS_SEQNO,
        SORLFOS_LFST_CODE,        SORLFOS_TERM_CODE,          SORLFOS_PRIORITY_NO,
        SORLFOS_CSTS_CODE,        SORLFOS_CACT_CODE,          SORLFOS_DATA_ORIGIN,
        SORLFOS_USER_ID,          SORLFOS_ACTIVITY_DATE,      SORLFOS_MAJR_CODE,
        SORLFOS_TERM_CODE_CTLG,   SORLFOS_TERM_CODE_END,      SORLFOS_DEPT_CODE,
        SORLFOS_MAJR_CODE_ATTACH, SORLFOS_LFOS_RULE,          SORLFOS_CONC_ATTACH_RULE,
        SORLFOS_START_DATE,       SORLFOS_END_DATE,           SORLFOS_TMST_CODE,
        SORLFOS_ROLLED_SEQNO,     SORLFOS_USER_ID_UPDATE,     SORLFOS_ACTIVITY_DATE_UPDATE,
        SORLFOS_CURRENT_CDE) 
    SELECT 
        V_SORLFOS_REC.SORLFOS_PIDM,                 V_SORLCUR_SEQNO_NEW,                      1,/*V_SORLFOS_REC.SORLFOS_SEQNO*/
        V_SORLFOS_REC.SORLFOS_LFST_CODE,            P_TERM_CODE,                              V_SORLFOS_REC.SORLFOS_PRIORITY_NO,
        V_SORLFOS_REC.SORLFOS_CSTS_CODE,            V_SORLFOS_REC.SORLFOS_CACT_CODE,          'WorkFlow',
        USER,                                       SYSDATE,                                  P_PROGRAM_NEW /*SORLFOS_MAJR_CODE*/,
        P_CATALOGO_NEW /*SORLFOS_TERM_CODE_CTLG*/,  V_SORLFOS_REC.SORLFOS_TERM_CODE_END,      V_SORLFOS_REC.SORLFOS_DEPT_CODE,
        V_SORLFOS_REC.SORLFOS_MAJR_CODE_ATTACH,     V_CMJR_RULE /*SORLFOS_LFOS_RULE*/,        V_SORLFOS_REC.SORLFOS_CONC_ATTACH_RULE,
        V_SORLFOS_REC.SORLFOS_START_DATE,           V_SORLFOS_REC.SORLFOS_END_DATE,           V_SORLFOS_REC.SORLFOS_TMST_CODE,
        V_SORLFOS_REC.SORLFOS_ROLLED_SEQNO,         USER,                                     SYSDATE,
        'Y'
    FROM SORLFOS V_SORLFOS_REC
    WHERE SORLFOS_PIDM = P_PIDM
        AND SORLFOS_LCUR_SEQNO = V_SORLCUR_SEQNO_OLD
        AND ROWNUM = 1;
    
    -- UPDATE SORLFOS_CURRENT_CDE(curriculum activo) PARA registros del mismo PERIODO.
    UPDATE SORLFOS 
        SET SORLFOS_CURRENT_CDE = NULL
    WHERE SORLFOS_PIDM = P_PIDM
        AND SORLFOS_LCUR_SEQNO = V_SORLCUR_SEQNO_OLD
        AND SORLFOS_CSTS_CODE IN ('INPROGRESS','STUDYPATH');
    
    ------------------------------------------------------------------------------------
    --
    COMMIT;
EXCEPTION
WHEN OTHERS THEN
    ROLLBACK;
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CAMBIAR_SGASTDN;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_CAMBIAR_SAAADMS ( 
    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PROGRAM_NEW           IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
    P_CATALOGO_NEW          IN VARCHAR2,
    P_MOD_ADM_NEW           IN VARCHAR2,
    P_MESSAGE               OUT VARCHAR2
)
AS
    V_TERM_CODE_LAST            SFBETRM.SFBETRM_TERM_CODE%TYPE;
    V_CURR_RULE                 SOBCURR.SOBCURR_CURR_RULE%TYPE;
    V_COLL_CODE                 SOBCURR.SOBCURR_COLL_CODE%TYPE;
    V_DEGC_CODE                 SOBCURR.SOBCURR_DEGC_CODE%TYPE;
    V_CMJR_RULE                 SORCMJR.SORCMJR_CMJR_RULE%TYPE;
    V_INDICADOR                 NUMBER;
    V_MESSAGE                   EXCEPTION;
    
    V_ATTS_CODE                 STVATTS.STVATTS_CODE%TYPE;      -------- COD atributos
        
    -- AGREGAR NUEVO REGISTRO
    CURSOR C_SORLCUR IS
        SELECT *
        FROM (
            SELECT *
            FROM SORLCUR 
            WHERE SORLCUR_PIDM = P_PIDM
                AND SORLCUR_LMOD_CODE = 'ADMISSIONS'
                AND SORLCUR_CURRENT_CDE = 'Y' --------------------- CURRENT CURRICULUM
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
        )WHERE ROWNUM = 1;
    
    V_SORLCUR_REC           SORLCUR%ROWTYPE;
    V_SORLCUR_SEQNO_OLD     SORLCUR.SORLCUR_SEQNO%TYPE;
    V_SORLCUR_SEQNO_INC     SORLCUR.SORLCUR_SEQNO%TYPE;
    V_SORLCUR_SEQNO_NEW     SORLCUR.SORLCUR_SEQNO%TYPE;
    
    V_INDICADOR             NUMBER;
    
BEGIN
    -- #######################################################################
    -- Get Reglas de Curriculum (CURR_RULE)
    SELECT  SOBCURR_CURR_RULE,  SOBCURR_COLL_CODE,  SOBCURR_DEGC_CODE,  SORCMJR_CMJR_RULE
    INTO    V_CURR_RULE,        V_COLL_CODE,        V_DEGC_CODE,        V_CMJR_RULE
    FROM (
        SELECT *
        FROM SOBCURR
        INNER JOIN (
            SELECT  SOBCURR_CURR_RULE CURR_RULE, SOBCURR_CAMP_CODE CAMP_CODE, SOBCURR_COLL_CODE COLL_CODE, 
                    SOBCURR_DEGC_CODE DEGC_CODE, SOBCURR_PROGRAM CPROGRAM, MAX(SOBCURR_TERM_CODE_INIT) TERM_CODE_INIT
            FROM SOBCURR 
            WHERE SOBCURR_LEVL_CODE = 'PG' 
            GROUP BY SOBCURR_CURR_RULE, SOBCURR_CAMP_CODE, SOBCURR_COLL_CODE, SOBCURR_DEGC_CODE, SOBCURR_PROGRAM
        )ON SOBCURR_CURR_RULE = CURR_RULE
            AND SOBCURR_CAMP_CODE = CAMP_CODE
            AND SOBCURR_COLL_CODE = COLL_CODE
            AND SOBCURR_DEGC_CODE = DEGC_CODE
            AND SOBCURR_PROGRAM = CPROGRAM
            AND SOBCURR_TERM_CODE_INIT = TERM_CODE_INIT
    ) 
    INNER JOIN (
        SELECT * 
        FROM SORCMJR
        INNER JOIN (
            SELECT SORCMJR_CURR_RULE CURR_RULE, SORCMJR_MAJR_CODE MAJR_CODE, SORCMJR_DEPT_CODE DEPT_CODE, MAX(SORCMJR_TERM_CODE_EFF) TERM_CODE_EFF
            FROM SORCMJR 
            GROUP BY SORCMJR_CURR_RULE, SORCMJR_MAJR_CODE, SORCMJR_DEPT_CODE
        )ON SORCMJR_CURR_RULE = CURR_RULE
            AND SORCMJR_MAJR_CODE = MAJR_CODE
            AND SORCMJR_DEPT_CODE = DEPT_CODE
            AND SORCMJR_TERM_CODE_EFF = TERM_CODE_EFF
    )ON SOBCURR_CURR_RULE = SORCMJR_CURR_RULE
    WHERE SOBCURR_CAMP_CODE = P_CAMP_CODE
        AND SOBCURR_LEVL_CODE = 'PG'
        AND SORCMJR_DEPT_CODE = P_DEPT_CODE
        AND SOBCURR_PROGRAM = P_PROGRAM_NEW;

    -- #######################################################################
    -- INSERT  ------> SORLCUR -
    OPEN C_SORLCUR;
    LOOP
        FETCH C_SORLCUR INTO V_SORLCUR_REC;
        EXIT WHEN C_SORLCUR%NOTFOUND;    
        
        -- GET SORLCUR_SEQNO DEL REGISTRO INACTIVO 
        SELECT MAX(SORLCUR_SEQNO + 1)
            INTO V_SORLCUR_SEQNO_INC
        FROM SORLCUR 
        WHERE SORLCUR_PIDM = V_SORLCUR_REC.SORLCUR_PIDM;
        
        -- GET SORLCUR_SEQNO NUEVO 
        SELECT MAX(SORLCUR_SEQNO + 2)
            INTO V_SORLCUR_SEQNO_NEW
        FROM SORLCUR
        WHERE SORLCUR_PIDM = V_SORLCUR_REC.SORLCUR_PIDM;
        
        -- GET SORLCUR_SEQNO ANTERIOR 
        V_SORLCUR_SEQNO_OLD := V_SORLCUR_REC.SORLCUR_SEQNO;
        DBMS_OUTPUT.PUT_LINE(V_SORLCUR_SEQNO_OLD);
        
        -- INACTIVE --- SORLCUR - (1 DE 2)
        INSERT INTO SORLCUR (
                  SORLCUR_PIDM,             SORLCUR_SEQNO,                SORLCUR_LMOD_CODE,
                  SORLCUR_TERM_CODE,        SORLCUR_KEY_SEQNO,            SORLCUR_PRIORITY_NO,
                  SORLCUR_ROLL_IND,         SORLCUR_CACT_CODE,            SORLCUR_USER_ID,
                  SORLCUR_DATA_ORIGIN,      SORLCUR_ACTIVITY_DATE,        SORLCUR_LEVL_CODE,
                  SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,            SORLCUR_TERM_CODE_CTLG,
                  SORLCUR_TERM_CODE_END,    SORLCUR_TERM_CODE_MATRIC,     SORLCUR_TERM_CODE_ADMIT,
                  SORLCUR_ADMT_CODE,        SORLCUR_CAMP_CODE,            SORLCUR_PROGRAM,
                  SORLCUR_START_DATE,       SORLCUR_END_DATE,             SORLCUR_CURR_RULE,
                  SORLCUR_ROLLED_SEQNO,     SORLCUR_STYP_CODE,            SORLCUR_RATE_CODE,
                  SORLCUR_LEAV_CODE,        SORLCUR_LEAV_FROM_DATE,       SORLCUR_LEAV_TO_DATE,
                  SORLCUR_EXP_GRAD_DATE,    SORLCUR_TERM_CODE_GRAD,       SORLCUR_ACYR_CODE,
                  SORLCUR_SITE_CODE,        SORLCUR_APPL_SEQNO,           SORLCUR_APPL_KEY_SEQNO,
                  SORLCUR_USER_ID_UPDATE,   SORLCUR_ACTIVITY_DATE_UPDATE, SORLCUR_GAPP_SEQNO,
                  SORLCUR_CURRENT_CDE ) 
        VALUES (  V_SORLCUR_REC.SORLCUR_PIDM,             V_SORLCUR_SEQNO_INC,                        V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                  P_TERM_CODE,                            V_SORLCUR_REC.SORLCUR_KEY_SEQNO,            V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                  V_SORLCUR_REC.SORLCUR_ROLL_IND,         'INACTIVE',/*SORLCUR_CACT_CODE*/            USER,
                  V_SORLCUR_REC.SORLCUR_DATA_ORIGIN,      SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                  V_SORLCUR_REC.SORLCUR_COLL_CODE,        V_SORLCUR_REC.SORLCUR_DEGC_CODE,            V_SORLCUR_REC.SORLCUR_TERM_CODE_CTLG,
                  P_TERM_CODE,                            V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                  V_SORLCUR_REC.SORLCUR_ADMT_CODE,        V_SORLCUR_REC.SORLCUR_CAMP_CODE,            V_SORLCUR_REC.SORLCUR_PROGRAM,
                  V_SORLCUR_REC.SORLCUR_START_DATE,       V_SORLCUR_REC.SORLCUR_END_DATE,             V_SORLCUR_REC.SORLCUR_CURR_RULE,
                  V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,     V_SORLCUR_REC.SORLCUR_STYP_CODE,            V_SORLCUR_REC.SORLCUR_RATE_CODE,
                  V_SORLCUR_REC.SORLCUR_LEAV_CODE,        V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                  V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,    V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                  V_SORLCUR_REC.SORLCUR_SITE_CODE,        V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                  USER,                                   SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                  NULL /*SORLCUR_CURRENT_CDE*/ );
        
        -- ACTIVE --- SORLCUR - (2 DE 2)
        INSERT INTO SORLCUR (
                  SORLCUR_PIDM,             SORLCUR_SEQNO,                SORLCUR_LMOD_CODE,
                  SORLCUR_TERM_CODE,        SORLCUR_KEY_SEQNO,            SORLCUR_PRIORITY_NO,
                  SORLCUR_ROLL_IND,         SORLCUR_CACT_CODE,            SORLCUR_USER_ID,
                  SORLCUR_DATA_ORIGIN,      SORLCUR_ACTIVITY_DATE,        SORLCUR_LEVL_CODE,
                  SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,            SORLCUR_TERM_CODE_CTLG,
                  SORLCUR_TERM_CODE_END,    SORLCUR_TERM_CODE_MATRIC,     SORLCUR_TERM_CODE_ADMIT,
                  SORLCUR_ADMT_CODE,        SORLCUR_CAMP_CODE,            SORLCUR_PROGRAM,
                  SORLCUR_START_DATE,       SORLCUR_END_DATE,             SORLCUR_CURR_RULE,
                  SORLCUR_ROLLED_SEQNO,     SORLCUR_STYP_CODE,            SORLCUR_RATE_CODE,
                  SORLCUR_LEAV_CODE,        SORLCUR_LEAV_FROM_DATE,       SORLCUR_LEAV_TO_DATE,
                  SORLCUR_EXP_GRAD_DATE,    SORLCUR_TERM_CODE_GRAD,       SORLCUR_ACYR_CODE,
                  SORLCUR_SITE_CODE,        SORLCUR_APPL_SEQNO,           SORLCUR_APPL_KEY_SEQNO,
                  SORLCUR_USER_ID_UPDATE,   SORLCUR_ACTIVITY_DATE_UPDATE, SORLCUR_GAPP_SEQNO,
                  SORLCUR_CURRENT_CDE ) 
        VALUES (  V_SORLCUR_REC.SORLCUR_PIDM,             V_SORLCUR_SEQNO_NEW,                        V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                  P_TERM_CODE,                            V_SORLCUR_REC.SORLCUR_KEY_SEQNO,            V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                  V_SORLCUR_REC.SORLCUR_ROLL_IND,         V_SORLCUR_REC.SORLCUR_CACT_CODE,            USER,
                  'WorkFlow',                             SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                  V_COLL_CODE,                            V_DEGC_CODE,                                P_CATALOGO_NEW /*SORLCUR_TERM_CODE_CTLG*/,
                  NULL,                                   V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                  V_SORLCUR_REC.SORLCUR_ADMT_CODE,        V_SORLCUR_REC.SORLCUR_CAMP_CODE,            P_PROGRAM_NEW,
                  V_SORLCUR_REC.SORLCUR_START_DATE,       V_SORLCUR_REC.SORLCUR_END_DATE,             V_CURR_RULE,
                  V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,     V_SORLCUR_REC.SORLCUR_STYP_CODE,            V_SORLCUR_REC.SORLCUR_RATE_CODE,
                  V_SORLCUR_REC.SORLCUR_LEAV_CODE,        V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                  V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,    V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                  V_SORLCUR_REC.SORLCUR_SITE_CODE,        V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                  USER,                                   SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                  V_SORLCUR_REC.SORLCUR_CURRENT_CDE );
        
        
        -- UPDATE TERM_CODE_END (vigencia curriculum del Registro ANTERIOR) 
        UPDATE SORLCUR 
            SET SORLCUR_TERM_CODE_END = P_TERM_CODE, SORLCUR_ACTIVITY_DATE_UPDATE = SYSDATE
        WHERE   SORLCUR_PIDM = P_PIDM 
        AND     SORLCUR_LMOD_CODE = 'ADMISSIONS'
        AND     SORLCUR_SEQNO = V_SORLCUR_SEQNO_OLD;
        
        -- UPDATE SORLCUR_CURRENT_CDE(curriculum activo)  -- Solo 1 curricula activa por PERIODO
        UPDATE SORLCUR 
            SET   SORLCUR_CURRENT_CDE = NULL
        WHERE   SORLCUR_PIDM = P_PIDM 
        AND     SORLCUR_LMOD_CODE = 'ADMISSIONS'
        AND     SORLCUR_TERM_CODE_END = P_TERM_CODE -- En caso el registro sea del mismo Periodo
        AND     SORLCUR_SEQNO = V_SORLCUR_SEQNO_OLD;
      
    END LOOP;
    CLOSE C_SORLCUR;
      
    -- #######################################################################
    -- INSERT --- SORLFOS - (1 DE 2) 
    INSERT INTO SORLFOS (
        SORLFOS_PIDM,             SORLFOS_LCUR_SEQNO,         SORLFOS_SEQNO,
        SORLFOS_LFST_CODE,        SORLFOS_TERM_CODE,          SORLFOS_PRIORITY_NO,
        SORLFOS_CSTS_CODE,        SORLFOS_CACT_CODE,          SORLFOS_DATA_ORIGIN,
        SORLFOS_USER_ID,          SORLFOS_ACTIVITY_DATE,      SORLFOS_MAJR_CODE,
        SORLFOS_TERM_CODE_CTLG,   SORLFOS_TERM_CODE_END,      SORLFOS_DEPT_CODE,
        SORLFOS_MAJR_CODE_ATTACH, SORLFOS_LFOS_RULE,          SORLFOS_CONC_ATTACH_RULE,
        SORLFOS_START_DATE,       SORLFOS_END_DATE,           SORLFOS_TMST_CODE,
        SORLFOS_ROLLED_SEQNO,     SORLFOS_USER_ID_UPDATE,     SORLFOS_ACTIVITY_DATE_UPDATE,
        SORLFOS_CURRENT_CDE) 
    SELECT 
        V_SORLFOS_REC.SORLFOS_PIDM,             V_SORLCUR_SEQNO_INC,                      1,/*V_SORLFOS_REC.SORLFOS_SEQNO*/
        V_SORLFOS_REC.SORLFOS_LFST_CODE,        P_TERM_CODE,                              V_SORLFOS_REC.SORLFOS_PRIORITY_NO,
        'CHANGED'/*SORLFOS_CSTS_CODE*/,         'INACTIVE'/*SORLFOS_CACT_CODE*/,          V_SORLFOS_REC.SORLFOS_DATA_ORIGIN,
        USER,                                   SYSDATE,                                  V_SORLFOS_REC.SORLFOS_MAJR_CODE,
        V_SORLFOS_REC.SORLFOS_TERM_CODE_CTLG,   V_SORLFOS_REC.SORLFOS_TERM_CODE_END,      V_SORLFOS_REC.SORLFOS_DEPT_CODE,
        V_SORLFOS_REC.SORLFOS_MAJR_CODE_ATTACH, V_SORLFOS_REC.SORLFOS_LFOS_RULE,          V_SORLFOS_REC.SORLFOS_CONC_ATTACH_RULE,
        V_SORLFOS_REC.SORLFOS_START_DATE,       V_SORLFOS_REC.SORLFOS_END_DATE,           V_SORLFOS_REC.SORLFOS_TMST_CODE,
        V_SORLFOS_REC.SORLFOS_ROLLED_SEQNO,     USER,                                     SYSDATE,
        NULL /*SORLFOS_CURRENT_CDE*/
    FROM SORLFOS V_SORLFOS_REC
    WHERE SORLFOS_PIDM = P_PIDM
        AND SORLFOS_LCUR_SEQNO = V_SORLCUR_SEQNO_OLD
        AND ROWNUM = 1;
    
    -- INSERT --- SORLFOS - (2 DE 2)
    INSERT INTO SORLFOS (
        SORLFOS_PIDM,             SORLFOS_LCUR_SEQNO,         SORLFOS_SEQNO,
        SORLFOS_LFST_CODE,        SORLFOS_TERM_CODE,          SORLFOS_PRIORITY_NO,
        SORLFOS_CSTS_CODE,        SORLFOS_CACT_CODE,          SORLFOS_DATA_ORIGIN,
        SORLFOS_USER_ID,          SORLFOS_ACTIVITY_DATE,      SORLFOS_MAJR_CODE,
        SORLFOS_TERM_CODE_CTLG,   SORLFOS_TERM_CODE_END,      SORLFOS_DEPT_CODE,
        SORLFOS_MAJR_CODE_ATTACH, SORLFOS_LFOS_RULE,          SORLFOS_CONC_ATTACH_RULE,
        SORLFOS_START_DATE,       SORLFOS_END_DATE,           SORLFOS_TMST_CODE,
        SORLFOS_ROLLED_SEQNO,     SORLFOS_USER_ID_UPDATE,     SORLFOS_ACTIVITY_DATE_UPDATE,
        SORLFOS_CURRENT_CDE) 
    SELECT 
        V_SORLFOS_REC.SORLFOS_PIDM,                 V_SORLCUR_SEQNO_NEW,                      1,/*V_SORLFOS_REC.SORLFOS_SEQNO*/
        V_SORLFOS_REC.SORLFOS_LFST_CODE,            P_TERM_CODE,                              V_SORLFOS_REC.SORLFOS_PRIORITY_NO,
        V_SORLFOS_REC.SORLFOS_CSTS_CODE,            V_SORLFOS_REC.SORLFOS_CACT_CODE,          'WorkFlow',
        USER,                                       SYSDATE,                                  P_PROGRAM_NEW /*SORLFOS_MAJR_CODE*/,
        P_CATALOGO_NEW /*SORLFOS_TERM_CODE_CTLG*/,   V_SORLFOS_REC.SORLFOS_TERM_CODE_END,     V_SORLFOS_REC.SORLFOS_DEPT_CODE,
        V_SORLFOS_REC.SORLFOS_MAJR_CODE_ATTACH,     V_CMJR_RULE /*SORLFOS_LFOS_RULE*/,        V_SORLFOS_REC.SORLFOS_CONC_ATTACH_RULE,
        V_SORLFOS_REC.SORLFOS_START_DATE,           V_SORLFOS_REC.SORLFOS_END_DATE,           V_SORLFOS_REC.SORLFOS_TMST_CODE,
        V_SORLFOS_REC.SORLFOS_ROLLED_SEQNO,         USER,                                     SYSDATE,
        'Y'
    FROM SORLFOS V_SORLFOS_REC
    WHERE SORLFOS_PIDM = P_PIDM
        AND SORLFOS_LCUR_SEQNO = V_SORLCUR_SEQNO_OLD
        AND ROWNUM = 1;
    
    -- UPDATE SORLFOS_CURRENT_CDE(curriculum activo) PARA registros del mismo PERIODO.
    UPDATE SORLFOS 
        SET SORLFOS_CURRENT_CDE = NULL
    WHERE SORLFOS_PIDM = P_PIDM
        AND SORLFOS_LCUR_SEQNO = V_SORLCUR_SEQNO_OLD
        AND SORLFOS_CSTS_CODE IN ('INPROGRESS','STUDYPATH');
    
    ------------------------------------------------------------------------------------
    --
    COMMIT;
EXCEPTION
WHEN OTHERS THEN
    ROLLBACK;
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CAMBIAR_SAAADMS;

END BWZKACOD;

/**********************************************************************************************/
--/
--show errors
--
--SET SCAN ON
/**********************************************************************************************/