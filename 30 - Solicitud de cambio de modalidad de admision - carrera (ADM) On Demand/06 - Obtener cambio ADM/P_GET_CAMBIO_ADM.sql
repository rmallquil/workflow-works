/*
SET SERVEROUTPUT ON
DECLARE P_PROGRAM_NEW VARCHAR2(4000);
 P_CAMBIO_ADM varchar2(4000);
 P_MOD_ADM_NEW VARCHAR2(4000);
 P_MOD_ADM_NEW_DESC VARCHAR2(4000);
 P_PROGRAM_NEW_DESC VARCHAR2(4000);
 P_MESSAGE VARCHAR2(4000);
BEGIN
    P_GET_CAMBIO_ADM('71849483', '201910', 'UREG', '315', '06', 'Previa', P_CAMBIO_ADM, P_MOD_ADM_NEW, P_MOD_ADM_NEW_DESC, P_PROGRAM_NEW, P_PROGRAM_NEW_DESC, P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_CAMBIO_ADM);
    DBMS_OUTPUT.PUT_LINE(P_MOD_ADM_NEW);
    DBMS_OUTPUT.PUT_LINE(P_MOD_ADM_NEW_DESC);
    DBMS_OUTPUT.PUT_LINE(P_PROGRAM_NEW);
    DBMS_OUTPUT.PUT_LINE(P_PROGRAM_NEW_DESC);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
END;
*/

CREATE OR REPLACE PROCEDURE P_GET_CAMBIO_ADM (
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_DEPT_CODE			IN STVDEPT.STVDEPT_CODE%TYPE,
    P_PROGRAM           IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_MOD_ADM           IN VARCHAR2,
    P_MOD_ADM_DESC      IN VARCHAR2,
    P_CAMBIO_ADM        OUT VARCHAR2,
    P_MOD_ADM_NEW       OUT VARCHAR2,
    P_MOD_ADM_NEW_DESC  OUT VARCHAR2,
    P_PROGRAM_NEW       OUT VARCHAR2,
    P_PROGRAM_NEW_DESC  OUT VARCHAR2,
    P_MESSAGE           OUT VARCHAR2
)
AS
	V_CONTADOR          NUMBER;
    V_FLAG_PROGRAM      NUMBER := 0;
    V_FLAG_MOD_ADM      NUMBER := 0;
	V_APEC_TERM         VARCHAR2(10);
    V_APEC_DEPT         VARCHAR2(10);
    V_APEC_MOD_ADM      VARCHAR(10);
    V_APEC_MOD_ADM_NEW  VARCHAR(10);
    V_PROGRAM_DESC      VARCHAR2(100);
BEGIN
    
    -- PERIODO
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;
    
    -- DEPARTAMENTO
    SELECT CZRDEPT_DEPT_BDUCCI
    INTO V_APEC_DEPT
    FROM CZRDEPT 
    WHERE CZRDEPT_CODE = P_DEPT_CODE;

    -- MODALIDAD DE ADMISION
    SELECT "IDModalidadPostu"
        INTO V_APEC_MOD_ADM
    FROM dbo.tblModalidadPostuRecruiter@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "IDModRecruiter" = P_MOD_ADM
    AND "departament" = V_APEC_DEPT;
    
    --INDICADOR QUE EXISTE EL CAMBIO
    SELECT COUNT(*)
        INTO V_CONTADOR
    FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
    WHERE TRIM("IDDependencia") = 'UCCI'
    AND TRIM("IDPerAcad") = V_APEC_TERM
    AND TRIM("IDEscuelaADM") = V_APEC_DEPT
    AND "IDAlumno" = P_ID
    AND (TRIM("IDEscuela") <> P_PROGRAM
        OR "IDModalidadPostu" <> V_APEC_MOD_ADM)
    AND TRIM("Ingresante") = '1'
    AND TRIM("Renuncia") = '0';
    
    IF V_CONTADOR > 0 THEN
        SELECT DISTINCT TRIM("IDEscuela"), "IDModalidadPostu"
            INTO P_PROGRAM_NEW, V_APEC_MOD_ADM_NEW
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE TRIM("IDDependencia") = 'UCCI'
        AND TRIM("IDPerAcad") = V_APEC_TERM
        AND TRIM("IDEscuelaADM") = V_APEC_DEPT
        AND "IDAlumno" = P_ID
        AND (TRIM("IDEscuela") <> P_PROGRAM
            OR "IDModalidadPostu" <> V_APEC_MOD_ADM)
        AND TRIM("Ingresante") = '1'
        AND TRIM("Renuncia") = '0';
        
        IF P_PROGRAM <> P_PROGRAM_NEW THEN
            -- >> DESC PROGRAM NEW
            SELECT SMRPRLE_PROGRAM_DESC
            INTO P_PROGRAM_NEW_DESC
            FROM SMRPRLE
            WHERE SMRPRLE_PROGRAM = P_PROGRAM_NEW;
            
            V_FLAG_PROGRAM := 1;
        ELSE
            -- >> DESC PROGRAM
            SELECT SMRPRLE_PROGRAM_DESC
            INTO P_PROGRAM_NEW_DESC
            FROM SMRPRLE
            WHERE SMRPRLE_PROGRAM = P_PROGRAM;
        END IF;
        
        IF V_APEC_MOD_ADM_NEW <> V_APEC_MOD_ADM THEN
            -- >> MODALIDAD DE ADMISION NUEVO
            SELECT "IDModRecruiter"
                INTO P_MOD_ADM_NEW
            FROM dbo.tblModalidadPostuRecruiter@BDUCCI.CONTINENTAL.EDU.PE
            WHERE "IDModalidadPostu" = V_APEC_MOD_ADM_NEW
            AND "departament" = V_APEC_DEPT;
            
            -- >> DESC MODALIDAD DE ADMISION
            SELECT STVADMT_DESC
                INTO P_MOD_ADM_NEW_DESC
            FROM STVADMT
            WHERE STVADMT_CODE = P_MOD_ADM_NEW;
            
            V_FLAG_MOD_ADM := 1;
        ELSE
            P_MOD_ADM_NEW := P_MOD_ADM;
            P_MOD_ADM_NEW_DESC := P_MOD_ADM_DESC;
        END IF;
        
        -- >> VALIDACIÓN DEL CAMBIO
        IF V_FLAG_MOD_ADM = 1 AND V_FLAG_PROGRAM = 0 THEN
            P_MESSAGE := 'Cambio de modalidad de admisión';
            P_CAMBIO_ADM := 'MA';
        ELSIF V_FLAG_MOD_ADM = 0 AND V_FLAG_PROGRAM = 1 THEN
            P_MESSAGE := 'Cambio de carrera';
            P_CAMBIO_ADM := 'CA';
        ELSIF V_FLAG_MOD_ADM = 1 AND V_FLAG_PROGRAM = 1 THEN
            P_MESSAGE := 'Cambio de modalidad de admision y carrera';
            P_CAMBIO_ADM := 'MC';
        ELSE
            P_MESSAGE := 'ERROR! El estudiante no tiene registrado su cambio de carrera y/o modalidad de admisión. Por favor, verificar el cambio en RECRUITER.';
            P_CAMBIO_ADM := 'NO';
        END IF;
    ELSE
        P_MESSAGE := 'ERROR! El estudiante no tiene registrado su cambio de carrera y/o modalidad de admisión. Por favor, verificar su cambio en RECRUITER.';
        P_CAMBIO_ADM := 'NO';
    END IF;

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_GET_CAMBIO_ADM;