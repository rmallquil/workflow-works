create or replace PACKAGE WFK_WFTG_SVRSVPR AS
/*
 BWKSVPR:
       Conti Package WF TG_SVRSVPR_DESPUES_INSERT
*/
-- FILE NAME..: BWKSVPR.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWKSVPR
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2017
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              

--## SOLICITUD DE TRASLADO INTERNO (CONTISTI) ## 
PROCEDURE P_CONTISTI_GETDATA( 
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
      );

--------------------------------------------------------------------------------


--------------------------------------------------------------------------------

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--    

END WFK_WFTG_SVRSVPR;