
/**********************************************************************++
-- creación BANNIS  Y PERMISOS
/**********************************************************************/
  --          CREATE OR REPLACE PUBLIC SYNONYM "BWZKSVPR" FOR "BANINST1"."BWZKSVPR";
  --          GRANT EXECUTE ON BANINST1.BWZKSVPR TO SATURN;
--------------------------------------------------------------------------
--------------------------------------------------------------------------

create or replace PACKAGE BWZKSVPR AS
/*
 BWZKSVPR:
       Conti Package WF TG_SVRSVPR_DESPUES_INSERT
*/
-- FILE NAME..: BWZKSVPR.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKSVPR
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2017
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              

--## SOLICITUD DE TRASLADO INTERNO (CONTISTI) ## 
PROCEDURE P_CONTISTI_GETDATA( 
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
      );

--------------------------------------------------------------------------------
--## SOLICITUD DE PRORROGA GASTO ADMINISTRATIVO (CONTISPGA) ## 
PROCEDURE P_CONTISPGA_GETDATA( 
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
      );

--------------------------------------------------------------------------------
--## SOLICITUD DE PPRONTO PAGO (CONTISPP) ## 
PROCEDURE P_CONTISPP_GETDATA( 
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
      );

--------------------------------------------------------------------------------
--## SOLICITUD DE PAGO TRAMITE BACHILLER y TITULACION (CONTISPBT) ## 
PROCEDURE P_CONTISPBT_GETDATA( 
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
      );

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--    

END BWZKSVPR;