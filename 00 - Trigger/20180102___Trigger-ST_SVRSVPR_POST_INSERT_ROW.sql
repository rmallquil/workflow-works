/**********************************************************************************************/
/* ST_SVRSVPR_POST_INSERT_ROW.sql                                                             */
/**********************************************************************************************/
/*                                                                                            */
/* Descripci�n corta: Script para generar el Trigger ST_SVRSVPR_POST_INSERT_ROW               */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI             FECHA       */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creaci�n del C�digo.                                        RML             06/OCT/2016 */
/*    --------------------                                                                    */
/*    Generar un evento desde la creaci�n de una nueva solicitud - Destinado para solicitudes */
/*    WORKFLOW.                                                                               */
/*                                                                                            */
/* 2. Actualizaci�n:                                                                          */
/*                                                                RML             06/DIC/2016 */  
/*    2.1 Modificaci�n "CAMBIO DE PLAN", se obtendra el periodo del registro "ELEGIBLE" y ya  */
/*        no del ultimo periodo matriculado(NRC). Solicitado por Mirian Flores.               */
/*                                                                RML             27/MAR/2017 */
/*    2.2 Se agrego el nombre de la sede en el titulo_sol para todas las solicitudes.         */
/*                                                                BFV             30/OCT/2017 */
/*    2.3 Se agrego la nueva solicitud DESCUENTO POR CONVENIO (SOL019).                       */
/*                                                                LAM             06/DIC/2017 */
/*    2.4 Se agrego la nueva solicitud MODIFICACI�N DE CUOTA INICIAL - VERANO (SOL020).       */
/*                                                                                            */                                                                   
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/


/*
-- buscar objecto
select dbms_metadata.get_ddl('TRIGGER','ST_SVRSVPR_POST_INSERT_ROW') from dual

-- Desabilitar - disable trigger
ALTER TRIGGER ST_SVRSVPR_POST_INSERT_ROW ENABLE;
DROP TRIGGER ST_SVRSVPR_POST_INSERT_ROW;
*/


CREATE OR REPLACE TRIGGER ST_SVRSVPR_POST_INSERT_ROW
BEFORE INSERT
   ON "SATURN"."SVRSVPR"
   FOR EACH ROW

DECLARE
      P_PIDM_ALUMNO             SPRIDEN.SPRIDEN_ID%TYPE;
      P_ID_ALUMNO               SPRIDEN.SPRIDEN_ID%TYPE;
      P_PERIODO                 SFBETRM.SFBETRM_TERM_CODE%TYPE;
      P_CORREO_ALUMNO           GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE;
      P_NOMBRE_ALUMNO           VARCHAR2(180); -- SPRIDEN.SPRIDEN_LAST_NAME, SPRIDEN.SPRIDEN_FIRST_NAME
      P_FOLIO_SOLICITUD         SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE;
      P_FECHA_SOLICITUD         SVRSVPR.SVRSVPR_ACTIVITY_DATE%TYPE;
      P_COMENTARIO_ALUMNO       SVRSVAD.SVRSVAD_ADDL_DATA_DESC%TYPE;
      P_MODALIDAD_ALUMNO        SGBSTDN.SGBSTDN_DEPT_CODE%TYPE; -- Departamento
      P_COD_SEDE                STVCAMP.STVCAMP_CODE%TYPE;  -- SGBSTDN.SGBSTDN_CAMP_CODE%TYPE;
      P_NOMBRE_SEDE             STVCAMP.STVCAMP_DESC%TYPE;
      
      P_PART_PERIODO            SOBPTRM.SOBPTRM_PTRM_CODE%TYPE; --------------- Parte-de-Periodo
      V_TITUTO_SOL              VARCHAR2(350);

      P_SRVC_CODE               SVRSVPR.SVRSVPR_SRVC_CODE%TYPE;
      P_INDICADOR               NUMBER;
      P_MESSAGE                 EXCEPTION;
      
      V_PARAMS          GOKPARM.T_PARAMETERLIST;
      EVENT_CODE        GTVEQNM.GTVEQNM_CODE%TYPE;
BEGIN
-- 
      -- -- -- -- GET CODIGO SERVICIO -- -- -- --
      P_SRVC_CODE            :=      :NEW.SVRSVPR_SRVC_CODE;
      
      P_PIDM_ALUMNO          :=      :NEW.SVRSVPR_PIDM;
      P_FOLIO_SOLICITUD      :=      :NEW.SVRSVPR_PROTOCOL_SEQ_NO;
      P_FECHA_SOLICITUD      :=      :NEW.SVRSVPR_ACTIVITY_DATE;
      -- P_COMENTARIO_ALUMNO    :=      :NEW.SVRSVPR_STU_COMMENT;    -- Comentario por defecto de solicitudes
      
      
      -- GET nombres , ID
      SELECT (SPRIDEN_FIRST_NAME || ' ' || SPRIDEN_LAST_NAME), SPRIDEN_ID INTO P_NOMBRE_ALUMNO, P_ID_ALUMNO  FROM SPRIDEN 
      WHERE SPRIDEN_PIDM = P_PIDM_ALUMNO AND SPRIDEN_CHANGE_IND IS NULL;
     
     
      -- GET email
      SELECT COUNT(*) INTO P_INDICADOR FROM GOREMAL WHERE GOREMAL_PIDM = P_PIDM_ALUMNO and GOREMAL_STATUS_IND = 'A';
      IF P_INDICADOR = 0 THEN
           P_CORREO_ALUMNO := '-';
      ELSE
          -----------------------
              SELECT COUNT(*) INTO P_INDICADOR FROM GOREMAL WHERE GOREMAL_PIDM = P_PIDM_ALUMNO and GOREMAL_STATUS_IND = 'A' 
              AND GOREMAL_EMAIL_ADDRESS LIKE ('%@continental.edu.pe');            
              IF (P_INDICADOR > 0) THEN
                  SELECT GOREMAL_EMAIL_ADDRESS INTO P_CORREO_ALUMNO FROM (
                    SELECT GOREMAL_EMAIL_ADDRESS, GOREMAL_PREFERRED_IND FROM GOREMAL 
                    WHERE GOREMAL_PIDM = P_PIDM_ALUMNO and GOREMAL_STATUS_IND = 'A' AND GOREMAL_EMAIL_ADDRESS LIKE ('%@continental.edu.pe')
                    ORDER BY GOREMAL_PREFERRED_IND DESC
                  )WHERE ROWNUM <= 1;
              ELSE
                  SELECT GOREMAL_EMAIL_ADDRESS INTO P_CORREO_ALUMNO FROM (
                      SELECT GOREMAL_EMAIL_ADDRESS, GOREMAL_PREFERRED_IND FROM GOREMAL
                      WHERE GOREMAL_PIDM = P_PIDM_ALUMNO and GOREMAL_STATUS_IND = 'A'
                      ORDER BY GOREMAL_PREFERRED_IND DESC
                  )WHERE ROWNUM <= 1;
              END IF;
      END IF;
      
      
      -- GET email   : Comentario Configurado como DATO ADICIONAL
      -- SELECT SVRSVAD_ADDL_DATA_DESC INTO P_COMENTARIO_ALUMNO FROM SVRSVAD
      -- WHERE SVRSVAD_PROTOCOL_SEQ_NO = :NEW.SVRSVPR_PROTOCOL_SEQ_NO AND SVRSVAD_ADDL_DATA_SEQ = 2;
      P_COMENTARIO_ALUMNO := '--';
      
      
      ----------------------- GET DATOS SEGUN TIPO DE SOLICITUD ---------------
            -- Codigo evento
            -- Periodo
            -- Departamente
            -- Sede
     ---------------------------------------------------------------------------
      IF P_SRVC_CODE = 'SOL003' THEN --########### RECTIF. REZAGADOS ###########
     ---------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL003');
              
              -- >> GET DEPARTAMENTO y CAMPUS --
              SELECT COUNT(*) INTO P_INDICADOR FROM SORLCUR INNER JOIN SORLFOS
              ON SORLCUR_PIDM = SORLFOS_PIDM AND SORLCUR_SEQNO = SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE SORLCUR_PIDM = P_PIDM_ALUMNO AND SORLCUR_LMOD_CODE = 'LEARNER' 
              AND SORLCUR_CACT_CODE   = 'ACTIVE' AND SORLCUR_CURRENT_CDE = 'Y';
              
              IF P_INDICADOR = 0 THEN
                    P_MODALIDAD_ALUMNO    := '-';
                    P_COD_SEDE            := '-';
                    P_NOMBRE_SEDE         := '-';
              ELSE  
                    -- GET DEPARTAMENTO y CAMPUS mas reciente del alumno 
                    SELECT SORLFOS_DEPT_CODE , SORLCUR_CAMP_CODE INTO P_MODALIDAD_ALUMNO, P_COD_SEDE 
                    FROM(
                          SELECT    SORLCUR_SEQNO,      
                                    SORLCUR_CAMP_CODE,    
                                    SORLFOS_DEPT_CODE
                          FROM SORLCUR        
                          INNER JOIN SORLFOS
                              ON SORLCUR_PIDM         = SORLFOS_PIDM 
                              AND SORLCUR_SEQNO       = SORLFOS.SORLFOS_LCUR_SEQNO
                          WHERE SORLCUR_PIDM          = P_PIDM_ALUMNO 
                              AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                              AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                              AND SORLCUR_CURRENT_CDE = 'Y'
                          ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
                    ) WHERE ROWNUM <= 1;
                    
                    -- GET nombre campus 
                    SELECT STVCAMP_DESC INTO P_NOMBRE_SEDE FROM STVCAMP WHERE STVCAMP_CODE = P_COD_SEDE;
              END IF;
              
              -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
              IF (P_MODALIDAD_ALUMNO = '-' OR P_COD_SEDE = '-') THEN
                  P_PERIODO := '-';
              ELSE
                    -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
                    SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                              WHEN 'UPGT' THEN 'W' 
                                              WHEN 'UREG' THEN 'R' 
                                              WHEN 'UPOS' THEN '-' 
                                              WHEN 'ITEC' THEN '-' 
                                              WHEN 'UCIC' THEN '-' 
                                              WHEN 'UCEC' THEN '-' 
                                              WHEN 'ICEC' THEN '-' 
                                              ELSE '1' END ||
                            CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                              WHEN 'F01' THEN 'A' 
                                              WHEN 'F02' THEN 'L' 
                                              WHEN 'F03' THEN 'C' 
                                              WHEN 'V00' THEN 'V' 
                                              ELSE '9' END
                    INTO P_PART_PERIODO
                    FROM STVCAMP,STVDEPT 
                    WHERE STVDEPT_CODE = P_MODALIDAD_ALUMNO AND STVCAMP_CODE = P_COD_SEDE;
                    
                    -- GET PERIODO ACTIVO (Para UVIR y UPGT se agrega parte de periodo 2)
                    SELECT COUNT(SFRRSTS_TERM_CODE) INTO P_INDICADOR FROM SFRRSTS 
                    WHERE SFRRSTS_PTRM_CODE IN (P_PART_PERIODO||'1' , (P_PART_PERIODO || CASE WHEN (P_MODALIDAD_ALUMNO='UVIR' OR P_MODALIDAD_ALUMNO='UPGT') THEN '2' ELSE '-' END) ) 
                    AND SFRRSTS_RSTS_CODE = 'RW' AND TO_DATE(TO_CHAR(:NEW.SVRSVPR_RECEPTION_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE;
                    IF P_INDICADOR = 0 THEN
                          P_PERIODO := '-';
                    ELSE
                          SELECT SFRRSTS_TERM_CODE INTO  P_PERIODO FROM (
                              -- Forma SFARSTS  ---> fechas para las partes de periodo
                              SELECT DISTINCT SFRRSTS_TERM_CODE 
                              FROM SFRRSTS
                              WHERE SFRRSTS_PTRM_CODE IN (P_PART_PERIODO||'1' , (P_PART_PERIODO || CASE WHEN (P_MODALIDAD_ALUMNO='UVIR' OR P_MODALIDAD_ALUMNO='UPGT') THEN '2' ELSE '-' END) ) 
                              AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                              AND TO_DATE(TO_CHAR(:NEW.SVRSVPR_RECEPTION_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE
                              ORDER BY SFRRSTS_TERM_CODE 
                          ) WHERE ROWNUM <= 1;

                          :NEW.SVRSVPR_TERM_CODE := P_PERIODO;
                    END IF;
                    
              END IF;
              
              V_TITUTO_SOL := 'Solicitud de Rectificaci�n de Matr�cula REZAGADOS  N� '|| P_FOLIO_SOLICITUD || ' - ' || P_NOMBRE_ALUMNO || ' - ID: '|| P_ID_ALUMNO  
                              || ' - ' || P_MODALIDAD_ALUMNO || ' - ' || P_NOMBRE_SEDE;

              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: �Solicitud de Rectificaci�n - �Nombre Alumno� - �ID Alumno� - �folio_solicitud�
                  v_Params(3).param_value := V_TITUTO_SOL;
                  v_Params(4).param_value := P_PIDM_ALUMNO;
                  v_Params(5).param_value := P_ID_ALUMNO;
                  v_Params(6).param_value := P_PERIODO;
                  v_Params(7).param_value := P_CORREO_ALUMNO;
                  v_Params(8).param_value := P_NOMBRE_ALUMNO;
                  v_Params(9).param_value := P_FOLIO_SOLICITUD;
                  v_Params(10).param_value := P_FECHA_SOLICITUD;
                  v_Params(11).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(12).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(13).param_value := P_COD_SEDE;
                  v_Params(14).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;
      
      --------------------------------------------------------------------------
      ELSIF P_SRVC_CODE = 'SOL004' THEN --########### CAMBIO DE PLAN ###########
      --------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL004');
              

              -- >> GET DEPARTAMENTO y CAMPUS --
              SELECT COUNT(*) INTO P_INDICADOR FROM SORLCUR INNER JOIN SORLFOS
              ON SORLCUR_PIDM = SORLFOS_PIDM AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
              WHERE SORLCUR_PIDM = P_PIDM_ALUMNO AND SORLCUR_LMOD_CODE = 'LEARNER' 
              AND SORLCUR_CACT_CODE   = 'ACTIVE' AND SORLCUR_CURRENT_CDE = 'Y';

              IF P_INDICADOR = 0 THEN
                    P_MODALIDAD_ALUMNO    := '-';
                    P_COD_SEDE            := '-';
                    P_NOMBRE_SEDE         := '-';
              ELSE  
                    -- GET DEPARTAMENTO y CAMPUS mas reciente del alumno 
                    SELECT SORLFOS_DEPT_CODE , SORLCUR_CAMP_CODE INTO P_MODALIDAD_ALUMNO, P_COD_SEDE FROM (
                          SELECT    SORLCUR_SEQNO,      
                                    SORLCUR_CAMP_CODE,    
                                    SORLFOS_DEPT_CODE
                          FROM SORLCUR        
                          INNER JOIN SORLFOS
                              ON SORLCUR_PIDM         = SORLFOS_PIDM 
                              AND SORLCUR_SEQNO       = SORLFOS_LCUR_SEQNO
                          WHERE SORLCUR_PIDM          = P_PIDM_ALUMNO 
                              AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                              AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                              AND SORLCUR_CURRENT_CDE = 'Y'
                          ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
                    ) WHERE ROWNUM <= 1;
                    
                    -- GET nombre campus 
                    SELECT STVCAMP_DESC INTO P_NOMBRE_SEDE FROM STVCAMP WHERE STVCAMP_CODE = P_COD_SEDE;
              END IF;
              
              
              -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
                  /*-- GET COD PARTE-PERIODO activo � un PERIODO proximo valido para realizar la solicitud (no "00")*/
              IF (P_MODALIDAD_ALUMNO = '-' OR P_COD_SEDE = '-') THEN
                  P_PERIODO := '-';
              ELSE
                    -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
                    SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                              WHEN 'UPGT' THEN 'W' 
                                              WHEN 'UREG' THEN 'R' 
                                              WHEN 'UPOS' THEN '-' 
                                              WHEN 'ITEC' THEN '-' 
                                              WHEN 'UCIC' THEN '-' 
                                              WHEN 'UCEC' THEN '-' 
                                              WHEN 'ICEC' THEN '-' 
                                              ELSE '1' END ||
                            CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                              WHEN 'F01' THEN 'A' 
                                              WHEN 'F02' THEN 'L' 
                                              WHEN 'F03' THEN 'C' 
                                              WHEN 'V00' THEN 'V' 
                                              ELSE '9' END
                    INTO P_PART_PERIODO
                    FROM STVCAMP,STVDEPT 
                    WHERE STVDEPT_CODE = P_MODALIDAD_ALUMNO AND STVCAMP_CODE = P_COD_SEDE;
                    
                    -- GET PERIODO ACTIVO 
                    SELECT COUNT(SFRRSTS_TERM_CODE) INTO P_INDICADOR FROM SFRRSTS
                    INNER JOIN SOBPTRM
                          ON SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
                          AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
                    WHERE SFRRSTS_RSTS_CODE = 'RW' 
                          AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND (TO_DATE(TO_CHAR(SFRRSTS_END_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') + 7)
                          AND SUBSTR(SOBPTRM_PTRM_CODE,3,1)<> 0 -- descartar partes de periodo '0'
                    ORDER BY SFRRSTS_TERM_CODE DESC;

                    --SELECT COUNT(SOBPTRM_TERM_CODE) INTO P_INDICADOR FROM SOBPTRM
                    --WHERE  SOBPTRM_PTRM_CODE LIKE ('' || P_PART_PERIODO || '%') 
                    --AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') <= SOBPTRM_START_DATE + 7;
                    --AND SOBPTRM_TERM_CODE NOT LIKE ('%00');--- descartar periodos "____00" (EXISTE CAMBIO DE PLAN DE ESTUDIO EN VERANO)
                      

                    IF P_INDICADOR = 0 THEN
                        P_PERIODO := '-';
                    ELSE
                        SELECT SOBPTRM_TERM_CODE INTO P_PERIODO
                        FROM (
                            -- GET COD PARTE-PERIODO activo � un PERIODO proximo valido para realizar el cambio de plan
                            SELECT SOBPTRM_TERM_CODE FROM SOBPTRM
                                   INNER JOIN SFRRSTS
                                         ON SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
                                         AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
                            WHERE SOBPTRM_PTRM_CODE LIKE ('' || P_PART_PERIODO || '%')
                                  AND SFRRSTS_RSTS_CODE = 'RW' 
                                  AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND (TO_DATE(TO_CHAR(SFRRSTS_END_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') + 7)
                                  AND SUBSTR(SOBPTRM_PTRM_CODE,3,1)<> 0 -- descartar partes de periodo '0'
                                  ORDER BY SOBPTRM_TERM_CODE ASC
                        ) WHERE ROWNUM <= 1;
                    
                        :NEW.SVRSVPR_TERM_CODE := P_PERIODO;
                    END IF;

              END IF;
              
              V_TITUTO_SOL := 'Solicitud de Cambio de Plan Estudios  N� '|| P_FOLIO_SOLICITUD || ' - ' || P_NOMBRE_ALUMNO || ' - ID: '|| P_ID_ALUMNO
                              || ' - ' || P_MODALIDAD_ALUMNO || ' - ' || P_NOMBRE_SEDE;

              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: �Solicitud de Rectificaci�n - �Nombre Alumno� - �ID Alumno� - �folio_solicitud�
                  v_Params(3).param_value := V_TITUTO_SOL ;
                  v_Params(4).param_value := P_PIDM_ALUMNO;
                  v_Params(5).param_value := P_ID_ALUMNO;
                  v_Params(6).param_value := P_PERIODO;
                  v_Params(7).param_value := P_CORREO_ALUMNO;
                  v_Params(8).param_value := P_NOMBRE_ALUMNO;
                  v_Params(9).param_value := P_FOLIO_SOLICITUD;
                  v_Params(10).param_value := P_FECHA_SOLICITUD;
                  v_Params(11).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(12).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(13).param_value := P_COD_SEDE;
                  v_Params(14).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;
              
      --------------------------------------------------------------------------
      ELSIF P_SRVC_CODE = 'SOL005' THEN --########### RESERVA ATRICULA ########### -- OBSERVACION PARA LA OBTENCION DEL PERIODO.
      --------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL005');
              
              -- >> GET DEPARTAMENTO y CAMPUS --
              SELECT COUNT(*) INTO P_INDICADOR FROM SORLCUR INNER JOIN SORLFOS
              ON SORLCUR_PIDM = SORLFOS_PIDM AND SORLCUR_SEQNO = SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE SORLCUR_PIDM = P_PIDM_ALUMNO AND SORLCUR_LMOD_CODE = 'LEARNER' 
              AND SORLCUR_CACT_CODE   = 'ACTIVE' AND SORLCUR_CURRENT_CDE = 'Y';
              
              IF P_INDICADOR = 0 THEN
                    P_MODALIDAD_ALUMNO    := '-';
                    P_COD_SEDE            := '-';
                    P_NOMBRE_SEDE         := '-';
              ELSE  
                    -- GET DEPARTAMENTO y CAMPUS mas reciente del alumno 
                    SELECT SORLFOS_DEPT_CODE , SORLCUR_CAMP_CODE INTO P_MODALIDAD_ALUMNO, P_COD_SEDE 
                    FROM(
                          SELECT    SORLCUR_SEQNO,      
                                    SORLCUR_CAMP_CODE,    
                                    SORLFOS_DEPT_CODE
                          FROM SORLCUR        
                          INNER JOIN SORLFOS
                              ON SORLCUR_PIDM         = SORLFOS_PIDM 
                              AND SORLCUR_SEQNO       = SORLFOS.SORLFOS_LCUR_SEQNO
                          WHERE SORLCUR_PIDM          = P_PIDM_ALUMNO 
                              AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                              AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                              AND SORLCUR_CURRENT_CDE = 'Y'
                          ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
                    ) WHERE ROWNUM <= 1;
                    
                    -- GET nombre campus 
                    SELECT STVCAMP_DESC INTO P_NOMBRE_SEDE FROM STVCAMP WHERE STVCAMP_CODE = P_COD_SEDE;
              END IF;
              
              -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
              IF (P_MODALIDAD_ALUMNO = '-' OR P_COD_SEDE = '-') THEN
                  P_PERIODO := '-';
              ELSE
                    -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
                    SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                              WHEN 'UPGT' THEN 'W' 
                                              WHEN 'UREG' THEN 'R' 
                                              WHEN 'UPOS' THEN '-' 
                                              WHEN 'ITEC' THEN '-' 
                                              WHEN 'UCIC' THEN '-' 
                                              WHEN 'UCEC' THEN '-' 
                                              WHEN 'ICEC' THEN '-' 
                                              ELSE '1' END ||
                            CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                              WHEN 'F01' THEN 'A' 
                                              WHEN 'F02' THEN 'L' 
                                              WHEN 'F03' THEN 'C' 
                                              WHEN 'V00' THEN 'V' 
                                              ELSE '9' END
                    INTO P_PART_PERIODO
                    FROM STVCAMP,STVDEPT 
                    WHERE STVDEPT_CODE = P_MODALIDAD_ALUMNO AND STVCAMP_CODE = P_COD_SEDE;
                    
                    -- GET PERIODO ACTIVO (Para UVIR y UPGT se agrega parte de periodo 2)
                    SELECT COUNT(SFRRSTS_TERM_CODE) INTO P_INDICADOR FROM SFRRSTS 
                    WHERE SFRRSTS_PTRM_CODE IN (P_PART_PERIODO||'1' , CASE WHEN P_MODALIDAD_ALUMNO='UVIR' OR P_MODALIDAD_ALUMNO='UPGT' THEN (P_PART_PERIODO||'2') ELSE '-' END  ) 
                    AND SFRRSTS_RSTS_CODE = 'RW' AND TO_DATE(TO_CHAR(:NEW.SVRSVPR_RECEPTION_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE;
                    IF P_INDICADOR = 0 THEN
                          P_PERIODO := '-';
                    ELSE
                          SELECT SFRRSTS_TERM_CODE INTO  P_PERIODO FROM (
                              -- Forma SFARSTS  ---> fechas para las partes de periodo
                              SELECT DISTINCT SFRRSTS_TERM_CODE 
                              FROM SFRRSTS
                              WHERE SFRRSTS_PTRM_CODE IN (P_PART_PERIODO||'1' , CASE WHEN P_MODALIDAD_ALUMNO='UVIR' OR P_MODALIDAD_ALUMNO='UPGT' THEN (P_PART_PERIODO||'2') ELSE '-' END  ) 
                              AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                              AND TO_DATE(TO_CHAR(:NEW.SVRSVPR_RECEPTION_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE
                              ORDER BY SFRRSTS_TERM_CODE
                          ) WHERE ROWNUM <= 1;

                          :NEW.SVRSVPR_TERM_CODE := P_PERIODO;
                    END IF;
                    
              END IF;
              
              V_TITUTO_SOL := 'Solicitud de Reserva de Matr�cula  N� '|| P_FOLIO_SOLICITUD || ' - ' || P_NOMBRE_ALUMNO || ' - ID: '|| P_ID_ALUMNO 
                              || ' - ' || P_MODALIDAD_ALUMNO || ' - ' || P_NOMBRE_SEDE;

              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: �Solicitud de Rectificaci�n - �Nombre Alumno� - �ID Alumno� - �folio_solicitud�
                  v_Params(3).param_value := V_TITUTO_SOL;
                  v_Params(4).param_value := P_PIDM_ALUMNO;
                  v_Params(5).param_value := P_ID_ALUMNO;
                  v_Params(6).param_value := P_PERIODO;
                  v_Params(7).param_value := P_CORREO_ALUMNO;
                  v_Params(8).param_value := P_NOMBRE_ALUMNO;
                  v_Params(9).param_value := P_FOLIO_SOLICITUD;
                  v_Params(10).param_value := P_FECHA_SOLICITUD;
                  v_Params(11).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(12).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(13).param_value := P_COD_SEDE;
                  v_Params(14).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;
      --------------------------------------------------------------------------
      ELSIF P_SRVC_CODE = 'SOL006' THEN --## SOBREPASO DE RETENCION DE DOCUMENTOS ## -- 
      --------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL006');
              
              -- >> GET DEPARTAMENTO y CAMPUS --
              SELECT COUNT(*) INTO P_INDICADOR FROM SORLCUR INNER JOIN SORLFOS
              ON SORLCUR_PIDM = SORLFOS_PIDM AND SORLCUR_SEQNO = SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE SORLCUR_PIDM = P_PIDM_ALUMNO AND SORLCUR_LMOD_CODE = 'LEARNER' 
              AND SORLCUR_CACT_CODE   = 'ACTIVE' AND SORLCUR_CURRENT_CDE = 'Y';
              
              IF P_INDICADOR = 0 THEN
                    P_MODALIDAD_ALUMNO    := '-';
                    P_COD_SEDE            := '-';
                    P_NOMBRE_SEDE         := '-';
              ELSE  
                    -- GET DEPARTAMENTO y CAMPUS mas reciente del alumno 
                    SELECT SORLFOS_DEPT_CODE , SORLCUR_CAMP_CODE INTO P_MODALIDAD_ALUMNO, P_COD_SEDE 
                    FROM(
                          SELECT    SORLCUR_SEQNO,      
                                    SORLCUR_CAMP_CODE,    
                                    SORLFOS_DEPT_CODE
                          FROM SORLCUR        
                          INNER JOIN SORLFOS
                              ON SORLCUR_PIDM         = SORLFOS_PIDM 
                              AND SORLCUR_SEQNO       = SORLFOS.SORLFOS_LCUR_SEQNO
                          WHERE SORLCUR_PIDM          = P_PIDM_ALUMNO 
                              AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                              AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                              AND SORLCUR_CURRENT_CDE = 'Y'
                          ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
                    ) WHERE ROWNUM <= 1;
                    
                    -- GET nombre campus 
                    SELECT STVCAMP_DESC INTO P_NOMBRE_SEDE FROM STVCAMP WHERE STVCAMP_CODE = P_COD_SEDE;
              END IF;
              
              -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
              IF (P_MODALIDAD_ALUMNO = '-' OR P_COD_SEDE = '-') THEN
                  P_PERIODO := '-';
              ELSE
                    -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
                    SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                              WHEN 'UPGT' THEN 'W' 
                                              WHEN 'UREG' THEN 'R' 
                                              WHEN 'UPOS' THEN '-' 
                                              WHEN 'ITEC' THEN '-' 
                                              WHEN 'UCIC' THEN '-' 
                                              WHEN 'UCEC' THEN '-' 
                                              WHEN 'ICEC' THEN '-' 
                                              ELSE '1' END ||
                            CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                              WHEN 'F01' THEN 'A' 
                                              WHEN 'F02' THEN 'L' 
                                              WHEN 'F03' THEN 'C' 
                                              WHEN 'V00' THEN 'V' 
                                              ELSE '9' END
                    INTO P_PART_PERIODO
                    FROM STVCAMP,STVDEPT 
                    WHERE STVDEPT_CODE = P_MODALIDAD_ALUMNO AND STVCAMP_CODE = P_COD_SEDE;
                    
                    -- GET PERIODO ACTIVO 
                    SELECT COUNT(SFRRSTS_TERM_CODE) INTO P_INDICADOR FROM SFRRSTS WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  ) 
                    AND SFRRSTS_RSTS_CODE = 'RW' AND :NEW.SVRSVPR_RECEPTION_DATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE;
                    IF P_INDICADOR = 0 THEN
                          P_PERIODO := '-';
                    ELSE
                          SELECT SFRRSTS_TERM_CODE INTO  P_PERIODO FROM (
                              -- Forma SFARSTS  ---> fechas para las partes de periodo
                              SELECT DISTINCT SFRRSTS_TERM_CODE 
                              FROM SFRRSTS
                              WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  )
                              AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                              AND :NEW.SVRSVPR_RECEPTION_DATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE
                              ORDER BY SFRRSTS_TERM_CODE DESC
                          ) WHERE ROWNUM <= 1;

                          :NEW.SVRSVPR_TERM_CODE := P_PERIODO;
                    END IF;
                    
              END IF;
              
              V_TITUTO_SOL := 'Solicitud de Sobrepaso de Retenci�n de Documentos  N� '|| P_FOLIO_SOLICITUD || ' - ' || P_NOMBRE_ALUMNO || ' - ID: '|| P_ID_ALUMNO
                              || ' - ' || P_MODALIDAD_ALUMNO || ' - ' || P_NOMBRE_SEDE;

              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: �Solicitud de Rectificaci�n - �Nombre Alumno� - �ID Alumno� - �folio_solicitud�
                  v_Params(3).param_value := V_TITUTO_SOL;
                  v_Params(4).param_value := P_PIDM_ALUMNO;
                  v_Params(5).param_value := P_ID_ALUMNO;
                  v_Params(6).param_value := P_PERIODO;
                  v_Params(7).param_value := P_CORREO_ALUMNO;
                  v_Params(8).param_value := P_NOMBRE_ALUMNO;
                  v_Params(9).param_value := P_FOLIO_SOLICITUD;
                  v_Params(10).param_value := P_FECHA_SOLICITUD;
                  v_Params(11).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(12).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(13).param_value := P_COD_SEDE;
                  v_Params(14).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;
      --------------------------------------------------------------------------
      ELSIF P_SRVC_CODE = 'SOL007' THEN --## SOLICITUD DE MODIFICACI�N DE LA CUOTA INICIAL ##
      --------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL007');
              
              -- >> GET DEPARTAMENTO y CAMPUS --
              SELECT COUNT(*) INTO P_INDICADOR FROM SORLCUR INNER JOIN SORLFOS
              ON SORLCUR_PIDM = SORLFOS_PIDM AND SORLCUR_SEQNO = SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE SORLCUR_PIDM = P_PIDM_ALUMNO AND SORLCUR_LMOD_CODE = 'LEARNER' 
              AND SORLCUR_CACT_CODE   = 'ACTIVE' AND SORLCUR_CURRENT_CDE = 'Y';
              
              IF P_INDICADOR = 0 THEN
                    P_MODALIDAD_ALUMNO    := '-';
                    P_COD_SEDE            := '-';
                    P_NOMBRE_SEDE         := '-';
              ELSE  
                    -- GET DEPARTAMENTO y CAMPUS mas reciente del alumno 
                    SELECT SORLFOS_DEPT_CODE , SORLCUR_CAMP_CODE INTO P_MODALIDAD_ALUMNO, P_COD_SEDE 
                    FROM(
                          SELECT    SORLCUR_SEQNO,      
                                    SORLCUR_CAMP_CODE,    
                                    SORLFOS_DEPT_CODE
                          FROM SORLCUR        
                          INNER JOIN SORLFOS
                              ON SORLCUR_PIDM         = SORLFOS_PIDM 
                              AND SORLCUR_SEQNO       = SORLFOS.SORLFOS_LCUR_SEQNO
                          WHERE SORLCUR_PIDM          = P_PIDM_ALUMNO 
                              AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                              AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                              AND SORLCUR_CURRENT_CDE = 'Y'
                          ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
                    ) WHERE ROWNUM <= 1;
                    
                    -- GET nombre campus 
                    SELECT STVCAMP_DESC INTO P_NOMBRE_SEDE FROM STVCAMP WHERE STVCAMP_CODE = P_COD_SEDE;
              END IF;
              
              -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
              IF (P_MODALIDAD_ALUMNO = '-' OR P_COD_SEDE = '-') THEN
                  P_PERIODO := '-';
              ELSE
                    -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
                    SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                              WHEN 'UPGT' THEN 'W' 
                                              WHEN 'UREG' THEN 'R' 
                                              WHEN 'UPOS' THEN '-' 
                                              WHEN 'ITEC' THEN '-' 
                                              WHEN 'UCIC' THEN '-' 
                                              WHEN 'UCEC' THEN '-' 
                                              WHEN 'ICEC' THEN '-' 
                                              ELSE '1' END ||
                            CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                              WHEN 'F01' THEN 'A' 
                                              WHEN 'F02' THEN 'L' 
                                              WHEN 'F03' THEN 'C' 
                                              WHEN 'V00' THEN 'V' 
                                              ELSE '9' END
                    INTO P_PART_PERIODO
                    FROM STVCAMP,STVDEPT 
                    WHERE STVDEPT_CODE = P_MODALIDAD_ALUMNO AND STVCAMP_CODE = P_COD_SEDE;
                    
                    -- GET PERIODO ACTIVO 
                    SELECT COUNT(SFRRSTS_TERM_CODE) INTO P_INDICADOR FROM SFRRSTS WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  ) 
                    AND SFRRSTS_RSTS_CODE = 'RW' AND :NEW.SVRSVPR_RECEPTION_DATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE;
                    IF P_INDICADOR = 0 THEN
                          P_PERIODO := '-';
                    ELSE
                          SELECT SFRRSTS_TERM_CODE INTO  P_PERIODO FROM (
                              -- Forma SFARSTS  ---> fechas para las partes de periodo
                              SELECT DISTINCT SFRRSTS_TERM_CODE 
                              FROM SFRRSTS
                              WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  )
                              AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                              AND :NEW.SVRSVPR_RECEPTION_DATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE
                              ORDER BY SFRRSTS_TERM_CODE DESC
                          ) WHERE ROWNUM <= 1;

                          :NEW.SVRSVPR_TERM_CODE := P_PERIODO;
                    END IF;
                    
              END IF;
              
              V_TITUTO_SOL := 'Solicitud de Modificaci�n de la Cuota Inicial  N� '|| P_FOLIO_SOLICITUD || ' - ' || P_NOMBRE_ALUMNO || ' - ID: '|| P_ID_ALUMNO
                              || ' - ' || P_MODALIDAD_ALUMNO || ' - ' || P_NOMBRE_SEDE;

              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: �Solicitud de Rectificaci�n - �Nombre Alumno� - �ID Alumno� - �folio_solicitud�
                  v_Params(3).param_value := V_TITUTO_SOL;
                  v_Params(4).param_value := P_PIDM_ALUMNO;
                  v_Params(5).param_value := P_ID_ALUMNO;
                  v_Params(6).param_value := P_PERIODO;
                  v_Params(7).param_value := P_CORREO_ALUMNO;
                  v_Params(8).param_value := P_NOMBRE_ALUMNO;
                  v_Params(9).param_value := P_FOLIO_SOLICITUD;
                  v_Params(10).param_value := P_FECHA_SOLICITUD;
                  v_Params(11).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(12).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(13).param_value := P_COD_SEDE;
                  v_Params(14).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;
              
      --------------------------------------------------------------------------
      ELSIF P_SRVC_CODE = 'SOL010' THEN --## SOLICITUD DE PRORROGA SIMPLE (CONTISPS) ## 
      --------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL010');

              BWZKSVPR.P_CONTISPGA_GETDATA(P_PIDM_ALUMNO,:NEW.SVRSVPR_RECEPTION_DATE,P_MODALIDAD_ALUMNO,P_COD_SEDE,P_NOMBRE_SEDE,P_PERIODO);

              IF P_PERIODO <> '-' THEN
                    :NEW.SVRSVPR_TERM_CODE := P_PERIODO;
              END IF;
              
              V_TITUTO_SOL := 'Solicitud de Pr�rroga N� '|| P_FOLIO_SOLICITUD || ' - ' || P_NOMBRE_ALUMNO || ' - ID: '|| P_ID_ALUMNO
                              || ' - ' || P_MODALIDAD_ALUMNO || ' - ' || P_NOMBRE_SEDE;

              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: �Solicitud de Rectificaci�n - �Nombre Alumno� - �ID Alumno� - �folio_solicitud�
                  v_Params(3).param_value := V_TITUTO_SOL;
                  v_Params(4).param_value := P_PIDM_ALUMNO;
                  v_Params(5).param_value := P_ID_ALUMNO;
                  v_Params(6).param_value := P_PERIODO;
                  v_Params(7).param_value := P_CORREO_ALUMNO;
                  v_Params(8).param_value := P_NOMBRE_ALUMNO;
                  v_Params(9).param_value := P_FOLIO_SOLICITUD;
                  v_Params(10).param_value := P_FECHA_SOLICITUD;
                  v_Params(11).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(12).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(13).param_value := P_COD_SEDE;
                  v_Params(14).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;

      --------------------------------------------------------------------------
      ELSIF P_SRVC_CODE = 'SOL012' THEN --## SOLICITUD DE SEGURO MEDICO ##
      --------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL012');
              
              -- >> GET DEPARTAMENTO y CAMPUS --
              SELECT COUNT(*) INTO P_INDICADOR FROM SORLCUR INNER JOIN SORLFOS
              ON SORLCUR_PIDM = SORLFOS_PIDM AND SORLCUR_SEQNO = SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE SORLCUR_PIDM = P_PIDM_ALUMNO AND SORLCUR_LMOD_CODE = 'LEARNER' 
              AND SORLCUR_CACT_CODE   = 'ACTIVE' AND SORLCUR_CURRENT_CDE = 'Y';
              
              IF P_INDICADOR = 0 THEN
                    P_MODALIDAD_ALUMNO    := '-';
                    P_COD_SEDE            := '-';
                    P_NOMBRE_SEDE         := '-';
              ELSE  
                    -- GET DEPARTAMENTO y CAMPUS mas reciente del alumno 
                    SELECT SORLFOS_DEPT_CODE , SORLCUR_CAMP_CODE INTO P_MODALIDAD_ALUMNO, P_COD_SEDE 
                    FROM(
                          SELECT    SORLCUR_SEQNO,      
                                    SORLCUR_CAMP_CODE,    
                                    SORLFOS_DEPT_CODE
                          FROM SORLCUR        
                          INNER JOIN SORLFOS
                              ON SORLCUR_PIDM         = SORLFOS_PIDM 
                              AND SORLCUR_SEQNO       = SORLFOS.SORLFOS_LCUR_SEQNO
                          WHERE SORLCUR_PIDM          = P_PIDM_ALUMNO 
                              AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                              AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                              AND SORLCUR_CURRENT_CDE = 'Y'
                          ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
                    ) WHERE ROWNUM <= 1;
                    
                    -- GET nombre campus 
                    SELECT STVCAMP_DESC INTO P_NOMBRE_SEDE FROM STVCAMP WHERE STVCAMP_CODE = P_COD_SEDE;
              END IF;
              
              -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
              IF (P_MODALIDAD_ALUMNO = '-' OR P_COD_SEDE = '-') THEN
                  P_PERIODO := '-';
              ELSE
                    -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
                    SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                              WHEN 'UPGT' THEN 'W' 
                                              WHEN 'UREG' THEN 'R' 
                                              WHEN 'UPOS' THEN '-' 
                                              WHEN 'ITEC' THEN '-' 
                                              WHEN 'UCIC' THEN '-' 
                                              WHEN 'UCEC' THEN '-' 
                                              WHEN 'ICEC' THEN '-' 
                                              ELSE '1' END ||
                            CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                              WHEN 'F01' THEN 'A' 
                                              WHEN 'F02' THEN 'L' 
                                              WHEN 'F03' THEN 'C' 
                                              WHEN 'V00' THEN 'V' 
                                              ELSE '9' END
                    INTO P_PART_PERIODO
                    FROM STVCAMP,STVDEPT 
                    WHERE STVDEPT_CODE = P_MODALIDAD_ALUMNO AND STVCAMP_CODE = P_COD_SEDE;
                    
                    -- GET PERIODO ACTIVO 
                    SELECT COUNT(SFRRSTS_TERM_CODE) INTO P_INDICADOR FROM SFRRSTS WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  ) 
                    AND SFRRSTS_RSTS_CODE = 'RW' AND :NEW.SVRSVPR_RECEPTION_DATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE;
                    IF P_INDICADOR = 0 THEN
                          P_PERIODO := '-';
                    ELSE
                          SELECT SFRRSTS_TERM_CODE INTO  P_PERIODO FROM (
                              -- Forma SFARSTS  ---> fechas para las partes de periodo
                              SELECT DISTINCT SFRRSTS_TERM_CODE 
                              FROM SFRRSTS
                              WHERE SFRRSTS_PTRM_CODE LIKE (P_PART_PERIODO || '1'  )
                              AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                              AND :NEW.SVRSVPR_RECEPTION_DATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE
                              ORDER BY SFRRSTS_TERM_CODE DESC
                          ) WHERE ROWNUM <= 1;

                          :NEW.SVRSVPR_TERM_CODE := P_PERIODO;
                    END IF;
                    
              END IF;
              
              V_TITUTO_SOL := 'Solicitud de Exoneraci�n de seguro de Salud  N� '|| P_FOLIO_SOLICITUD || ' - ' || P_NOMBRE_ALUMNO || ' - ID: '|| P_ID_ALUMNO
                              || ' - ' || P_MODALIDAD_ALUMNO || ' - ' || P_NOMBRE_SEDE;

              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: �Solicitud de Rectificaci�n - �Nombre Alumno� - �ID Alumno� - �folio_solicitud�
                  v_Params(3).param_value := V_TITUTO_SOL;
                  v_Params(4).param_value := P_PIDM_ALUMNO;
                  v_Params(5).param_value := P_ID_ALUMNO;
                  v_Params(6).param_value := P_PERIODO;
                  v_Params(7).param_value := P_CORREO_ALUMNO;
                  v_Params(8).param_value := P_NOMBRE_ALUMNO;
                  v_Params(9).param_value := P_FOLIO_SOLICITUD;
                  v_Params(10).param_value := P_FECHA_SOLICITUD;
                  v_Params(11).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(12).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(13).param_value := P_COD_SEDE;
                  v_Params(14).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;
              
      --------------------------------------------------------------------------
      ELSIF P_SRVC_CODE = 'SOL013' THEN --## SOLICITUD DE TRASLADO INTERNO (CONTISTI) ## 
      --------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL013');

              BWZKSVPR.P_CONTISTI_GETDATA(P_PIDM_ALUMNO,:NEW.SVRSVPR_RECEPTION_DATE,P_MODALIDAD_ALUMNO,P_COD_SEDE,P_NOMBRE_SEDE,P_PERIODO);

              IF P_PERIODO <> '-' THEN
                    :NEW.SVRSVPR_TERM_CODE := P_PERIODO;
              END IF;
              
              V_TITUTO_SOL := 'Solicitud de Traslado Interno N� '|| P_FOLIO_SOLICITUD || ' - ' || P_NOMBRE_ALUMNO || ' - ID: '|| P_ID_ALUMNO
                              || ' - ' || P_MODALIDAD_ALUMNO || ' - ' || P_NOMBRE_SEDE;

              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: �Solicitud de Rectificaci�n - �Nombre Alumno� - �ID Alumno� - �folio_solicitud�
                  v_Params(3).param_value := V_TITUTO_SOL;
                  v_Params(4).param_value := P_PIDM_ALUMNO;
                  v_Params(5).param_value := P_ID_ALUMNO;
                  v_Params(6).param_value := P_PERIODO;
                  v_Params(7).param_value := P_CORREO_ALUMNO;
                  v_Params(8).param_value := P_NOMBRE_ALUMNO;
                  v_Params(9).param_value := P_FOLIO_SOLICITUD;
                  v_Params(10).param_value := P_FECHA_SOLICITUD;
                  v_Params(11).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(12).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(13).param_value := P_COD_SEDE;
                  v_Params(14).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;
      
      --------------------------------------------------------------------------
      ELSIF P_SRVC_CODE = 'SOL017' THEN --## SOLICITUD DE PRONTO PAGO (CONTISPP) ## 
      --------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL017');

              BWZKSVPR.P_CONTISPP_GETDATA(P_PIDM_ALUMNO,:NEW.SVRSVPR_RECEPTION_DATE,P_MODALIDAD_ALUMNO,P_COD_SEDE,P_NOMBRE_SEDE,P_PERIODO);

              IF P_PERIODO <> '-' THEN
                    :NEW.SVRSVPR_TERM_CODE := P_PERIODO;
              END IF;
              
              V_TITUTO_SOL := 'Solicitud de descuento por Pronto Pago N� '|| P_FOLIO_SOLICITUD || ' - ' || P_NOMBRE_ALUMNO || ' - ID: '|| P_ID_ALUMNO
                              || ' - ' || P_MODALIDAD_ALUMNO || ' - ' || P_NOMBRE_SEDE;

              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: �Solicitud de Rectificaci�n - �Nombre Alumno� - �ID Alumno� - �folio_solicitud�
                  v_Params(3).param_value := V_TITUTO_SOL;
                  v_Params(4).param_value := P_PIDM_ALUMNO;
                  v_Params(5).param_value := P_ID_ALUMNO;
                  v_Params(6).param_value := P_PERIODO;
                  v_Params(7).param_value := P_CORREO_ALUMNO;
                  v_Params(8).param_value := P_NOMBRE_ALUMNO;
                  v_Params(9).param_value := P_FOLIO_SOLICITUD;
                  v_Params(10).param_value := P_FECHA_SOLICITUD;
                  v_Params(11).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(12).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(13).param_value := P_COD_SEDE;
                  v_Params(14).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;
      --            
      --------------------------------------------------------------------------
      ELSIF P_SRVC_CODE = 'SOL018' THEN --## TR�MITE DE PAGOS DE BACHILLER Y T�TULO (CONTISPBT) ## 
      --------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL018');

              BWZKSVPR.P_CONTISPBT_GETDATA(P_PIDM_ALUMNO,:NEW.SVRSVPR_RECEPTION_DATE,P_MODALIDAD_ALUMNO,P_COD_SEDE,P_NOMBRE_SEDE,P_PERIODO);

              IF P_PERIODO <> '-' THEN
                    :NEW.SVRSVPR_TERM_CODE := P_PERIODO;
              END IF;
              
              V_TITUTO_SOL := 'Solicitud de Tr�mite de pagos de Bachiller - T�tulo N� '|| P_FOLIO_SOLICITUD || ' - ' || P_NOMBRE_ALUMNO || ' - ID: '|| P_ID_ALUMNO
                              || ' - ' || P_MODALIDAD_ALUMNO || ' - ' || P_NOMBRE_SEDE;

              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: �Solicitud de Rectificaci�n - �Nombre Alumno� - �ID Alumno� - �folio_solicitud�
                  v_Params(3).param_value := V_TITUTO_SOL;
                  v_Params(4).param_value := P_PIDM_ALUMNO;
                  v_Params(5).param_value := P_ID_ALUMNO;
                  v_Params(6).param_value := P_PERIODO;
                  v_Params(7).param_value := P_CORREO_ALUMNO;
                  v_Params(8).param_value := P_NOMBRE_ALUMNO;
                  v_Params(9).param_value := P_FOLIO_SOLICITUD;
                  v_Params(10).param_value := P_FECHA_SOLICITUD;
                  v_Params(11).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(12).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(13).param_value := P_COD_SEDE;
                  v_Params(14).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;
--            
      --------------------------------------------------------------------------
      ELSIF P_SRVC_CODE = 'SOL019' THEN --## SOLICITUD DE DESCUENTO POR CONVENIO (CONTISDPC) ## 
      --------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL019');

              BWZKSVPR.P_CONTISDPC_GETDATA(P_PIDM_ALUMNO,:NEW.SVRSVPR_RECEPTION_DATE,P_MODALIDAD_ALUMNO,P_COD_SEDE,P_NOMBRE_SEDE,P_PERIODO);

              IF P_PERIODO <> '-' THEN
                    :NEW.SVRSVPR_TERM_CODE := P_PERIODO;
              END IF;
              
              V_TITUTO_SOL := 'Solicitud de Descuento por Convenio - T�tulo N� '|| P_FOLIO_SOLICITUD || ' - ' || P_NOMBRE_ALUMNO || ' - ID: '|| P_ID_ALUMNO
                              || ' - ' || P_MODALIDAD_ALUMNO || ' - ' || P_NOMBRE_SEDE;

              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: �Solicitud de Rectificaci�n - �Nombre Alumno� - �ID Alumno� - �folio_solicitud�
                  v_Params(3).param_value := V_TITUTO_SOL;
                  v_Params(4).param_value := P_PIDM_ALUMNO;
                  v_Params(5).param_value := P_ID_ALUMNO;
                  v_Params(6).param_value := P_PERIODO;
                  v_Params(7).param_value := P_CORREO_ALUMNO;
                  v_Params(8).param_value := P_NOMBRE_ALUMNO;
                  v_Params(9).param_value := P_FOLIO_SOLICITUD;
                  v_Params(10).param_value := P_FECHA_SOLICITUD;
                  v_Params(11).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(12).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(13).param_value := P_COD_SEDE;
                  v_Params(14).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;
      
      --------------------------------------------------------------------------
      ELSIF P_SRVC_CODE = 'SOL020' THEN --## SOLICITUD DE MODIFICACI�N DE CUOTA INICIAL - VERANO (CONTISMCV) ## 
      --------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL020');

              BWZKSVPR.P_CONTISMCV_GETDATA(P_PIDM_ALUMNO,:NEW.SVRSVPR_RECEPTION_DATE,P_MODALIDAD_ALUMNO,P_COD_SEDE,P_NOMBRE_SEDE,P_PERIODO);

              IF P_PERIODO <> '-' THEN
                    :NEW.SVRSVPR_TERM_CODE := P_PERIODO;
              END IF;
              
              V_TITUTO_SOL := 'Solicitud de Modificaci�n de Cuota Inicial - Verano N� '|| P_FOLIO_SOLICITUD || ' - ' || P_NOMBRE_ALUMNO || ' - ID: '|| P_ID_ALUMNO
                              || ' - ' || P_MODALIDAD_ALUMNO || ' - ' || P_NOMBRE_SEDE;

              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: �Solicitud de Rectificaci�n - �Nombre Alumno� - �ID Alumno� - �folio_solicitud�
                  v_Params(3).param_value := V_TITUTO_SOL;
                  v_Params(4).param_value := P_PIDM_ALUMNO;
                  v_Params(5).param_value := P_ID_ALUMNO;
                  v_Params(6).param_value := P_PERIODO;
                  v_Params(7).param_value := P_CORREO_ALUMNO;
                  v_Params(8).param_value := P_NOMBRE_ALUMNO;
                  v_Params(9).param_value := P_FOLIO_SOLICITUD;
                  v_Params(10).param_value := P_FECHA_SOLICITUD;
                  v_Params(11).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(12).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(13).param_value := P_COD_SEDE;
                  v_Params(14).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;
      END IF;
END;

