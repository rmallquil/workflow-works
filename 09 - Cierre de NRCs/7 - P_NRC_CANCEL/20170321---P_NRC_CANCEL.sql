/*
SET SERVEROUTPUT ON
DECLARE   P_RESP                 VARCHAR2(100);
BEGIN
    P_NRC_CANCEL('201710','1199','A',5);
    DBMS_OUTPUT.PUT_LINE( '-- ok');
END;
*/


CREATE OR REPLACE PROCEDURE P_NRC_CANCEL (
    P_PERIODO         IN STVTERM.STVTERM_CODE%TYPE,
    P_NRC             IN SSBSECT.SSBSECT_CRN%TYPE,
    P_OLD_SSTS_CODE   IN SSBSECT.SSBSECT_SSTS_CODE%TYPE,
    P_MAXENROL        IN SSBSECT.SSBSECT_MAX_ENRL%TYPE
)
/* ===================================================================================================================
  NOMBRE    : P_NRC_CANCEL
  FECHA     : 21/03/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Restaura el ESTADO y # de VACANTES del NRC al a uno anterior de solicitar su cierre.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */
AS
          
BEGIN
     
    -- CERRAR (C) NRC 
    UPDATE SSBSECT
    SET   SSBSECT_SSTS_CODE     = P_OLD_SSTS_CODE,  -- Estado
          SSBSECT_MAX_ENRL      = P_MAXENROL,   -- 
          SSBSECT_SEATS_AVAIL   = P_MAXENROL - SSBSECT_ENRL,
          SSBSECT_ACTIVITY_DATE = SYSDATE,
          SSBSECT_DATA_ORIGIN   = 'WorkFlow',
          SSBSECT_USER_ID       = USER
    WHERE SSBSECT_TERM_CODE = P_PERIODO
    AND   SSBSECT_CRN = P_NRC
    AND   P_OLD_SSTS_CODE IN (SELECT STVSSTS_CODE FROM STVSSTS)
    AND   P_MAXENROL > 0; 
    
    COMMIT;
    
EXCEPTION
  WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END P_NRC_CANCEL;