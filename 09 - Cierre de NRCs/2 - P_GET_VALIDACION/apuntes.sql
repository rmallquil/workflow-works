
SET SERVEROUTPUT ON;
DECLARE 
      P_PERIODO         STVTERM.STVTERM_CODE%TYPE := '201710' ;
      P_NRC_IN          SSBSECT.SSBSECT_CRN%TYPE := '1199'    ; -- 1175 1199 1004 1114
      P_ERROR           VARCHAR2(2000);
      V_INDICADOR       NUMBER;
      E_INVALID         EXCEPTION;
      V_LIGA            NUMBER;
      V_SEQ_NUMB        SSBSECT.SSBSECT_SEQ_NUMB%TYPE;
      V_LIGA_IDENT      SSBSECT.SSBSECT_LINK_IDENT%TYPE;
      V_LIGA_CONEC      SSRLINK.SSRLINK_LINK_CONN%TYPE;
BEGIN
    
    -- VALIDAR NRC CUMPLA CON LO NECESARIO (**)
    SELECT COUNT(*) INTO V_INDICADOR 
    FROM SSBSECT 
    WHERE SSBSECT_TERM_CODE = P_PERIODO 
    AND   SSBSECT_CRN = P_NRC_IN
    AND   SSBSECT_MAX_ENRL = '0'   -- ** Maximo de vacante
    AND   SSBSECT_SSTS_CODE = 'P'; -- ** Estado 'P' por cerrar
    IF (V_INDICADOR = 0 ) THEN
        --RAISE E_INVALID;
        DBMS_OUTPUT.PUT_LINE( 'No se detecto el NRC o no cumple con algun requisito para cerrarse.');
    END IF;
    
    -- Get si NRC es: SIMPLE, PADRE, HIJO.
    SELECT  
          CASE  WHEN SUBSTR(SSBSECT_SEQ_NUMB,1,2) = SSRLINK_LINK_CONN   THEN 1 -- Indent <-> Conector (NRC HIJO)
                WHEN SUBSTR(SSBSECT_SEQ_NUMB,1,2) = SSBSECT_LINK_IDENT  THEN 2 -- Seccion <-> Conector (NRC PADRE)
                WHEN NVL(TRIM(SSRLINK_LINK_CONN),'0') = '0'             THEN 3 -- (NRC SIMPLE)
                ELSE 0 END,
          SSBSECT_SEQ_NUMB, -- Seccion
          SSBSECT_LINK_IDENT, -- IDEN_LIGA -- IDENTIFICADOR LIGA
          SSRLINK_LINK_CONN -- CONE_LIGA
    INTO  V_LIGA,
          V_SEQ_NUMB,
          V_LIGA_IDENT,
          V_LIGA_CONEC
    FROM SSBSECT -- NRCs
    LEFT JOIN SSRLINK -- CONECTOR LIGAS
      ON SSRLINK_TERM_CODE = SSBSECT_TERM_CODE
      AND SSRLINK_CRN = SSBSECT_CRN
    WHERE  SSBSECT_TERM_CODE = P_PERIODO AND SSBSECT_CRN = P_NRC_IN;
    
    DBMS_OUTPUT.PUT_LINE(V_LIGA||' - '||V_SEQ_NUMB||' - '||V_LIGA_IDENT||' - '||V_LIGA_CONEC);
    
EXCEPTION
    WHEN E_INVALID THEN
        P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '|| 'No se detecto el NRC o no cumple con algun requisito para cerrarse.';
    WHEN OTHERS THEN
        P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
        DBMS_OUTPUT.PUT_LINE(P_ERROR);
END;