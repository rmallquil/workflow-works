create or replace PACKAGE BODY WFK_CONTISCNRC AS
/*******************************************************************************
 WFK_CONTISESM:
       Conti Package SOLICITUD DE CIERRER DE NRC
*******************************************************************************/
-- FILE NAME..: WFK_CONTISCNRC.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_CONTISCNRC
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2017
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/


PROCEDURE P_GET_VALIDACION (
      P_PERIODO         IN STVTERM.STVTERM_CODE%TYPE,
      P_NRC             IN SSBSECT.SSBSECT_CRN%TYPE,
      P_LIGA            OUT NUMBER,
      P_LIGA_IDENT      OUT SSBSECT.SSBSECT_LINK_IDENT%TYPE,
      P_LIGA_CONEC      OUT SSRLINK.SSRLINK_LINK_CONN%TYPE,
      P_NUM_CURSO       OUT SSBSECT.SSBSECT_CRSE_NUMB%TYPE,
      P_TITULO          OUT SCRSYLN.SCRSYLN_LONG_COURSE_TITLE%TYPE,
      P_SECCION         OUT SSBSECT.SSBSECT_SEQ_NUMB%TYPE,
      P_ENROLADOS       OUT SSBSECT.SSBSECT_ENRL%TYPE,
      P_CAMP_DESC       OUT STVCAMP.STVCAMP_DESC%TYPE,
      P_PTRM_CODE       OUT SSBSECT.SSBSECT_PTRM_CODE%TYPE,
      P_GTVINSM_DESC    OUT GTVINSM.GTVINSM_DESC%TYPE,
      P_INSTRUC_ID      OUT SPRIDEN.SPRIDEN_ID%TYPE,
      P_INSTRUC_NAME    OUT VARCHAR2,
      P_ERROR           OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_VALIDACION
  FECHA     : 03/03/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida y obtiene los datos y calcula si NRC es Hijo,Padre o un NRC sin ligados.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */
AS
      V_INDICADOR       NUMBER;
      E_INVALID         EXCEPTION;
      V_SUBJ_CODE       SSBSECT.SSBSECT_SUBJ_CODE%TYPE;
      V_CAMP_CODE       SSBSECT.SSBSECT_CAMP_CODE%TYPE;
      V_INSM_CODE       SSBSECT.SSBSECT_INSM_CODE%TYPE;
      
      V_SCRSYLN_REC     SCRSYLN%ROWTYPE;

      -- GET SCRSYLN -
      CURSOR C_SCRSYLN IS
      SELECT SCRSYLN_LONG_COURSE_TITLE 
      FROM (
        SELECT SCRSYLN_LONG_COURSE_TITLE FROM SCRSYLN 
        WHERE SCRSYLN_SUBJ_CODE = V_SUBJ_CODE 
        AND SCRSYLN_CRSE_NUMB = P_NUM_CURSO 
        AND SCRSYLN_TERM_CODE_END IS NULL 
        ORDER BY SCRSYLN_TERM_CODE_EFF DESC
      ) WHERE ROWNUM = 1;

      -- GET DATOS DEL DOCENTE - TOP 1 -
      CURSOR C_SPRIDEN IS
      SELECT SPRIDEN_ID ,  NOMBRES
      FROM (
          SELECT SPRIDEN_ID, SPRIDEN_LAST_NAME || ', ' || SPRIDEN_FIRST_NAME AS NOMBRES
          FROM SPRIDEN 
          INNER JOIN SIRASGN 
          ON SPRIDEN_PIDM = SIRASGN_PIDM
          WHERE  SIRASGN_TERM_CODE = P_PERIODO
          AND SIRASGN_CRN = P_NRC 
          ORDER BY SIRASGN_CATEGORY
      ) WHERE ROWNUM = 1;
BEGIN
    
      -- VALIDAR NRC CUMPLA CON LO NECESARIO (**)
      SELECT COUNT(*) INTO V_INDICADOR 
      FROM SSBSECT 
      WHERE SSBSECT_TERM_CODE = P_PERIODO 
      AND   SSBSECT_CRN = P_NRC
      AND   SSBSECT_MAX_ENRL = '0'   -- ** Maximo de vacante
      AND   SSBSECT_SSTS_CODE = 'P'; -- ** Estado 'P' por cerrar
      
      -- Get si NRC es: SIMPLE, PADRE, HIJO.
      SELECT COUNT(*) INTO P_LIGA FROM SSBSECT -- NRCs
      LEFT JOIN SSRLINK ON SSRLINK_TERM_CODE = SSBSECT_TERM_CODE AND SSRLINK_CRN = SSBSECT_CRN
      WHERE  SSBSECT_TERM_CODE = P_PERIODO AND SSBSECT_CRN = P_NRC;
      IF (P_LIGA > 0) THEN
          SELECT  
                CASE  WHEN SUBSTR(SSBSECT_SEQ_NUMB,1,2) = SSRLINK_LINK_CONN AND NVL(TRIM(SSBSECT_LINK_IDENT),'FALSE') <> 'FALSE' THEN 1 -- Indent <-> Conector (NRC HIJO)
                      WHEN SUBSTR(SSBSECT_SEQ_NUMB,1,2) = SSBSECT_LINK_IDENT AND NVL(TRIM(SSRLINK_LINK_CONN),'FALSE') <> 'FALSE' THEN 2 -- Seccion <-> Conector (NRC PADRE)
                      WHEN NVL(TRIM(SSRLINK_LINK_CONN),'0') = '0' AND NVL(TRIM(SSBSECT_LINK_IDENT),'0') = '0'             THEN 3 -- (NRC SIMPLE)
                      ELSE 0 END,
                SSBSECT_SUBJ_CODE,  -- Materia -->  Catalogo
                SSBSECT_CRSE_NUMB,  -- Numero Curso
                SSBSECT_SEQ_NUMB,   -- Seccion
                SSBSECT_INSM_CODE,  -- Metodo educativo
                SSBSECT_LINK_IDENT, -- IDEN_LIGA -- IDENTIFICADOR LIGA
                SSRLINK_LINK_CONN,  -- CONE_LIGA
                SSBSECT_ENRL,       -- N Enrolados (Matriculados)
                SSBSECT_CAMP_CODE,  -- Campus
                SSBSECT_PTRM_CODE   -- Parte Periodo
          INTO  P_LIGA,
                V_SUBJ_CODE,
                P_NUM_CURSO,
                P_SECCION,
                V_INSM_CODE,
                P_LIGA_IDENT,
                P_LIGA_CONEC,
                P_ENROLADOS,
                V_CAMP_CODE,
                P_PTRM_CODE
          FROM SSBSECT -- NRCs
          LEFT JOIN SSRLINK -- CONECTOR LIGAS
            ON SSRLINK_TERM_CODE = SSBSECT_TERM_CODE
            AND SSRLINK_CRN = SSBSECT_CRN
          WHERE  SSBSECT_TERM_CODE = P_PERIODO AND SSBSECT_CRN = P_NRC;
          
          -- Metodo educativo
          SELECT GTVINSM_DESC INTO P_GTVINSM_DESC FROM GTVINSM WHERE GTVINSM_CODE = V_INSM_CODE;

          -- GET nombre campus 
          SELECT STVCAMP_DESC INTO P_CAMP_DESC FROM STVCAMP WHERE STVCAMP_CODE = V_CAMP_CODE;
                                                      
          -- GET TITULO: Forma SCACRSE -> Opciones/Syllabus (SCASYLB) 
          OPEN C_SCRSYLN;
          LOOP
            FETCH C_SCRSYLN INTO P_TITULO;
            EXIT WHEN C_SCRSYLN%NOTFOUND;
            END LOOP;
          CLOSE C_SCRSYLN;
          P_TITULO := NVL(TRIM(P_TITULO),'');

          -- GET DATOS DEL DOCENTE - TOP 1 -
          OPEN C_SPRIDEN;
          LOOP
            FETCH C_SPRIDEN INTO P_INSTRUC_ID, P_INSTRUC_NAME;
            EXIT WHEN C_SPRIDEN%NOTFOUND;
            END LOOP;
          CLOSE C_SPRIDEN;
          P_INSTRUC_ID    := NVL(TRIM(P_INSTRUC_ID),'---');
          P_INSTRUC_NAME  := NVL(TRIM(P_INSTRUC_NAME),'NO ASIGNADO');

      END IF;
      
      /*******************************************************************
      -- VALIDACION 
      ********************************************************************/
      IF (V_INDICADOR = 0 OR P_LIGA = 0 ) THEN
          P_LIGA      := 0;
          RAISE E_INVALID;
      END IF;
    
EXCEPTION
    WHEN E_INVALID THEN
        P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '|| 'No se logra identificar si el NRC tiene o no otros NRCs LIGADOS, favor de verificar la configuración del NRC.';
    WHEN OTHERS THEN
        P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_GET_VALIDACION;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_DESMATRICULA_NRC (
      P_PERIODO         IN STVTERM.STVTERM_CODE%TYPE,
      P_NRC_IN          IN SSBSECT.SSBSECT_CRN%TYPE,
      P_LIGA            IN NUMBER,
      P_LIGA_IDENT      IN SSBSECT.SSBSECT_LINK_IDENT%TYPE,
      P_LIGA_CONEC      IN SSRLINK.SSRLINK_LINK_CONN%TYPE,
      P_NUM_CURSO       IN SSBSECT.SSBSECT_CRSE_NUMB%TYPE,
      P_HTML1           OUT VARCHAR2,
      P_HTML2           OUT VARCHAR2,
      P_HTML3           OUT VARCHAR2,
      P_HTML4           OUT VARCHAR2,
      P_HTML5           OUT VARCHAR2,
      P_MAIL_STDN       OUT VARCHAR2,
      P_ERROR           OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_DESMATRICULA_NRC
  FECHA     : 08/03/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Lista y prepara los NRCs para desmaticular alumnos.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */
AS
      
      V_NRC_P           SSBSECT.SSBSECT_CRN%TYPE;
      E_INVALID         EXCEPTION;
      E_INVALIDNRC      EXCEPTION;
      
      V_SSBSECT_REC     SSBSECT%ROWTYPE;
      -- ADD SSBSECT -
      CURSOR C_SSBSECT IS
        SELECT SSBSECT.* FROM SSBSECT 
        INNER JOIN SSRLINK
           ON SSRLINK_TERM_CODE = SSBSECT_TERM_CODE
          AND SSRLINK_CRN = SSBSECT_CRN
        WHERE SSBSECT_LINK_IDENT = P_LIGA_CONEC
        AND SSRLINK_LINK_CONN = P_LIGA_IDENT
        AND SSBSECT_CRSE_NUMB = P_NUM_CURSO
        AND SSBSECT_TERM_CODE = P_PERIODO;
      
BEGIN
    
    IF P_LIGA = 0 THEN
        RAISE E_INVALID;
    /*******************************************************************
    -- Para NRCs --> HIJO (1)
    ********************************************************************/
    ELSIF (P_LIGA = 1) THEN

        -- get NRC PADRE
        SELECT SSBSECT_CRN INTO V_NRC_P
        FROM SSBSECT 
        INNER JOIN SSRLINK
           ON SSRLINK_TERM_CODE = SSBSECT_TERM_CODE
          AND SSRLINK_CRN = SSBSECT_CRN
        WHERE SSBSECT_LINK_IDENT = P_LIGA_CONEC
        AND SSRLINK_LINK_CONN = P_LIGA_IDENT
        AND SSBSECT_CRSE_NUMB = P_NUM_CURSO
        AND SSBSECT_TERM_CODE = P_PERIODO;

        -- Desabilitando NRC a alumnos incritos (DESMATRICULANDO A LOS NRCs)
        P_ALUMDEL_NRC(P_PERIODO, P_NRC_IN, V_NRC_P,P_HTML1,P_HTML2,P_HTML3,P_HTML4,P_HTML5,P_MAIL_STDN);
        
        
    /*******************************************************************
    -- Para NRCs --> PADRE (2)  Ó  NRC SIMPLE --> (3)
    ********************************************************************/
    ELSIF (P_LIGA = 2 OR P_LIGA = 3) THEN
        
        OPEN C_SSBSECT;
        LOOP
            FETCH C_SSBSECT INTO V_SSBSECT_REC;
            IF C_SSBSECT%FOUND THEN
            
                -- (**) NOTA:
                -- UPDATE NRC Hijos AL CULMINAR SE CERRARAN AUTOMATICAMENTE  
                --
                
                -- Desabilitando NRC a alumnos incritos (DESMATRICULANDO A LOS NRCs)
                P_ALUMDEL_NRC(V_SSBSECT_REC.SSBSECT_TERM_CODE, V_SSBSECT_REC.SSBSECT_CRN,NULL,P_HTML1,P_HTML2,P_HTML3,P_HTML4,P_HTML5,P_MAIL_STDN);
                
            ELSE  
                -- Desabilitando NRC a alumnos incritos (DESMATRICULANDO A LOS NRCs)
                P_ALUMDEL_NRC(P_PERIODO, P_NRC_IN,NULL,P_HTML1,P_HTML2,P_HTML3,P_HTML4,P_HTML5,P_MAIL_STDN);
                EXIT;
            END IF;
        END LOOP;
        CLOSE C_SSBSECT;
          
    END IF;
    
    COMMIT;
    
EXCEPTION
  WHEN E_INVALID THEN
      P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '|| 'No se detecto el NRC o no cumple con algun requisito para cerrarse.';
  WHEN OTHERS THEN
      P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_DESMATRICULA_NRC;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_NRC_CLOSE (
      P_PERIODO         IN STVTERM.STVTERM_CODE%TYPE,
      P_NRC_IN          IN SSBSECT.SSBSECT_CRN%TYPE,
      P_LIGA            IN NUMBER,
      P_LIGA_IDENT      IN SSBSECT.SSBSECT_LINK_IDENT%TYPE,
      P_LIGA_CONEC      IN SSRLINK.SSRLINK_LINK_CONN%TYPE,
      P_NUM_CURSO       IN SSBSECT.SSBSECT_CRSE_NUMB%TYPE,
      P_ERROR           OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_NRC_CLOSE
  FECHA     : 09/03/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Retira los horarios y docentes asignados a el(los) NRC(s) para posteriormente CERARLOS.

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  001   30/03/2017    rmallqui      Se agrego la eliminacion de identificador y conector de liga, asi como estableces la seccion en "0"
  =================================================================================================================== */
AS
      
      E_INVALID         EXCEPTION;
      E_INVALIDNRC      EXCEPTION;
      
      V_SSBSECT_REC     SSBSECT%ROWTYPE;
      -- ADD SSBSECT -
      CURSOR C_SSBSECT IS
        SELECT SSBSECT.* FROM SSBSECT 
        INNER JOIN SSRLINK
           ON SSRLINK_TERM_CODE = SSBSECT_TERM_CODE
          AND SSRLINK_CRN = SSBSECT_CRN
        WHERE SSBSECT_LINK_IDENT = P_LIGA_CONEC
        AND SSRLINK_LINK_CONN = P_LIGA_IDENT
        AND SSBSECT_CRSE_NUMB = P_NUM_CURSO
        AND SSBSECT_TERM_CODE = P_PERIODO;
      
BEGIN
    
    IF P_LIGA = 0 THEN
        RAISE E_INVALID;
    /*******************************************************************
    -- Para NRCs --> HIJO (1)
    ********************************************************************/
    ELSIF (P_LIGA = 1) THEN

        -- Eliminar Asesor(es)
        DELETE FROM SIRASGN
        WHERE SIRASGN_TERM_CODE = P_PERIODO AND SIRASGN_CRN = P_NRC_IN;
        
        -- Eliminar Horario(s)
        DELETE FROM SSRMEET
        WHERE SSRMEET_TERM_CODE = P_PERIODO AND SSRMEET_CRN = P_NRC_IN;

        -- Eliminando el conector de liga  (NRC HIJO que se esta cerrando)
        DELETE FROM SSRLINK 
        WHERE SSRLINK_CRN     = P_NRC_IN
        AND SSRLINK_TERM_CODE = P_PERIODO
        AND SSRLINK_LINK_CONN = P_LIGA_CONEC;
        
        -- CERRAR (C) NRC 
        UPDATE SSBSECT
        SET   SSBSECT_SEQ_NUMB      = '0',
              SSBSECT_SSTS_CODE     = 'C', -- "C" CERRADO
              SSBSECT_ACTIVITY_DATE = SYSDATE,
              SSBSECT_DATA_ORIGIN   = 'WorkFlow',
              SSBSECT_USER_ID       = USER,
              SSBSECT_LINK_IDENT    = NULL
        WHERE SSBSECT_TERM_CODE = P_PERIODO
        AND   SSBSECT_CRN = P_NRC_IN; 

        
    /*******************************************************************
    -- Para NRCs --> PADRE (2)  Ó  NRC SIMPLE --> (3)
    ********************************************************************/
    ELSIF (P_LIGA = 2 OR P_LIGA = 3) THEN
        
        OPEN C_SSBSECT;
        LOOP
            FETCH C_SSBSECT INTO V_SSBSECT_REC;
            IF C_SSBSECT%FOUND THEN
            
                -- Eliminar Asesor(es)
                DELETE FROM SIRASGN
                WHERE SIRASGN_TERM_CODE = V_SSBSECT_REC.SSBSECT_TERM_CODE AND SIRASGN_CRN = V_SSBSECT_REC.SSBSECT_CRN;

                -- Eliminar Horario(s)
                DELETE FROM SSRMEET
                WHERE SSRMEET_TERM_CODE = V_SSBSECT_REC.SSBSECT_TERM_CODE AND SSRMEET_CRN = V_SSBSECT_REC.SSBSECT_CRN;
                
                -- Eliminando el conector de liga  (NRCs HIJOS Ó LIGADOS)
                DELETE FROM SSRLINK 
                WHERE SSRLINK_CRN     = V_SSBSECT_REC.SSBSECT_CRN
                AND SSRLINK_TERM_CODE = V_SSBSECT_REC.SSBSECT_TERM_CODE
                AND SSRLINK_LINK_CONN = P_LIGA_IDENT;

                -- CERRAR (C) NRC 
                UPDATE SSBSECT
                SET   SSBSECT_SEQ_NUMB      = '0',
                      SSBSECT_SSTS_CODE     = 'C', -- "C" CERRADO
                      SSBSECT_ACTIVITY_DATE = SYSDATE,
                      SSBSECT_DATA_ORIGIN   = 'WorkFlow',
                      SSBSECT_USER_ID       = USER,
                      SSBSECT_LINK_IDENT    = NULL
                WHERE SSBSECT_TERM_CODE = V_SSBSECT_REC.SSBSECT_TERM_CODE
                AND   SSBSECT_CRN = V_SSBSECT_REC.SSBSECT_CRN; 

            ELSE  
                
                -- Eliminar Asesor(es)
                DELETE FROM SIRASGN
                WHERE SIRASGN_TERM_CODE = P_PERIODO AND SIRASGN_CRN = P_NRC_IN;
                
                -- Eliminar Horario(s)
                DELETE FROM SSRMEET
                WHERE SSRMEET_TERM_CODE = P_PERIODO AND SSRMEET_CRN = P_NRC_IN;
                
                -- Eliminando el conector de liga (NRC PADRE O SIMPLE)
                DELETE FROM SSRLINK 
                WHERE SSRLINK_CRN     = P_NRC_IN
                AND SSRLINK_TERM_CODE = P_PERIODO
                AND SSRLINK_LINK_CONN = P_LIGA_CONEC;

                -- CERRAR (C) NRC 
                UPDATE SSBSECT
                SET   SSBSECT_SEQ_NUMB      = '0',
                      SSBSECT_SSTS_CODE     = 'C', -- "C" CERRADO
                      SSBSECT_ACTIVITY_DATE = SYSDATE,
                      SSBSECT_DATA_ORIGIN   = 'WorkFlow',
                      SSBSECT_USER_ID       = USER,
                      SSBSECT_LINK_IDENT    = NULL
                WHERE SSBSECT_TERM_CODE = P_PERIODO
                AND   SSBSECT_CRN = P_NRC_IN; 

                EXIT;
                
            END IF;
        END LOOP;
        CLOSE C_SSBSECT;
         
    END IF;

    COMMIT;
    
EXCEPTION
  WHEN E_INVALID THEN
      P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '|| 'No se detecto el NRC o no cumple con algun requisito para cerrarse.';
  WHEN OTHERS THEN
      P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_NRC_CLOSE;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_ALUMDEL_NRC (
      P_PERIODO      IN STVTERM.STVTERM_CODE%TYPE,
      P_NRC          IN SSBSECT.SSBSECT_CRN%TYPE,
      P_NRCSUB       IN SSBSECT.SSBSECT_CRN%TYPE DEFAULT NULL,
      P_HTML1        OUT VARCHAR2,
      P_HTML2        OUT VARCHAR2,
      P_HTML3        OUT VARCHAR2,
      P_HTML4        OUT VARCHAR2,
      P_HTML5        OUT VARCHAR2,
      P_MAIL_STDN    OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_ALUMDEL_NRC
  FECHA     : 08/03/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Cambiar los estados de los alumnos inscritos al NRC. Estado DD(Curso Eliminado)
              el cual se puede visualizar en SFAREGS

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */
AS
    V_NALUMNOS              NUMBER;
    V_ROWAFFECT             NUMBER;
    V_ID_ALUMNO             SPRIDEN.SPRIDEN_ID%TYPE;
    V_NAMES                 VARCHAR2(150);
    SAVE_ACT_DATE_OUT       VARCHAR2(100);
    RETURN_STATUS_IN_OUT    NUMBER;
    V_HTML_COUNT            NUMBER := 0;
    V_HTML                  VARCHAR2(4000);
    V_INDICE                NUMBER := 0;
    V_PHONE1                VARCHAR(49);
    V_PHONE2                VARCHAR(49);
    V_PHONE3                VARCHAR(49);

    V_MESSAGE               EXCEPTION;

    V_EMAIL                 GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE;
    V_EMAILCORP             GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE;

    V_SFRSTCR_REC           SFRSTCR%ROWTYPE;

    -- ADD SSBSECT -
    CURSOR C_SFRSTCR IS
      SELECT * FROM SFRSTCR 
      WHERE SFRSTCR_TERM_CODE = P_PERIODO  
      AND SFRSTCR_CRN = P_NRC
      AND SFRSTCR_RSTS_CODE IN  ('RE','RW');
BEGIN
    
    P_HTML1 := '<tr></tr>';
    P_HTML2 := '<tr></tr>';
    P_HTML3 := '<tr></tr>';
    P_HTML4 := '<tr></tr>';
    P_HTML5 := '<tr></tr>';
    P_MAIL_STDN := '';

    -- Desabilitando NRC a alumnos incritos (DESMATRICULANDO A LOS NRCs)
    OPEN C_SFRSTCR;
    LOOP
        FETCH C_SFRSTCR INTO V_SFRSTCR_REC;
        IF C_SFRSTCR%FOUND THEN
            
            -- GET nombres , ID
            SELECT (SPRIDEN_FIRST_NAME || ' ' || SPRIDEN_LAST_NAME), SPRIDEN_ID INTO V_NAMES, V_ID_ALUMNO  FROM SPRIDEN 
            WHERE SPRIDEN_PIDM = V_SFRSTCR_REC.SFRSTCR_PIDM AND SPRIDEN_CHANGE_IND IS NULL;

            --V_EMAIL     := F_GET_MAIL(V_SFRSTCR_REC.SFRSTCR_PIDM,FALSE);
            V_EMAILCORP := F_GET_MAIL(V_SFRSTCR_REC.SFRSTCR_PIDM,TRUE);
            V_PHONE1    := F_GET_PHONE(V_SFRSTCR_REC.SFRSTCR_PIDM,1);
            V_PHONE2    := F_GET_PHONE(V_SFRSTCR_REC.SFRSTCR_PIDM,2);
            V_PHONE3    := F_GET_PHONE(V_SFRSTCR_REC.SFRSTCR_PIDM,3);
            V_INDICE    := V_INDICE + 1;
            IF NVL(TRIM(V_EMAILCORP),'-') <> '-' THEN 
                  P_MAIL_STDN := CASE WHEN V_INDICE = 1 THEN (P_MAIL_STDN || V_EMAILCORP) ELSE (P_MAIL_STDN || ',' || V_EMAILCORP) END;
            END IF;            
            
            /***********************************************************************************************************
            -- Exportar reporte de alumnos desmaticulados y retornar los correos para la notificacion a los alumnos
            ***********************************************************************************************************/
            V_HTML := '<tr><td>' || V_INDICE || '</td><td>' || V_ID_ALUMNO || '</td><td>' || V_NAMES || '</td><td>' || V_EMAILCORP || '</td><td>' || V_PHONE1 || '</td><td>' || V_PHONE2 || '</td><td>' || V_PHONE3 || '</td></tr>';
            
            IF    (LENGTH(NVL(TRIM(P_HTML1),'-')) + LENGTH(V_HTML) < 4000) AND V_HTML_COUNT < 1 THEN
                      P_HTML1       := P_HTML1 || V_HTML;
            ELSIF (LENGTH(NVL(TRIM(P_HTML2),'-')) + LENGTH(V_HTML) < 4000) AND V_HTML_COUNT < 2 THEN 
                      P_HTML2       := P_HTML2 || V_HTML;
                      V_HTML_COUNT  := 1;
            ELSIF (LENGTH(NVL(TRIM(P_HTML3),'-')) + LENGTH(V_HTML) < 4000) AND V_HTML_COUNT < 3 THEN 
                      P_HTML3       := P_HTML3 || V_HTML;
                      V_HTML_COUNT  := 2;
            ELSIF (LENGTH(NVL(TRIM(P_HTML4),'-')) + LENGTH(V_HTML) < 4000) AND V_HTML_COUNT < 4 THEN 
                      P_HTML4       := P_HTML4 || V_HTML;
                      V_HTML_COUNT  := 3;
            ELSIF (LENGTH(NVL(TRIM(P_HTML5),'-')) + LENGTH(V_HTML) < 4000) AND V_HTML_COUNT < 5 THEN 
                      P_HTML5       := P_HTML5 || V_HTML;
                      V_HTML_COUNT  := 4;
            ELSE
                  RAISE V_MESSAGE;
            END IF;

            /******************************************************************************
            -- Desmatricular y Estimar deuda (BANNER)
            *******************************************************************************/
            UPDATE SFRSTCR 
            SET SFRSTCR_RSTS_CODE = 'DD',           SFRSTCR_RSTS_DATE = SYSDATE,
                SFRSTCR_ERROR_FLAG = 'D',           SFRSTCR_BILL_HR = 0,
                SFRSTCR_WAIV_HR = 0,                SFRSTCR_CREDIT_HR = 0,
                SFRSTCR_ACTIVITY_DATE = SYSDATE,    SFRSTCR_USER = 'WorkFlow',
                SFRSTCR_ASSESS_ACTIVITY_DATE = SYSDATE      
            WHERE SFRSTCR_TERM_CODE = V_SFRSTCR_REC.SFRSTCR_TERM_CODE
            AND SFRSTCR_CRN IN (V_SFRSTCR_REC.SFRSTCR_CRN , P_NRCSUB)
            AND SFRSTCR_PIDM = V_SFRSTCR_REC.SFRSTCR_PIDM
            AND SFRSTCR_RSTS_CODE IN  ('RE','RW'); -- Estados válidos (SFQRSTS)
            V_ROWAFFECT := SQL%ROWCOUNT;

            IF (V_ROWAFFECT > 0) THEN
                  -- Get ID del alumno
                  SELECT SPRIDEN_ID INTO V_ID_ALUMNO 
                  FROM SPRIDEN WHERE SPRIDEN_PIDM = V_SFRSTCR_REC.SFRSTCR_PIDM 
                  AND SPRIDEN_CHANGE_IND IS NULL;

                   -- #######################################################################
                  -- Procesar DEUDA - Volviendo a estimar la deuda devido al cambio de TARIFA.
                  SFKFEES.p_processfeeassessment (  V_SFRSTCR_REC.SFRSTCR_TERM_CODE,
                                                    V_SFRSTCR_REC.SFRSTCR_PIDM,
                                                    SYSDATE,      -- assessment effective date(Evaluación de la fecha efectiva)
                                                    SYSDATE,  -- refund by total refund date(El reembolso por fecha total del reembolso)
                                                    'R',          -- use regular assessment rules(utilizar las reglas de evaluación periódica)
                                                    'Y',          -- create TBRACCD records
                                                    'SFAREGS',    -- where assessment originated from
                                                    'Y',          -- commit changes
                                                    SAVE_ACT_DATE_OUT,    -- OUT -- save_act_date
                                                    'N',          -- do not ignore SFRFMAX rules
                                                    RETURN_STATUS_IN_OUT );   -- OUT -- return_status
                  ------------------------------------------------------------------------------  
                  -- -- forma TVAAREV "Aplicar Transacciones" -- No necesariamente necesario.
                  -- TZJAPOL.p_run_proc_tvrappl(V_ID_ALUMNO);
                  ------------------------------------------------------------------------------  
            END IF;
            
        ELSE  
            
            /******************************************************************************************/
            -- CASO LIGA 1 (NRC HIJO): Actualizar los matriculados y vacantes restasnter del NRC PADRE
            /******************************************************************************************/

            -- CALCULAR Numero de matriculados válidos (SSBSECT_ENRL) en el NRC PADRE 
            SELECT COUNT(*) INTO V_NALUMNOS 
            FROM SFRSTCR
            WHERE SFRSTCR_CRN = P_NRCSUB
            AND SFRSTCR_RSTS_CODE IN  ('RE','RW');

            -- UPDATE (SSBSECT_ENRL) actualiza los datos del numero matriculados en el NRC PADRE 
            UPDATE SSBSECT
            SET   SSBSECT_ENRL        = V_NALUMNOS, -- # matriculados válidos en el nrc
                  SSBSECT_SEATS_AVAIL = SSBSECT_MAX_ENRL - V_NALUMNOS, -- # vacantes restantes
                  SSBSECT_CENSUS_ENRL = CASE WHEN SYSDATE < SSBSECT_CENSUS_ENRL_DATE THEN V_NALUMNOS ELSE SSBSECT_CENSUS_ENRL END, -- # de matriculados antes de una fecha SSBSECT_CENSUS_ENRL_DATE
                  SSBSECT_ACTIVITY_DATE = SYSDATE,
                  SSBSECT_DATA_ORIGIN   = 'WorkFlow',
                  SSBSECT_USER_ID       = USER
            WHERE SSBSECT_TERM_CODE = P_PERIODO
            AND   SSBSECT_CRN = P_NRCSUB; 
            
            EXIT;

        END IF;
    END LOOP;
    CLOSE C_SFRSTCR;
    
    -- UPDATE NRC Hijos CUMPLA CON LO NECESARIO CON REQUISITOS (**) VACANTE REAL = 0
    UPDATE SSBSECT
    SET   SSBSECT_MAX_ENRL    = 0, -- # vacantes
          SSBSECT_ENRL        = 0, -- # matriculados validos en el nrc
          SSBSECT_SEATS_AVAIL = 0, -- # vacantes restantes
          SSBSECT_CENSUS_ENRL = CASE WHEN SYSDATE < SSBSECT_CENSUS_ENRL_DATE THEN 0 ELSE SSBSECT_CENSUS_ENRL END, -- # de matriculados antes de una fecha SSBSECT_CENSUS_ENRL_DATE
          SSBSECT_ACTIVITY_DATE = SYSDATE,
          SSBSECT_DATA_ORIGIN   = 'WorkFlow',
          SSBSECT_USER_ID       = USER
    WHERE SSBSECT_TERM_CODE = P_PERIODO
    AND   SSBSECT_CRN = P_NRC; 
    
    COMMIT;


EXCEPTION
  WHEN V_MESSAGE THEN 
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| '- La cantidad de registros supera la capacidad del los parametros retornados.' );
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_ALUMDEL_NRC;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_NRC_CANCEL (
    P_PERIODO         IN STVTERM.STVTERM_CODE%TYPE,
    P_NRC             IN SSBSECT.SSBSECT_CRN%TYPE,
    P_OLD_SSTS_CODE   IN SSBSECT.SSBSECT_SSTS_CODE%TYPE,
    P_MAXENROL        IN SSBSECT.SSBSECT_MAX_ENRL%TYPE
)
/* ===================================================================================================================
  NOMBRE    : P_NRC_CANCEL
  FECHA     : 21/03/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Restaura el ESTADO y # de VACANTES del NRC al a uno anterior de solicitar su cierre.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */
AS
          
BEGIN
     
    -- CERRAR (C) NRC 
    UPDATE SSBSECT
    SET   SSBSECT_SSTS_CODE     = P_OLD_SSTS_CODE,  -- Estado
          SSBSECT_MAX_ENRL      = P_MAXENROL,   -- 
          SSBSECT_SEATS_AVAIL   = P_MAXENROL - SSBSECT_ENRL,
          SSBSECT_ACTIVITY_DATE = SYSDATE,
          SSBSECT_DATA_ORIGIN   = 'WorkFlow',
          SSBSECT_USER_ID       = USER
    WHERE SSBSECT_TERM_CODE = P_PERIODO
    AND   SSBSECT_CRN = P_NRC
    AND   P_OLD_SSTS_CODE IN (SELECT STVSSTS_CODE FROM STVSSTS); 
    
    COMMIT;
    
EXCEPTION
  WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END P_NRC_CANCEL;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


FUNCTION  F_GET_MAIL (
          P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
          P_MAILCORP        BOOLEAN          
) RETURN VARCHAR2        
AS
/* ===================================================================================================================
  NOMBRE    : F_GET_MAIL
  FECHA     : 17/03/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene el correo corporativo u uno alternativo del alumno  por PIDM.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */
          P_EMAIL           GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE;
          V_EMAIL           GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE;
          
          -- GET MAIL
          CURSOR C_GOREMAL IS
          SELECT GOREMAL_EMAIL_ADDRESS FROM (
              SELECT GOREMAL_EMAIL_ADDRESS, GOREMAL_PREFERRED_IND FROM GOREMAL
              WHERE GOREMAL_PIDM = P_PIDM AND GOREMAL_STATUS_IND = 'A'
              ORDER BY GOREMAL_PREFERRED_IND DESC
          )WHERE ROWNUM <= 1;
          
          CURSOR C_GOREMALCORP IS
          SELECT GOREMAL_EMAIL_ADDRESS FROM (
              SELECT GOREMAL_EMAIL_ADDRESS, GOREMAL_PREFERRED_IND FROM GOREMAL
              WHERE GOREMAL_PIDM = P_PIDM AND GOREMAL_STATUS_IND = 'A' AND  GOREMAL_EMAIL_ADDRESS LIKE ('%@continental.edu.pe')
              ORDER BY GOREMAL_PREFERRED_IND DESC
          )WHERE ROWNUM <= 1;
BEGIN
     P_EMAIL := NULL;
     -- GET email
     IF (P_MAILCORP) THEN
          OPEN C_GOREMALCORP;
          LOOP
            FETCH C_GOREMALCORP INTO V_EMAIL;
            IF C_GOREMALCORP%FOUND THEN
                P_EMAIL := V_EMAIL;
            ELSE EXIT;
          END IF;
          END LOOP;
          CLOSE C_GOREMALCORP;
      ELSE  
          OPEN C_GOREMAL;
          LOOP
            FETCH C_GOREMAL INTO V_EMAIL;
            IF C_GOREMAL%FOUND THEN
                P_EMAIL := V_EMAIL;
            ELSE EXIT;
          END IF;
          END LOOP;
          CLOSE C_GOREMAL;
      END IF;
      
      RETURN P_EMAIL;
END F_GET_MAIL;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


FUNCTION  F_GET_PHONE (
          P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
          P_N_ORDER         NUMBER
) RETURN VARCHAR2        
AS
/* ===================================================================================================================
  NOMBRE    : F_GET_PHONE
  FECHA     : 21/03/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Segun el PIDM obtiene el TELEFONO que se encuentra en el orden "P_N_ORDER" indicado.
              
              P_N_ORDER
                1º       --> numero preferido
                2º a mas --> numero segun el orden indicado

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */
          V_PHONE           VARCHAR2(49);
          V_RESP_PHONE      VARCHAR2(49);
          
          -- GET PHONE
          CURSOR C_SPRTELE_PRIM IS
          SELECT PRONE FROM (
              SELECT TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) PRONE
              FROM SPRTELE LEFT JOIN STVTELE ON SPRTELE_TELE_CODE = STVTELE_CODE
              WHERE SPRTELE_PIDM = P_PIDM AND SPRTELE_PRIMARY_IND = 'Y' AND SPRTELE_STATUS_IND IS NULL
              ORDER BY SPRTELE_SEQNO
          )WHERE ROWNUM = 1;
          
          CURSOR C_SPRTELE IS
          SELECT PRONE FROM (
              SELECT TRIM(SPRTELE_PHONE_AREA || ' ' || SPRTELE_PHONE_NUMBER) PRONE
              FROM SPRTELE LEFT JOIN STVTELE ON SPRTELE_TELE_CODE = STVTELE_CODE
              WHERE SPRTELE_PIDM = P_PIDM AND SPRTELE_PRIMARY_IND IS NULL AND SPRTELE_STATUS_IND IS NULL
              ORDER BY SPRTELE_SEQNO
          )WHERE ROWNUM = P_N_ORDER;
BEGIN
     V_RESP_PHONE := '-';
     -- GET telefono
     IF (P_N_ORDER = 1) THEN
          OPEN C_SPRTELE_PRIM;
          LOOP
            FETCH C_SPRTELE_PRIM INTO V_PHONE;
            IF C_SPRTELE_PRIM%FOUND THEN
                V_RESP_PHONE := CASE WHEN V_PHONE LIKE '%<%' OR V_PHONE LIKE '%>%' THEN '-' ELSE V_PHONE END ;
            ELSE EXIT;
          END IF;
          END LOOP;
          CLOSE C_SPRTELE_PRIM;
      ELSE  
          OPEN C_SPRTELE;
          LOOP
            FETCH C_SPRTELE INTO V_PHONE;
            IF C_SPRTELE%FOUND THEN
                V_RESP_PHONE := CASE WHEN V_PHONE LIKE '%<%' OR V_PHONE LIKE '%>%' THEN '-' ELSE V_PHONE END ;
            ELSE EXIT;
          END IF;
          END LOOP;
          CLOSE C_SPRTELE;
      END IF;
      
      RETURN V_RESP_PHONE;
END F_GET_PHONE;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--                   
END WFK_CONTISCNRC;