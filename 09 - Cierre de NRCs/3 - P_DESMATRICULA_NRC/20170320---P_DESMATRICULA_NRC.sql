/*
SET SERVEROUTPUT ON
declare 
      P_LIGA            NUMBER;
      P_LIGA_IDENT      SSBSECT.SSBSECT_LINK_IDENT%TYPE;
      P_LIGA_CONEC      SSRLINK.SSRLINK_LINK_CONN%TYPE;
      P_NUM_CURSO       SSBSECT.SSBSECT_CRSE_NUMB%TYPE;
      P_TITULO          SCRSYLN.SCRSYLN_LONG_COURSE_TITLE%TYPE;
      P_SECCION         SSBSECT.SSBSECT_SEQ_NUMB%TYPE;
      P_ENROLADOS       SSBSECT.SSBSECT_ENRL%TYPE;
      P_HTML1           VARCHAR2(4000);
      P_HTML2           VARCHAR2(4000);
      P_HTML3           VARCHAR2(4000);
      P_HTML4           VARCHAR2(4000);
      P_HTML5           VARCHAR2(4000);
      P_ERROR           VARCHAR2(4000);
begin  -- 1279 1119
    WFK_CONTISCNRC.P_GET_VALIDACION('201710','1654',P_LIGA,P_LIGA_IDENT,P_LIGA_CONEC,P_NUM_CURSO,P_TITULO,P_SECCION,P_ENROLADOS,P_ERROR); -- 381 380
    DBMS_OUTPUT.PUT_LINE(P_LIGA||' - '||P_LIGA_IDENT||' - '||P_LIGA_CONEC||' - '||P_NUM_CURSO||' - '||P_TITULO||' - '||P_SECCION||' - '||P_ENROLADOS||' - '||P_ERROR);
    
    --- PRUEBA DEL SP
    WFK_CONTISCNRC.P_DESMATRICULA_NRC('201710','1199', 1, P_LIGA_IDENT, P_LIGA_CONEC,P_NUM_CURSO,P_HTML1,P_HTML2,P_HTML3,P_HTML4,P_HTML5, P_ERROR);
    DBMS_OUTPUT.PUT_LINE(P_HTML1);
    DBMS_OUTPUT.PUT_LINE(P_HTML2);
    DBMS_OUTPUT.PUT_LINE(P_HTML3);
    DBMS_OUTPUT.PUT_LINE(P_HTML4);
    DBMS_OUTPUT.PUT_LINE(P_HTML5);
    DBMS_OUTPUT.PUT_LINE(P_ERROR);
end;
*/


CREATE OR REPLACE PROCEDURE P_DESMATRICULA_NRC (
      P_PERIODO         IN STVTERM.STVTERM_CODE%TYPE,
      P_NRC_IN          IN SSBSECT.SSBSECT_CRN%TYPE,
      P_LIGA            IN NUMBER,
      P_LIGA_IDENT      IN SSBSECT.SSBSECT_LINK_IDENT%TYPE,
      P_LIGA_CONEC      IN SSRLINK.SSRLINK_LINK_CONN%TYPE,
      P_NUM_CURSO       IN SSBSECT.SSBSECT_CRSE_NUMB%TYPE,
      P_HTML1           OUT VARCHAR2,
      P_HTML2           OUT VARCHAR2,
      P_HTML3           OUT VARCHAR2,
      P_HTML4           OUT VARCHAR2,
      P_HTML5           OUT VARCHAR2,
      P_ERROR           OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_DESMATRICULA_NRC
  FECHA     : 08/03/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Lista y prepara los NRCs para desmaticular alumnos.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */
AS
      
      V_NRC_P           SSBSECT.SSBSECT_CRN%TYPE;
      E_INVALID         EXCEPTION;
      E_INVALIDNRC      EXCEPTION;
      
      V_SSBSECT_REC     SSBSECT%ROWTYPE;
      -- ADD SSBSECT -
      CURSOR C_SSBSECT IS
        SELECT SSBSECT.* FROM SSBSECT 
        INNER JOIN SSRLINK
           ON SSRLINK_TERM_CODE = SSBSECT_TERM_CODE
          AND SSRLINK_CRN = SSBSECT_CRN
        WHERE SSBSECT_LINK_IDENT = P_LIGA_CONEC
        AND SSRLINK_LINK_CONN = P_LIGA_IDENT
        AND SSBSECT_CRSE_NUMB = P_NUM_CURSO
        AND SSBSECT_TERM_CODE = P_PERIODO;
      
BEGIN
    
    IF P_LIGA = 0 THEN
        RAISE E_INVALID;
    /*******************************************************************
    -- Para NRCs --> HIJO (1)
    ********************************************************************/
    ELSIF (P_LIGA = 1) THEN

        -- get NRC PADRE
        SELECT SSBSECT_CRN INTO V_NRC_P
        FROM SSBSECT 
        INNER JOIN SSRLINK
           ON SSRLINK_TERM_CODE = SSBSECT_TERM_CODE
          AND SSRLINK_CRN = SSBSECT_CRN
        WHERE SSBSECT_LINK_IDENT = P_LIGA_CONEC
        AND SSRLINK_LINK_CONN = P_LIGA_IDENT
        AND SSBSECT_CRSE_NUMB = P_NUM_CURSO
        AND SSBSECT_TERM_CODE = P_PERIODO;

        -- Desabilitando NRC a alumnos incritos (DESMATRICULANDO A LOS NRCs)
        P_ALUMDEL_NRC(P_PERIODO, P_NRC_IN, V_NRC_P,P_HTML1,P_HTML2,P_HTML3,P_HTML4,P_HTML5);
        
        
    /*******************************************************************
    -- Para NRCs --> PADRE (2)  Ó  NRC SIMPLE --> (3)
    ********************************************************************/
    ELSIF (P_LIGA = 2 OR P_LIGA = 3) THEN
        
        OPEN C_SSBSECT;
        LOOP
            FETCH C_SSBSECT INTO V_SSBSECT_REC;
            IF C_SSBSECT%FOUND THEN
            
                -- (**) NOTA:
                -- UPDATE NRC Hijos AL CULMINAR SE CERRARAN AUTOMATICAMENTE  
                --
                
                -- Desabilitando NRC a alumnos incritos (DESMATRICULANDO A LOS NRCs)
                P_ALUMDEL_NRC(V_SSBSECT_REC.SSBSECT_TERM_CODE, V_SSBSECT_REC.SSBSECT_CRN,NULL,P_HTML1,P_HTML2,P_HTML3,P_HTML4,P_HTML5);
                
            ELSE  
                -- Desabilitando NRC a alumnos incritos (DESMATRICULANDO A LOS NRCs)
                P_ALUMDEL_NRC(P_PERIODO, P_NRC_IN,NULL,P_HTML1,P_HTML2,P_HTML3,P_HTML4,P_HTML5);
                EXIT;
            END IF;
        END LOOP;
        CLOSE C_SSBSECT;
          
    END IF;
    
    COMMIT;
    
EXCEPTION
  WHEN E_INVALID THEN
      P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '|| 'No se detecto el NRC o no cumple con algun requisito para cerrarse.';
  WHEN OTHERS THEN
      P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_DESMATRICULA_NRC;