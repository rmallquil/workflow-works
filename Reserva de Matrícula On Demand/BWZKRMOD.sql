/**********************************************************************************************/
/* BWZKRMOD.sql                                                                               */
/**********************************************************************************************/
/*                                                                                            */
/* Descripción corta: Script para generar el Paquete de automatización del proceso de         */
/*                    Solicitud de Reserva de Matrícula On Demand                             */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI             FECHA       */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creación del Código.                                        LAM             03/ENE/2018 */
/*    --------------------                                                                    */
/*    Creación del paquete de Reserva de Matrícula On Demand                                  */
/*    --Paquete generico                                                                      */
/*    Procedure P_VALID_ID: Valida si el DNI ingresado existe en la DB-BANNER.                */
/*    --Paquete generico                                                                      */
/*    Procedure P_GET_INFO_STUDENT: Obtiene informacion del estudiante mediante el ID.        */
/*    Procedure P_VERIFICA_ESTADO: Verifica si el estudiante es apto para realizar la reserva */
/*              de matricula.                                                                 */
/*    --Paquete generico                                                                      */
/*    Procedure P_VERIFICA_FECHA_DISPO: Verificar si la fecha de solicitud se encuentra dentro*/
/*              de las fecha configuradas para el proceso.                                    */
/*    ------------------------------- ANULADO -------------------------------                 */
/*    Procedure P_VERIFICA_RETENCIONES: Verificar si la fecha de solicitud se encuentra dentro*/
/*              de las fecha configuradas para el proceso.                                    */
/*    -----------------------------------------------------------------------                 */
/*    REEMPLAZA AL PROCEDIMIENTO P_VERIFICA_RETENCIONES                                       */
/*    Procedure P_VERIFICAR_APTO: Verifica que el estudiante solicitante                      */
/*              cumplas las condiciones para hacer uso del WF.                                */
/*    Procedure P_GET_USUARIO: En base a una sede y un rol, el procedimiento LOS CORREOS      */
/*              del(os) responsable(s) de Registros Academicos de esa sede.                   */
/*    Procedure P_GET_SIDEUDA_ALUMNO: El objetivo es que en base a un ID y un codigo de       */
/*              detalle verifique la existencia de deuda de MATRICULA del alumno en cualquier */
/*              periodo, divisa PEN.                                                          */
/*    Procedure P_ELIMINAR_NRC: Eliminar los NRC's , STUPDY PATH, Informacion de Ingreso.     */
/*              Vuelve a estimar sus deudas cancelando sus deudas generadas                   */
/*              (MONTOS EN NEGATIVO)                                                          */
/*    Procedure P_GET_PAGOS: Obtener el monto abonado de matricula y de las cuotas.           */
/* 2. Aumento P_VERIFICAR_APTO                                                                */
/*    Se incluye el SP para validar que el estudiante cumpla con todas sus condiciones.       */
/* 3. Actualizacion Paquete Generico                              BYF             31/ENE/2019 */
/*    Se cambia el origen de datos de algunos SP para mejorar en su mantenimiento.            */
/*    Actualizacion P_VERIFICAR_APTO                                                          */
/*    Se aumenta la restricción para los estudiantes de Beca 18.                              */
/*    Actualizacion Consulta                                                                  */
/*    Se actualiza la consulta para obtener la PARTE PERIODO en todo el paquete.              */
/*                                                                                            */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/

-- PERMISOS DE EJECUCIÓN
/**********************************************************************************************/
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/
CREATE OR REPLACE PACKAGE BWZKRMOD AS

PROCEDURE P_VALID_ID (
        P_ID            IN  SPRIDEN.SPRIDEN_ID%TYPE,
        P_DNI_VALID     OUT VARCHAR2
);

----------------------------------------------------------------------------------------------------------------------

PROCEDURE P_GET_INFO_STUDENT (
        P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
        P_PIDM              OUT SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE         OUT SOBPTRM.SOBPTRM_TERM_CODE%TYPE, 
        P_PART_PERIODO      OUT SOBPTRM.SOBPTRM_PTRM_CODE%TYPE, 
        P_DEPT_CODE         OUT STVDEPT.STVDEPT_CODE%TYPE,
        P_CAMP_CODE         OUT STVCAMP.STVCAMP_CODE%TYPE,
        P_CAMP_DESC         OUT STVCAMP.STVCAMP_DESC%TYPE,
        P_PROGRAM           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
        P_EMAIL             OUT GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE,
        P_NAME              OUT VARCHAR2,
        P_FECHA_SOL         OUT VARCHAR2
);

----------------------------------------------------------------------------------------------------------------------

PROCEDURE P_VERIFICA_ESTADO(
        P_PIDM         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE    IN STVTERM.STVTERM_CODE%TYPE,
        P_ESTADO       OUT VARCHAR2,
        P_DESCRIP      OUT VARCHAR2
);

----------------------------------------------------------------------------------------------------------------------

PROCEDURE P_VERIFICA_FECHA_DISPO (
        P_TERM_CODE      IN SOBPTRM.SOBPTRM_TERM_CODE%TYPE,
        P_FECHA_SOL      IN VARCHAR2,
        P_PART_PERIODO   IN SOBPTRM.SOBPTRM_PTRM_CODE%TYPE,
        P_FECHA_VALIDA   OUT VARCHAR2,
        P_DESCRIP        OUT VARCHAR2 
);


----------------------------------------------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_APTO (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_STATUS_APTO     OUT VARCHAR2,
        P_REASON          OUT VARCHAR2
);

----------------------------------------------------------------------------------------------------------------------

--PROCEDURE P_VERIFICA_RETENCIONES (
--    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
--    P_RET_CODE_01     IN SPRHOLD.SPRHOLD_HLDD_CODE%TYPE,
--    P_RET_CODE_02     IN SPRHOLD.SPRHOLD_HLDD_CODE%TYPE,
--    P_RETENCION       OUT VARCHAR2,
--    P_DESCRIP         OUT VARCHAR2,
--    P_MESSAGE         OUT VARCHAR2
--);

----------------------------------------------------------------------------------------------------------------------

PROCEDURE P_GET_USUARIO (
        P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
        P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
        P_CORREO              OUT VARCHAR2,
        P_ROL_SEDE            OUT VARCHAR2,
        P_ERROR               OUT VARCHAR2
);

----------------------------------------------------------------------------------------------------------------------

PROCEDURE P_GET_SIDEUDA_ALUMNO (
        P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_CODIGO_DETALLE      IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE,
        P_TERM_CODE           IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE,  --------- APEC
        P_DEUDA               OUT VARCHAR2
);              

----------------------------------------------------------------------------------------------------------------------

PROCEDURE P_ELIMINAR_NRC ( 
        P_PIDM                    IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE               IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
        P_DEPT_CODE               IN STVDEPT.STVDEPT_CODE%TYPE,
        P_CAMP_CODE               IN STVCAMP.STVCAMP_CODE%TYPE,
        P_FECHA_SOL               IN VARCHAR2,
        P_ERROR                   OUT VARCHAR2
);

----------------------------------------------------------------------------------------------------------------------

PROCEDURE P_GET_PAGOS (
        P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE           IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE,  --------- APEC
        P_FECHA_SOL           IN VARCHAR2,
        P_ABONO_MATRIC        OUT NUMBER,
        P_ABONO_CUOTAS        OUT NUMBER
);              

END BWZKRMOD;

/**********************************************************************************************/
--  /
--  show errors 
-- 
--  SET SCAN ON
--  WHENEVER SQLERROR CONTINUE
--  DROP PUBLIC SYNONYM BWZKRMOD;
--  WHENEVER SQLERROR EXIT ROLLBACK
--  CREATE PUBLIC SYNONYM BWZKRMOD FOR BANINST1.BWZKRMOD;
--  WHENEVER SQLERROR CONTINUE
--  START gurgrtb BWZKRMOD
--  START gurgrth BWZKRMOD
--  WHENEVER SQLERROR EXIT ROLLBACK
/**********************************************************************************************/
