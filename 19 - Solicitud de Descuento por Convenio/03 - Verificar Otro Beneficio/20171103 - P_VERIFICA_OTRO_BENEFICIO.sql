
--SET SERVEROUTPUT ON
--   declare
--   P_OTRO_BENEFICIO	VARCHAR2(10);
--   P_DESC_BENEFICIO VARCHAR2(4000);
--   P_ERROR				  VARCHAR2(4000);
--begin
--  
--  P_VERIFICA_OTRO_BENEFICIO(554226,'48279701', 'UREG', '201720',P_OTRO_BENEFICIO,P_DESC_BENEFICIO,P_ERROR);
--  DBMS_OUTPUT.PUT_LINE(P_OTRO_BENEFICIO);
--  DBMS_OUTPUT.PUT_LINE(P_DESC_BENEFICIO);
--  DBMS_OUTPUT.PUT_LINE(P_ERROR);
--
--end;

/* ===================================================================================================================
  NOMBRE    : P_VERIFICA_OTRO_BENEFICIO
  FECHA     : 03/11/2017
  AUTOR     : Flores Vilcapoma Brian
  OBJETIVO  : Verifica si el estudiante tiene un Beneficio economico activo (Convenio - Descuento - Beca)
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
CREATE OR REPLACE PROCEDURE P_VERIFICA_OTRO_BENEFICIO (
    P_PIDM				    IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO			  IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE			  IN STVDEPT.STVDEPT_CODE%TYPE,
    P_TERM_CODE			  IN STVTERM.STVTERM_CODE%TYPE,
    P_OTRO_BENEFICIO	OUT VARCHAR2,
    P_DESC_BENEFICIO  OUT VARCHAR2,
    P_ERROR				    OUT VARCHAR2
)
AS
      V_ERROR         EXCEPTION;
      V_BENEFICIO     VARCHAR2(5);
      V_CONTADOR      NUMBER;
      V_APEC_CAMP     VARCHAR2(10);
      V_APEC_DEPT     VARCHAR2(10);
      V_APEC_TERM     VARCHAR2(10);

BEGIN

  SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
  SELECT CZRDEPT_DEPT_BDUCCI INTO V_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO

	SELECT COUNT("IDAlumno") INTO V_CONTADOR
	FROM BNE.tblAlumnoBeneficio@BDUCCI.CONTINENTAL.EDU.PE
	WHERE "IDEscuelaADM" = V_APEC_DEPT
	AND "IDPerAcad" = V_APEC_TERM
	AND "IDAlumno" = P_ID_ALUMNO
  AND "Activo" = '1'
  ORDER BY "FechaRegistro" DESC;
  
  IF V_CONTADOR > 0 THEN
    
    	SELECT "TipoBeneficio" INTO V_BENEFICIO
      FROM BNE.tblAlumnoBeneficio@BDUCCI.CONTINENTAL.EDU.PE
      WHERE "IDEscuelaADM" = V_APEC_DEPT
      AND "IDPerAcad" = V_APEC_TERM
      AND "IDAlumno" = P_ID_ALUMNO
      AND "Activo" = '1'
      AND ROWNUM <= 1
      ORDER BY "FechaRegistro" DESC;
  
      CASE V_BENEFICIO 
      WHEN 'C' THEN
        P_OTRO_BENEFICIO := 'TRUE';
        P_DESC_BENEFICIO := 'Convenio';
      WHEN 'B' THEN
        P_OTRO_BENEFICIO := 'TRUE';
        P_DESC_BENEFICIO := 'Beca/Media Beca';
      WHEN 'D' THEN
        P_OTRO_BENEFICIO := 'TRUE';
        P_DESC_BENEFICIO := 'Descuento';
      ELSE
        P_OTRO_BENEFICIO := 'FALSE';
        P_DESC_BENEFICIO := 'Ninguno';
      END CASE;
      
    ELSE
      P_OTRO_BENEFICIO := 'FALSE';
      P_DESC_BENEFICIO := 'Ninguno';
    END IF;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICA_OTRO_BENEFICIO;


