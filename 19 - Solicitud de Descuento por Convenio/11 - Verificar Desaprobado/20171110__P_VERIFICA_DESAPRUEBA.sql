/*
SET SERVEROUTPUT ON
DECLARE P_ERROR VARCHAR2(4000);
 P_DESAPROBO VARCHAR2(4000);
 P_ASIG_DESAP VARCHAR2(4000);
BEGIN
    P_VERIFICA_DESAPRUEBA(199331,'72049226','201400',P_DESAPROBO,P_ASIG_DESAP, P_ERROR);
    DBMS_OUTPUT.PUT_LINE(P_DESAPROBO);
    DBMS_OUTPUT.PUT_LINE(P_ASIG_DESAP);
    DBMS_OUTPUT.PUT_LINE(P_ERROR);
END;
*/
/* ===================================================================================================================
  NOMBRE    : P_VERIFICA_DESAPRUEBA
  FECHA     : 10/11/2017
  AUTOR     : Flores Vilcapoma, Brian
  OBJETIVO  : Verifica si el estudiante en un periodo anterior se desaprobo o no.

MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  =================================================================================================================== */
CREATE OR REPLACE PROCEDURE P_VERIFICA_DESAPRUEBA (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_DESAPROBO           OUT VARCHAR2,
      P_ASIG_DESAP          OUT VARCHAR2,
      P_ERROR               OUT VARCHAR2
)
AS
      -- @PARAMETERS
      V_CONTADOR            NUMBER := 0;
      V_CURSO               VARCHAR2(100);
      V_ERROR               EXCEPTION;

      CURSOR C_CURSOS IS
      WITH 
      Aprob AS (
        SELECT   
        /*+ FULL(S) FULL(E) FULL(M)  */ 
        SMRRQCM_PIDM PIDM,
        S.SMRDOUS_SUBJ_CODE||''||E.SCBCRSE_CRSE_NUMB CODIGO,
        E.SCBCRSE_TITLE CURSO,
        M.SMRRQCM_PROGRAM PROGRAM,
        SMRDOUS_TERM_CODE AS TERM,
        nvl(SMRDOUS_GRDE_CODE, 0) AS SCORE,
        nvl(SCBCRSE_CREDIT_HR_HIGH, 0) CREDITO,
        SMRDOUS_CRSE_SOURCE ORIGEN
        FROM SMRRQCM M
        INNER JOIN SMRDOUS S ON 
            M.SMRRQCM_PIDM=S.SMRDOUS_PIDM
            AND M.SMRRQCM_REQUEST_NO=S.SMRDOUS_REQUEST_NO
        INNER JOIN SCBCRSE E ON 
            S.SMRDOUS_SUBJ_CODE = E.SCBCRSE_SUBJ_CODE
            AND S.SMRDOUS_CRSE_NUMB = E.SCBCRSE_CRSE_NUMB 
        WHERE M.SMRRQCM_REQUEST_NO=
            (
            SELECT MAX(SMRRQCM_REQUEST_NO)
            FROM SMRRQCM
            WHERE SMRRQCM_PIDM=P_PIDM
            AND SMRRQCM_PROGRAM=M.SMRRQCM_PROGRAM
            )
        AND nvl(SUBSTR( SMRDOUS_AREA, 4 , 1 ), 0) <> 'D'
        AND E.SCBCRSE_CRSE_NUMB NOT IN ('CN001','CN002')
        AND S.SMRDOUS_CRSE_SOURCE IN ('H','T')
        AND M.SMRRQCM_PIDM = P_PIDM
      ),
      
    Desaprob AS (
        SELECT 
       /*+ FULL(N) FULL(E) FULL(M) FULL(N)  */ 
        UNIQUE
        N.SMRDOCN_PIDM PIDM,
        E.SCBCRSE_SUBJ_CODE||''||N.SMRDOCN_CRSE_NUMB CODIGO,
        E.SCBCRSE_TITLE CURSO,
        M.SMRRQCM_PROGRAM PROGRAM,
        SMRDOCN_TERM_CODE AS TERM,
        nvl(SMRDOCN_GRDE_CODE, 0) AS SCORE,
        nvl( SCBCRSE_CREDIT_HR_HIGH , 0) CREDITO,
        SMRDOCN_CRSE_SOURCE ORIGEN
        FROM SMRRQCM M
        INNER JOIN SMRDOCN N ON 
            N.SMRDOCN_PIDM=M.SMRRQCM_PIDM
            AND N.SMRDOCN_REQUEST_NO=M.SMRRQCM_REQUEST_NO
            AND SMRDOCN_GRDE_CODE<='10' 
            AND SMRDOCN_CRSE_SOURCE IN ('T','H')
        LEFT JOIN SCBCRSE E ON 
            E.SCBCRSE_CRSE_NUMB=N.SMRDOCN_CRSE_NUMB
            AND E.SCBCRSE_SUBJ_CODE=N.SMRDOCN_SUBJ_CODE
        INNER JOIN SORLCUR R ON 
            M.SMRRQCM_PIDM=R.SORLCUR_PIDM
            AND M.SMRRQCM_PROGRAM=R.SORLCUR_PROGRAM
        WHERE M.SMRRQCM_REQUEST_NO=
            (
              SELECT MAX(SMRRQCM_REQUEST_NO)
              FROM SMRRQCM
              WHERE SMRRQCM_PIDM = P_PIDM
              AND SMRRQCM_PROGRAM=M.SMRRQCM_PROGRAM
             )
        AND SORLCUR_LMOD_CODE='LEARNER'
        AND SORLCUR_ROLL_IND = 'Y'
        AND SORLCUR_CACT_CODE ='ACTIVE'
        AND M.SMRRQCM_PIDM = P_PIDM
        AND SMRDOCN_TERM_CODE >=SORLCUR_TERM_CODE
        AND SMRDOCN_TERM_CODE <= NVL(SORLCUR_TERM_CODE_END,'99999')
        AND SORLCUR_TERM_CODE_CTLG<>'000000'
    ),
    TODO AS (
        SELECT * FROM Aprob
        UNION ALL
        SELECT * FROM Desaprob
    ),
    MaxTerm AS (
        SELECT MAX(TERM) TERM
        FROM TODO
        WHERE TERM < P_TERM_CODE
    ),
    DesaprobTerm AS (
        SELECT * FROM DESAPROB DP
        INNER JOIN MaxTerm MT ON
          DP.TERM = MT.TERM
        )
    select CURSO
    from DesaprobTerm;
    
BEGIN

    OPEN C_CURSOS;

      LOOP
          FETCH C_CURSOS INTO V_CURSO;
          EXIT WHEN C_CURSOS%NOTFOUND ;
                 
            IF V_CONTADOR = 0 THEN
              P_ASIG_DESAP := to_char(V_CURSO);
              V_CONTADOR := 1;
            ELSE 
            P_ASIG_DESAP := P_ASIG_DESAP || ', ' || to_char(V_CURSO);
            V_CONTADOR := V_CONTADOR + 1;
            END IF;

      END LOOP;
      CLOSE C_CURSOS;

    IF V_CONTADOR > 0 THEN
      P_DESAPROBO := 'TRUE';
    ELSE
      P_DESAPROBO := 'FALSE';
      P_ASIG_DESAP := 'NINGUNO';
    END IF;
  
EXCEPTION
  WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICA_DESAPRUEBA;