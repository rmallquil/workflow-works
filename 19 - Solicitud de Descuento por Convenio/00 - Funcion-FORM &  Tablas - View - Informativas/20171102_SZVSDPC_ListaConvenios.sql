/******************************************************************************/
/* SZVSDPC.sql                                                               */
/******************************************************************************/
/*                                                                            */
/* Descripci�n corta: Script para generar la vista SZVSDPC					  */
/******************************************************************************/
/*                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                 INI    FECHA    */
/* ---------------------------------------------------------- --- ----------- */
/* 1. Creaci�n del C�digo.                                    BYF 02/NOV/2017 */
/*    Lista los convenios vigentes					                          */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*                                                                            */
/* FIN DEL SEGUIMIENTO                                                        */
/*                                                                            */
/******************************************************************************/ 

CREATE OR REPLACE VIEW "BANINST1".SZVSDPC
(
  SZVSDPC_CONVENIO_ID, 
  SZVSDPC_CONVENIO
) AS 

WITH
CTE_tblConvenios AS (
        -- GET Convenios
        SELECT  "IDConvenio" IDConvenio,
                "NomEmpresa" NomEmpresa
        FROM BNE.tblConvenios@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "Activo"='1'
),
CTE_tblConveniosDetalle AS (
		-- GET Detalles de Convenio
        SELECT "IDConvenio" IDConvenio,
        "Modalidad" IDEscuelaADM
        FROM  BNE.tblConveniosDetalle@BDUCCI.CONTINENTAL.EDU.PE
		WHERE ("MatriPorcent" <> 0 or "CuotaPorcent" <> 0)
		and ("MatriPorcent" is not null or "CuotaPorcent" is not null)
)

select distinct co.IDConvenio SZVSDPC_CONVENIO_ID, NomEmpresa SZVSDPC_CONVENIO
from CTE_tblConvenios co
inner join CTE_tblConveniosDetalle cd on
	co.IDConvenio = cd.IDConvenio
where IDEscuelaADM = (SELECT SGBSTDN_DEPT_CODE FROM (
        SELECT SGBSTDN_MAJR_CODE_1,SGBSTDN_CAMP_CODE,SGBSTDN_DEPT_CODE 
          FROM SGBSTDN WHERE SGBSTDN_PIDM = BVSKOSAJ.F_GetParamValue('SOL019',999)
        ORDER BY SGBSTDN_TERM_CODE_EFF DESC
      ) WHERE ROWNUM = 1)
order by NomEmpresa asc;

COMMENT ON TABLE SZVSDPC IS 'Listado de convenios activos';
COMMENT ON COLUMN SZVSDPC.SZVSDPC_CONVENIO_ID IS 'C�digo del convenio';
COMMENT ON COLUMN SZVSDPC.SZVSDPC_CONVENIO IS 'Nombre de la empresa del convenio';