/*
drop procedure p_get_usuario_regaca;
GRANT EXECUTE ON p_get_usuario_regaca TO wfobjects;
GRANT EXECUTE ON p_get_usuario_regaca TO wfauto;

set serveroutput on
DECLARE
      P_ROLE_SEDE           VARCHAR2(4000);
      P_ROLE_EMAILS         VARCHAR2(4000);
      P_ERROR               VARCHAR2(4000);
begin
  P_GET_DATOSROLE_DPP('s01','bu',P_ROLE_SEDE,P_ROLE_EMAILS,P_ERROR);
  DBMS_OUTPUT.PUT_LINE(P_ROLE_SEDE);
  DBMS_OUTPUT.PUT_LINE(P_ROLE_EMAILS);
  DBMS_OUTPUT.PUT_LINE(P_ERROR);
end;
*/

/* ===================================================================================================================
  NOMBRE    : P_GET_USUARIO_DSCTOC
  FECHA     : 04/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento obtiene LOS CORREOS del(os) responsable(s) 
              asignados a dicho ROL + SEDE (ROL_SEDE).
  =================================================================================================================== */
create or replace PROCEDURE P_GET_USUARIO_DSCTOC (
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE, -- Case sensitive
      P_ROLE_CODE           IN WORKFLOW.ROLE.NAME%TYPE, -- Case sensitive
      P_ROLE_SEDE           OUT VARCHAR2,
      P_ROLE_EMAILS         OUT VARCHAR2,
      P_ERROR               OUT VARCHAR2
)
AS
      -- @PARAMETERS
      V_ROLE_ID                 NUMBER;
      V_ORG_ID                  NUMBER;
      V_Email_Address           VARCHAR2(100);
    
      V_SECCION_EXCEPT          VARCHAR2(50);
      
      CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = V_ORG_ID AND ROLE_ID = V_ROLE_ID;
      
      V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
BEGIN 
--
      P_ROLE_SEDE := P_ROLE_CODE || '_DPC_' || P_COD_SEDE;
      
      -- Obtener el ROL_ID 
      V_SECCION_EXCEPT := 'ROLES';
      SELECT ID INTO V_ROLE_ID FROM WORKFLOW.ROLE WHERE NAME = P_ROLE_SEDE;
      V_SECCION_EXCEPT := '';
      
      -- Obtener el ORG_ID 
      V_SECCION_EXCEPT := 'ORGRANIZACION';
      SELECT ID INTO V_ORG_ID FROM WORKFLOW.ORGANIZATION WHERE NAME = 'Root';
      V_SECCION_EXCEPT := '';
     
      -- Obtener los datos de usuarios que relaciona rol y usuario
      V_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
       -- #######################################################################
      OPEN C_ROLE_ASSIGNMENT;
      LOOP
          FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
          EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
                      
                      -- Obtener Datos Usuario
                      SELECT Email_Address INTO V_Email_Address FROM WORKFLOW.WFUSER WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;
          
                      P_ROLE_EMAILS := P_ROLE_EMAILS || V_Email_Address || ',';
      END LOOP;
      CLOSE C_ROLE_ASSIGNMENT;
      V_SECCION_EXCEPT := '';
      
      -- Extraer el ultimo digito en caso sea un "coma"(,)
      SELECT SUBSTR(P_ROLE_EMAILS,1,LENGTH(P_ROLE_EMAILS) -1) INTO P_ROLE_EMAILS
      FROM DUAL
      WHERE SUBSTR(P_ROLE_EMAILS,-1,1) = ',';
      
EXCEPTION
  WHEN TOO_MANY_ROWS THEN 
          IF ( V_SECCION_EXCEPT = 'ROLES') THEN
              P_ERROR := '- Se encontraron mas de un ROL con el mismo nombre: ' || P_ROLE_CODE || P_COD_SEDE;
          ELSIF (V_SECCION_EXCEPT = 'ORGANIZACION') THEN
              P_ERROR := '- Se encontraron mas de una ORGANIZACIÓN con el mismo nombre.';
          ELSIF (V_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := '- Se encontraron mas de un usuario con el mismo ROL.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN NO_DATA_FOUND THEN
          IF ( V_SECCION_EXCEPT = 'ROLES') THEN
              P_ERROR := '- NO se encontró el nombre del ROL: ' || P_ROLE_CODE || P_COD_SEDE;
          ELSIF (V_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_ERROR := '- NO se encontró el nombre de la ORGANIZACION.';
          ELSIF (V_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := '- NO  se encontró ningun usuario con esas caracteristicas.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_USUARIO_DSCTOC;
