/*
SET SERVEROUTPUT ON
DECLARE P_DEUDA VARCHAR2(10);
BEGIN
    P_DEUDA := F_VALIDAR_FECHA_DISPONIBLE(4018);
    DBMS_OUTPUT.PUT_LINE(P_DEUDA);
END;
*/

create or replace FUNCTION F_VALIDAR_FECHA_DISPONIBLE (
        P_FOLIO_SOLICITUD   IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_VALIDAR_FECHA_DISPONIBLE
  FECHA     : 10/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida que a la fecha de recepcion de la solicitud este dentro de las 48 horas para su atencion.
              se considdera la atencion hasta antes de finalizar el dia de finalizada las 48 horas

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS    
    V_RECEPTION_DATE          SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE;
    
    -- CURSOR 
    CURSOR C_SVRSVPR IS
    SELECT SVRSVPR_RECEPTION_DATE FROM SVRSVPR
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;

BEGIN
--
    -- >> GET - FECHA DE RECEPCION DE LA SOLICITUD DE SERVICIO --
    OPEN C_SVRSVPR;
    LOOP
      FETCH C_SVRSVPR INTO  V_RECEPTION_DATE;
      EXIT WHEN C_SVRSVPR%NOTFOUND;
    END LOOP;
    CLOSE C_SVRSVPR;


    IF(SYSDATE < TO_DATE(TO_CHAR(V_RECEPTION_DATE + 3,'dd/mm/yyyy'),'dd/mm/yyyy')) THEN
        RETURN 'TRUE';
    ELSE
        RETURN 'FALSE';
    END IF;
    
EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END F_VALIDAR_FECHA_DISPONIBLE;  