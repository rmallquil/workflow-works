/*
SET SERVEROUTPUT ON
DECLARE P_FECHA_VALIDA VARCHAR2(10);
BEGIN
    P_FECHA_VALIDA := F_VALIDAR_FECHA_DISPONIBLE('21/05/2017');
    DBMS_OUTPUT.PUT_LINE(P_FECHA_VALIDA);
END;
*/

create or replace FUNCTION F_VALIDAR_FECHA_DISPONIBLE (
        P_DATE   IN DATE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_VALIDAR_FECHA_DISPONIBLE
  FECHA     : 24/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida que la fecha ENVIADA este dentro de las 48 horas para su atencion.
              se considdera la atencion hasta antes de finalizar el dia de finalizada las 48 horas

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS    
    V_RECEPTION_DATE          SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE;
    
    -- CURSOR 
--    CURSOR C_SVRSVPR IS
--    SELECT SVRSVPR_RECEPTION_DATE FROM SVRSVPR
--    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;

BEGIN
--
    -- >> GET - FECHA DE RECEPCION DE LA SOLICITUD DE SERVICIO --
--    OPEN C_SVRSVPR;
--    LOOP
--      FETCH C_SVRSVPR INTO  V_RECEPTION_DATE;
--      EXIT WHEN C_SVRSVPR%NOTFOUND;
--    END LOOP;
--    CLOSE C_SVRSVPR;

    -- Valida que la fecha este SEA MENOR A 3 DIAS DE TOLERANCIA.
    IF(SYSDATE < TO_DATE(TO_CHAR(P_DATE + 3,'dd/mm/yyyy'),'dd/mm/yyyy')) THEN
        RETURN 'TRUE';
    ELSE
        RETURN 'FALSE';
    END IF;
    
EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END F_VALIDAR_FECHA_DISPONIBLE;  