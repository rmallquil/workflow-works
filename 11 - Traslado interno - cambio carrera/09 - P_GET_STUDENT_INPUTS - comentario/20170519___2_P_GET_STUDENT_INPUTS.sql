  /*
SET SERVEROUTPUT ON
DECLARE
    P_PROGRAM_OLD           SOBCURR.SOBCURR_PROGRAM%TYPE;
    P_PROGRAM_OLD_DESC      VARCHAR2(250);
    P_PROGRAM_NEW           SOBCURR.SOBCURR_PROGRAM%TYPE;
    P_PROGRAM_NEW_DESC      VARCHAR2(100);
    P_COMENTARIO_ALUMNO     VARCHAR2(4000);
    P_TELEFONO_ALUMNO       VARCHAR2(50);
begin
    P_GET_STUDENT_INPUTS(4025,P_PROGRAM_OLD,P_PROGRAM_OLD_DESC,P_PROGRAM_NEW,P_PROGRAM_NEW_DESC,P_COMENTARIO_ALUMNO,P_TELEFONO_ALUMNO);
    DBMS_OUTPUT.PUT_LINE(P_COMENTARIO_ALUMNO);
    DBMS_OUTPUT.PUT_LINE(P_PROGRAM_OLD || ' **** ' || P_PROGRAM_OLD_DESC || ' **** ' || P_PROGRAM_NEW  || ' **** ' || P_PROGRAM_NEW_DESC || ' **** ' || P_TELEFONO_ALUMNO);
end;
  */

CREATE or replace PROCEDURE P_GET_STUDENT_INPUTS (
      P_FOLIO_SOLICITUD       IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_PROGRAM_OLD           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_PROGRAM_OLD_DESC      OUT VARCHAR2,
      P_PROGRAM_NEW           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_PROGRAM_NEW_DESC      OUT VARCHAR2,
      P_COMENTARIO_ALUMNO     OUT VARCHAR2,
      P_TELEFONO_ALUMNO       OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_STUDENT_INPUTS
  FECHA     : 11/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Para Solicitudes 'SOL013' : 
              Obtiene EL PROGRAMA, PROGRAM_DESC, COMENTARIO Y TELEFONO de una solicitud especifica. 
              Se establecio que ultimo dato es el TELEFONO y el penultimo el COMENTARIO.

  NRO     FECHA         USUARIO       MODIFICACION
  =================================================================================================================== */
AS
      V_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
      V_COMENTARIO_ALUMNO       VARCHAR2(4000);
      V_ADDL_DATA_SEQ           SVRSVAD.SVRSVAD_ADDL_DATA_SEQ%TYPE;
      V_PIDM                    SPRIDEN.SPRIDEN_PIDM%TYPE;
      V_ERROR                   EXCEPTION; 
      
      -- OBTENER PROGRAMA ACTUAL(Carrera) y  descriocion
      CURSOR C_PROGRAM_OLD IS
        SELECT SORLCUR_PROGRAM, SZVMAJR_DESCRIPTION FROM (
                SELECT  SORLCUR_PROGRAM,SZVMAJR_DESCRIPTION
                FROM SORLCUR        
                INNER JOIN SORLFOS
                    ON SORLCUR_PIDM         = SORLFOS_PIDM 
                    AND SORLCUR_SEQNO       = SORLFOS.SORLFOS_LCUR_SEQNO
                INNER JOIN SZVMAJR 
                    ON SORLCUR_PROGRAM = SZVMAJR_CODE 
                WHERE SORLCUR_PIDM          = V_PIDM 
                    AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                    AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                    AND SORLCUR_CURRENT_CDE = 'Y'
                ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
        ) WHERE ROWNUM = 1;
      
      -- OBTENER NUEVO PROGRAMA (Carrera) y detalle selecionado por el estudiante
      CURSOR C_PROGRAM_NEW IS
        SELECT SVRSVAD_ADDL_DATA_CDE,SVRSVAD_ADDL_DATA_DESC
        FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
        INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
            ON SVRSVAD_ADDL_DATA_SEQ    = SVRSRAD_ADDL_DATA_SEQ
        WHERE   SVRSVAD_ADDL_DATA_SEQ   = 1 -- Se configuro como primer dato  CARRERA
          AND SVRSRAD_SRVC_CODE         = V_CODIGO_SOLICITUD
          AND SVRSVAD_PROTOCOL_SEQ_NO   = P_FOLIO_SOLICITUD;
          
      
      -- OBTENER COMENTARIO Y EL TELEFONO DE LA SOLICITUD (LOS DOS ULTIMOS DATOS)
      CURSOR C_SVRSVAD IS
        SELECT SVRSVAD_ADDL_DATA_SEQ,SVRSVAD_ADDL_DATA_DESC
        FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
        INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
        ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
        WHERE SVRSRAD_SRVC_CODE = V_CODIGO_SOLICITUD
        AND SVRSVAD_ADDL_DATA_CDE = 'PREGUNTA'
        AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
        ORDER BY SVRSVAD_ADDL_DATA_SEQ;
      
BEGIN
      
      -- GET CODIGO SOLICITUD
      SELECT SVRSVPR_SRVC_CODE, SVRSVPR_PIDM
      INTO V_CODIGO_SOLICITUD, V_PIDM
      FROM SVRSVPR 
      WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
      
      -- OBTENER PROGRAMA ACTUAL(Carrera) y  descriocion
      OPEN C_PROGRAM_OLD;
        LOOP
          FETCH C_PROGRAM_OLD INTO P_PROGRAM_OLD, P_PROGRAM_OLD_DESC;
          EXIT WHEN C_PROGRAM_OLD%NOTFOUND;
        END LOOP;
      CLOSE C_PROGRAM_OLD;
      
      -- OBTENER NUEVO PROGRAMA (Carrera) y su descriocion
      OPEN C_PROGRAM_NEW;
        LOOP
          FETCH C_PROGRAM_NEW INTO P_PROGRAM_NEW, P_PROGRAM_NEW_DESC;
          EXIT WHEN C_PROGRAM_NEW%NOTFOUND;
        END LOOP;
      CLOSE C_PROGRAM_NEW;
      
      -- OBTENER COMENTARIO Y EL TELEFONO DE LA SOLICITUD (LOS DOS ULTIMOS DATOS)
      OPEN C_SVRSVAD;
        LOOP
          FETCH C_SVRSVAD INTO V_ADDL_DATA_SEQ, V_COMENTARIO_ALUMNO;
          IF C_SVRSVAD%FOUND THEN
              IF V_ADDL_DATA_SEQ = 2 THEN
                  P_COMENTARIO_ALUMNO := V_COMENTARIO_ALUMNO;
              ELSIF V_ADDL_DATA_SEQ = 3 THEN
                  P_TELEFONO_ALUMNO := V_COMENTARIO_ALUMNO;
              END IF;
          ELSE EXIT;
        END IF;
        END LOOP;
      CLOSE C_SVRSVAD;
      
      IF (P_TELEFONO_ALUMNO IS NULL OR P_COMENTARIO_ALUMNO IS NULL OR P_PROGRAM_NEW IS NULL) THEN
          RAISE V_ERROR;
      END IF;
EXCEPTION
  WHEN V_ERROR THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontró el comentario y/o el teléfono.' );
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_STUDENT_INPUTS;