/*
SET SERVEROUTPUT ON
DECLARE P_MESSAGE VARCHAR2(4000);
BEGIN
    P_REMOVE_CTA(326495,'43317482','UPGT','S01','201710','105',P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
    
    P_REMOVE_CTA(127673,'41453900','UREG','S01','201710','315',P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
END;
*/

CREATE OR REPLACE PROCEDURE P_REMOVE_CTA (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_PROGRAM_OLD         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_MESSAGE             OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_REMOVE_CTA
  FECHA     : 29/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Desabilita la cta corriente especifica por PIDM.
              
  MODIFICACIONES
  NRO       FECHA         USUARIO         MODIFICACION
  001       01/08/2017    rmallquo        Se agrego la eliminacion de la cuenta
  =================================================================================================================== */
AS
      -- @PARAMETERS
      V_APEC_CAMP             VARCHAR2(9);
      V_APEC_TERM             VARCHAR2(9);
      V_APEC_DEPT             VARCHAR2(9);
      V_PART_PERIODO          VARCHAR2(9);
      V_ABONO_OLD             NUMBER := 0;
      
      V_RESULT                INTEGER;
      V_FECINIC               DATE;
      V_APEC_ALUMNO           VARCHAR2(15);
      V_IDSECCIONC            VARCHAR2(10);
BEGIN 
--
        --**************************************************************************************************************
        -- GET CRONOGRAMA SECCIONC
        SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                  WHEN 'UPGT' THEN 'W' 
                                  WHEN 'UREG' THEN 'R' 
                                  WHEN 'UPOS' THEN '-' 
                                  WHEN 'ITEC' THEN '-' 
                                  WHEN 'UCIC' THEN '-' 
                                  WHEN 'UCEC' THEN '-' 
                                  WHEN 'ICEC' THEN '-' 
                                  ELSE '1' END ||
                CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                  WHEN 'F01' THEN 'A' 
                                  WHEN 'F02' THEN 'L' 
                                  WHEN 'F03' THEN 'C' 
                                  WHEN 'V00' THEN 'V' 
                                  ELSE '9' END
        INTO V_PART_PERIODO
        FROM STVCAMP,STVDEPT 
        WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;        
        
        SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
        SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
        SELECT CZRDEPT_DEPT_BDUCCI INTO V_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO
        
        -- GET  IDSECCIONC Y IDALUMNO CON SU CTA CORRIENTE
        WITH 
            CTE_tblSeccionC AS (
                    -- GET SECCIONC
                    SELECT  "IDSeccionC" IDSeccionC,
                            "FecInic" FecInic
                    FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                    WHERE "IDDependencia"='UCCI'
                    AND "IDsede"    = V_APEC_CAMP
                    AND "IDPerAcad" = V_APEC_TERM
                    AND "IDEscuela" = P_PROGRAM_OLD
                    AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                    AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                    AND "IDSeccionC" <> '15NEX1A'
                    AND SUBSTRB("IDSeccionC",-2,2) IN (
                        -- PARTE PERIODO           
                        SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (V_PART_PERIODO || '%')
                    )
            ),
            CTE_tblPersonaAlumno AS (
                    SELECT "IDAlumno" IDAlumno 
                    FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
                    WHERE "IDPersona" IN ( 
                        SELECT "IDPersona" FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM
                    )
            )
            SELECT SUM("Abono") Abono, "IDSeccionC", "FecInic", "IDAlumno" INTO V_ABONO_OLD, V_IDSECCIONC, V_FECINIC, V_APEC_ALUMNO
            FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
            WHERE "IDDependencia" = 'UCCI'
            AND "IDAlumno"     IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
            AND "IDSede"      = V_APEC_CAMP
            AND "IDPerAcad"   = V_APEC_TERM
            AND "IDEscuela"   = P_PROGRAM_OLD
            AND "IDSeccionC"  IN (SELECT IDSeccionC FROM CTE_tblSeccionC)
            GROUP BY "IDSeccionC", "FecInic", "IDAlumno";
        
        
        -- Validar y DESABILITAR la cta corriente
        IF (V_ABONO_OLD IS NULL) THEN
            P_MESSAGE := ' No Se encontro la Cuenta Corriente';
        ELSIF(V_ABONO_OLD > 0) THEN
            P_MESSAGE := ' Se detecto ABONOS en la Cuenta Corriente.';
        ELSE 
            
             -- Eliminar la CTA CORRIENTE.(en caso no tenga abonos)
            ---------------------------------------    
            V_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
                  'dbo.sp_EliminarCtaCorriente "'
                  || 'UCCI" , "'|| V_APEC_ALUMNO || '" , "' || V_APEC_TERM || '" , "' || V_APEC_CAMP ||'" , "'
                  || V_IDSECCIONC ||'" , "'|| TO_CHAR(V_FECINIC, 'dd/mm/yyyy')  ||'" '
            );
            
            COMMIT;
        END IF;
        
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_REMOVE_CTA;