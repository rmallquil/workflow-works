/*************************************************************************************************************************************
------------------ INSERTAR ESCALA --------
**************************************************************************************************************************************/
PROCEDURE P_GET_USUARIO (
      P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
      P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE
)
/* ===================================================================================================================
  NOMBRE    : P_GET_USUARIO
  FECHA     : 04/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento LOS CORREOS deL(os) responsable(s) 
              de Registros Académicos de esa sede.
  =================================================================================================================== */
AS
      -- @PARAMETERS
      V_APEC_CAMP             VARCHAR2(9);
      V_APEC_TERM             VARCHAR2(10);
      V_DATE_NOW              DATE:= SYSDATE;
      V_USER                  VARCHAR2(15) := USER;
BEGIN 
--
        SELECT CZRCAMP_CAMP_BDUCCI INTO P_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
        SELECT CZRTERM_TERM_BDUCCI INTO P_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE ;-- PERIODO
        
--      SELECT  "IDSede" , "IDDependencia", "IDAlumno", "IDPerAcad", "IDSeccionC", "FecInic", "FechaEscala", "IDEscala", 
--              "Beca", "Cronograma", "IDEscala1", "IDEscala2", "IDEscala3", "IDEscala4", "IDEscala5", "IDEscala6", "Beca1", 
--              "Beca2", "Beca3", "Beca4", "Beca5", "Beca6", "Fechavalidez", "IDPersonal", "Observacion", "IDConcepto", 
--              "Reincorporacion", "Ultimo", "TipoCambEscala", "email", "IDCategoriaPens" 
--      FROM dbo.tblAlumnoEscala@BDUCCI.CONTINENTAL.EDU.PE
      INSERT INTO dbo.tblAlumnoEscala@BDUCCI.CONTINENTAL.EDU.PE (
                "IDSede" ,               "IDDependencia",              "IDAlumno", 
                "IDPerAcad",              "IDSeccionC",                 "FecInic", 
                "FechaEscala",            "IDEscala",                   "Beca", 
                "Cronograma",             "IDEscala1",                  "IDEscala2", 
                "IDEscala3",              "IDEscala4",                  "IDEscala5", 
                "IDEscala6",              "Beca1",                      "Beca2", 
                "Beca3",                  "Beca4",                      "Beca5", 
                "Beca6",                  "Fechavalidez",               "IDPersonal", 
                "Observacion",            "IDConcepto",                 "Reincorporacion", 
                "Ultimo",                 "TipoCambEscala",             "email", 
                "IDCategoriaPens")
      VALUES (
                V_APEC_CAMP,              'UCCI',                       P_ID_ALUMNO,
                V_APEC_TERM,              'XESCALA',                    '01/01/1980',
                V_DATE_NOW,               'X',                          0,
                '1',                      2/*SEGUN REGLAS*/,            2/*SEGUN REGLAS*/,
                2/*SEGUN REGLAS*/,        2/*SEGUN REGLAS*/,            2/*SEGUN REGLAS*/,
                2/*SEGUN REGLAS*/,        0,                            0,
                0,                        0,                            0,
                0,                        V_DATE_NOW,                   V_USER,
                'Observaciones ..',       'C00',                        0,
                0,                        NULL,                         0,
                'X'/*IDCategoriaPens*/
      );
      
      
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_USUARIO;