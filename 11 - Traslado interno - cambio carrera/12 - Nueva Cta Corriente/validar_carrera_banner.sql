use BDUCCI 
go
declare
  @c_Divs char (4),--dependencia
  @c_Campus varchar(4), -- sede
  @c_TermCode char(6), -- periodo
  @c_ID varchar(10) -- dni
SET @c_Divs = 'UCCI'
SET @c_Campus = 'HYO'
SET @c_TermCode = '2017-2'
SET @c_ID = '45560309' -- alumnos de beca 18
-----------------------------------
-----------------------------------
DECLARE 
    
    ------ UPDATE OTROS CONCEPTOS
    @c_IDPersona varchar(15),
    @c_idalumno varchar(10),
------------------------------- VARIABLES DE BANNER
    @c_carrera varchar(3),
    @c_TERM_CODE varchar(6),
    @c_pidm numeric(8,0),
    @c_departament varchar(10),
    @V_NCRED_TOTALES numeric(7,3),
    @V_NCRED_BECA numeric(7,3),
    @V_BECA18 char(1),
    @c_ejecutar char(1)

select distinct top 1 @c_idalumno = IDAlumno from dbo.tblMatricula where idalumno IN (
                                                    Select distinct idalumno from dbo.DAT_AlumnoBasico where IDPersona IN (SELECT IDPersona FROM dbo.tblpersona t1 WHERE t1.DNI=@c_ID)
                                                )
Select @c_IDPersona = IDPersona, @c_pidm = IDPersonaN from dbo.tblpersona where IDPersona = (select IDPersona from dbo.tblPersonaAlumno where IDAlumno=@c_idalumno)

if (@c_pidm is null)
        begin
        select top(1) @c_IDPersona=IDPersona, @c_pidm = idpersonaN from dbo.tblPersona where DNI=@c_ID
    end

    SET @c_TERM_CODE = CONCAT(SUBSTRING(@c_termcode,1,4),SUBSTRING(@c_termcode,6,1),'0')
------------------ TRAENDO LOS DATOS DE BANNER
    EXECUTE('BEGIN BANINST1.TZKCDAA.P_MATERIAS_CREDITOS(?,?,?,?,?,?,?,?,?); END;',
    --EXECUTE('BEGIN BANINST1.PRUEBASP_MATERIAS_CREDITOS(?,?,?,?,?,?,?,?,?); END;',
    @c_pidm,
    @c_TERM_CODE,
    @c_Divs OUTPUT,
    @c_Campus OUTPUT,
    @c_departament OUTPUT,
    @c_carrera   OUTPUT,
    @V_NCRED_TOTALES OUTPUT,
    @V_NCRED_BECA OUTPUT,
    @V_BECA18 OUTPUT)
    AT BANNERCAU
	--AT MIGR
select @c_pidm, @c_TERM_CODE, @c_Divs,@c_Campus,@c_departament,@c_carrera,@V_NCRED_TOTALES,@V_NCRED_BECA,@V_BECA18

select * from dbo.tblEscuela where IDEscuela=@c_carrera				