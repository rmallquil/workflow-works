/**************************************************************
        SP de BDUCCI ---------------> APEC
**************************************************************/

/* PERMISOS - IMPORTANTE */ 
--        grant execute on [dbo].[sp_AsignarEscala_CtaCorriente]  to [SSLSCDataCBanner]

/**************************************************************/

USE [BDUCCI]
GO
/* ===================================================================================================================
NOMBRE    : [dbo].[sp_AsignarEscala_CtaCorriente]
FECHA   : 17/08/2017
AUTOR   : Richard Mallqui Lopez 
OBJETIVO  : Asignar Escala y crea Cuenta Corriente, 
        **Basado en el script proporcionado por BFLORESV BNE.sp_Escala_Traslado

MODIFICACIONES
NRO   FECHA   USUARIO   MODIFICACION
=================================================================================================================== */
CREATE PROCEDURE [dbo].[sp_AsignarEscala_CtaCorriente]
  @c_IDDependencia  VARCHAR(100),
  @c_IDAlumno     VARCHAR(100),
  @c_IDPerAcad    VARCHAR(100),
  @c_IDEscuelaADM   VARCHAR(100),
  @c_IDSede     VARCHAR(100),
  @c_Personal     VARCHAR(50)
AS
  DECLARE 
  @IDEscala   VARCHAR(2),
  @Cronograma   VARCHAR(3),
  @Observacion  VARCHAR(255) = 'Asignación de escala - Traslado Interno WORKFLOW'
     
  DECLARE 
  @t_cronogramas table (
          IDEscuelaADM  varchar(4), 
          IDSede      varchar(4), 
          Cronograma    varchar(2), 
          IDEscala    varchar(2)
    )
BEGIN 
----- 
  insert into @t_cronogramas values ('ADM','HYO', '01', '2')
  insert into @t_cronogramas values ('ADM','ARQP', '16', '2')
  insert into @t_cronogramas values ('ADG','HYO', '03', '2')
  insert into @t_cronogramas values ('ADG','ARQP', '10', '2')
  insert into @t_cronogramas values ('ADG','CUZ', '14', '2')
  insert into @t_cronogramas values ('ADG','LIMA', '06', '2')
  insert into @t_cronogramas values ('ADV','VIR', '08', '1')
  
  -------------------------------------------------------------
  -- GET CRONOGRAMA y ESCALA
  SELECT @Cronograma = Cronograma, @IDEscala = IDEscala
  FROM @t_cronogramas
  WHERE IDEscuelaADM = @c_IDEscuelaADM
    AND IDSede = @c_IDSede
  
  
  SET NOCOUNT ON;
  SET FMTONLY OFF
  BEGIN TRY
    BEGIN TRANSACTION
      
      IF @Cronograma IS NULL OR @IDEscala IS NULL BEGIN
        RAISERROR (15600,-1,-1,'Inconsistencia detectada: No se encontro la escala y/o cronograma.'); 
      END;

      IF NOT EXISTS (SELECT IDAlumno FROM tblAlumnoEscala 
              where IDAlumno = @c_IDAlumno 
                and IDSede = @c_IDSede 
                and Cronograma  = @Cronograma 
                and IDPersonal  = @c_Personal
                and Observacion = @Observacion)
      BEGIN
          --ASIGNACIÓN DE ESCALA
          INSERT INTO tblAlumnoEscala (
              IDSede,         IDDependencia,  IDAlumno,         IDPerAcad,
              IDSeccionC,     FecInic,        FechaEscala,      IDEscala,
              Beca,           Cronograma,     IDEscala1,        IDEscala2, 
              IDEscala3,      IDEscala4,      IDEscala5,        IDEscala6,
              Beca1,          Beca2,          Beca3,            Beca4,
              Beca5,          Beca6,          Fechavalidez,     IDPersonal, 
              Observacion,    IDConcepto,     Reincorporacion,  Variacion,
              ModPens,        NroCred,        Descuento,        MontoPagar,
              MasCredNormal,  Ultimo
          )
          SELECT DISTINCT TOP 1 
              @c_IDSede,      @c_IDDependencia,     @c_IDAlumno,    @c_IDPerAcad,
              'XESCALA',      '01/01/1980',         GETDATE(),      @IDEscala,
              0,              @Cronograma,          @IDEscala,      @IDEscala, 
              @IDEscala,      @IDEscala,            @IDEscala,      @IDEscala,
              0,              0,                    0,              0,
              0,              0,                    GETDATE(),      @c_Personal,
              @Observacion,   'C00',                '0',            NULL, 
              NULL,           NULL,                 NULL,           NULL, 
              NULL,           '0'
      END

      exec [dbo].[sp_ActualizarMontoPagarBanner] @c_IDDependencia,@c_IDSede,@c_IDPerAcad,@c_IDAlumno
  
      exec [BNE].[sp_Actualizar_Registro_SME_Alumnoestado] @c_IDAlumno,@c_IDPerAcad,@c_IDSede,@c_IDDependencia

      COMMIT TRANSACTION

  END TRY
  BEGIN CATCH

      --INSTRUCCIONES EN CASO DE ERRORES
      DECLARE @ErrorMessage NVARCHAR(4000);  
      DECLARE @ErrorSeverity INT;  
      DECLARE @ErrorState INT;  

      SELECT   
        @ErrorMessage = ERROR_MESSAGE(),  
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorState = ERROR_STATE();  

      -- Use RAISERROR inside the CATCH block to return error  
      -- information about the original error that caused  
      -- execution to jump to the CATCH block.  
      RAISERROR (
        @ErrorMessage, -- Message text.  
        @ErrorSeverity, -- Severity.  
        @ErrorState -- State.  
        );  
    
      ROLLBACK TRANSACTION

  END CATCH
END

GO 

grant execute on [dbo].[sp_AsignarEscala_CtaCorriente]  to [SSLSCDataCBanner]