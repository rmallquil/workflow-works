/*
-- buscar objecto
select dbms_metadata.get_ddl('PROCEDURE','P_SET_CODDETALLE') from dual
drop procedure p_set_coddetalle_alumno;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfobjects;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfauto;

SET SERVEROUTPUT ON
DECLARE   P_TRAN_NUMBER           TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
          P_ERROR                 VARCHAR2(100);
BEGIN
    P_SET_CODDETALLE(241106,'REC','201620',P_TRAN_NUMBER,P_ERROR);
    DBMS_OUTPUT.PUT_LINE(P_TRAN_NUMBER  || '--' || P_ERROR);
END;
*/
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE P_SET_CODDETALLE ( 
        P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
        P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
        P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
        P_DETL_CODE             IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
        P_TRAN_NUMBER           OUT TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
        P_ERROR                 OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SET_CODDETALLE
  FECHA     : 09/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : genera una deuda por CODIGO DETALLE.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
    V_NOM_SERVICIO                VARCHAR2(30);
    
    V_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;          
    V_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
    V_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
    V_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
    V_ALUM_TERM_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
    V_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP 
    V_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE

    V_CARGO_MINIMO          SFRRGFE.SFRRGFE_MIN_CHARGE%TYPE;
    V_ROWID                 GB_COMMON.INTERNAL_RECORD_ID_TYPE;
    V_SUB_PTRM              VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
     
    -- APEC PARAMS
    V_PART_PERIODO          VARCHAR2(9);
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_DEPT             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(10);
    V_APEC_IDSECCIONC       VARCHAR2(15);
    V_APEC_FECINIC          DATE;
    V_APEC_MOUNT            NUMBER;
    V_APEC_IDALUMNO         VARCHAR2(10);
      
    -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
    CURSOR C_SORLCUR_FOS IS
    SELECT    SORLCUR_LEVL_CODE,        
              SORLCUR_COLL_CODE,
              SORLCUR_DEGC_CODE,  
              SORLCUR_PROGRAM,      
              SORLCUR_TERM_CODE_ADMIT,  
              SORLCUR_STYP_CODE,  
              SORLCUR_RATE_CODE  
    FROM (
            SELECT    SORLCUR_LEVL_CODE,    SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,  
                      SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  SORLCUR_STYP_CODE,  
                      SORLCUR_RATE_CODE
            FROM SORLCUR        INNER JOIN SORLFOS
                  ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                  AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
            WHERE   SORLCUR_PIDM        =   P_PIDM
                AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y'
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
    ) WHERE ROWNUM <= 1;
    
    -- Calculando parte PERIODO (sub PTRM)
    CURSOR C_SFRRSTS_PTRM IS
    SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                              WHEN 'UPGT' THEN 'W' 
                              WHEN 'UREG' THEN 'R' 
                              WHEN 'UPOS' THEN '-' 
                              WHEN 'ITEC' THEN '-' 
                              WHEN 'UCIC' THEN '-' 
                              WHEN 'UCEC' THEN '-' 
                              WHEN 'ICEC' THEN '-' 
                              ELSE '1' END ||
            CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                              WHEN 'F01' THEN 'A' 
                              WHEN 'F02' THEN 'L' 
                              WHEN 'F03' THEN 'C' 
                              WHEN 'V00' THEN 'V' 
                              ELSE '9' END SUBPTRM
    FROM STVCAMP,STVDEPT 
    WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;
    
BEGIN

      -- #######################################################################
      -- >> GET DATOS --
      OPEN C_SORLCUR_FOS;
      LOOP
        FETCH C_SORLCUR_FOS INTO  V_ALUM_NIVEL,     V_ALUM_ESCUELA,   V_ALUM_GRADO,
                                  V_ALUM_PROGRAMA,  V_ALUM_TERM_ADM,  V_ALUM_TIPO_ALUM, V_ALUM_TARIFA;
        EXIT WHEN C_SORLCUR_FOS%NOTFOUND;
      END LOOP;
      CLOSE C_SORLCUR_FOS;
      

      ---#################################----- APEC -----#######################################
      --########################################################################################
      -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
      IF PKG_GLOBAL.GET_VAL = 0  THEN -- 0 ---> APEC(BDUCCI)

            -- GET CRONOGRAMA SECCIONC
              -- >> calculando PARTE PERIODO  --
              OPEN C_SFRRSTS_PTRM;
                LOOP
                  FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
                  EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
                END LOOP;
              CLOSE C_SFRRSTS_PTRM;
            
            SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
            SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
            SELECT CZRDEPT_DEPT_BDUCCI INTO V_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO

            -- GET DATOS CTA CORRIENTE -- P_APEC_IDALUMNO
            WITH 
                CTE_tblSeccionC AS (
                        -- GET SECCIONC
                        SELECT  "IDSeccionC" IDSeccionC,
                                "FecInic" FecInic
                        FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                        WHERE "IDDependencia"='UCCI'
                        AND "IDsede"    = V_APEC_CAMP
                        AND "IDPerAcad" = V_APEC_TERM
                        AND "IDEscuela" = V_ALUM_PROGRAMA
                        AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                        AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                        AND "IDSeccionC" <> '15NEX1A'
                        AND SUBSTRB("IDSeccionC",-2,2) IN (
                            -- PARTE PERIODO         
                            SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (V_SUB_PTRM || '%')
                        )
                ),
                CTE_tblPersonaAlumno AS (
                        SELECT "IDAlumno" IDAlumno 
                        FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
                        WHERE "IDPersona" IN ( 
                            SELECT "IDPersona" FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM
                        )
                )
            SELECT "IDAlumno", "IDSeccionC" INTO V_APEC_IDALUMNO, V_APEC_IDSECCIONC
            FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
            WHERE "IDDependencia" = 'UCCI'
            AND "IDAlumno"    IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
            AND "IDSede"      = V_APEC_CAMP
            AND "IDPerAcad"   = V_APEC_TERM
            AND "IDEscuela"   = V_ALUM_PROGRAMA
            AND "IDSeccionC"  IN ( SELECT IDSeccionC FROM CTE_tblSeccionC )
            AND "IDConcepto"  = P_DETL_CODE;
            
            -- GET MONTO 
            SELECT "Monto" INTO V_APEC_MOUNT
            FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE 
            WHERE "IDDependencia" = 'UCCI'
            AND "IDSede"      = V_APEC_CAMP
            AND "IDPerAcad"   = V_APEC_TERM
            AND "IDSeccionC"  = V_APEC_IDSECCIONC
            AND "IDConcepto"  = P_DETL_CODE;

            -- UPDATE MONTO(s)
            UPDATE dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
            SET "Cargo" = "Cargo" + V_APEC_MOUNT
            WHERE "IDDependencia" = 'UCCI'
            AND "IDAlumno"    = V_APEC_IDALUMNO
            AND "IDSede"      = V_APEC_CAMP
            AND "IDPerAcad"   = V_APEC_TERM
            AND "IDEscuela"   = V_ALUM_PROGRAMA
            AND "IDSeccionC"  = V_APEC_IDSECCIONC
            AND "IDConcepto"  = P_DETL_CODE;
            
            P_TRAN_NUMBER := 0;

      ELSE -- 1 ---> BANNER

              -- GET - Codigo detalle y tambien VALIDA que si se encuentre configurado correctamente.
              SELECT SFRRGFE_MIN_CHARGE INTO V_CARGO_MINIMO
              FROM SFRRGFE 
              WHERE SFRRGFE_TERM_CODE = P_TERM_CODE AND SFRRGFE_DETL_CODE = P_DETL_CODE AND SFRRGFE_TYPE = 'STUDENT'
              AND NVL(NVL(SFRRGFE_LEVL_CODE, v_alum_nivel),'-')           = NVL(NVL(v_alum_nivel, SFRRGFE_LEVL_CODE),'-')--------------------- Nivel
              AND NVL(NVL(SFRRGFE_CAMP_CODE, P_CAMP_CODE),'-')          = NVL(NVL(P_CAMP_CODE, SFRRGFE_CAMP_CODE),'-')  ----------------- Campus (sede)
              AND NVL(NVL(SFRRGFE_COLL_CODE, v_alum_escuela),'-')         = NVL(NVL(v_alum_escuela, SFRRGFE_COLL_CODE),'-') --------------- Escuela 
              AND NVL(NVL(SFRRGFE_DEGC_CODE, v_alum_grado),'-')           = NVL(NVL(v_alum_grado, SFRRGFE_DEGC_CODE),'-') ----------- Grado
              AND NVL(NVL(SFRRGFE_PROGRAM, v_alum_programa),'-')          = NVL(NVL(v_alum_programa, SFRRGFE_PROGRAM),'-') ---------- Programa
              AND NVL(NVL(SFRRGFE_TERM_CODE_ADMIT, V_ALUM_TERM_ADM),'-')  = NVL(NVL(V_ALUM_TERM_ADM, SFRRGFE_TERM_CODE_ADMIT),'-') --------- Periodo Admicion
              -- SFRRGFE_PRIM_SEC_CDE -- Curriculums (prim, secundario, cualquiera)
              -- SFRRGFE_LFST_CODE -- Tipo Campo Estudio (MAJOR, ...)
              -- SFRRGFE_MAJR_CODE -- Codigo Campo Estudio (Carrera)
              AND NVL(NVL(SFRRGFE_DEPT_CODE, P_DEPT_CODE),'-')    = NVL(NVL(P_DEPT_CODE, SFRRGFE_DEPT_CODE),'-') ------------ Departamento
              -- SFRRGFE_LFST_PRIM_SEC_CDE -- Campo Estudio   (prim, secundario, cualquiera)
              AND NVL(NVL(SFRRGFE_STYP_CODE_CURRIC, v_alum_tipo_alum),'-') = NVL(NVL(v_alum_tipo_alum, SFRRGFE_STYP_CODE_CURRIC),'-') ------ Tipo Alumno Curriculum
              AND NVL(NVL(SFRRGFE_RATE_CODE_CURRIC, v_alum_tarifa),'-')   = NVL(NVL(v_alum_tarifa, SFRRGFE_RATE_CODE_CURRIC),'-'); ------- Trf Curriculum (Escala)

              -- GET - Descripcion de CODIGO DETALLE  
              SELECT TBBDETC_DESC INTO V_NOM_SERVICIO FROM TBBDETC WHERE TBBDETC_DETAIL_CODE = P_DETL_CODE;
            
              -- #######################################################################
              -- GENERAR DEUDA
              TB_RECEIVABLE.p_create ( p_pidm                 =>  P_PIDM,        -- PIDEM ALUMNO
                                       p_term_code            =>  P_TERM_CODE,          -- DETALLE
                                       p_detail_code          =>  P_DETL_CODE,      -- CODIGO DETALLE 
                                       p_user                 =>  USER,               -- USUARIO
                                       p_entry_date           =>  SYSDATE,     
                                       p_amount               =>  V_CARGO_MINIMO,
                                       p_effective_date       =>  SYSDATE,
                                       p_bill_date            =>  NULL,    
                                       p_due_date             =>  NULL,    
                                       p_desc                 =>  V_NOM_SERVICIO,    
                                       p_receipt_number       =>  NULL,     -- numero de recibo que se muestra en la forma TVAAREV
                                       p_tran_number_paid     =>  NULL,     -- numero de transaccion que ser� pagara
                                       p_crossref_pidm        =>  NULL,    
                                       p_crossref_number      =>  NULL,    
                                       p_crossref_detail_code =>  NULL,     
                                       p_srce_code            =>  'T',    -- defaul 'T', pero p_processfeeassessment crea un 'R' Modulo registro  
                                       p_acct_feed_ind        =>  'Y',
                                       p_session_number       =>  0,    
                                       p_cshr_end_date        =>  NULL,    
                                       p_crn                  =>  NULL,
                                       p_crossref_srce_code   =>  NULL,
                                       p_loc_mdt              =>  NULL,
                                       p_loc_mdt_seq          =>  NULL,    
                                       p_rate                 =>  NULL,    
                                       p_units                =>  NULL,     
                                       p_document_number      =>  NULL,    
                                       p_trans_date           =>  NULL,    
                                       p_payment_id           =>  NULL,    
                                       p_invoice_number       =>  NULL,    
                                       p_statement_date       =>  NULL,    
                                       p_inv_number_paid      =>  NULL,    
                                       p_curr_code            =>  NULL,    
                                       p_exchange_diff        =>  NULL,    
                                       p_foreign_amount       =>  NULL,    
                                       p_late_dcat_code       =>  NULL,    
                                       p_atyp_code            =>  NULL,    
                                       p_atyp_seqno           =>  NULL,    
                                       p_card_type_vr         =>  NULL,    
                                       p_card_exp_date_vr     =>  NULL,     
                                       p_card_auth_number_vr  =>  NULL,    
                                       p_crossref_dcat_code   =>  NULL,    
                                       p_orig_chg_ind         =>  NULL,    
                                       p_ccrd_code            =>  NULL,    
                                       p_merchant_id          =>  NULL,    
                                       p_data_origin          =>  'WorkFlow',    
                                       p_override_hold        =>  'N',     
                                       p_tran_number_out      =>  P_TRAN_NUMBER, 
                                       p_rowid_out            =>  V_ROWID);
      
      END IF;


      COMMIT;
EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CODDETALLE;