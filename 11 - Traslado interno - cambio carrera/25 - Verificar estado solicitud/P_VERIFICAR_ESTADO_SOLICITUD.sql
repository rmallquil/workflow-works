/*
SET SERVEROUTPUT ON
DECLARE P_STATUS_SOL VARCHAR2(4000);
 P_MESSAGE VARCHAR2(4000);
BEGIN
    P_VERIFICAR_ESTADO_SOLICITUD(123287, P_STATUS_SOL, P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_STATUS_SOL);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
END;
*/

CREATE OR REPLACE PROCEDURE P_VERIFICAR_ESTADO_SOLICITUD (
        P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_STATUS_SOL          OUT VARCHAR2,
        P_MESSAGE             OUT VARCHAR2
)
AS
        V_SOL                     NUMBER := 0;
        V_ESTADO                  SVVSRVS.SVVSRVS_CODE%TYPE;
        V_PRIORIDAD               SVRSVPR.SVRSVPR_RSRV_SEQ_NO%TYPE;
        V_ERROR                   EXCEPTION;
BEGIN

    --################################################################################################
    --VERIFICAR EXISTENCIA DE SOLICITUD
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOL
    FROM SVRSVPR 
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
    
    IF V_SOL > 0 THEN
        -- Obtener estado de solicitud
        SELECT SVRSVPR_SRVS_CODE
        INTO V_ESTADO
        FROM SVRSVPR 
        WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
        
        IF V_ESTADO = 'AN' THEN
            P_STATUS_SOL := 'FALSE';
        ELSE
            P_STATUS_SOL := 'TRUE';
        END IF;
    ELSE
        RAISE V_ERROR;
    END IF;
    
EXCEPTION
    WHEN V_ERROR THEN
        P_MESSAGE:='El número de la solicitud no existe.';
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| P_MESSAGE);
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_ESTADO_SOLICITUD;