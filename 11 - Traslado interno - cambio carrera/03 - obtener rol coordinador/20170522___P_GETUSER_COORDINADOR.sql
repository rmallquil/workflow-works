/*
drop procedure p_get_usuario_regaca;
GRANT EXECUTE ON p_get_usuario_regaca TO wfobjects;
GRANT EXECUTE ON p_get_usuario_regaca TO wfauto;

set serveroutput on
DECLARE
p_usuario_regaca          VARCHAR2(255);
P_ROL_SEDE                VARCHAR2(255);
begin
  P_GETUSER_COORDINADOR('S01','coordinador','110',P_ROL_SEDE);
  DBMS_OUTPUT.PUT_LINE(P_ROL_SEDE);
end;
*/


create or replace PROCEDURE P_GETUSER_COORDINADOR (
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
      P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
      P_PROGRAM_NEW         IN SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_ROL_COORDINADOR     OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GETUSER_COORDINADOR
  FECHA     : 22/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene el usuario WF del cordinador,
              Se establecio que el usuario se compone por "COORDINADOR + PROGRAM_CODE + CAMPUS"
                  - Ejm: COORDINADOR110S01 
  =================================================================================================================== */
AS
            -- @PARAMETERS
      V_ROLE_ID                 NUMBER;
      V_ORG_ID                  NUMBER;
      V_INDICADOR               NUMBER;
      V_EXEPTION                EXCEPTION;
      
      V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
BEGIN 
---.
      P_ROL_COORDINADOR := P_ROL ||  P_PROGRAM_NEW || P_CAMP_CODE;
      
      -- Obtener el ROL_ID 
      SELECT ID INTO V_ROLE_ID FROM WORKFLOW.ROLE WHERE NAME = P_ROL_COORDINADOR;
      
      -- Obtener el ORG_ID 
      SELECT ID INTO V_ORG_ID FROM WORKFLOW.ORGANIZATION WHERE NAME = 'Root';
      
      --Validar que exista el ROL
      SELECT COUNT(*) INTO V_INDICADOR FROM WORKFLOW.ROLE_ASSIGNMENT 
      WHERE ORG_ID = V_ORG_ID AND ROLE_ID = V_ROLE_ID;
      IF V_INDICADOR <> 1 THEN
          RAISE V_EXEPTION;
      END if;
      
EXCEPTION
  WHEN V_EXEPTION THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| 'No se encontro el ROL.' || P_ROL_COORDINADOR);
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GETUSER_COORDINADOR;
