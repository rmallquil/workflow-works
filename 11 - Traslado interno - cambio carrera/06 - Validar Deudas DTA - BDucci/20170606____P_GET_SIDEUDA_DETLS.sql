/*
SET SERVEROUTPUT ON
DECLARE P_DEUDA_CODE NUMBER;
BEGIN
        P_GET_SIDEUDA_DETLS(241106,'74288044','S01','201710','C01','C02',P_DEUDA_CODE);
        DBMS_OUTPUT.PUT_LINE(P_DEUDA_CODE);
END;
*/


create or replace PROCEDURE P_GET_SIDEUDA_DETLS (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE, --------------- APEC
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, ------------- APEC
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE, ------------- APEC
      P_DETL_1CODE          IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE, ------ APEC
      P_DETL_2CODE          IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE, ------ APEC
      P_DEUDA_CODE          OUT NUMBER
)
/* ===================================================================================================================
  NOMBRE    : P_GET_SIDEUDA
  FECHA     : 24/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Verificar en base a un PIDM y el Num. Transaccion verifique la existencia de deuda. divisa PEN.
              - APEC verificar si el PIDM tiene deuda en algun concepto especifico para la seccion "DTA".

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */              
AS
      PRAGMA AUTONOMOUS_TRANSACTION;
      V_DEUDA_INDICADOR       NUMBER;
      -- APEC PARAMS
      V_APEC_SECC_SEDE        VARCHAR2(9);
      V_APEC_TERM             VARCHAR2(10);
      V_APEC_IDSECCIONC       VARCHAR2(15);
      V_APEC_IDSECCIONC_INIC  DATE;
      V_APEC_MOUNT1           NUMBER;      
      V_APEC_MOUNT2           NUMBER;
      
BEGIN
--
    
    --#################################----- APEC -----#######################################
    --########################################################################################
    -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
    IF PKG_GLOBAL.GET_VAL = 0 THEN -- PRIORIDAD 2 ==> GENERAR DEUDA
          
          SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
          
          --**********************************************************************************************
          -- GET SECCIONC
          SELECT  "IDsede", "IDSeccionC" , "FecInic" INTO V_APEC_SECC_SEDE, V_APEC_IDSECCIONC, V_APEC_IDSECCIONC_INIC
          FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia"='UCCI'
          AND "IDPerAcad" = V_APEC_TERM
          AND "IDSeccionC" = 'DTA' -- 'DTA' SeccionC Especial para traslado interno
          AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
          AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
          AND "IDSeccionC" <> '15NEX1A';

          -- GET 1° DEUDA(s)
          SELECT "Deuda" INTO V_APEC_MOUNT1 FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia" = 'UCCI'
          AND "IDAlumno"    = P_ID_ALUMNO
          AND "IDSede"      = V_APEC_SECC_SEDE
          AND "IDPerAcad"   = V_APEC_TERM
          AND "IDSeccionC"  = V_APEC_IDSECCIONC
          AND "FecInic"     = V_APEC_IDSECCIONC_INIC
          AND "IDConcepto"  = P_DETL_1CODE;
          
          -- GET 2° DEUDA(s) 
          SELECT "Deuda" INTO V_APEC_MOUNT2 FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia" = 'UCCI'
          AND "IDAlumno"    = P_ID_ALUMNO
          AND "IDSede"      = V_APEC_SECC_SEDE
          AND "IDPerAcad"   = V_APEC_TERM
          AND "IDSeccionC"  = V_APEC_IDSECCIONC
          AND "FecInic"     = V_APEC_IDSECCIONC_INIC
          AND "IDConcepto"  = P_DETL_2CODE;
            
          COMMIT;
    END IF;
    
    /*
      - CODIGOS DEUDA
        3 : Tiene deuda en los dos conceptos
        1 : Tiene deuda solo en el primer concepto
        2 : Tiene deuda solo en el segundo concepto
    */
    P_DEUDA_CODE := CASE  WHEN V_APEC_MOUNT1 > 0 AND V_APEC_MOUNT2 > 0 THEN 3
                          WHEN V_APEC_MOUNT1 > 0 THEN 1 
                          WHEN V_APEC_MOUNT2 > 0 THEN 2 ELSE 0 END;
                    
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_SIDEUDA_DETLS;
