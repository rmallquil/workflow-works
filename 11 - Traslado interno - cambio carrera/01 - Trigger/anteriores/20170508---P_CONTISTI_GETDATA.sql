SET SERVEROUTPUT ON
DECLARE P_DEPT_CODE            STVDEPT.STVDEPT_CODE%TYPE;
        P_CAMP_CODE            STVCAMP.STVCAMP_CODE%TYPE;
        P_CAMP_DESC            STVCAMP.STVCAMP_DESC%TYPE;
        P_TERM_CODE            STVTERM.STVTERM_CODE%TYPE;
BEGIN
    P_CONTISTI_GETDATA(557036,'03/05/2017',P_DEPT_CODE,P_CAMP_CODE,P_CAMP_DESC,P_TERM_CODE); -- 43605864
    DBMS_OUTPUT.PUT_LINE('::::::::: ' || P_DEPT_CODE|| ' *** ' || P_CAMP_CODE|| ' *** ' || P_CAMP_DESC|| ' *** ' || P_TERM_CODE);
    
    P_CONTISTI_GETDATA(386582,'03/05/2017',P_DEPT_CODE,P_CAMP_CODE,P_CAMP_DESC,P_TERM_CODE);
    DBMS_OUTPUT.PUT_LINE('::::::::: ' || P_DEPT_CODE|| ' *** ' || P_CAMP_CODE|| ' *** ' || P_CAMP_DESC|| ' *** ' || P_TERM_CODE);
    
    P_CONTISTI_GETDATA(234562,'03/05/2017',P_DEPT_CODE,P_CAMP_CODE,P_CAMP_DESC,P_TERM_CODE);
    DBMS_OUTPUT.PUT_LINE('::::::::: ' || P_DEPT_CODE|| ' *** ' || P_CAMP_CODE|| ' *** ' || P_CAMP_DESC|| ' *** ' || P_TERM_CODE);
END;

CREATE OR REPLACE PROCEDURE P_CONTISTI_GETDATA( 
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
)
/* ===================================================================================================================
  NOMBRE    : P_CONTISTI_GETDATA
  FECHA     : 08/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtener los datos dept, camp, nombre camp y periodo de un PIDM

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
      
      P_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo

      -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
      CURSOR C_CAMPDEPT IS
      SELECT    SORLCUR_CAMP_CODE,
                SORLFOS_DEPT_CODE,
                STVCAMP_DESC
      FROM (
              SELECT    SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, STVCAMP_DESC
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
              INNER JOIN STVCAMP
                    ON SORLCUR_CAMP_CODE = STVCAMP_CODE
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
              ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM = 1;

      -- Calculando parte PERIODO (sub PTRM)
      CURSOR C_SFRRSTS_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;

      -- CURSOR GET TERM
      CURSOR C_STUDN_TERM IS
      SELECT SFRRSTS_TERM_CODE FROM (
          -- Forma SFARSTS  ---> fechas para las partes de periodo
          SELECT DISTINCT SFRRSTS_TERM_CODE 
          FROM SFRRSTS
          WHERE SFRRSTS_PTRM_CODE IN (P_SUB_PTRM||'1' , (P_SUB_PTRM || CASE WHEN (P_DEPT_CODE ='UVIR' OR P_DEPT_CODE ='UPGT') THEN '2' ELSE '-' END) ) 
          AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
          AND TO_DATE(TO_CHAR(P_RECEPTION_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SFRRSTS_END_DATE
          ORDER BY SFRRSTS_TERM_CODE DESC
      ) WHERE ROWNUM <= 1;


BEGIN

      P_DEPT_CODE  := '-';
      P_CAMP_CODE  := '-';
      P_CAMP_DESC  := '-';
      P_TERM_CODE  := '-';
      

      -- >> GET DEPARTAMENTO y CAMPUS --
      OPEN C_CAMPDEPT;
      LOOP
        FETCH C_CAMPDEPT INTO P_CAMP_CODE,P_DEPT_CODE,P_CAMP_DESC ;
        EXIT WHEN C_CAMPDEPT%NOTFOUND;
      END LOOP;
      CLOSE C_CAMPDEPT;


      -- >> calculando PARTE PERIODO  --
      OPEN C_SFRRSTS_PTRM;
      LOOP
        FETCH C_SFRRSTS_PTRM INTO P_SUB_PTRM;
        EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
      END LOOP;
      CLOSE C_SFRRSTS_PTRM;


      -- PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
      -- >> GET PERIODO  (TERM)-- 
      OPEN C_STUDN_TERM;
      LOOP
        FETCH C_STUDN_TERM INTO P_TERM_CODE;
        EXIT WHEN C_STUDN_TERM%NOTFOUND;
      END LOOP;
      CLOSE C_STUDN_TERM;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CONTISTI_GETDATA;
