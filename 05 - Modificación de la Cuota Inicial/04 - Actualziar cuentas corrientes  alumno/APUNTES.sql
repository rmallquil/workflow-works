SELECT * FROM SPRIDEN WHERE SPRIDEN_PIDM = 428093; -- 71569303
SELECT * FROM SGBSTDN WHERE SGBSTDN_PIDM = '428093';
SELECT * FROM SORLCUR WHERE SORLCUR_PIDM = '428093';
SELECT * FROM SORLFOS WHERE SORLFOS_PIDM = '428093';

SELECT * FROM TBRACCD WHERE TBRACCD_PIDM = '428093';
SELECT * FROM TBRAPPL WHERE TBRAPPL_PIDM = '428093';
SELECT * FROM SGRSTSP WHERE SGRSTSP_PIDM = '428093';
SELECT * FROM SFRENSP WHERE SFRENSP_PIDM = '428093';
SELECT * FROM SFBETRM WHERE SFBETRM_PIDM = '428093';


SELECT * FROM sovlcur WHERE sovlcur_PIDM = 29896;

--- eliminar registros para pruebas
DELETE FROM SGBSTDN WHERE SGBSTDN_PIDM = '428093' AND SGBSTDN_TERM_CODE_EFF = '201620';COMMIT;
DELETE FROM SORLFOS WHERE SORLFOS_PIDM = '428093' AND SORLFOS_LCUR_SEQNO = '3';COMMIT;
DELETE FROM SORLCUR WHERE SORLCUR_PIDM = '428093' AND SORLCUR_SEQNO = '3';COMMIT;
UPDATE SORLCUR SET SORLCUR_TERM_CODE_END = NULL,SORLCUR_CURRENT_CDE='Y'  WHERE SORLCUR_PIDM = '428093' AND SORLCUR_LMOD_CODE = 'LEARNER'; COMMIT;
DELETE FROM SGRSTSP WHERE SGRSTSP_PIDM = '428093' AND SGRSTSP_TERM_CODE_EFF = '201620';COMMIT;
DELETE FROM SFRENSP WHERE SFRENSP_PIDM = '428093' AND SFRENSP_TERM_CODE = '201620';COMMIT;
DELETE FROM SFBETRM WHERE SFBETRM_PIDM = '428093' AND SFBETRM_TERM_CODE = '201620';COMMIT;
--UPDATE SGBSTDN SET SGBSTDN_STST_CODE = 'AS' WHERE SGBSTDN_PIDM = '428093' AND SGBSTDN_TERM_CODE_EFF = '201610';COMMIT;
DELETE FROM TBRACCD WHERE TBRACCD_PIDM = '428093';COMMIT;
DELETE FROM TBRAPPL WHERE TBRAPPL_PIDM = '428093';COMMIT;

  
  -- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  SET SERVEROUTPUT ON
  DECLARE 
        P_ID_ALUMNO               SPRIDEN.SPRIDEN_PIDM%TYPE := '428093';
        P_PERIODO                 SFBETRM.SFBETRM_TERM_CODE%TYPE := '201620';
        P_ERROR                   VARCHAR2(100);
        P_DNI_ALUMNO              SPRIDEN.SPRIDEN_ID%TYPE;
        
        -- ADD SGBSTDN - EN CASO NO EXISTA PARA EL PERIODO INDICADO
        CURSOR C_SGBSTDN IS
        SELECT * FROM (
            SELECT *
            FROM SGBSTDN WHERE SGBSTDN_PIDM = P_ID_ALUMNO
            ORDER BY SGBSTDN_TERM_CODE_EFF DESC
         )WHERE ROWNUM = 1
         AND NOT EXISTS (
            SELECT *
            FROM SGBSTDN WHERE SGBSTDN_PIDM = P_ID_ALUMNO
            AND SGBSTDN_TERM_CODE_EFF = P_PERIODO
         );
        
        -- AGREGAR NUEVO REGISTRO
        CURSOR C_SORLCUR IS
        SELECT * FROM (
            SELECT * FROM SORLCUR 
            WHERE SORLCUR_PIDM = P_ID_ALUMNO
            AND SORLCUR_LMOD_CODE = 'LEARNER'
            AND SORLCUR_CURRENT_CDE = 'Y' --------------------- CURRENT CURRICULUM
            ORDER BY SORLCUR_SEQNO DESC
        )WHERE ROWNUM = 1;
        
  --      -- AGREGAR NUEVO REGISTRO
  --      CURSOR C_SORLFOS IS
  --      SELECT * FROM (
  --          SELECT * FROM SORLFOS 
  --          WHERE SORLFOS_PIDM = P_ID_ALUMNO
  --          AND SORLFOS_CSTS_CODE = 'INPROGRESS'
  --          AND SORLFOS_CACT_CODE = 'ACTIVE'
  --          ORDER BY SORLFOS_LCUR_SEQNO DESC, SORLFOS_SEQNO ASC
  --      )WHERE ROWNUM = 1;
        
        V_SGBSTDN_REC         SGBSTDN%ROWTYPE;
        V_SORLCUR_REC         SORLCUR%ROWTYPE;
        V_SORLFOS_REC         SORLFOS%ROWTYPE;
        
        P_SORLCUR_SEQNO_OLD   SORLCUR.SORLCUR_SEQNO%TYPE;
        P_SORLCUR_SEQNO_NEW   SORLCUR.SORLCUR_SEQNO%TYPE;
        P_SORLCUR_RATE_CODE   SORLCUR.SORLCUR_RATE_CODE%TYPE; -- TARIFA
        
        SAVE_ACT_DATE_OUT       VARCHAR2(100);
        RETURN_STATUS_IN_OUT    NUMBER;
  BEGIN 
        
        -- GET DNI alumno
        SELECT SPRIDEN_ID INTO P_DNI_ALUMNO FROM SPRIDEN WHERE SPRIDEN_PIDM = P_ID_ALUMNO;
        
        -- #######################################################################
        -- INSERT  ------> SGBSTDN -
        OPEN C_SGBSTDN;
        LOOP
            FETCH C_SGBSTDN INTO V_SGBSTDN_REC;
            EXIT WHEN C_SGBSTDN%NOTFOUND;
            
              INSERT INTO SGBSTDN (
                        SGBSTDN_PIDM,                 SGBSTDN_TERM_CODE_EFF,          SGBSTDN_STST_CODE,
                        SGBSTDN_LEVL_CODE,            SGBSTDN_STYP_CODE,              SGBSTDN_TERM_CODE_MATRIC,
                        SGBSTDN_TERM_CODE_ADMIT,      SGBSTDN_EXP_GRAD_DATE,          SGBSTDN_CAMP_CODE,
                        SGBSTDN_FULL_PART_IND,        SGBSTDN_SESS_CODE,              SGBSTDN_RESD_CODE,
                        SGBSTDN_COLL_CODE_1,          SGBSTDN_DEGC_CODE_1,            SGBSTDN_MAJR_CODE_1,
                        SGBSTDN_MAJR_CODE_MINR_1,     SGBSTDN_MAJR_CODE_MINR_1_2,     SGBSTDN_MAJR_CODE_CONC_1,
                        SGBSTDN_MAJR_CODE_CONC_1_2,   SGBSTDN_MAJR_CODE_CONC_1_3,     SGBSTDN_COLL_CODE_2,
                        SGBSTDN_DEGC_CODE_2,          SGBSTDN_MAJR_CODE_2,            SGBSTDN_MAJR_CODE_MINR_2,
                        SGBSTDN_MAJR_CODE_MINR_2_2,   SGBSTDN_MAJR_CODE_CONC_2,       SGBSTDN_MAJR_CODE_CONC_2_2,
                        SGBSTDN_MAJR_CODE_CONC_2_3,   SGBSTDN_ORSN_CODE,              SGBSTDN_PRAC_CODE,
                        SGBSTDN_ADVR_PIDM,            SGBSTDN_GRAD_CREDIT_APPR_IND,   SGBSTDN_CAPL_CODE,
                        SGBSTDN_LEAV_CODE,            SGBSTDN_LEAV_FROM_DATE,         SGBSTDN_LEAV_TO_DATE,
                        SGBSTDN_ASTD_CODE,            SGBSTDN_TERM_CODE_ASTD,         SGBSTDN_RATE_CODE,
                        SGBSTDN_ACTIVITY_DATE,        SGBSTDN_MAJR_CODE_1_2,          SGBSTDN_MAJR_CODE_2_2,
                        SGBSTDN_EDLV_CODE,            SGBSTDN_INCM_CODE,              SGBSTDN_ADMT_CODE,
                        SGBSTDN_EMEX_CODE,            SGBSTDN_APRN_CODE,              SGBSTDN_TRCN_CODE,
                        SGBSTDN_GAIN_CODE,            SGBSTDN_VOED_CODE,              SGBSTDN_BLCK_CODE,
                        SGBSTDN_TERM_CODE_GRAD,       SGBSTDN_ACYR_CODE,              SGBSTDN_DEPT_CODE,
                        SGBSTDN_SITE_CODE,            SGBSTDN_DEPT_CODE_2,            SGBSTDN_EGOL_CODE,
                        SGBSTDN_DEGC_CODE_DUAL,       SGBSTDN_LEVL_CODE_DUAL,         SGBSTDN_DEPT_CODE_DUAL,
                        SGBSTDN_COLL_CODE_DUAL,       SGBSTDN_MAJR_CODE_DUAL,         SGBSTDN_BSKL_CODE,
                        SGBSTDN_PRIM_ROLL_IND,        SGBSTDN_PROGRAM_1,              SGBSTDN_TERM_CODE_CTLG_1,
                        SGBSTDN_DEPT_CODE_1_2,        SGBSTDN_MAJR_CODE_CONC_121,     SGBSTDN_MAJR_CODE_CONC_122,
                        SGBSTDN_MAJR_CODE_CONC_123,   SGBSTDN_SECD_ROLL_IND,          SGBSTDN_TERM_CODE_ADMIT_2,
                        SGBSTDN_ADMT_CODE_2,          SGBSTDN_PROGRAM_2,              SGBSTDN_TERM_CODE_CTLG_2,
                        SGBSTDN_LEVL_CODE_2,          SGBSTDN_CAMP_CODE_2,            SGBSTDN_DEPT_CODE_2_2,
                        SGBSTDN_MAJR_CODE_CONC_221,   SGBSTDN_MAJR_CODE_CONC_222,     SGBSTDN_MAJR_CODE_CONC_223,
                        SGBSTDN_CURR_RULE_1,          SGBSTDN_CMJR_RULE_1_1,          SGBSTDN_CCON_RULE_11_1,
                        SGBSTDN_CCON_RULE_11_2,       SGBSTDN_CCON_RULE_11_3,         SGBSTDN_CMJR_RULE_1_2,
                        SGBSTDN_CCON_RULE_12_1,       SGBSTDN_CCON_RULE_12_2,         SGBSTDN_CCON_RULE_12_3,
                        SGBSTDN_CMNR_RULE_1_1,        SGBSTDN_CMNR_RULE_1_2,          SGBSTDN_CURR_RULE_2,
                        SGBSTDN_CMJR_RULE_2_1,        SGBSTDN_CCON_RULE_21_1,         SGBSTDN_CCON_RULE_21_2,
                        SGBSTDN_CCON_RULE_21_3,       SGBSTDN_CMJR_RULE_2_2,          SGBSTDN_CCON_RULE_22_1,
                        SGBSTDN_CCON_RULE_22_2,       SGBSTDN_CCON_RULE_22_3,         SGBSTDN_CMNR_RULE_2_1,
                        SGBSTDN_CMNR_RULE_2_2,        SGBSTDN_PREV_CODE,              SGBSTDN_TERM_CODE_PREV,
                        SGBSTDN_CAST_CODE,            SGBSTDN_TERM_CODE_CAST,         SGBSTDN_DATA_ORIGIN,
                        SGBSTDN_USER_ID,              SGBSTDN_SCPC_CODE ) 
              VALUES (  V_SGBSTDN_REC.SGBSTDN_PIDM,                 P_PERIODO,                                    V_SGBSTDN_REC.SGBSTDN_STST_CODE,
                        V_SGBSTDN_REC.SGBSTDN_LEVL_CODE,            V_SGBSTDN_REC.SGBSTDN_STYP_CODE,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_MATRIC,
                        V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT,      V_SGBSTDN_REC.SGBSTDN_EXP_GRAD_DATE,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE,
                        V_SGBSTDN_REC.SGBSTDN_FULL_PART_IND,        V_SGBSTDN_REC.SGBSTDN_SESS_CODE,              V_SGBSTDN_REC.SGBSTDN_RESD_CODE,
                        V_SGBSTDN_REC.SGBSTDN_COLL_CODE_1,          V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_1,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1,
                        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1_2,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1,
                        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_3,     V_SGBSTDN_REC.SGBSTDN_COLL_CODE_2,
                        V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2,
                        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_2,
                        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_3,   V_SGBSTDN_REC.SGBSTDN_ORSN_CODE,              V_SGBSTDN_REC.SGBSTDN_PRAC_CODE,
                        V_SGBSTDN_REC.SGBSTDN_ADVR_PIDM,            V_SGBSTDN_REC.SGBSTDN_GRAD_CREDIT_APPR_IND,   V_SGBSTDN_REC.SGBSTDN_CAPL_CODE,
                        V_SGBSTDN_REC.SGBSTDN_LEAV_CODE,            V_SGBSTDN_REC.SGBSTDN_LEAV_FROM_DATE,         V_SGBSTDN_REC.SGBSTDN_LEAV_TO_DATE,
                        V_SGBSTDN_REC.SGBSTDN_ASTD_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ASTD,         V_SGBSTDN_REC.SGBSTDN_RATE_CODE,
                        SYSDATE,                                    V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2_2,
                        V_SGBSTDN_REC.SGBSTDN_EDLV_CODE,            V_SGBSTDN_REC.SGBSTDN_INCM_CODE,              V_SGBSTDN_REC.SGBSTDN_ADMT_CODE,
                        V_SGBSTDN_REC.SGBSTDN_EMEX_CODE,            V_SGBSTDN_REC.SGBSTDN_APRN_CODE,              V_SGBSTDN_REC.SGBSTDN_TRCN_CODE,
                        V_SGBSTDN_REC.SGBSTDN_GAIN_CODE,            V_SGBSTDN_REC.SGBSTDN_VOED_CODE,              V_SGBSTDN_REC.SGBSTDN_BLCK_CODE,
                        V_SGBSTDN_REC.SGBSTDN_TERM_CODE_GRAD,       V_SGBSTDN_REC.SGBSTDN_ACYR_CODE,              V_SGBSTDN_REC.SGBSTDN_DEPT_CODE,
                        V_SGBSTDN_REC.SGBSTDN_SITE_CODE,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2,            V_SGBSTDN_REC.SGBSTDN_EGOL_CODE,
                        V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_DUAL,
                        V_SGBSTDN_REC.SGBSTDN_COLL_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_BSKL_CODE,
                        V_SGBSTDN_REC.SGBSTDN_PRIM_ROLL_IND,        V_SGBSTDN_REC.SGBSTDN_PROGRAM_1,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CTLG_1,
                        V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_1_2,        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_121,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_122,
                        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_123,   V_SGBSTDN_REC.SGBSTDN_SECD_ROLL_IND,          V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT_2,
                        V_SGBSTDN_REC.SGBSTDN_ADMT_CODE_2,          V_SGBSTDN_REC.SGBSTDN_PROGRAM_2,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CTLG_2,
                        V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_2,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE_2,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2_2,
                        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_221,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_222,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_223,
                        V_SGBSTDN_REC.SGBSTDN_CURR_RULE_1,          V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_1,          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_1,
                        V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_3,         V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_2,
                        V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_1,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_2,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_3,
                        V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_1,        V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_2,          V_SGBSTDN_REC.SGBSTDN_CURR_RULE_2,
                        V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_1,        V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_1,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_2,
                        V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_3,       V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_2,          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_1,
                        V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_3,         V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_1,
                        V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_2,        V_SGBSTDN_REC.SGBSTDN_PREV_CODE,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_PREV,
                        V_SGBSTDN_REC.SGBSTDN_CAST_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CAST,         'WorkFlow',
                        USER,                                       V_SGBSTDN_REC.SGBSTDN_SCPC_CODE );
              
              DBMS_OUTPUT.put_line(V_SGBSTDN_REC.SGBSTDN_PIDM || ' ---- ' || V_SGBSTDN_REC.SGBSTDN_TERM_CODE_EFF || ' -- PERDIODO: ' || P_PERIODO);
        END LOOP;
        CLOSE C_SGBSTDN;
        
        -- #######################################################################
        -- INSERT  ------> SORLCUR -
        OPEN C_SORLCUR;
        LOOP
            FETCH C_SORLCUR INTO V_SORLCUR_REC;
            EXIT WHEN C_SORLCUR%NOTFOUND;
            
            ---- GET NUEVA TARIFA Y VALIDANDO
            SELECT STVRATE_CODE  INTO P_SORLCUR_RATE_CODE  FROM STVRATE
            WHERE STVRATE_CODE = V_SORLCUR_REC.SORLCUR_RATE_CODE || '-M'; 
            
            -- GET ANTERIOR Y NUEVO "SEQNO"
            SELECT MAX(SORLCUR_SEQNO), MAX(SORLCUR_SEQNO) + 1 INTO P_SORLCUR_SEQNO_OLD, P_SORLCUR_SEQNO_NEW FROM SORLCUR
            WHERE SORLCUR_PIDM = P_ID_ALUMNO;
            
            INSERT INTO SORLCUR (
                        SORLCUR_PIDM,             SORLCUR_SEQNO,                SORLCUR_LMOD_CODE,
                        SORLCUR_TERM_CODE,        SORLCUR_KEY_SEQNO,            SORLCUR_PRIORITY_NO,
                        SORLCUR_ROLL_IND,         SORLCUR_CACT_CODE,            SORLCUR_USER_ID,
                        SORLCUR_DATA_ORIGIN,      SORLCUR_ACTIVITY_DATE,        SORLCUR_LEVL_CODE,
                        SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,            SORLCUR_TERM_CODE_CTLG,
                        SORLCUR_TERM_CODE_END,    SORLCUR_TERM_CODE_MATRIC,     SORLCUR_TERM_CODE_ADMIT,
                        SORLCUR_ADMT_CODE,        SORLCUR_CAMP_CODE,            SORLCUR_PROGRAM,
                        SORLCUR_START_DATE,       SORLCUR_END_DATE,             SORLCUR_CURR_RULE,
                        SORLCUR_ROLLED_SEQNO,     SORLCUR_STYP_CODE,            SORLCUR_RATE_CODE,
                        SORLCUR_LEAV_CODE,        SORLCUR_LEAV_FROM_DATE,       SORLCUR_LEAV_TO_DATE,
                        SORLCUR_EXP_GRAD_DATE,    SORLCUR_TERM_CODE_GRAD,       SORLCUR_ACYR_CODE,
                        SORLCUR_SITE_CODE,        SORLCUR_APPL_SEQNO,           SORLCUR_APPL_KEY_SEQNO,
                        SORLCUR_USER_ID_UPDATE,   SORLCUR_ACTIVITY_DATE_UPDATE, SORLCUR_GAPP_SEQNO,
                        SORLCUR_CURRENT_CDE ) 
            VALUES (    V_SORLCUR_REC.SORLCUR_PIDM,             P_SORLCUR_SEQNO_NEW,                            V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                        P_PERIODO,                              V_SORLCUR_REC.SORLCUR_KEY_SEQNO,            V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                        V_SORLCUR_REC.SORLCUR_ROLL_IND,         V_SORLCUR_REC.SORLCUR_CACT_CODE,            USER,
                        'WorkFlow',                             SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                        V_SORLCUR_REC.SORLCUR_COLL_CODE,        V_SORLCUR_REC.SORLCUR_DEGC_CODE,            V_SORLCUR_REC.SORLCUR_TERM_CODE_CTLG,
                        NULL,                                   V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                        V_SORLCUR_REC.SORLCUR_ADMT_CODE,        V_SORLCUR_REC.SORLCUR_CAMP_CODE,            V_SORLCUR_REC.SORLCUR_PROGRAM,
                        V_SORLCUR_REC.SORLCUR_START_DATE,       V_SORLCUR_REC.SORLCUR_END_DATE,             V_SORLCUR_REC.SORLCUR_CURR_RULE,
                        V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,     V_SORLCUR_REC.SORLCUR_STYP_CODE,            (V_SORLCUR_REC.SORLCUR_RATE_CODE || '-M'),
                        V_SORLCUR_REC.SORLCUR_LEAV_CODE,        V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                        V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,    V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                        V_SORLCUR_REC.SORLCUR_SITE_CODE,        V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                        USER,                                   SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                        V_SORLCUR_REC.SORLCUR_CURRENT_CDE );
            
              -- UPDATE TERM_CODE_END(vigencia curriculum) Y SORLCUR_CURRENT_CDE(curriculum activo)
              UPDATE SORLCUR 
              SET   SORLCUR_TERM_CODE_END = P_PERIODO, 
                    SORLCUR_ACTIVITY_DATE_UPDATE = SYSDATE,
                    SORLCUR_CURRENT_CDE = NULL
              WHERE   SORLCUR_PIDM = P_ID_ALUMNO 
              AND     SORLCUR_LMOD_CODE = 'LEARNER'
              AND     SORLCUR_SEQNO = V_SORLCUR_REC.SORLCUR_SEQNO;
            
            DBMS_OUTPUT.put_line(V_SORLCUR_REC.SORLCUR_PIDM || ' ---- ' || V_SORLCUR_REC.SORLCUR_SEQNO);
        END LOOP;
        CLOSE C_SORLCUR;
        
        
        -- #######################################################################
        -- INSERT --- SORLFOS -
        INSERT INTO SORLFOS (
              SORLFOS_PIDM,             SORLFOS_LCUR_SEQNO,         SORLFOS_SEQNO,
              SORLFOS_LFST_CODE,        SORLFOS_TERM_CODE,          SORLFOS_PRIORITY_NO,
              SORLFOS_CSTS_CODE,        SORLFOS_CACT_CODE,          SORLFOS_DATA_ORIGIN,
              SORLFOS_USER_ID,          SORLFOS_ACTIVITY_DATE,      SORLFOS_MAJR_CODE,
              SORLFOS_TERM_CODE_CTLG,   SORLFOS_TERM_CODE_END,      SORLFOS_DEPT_CODE,
              SORLFOS_MAJR_CODE_ATTACH, SORLFOS_LFOS_RULE,          SORLFOS_CONC_ATTACH_RULE,
              SORLFOS_START_DATE,       SORLFOS_END_DATE,           SORLFOS_TMST_CODE,
              SORLFOS_ROLLED_SEQNO,     SORLFOS_USER_ID_UPDATE,     SORLFOS_ACTIVITY_DATE_UPDATE,
              SORLFOS_CURRENT_CDE) 
        SELECT 
              V_SORLFOS_REC.SORLFOS_PIDM,             P_SORLCUR_SEQNO_NEW,                      1,/*V_SORLFOS_REC.SORLFOS_SEQNO*/
              V_SORLFOS_REC.SORLFOS_LFST_CODE,        V_SORLFOS_REC.SORLFOS_TERM_CODE,          V_SORLFOS_REC.SORLFOS_PRIORITY_NO,
              V_SORLFOS_REC.SORLFOS_CSTS_CODE,        V_SORLFOS_REC.SORLFOS_CACT_CODE,          'WorkFlow',
              USER,                                   SYSDATE,                                  V_SORLFOS_REC.SORLFOS_MAJR_CODE,
              V_SORLFOS_REC.SORLFOS_TERM_CODE_CTLG,   V_SORLFOS_REC.SORLFOS_TERM_CODE_END,      V_SORLFOS_REC.SORLFOS_DEPT_CODE,
              V_SORLFOS_REC.SORLFOS_MAJR_CODE_ATTACH, V_SORLFOS_REC.SORLFOS_LFOS_RULE,          V_SORLFOS_REC.SORLFOS_CONC_ATTACH_RULE,
              V_SORLFOS_REC.SORLFOS_START_DATE,       V_SORLFOS_REC.SORLFOS_END_DATE,           V_SORLFOS_REC.SORLFOS_TMST_CODE,
              V_SORLFOS_REC.SORLFOS_ROLLED_SEQNO,     USER,                                     SYSDATE,
              'Y'
        FROM SORLFOS V_SORLFOS_REC
        WHERE SORLFOS_PIDM = P_ID_ALUMNO
        AND SORLFOS_LCUR_SEQNO = P_SORLCUR_SEQNO_OLD;
         
        -- UPDATE SORLFOS_CURRENT_CDE(curriculum activo)
        UPDATE SORLFOS 
        SET   SORLFOS_CURRENT_CDE = NULL
        WHERE   SORLFOS_PIDM = P_ID_ALUMNO
        AND     SORLFOS_LCUR_SEQNO = P_SORLCUR_SEQNO_OLD
        AND     SORLFOS_CSTS_CODE = 'INPROGRESS';
                 
  --      OPEN C_SORLFOS;
  --      LOOP
  --          FETCH C_SORLFOS INTO V_SORLFOS_REC;
  --          EXIT WHEN C_SORLFOS%NOTFOUND;          
  --          -- DBMS_OUTPUT.put_line(V_SORLFOS_REC.SORLCUR_PIDM || ' ---- ' ||);
  --      END LOOP;
  --      CLOSE C_SORLFOS;
        
        
        -- #######################################################################
        -- Procesar DEUDA - Volviendo a estimar la deuda devido al cambio de TARIFA.
        SFKFEES.p_processfeeassessment (  P_PERIODO,
                                          P_ID_ALUMNO,
                                          SYSDATE,      -- assessment effective date(Evaluación de la fecha efectiva)
                                          SYSDATE,  -- refund by total refund date(El reembolso por fecha total del reembolso)
                                          'R',          -- use regular assessment rules(utilizar las reglas de evaluación periódica)
                                          'Y',          -- create TBRACCD records
                                          'SFAREGS',    -- where assessment originated from
                                          'Y',          -- commit changes
                                          SAVE_ACT_DATE_OUT,    -- OUT -- save_act_date
                                          'N',          -- do not ignore SFRFMAX rules
                                          RETURN_STATUS_IN_OUT );   -- OUT -- return_status
        ------------------------------------------------------------------------------  
      
        -- forma TVAAREV "Aplicar Transacciones" -- No necesariamente necesario.
        TZJAPOL.p_run_proc_tvrappl(P_DNI_ALUMNO);
      
        ------------------------------------------------------------------------------                                    
                                            
        DBMS_OUTPUT.PUT_LINE( SAVE_ACT_DATE_OUT ||'--'|| RETURN_STATUS_IN_OUT);
          
        COMMIT;
  
  EXCEPTION
  WHEN OTHERS THEN
        ROLLBACK;
        --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
        P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
  END;
  