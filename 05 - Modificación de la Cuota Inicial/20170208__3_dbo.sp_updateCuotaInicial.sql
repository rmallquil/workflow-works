/*==============================================================================================================
NOMBRE  : [dbo].[sp_updateCuotaInicial]
FECHA   : [26/01/2017]
AUTOR   : FHUAMAN
OBJETIVO:   
            *   MODIFICAR LA CUOTA INICIAL, ESTO SOLO SE EJECUTA A DEMANDA DEL ALUMNO CUANDO NO DESEA PAGAR EL COSTO DE CREDITO POR 20
				PROCEDO POR WORK FLOW
MODIFICACIONES:
NRO     FECHA           USUARIO     MODIFICACION
001		08/02/2017 		FHUAMAN		Obtiene valores para los parametros desde Banner.
002		08/02/2017		RMALLQUI	(dbo.tblCtaAlumnosWF) Registra al alumno que se modific� su cuota inicial 
================================================================================================================*/

ALTER PROCEDURE [dbo].[sp_updateCuotaInicial]
(
	@c_Divs char (4),--dependencia
	@c_Campus varchar(4), -- sede
	@c_TermCode char(6), -- periodo
	@c_ID varchar(10), -- dni
	@c_NCred int 
)
as
--declare 
--	@c_Divs char (4),--dependencia
--	@c_Campus varchar(4), -- sede
--	@c_TermCode char(6), -- periodo
--	@c_ID varchar(10), -- dni
--	@c_NCred int 
--SET @c_Divs = 'UCCI'
--SET @c_Campus = 'HYO'
--SET @c_TermCode = '2017-1'
--SET @c_ID = '71713661' -- alumno regular
--SET @c_NCred=11
--------------------------------------------------------------------
---------------- VARIABLES DEL PROCEDIMIENTO -----------------------
--------------------------------------------------------------------
DECLARE
	@c_IDusuario varchar(20),
	@c_IDInstitucion varchar(20),
	@c_FechaRegistro datetime,
	@c_MatriculaTipo varchar(2),
	@c_IDSeccionC varchar(30),
	@c_FecInic datetime,
	@c_IDPersona varchar(15),
	@c_FechaEscala datetime,
	@c_Cronograma varchar(3),
	@c_Departament varchar(6), -- idescuelaADM
	@c_Carrera varchar(6),
	@c_escala1 varchar(2),@c_escala2 varchar(2),@c_escala3 varchar(2),@c_escala4 varchar(2),@c_escala5 varchar(2),@c_escala6 varchar(2),
	@c_beca1 varchar(2),@c_beca2 varchar(2),@c_beca3 varchar(2),@c_beca4 varchar(2),@c_beca5 varchar(2),@c_beca6 varchar(2),
	@c_categoriaPens varchar(16),
	@c_CostoCred float,
	@c_cuotaInicial float,
	@c_pidm numeric(8,0),
	@c_TERM_CODE varchar(6), --- periodo academico BANNER
-------------------------- OBTENCI�N DE DATOS DE BANNER
	@V_NCRED_TOTALES numeric(7,3),
	@V_NCRED_BECA numeric(7,3),
	@V_BECA18 char(1)
---------------------------
SET @c_pidm =  (SELECT top(1) IDPersonaN FROM dbo.tblPersona where DNI= @c_ID)
SET @c_TERM_CODE = CONCAT(SUBSTRING(@c_termcode,1,4),SUBSTRING(@c_termcode,6,1),'0')

------------------ TRAENDO LOS DATOS DE BANNER
EXECUTE('BEGIN BANINST1.TZKCDAA.P_MATERIAS_CREDITOS(?,?,?,?,?,?,?,?,?); END;',
--EXECUTE('BEGIN BANINST1.PRUEBASP_MATERIAS_CREDITOS(?,?,?,?,?,?,?,?,?); END;',
@c_pidm,
@c_TERM_CODE,
@c_Divs OUTPUT,
@c_Campus OUTPUT,
@c_departament OUTPUT,
@c_carrera   OUTPUT,
@V_NCRED_TOTALES OUTPUT,
@V_NCRED_BECA OUTPUT,
@V_BECA18 OUTPUT)
AT BANNER
SET @c_Campus = (select IDSede from [dbo].[tblCampus] where Campus=@c_Campus)
SET @c_departament = CASE
					WHEN @c_departament='UREG' THEN 'ADM'
					WHEN @c_departament='UVIR' THEN 'ADV'
					WHEN @c_departament='UPGT' THEN 'ADG'
				END
-----------------------------------------------------------------
	SET @c_IDusuario='BANNER'
	SET @c_IDInstitucion ='00000000000'
	SET @c_FechaRegistro = GETDATE()
	SET @c_MatriculaTipo='2'

	select @c_FechaEscala=MAX(FechaEscala) from dbo.tblalumnoescala where idalumno = @c_ID and IDPeracad = @c_TermCode


	select	@c_Escala1=IDEscala1,
			@c_Escala2=IDEscala2,
			@c_Escala3=IDEscala3,
			@c_Escala4=IDEscala4,
			@c_Escala5=IDEscala5,
			@c_Escala6=IDEscala6,
			@c_beca1=beca1,
			@c_beca2=beca2,
			@c_beca3=beca3,
			@c_beca4=beca4,
			@c_beca5=beca5,
			@c_beca6=beca6,
			@c_Cronograma=cronograma
	from dbo.tblalumnoescala
	where 
	idalumno=@c_ID
	and IDPeracad = @c_TermCode
	and FechaEscala = @c_fechaescala

	IF (LEN(@c_Cronograma)=1)
		BEGIN
			SET @c_Cronograma = CONCAT('0',1)
		END

	
-------------------------------------------------
	SELECT 
		@c_IDSeccionC=IDSeccionC,
		@c_FecInic=FecInic,
		@c_categoriaPens=IDCategoriaPens
	FROM tblSeccionC
	WHERE 
		IDDependencia=@c_Divs
		AND IDsede=@c_Campus
		AND IDPerAcad=@c_TermCode
		AND IDEscuela=@c_Carrera
		and left(IDSeccionC,7) <> 'INT_PSI'
		and left(IDSeccionC,7) <> 'INT_ENF'
		and IDSeccionC <> '15NEX1A'
		AND RIGHT(IDSeccionc,2) = @c_Cronograma
-----------------------------------------------
	
-- verificando c�digo de alumno
	select 
		@c_CostoCred=PensCred 
	from dbo.tblCategoriaPens 
	WHERE
		IDDependencia=@c_Divs
		AND IDSede=@c_Campus
		AND IDEscuela=@c_Carrera
		AND IDCategoriaPens=@c_categoriaPens
		AND IDEscala=@c_escala1
	

----------- CUOTA INICIAL SOLO PARA REGULAR --------------
IF ((RIGHT(@c_IDSeccionc,2)!='20') AND @c_Departament = 'ADM' )
	BEGIN
		IF EXISTS (
				SELECT IDAlumno FROM dbo.tblCtaCorriente
				WHERE 
				IDDependencia=@c_Divs
				AND IDSede=@c_Campus
				AND IDPerAcad=@c_TermCode
				AND RIGHT(IDSeccionC,2)=@c_Cronograma
				AND IDAlumno = @c_ID
		)
			
			BEGIN
				IF (@c_Cronograma!='20' AND @c_Departament='ADM')
					BEGIN
						SET @c_cuotaInicial=@c_NCred*@c_CostoCred
						--select @c_Carrera, @c_Departament,@c_FechaEscala,@c_IDSeccionC,@c_FecInic,@c_categoriaPens, @c_Escala1,@c_CostoCred,@c_NCred,@c_cuotaInicial
						
						UPDATE dbo.tblCtaCorriente
							SET Cargo=@c_cuotaInicial
						WHERE 
						IDDependencia=@c_Divs
						AND IDSede=@c_Campus
						AND IDPerAcad=@c_TermCode
						AND RIGHT(IDSeccionC,2)=@c_Cronograma
						AND IDAlumno = @c_ID
						AND IDConcepto='C01'
						PRINT 'UPDATE CORRECTO'
					END				
			END
		ELSE
			BEGIN
				PRINT 'NO TIENE CTA CORRIENTE'
			END
END

--SELECT * FROM dbo.tblCtaCorriente
--WHERE 
--IDDependencia=@c_Divs
--AND IDSede=@c_Campus
--AND IDPerAcad=@c_TermCode
--AND RIGHT(IDSeccionC,2)=@c_Cronograma
--AND IDAlumno = @c_ID
--AND IDConcepto='C01'


-- HISTORICO de alumnos que modificaron su cuota inicial
IF NOT EXISTS (SELECT IDAlumno FROM dbo.tblCtaAlumnosWF WHERE IDAlumno = @c_ID AND IDPerAcad = @c_TermCode)
BEGIN
	INSERT INTO dbo.tblCtaAlumnosWF (IDAlumno, IDPerAcad ,IDDependencia,IDSede,IDSeccionC)
	VALUES  (@c_ID,@c_TermCode,@c_Divs,@c_Campus,@c_IDSeccionc)
END
 

