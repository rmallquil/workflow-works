/* ===================================================================================================================
  NOMBRE    : P_VERIFICAR_CTA_CTE
  FECHA     : 11/12/2017
  AUTOR     : Arana Milla, Karina Lizbeth
  OBJETIVO  : Verifica que se encuentre creada la cuenta corriente de Verano (C00 - C02)
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */


CREATE OR REPLACE PROCEDURE P_VERIFICAR_CTA_CTE (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO       IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_TERM_CODE       IN STVTERM.STVTERM_CODE%TYPE,
    P_COD_SEDE        IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CTA_CTE         OUT VARCHAR2,
    P_DESCRIP_CTA_CTE OUT VARCHAR2,
    P_ERROR           OUT VARCHAR2
)
AS
    V_ERROR       EXCEPTION;
    V_CONTADOR    NUMBER;
    V_APEC_TERM   VARCHAR2(10);
    V_APEC_CAMP   VARCHAR2(10);

BEGIN

-- PERIODO
SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;
-- CAMPUS 
SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_COD_SEDE;

WITH
CTE_tblCronogramas AS (
    -- GET Cronogramas activos
    SELECT  "IDSede" IDSede,
        "Cronograma" Cronograma
    FROM tblCronogramas@BDUCCI.CONTINENTAL.EDU.PE 
    WHERE "Activo" = '1'
    AND "Pronabec" = '0'
    AND "IDDepartamento" = P_DEPT_CODE
    AND "IDCampus" = P_COD_SEDE
),
CTE_tblCtaCorriente AS (
    -- GET Cuenta Corriente
    SELECT DISTINCT "IDAlumno" IDAlumno,
    "IDSede" IDSede,
    "IDSeccionC" IDSeccionC
    FROM  tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "IDDependencia" = 'UCCI'
    AND "IDSede" = V_APEC_CAMP
    AND "IDPerAcad" = V_APEC_TERM
    AND LENGTH("IDSeccionC") = 5
    AND "IDConcepto" in ('C00','C01')
    AND "IDAlumno" = P_ID_ALUMNO
)
  SELECT COUNT(IDAlumno) INTO V_CONTADOR
  FROM CTE_tblCtaCorriente cc
  INNER JOIN CTE_tblCronogramas cr ON
    cc.IDSede = cr.IDSede
    AND SUBSTR(cc.IDSeccionC,-2,2) = cr.Cronograma;

  IF V_CONTADOR > 0 THEN
    P_CTA_CTE := '1';
    P_DESCRIP_CTA_CTE := 'OK';
  ELSE
    P_CTA_CTE := '0';
    P_DESCRIP_CTA_CTE := 'NO TIENE CUENTA GENERADA';
  END IF;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_CTA_CTE;