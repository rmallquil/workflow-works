/*
SET SERVEROUTPUT ON
DECLARE P_STATUS_APTO VARCHAR2(30000);
 P_REASON VARCHAR2(30000);
BEGIN
    P_VERIFICAR_APTO(320724, '201810', P_STATUS_APTO, P_REASON);
    DBMS_OUTPUT.PUT_LINE(P_STATUS_APTO);
    DBMS_OUTPUT.PUT_LINE(P_REASON);
END;
*/

CREATE OR REPLACE PROCEDURE P_VERIFICAR_APTO (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_STATUS_APTO     OUT VARCHAR2,
        P_REASON          OUT VARCHAR2
)
AS
        V_RET01             INTEGER := 0; --VALIDA RETENCIÓN 01
        V_RET02             INTEGER := 0; --VALIDA RETENCIÓN 02
        V_SOLRES            INTEGER := 0; --VALIDA SOLICITUD RESERVA
        V_SOLTRA            INTEGER := 0; --VALIDA SOLICITUD TRASLADO INTERNO UNICO
        V_RESPUESTA         VARCHAR2(4000) := NULL;
        V_FLAG              INTEGER := 0;
        
        P_CODIGO_SOL          SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL013';
        V_CODE_DEPT           STVDEPT.STVDEPT_CODE%TYPE; 
        V_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
        V_CODE_CAMP           STVCAMP.STVCAMP_CODE%TYPE;
        
        V_CREDITOS            INTEGER;
        V_CICLO               INTEGER;

BEGIN

    --################################################################################################
    ---VALIDACIÓN DE RETENCIÓN 01 (DEUDA PENDIENTE)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET01
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '01'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
    
    IF V_RET01 > 0 THEN
        V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de deuda pendiente activa. ';
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE RETENCIÓN 02 (DOCUMENTO PENDIENTE) 
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET02    
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '02'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RET02 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de documento pendiente activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de documento pendiente activa. ';
        END IF;
        V_FLAG := 1;
    END IF;
    

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RESERVA DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLRES
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL005'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLRES > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        END IF;
        V_FLAG := 1;
    END IF;


    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE TRASLADO INTERNO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLTRA
    FROM SVRSVPR
    WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL013'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLTRA > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        END IF;
        V_FLAG := 1;
    END IF;
    
    --################################################################################################
    --OBTENER CREDITOS Y CICLO
    --################################################################################################

        BWZKWFFN.P_CREDITOS_CICLO(P_PIDM,V_CREDITOS,V_CICLO);

    --################################################################################################    
    ---MINIMO 198 CREDITOS PARA ACCEDER
    --################################################################################################
    IF V_CREDITOS < 198 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- No tiene los créditos solicitados (mínimo 198 créditos aprobados). ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- No tiene los créditos solicitados (mínimo 198 créditos aprobados). ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN FINAL
    --################################################################################################
    IF V_FLAG > 0 THEN
        P_STATUS_APTO := 'FALSE';
        P_REASON := V_RESPUESTA;
    ELSE
        P_STATUS_APTO := 'TRUE';
        P_REASON := 'OK';
    END IF;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VERIFICAR_APTO;