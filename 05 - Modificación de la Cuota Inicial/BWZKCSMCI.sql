/* ---------------------------------------------------------------------------------------------
-- buscar objecto
select dbms_metadata.get_ddl('PACKAGE','BWZKCSMCI') from dual
--------------------------------------------------------------------------------------------- */

create or replace PACKAGE BWZKCSMCI AS
/*******************************************************************************
 BWZKCSMCI:
    Paquete Web _ Desarrollo Propio _ Paquete _ Conti Solicitud Modificación Cuota Inicial
*******************************************************************************/
-- FILE NAME..: BWZKCSMCI.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKCSMCI
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/

PROCEDURE P_GET_USUARIO_REGACA (
                P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
                P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
                P_CORREO              OUT VARCHAR2,
                P_ROL_SEDE            OUT VARCHAR2,
                P_ERROR               OUT VARCHAR2
          );

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
                P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
                P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
                P_AN                  OUT NUMBER,
                P_ERROR               OUT VARCHAR2
          );
              
--------------------------------------------------------------------------------

PROCEDURE P_OBTENER_COMENTARIO_ALUMNO (
                    P_NUMERO_SOLICITUD        IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
                    P_COMENTARIO_ALUMNO       OUT VARCHAR2
              );

--------------------------------------------------------------------------------

PROCEDURE P_UPDATE_CTA_CORRIENTE (
                P_PIDM_ALUMNO      IN SPRIDEN.SPRIDEN_PIDM%TYPE,
                P_PERIODO          IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
                P_ERROR            OUT VARCHAR2
            );

--------------------------------------------------------------------------------

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END BWZKCSMCI;