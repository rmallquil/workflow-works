/**********************************************************************************************/
/* BWZKSMCI.sql                                                                               */
/**********************************************************************************************/
/*                                                                                            */
/* Descripción corta: Script para generar el Paquete de automatización del proceso de         */
/*                    Solicitud de Modificación de Cuota Inicial.                             */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI                FECHA    */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creacion del Código.                                        RML             20/ENE/2017 */
/*    --------------------                                                                    */
/*    Creación del paquete de Modificación de Cuota Inicial.                                  */
/*    Procedure P_GET_STUDENT_INPUTS: Obtiene los datos de la solicitud.                      */
/*    Proceudre P_VERIFICAR_APTO: Verifica que el estudiante cumpla las condiciones básicas   */
/*                              para acceder al WF.                                           */
/*    --Paquete generico                                                                      */
/*    Procedure P_VERIFICAR_CTA_CTE: Veifica si el ID tiene cta cte en el periodo actual.     */
/*    Procedure P_GET_USUARIO_REGACA: En base a una sede y rol, el procedimiento LOS CORREOS  */
/*                                    del(los) responsable(s) de Registros Académicos de      */
/*                                    esa sede.                                               */
/*    --Paquete generico                                                                      */
/*    Procedure P_VERIFICAR_ESTADO_SOLICITUD: Verifica el estado vigente de la solicitud      */
/*    --Paquete generico                                                                      */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud (XXXXXX)          */
/*    --Paquete generico                                                                      */
/*    Procedure P_SET_RECALCULO_CUOTA: Recalcula la cta cte en base a los créditos obtenidos. */
/* 2. Aumento P_VERIFICAR_APTO                                    BFV             09/JUL/2018 */
/*    Se aumenta el SP que verifica que el estudiante cumplas las condiciones basicas para    */
/*    acceder al WF.                                                                          */
/*    Aumento P_VERIFICAR_ESTADO_SOLICITUD                                                    */
/*    Verifica el estado vigente de la solicitud.                                             */
/* 3. Actualizacion Paquete Generico                              BFV             31/ENE/2019 */
/*    Se actualiza los SP P_CAMBIO_ESTADO_SOLICITUD, P_VERIFICAR_ESTADO_SOLICITUD,            */
/*      P_SET_RECALCULO_CUOTA, P_VERIFICAR_CTA_CTE para que llamen desde el paquete generico. */
/*                                                                                            */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/
-- PERMISOS DE EJECUCIÓN
/**********************************************************************************************/
--  SET SCAN ON 
-- 
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE 
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/

CREATE OR REPLACE PACKAGE BWZKSMCI AS

PROCEDURE P_GET_STUDENT_INPUTS (
        P_FOLIO_SOLICITUD      IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_CREDITOS_CODE        OUT SVRSVAD.SVRSVAD_ADDL_DATA_CDE%TYPE,
        P_CREDITOS_DESC        OUT SVRSVAD.SVRSVAD_ADDL_DATA_DESC%TYPE
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_APTO (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_STATUS_APTO     OUT VARCHAR2,
        P_REASON          OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_CTA_CTE (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_ID_ALUMNO       IN SPRIDEN.SPRIDEN_ID%TYPE,
        P_DEPT_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_TERM_CODE       IN STVTERM.STVTERM_CODE%TYPE,
        P_COD_SEDE        IN STVCAMP.STVCAMP_CODE%TYPE,
        P_CTA_CTE         OUT VARCHAR2,
        P_DESCRIP_CTA_CTE OUT VARCHAR2,
        P_ERROR           OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_USUARIO_REGACA (
        P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
        P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
        P_CORREO              OUT VARCHAR2,
        P_ROL_SEDE            OUT VARCHAR2,
        P_ERROR               OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_ESTADO_SOLICITUD (
        P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_STATUS_SOL          OUT VARCHAR2,
        P_ERROR               OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
        P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
        P_AN                  OUT NUMBER,
        P_ERROR               OUT VARCHAR2
);
              
--------------------------------------------------------------------------------

PROCEDURE P_SET_RECALCULO_CUOTA (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVTERM.STVTERM_CODE%TYPE,
        P_CREDITOS_CODE   IN SVRSVAD.SVRSVAD_ADDL_DATA_CDE%TYPE,
        P_MESSAGE         OUT VARCHAR2
);

--------------------------------------------------------------------------------

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END BWZKSMCI;


/**********************************************************************************************/
--  /
--  show errors 
-- 
--  SET SCAN ON
--  WHENEVER SQLERROR CONTINUE
--  DROP PUBLIC SYNONYM BWZKSMCI;
--  WHENEVER SQLERROR EXIT ROLLBACK
--  CREATE PUBLIC SYNONYM BWZKSMCI FOR BANINST1.BWZKSMCI;
--  WHENEVER SQLERROR CONTINUE
--  START gurgrtb BWZKSMCI
--  START gurgrth BWZKSMCI
--  WHENEVER SQLERROR EXIT ROLLBACK
/**********************************************************************************************/