/**********************************************************************************************/
/* BWZKSMCI BODY.sql                                                                          */
/**********************************************************************************************/
/*                                                                                            */
/* Descripción corta: Script para generar el Paquete de automatización del proceso de         */
/*                    Solicitud de Modificación de Cuota Inicial.                             */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI                FECHA    */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creacion del Código.                                        RML             20/ENE/2017 */
/*    --------------------                                                                    */
/*    Creación del paquete de Modificación de Cuota Inicial.                                  */
/*    Procedure P_GET_STUDENT_INPUTS: Obtiene los datos de la solicitud.                      */
/*    Proceudre P_VERIFICAR_APTO: Verifica que el estudiante cumpla las condiciones básicas   */
/*                              para acceder al WF.                                           */
/*    Procedure P_VERIFICAR_CTA_CTE: Veifica si el ID tiene cta cte en el periodo actual.     */
/*    Procedure P_GET_USUARIO_REGACA: En base a una sede y rol, el procedimiento LOS CORREOS  */
/*                                    del(los) responsable(s) de Registros Académicos de      */
/*                                    esa sede.                                               */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud (XXXXXX)          */
/*    Procedure P_SET_RECALCULO_CUOTA: Recalcula la cta cte en base a los créditos obtenidos. */
/*                                                                                            */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/
-- PERMISOS DE EJECUCIÓN
/**********************************************************************************************/
--  SET SCAN ON 
-- 
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE 
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/

CREATE OR REPLACE PACKAGE BODY BWZKSMCI AS

PROCEDURE P_GET_STUDENT_INPUTS (
    P_FOLIO_SOLICITUD      IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_CREDITOS_CODE        OUT SVRSVAD.SVRSVAD_ADDL_DATA_CDE%TYPE,
    P_CREDITOS_DESC        OUT SVRSVAD.SVRSVAD_ADDL_DATA_DESC%TYPE
)
AS
      V_ERROR                   EXCEPTION;
BEGIN
    SELECT SVRSVAD_ADDL_DATA_CDE, SVRSVAD_ADDL_DATA_DESC
        INTO P_CREDITOS_CODE, P_CREDITOS_DESC
    FROM SVRSVAD 
    INNER JOIN SVRSRAD ON
        SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
    WHERE SVRSRAD_SRVC_CODE = 'SOL007'
        AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
    ORDER BY SVRSVAD_ADDL_DATA_SEQ;
    
    IF P_CREDITOS_CODE IS NULL THEN
        RAISE V_ERROR;
    END IF;
      
EXCEPTION
WHEN V_ERROR THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontró el identificador de la solicitud.' );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_STUDENT_INPUTS;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_APTO (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_STATUS_APTO     OUT VARCHAR2,
    P_REASON          OUT VARCHAR2
)
AS
    V_RET01             INTEGER := 0; --VALIDA RETENCIÓN 01
    V_RET02             INTEGER := 0; --VALIDA RETENCIÓN 02
    V_SOLRES            INTEGER := 0; --VALIDA SOLICITUD RESERVA
    V_SOLTRA            INTEGER := 0; --VALIDA SOLICITUD TRASLADO INTERNO UNICO
    V_RESPUESTA         VARCHAR2(4000) := NULL;
    V_FLAG              INTEGER := 0;
    
    P_CODIGO_SOL          SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL013';
    V_CODE_DEPT           STVDEPT.STVDEPT_CODE%TYPE; 
    V_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
    V_CODE_CAMP           STVCAMP.STVCAMP_CODE%TYPE;
    
    V_CREDITOS            INTEGER;
    V_CICLO               INTEGER;

BEGIN

    --################################################################################################
    ---VALIDACIÓN DE RETENCIÓN 01 (DEUDA PENDIENTE)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET01
    FROM SPRHOLD
    WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '01'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
    
    IF V_RET01 > 0 THEN
        V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de deuda pendiente activa. ';
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE RETENCIÓN 02 (DOCUMENTO PENDIENTE) 
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET02    
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '02'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RET02 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de documento pendiente activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de documento pendiente activa. ';
        END IF;
        V_FLAG := 1;
    END IF;
    

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RESERVA DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLRES
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL005'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLRES > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        END IF;
        V_FLAG := 1;
    END IF;


    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE TRASLADO INTERNO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLTRA
    FROM SVRSVPR
    WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL013'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLTRA > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        END IF;
        V_FLAG := 1;
    END IF;
    

    --################################################################################################
    ---VALIDACIÓN FINAL
    --################################################################################################
    IF V_FLAG > 0 THEN
        P_STATUS_APTO := 'FALSE';
        P_REASON := V_RESPUESTA;
    ELSE
        P_STATUS_APTO := 'TRUE';
        P_REASON := 'OK';
    END IF;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VERIFICAR_APTO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_CTA_CTE (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO       IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_TERM_CODE       IN STVTERM.STVTERM_CODE%TYPE,
    P_COD_SEDE        IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CTA_CTE         OUT VARCHAR2,
    P_DESCRIP_CTA_CTE OUT VARCHAR2,
    P_ERROR           OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_VERIFICAR_CTA_CTE (P_PIDM, P_ID_ALUMNO, P_DEPT_CODE, P_TERM_CODE, P_COD_SEDE, P_CTA_CTE, P_DESCRIP_CTA_CTE, P_ERROR);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_CTA_CTE;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_USUARIO_REGACA (
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
      P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
      P_CORREO              OUT VARCHAR2,
      P_ROL_SEDE            OUT VARCHAR2,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_USUARIO
  FECHA     : 04/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento LOS CORREOS deL(os) responsable(s) 
              de Registros Academicos de esa sede.
  =================================================================================================================== */
AS
      -- @PARAMETERS
      V_ROLE_ID                 NUMBER;
      V_ORG_ID                  NUMBER;
      V_Email_Address           VARCHAR2(100);
    
      V_SECCION_EXCEPT          VARCHAR2(50);
      
      CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = V_ORG_ID AND ROLE_ID = V_ROLE_ID;
      
      V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
BEGIN 
--
    P_ROL_SEDE := P_ROL || P_CAMP_CODE;
    
    -- Obtener el ROL_ID 
    V_SECCION_EXCEPT := 'ROLES';
    
    SELECT ID 
        INTO V_ROLE_ID 
    FROM WORKFLOW.ROLE 
    WHERE NAME = P_ROL_SEDE;
    
    V_SECCION_EXCEPT := '';
    
    -- Obtener el ORG_ID 
    V_SECCION_EXCEPT := 'ORGRANIZACION';
    
    SELECT ID 
        INTO V_ORG_ID 
    FROM WORKFLOW.ORGANIZATION
    WHERE NAME = 'Root';
    
    V_SECCION_EXCEPT := '';
    
    -- Obtener los datos de usuarios que relaciona rol y usuario
    V_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
    -- #######################################################################
    OPEN C_ROLE_ASSIGNMENT;
    LOOP
        FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
        EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
            -- Obtener Datos Usuario
            SELECT Email_Address 
                INTO V_Email_Address
            FROM WORKFLOW.WFUSER
            WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;
            
            P_CORREO := P_CORREO || V_Email_Address || ',';
    END LOOP;
    CLOSE C_ROLE_ASSIGNMENT;
    
    V_SECCION_EXCEPT := '';
    
    -- Extraer el ultimo digito en caso sea un "coma"(,)
    SELECT SUBSTR(P_CORREO,1,LENGTH(P_CORREO) -1)
        INTO P_CORREO
    FROM DUAL
    WHERE SUBSTR(P_CORREO,-1,1) = ',';
      
EXCEPTION
WHEN TOO_MANY_ROWS THEN 
    IF ( V_SECCION_EXCEPT = 'ROLES') THEN
      P_ERROR := '- Se encontraron mas de un ROL con el mismo nombre: ' || P_ROL || P_CAMP_CODE;
    ELSIF (V_SECCION_EXCEPT = 'ORGANIZACION') THEN
      P_ERROR := '- Se encontraron mas de una ORGANIZACIÓN con el mismo nombre.';
    ELSIF (V_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
      P_ERROR := '- Se encontraron mas de un usuario con el mismo ROL.';
    ELSE  P_ERROR := SQLERRM;
    END IF; 
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
WHEN NO_DATA_FOUND THEN
    IF ( V_SECCION_EXCEPT = 'ROLES') THEN
      P_ERROR := '- NO se encontró el nombre del ROL: ' || P_ROL || P_CAMP_CODE;
    ELSIF (V_SECCION_EXCEPT = 'ORGANIZACION') THEN
      P_ERROR := '- No se encontró el nombre de la ORGANIZACION.';
    ELSIF (V_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
      P_ERROR := '- No se encontró ningun usuario con esas caracteristicas.';
    ELSE  P_ERROR := SQLERRM;
    END IF; 
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_USUARIO_REGACA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_ESTADO_SOLICITUD (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_STATUS_SOL          OUT VARCHAR2,
    P_ERROR               OUT VARCHAR2
)
AS
BEGIN

BWZKPSPG.P_VERIFICAR_ESTADO_SOLICITUD (P_FOLIO_SOLICITUD, P_STATUS_SOL, P_ERROR);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_ESTADO_SOLICITUD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
    P_AN                  OUT NUMBER,
    P_ERROR               OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_CAMBIO_ESTADO_SOLICITUD (P_FOLIO_SOLICITUD, P_ESTADO, P_AN, P_ERROR);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CAMBIO_ESTADO_SOLICITUD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_RECALCULO_CUOTA (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVTERM.STVTERM_CODE%TYPE,
    P_CREDITOS_CODE   IN SVRSVAD.SVRSVAD_ADDL_DATA_CDE%TYPE,
    P_MESSAGE         OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_SET_RECALCULO_CUOTA (P_PIDM, P_TERM_CODE, P_CREDITOS_CODE, P_MESSAGE);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_RECALCULO_CUOTA;
  
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--                   
END BWZKSMCI;

/**************************************************************************************************/
--/
--show errors
--
--SET SCAN ON
/**************************************************************************************************/