/* 
SET SERVEROUTPUT ON
declare P_MESSAGE             VARCHAR2(4000);
begin
    P_SETPRORROGA_GA(263159,'UPGT','S01','201710','09762075','109','C03','ELE03','22-Apr-2017 00:00:00','15-Jun-2017 00:00:00','COMENTARIO ALUMNO',P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE('-- FIN --');
end;
*/

CREATE OR REPLACE PROCEDURE P_SETPRORROGA_GA (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_APEC_IDALUMNO       IN VARCHAR2,
      P_PROGRAM             IN SORLCUR.SORLCUR_PROGRAM%TYPE,
      P_PRORR_CONCEPTO      IN VARCHAR2,
      P_PRORR_SECCIONC      IN VARCHAR2,
      P_FECINIC             IN VARCHAR2,
      P_FEC_PRORRG_CARGO    IN VARCHAR2, -- REGLA: LA FECHA LIMITE DE LA PRORROGA PARA EL PAGO DE LA CUOTA ES EL 15/MM/YYYY
      P_COMENTARIO          IN VARCHAR2, -- EN APEC SOLO PERMITE 200 CARACTERES
      P_MESSAGE             OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SETPRORROGA_GA
  FECHA     : 11/07/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : - Registra la prorroga para la cuota del alumno validando que no exista una anterior.
              - Retira la penalidad de Gasto Administrativo en caso se haya generado.
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
      V_GA_CONCEPTO         VARCHAR2(5); --  CONCEPTO Gasto Administrativo (GA)
      V_GA_COSTO            INTEGER := 25;     --  COSTP POR Gasto Administrativo (GA)
      V_APEC_CAMP           VARCHAR2(9);
      V_APEC_DEPT           VARCHAR2(9);
      V_APEC_TERM           VARCHAR2(10);
      V_COMENTARIO          VARCHAR(200);
      V_RESPUESTA           CHAR(1);
      V_ERROR               EXCEPTION;      
      V_RESULT              INTEGER;
      V_APEC_NIVEL          VARCHAR2(9) := 'UCCI';
      
      V_FECINIC             DATE;
      V_FEC_PRORRG_CARGO    DATE;
BEGIN

    -- CONVERTIR LA FECHA A UN FORMATO VALIDO
    SELECT 
          TO_DATE(P_FECINIC,'dd/mm/yyyy hh24:mi:ss'),
          TO_DATE(P_FEC_PRORRG_CARGO,'dd/mm/yyyy hh24:mi:ss')
    INTO 
          V_FECINIC,
          V_FEC_PRORRG_CARGO
    FROM DUAL;
    
    ----------------------------------------------
    SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
    SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
    SELECT CZRDEPT_DEPT_BDUCCI INTO V_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO
    
    -- CONCEPTO DEL CARGO ADMINISTRATIVO
    SELECT 'GA' || SUBSTR(TRIM(P_PRORR_CONCEPTO), -1) INTO V_GA_CONCEPTO FROM DUAL;
    
    -- GET COMENTARIO (el campo de la tabla esta configurado para 200 caracteres)
    SELECT SUBSTR(TRIM(P_COMENTARIO),0,200) INTO V_COMENTARIO FROM DUAL;

    -- REGISTRAR PRORROGA Y ACTUALIZAR CTA CORRIENTE.(en caos ya tenga Gasto Administrativo)
    ---------------------------------------    
    V_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
          'dbo.sp_SetProrrogaCuota "'
          || P_APEC_IDALUMNO ||'" , "'|| V_APEC_CAMP || '" , "' || V_APEC_NIVEL || '" , "' || V_APEC_TERM ||'" , "'|| P_PRORR_SECCIONC ||'" , "'|| P_PRORR_CONCEPTO 
          || '" , "'|| P_PROGRAM ||'" , "'|| V_GA_CONCEPTO ||'" , "'|| '25' ||'" , "'|| V_APEC_DEPT ||'" , "'|| TO_CHAR(V_FECINIC, 'dd/mm/yyyy') 
          ||'" , "'|| TO_CHAR(V_FEC_PRORRG_CARGO, 'dd/mm/yyyy') || '" , "'|| 'WorkFlow'||'" , "'|| V_COMENTARIO ||'" '
    );
   
    COMMIT;
    
EXCEPTION
  WHEN V_ERROR THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontro una CTA CORRIENTE para el alumno.' );
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SETPRORROGA_GA;