USE [BDUCCI]
GO
/****** Object:  StoredProcedure [dbo].[sp_updateCargoAdministrativo]    Script Date: 24/07/2017 17:44:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ===================================================================================================================
NOMBRE      : [dbo].[sp_updateCargoAdministrativo]
FECHA       : 
AUTOR       : Franklin Huaman 
OBJETIVO    : Actualiza los cargos administrativos, según periodo academico, código de cocepto, modalidad

MODIFICACIONES
NRO     FECHA			USUARIO		MODIFICACION
001		24/07/2017		FHUAMANA	se modifica en la consulta: AND ISNULL(FecProrroga,'01/01/1900')<getdate()
=================================================================================================================== */
ALTER PROCEDURE [dbo].[sp_updateCargoAdministrativo]
@c_IDPerAcad varchar(6),
@c_IDConcepto varchar(5),
@c_cargoAdm varchar(5),
@c_Modalidad varchar(6)
AS
--DECLARE
--	@c_IDPerAcad varchar(6),
--	@c_IDConcepto varchar(5),
--	@c_cargoAdm varchar(5),
--	@c_Modalidad varchar(6)

--SET @c_IDPerAcad = '2017-1'
--SET @c_IDConcepto = 'C03'
--SET @c_cargoAdm = 'GA3'
--SET @c_Modalidad = 'UREG'
declare
@c_idsede varchar(10),
@c_idalumno varchar(10),
@c_iddependencia varchar(10),
@c_idseccionc varchar(10),
@c_fecinic datetime,
@c_idescuela  varchar(10)
IF (@c_Modalidad='UREG')
BEGIN
	DECLARE CUR_1 CURSOR FOR
	SELECT
		idsede,idalumno,idperacad,iddependencia,idseccionc,fecinic,idescuela 
	FROM
	dbo.tblctacorriente 
	where idperacad = @c_IDPerAcad and idconcepto = @c_IDConcepto
	and right(idseccionc,2) in ('01','02','16')
	and deuda > 0
	AND FecCargo<getdate()
	AND ISNULL(FecProrroga,'01/01/1900')<getdate()
	AND IDESCUELA IN (SELECT IDESCUELA FROM DBO.TBLESCUELA WHERE IDTIPOESC ='CAR')
	AND right(IDSeccionC,2) <>'20'
	OPEN CUR_1
	FETCH next FROM CUR_1 INTO @c_idsede, @c_idalumno, @c_idperacad, @c_iddependencia, @c_idseccionc, @c_fecinic, @c_idescuela
	WHILE @@FETCH_STATUS = 0
	BEGIN
		UPDATE dbo.tblctacorriente set cargo=25
		WHERE 
			idsede=@c_idsede
			and idalumno=@c_idalumno
			and idperacad=@c_idperacad
			and iddependencia=@c_iddependencia
			and idseccionc=@c_idseccionc
			and idescuela = @c_idescuela
			and idconcepto = @c_cargoAdm
	FETCH next FROM CUR_1 INTO @c_idsede, @c_idalumno, @c_idperacad, @c_iddependencia, @c_idseccionc, @c_fecinic, @c_idescuela
	END
	CLOSE CUR_1
	DEALLOCATE CUR_1
END
IF (@c_Modalidad='UVIR')
BEGIN
	DECLARE CUR_1 CURSOR FOR
	SELECT
		idsede,idalumno,idperacad,iddependencia,idseccionc,fecinic,idescuela 
	FROM
	dbo.tblctacorriente 
	where idperacad = @c_IDPerAcad and idconcepto = @c_IDConcepto
	and right(idseccionc,2) in ('08')
	and deuda>0
	AND FecCargo<getdate()
	AND ISNULL(FecProrroga,'01/01/1900')<getdate()
	AND IDESCUELA IN (SELECT IDESCUELA FROM DBO.TBLESCUELA WHERE IDTIPOESC ='CAR')
	AND right(IDSeccionC,2) <>'20'
	OPEN CUR_1
	FETCH next FROM CUR_1 INTO @c_idsede, @c_idalumno, @c_idperacad, @c_iddependencia, @c_idseccionc, @c_fecinic, @c_idescuela
	WHILE @@FETCH_STATUS = 0
	BEGIN
		UPDATE dbo.tblctacorriente set cargo=25
		WHERE 
			idsede=@c_idsede
			and idalumno=@c_idalumno
			and idperacad=@c_idperacad
			and iddependencia=@c_iddependencia
			and idseccionc=@c_idseccionc
			and idescuela = @c_idescuela
			and idconcepto = @c_cargoAdm
	FETCH next FROM CUR_1 INTO @c_idsede, @c_idalumno, @c_idperacad, @c_iddependencia, @c_idseccionc, @c_fecinic, @c_idescuela
	END
	CLOSE CUR_1
	DEALLOCATE CUR_1
END
IF (@c_Modalidad='UPGT')
BEGIN
	DECLARE CUR_1 CURSOR FOR
	SELECT
		idsede,idalumno,idperacad,iddependencia,idseccionc,fecinic,idescuela 
	FROM
	dbo.tblctacorriente 
	where idperacad = @c_IDPerAcad and idconcepto = @c_IDConcepto
	and right(idseccionc,2) in ('03','10','14')
	and deuda>0
	AND FecCargo<getdate()
	AND ISNULL(FecProrroga,'01/01/1900')<getdate()
	AND IDESCUELA IN (SELECT IDESCUELA FROM DBO.TBLESCUELA WHERE IDTIPOESC ='CAR')
	AND right(IDSeccionC,2) <>'20'
	OPEN CUR_1
	FETCH next FROM CUR_1 INTO @c_idsede, @c_idalumno, @c_idperacad, @c_iddependencia, @c_idseccionc, @c_fecinic, @c_idescuela
	WHILE @@FETCH_STATUS = 0
	BEGIN
		UPDATE dbo.tblctacorriente set cargo=25
		WHERE 
			idsede=@c_idsede
			and idalumno=@c_idalumno
			and idperacad=@c_idperacad
			and iddependencia=@c_iddependencia
			and idseccionc=@c_idseccionc
			and idescuela = @c_idescuela
			and idconcepto = @c_cargoAdm
	FETCH next FROM CUR_1 INTO @c_idsede, @c_idalumno, @c_idperacad, @c_iddependencia, @c_idseccionc, @c_fecinic, @c_idescuela
	END
	CLOSE CUR_1
	DEALLOCATE CUR_1
END