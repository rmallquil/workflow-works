/**********************************************************************************************/
/* BWZKCSPS BODY.sql                                                                          */
/**********************************************************************************************/
/*                                                                                            */
/* Descripcion corta: Script para generar el Paquete de automatizacion del proceso de         */
/*                    Solicitud de Prorroga de pagos.                                         */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI                FECHA    */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creacion del Codigo.                                        RML             12/JUL/2017 */
/*    --------------------                                                                    */
/*    Creacion del paquete de Reserva de Matricula On Demand                                  */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud (XXXXXX).         */
/*    Procedure P_OBTENER_COMENTARIO_ALUMNO: Obtiene EL COMENTARIO como unico dato ingresado  */
/*              por el estudiante.                                                            */
/*    Procedure P_GETDATAX_CARGO: Obtiene los datos de la CTA alumno (conceptos) PARA realizar*/
/*              la prorroga.                                                                  */
/*    Procedure P_SETPRORROGA_GA: Registra la prorroga para la cuota del alumno validando que */
/*              no exista una anterior.Retira la penalidad de Gasto Administrativo en caso se */
/*              haya generado.                                                                */
/*    Procedure P_SIDEUDA_CUOTA: Verifica la existencia de deuda para un APEC_IDAlumno Y      */
/*              CONCEPTO DETERMINADO.                                                         */    
/*    Procedure P_CALC_DIAS: Calcula los dias restante a una fecha. Devuelve                  */ 
/*                           0- Si estamos a mas de 2 de la fecha limite de prorroga.         */
/*                           1- Si estamos a 1 o a cero dias de la fecha limite de prorroga.  */  
/*                           2- Si ya estamos en la fecha o posterior a la fecha limite de    */
/*                              prorroga                                                      */
/*                                                                                            */
/* 2. Actualizacion P_GETDATAX_CARGO                           LAM                10/APR/2018 */
/*    Se cambia la validacion de la seccion de caja por length(IDSeccionC)=5.                 */        
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/
  -- PERMISOS DE EJECUCION
  /**********************************************************************************************/
  --  SET SCAN ON 
  -- 
  --  CONNECT baninst1/&&baninst1_password;
  --  WHENEVER SQLERROR CONTINUE 
  -- 
  --  SET SCAN OFF
  --  SET ECHO OFF
  /**********************************************************************************************/

CREATE OR REPLACE PACKAGE BODY BWZKCSPS AS
       
PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
    P_AN                  OUT NUMBER,
    P_ERROR               OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_CAMBIO_ESTADO_SOLICITUD (P_FOLIO_SOLICITUD, P_ESTADO, P_AN, P_ERROR);

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CAMBIO_ESTADO_SOLICITUD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_OBTENER_COMENTARIO_ALUMNO (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_COMENTARIO          OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_OBTENER_COMENTARIO_ALUMNO
  FECHA     : 07/07/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene EL COMENTARIO como unico dato ingresado por el estudiante.
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
    V_ERROR                   EXCEPTION;
    V_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
    V_COMENTARIO              VARCHAR2(4000);
    V_CLAVE                   VARCHAR2(4000);
    V_ADDL_DATA_SEQ           SVRSVAD.SVRSVAD_ADDL_DATA_SEQ%TYPE;
    
    -- GET COMENTARIO Y TELEFONO de la solicitud. (comentario PERSONALZIADO.)
    CURSOR C_SVRSVAD IS
        SELECT SVRSVAD_ADDL_DATA_SEQ, SVRSVAD_ADDL_DATA_CDE, SVRSVAD_ADDL_DATA_DESC
        FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
        INNER JOIN SVRSRAD ON --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
            SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
        WHERE SVRSRAD_SRVC_CODE = V_CODIGO_SOLICITUD
            --AND SVRSVAD_ADDL_DATA_CDE = 'PREGUNTA'
            AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
        ORDER BY SVRSVAD_ADDL_DATA_SEQ;
BEGIN
      
    -- GET CODIGO SOLICITUD
    SELECT SVRSVPR_SRVC_CODE 
        INTO V_CODIGO_SOLICITUD 
    FROM SVRSVPR 
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
      
    -- OBTENER COMENTARIO Y EL TELEFONO DE LA SOLICITUD (LOS DOS ULTIMOS DATOS)
    OPEN C_SVRSVAD;
    LOOP
        FETCH C_SVRSVAD INTO V_ADDL_DATA_SEQ, V_CLAVE, V_COMENTARIO;
        IF C_SVRSVAD%FOUND THEN
            IF V_ADDL_DATA_SEQ = 1 THEN
                -- OBTENER COMENTARIO DE LA SOLICITUD
                P_COMENTARIO := V_COMENTARIO;
            END IF;
        ELSE EXIT;
        END IF;
    END LOOP;
    CLOSE C_SVRSVAD;

    IF P_COMENTARIO IS NULL THEN
        RAISE V_ERROR;
    END IF;
      
EXCEPTION
WHEN V_ERROR THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontró el comentario y/o el telefono.' );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_OBTENER_COMENTARIO_ALUMNO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GETDATAX_CARGO (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_APEC_IDALUMNO       OUT VARCHAR2,
    P_PROGRAM             OUT SORLCUR.SORLCUR_PROGRAM%TYPE,
    P_PRORR_CONCEPTO      OUT VARCHAR2,
    P_PRORR_SECCIONC      OUT VARCHAR2,
    P_FECCARGO            OUT VARCHAR2,
    P_FECINIC             OUT VARCHAR2,
    P_FEC_PRORRG_CARGO    OUT VARCHAR2 -- REGLA: LA FECHA LIMITE DE LA PRORROGA PARA EL PAGO DE LA CUOTA ES EL 15/MM/YYYY
)
/* ===================================================================================================================
  NOMBRE    : P_GETDATAX_CARGO
  FECHA     : 07/07/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene los datos de la CTA alumno (conceptos) PARA realizar la prorroga.
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
    V_ERROR               EXCEPTION;      
    V_ERROR1              EXCEPTION;     
    V_APEC_IDALUMNO       VARCHAR2(20);
    V_APEC_CAMP           VARCHAR2(9);
    V_APEC_TERM           VARCHAR2(9);
    V_IDSECCIONC          VARCHAR2(18);
    V_FECINIC             DATE;
    V_CONCEPTO            VARCHAR2(9);
    V_FECCARGO            DATE;
    
    V_FEC_PRORRG_INI      DATE;
    V_FEC_PRORRG_FIN      DATE;
    
    V_PROGRAM             SORLCUR.SORLCUR_PROGRAM%TYPE;
    V_RECEPTION_DATE      SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE;
    V_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
    V_PTRM                SOBPTRM.SOBPTRM_PTRM_CODE%TYPE;
    
    -- CURSOR GET PROGRAMA
    CURSOR GET_DATACURR_C IS
        SELECT SORLCUR_PROGRAM
        FROM (
            SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_PROGRAM
            FROM SORLCUR
            INNER JOIN SORLFOS ON
                SORLCUR_PIDM = SORLFOS_PIDM 
                AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
            WHERE SORLCUR_PIDM = P_PIDM 
                AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE = 'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y'
                AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
        ) WHERE ROWNUM = 1;
    
    -- Calculando parte PERIODO (sub PTRM --> SFRRSTS)
    CURSOR GET_PTRM_C IS
        SELECT DISTINCT SUBSTR(CZRPTRM_CODE,1,2) SUBPTRM
        FROM CZRPTRM 
        WHERE CZRPTRM_DEPT = P_DEPT_CODE
        AND CZRPTRM_CAMP_CODE = P_CAMP_CODE;
    
    -- OBTENER PERIODO ACTIVO (SOATERM) inicio a fin
    CURSOR SOBPTRM_C IS
        SELECT SOBPTRM_PTRM_CODE FROM
        (
            SELECT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE, MIN(SOBPTRM_START_DATE) SOBPTRM_START_DATE, MAX(SOBPTRM_END_DATE) SOBPTRM_END_DATE
            FROM SOBPTRM
            WHERE SOBPTRM_PTRM_CODE IN (V_SUB_PTRM||'1' , (V_SUB_PTRM || CASE WHEN (P_DEPT_CODE ='UVIR' OR P_DEPT_CODE ='UPGT') THEN '2' ELSE '-' END) ) 
                AND SOBPTRM_PTRM_CODE NOT LIKE '%0'
            GROUP BY SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE
        ) WHERE ROWNUM = 1
        AND TRUNC(V_RECEPTION_DATE) BETWEEN SOBPTRM_START_DATE AND SOBPTRM_END_DATE
        ORDER BY SOBPTRM_TERM_CODE DESC;  
    
    -- GET FECHAS DE cARGO Y los conceptos validos para las Prorrogas
    -- UREG : C02,C03,C04
    -- UPGT : C02,C03
    CURSOR GET_FCARGO_C IS
    SELECT IDAlumno, IDSeccionC, FecInic, IDConcepto, FecCargo
    FROM (
        WITH 
            CTE_tblSeccionC AS (
              -- GET SECCIONC
              SELECT  "IDSeccionC" IDSeccionC,
                      "FecInic" FecInic
              FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
              WHERE "IDDependencia"='UCCI'
              AND "IDsede"    = V_APEC_CAMP
              AND "IDPerAcad" = V_APEC_TERM
              AND "IDEscuela" = V_PROGRAM
              AND LENGTH("IDSeccionC") = 5
              AND SUBSTRB("IDSeccionC",-2,2) IN (
                  -- PARTE PERIODO
                  SELECT CZRPTRM_PTRM_BDUCCI 
                  FROM CZRPTRM
                  WHERE CZRPTRM_CODE = V_PTRM
              )
            ),
            CTE_tblPersonaAlumno AS (
              SELECT "IDAlumno" IDAlumno 
              FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
              WHERE "IDPersona" IN ( 
                  SELECT "IDPersona"
                  FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE
                  WHERE "IDPersonaN" = P_PIDM
              )
            )
        SELECT "IDAlumno" IDAlumno, "IDSeccionC" IDSeccionC, "FecInic" FecInic, "IDConcepto" IDConcepto, "FecCargo" FecCargo
        FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDependencia" = 'UCCI'
        AND "IDAlumno"     IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
        AND "IDSede"      = V_APEC_CAMP
        AND "IDPerAcad"   = V_APEC_TERM
        AND "IDEscuela"   = V_PROGRAM
        AND "IDSeccionC"  IN (SELECT IDSeccionC FROM CTE_tblSeccionC)
        AND "Prorroga"    = '0'
        AND "Estado"      = 'M' --NO SE DA PRORROGAS A OTROS ESTADOS
        AND "IDConcepto"  IN ('C02','C03', CASE WHEN P_DEPT_CODE = 'UREG' THEN 'C04' ELSE '-' END) 
    );
      
BEGIN
    
    -----------------------------------------------------------
    -- GET CODIGO SOLICITUD
    SELECT SVRSVPR_RECEPTION_DATE
        INTO V_RECEPTION_DATE
    FROM SVRSVPR 
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
    
    -----------------------------------------------------------
    -- >> GET PROGRAM --
    OPEN GET_DATACURR_C;
    LOOP
        FETCH GET_DATACURR_C INTO V_PROGRAM;
        IF GET_DATACURR_C%FOUND THEN
            P_PROGRAM := V_PROGRAM;
        ELSE EXIT;
        END IF;
    END LOOP;
    CLOSE GET_DATACURR_C;

    ------------------------------------------------------------
    -- >> calculando SUB PARTE PERIODO  --
    OPEN GET_PTRM_C;
    LOOP
        FETCH GET_PTRM_C INTO V_SUB_PTRM;
        EXIT WHEN GET_PTRM_C%NOTFOUND;
    END LOOP;
    CLOSE GET_PTRM_C;
    -- >> Obteniendo LA PARTE PERIODO  --
    OPEN SOBPTRM_C;
    LOOP
        FETCH SOBPTRM_C INTO V_PTRM;
        EXIT WHEN SOBPTRM_C%NOTFOUND;
    END LOOP;
    CLOSE SOBPTRM_C;

    ----------------------------------------------------------------------------------
    -- GET FECHAS DE cARGO Y los conceptos validos para las Prorrogas
    -- UREG : C02,C03,C04
    -- UPGT : C02,C03
    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP
    FROM CZRCAMP
    WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
    
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
    
    OPEN GET_FCARGO_C;
    LOOP 
        FETCH GET_FCARGO_C INTO V_APEC_IDALUMNO, V_IDSECCIONC, V_FECINIC, V_CONCEPTO, V_FECCARGO;
        IF GET_FCARGO_C%FOUND THEN
            ------------------ LAS REGLAS PARA LOS DEPARTAMENTOS YA SE FILTRARON EN LA CONSULTA -----------------
            -- En la regla de caja y bienestar la fecha de vencimiento de pago es el 4 de cada mes a una pension.
            V_FEC_PRORRG_INI := TO_DATE(('20/' || TO_CHAR(ADD_MONTHS(V_FECCARGO, -1), 'mm/yyyy')),'dd/mm/yyyy');
            V_FEC_PRORRG_FIN := TO_DATE(('07/' || TO_CHAR(V_FECCARGO,'mm/yyyy')),'dd/mm/yyyy') + 1;
            
            IF (V_FEC_PRORRG_INI <= (V_RECEPTION_DATE) AND (V_RECEPTION_DATE) < V_FEC_PRORRG_FIN) THEN
                P_FEC_PRORRG_CARGO := TO_CHAR(TO_DATE(('15/' || TO_CHAR(V_FECCARGO,'mm/yyyy')),'dd/mm/yyyy'), 'dd/mm/yyyy hh24:mi:ss');
                P_APEC_IDALUMNO    := V_APEC_IDALUMNO;
                P_PRORR_SECCIONC   := V_IDSECCIONC;
                P_PRORR_CONCEPTO   := V_CONCEPTO;
                P_FECCARGO         := TO_CHAR(V_FECCARGO, 'dd/mm/yyyy hh24:mi:ss'); 
                P_FECINIC          := TO_CHAR(V_FECINIC, 'dd/mm/yyyy hh24:mi:ss');
            END IF;
            ------------------------------------------------------------
        ELSE EXIT;
        END IF;
    END LOOP;
    CLOSE GET_FCARGO_C;
    
    IF(P_APEC_IDALUMNO IS NULL OR P_PRORR_SECCIONC IS NULL OR P_PRORR_CONCEPTO IS NULL) THEN
          RAISE V_ERROR;
    END IF;
EXCEPTION
WHEN V_ERROR THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontro la "Cuenta corriente", una "cuota valida" para la prorroga o esta en un estado diferente a "Matriculado".' );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GETDATAX_CARGO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SETPRORROGA_GA (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
    P_APEC_IDALUMNO       IN VARCHAR2,
    P_PROGRAM             IN SORLCUR.SORLCUR_PROGRAM%TYPE,
    P_PRORR_CONCEPTO      IN VARCHAR2,
    P_PRORR_SECCIONC      IN VARCHAR2,
    P_FECINIC             IN VARCHAR2,
    P_FEC_PRORRG_CARGO    IN VARCHAR2, -- REGLA: LA FECHA LIMITE DE LA PRORROGA PARA EL PAGO DE LA CUOTA ES EL 15/MM/YYYY
    P_COMENTARIO          IN VARCHAR2, -- EN APEC SOLO PERMITE 200 CARACTERES
    P_MESSAGE             OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SETPRORROGA_GA
  FECHA     : 11/07/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : - Registra la prorroga para la cuota del alumno validando que no exista una anterior.
              - Retira la penalidad de Gasto Administrativo en caso se haya generado.
  
  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  001   02/10/2017    rmallqui      Extraccion del comentario en base de 120 caracteres (anterior 200 caracteres)
  =================================================================================================================== */
AS
    V_GA_CONCEPTO         VARCHAR2(5); --  CONCEPTO Gasto Administrativo (GA)
    V_GA_COSTO            INTEGER := 25;     --  COSTP POR Gasto Administrativo (GA)
    V_APEC_CAMP           VARCHAR2(9);
    V_APEC_DEPT           VARCHAR2(9);
    V_APEC_TERM           VARCHAR2(10);
    V_COMENTARIO          VARCHAR(200);
    V_RESPUESTA           CHAR(1);
    V_ERROR               EXCEPTION;      
    V_RESULT              INTEGER;
    V_APEC_NIVEL          VARCHAR2(9) := 'UCCI';
    
    V_FECINIC             DATE;
    V_FEC_PRORRG_CARGO    DATE;
BEGIN

    -- CONVERTIR LA FECHA A UN FORMATO VALIDO
    SELECT 
          TO_DATE(P_FECINIC,'dd/mm/yyyy hh24:mi:ss'),
          TO_DATE(P_FEC_PRORRG_CARGO,'dd/mm/yyyy hh24:mi:ss')
    INTO 
          V_FECINIC,
          V_FEC_PRORRG_CARGO
    FROM DUAL;
    
    ----------------------------------------------
    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP 
    FROM CZRCAMP
    WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
    
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
    
    SELECT CZRDEPT_DEPT_BDUCCI
        INTO V_APEC_DEPT
    FROM CZRDEPT
    WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO
    
    -- CONCEPTO DEL CARGO ADMINISTRATIVO
    SELECT 'G' || SUBSTR(TRIM(P_PRORR_CONCEPTO), -2)
        INTO V_GA_CONCEPTO
    FROM DUAL;
    
    -- GET COMENTARIO (el campo de la tabla esta configurado para 200 caracteres)
    SELECT SUBSTR(TRIM(P_COMENTARIO),0,120)
        INTO V_COMENTARIO
    FROM DUAL;

    -- REGISTRAR PRORROGA Y ACTUALIZAR CTA CORRIENTE.(en caos ya tenga Gasto Administrativo)
    ---------------------------------------    
    V_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
          'dbo.sp_SetProrrogaCuota "'
          || P_APEC_IDALUMNO ||'" , "'|| V_APEC_CAMP || '" , "' || V_APEC_NIVEL || '" , "' || V_APEC_TERM ||'" , "'|| P_PRORR_SECCIONC ||'" , "'|| P_PRORR_CONCEPTO 
          || '" , "'|| P_PROGRAM ||'" , "'|| V_GA_CONCEPTO ||'" , "'|| '25' ||'" , "'|| V_APEC_DEPT ||'" , "'|| TO_CHAR(V_FECINIC, 'dd/mm/yyyy') 
          ||'" , "'|| TO_CHAR(V_FEC_PRORRG_CARGO, 'dd/mm/yyyy') || '" , "'|| 'WorkFlow'||'" , "'|| V_COMENTARIO ||'" '
    );
   
    COMMIT;
    
EXCEPTION
WHEN V_ERROR THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontró una CTA CORRIENTE para el alumno.' );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SETPRORROGA_GA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SIDEUDA_CUOTA (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_APEC_IDALUMNO       IN VARCHAR2,
      P_PROGRAM             IN SORLCUR.SORLCUR_PROGRAM%TYPE,
      P_PRORR_CONCEPTO      IN VARCHAR2, -- APEC_CONCEPTO
      P_PRORR_SECCIONC      IN VARCHAR2, -- APEC_SECCIONC
      P_FECINIC             IN VARCHAR2, -- APEC_CTA_FECINI 
      P_DEUDA               OUT VARCHAR2,
      P_MESSAGE             OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SIDEUDA_CUOTA
  FECHA     : 12/07/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : - Verifica la existencia de deuda para un APEC_IDAlumno Y CONCEPTO DETERMINADO
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
      V_DEUDA               NUMBER;
      V_ERROR               EXCEPTION;      
      V_APEC_CAMP           VARCHAR2(9);
      V_APEC_DEPT           VARCHAR2(9);
      V_APEC_TERM           VARCHAR2(10);
      
      V_FECINIC             DATE;
BEGIN
    
    -- CONVERTIR LA FECHA A UN FORMATO VALIDO
    --    SELECT  TO_DATE(P_FECINIC,'dd/mm/yyyy hh24:mi:ss')
    --    INTO    V_FECINIC
    --    FROM DUAL;
    
    SELECT CZRCAMP_CAMP_BDUCCI
        INTO V_APEC_CAMP
    FROM CZRCAMP
    WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
    
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
    
    SELECT CZRDEPT_DEPT_BDUCCI
        INTO V_APEC_DEPT
    FROM CZRDEPT 
    WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO
    
    SELECT "FecInic"
        INTO V_FECINIC
    FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
    WHERE "IDDependencia" = 'UCCI'
    AND "IDAlumno"    = P_APEC_IDALUMNO
    AND "IDSede"      = V_APEC_CAMP
    AND "IDPerAcad"   = V_APEC_TERM
    AND "IDEscuela"   = P_PROGRAM
    AND "IDSeccionC"  = P_PRORR_SECCIONC
    AND "IDConcepto"  = P_PRORR_CONCEPTO;
    
    ----------------------------------------------
    SELECT "Deuda"
        INTO V_DEUDA
    FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
    WHERE "IDDependencia" = 'UCCI'
    AND "IDAlumno"    = P_APEC_IDALUMNO
    AND "IDSede"      = V_APEC_CAMP
    AND "IDPerAcad"   = V_APEC_TERM
    AND "IDEscuela"   = P_PROGRAM
    AND "IDSeccionC"  = P_PRORR_SECCIONC
    AND "FecInic"     = V_FECINIC --TO_CHAR(V_FECINIC, 'dd/mm/yyyy')
    AND "IDConcepto"  = P_PRORR_CONCEPTO;
    
    IF V_DEUDA > 0 THEN
        P_DEUDA   := 'TRUE';
    ELSE
        P_DEUDA   := 'FALSE';
    END IF;
    
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SIDEUDA_CUOTA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CALC_DIAS (
    P_FEC_PRORRG_CARGO    IN VARCHAR2, -- FECHA a cual se calcula los dias restantes(SYSDATE).
    P_CODIGO              OUT NUMBER
)
/* ===================================================================================================================
  NOMBRE    : P_SIFECHA_NOTIFICAR
  FECHA     : 12/07/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Calcula los dias restante a una fecha. Devuelve 
                      0- Si estamos a mas de 2 de la fecha limite de la prorroga
                      1- Si estamos a 1 o a cero dias de la fecha limite de la prorroga
                      2- Si ya estamos en la fecha o posterior a la fecha limite de la prorroga
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
    V_INDICADOR         NUMBER;
    V_FEC_PRORRG_CARGO  DATE;
BEGIN

    -- CONVERTIR LA FECHA A UN FORMATO VALIDO
    SELECT TO_DATE(P_FEC_PRORRG_CARGO,'dd/mm/yyyy hh24:mi:ss')
        INTO V_FEC_PRORRG_CARGO
    FROM DUAL;
    
    V_INDICADOR := V_FEC_PRORRG_CARGO - TO_DATE(TO_CHAR(SYSDATE(), 'dd/mm/yyyy'),'dd/mm/yyyy');
    
    P_CODIGO := CASE WHEN V_INDICADOR <= 0 THEN 2
                          WHEN V_INDICADOR = 1 OR V_INDICADOR = 2 THEN 1
                          ELSE 0 END;
    
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CALC_DIAS;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END BWZKCSPS;

  /**********************************************************************************************/
  --/
  --show errors
  --
  --SET SCAN ON
  /**********************************************************************************************/