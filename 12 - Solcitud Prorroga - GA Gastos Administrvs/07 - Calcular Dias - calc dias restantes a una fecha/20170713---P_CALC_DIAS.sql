/* 
SET SERVEROUTPUT ON
declare P_CODIGO         NUMBER;
begin
    P_CALC_DIAS('22-Jul-2017 00:00:00',P_CODIGO);
    DBMS_OUTPUT.PUT_LINE(P_CODIGO);
end;
*/

CREATE OR REPLACE PROCEDURE P_CALC_DIAS (
      P_FEC_PRORRG_CARGO    IN VARCHAR2, -- FECHA a cual se calcula los dias restantes(SYSDATE).
      P_CODIGO              OUT NUMBER
)
/* ===================================================================================================================
  NOMBRE    : P_SIFECHA_NOTIFICAR
  FECHA     : 12/07/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Calcula los dias restante a una fecha. Devuelve 
                      0- Si estamos a mas de 2 de la fecha limite de la prórroga
                      1- Si estamos a 1 ó a cero dias de la fecha limite de la prórroga
                      2- Si ya estamos en la fecha ó posterior a la fecha limite de la prórroga
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
      V_INDICADOR         NUMBER;
      V_FEC_PRORRG_CARGO  DATE;

BEGIN
    
    -- CONVERTIR LA FECHA A UN FORMATO VALIDO
    SELECT  TO_DATE(P_FEC_PRORRG_CARGO,'dd/mm/yyyy hh24:mi:ss')
    INTO    V_FEC_PRORRG_CARGO
    FROM DUAL;
    
    V_INDICADOR := V_FEC_PRORRG_CARGO - TO_DATE(TO_CHAR(SYSDATE(), 'dd/mm/yyyy'),'dd/mm/yyyy');
    
    P_CODIGO := CASE WHEN V_INDICADOR <= 0 THEN 2
                          WHEN V_INDICADOR = 1 OR V_INDICADOR = 2 THEN 1
                          ELSE 0 END;
    
EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CALC_DIAS;
