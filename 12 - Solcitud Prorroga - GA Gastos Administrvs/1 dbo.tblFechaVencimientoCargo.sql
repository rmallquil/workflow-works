---------------------------------------------------------------
--------------------------- CICLO REGULAR ---------------------------
---------------------------------------------------------------

INSERT INTO dbo.tblFechaVencimientoCargo (FecCargo,IDPerAcad,IDConcepto,IDCargo,Modalidad)
select distinct CONVERT(VARCHAR(12), FecCargo+1,103),IDPerAcad,IDConcepto,
CASE 
	WHEN IDConcepto='C02' THEN 'GA2'
	WHEN IDConcepto='C03' THEN 'GA3'
	WHEN IDConcepto='C04' THEN 'GA4'
	WHEN IDConcepto='C05' THEN 'GA5'
	END IDCargo,
'UREG'from dbo.tblCtaCorriente
where 
RIGHT(IDSeccionC,2) IN ('01','02','16')
and IDDependencia='UCCI'
And IDPerAcad='2017-2'
AND IDConcepto IN ('C02','C03','C04','C05')
AND IDESCUELA IN (SELECT IDESCUELA FROM DBO.TBLESCUELA WHERE IDTIPOESC ='CAR')
---------------------------------------------------------------
--------------------------- VIRTUAL ---------------------------
---------------------------------------------------------------
INSERT INTO dbo.tblFechaVencimientoCargo (FecCargo,IDPerAcad,IDConcepto,IDCargo,Modalidad)
select distinct CONVERT(VARCHAR(12), FecCargo+1,103),IDPerAcad,IDConcepto,
CASE 
	WHEN IDConcepto='C02' THEN 'GA2'
	WHEN IDConcepto='C03' THEN 'GA3'
	WHEN IDConcepto='C04' THEN 'GA4'
	WHEN IDConcepto='C05' THEN 'GA5'
	END IDCargo,
'UVIR'from dbo.tblCtaCorriente
where 
RIGHT(IDSeccionC,2) IN ('08')
and IDDependencia='UCCI'
And IDPerAcad='2017-2'
AND IDConcepto IN ('C02','C03','C04','C05')
AND IDESCUELA IN (SELECT IDESCUELA FROM DBO.TBLESCUELA WHERE IDTIPOESC ='CAR')
---------------------------------------------------------------
---------------------- GENTE QUE TRABAJA ----------------------
---------------------------------------------------------------
INSERT INTO dbo.tblFechaVencimientoCargo (FecCargo,IDPerAcad,IDConcepto,IDCargo,Modalidad)
select distinct CONVERT(VARCHAR(12), FecCargo+1,103),IDPerAcad,IDConcepto,
CASE 
	WHEN IDConcepto='C02' THEN 'GA2'
	WHEN IDConcepto='C03' THEN 'GA3'
	WHEN IDConcepto='C04' THEN 'GA4'
	WHEN IDConcepto='C05' THEN 'GA5'
	END IDCargo,
'UPGT'from dbo.tblCtaCorriente
where 
RIGHT(IDSeccionC,2) IN ('03','10','14')
and IDDependencia='UCCI'
And IDPerAcad='2017-2'
AND IDConcepto IN ('C02','C03','C04','C05')
AND IDESCUELA IN (SELECT IDESCUELA FROM DBO.TBLESCUELA WHERE IDTIPOESC ='CAR')

INSERT INTO dbo.tblFechaVencimientoCargo VALUES 
('16/09/2017','2017-2','C02','GA2','UREG'),
('16/10/2017','2017-2','C03','GA3','UREG'),
('16/11/2017','2017-2','C04','GA4','UREG'),
---------------------------------------------
('16/09/2017','2017-2','C02','GA2','UPGT'),
('16/10/2017','2017-2','C02','GA2','UPGT'),
----------------------------------------------
('16/09/2017','2017-2','C02','GA2','UVIR'),
('16/10/2017','2017-2','C03','GA3','UVIR')