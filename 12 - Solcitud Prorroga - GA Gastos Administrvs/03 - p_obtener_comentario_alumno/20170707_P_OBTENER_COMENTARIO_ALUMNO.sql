/*
SET SERVEROUTPUT ON
declare P_COMENTARIO_ALUMNO       VARCHAR2(4000);
begin
    P_OBTENER_COMENTARIO_ALUMNO(4086,P_COMENTARIO_ALUMNO);
    DBMS_OUTPUT.PUT_LINE(P_COMENTARIO_ALUMNO);
end;
*/

CREATE or replace PROCEDURE P_OBTENER_COMENTARIO_ALUMNO (
      P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_COMENTARIO          OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_OBTENER_COMENTARIO_ALUMNO
  FECHA     : 07/'07/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene EL COMENTARIO como unico dato ingresado por el estudiante.
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
      V_ERROR                   EXCEPTION;
      V_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
      V_COMENTARIO              VARCHAR2(4000);
      V_CLAVE                   VARCHAR2(4000);
      V_ADDL_DATA_SEQ           SVRSVAD.SVRSVAD_ADDL_DATA_SEQ%TYPE;
      
      -- GET COMENTARIO Y TELEFONO de la solicitud. (comentario PERSONALZIADO.)
      CURSOR C_SVRSVAD IS
        SELECT SVRSVAD_ADDL_DATA_SEQ, SVRSVAD_ADDL_DATA_CDE, SVRSVAD_ADDL_DATA_DESC
        FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
        INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
        ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
        WHERE SVRSRAD_SRVC_CODE = V_CODIGO_SOLICITUD
        --AND SVRSVAD_ADDL_DATA_CDE = 'PREGUNTA'
        AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
        ORDER BY SVRSVAD_ADDL_DATA_SEQ;
BEGIN
      
      -- GET CODIGO SOLICITUD
      SELECT SVRSVPR_SRVC_CODE 
      INTO V_CODIGO_SOLICITUD 
      FROM SVRSVPR 
      WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
      
      -- OBTENER COMENTARIO Y EL TELEFONO DE LA SOLICITUD (LOS DOS ULTIMOS DATOS)
      OPEN C_SVRSVAD;
        LOOP
          FETCH C_SVRSVAD INTO V_ADDL_DATA_SEQ, V_CLAVE, V_COMENTARIO;
          IF C_SVRSVAD%FOUND THEN
              IF V_ADDL_DATA_SEQ = 1 THEN
                  -- OBTENER COMENTARIO DE LA SOLICITUD
                  P_COMENTARIO := V_COMENTARIO;
              END IF;
          ELSE EXIT;
        END IF;
        END LOOP;
      CLOSE C_SVRSVAD;

      IF P_COMENTARIO IS NULL THEN
          RAISE V_ERROR;
      END IF;
      
EXCEPTION
  WHEN V_ERROR THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontró el comentario y/o el teléfono.' );
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_OBTENER_COMENTARIO_ALUMNO;
