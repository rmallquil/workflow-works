/* 
SET SERVEROUTPUT ON
declare P_APEC_IDALUMNO     VARCHAR2(4000);
      P_PROGRAM             SORLCUR.SORLCUR_PROGRAM%TYPE;
      P_PRORR_CONCEPTO      VARCHAR2(4000);
      P_PRORR_SECCIONC      VARCHAR2(4000);
      P_FECCARGO            DATE;
      P_FECINIC             DATE;
      P_FEC_PRORRG_CARGO    DATE; -- REGLA: LA FECHA LIMITE DE LA PRORROGA PARA EL PAGO DE LA CUOTA ES EL 15/MM/YYYY
begin
    P_GETDATAX_CARGO(263159,'UPGT','F01','201710',4051,P_APEC_IDALUMNO,P_PROGRAM,P_PRORR_CONCEPTO,P_PRORR_SECCIONC,P_FECCARGO,P_FECINIC,P_FEC_PRORRG_CARGO);
    DBMS_OUTPUT.PUT_LINE(P_APEC_IDALUMNO);
    DBMS_OUTPUT.PUT_LINE(P_PROGRAM);
    DBMS_OUTPUT.PUT_LINE(P_PRORR_CONCEPTO);
    DBMS_OUTPUT.PUT_LINE(P_PRORR_SECCIONC);
    DBMS_OUTPUT.PUT_LINE(P_FECCARGO);
    DBMS_OUTPUT.PUT_LINE(P_FECINIC);
    DBMS_OUTPUT.PUT_LINE(P_FEC_PRORRG_CARGO);
end;
*/


CREATE OR REPLACE PROCEDURE P_GETDATAX_CARGO (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_APEC_IDALUMNO       OUT VARCHAR2,
      P_PROGRAM             OUT SORLCUR.SORLCUR_PROGRAM%TYPE,
      P_PRORR_CONCEPTO      OUT VARCHAR2,
      P_PRORR_SECCIONC      OUT VARCHAR2,
      P_FECCARGO            OUT DATE,
      P_FECINIC             OUT DATE,
      P_FEC_PRORRG_CARGO    OUT DATE -- REGLA: LA FECHA LIMITE DE LA PRORROGA PARA EL PAGO DE LA CUOTA ES EL 15/MM/YYYY
)
/* ===================================================================================================================
  NOMBRE    : P_GETDATAX_CARGO
  FECHA     : 07/07/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene los datos de la CTA alumno (conceptos) PARA realizar la prorroga.
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
      V_ERROR               EXCEPTION;      
      V_APEC_IDALUMNO       VARCHAR2(20);
      V_APEC_CAMP           VARCHAR2(9);
      V_APEC_TERM           VARCHAR2(9);
      V_IDSECCIONC          VARCHAR2(18);
      V_FECINIC             DATE;
      V_CONCEPTO            VARCHAR2(9);
      V_FECCARGO            DATE;
      
      V_FEC_PRORRG_INI      DATE;
      V_FEC_PRORRG_FIN      DATE;
      
      V_PROGRAM             SORLCUR.SORLCUR_PROGRAM%TYPE;
      V_RECEPTION_DATE      SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE;
      V_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_PTRM                SOBPTRM.SOBPTRM_PTRM_CODE%TYPE;
      
     -- CURSOR GET PROGRAMA
      CURSOR GET_DATACURR_C IS
      SELECT   SORLCUR_PROGRAM
      FROM (
              SELECT    SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_PROGRAM 
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE =   'Y'
              ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM = 1;
      
      -- Calculando parte PERIODO (sub PTRM --> SFRRSTS)
      CURSOR GET_PTRM_C IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;
      
      -- OBTENER PERIODO ACTIVO (SOATERM) inicio a fin
      CURSOR SOBPTRM_C IS
      SELECT SOBPTRM_PTRM_CODE FROM
      (
        SELECT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE, MIN(SOBPTRM_START_DATE) SOBPTRM_START_DATE, MAX(SOBPTRM_END_DATE) SOBPTRM_END_DATE
        FROM SOBPTRM
        WHERE SOBPTRM_PTRM_CODE IN (V_SUB_PTRM||'1' , (V_SUB_PTRM || CASE WHEN (P_DEPT_CODE ='UVIR' OR P_DEPT_CODE ='UPGT') THEN '2' ELSE '-' END) ) 
        AND SOBPTRM_PTRM_CODE NOT LIKE '%0'
        GROUP BY SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE
      ) WHERE ROWNUM = 1
      AND TO_DATE(TO_CHAR(V_RECEPTION_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SOBPTRM_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SOBPTRM_END_DATE
      ORDER BY SOBPTRM_TERM_CODE DESC;  
      
      -- GET FECHAS DE cARGO Y los conceptos validos para las ProrrogaS
        -- UREG : C02,C03,C04
        -- UPGT : C02,C03
      CURSOR GET_FCARGO_C IS
      SELECT IDAlumno, IDSeccionC, FecInic, IDConcepto, FecCargo
      FROM (
          WITH 
              CTE_tblSeccionC AS (
                      -- GET SECCIONC
                      SELECT  "IDSeccionC" IDSeccionC,
                              "FecInic" FecInic
                      FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                      WHERE "IDDependencia"='UCCI'
                      AND "IDsede"    = V_APEC_CAMP
                      AND "IDPerAcad" = V_APEC_TERM
                      AND "IDEscuela" = V_PROGRAM
                      AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                      AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                      AND "IDSeccionC" <> '15NEX1A'
                      AND SUBSTRB("IDSeccionC",-2,2) IN (
                          -- PARTE PERIODO
                          SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE = V_PTRM
                      )
              ),
              CTE_tblPersonaAlumno AS (
                      SELECT "IDAlumno" IDAlumno 
                      FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
                      WHERE "IDPersona" IN ( 
                          SELECT "IDPersona" FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM
                      )
              )
          SELECT "IDAlumno" IDAlumno, "IDSeccionC" IDSeccionC, "FecInic" FecInic, "IDConcepto" IDConcepto, "FecCargo" FecCargo
          FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia" = 'UCCI'
          AND "IDAlumno"     IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
          AND "IDSede"      = V_APEC_CAMP
          AND "IDPerAcad"   = V_APEC_TERM
          AND "IDEscuela"   = V_PROGRAM
          AND "IDSeccionC"  IN (SELECT IDSeccionC FROM CTE_tblSeccionC)
          AND "Prorroga"    = '0'
          AND "IDConcepto"  IN ('C02','C03', CASE WHEN P_DEPT_CODE = 'UREG' THEN 'C04' ELSE '-' END) 
      );
      
BEGIN
    
    -----------------------------------------------------------
    -- GET CODIGO SOLICITUD
    SELECT SVRSVPR_RECEPTION_DATE
    INTO V_RECEPTION_DATE
    FROM SVRSVPR 
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
    
    -----------------------------------------------------------
    -- >> GET PROGRAM --
    OPEN GET_DATACURR_C;
    LOOP
        FETCH GET_DATACURR_C INTO V_PROGRAM;
        IF GET_DATACURR_C%FOUND THEN
            P_PROGRAM := V_PROGRAM;
        ELSE EXIT;
        END IF;
    END LOOP;
    CLOSE GET_DATACURR_C;

    ------------------------------------------------------------
    -- >> calculando SUB PARTE PERIODO  --
    OPEN GET_PTRM_C;
    LOOP
        FETCH GET_PTRM_C INTO V_SUB_PTRM;
        EXIT WHEN GET_PTRM_C%NOTFOUND;
    END LOOP;
    CLOSE GET_PTRM_C;
    -- >> Obteniendo LA PARTE PERIODO  --
    OPEN SOBPTRM_C;
      LOOP
        FETCH SOBPTRM_C INTO V_PTRM;
        EXIT WHEN SOBPTRM_C%NOTFOUND;
      END LOOP;
    CLOSE SOBPTRM_C;

    ----------------------------------------------------------------------------------
    -- GET FECHAS DE cARGO Y los conceptos validos para las ProrrogaS
        -- UREG : C02,C03,C04
        -- UPGT : C02,C03
        SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
        SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO

        OPEN GET_FCARGO_C;
            LOOP 
                FETCH GET_FCARGO_C INTO V_APEC_IDALUMNO, V_IDSECCIONC, V_FECINIC, V_CONCEPTO, V_FECCARGO;
                IF GET_FCARGO_C%FOUND THEN
                    ------------------ LAS REGLAS PARA LOS DEPARTAMENTOS YA SE FILTRARON EN LA CONSULTA -----------------
                    -- En la regla de caja y bienestar la fecha de vencimiento de pago es el 4 de cada mes a una pension.
                    V_FEC_PRORRG_INI := TO_DATE(('20/' || TO_CHAR(ADD_MONTHS(V_FECCARGO, -1), 'mm/yyyy')),'dd/mm/yyyy');
                    V_FEC_PRORRG_FIN := TO_DATE(('07/' || TO_CHAR(V_FECCARGO,'mm/yyyy')),'dd/mm/yyyy') + 1;
                    IF (V_FEC_PRORRG_INI <= (V_RECEPTION_DATE) AND (V_RECEPTION_DATE) < V_FEC_PRORRG_FIN) THEN
                        P_FEC_PRORRG_CARGO := TO_DATE(('15/' || TO_CHAR(V_FECCARGO,'mm/yyyy')),'dd/mm/yyyy');
                        P_APEC_IDALUMNO    := V_APEC_IDALUMNO;
                        P_PRORR_SECCIONC   := V_IDSECCIONC;
                        P_PRORR_CONCEPTO   := V_CONCEPTO;
                        P_FECCARGO         := V_FECCARGO;
                        P_FECINIC          := V_FECINIC;
                    END IF;
                    ------------------------------------------------------------
                ELSE EXIT;
            END IF;
            END LOOP;
        CLOSE GET_FCARGO_C;
        
        IF(P_APEC_IDALUMNO IS NULL OR P_PRORR_SECCIONC IS NULL  OR P_PRORR_CONCEPTO IS NULL) THEN
              RAISE V_ERROR;
        END IF;
EXCEPTION
  WHEN V_ERROR THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontro la "Cuenta corriente" o una "cuota válida" para la prórroga.' );
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GETDATAX_CARGO;