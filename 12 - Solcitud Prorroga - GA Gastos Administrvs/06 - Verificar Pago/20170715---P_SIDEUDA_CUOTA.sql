/* 
SET SERVEROUTPUT ON
declare P_DEUDA  VARCHAR2(10);
      P_MESSAGE  VARCHAR2(4000);
begin
    P_SIDEUDA_CUOTA(263159,'UPGT','S01','201710','09762075','109','C03','ELE03','22-Apr-2017 00:00:00',P_DEUDA,P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_DEUDA);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
end;
*/

CREATE OR REPLACE PROCEDURE P_SIDEUDA_CUOTA (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_APEC_IDALUMNO       IN VARCHAR2,
      P_PROGRAM             IN SORLCUR.SORLCUR_PROGRAM%TYPE,
      P_PRORR_CONCEPTO      IN VARCHAR2, -- APEC_CONCEPTO
      P_PRORR_SECCIONC      IN VARCHAR2, -- APEC_SECCIONC
      P_FECINIC             IN VARCHAR2, -- APEC_CTA_FECINI 
      P_DEUDA               OUT VARCHAR2,
      P_MESSAGE             OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SIDEUDA_CUOTA
  FECHA     : 12/07/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : - Verifica la existencia de deuda para un APEC_IDAlumno Y CONCEPTO DETERMINADO
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
      V_DEUDA               NUMBER;
      V_ERROR               EXCEPTION;      
      V_APEC_CAMP           VARCHAR2(9);
      V_APEC_DEPT           VARCHAR2(9);
      V_APEC_TERM           VARCHAR2(10);
      
      V_FECINIC             DATE;
BEGIN
    
    -- CONVERTIR LA FECHA A UN FORMATO VALIDO
--    SELECT  TO_DATE(P_FECINIC,'dd/mm/yyyy hh24:mi:ss')
--    INTO    V_FECINIC
--    FROM DUAL;
    
    SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
    SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
    SELECT CZRDEPT_DEPT_BDUCCI INTO V_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO
    
    
    SELECT "FecInic" INTO V_FECINIC
    FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
    WHERE "IDDependencia" = 'UCCI'
    AND "IDAlumno"    = P_APEC_IDALUMNO
    AND "IDSede"      = V_APEC_CAMP
    AND "IDPerAcad"   = V_APEC_TERM
    AND "IDEscuela"   = P_PROGRAM
    AND "IDSeccionC"  = P_PRORR_SECCIONC
    AND "IDConcepto"  = P_PRORR_CONCEPTO;
    
    ----------------------------------------------
    SELECT "Deuda" INTO V_DEUDA
    FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
    WHERE "IDDependencia" = 'UCCI'
    AND "IDAlumno"    = P_APEC_IDALUMNO
    AND "IDSede"      = V_APEC_CAMP
    AND "IDPerAcad"   = V_APEC_TERM
    AND "IDEscuela"   = P_PROGRAM
    AND "IDSeccionC"  = P_PRORR_SECCIONC
    AND "FecInic"     = V_FECINIC --TO_CHAR(V_FECINIC, 'dd/mm/yyyy')
    AND "IDConcepto"  = P_PRORR_CONCEPTO;
    
    IF V_DEUDA > 0 THEN
      P_DEUDA   := 'TRUE';
    ELSE
      P_DEUDA   := 'FALSE';
    END IF;
    
EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SIDEUDA_CUOTA;
