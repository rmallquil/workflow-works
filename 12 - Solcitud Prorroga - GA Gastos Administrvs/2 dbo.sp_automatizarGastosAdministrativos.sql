USE [BDUCCI]
GO
/****** Object:  StoredProcedure [dbo].[sp_automatizarGastosAdministrativos]    Script Date: 24/07/2017 16:49:03 ******/
/* ===================================================================================================================
NOMBRE      : [dbo].[sp_automatizarGastosAdministrativos]
FECHA       : 
AUTOR       : Franklin Huaman 
OBJETIVO    : Gestiona la actualizaci�n de concepto de cargos administrativos, seg�n modalidad y fecha de vencimiento

MODIFICACIONES
NRO     FECHA			USUARIO		MODIFICACION
001		24/07/2017		FHUAMANA	Se comenta los print's
=================================================================================================================== */
ALTER PROCEDURE [dbo].[sp_automatizarGastosAdministrativos]
AS
DECLARE
@c_hoy VARCHAR(12),
@c_term_code VARCHAR(6),
@c_IDConcepto VARCHAR(5),
@c_IDCargo VARCHAR(6),
@c_Modalidad VARCHAR(4)

set @c_hoy = CONVERT(varchar,GETDATE(),103)

--SELECT * FROM dbo.tblFechaVencimientoCargo  WHERE fecCargo=@c_hoy AND modalidad='UREG'

IF EXISTS (SELECT * FROM dbo.tblFechaVencimientoCargo  WHERE fecCargo=@c_hoy AND modalidad='UREG')
	BEGIN
		SELECT TOP 1
			@c_term_code  = IDPerAcad,
			@c_IDConcepto  = IDConcepto,
			@c_IDCargo  = IDCargo,
			@c_Modalidad  = Modalidad
			FROM dbo.tblFechaVencimientoCargo 
			WHERE  
				fecCargo=@c_hoy
				AND modalidad='UREG'
--		print concat(@c_term_code,'-', @c_IDConcepto,'-', @c_IDCargo,'-', @c_Modalidad)
		exec dbo.sp_updateCargoAdministrativo @c_term_code,@c_IDConcepto,@c_IDCargo,@c_Modalidad
		
	END

IF EXISTS (SELECT * FROM dbo.tblFechaVencimientoCargo  WHERE   fecCargo=@c_hoy AND modalidad='UPGT')
	BEGIN
		SELECT 
			@c_term_code  = IDPerAcad,
			@c_IDConcepto  = IDConcepto,
			@c_IDCargo  = IDCargo,
			@c_Modalidad  = Modalidad
			FROM dbo.tblFechaVencimientoCargo 
			WHERE  
				fecCargo=@c_hoy
				AND modalidad='UPGT'
--		print concat(@c_term_code,'-', @c_IDConcepto,'-', @c_IDCargo,'-', @c_Modalidad)
		exec dbo.sp_updateCargoAdministrativo @c_term_code,@c_IDConcepto,@c_IDCargo,@c_Modalidad
		
	END

IF EXISTS (SELECT * FROM dbo.tblFechaVencimientoCargo  WHERE   fecCargo=@c_hoy AND modalidad='UVIR')
	BEGIN
		SELECT 
			@c_term_code  = IDPerAcad,
			@c_IDConcepto  = IDConcepto,
			@c_IDCargo  = IDCargo,
			@c_Modalidad  = Modalidad
			FROM dbo.tblFechaVencimientoCargo 
			WHERE  
				fecCargo=@c_hoy
				AND modalidad='UVIR'
--		print concat(@c_term_code,'-', @c_IDConcepto,'-', @c_IDCargo,'-', @c_Modalidad)
		exec dbo.sp_updateCargoAdministrativo @c_term_code,@c_IDConcepto,@c_IDCargo,@c_Modalidad
	END