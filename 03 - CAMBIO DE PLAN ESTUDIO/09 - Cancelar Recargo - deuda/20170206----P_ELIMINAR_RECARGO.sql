/*
set serveroutput on
DECLARE p_error varchar2(64);
begin
  P_ELIMINAR_RECARGO(241106,'111','201620','CPLA',p_error);
  DBMS_OUTPUT.PUT_LINE(p_error);
end;
*/

CREATE OR REPLACE PROCEDURE P_ELIMINAR_RECARGO (
        P_PIDM_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_NUMERO_SOLICITUD      IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_TERM_PERIODO          IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE, ------ APEC
        P_CODIGO_DETALLE        IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE, ------ APEC
        P_ERROR                 OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_ELIMINAR_RECARGO
  FECHA     : 19/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Cancela una transaccion(DEUDA) generado por una solicitud.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  001   06/02/2017    RMALLQUI    Se comento la linea para la generacion de deuda para el periodo 2017-1 APEC
  =================================================================================================================== */
AS    
        P_TRAN_NUMBER           TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
        P_DNI_ALUMNO            SPRIDEN.SPRIDEN_ID%TYPE;
        P_INDICADOR             NUMBER;
        P_MESSAGE               EXCEPTION;
        P_FOUND_AMOUNT          TBRACCD.TBRACCD_AMOUNT%TYPE;
        P_COD_DETALLE           TBRACCD.TBRACCD_DETAIL_CODE%TYPE;
        P_NOM_SERVICIO          VARCHAR2(30);
        P_PERIODO               SFBETRM.SFBETRM_TERM_CODE%TYPE;
        P_TRAN_NUMBER_OUT       TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
        P_ROWID                 GB_COMMON.INTERNAL_RECORD_ID_TYPE;
        
        -- APEC PARAMS
        P_SERVICE               VARCHAR2(10);
        P_PART_PERIODO          VARCHAR2(9);
        P_APEC_CAMP             VARCHAR2(9);
        P_APEC_DEPT             VARCHAR2(9);
        P_APEC_TERM             VARCHAR2(10);
        P_APEC_IDSECCIONC       VARCHAR2(15);
        P_APEC_FECINIC          DATE;
        P_APEC_MOUNT          NUMBER;
        P_APEC_IDALUMNO       VARCHAR2(10);
        P_APEC_CONCEPTO           TBRACCD.TBRACCD_DETAIL_CODE%TYPE;
        C_SEQNO                   SORLCUR.SORLCUR_SEQNO %type;
        C_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;
        C_ALUM_CAMPUS             SORLCUR.SORLCUR_CAMP_CODE%type;
        C_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
        C_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
        C_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
        C_ALUM_PERD_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
        C_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP
        C_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE
        C_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%type;
        
BEGIN
--
    
      --#################################----- APEC -----#######################################
      --########################################################################################
      -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
      IF PKG_GLOBAL.GET_VAL = 0 THEN -- PRIORIDAD 2 ==> GENERAR DEUDA

          SELECT  CASE P_CODIGO_DETALLE WHEN 'CPLA' THEN 'CPE' 
                  ELSE P_CODIGO_DETALLE END
          INTO P_APEC_CONCEPTO
          FROM DUAL;
          
          -- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
          SELECT    SORLCUR_SEQNO,      
                    SORLCUR_LEVL_CODE,    
                    SORLCUR_CAMP_CODE,        
                    SORLCUR_COLL_CODE,
                    SORLCUR_DEGC_CODE,  
                    SORLCUR_PROGRAM,      
                    SORLCUR_TERM_CODE_ADMIT,  
                    --SORLCUR_STYP_CODE,
                    SORLCUR_STYP_CODE,  
                    SORLCUR_RATE_CODE,    
                    SORLFOS_DEPT_CODE   
          INTO      C_SEQNO,
                    C_ALUM_NIVEL, 
                    C_ALUM_CAMPUS, 
                    C_ALUM_ESCUELA, 
                    C_ALUM_GRADO, 
                    C_ALUM_PROGRAMA, 
                    C_ALUM_PERD_ADM,
                    C_ALUM_TIPO_ALUM,
                    C_ALUM_TARIFA,
                    C_ALUM_DEPARTAMENTO
          FROM (
                  SELECT    SORLCUR_SEQNO,      SORLCUR_LEVL_CODE,    SORLCUR_CAMP_CODE,        SORLCUR_COLL_CODE,
                            SORLCUR_DEGC_CODE,  SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  --SORLCUR_STYP_CODE,
                            SORLCUR_STYP_CODE,  SORLCUR_RATE_CODE,    SORLFOS_DEPT_CODE
                  FROM SORLCUR        INNER JOIN SORLFOS
                        ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                        AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
                  WHERE   SORLCUR_PIDM        =   P_PIDM_ALUMNO 
                      AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                      -- AND SORLCUR_TERM_CODE   =   P_TERM_PERIODO 
                      AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                      AND SORLCUR_CURRENT_CDE = 'Y'
                  ORDER BY SORLCUR_SEQNO DESC 
          ) WHERE ROWNUM <= 1;
          
          -- GET CRONOGRAMA SECCIONC
          SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                            WHEN 'UPGT' THEN 'W' 
                                            WHEN 'UREG' THEN 'R' 
                                            WHEN 'UPOS' THEN '-' 
                                            WHEN 'ITEC' THEN '-' 
                                            WHEN 'UCIC' THEN '-' 
                                            WHEN 'UCEC' THEN '-' 
                                            WHEN 'ICEC' THEN '-' 
                                            ELSE '1' END ||
                          CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                            WHEN 'F01' THEN 'A' 
                                            WHEN 'F02' THEN 'L' 
                                            WHEN 'F03' THEN 'C' 
                                            WHEN 'V00' THEN 'V' 
                                            ELSE '9' END
                  INTO P_PART_PERIODO
                  FROM STVCAMP,STVDEPT 
                  WHERE STVDEPT_CODE = C_ALUM_DEPARTAMENTO AND STVCAMP_CODE = C_ALUM_CAMPUS;
          
          SELECT CZRCAMP_CAMP_BDUCCI INTO P_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = C_ALUM_CAMPUS;-- CAMPUS 
          SELECT CZRTERM_TERM_BDUCCI INTO P_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_PERIODO ;-- PERIODO
          SELECT CZRDEPT_DEPT_BDUCCI INTO P_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = C_ALUM_DEPARTAMENTO;-- DEPARTAMENTO
          
          -- GET DATOS CTA CORRIENTE -- P_APEC_IDALUMNO, P_APEC_DEUDA
          WITH 
              CTE_tblSeccionC AS (
                      -- GET SECCIONC
                      SELECT  "IDSeccionC" IDSeccionC,
                              "FecInic" FecInic
                      FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                      WHERE "IDDependencia"='UCCI'
                      AND "IDsede"    = P_APEC_CAMP
                      AND "IDPerAcad" = P_APEC_TERM
                      AND "IDEscuela" = C_ALUM_PROGRAMA
                      AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                      AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                      AND "IDSeccionC" <> '15NEX1A'
                      AND SUBSTRB("IDSeccionC",-2,2) IN (
                          -- PARTE PERIODO           
                          SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (P_PART_PERIODO || '%')
                      )
              ),
              CTE_tblPersonaAlumno AS (
                      SELECT "IDAlumno" IDAlumno 
                      FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
                      WHERE "IDPersona" IN ( 
                          SELECT "IDPersona" FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM_ALUMNO
                      )
              )
          SELECT "IDAlumno", "IDSeccionC" INTO P_APEC_IDALUMNO, P_APEC_IDSECCIONC
          FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia" = 'UCCI'
          AND "IDAlumno"    IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
          AND "IDSede"      = P_APEC_CAMP
          AND "IDPerAcad"   = P_APEC_TERM
          AND "IDEscuela"   = C_ALUM_PROGRAMA
          AND "IDSeccionC"  IN ( SELECT IDSeccionC FROM CTE_tblSeccionC )
          AND "IDConcepto"  = P_APEC_CONCEPTO;
          
          -- GET MONTO 
          SELECT "Monto" INTO P_APEC_MOUNT
          FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia" = 'UCCI'
          AND "IDSede"      = P_APEC_CAMP
          AND "IDPerAcad"   = P_APEC_TERM
          AND "IDSeccionC"  = P_APEC_IDSECCIONC
          AND "IDConcepto"  = P_APEC_CONCEPTO;
          
          /*
          -- UPDATE MONTO(s)
          UPDATE dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
          SET   "Cargo" = "Cargo" - P_APEC_MOUNT
          WHERE "IDDependencia" = 'UCCI'
          AND "IDAlumno"    = P_APEC_IDALUMNO
          AND "IDSede"      = P_APEC_CAMP
          AND "IDPerAcad"   = P_APEC_TERM
          AND "IDEscuela"   = C_ALUM_PROGRAMA
          AND "IDSeccionC"  = P_APEC_IDSECCIONC
          AND "IDConcepto"  = P_APEC_CONCEPTO;
          */
          
          
    ELSE --**************************** 1 ---> BANNER ***************************     
    
          -- GET DNI alumno
          SELECT SPRIDEN_ID INTO P_DNI_ALUMNO FROM SPRIDEN WHERE SPRIDEN_PIDM = P_PIDM_ALUMNO AND SPRIDEN_CHANGE_IND IS NULL;
          
          -- GET NUMBER TRANS.
          SELECT SVRSVPR_ACCD_TRAN_NUMBER INTO P_TRAN_NUMBER 
          FROM SVRSVPR 
          WHERE SVRSVPR_PIDM = P_PIDM_ALUMNO AND SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD ;
          
          -- Validar si la transaccion (DEUDA) NO tubo algun movimiento.
          SELECT COUNT(*) INTO P_INDICADOR
          FROM (
                  -- : listar LOS CODIGOS QUE YA TIENEN ALGUN MOVIMIENTO. : TRAN , CHG 
                  SELECT TBRAPPL_PIDM TEMP_PIDM ,TBRAPPL_PAY_TRAN_NUMBER TEMP_PAY_TRAN_NUMBER
                  FROM TBRAPPL
                  WHERE TBRAPPL.TBRAPPL_PIDM = P_PIDM_ALUMNO
                  UNION 
                  SELECT TBRAPPL_PIDM, TBRAPPL_CHG_TRAN_NUMBER
                  FROM TBRAPPL
                  WHERE TBRAPPL.TBRAPPL_PIDM = P_PIDM_ALUMNO
          ) WHERE P_TRAN_NUMBER = TEMP_PAY_TRAN_NUMBER;
          
          IF P_INDICADOR = 1 THEN
                RAISE P_MESSAGE;
          END IF;
              
              
          -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
          TZKCDAA.p_calc_deuda_alumno(P_PIDM_ALUMNO,'PEN');
          COMMIT;
              
          -- GET , COD_DETALLE , AMOUNT(NEGATIVO) de la transaccion a cancelar  
          SELECT  (TZRCDAB_AMOUNT * (-1)), 
                  TZRCDAB_DETAIL_CODE, 
                  TZRCDAB_TERM_CODE
          INTO    P_FOUND_AMOUNT,
                  P_COD_DETALLE, 
                  P_PERIODO
          FROM   TZRCDAB
          WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
          AND TZRCDAB_PIDM = P_PIDM_ALUMNO 
          AND TZRCDAB_TRAN_NUMBER_PRIN = P_TRAN_NUMBER;
          
          -- GET DESCRIPCION del cod_detalle
          SELECT TBBDETC_DESC INTO P_NOM_SERVICIO FROM TBBDETC WHERE TBBDETC_DETAIL_CODE = P_COD_DETALLE;
      
      
          -- #######################################################################
            TB_RECEIVABLE.p_create ( p_pidm                 =>  P_PIDM_ALUMNO,      -- PIDM ALUMNO
                                     p_term_code            =>  P_PERIODO,          -- DETALLE
                                     p_detail_code          =>  P_COD_DETALLE,      -- CODIGO DETALLE  -- mismo codigo detalle
                                     p_user                 =>  USER,               -- USUARIO
                                     p_entry_date           =>  SYSDATE,     
                                     p_amount               =>  P_FOUND_AMOUNT,
                                     p_effective_date       =>  SYSDATE,
                                     p_bill_date            =>  NULL,    
                                     p_due_date             =>  NULL,    
                                     p_desc                 =>  P_NOM_SERVICIO,       -- referencia por consultar 
                                     p_receipt_number       =>  NULL,     -- numero de recibo que se muestra en la forma TVAAREV
                                     p_tran_number_paid     =>  P_TRAN_NUMBER,      -- numero de transaccion que será pagara
                                     p_crossref_pidm        =>  NULL,    
                                     p_crossref_number      =>  NULL,    
                                     p_crossref_detail_code =>  NULL,     
                                     p_srce_code            =>  'T',    -- defaul 'T', pero p_processfeeassessment crea un 'R' Modulo registro  
                                     p_acct_feed_ind        =>  'Y',
                                     p_session_number       =>  0,    
                                     p_cshr_end_date        =>  NULL,    
                                     p_crn                  =>  NULL,
                                     p_crossref_srce_code   =>  NULL,
                                     p_loc_mdt              =>  NULL,
                                     p_loc_mdt_seq          =>  NULL,    
                                     p_rate                 =>  NULL,    
                                     p_units                =>  NULL,     
                                     p_document_number      =>  NULL,    
                                     p_trans_date           =>  NULL,    
                                     p_payment_id           =>  NULL,    
                                     p_invoice_number       =>  NULL,    
                                     p_statement_date       =>  NULL,    
                                     p_inv_number_paid      =>  NULL,    
                                     p_curr_code            =>  NULL,    
                                     p_exchange_diff        =>  NULL,    
                                     p_foreign_amount       =>  NULL,    
                                     p_late_dcat_code       =>  NULL,    
                                     p_atyp_code            =>  NULL,    
                                     p_atyp_seqno           =>  NULL,    
                                     p_card_type_vr         =>  NULL,    
                                     p_card_exp_date_vr     =>  NULL,     
                                     p_card_auth_number_vr  =>  NULL,    
                                     p_crossref_dcat_code   =>  NULL,    
                                     p_orig_chg_ind         =>  NULL,    
                                     p_ccrd_code            =>  NULL,    
                                     p_merchant_id          =>  NULL,    
                                     p_data_origin          =>  'WORKFLOW',    
                                     p_override_hold        =>  'N',     
                                     p_tran_number_out      =>  P_TRAN_NUMBER_OUT, 
                                     p_rowid_out            =>  P_ROWID);
          
            ------------------------------------------------------------------------------  
          
            -- forma TVAAREV "Aplicar Transacciones" -- No necesariamente necesario.
            TZJAPOL.p_run_proc_tvrappl(P_DNI_ALUMNO);
        
            ------------------------------------------------------------------------------  
            
    END IF;
    
    
    COMMIT;
EXCEPTION
  WHEN P_MESSAGE THEN
        P_ERROR := 'A ocurrido un inconveniente  - La deuda tubo movimientos por lo que no se puede cancelar directamente.';
        --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||P_ERROR);
  WHEN OTHERS THEN
        --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
        P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_ELIMINAR_RECARGO;