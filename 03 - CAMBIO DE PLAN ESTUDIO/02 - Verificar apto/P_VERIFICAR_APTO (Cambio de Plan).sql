/*
SET SERVEROUTPUT ON
DECLARE P_STATUS_APTO VARCHAR2(4000);
 P_REASON VARCHAR2(4000);
BEGIN
    P_VERIFICAR_APTO(216, '201820', P_STATUS_APTO, P_REASON);
    DBMS_OUTPUT.PUT_LINE(P_STATUS_APTO);
    DBMS_OUTPUT.PUT_LINE(P_REASON);
END;
*/

CREATE OR REPLACE PROCEDURE P_VERIFICAR_APTO (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_STATUS_APTO     OUT VARCHAR2,
        P_REASON          OUT VARCHAR2
)
AS
        V_RET01             INTEGER := 0; --VALIDA RETENCIÓN 01
        V_RET02             INTEGER := 0; --VALIDA RETENCIÓN 02
        V_SOLTRA            INTEGER := 0; --VALIDA SOLICITUD TRASLADO INTERNO
        V_SOLRES            INTEGER := 0; --VALIDA SOLICITUD RESERVA
        V_SOLREC            INTEGER := 0; --VALIDA SOLICITUD RECTIFICACION
        V_SOLCUO            INTEGER := 0; --VALIDA SOLICITUD CAMBIO CUOTA INICIAL
        V_SOLPER            INTEGER := 0; --VALIDA SOLICITUD PERMANENCIA
        V_RET               INTEGER := 0; --VALIDA SOLICITUD RETENCIÓN 07
        V_SOLDIR            INTEGER := 0; --VALIDA SOLICITUD DIRIGIDA
        V_RESPUESTA         VARCHAR2(4000) := NULL;
        V_FLAG              INTEGER := 0;
        
        V_CODE_DEPT      STVDEPT.STVDEPT_CODE%TYPE;
        V_CODE_TERM      STVTERM.STVTERM_CODE%TYPE;
        V_CODE_CAMP      STVCAMP.STVCAMP_CODE%TYPE;
        V_PROGRAM        SORLCUR.SORLCUR_PROGRAM%TYPE;
        V_TERMCTLG       SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';
        V_TERMCTLG_MAX   SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';
        
        V_SUB_PTRM              VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
        V_PTRM_CODE             STVPTRM.STVPTRM_CODE%TYPE;
        V_MATRIC_FECHA          SFRRSTS.SFRRSTS_START_DATE%TYPE; --- Fecha de una semana antes de matricula
        
        V_PRIORIDAD             NUMBER;
        V_HAS_RETENCION         NUMBER;
        V_HAS_NRC               NUMBER;
        
    --################################################################################################
    -- OBTENER PERIODO DE CATALOGO, CAMPUS, DEPT y CARRERA
    --################################################################################################
    CURSOR C_SORLCUR IS
        SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
        FROM(
            SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
            FROM SORLCUR 
            INNER JOIN SORLFOS
                ON SORLCUR_PIDM         = SORLFOS_PIDM 
                AND SORLCUR_SEQNO       = SORLFOS_LCUR_SEQNO
            WHERE SORLCUR_PIDM          = P_PIDM 
                AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y'
                AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
        ) WHERE ROWNUM = 1;

    --################################################################################################
    -- OBTENER PARTE DE PERIODO (sub PTRM)
    --################################################################################################
    CURSOR C_SFRRSTS_PTRM IS
        SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
        FROM STVCAMP,STVDEPT 
        WHERE STVDEPT_CODE = V_CODE_DEPT 
        AND STVCAMP_CODE = V_CODE_CAMP;
        
    --################################################################################################
    /*
        GET TERM (PERIODO - Solo usando parte de periodo 1) 
        A LA FECHA QUE ESTE DENTRO DE LA FECHA DE INICIO DE MATRICULA(SFRRSTS_START_DATE) y 
        - y UNA SEMANA ANTES DE INICIO DE CLASES(SOBPTRM_START_DATE-7) -- LOS Q NO ESTUDIARON EL PERIODO ANTERIOR
        - HASTA 1 mes despues de inicio de clases (SOBPTRM_START_DATE) -- ALUMNO NORMAL 
        - Hasta 1 mes despues para calcular el PERIODO debido a las constantes ampliaciones de fechas.
    */
    --################################################################################################
    CURSOR C_STUDN_TERM IS
        SELECT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE, FECHA_MATRICULA FROM (
            SELECT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE, FECHA_MATRICULA FROM (
                -- Forma SOATERM - SFARSTS  ---> fechas para las partes de periodo
                SELECT DISTINCT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE, TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') FECHA_MATRICULA
                FROM SOBPTRM
                INNER JOIN SFRRSTS ON 
                    SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
                    AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
                WHERE SFRRSTS_RSTS_CODE = 'RW'
                    AND SOBPTRM_PTRM_CODE IN (V_SUB_PTRM || '1', CASE WHEN V_CODE_DEPT='UVIR' OR V_CODE_DEPT='UPGT' THEN (V_SUB_PTRM||'2') ELSE '-' END) 
                    AND SOBPTRM_PTRM_CODE NOT LIKE '%0'          
                    AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE-7,'dd/mm/yyyy'),'dd/mm/yyyy') AND (ADD_MONTHS(SOBPTRM_START_DATE,1))
                ORDER BY SOBPTRM_TERM_CODE 
            ) ORDER BY SOBPTRM_TERM_CODE ,SOBPTRM_PTRM_CODE 
        ) WHERE ROWNUM <= 1;

BEGIN
    ---P_VERIFICAR_APTO PARA SOLICITUD DE CAMBIO DE PLAN
    
    --################################################################################################
    -->> Obtener SEDE, DEPT, TERM CATALOGO Y PROGRAMA (CARRERA)
    --################################################################################################
    OPEN C_SORLCUR;
    LOOP
        FETCH C_SORLCUR INTO V_CODE_CAMP, V_CODE_DEPT, V_TERMCTLG, V_PROGRAM;
        EXIT WHEN C_SORLCUR%NOTFOUND;
        END LOOP;
    CLOSE C_SORLCUR;

    -->> Obtener PARTE PERIODO
    OPEN C_SFRRSTS_PTRM;
    LOOP
        FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
        EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
    END LOOP;
    CLOSE C_SFRRSTS_PTRM;
    
    --################################################################################################
    --- REVISIÓN DE PLAN DE ESTUDIOS
    --################################################################################################
    IF (V_TERMCTLG >= V_TERMCTLG_MAX) THEN
        V_RESPUESTA := '<br />' || '<br />' || '- Se encuentra en un plan de estudios vigente. ';
        V_FLAG := 1;
    END IF; 

    --################################################################################################
    --- VALIDACIÓN DE ESTUDIANTES DE PROGRAMA 309 (Administración - MKT y Neg. Internacionales)
    --################################################################################################
    IF V_PROGRAM = '309' THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Se encuentra en la carrera de Administración - Marketing y Neg. Internacionales. No esta habilitado para este flujo. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Se encuentra en la carrera de Administración - Marketing y Neg. Internacionales. No esta habilitado para este flujo. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    --- VALIDACIÓN DE ESTUDIANTES DE PROGRAMA 308 (Administración)
    --################################################################################################
    IF V_PROGRAM = '308' THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Se encuentra en la carrera de Administración. No esta habilitado para este flujo. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Se encuentra en la carrera de Administración. No esta habilitado para este flujo. ';            
        END IF;
        V_FLAG := 1;
    END IF; 

    --################################################################################################
    ---VALIDACIÓN DE RETENCIÓN 01 (DEUDA PENDIENTE)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET01
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '01'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
    
    IF V_RET01 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de deuda pendiente activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de deuda pendiente activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################   
   ---VALIDACIÓN DE RETENCIÓN 02 (DOCUMENTO PENDIENTE)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET02    
    FROM SPRHOLD
    WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '02'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RET02 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de documento pendiente activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de documento pendiente activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;
    
    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE TRASLADO INTERNO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLTRA
    FROM SVRSVPR
    WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL013'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
    
    IF V_SOLTRA > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RESERVA DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLRES
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL005'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLRES > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RECTIFICACIÓN DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLREC
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL003'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLREC > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE CAMBIO DE CUOTA INICIAL ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLCUO
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL007'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCUO > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Cambio de cuota inicial activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Cambio de cuota inicial activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE PERMANENCIA DE PLAN DE ESTUDIOS ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLPER
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL015'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLPER > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Permanencia de Plan de Estudio activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Permanencia de Plan de Estudio activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE DIRIGIDO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLDIR
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL014'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLDIR > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

--    ---GET TERM (PERIODO - Solo usando parte de periodo 1) */
--      OPEN C_STUDN_TERM;
--      LOOP
--        FETCH C_STUDN_TERM INTO V_CODE_TERM, V_PTRM_CODE, V_MATRIC_FECHA;
--        EXIT WHEN C_STUDN_TERM%NOTFOUND;
--        END LOOP;
--      CLOSE C_STUDN_TERM;
      
--    /******************************************************************************/
--    -- PARA SEGUNDO MODULO: Verificar que no tenga NRC en el primer modulo
--    IF (V_PTRM_CODE = (V_SUB_PTRM||'2')) THEN
--        SELECT COUNT(*)
--        INTO V_HAS_NRC
--        FROM SFRSTCR  
--        WHERE SFRSTCR_TERM_CODE = P_TERM_CODE
--            AND SFRSTCR_PIDM = P_PIDM
--            AND SFRSTCR_PTRM_CODE = (V_SUB_PTRM||'1');
--        
--        IF  (V_HAS_NRC > 0 OR  -- No haber estudiado en el primer modulo
--            SYSDATE < V_MATRIC_FECHA) -- En segundo modulo: desde inicio de matricula <-> inicio de clases 
--            THEN
--                P_STATUS_APTO := 'FALSE';
--                P_REASON := 'El estudiante tiene matricula';
--                RETURN;
--        END IF;
--    -- PARA PRIMER MODULO O UN CASO NORMAL: verificar si estudió el periodo anterior.
--    ELSE
--        IF (SYSDATE < V_MATRIC_FECHA) -- De haber estudiado el periodo anterior: matricula <-> inicio de clases
--        THEN
--            P_STATUS_APTO := 'FALSE';
--            P_REASON := 'No esta en fecha de matricula';
--            RETURN;
--        END IF;
--    END IF;

    --################################################################################################
    ---VALIDACIÓN DE RETENCION 07 ACTIVA
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '07'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RET <= 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- No tiene activo la retención de Cambio de Plan de Estudio. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- No tiene activo la retención de Cambio de Plan de Estudio. ';
        END IF;
        V_FLAG := 1;
    END IF;
    
    --################################################################################################
    ---VALIDACIÓN FINAL
    --################################################################################################
    IF V_FLAG > 0 THEN
        P_STATUS_APTO := 'FALSE';
        P_REASON := V_RESPUESTA;
    ELSE
        P_STATUS_APTO := 'TRUE';
        P_REASON := 'OK';
    END IF;
    
EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VERIFICAR_APTO;