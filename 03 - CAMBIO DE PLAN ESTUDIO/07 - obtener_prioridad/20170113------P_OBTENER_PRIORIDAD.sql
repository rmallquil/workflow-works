/*
SET SERVEROUTPUT ON
DECLARE   P_TRAN_NUMBER           TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
          P_PRIORIDAD             NUMBER;
          P_ERROR                 VARCHAR2(400);
BEGIN
    P_OBTENER_PRIORIDAD(584,'C00','201620',P_PRIORIDAD,P_ERROR);
    DBMS_OUTPUT.PUT_LINE(P_PRIORIDAD ||' -- ' || P_ERROR);
END;
*/

CREATE OR REPLACE PROCEDURE P_OBTENER_PRIORIDAD (
          P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
          P_COD_DETALLE         IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE, ------------- APEC
          P_PERIODO             IN SFBETRM.SFBETRM_TERM_CODE%TYPE, ------------- APEC
          P_PRIORIDAD           OUT SVRRSRV.SVRRSRV_SEQ_NO%TYPE, ------------ Sequence number of the Service Rule
          P_ERROR               OUT VARCHAR2 ----------------------------------- APEC
)
/* ===================================================================================================================
  NOMBRE    : P_OBTENER_PRIORIDAD
  FECHA     : 13/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene la PRIORIDAD(Sequence number of the Service Rule) de la solicitud. 
              RETORNA "0" en caso no encuentre la solicitud. 
      APEC  : En caso de APEC, solicitudes de prioridad "2" se le generaran deuda
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
          P_INDICADOR           NUMBER;
          
          -- APEC PARAMS
          P_PIDM_ALUMNO         SPRIDEN.SPRIDEN_PIDM%TYPE;
          P_APEC_CONCEPTO       TBRACCD.TBRACCD_DETAIL_CODE%TYPE;
BEGIN
      
      SELECT COUNT(*) INTO P_INDICADOR FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
      IF P_INDICADOR < 1 THEN
      
            P_PRIORIDAD := P_INDICADOR;
            
      ELSE
            SELECT  SVRSVPR_RSRV_SEQ_NO,  -------------------------------------- Sequence number of the Service Rule.
                    SVRSVPR_PIDM
            INTO    P_PRIORIDAD, 
                    P_PIDM_ALUMNO 
            FROM SVRSVPR
            WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
            
            --#################################----- APEC -----#######################################
            --########################################################################################
            -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
            IF PKG_GLOBAL.GET_VAL = 0  THEN -- PRIORIDAD 2 ==> GENERAR DEUDA (AND P_PRIORIDAD = 2)

                SELECT  CASE P_COD_DETALLE WHEN 'CPLA' THEN 'CPE' 
                        ELSE P_COD_DETALLE END
                INTO P_APEC_CONCEPTO
                FROM DUAL;
            
                -- REALIZAR "COMMIT" despues del SP donde sea mas conveniente. --> P_APEC_SET_DEUDA
                P_APEC_SET_DEUDA(P_PIDM_ALUMNO,P_APEC_CONCEPTO,P_PERIODO, P_ERROR);    
                COMMIT;
                
            END IF;
          
      END IF;
EXCEPTION ---- EXCEPTION generado por la generacion de deuda
  WHEN OTHERS THEN
        --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
        P_ERROR := 'A ocurrido un error - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_OBTENER_PRIORIDAD;