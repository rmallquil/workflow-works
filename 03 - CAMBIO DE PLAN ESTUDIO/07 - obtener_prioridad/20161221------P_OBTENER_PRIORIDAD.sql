
/*

SET SERVEROUTPUT ON
declare PRIORIDAD varchar2(200);
begin
    WFK_CONTICPE.P_OBTENER_PRIORIDAD(438,PRIORIDAD);
    DBMS_OUTPUT.PUT_LINE(PRIORIDAD);
end;

*/



-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CREATE OR REPLACE PROCEDURE P_OBTENER_PRIORIDAD (
          P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
          P_PRIORIDAD           OUT SVRRSRV.SVRRSRV_SEQ_NO%TYPE ---------------- Sequence number of the Service Rule
)
/* ===================================================================================================================
  NOMBRE    : P_OBTENER_PRIORIDAD
  FECHA     : 21/12/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene la PRIORIDAD(Sequence number of the Service Rule) de la solicitud. 
              RETORNA "0"en caso no encuentre la solicitud.

  =================================================================================================================== */
AS
          
          P_INDICADOR           NUMBER;
BEGIN
      
      SELECT COUNT(*) INTO P_INDICADOR FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
      IF P_INDICADOR < 1 THEN
          P_PRIORIDAD := P_INDICADOR;
      ELSE
          SELECT SVRSVPR_RSRV_SEQ_NO  ------------------------------------------ Sequence number of the Service Rule.
          INTO P_PRIORIDAD 
          FROM SVRSVPR
          WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
      END IF;
      
END P_OBTENER_PRIORIDAD;

