/*
SET SERVEROUTPUT ON
DECLARE   P_ERROR               VARCHAR2(200);
          P_FROM_DATE           SPRHOLD.SPRHOLD_FROM_DATE%TYPE;
BEGIN
    --WFK_CONTISRD.P_MODIFICAR_RETENCION(20451,'02','30-Dec-2016 16:28:36',P_ERROR);
    P_MODIFICAR_RETENCION(20451,'11','30-Dec-2016 16:28:36',P_ERROR);
    DBMS_OUTPUT.PUT_LINE(SQL%ROWCOUNT || ' ---- ' || P_ERROR );
END;
*/


CREATE OR REPLACE PROCEDURE P_MODIFICAR_RETENCION (
      P_PIDM_ALUMNO           IN  SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_COD_RETENCION       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_ERROR               OUT VARCHAR2
) 
/* ===================================================================================================================
  NOMBRE    : P_MODIFICAR_RETENCION
  FECHA     : 22/12/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : El objetivo es que en base a un ID se MODIFICA la retencion del tipo a un día posterior de la solicitud 

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION  
  =================================================================================================================== */
AS
      P_INDICADOR               NUMBER;
      E_INVALID_COD_RETENCION   EXCEPTION;
BEGIN
      
      -- VALIDAR codigo del TIPO DE RETENCION - STVHLDD
      SELECT COUNT(*) INTO P_INDICADOR 
      FROM STVHLDD
      WHERE STVHLDD_CODE = P_COD_RETENCION;
      
      -- INSERTAR REGISTRO DE FECHA DE RETENCION DOCUMENTOS - SOAHOLD
      IF ( P_INDICADOR = 0 ) THEN
      
            RAISE E_INVALID_COD_RETENCION;
      
      ELSE
            
            UPDATE SPRHOLD
            SET   
                  --SPRHOLD_FROM_DATE = SYSDATE + 1,
                  SPRHOLD_TO_DATE = SYSDATE - 1,
                  SPRHOLD_ACTIVITY_DATE = SYSDATE
            WHERE SPRHOLD_PIDM = P_PIDM_ALUMNO
            AND SPRHOLD_HLDD_CODE = P_COD_RETENCION;
            
            COMMIT;
      END IF;

EXCEPTION
  WHEN E_INVALID_COD_RETENCION THEN
          P_ERROR  := '- El "CÓDIGO DE RETENCION" enviado es inválido.';
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_MODIFICAR_RETENCION;