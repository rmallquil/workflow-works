/*
-- buscar objecto
select dbms_metadata.get_ddl('TRIGGER','ST_SVRSVPR_POST_INSERT_ROW') from dual

-- Desabilitar - disable trigger
ALTER TRIGGER ST_SVRSVPR_POST_INSERT_ROW ENABLE;
DROP TRIGGER ST_SVRSVPR_POST_INSERT_ROW;
*/


CREATE OR REPLACE TRIGGER ST_SVRSVPR_POST_INSERT_ROW
BEFORE INSERT
   ON "SATURN"."SVRSVPR"
   FOR EACH ROW
/* ===================================================================================================================
  NOMBRE    : ST_UA_SVRSVPR_POST_INSERT_ROW
  FECHA     : 06/10/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Generar un evento desde la creación de una nueva solicitud - Destinado para solicitudes WORKFLOW.

  MODIFICACIONES
  NRO     FECHA         USUARIO       MODIFICACION
  001     06/12/2016    RMALLQUI      Modificaciòn "CAMBIO DE PLAN" - Se obtendra el periodo del registro "ELEGIBLE" y 
                                      ya no del ultimo periodo matriculado(NRC). Solicitado por Mirian Flores.          
  002     27/03/2017    RMALLQUI      Se agrego el nombre de la sede en el titulo_sol para todas las solicitudes
  =================================================================================================================== */
DECLARE
      P_PIDM_ALUMNO             SPRIDEN.SPRIDEN_ID%TYPE;
      P_ID_ALUMNO               SPRIDEN.SPRIDEN_ID%TYPE;
      P_PERIODO                 SFBETRM.SFBETRM_TERM_CODE%TYPE;
      P_CORREO_ALUMNO           GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE;
      P_NOMBRE_ALUMNO           VARCHAR2(180); -- SPRIDEN.SPRIDEN_LAST_NAME, SPRIDEN.SPRIDEN_FIRST_NAME
      P_FOLIO_SOLICITUD         SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE;
      P_FECHA_SOLICITUD         SVRSVPR.SVRSVPR_ACTIVITY_DATE%TYPE;
      P_COMENTARIO_ALUMNO       SVRSVAD.SVRSVAD_ADDL_DATA_DESC%TYPE;
      P_MODALIDAD_ALUMNO        SGBSTDN.SGBSTDN_DEPT_CODE%TYPE; -- Departamento
      P_COD_SEDE                STVCAMP.STVCAMP_CODE%TYPE;  -- SGBSTDN.SGBSTDN_CAMP_CODE%TYPE;
      P_NOMBRE_SEDE             STVCAMP.STVCAMP_DESC%TYPE;
      
      P_PART_PERIODO            SOBPTRM.SOBPTRM_PTRM_CODE%TYPE; --------------- Parte-de-Periodo
      V_TITUTO_SOL              VARCHAR2(350);

      P_SRVC_CODE               SVRSVPR.SVRSVPR_SRVC_CODE%TYPE;
      P_INDICADOR               NUMBER;
      P_MESSAGE                 EXCEPTION;
      
      V_PARAMS          GOKPARM.T_PARAMETERLIST;
      EVENT_CODE        GTVEQNM.GTVEQNM_CODE%TYPE;
BEGIN
-- 
      -- -- -- -- GET CODIGO SERVICIO -- -- -- --
      P_SRVC_CODE            :=      :NEW.SVRSVPR_SRVC_CODE;
      
      P_PIDM_ALUMNO          :=      :NEW.SVRSVPR_PIDM;
      P_FOLIO_SOLICITUD      :=      :NEW.SVRSVPR_PROTOCOL_SEQ_NO;
      P_FECHA_SOLICITUD      :=      :NEW.SVRSVPR_ACTIVITY_DATE;
      -- P_COMENTARIO_ALUMNO    :=      :NEW.SVRSVPR_STU_COMMENT;    -- Comentario por defecto de solicitudes
      
      
      -- GET nombres , ID
      SELECT (SPRIDEN_FIRST_NAME || ' ' || SPRIDEN_LAST_NAME), SPRIDEN_ID INTO P_NOMBRE_ALUMNO, P_ID_ALUMNO  FROM SPRIDEN 
      WHERE SPRIDEN_PIDM = P_PIDM_ALUMNO AND SPRIDEN_CHANGE_IND IS NULL;
     
     
      -- GET email
      SELECT COUNT(*) INTO P_INDICADOR FROM GOREMAL WHERE GOREMAL_PIDM = P_PIDM_ALUMNO and GOREMAL_STATUS_IND = 'A';
      IF P_INDICADOR = 0 THEN
           P_CORREO_ALUMNO := '-';
      ELSE
          -----------------------
              SELECT COUNT(*) INTO P_INDICADOR FROM GOREMAL WHERE GOREMAL_PIDM = P_PIDM_ALUMNO and GOREMAL_STATUS_IND = 'A' 
              AND GOREMAL_EMAIL_ADDRESS LIKE ('%@continental.edu.pe');            
              IF (P_INDICADOR > 0) THEN
                  SELECT GOREMAL_EMAIL_ADDRESS INTO P_CORREO_ALUMNO FROM (
                    SELECT GOREMAL_EMAIL_ADDRESS, GOREMAL_PREFERRED_IND FROM GOREMAL 
                    WHERE GOREMAL_PIDM = P_PIDM_ALUMNO and GOREMAL_STATUS_IND = 'A' AND GOREMAL_EMAIL_ADDRESS LIKE ('%@continental.edu.pe')
                    ORDER BY GOREMAL_PREFERRED_IND DESC
                  )WHERE ROWNUM <= 1;
              ELSE
                  SELECT GOREMAL_EMAIL_ADDRESS INTO P_CORREO_ALUMNO FROM (
                      SELECT GOREMAL_EMAIL_ADDRESS, GOREMAL_PREFERRED_IND FROM GOREMAL
                      WHERE GOREMAL_PIDM = P_PIDM_ALUMNO and GOREMAL_STATUS_IND = 'A'
                      ORDER BY GOREMAL_PREFERRED_IND DESC
                  )WHERE ROWNUM <= 1;
              END IF;
      END IF;
      
      
      -- GET email   : Comentario Configurado como DATO ADICIONAL
      -- SELECT SVRSVAD_ADDL_DATA_DESC INTO P_COMENTARIO_ALUMNO FROM SVRSVAD
      -- WHERE SVRSVAD_PROTOCOL_SEQ_NO = :NEW.SVRSVPR_PROTOCOL_SEQ_NO AND SVRSVAD_ADDL_DATA_SEQ = 2;
      P_COMENTARIO_ALUMNO := '--';
      
      
      ----------------------- GET DATOS SEGUN TIPO DE SOLICITUD ---------------
            -- Codigo evento
            -- Periodo
            -- Departamente
            -- Sede
     ---------------------------------------------------------------------------
      IF P_SRVC_CODE = 'SOL003' THEN --########### RECTIF. REZAGADOS ###########
     ---------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL003');
              
              
      --------------------------------------------------------------------------
      ELSIF P_SRVC_CODE = 'SOL004' THEN --########### CAMBIO DE PLAN ###########
      --------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL004');
              

              -- >> GET DEPARTAMENTO y CAMPUS --
              SELECT COUNT(*) INTO P_INDICADOR FROM SORLCUR INNER JOIN SORLFOS
              ON SORLCUR_PIDM = SORLFOS_PIDM AND SORLCUR_SEQNO = SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE SORLCUR_PIDM = P_PIDM_ALUMNO AND SORLCUR_LMOD_CODE = 'LEARNER' 
              AND SORLCUR_CACT_CODE   = 'ACTIVE' AND SORLCUR_CURRENT_CDE = 'Y';

              IF P_INDICADOR = 0 THEN
                    P_MODALIDAD_ALUMNO    := '-';
                    P_COD_SEDE            := '-';
                    P_NOMBRE_SEDE         := '-';
              ELSE  
                    -- GET DEPARTAMENTO y CAMPUS mas reciente del alumno 
                    SELECT SORLFOS_DEPT_CODE , SORLCUR_CAMP_CODE INTO P_MODALIDAD_ALUMNO, P_COD_SEDE FROM (
                          SELECT    SORLCUR_SEQNO,      
                                    SORLCUR_CAMP_CODE,    
                                    SORLFOS_DEPT_CODE
                          FROM SORLCUR        
                          INNER JOIN SORLFOS
                              ON SORLCUR_PIDM         = SORLFOS_PIDM 
                              AND SORLCUR_SEQNO       = SORLFOS.SORLFOS_LCUR_SEQNO
                          WHERE SORLCUR_PIDM          = P_PIDM_ALUMNO 
                              AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                              AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                              AND SORLCUR_CURRENT_CDE = 'Y'
                          ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
                    ) WHERE ROWNUM <= 1;
                    
                    -- GET nombre campus 
                    SELECT STVCAMP_DESC INTO P_NOMBRE_SEDE FROM STVCAMP WHERE STVCAMP_CODE = P_COD_SEDE;
              END IF;
              
              
              -- GET PERIODO ACTIVO - obtenido usando el DEPTARTAMENTO y SEDE del alumno
                  /*-- GET COD PARTE-PERIODO activo ò un PERIODO proximo valido para realizar la solicitud (no "00")*/
              IF (P_MODALIDAD_ALUMNO = '-' OR P_COD_SEDE = '-') THEN
                  P_PERIODO := '-';
              ELSE
                    -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
                    SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                              WHEN 'UPGT' THEN 'W' 
                                              WHEN 'UREG' THEN 'R' 
                                              WHEN 'UPOS' THEN '-' 
                                              WHEN 'ITEC' THEN '-' 
                                              WHEN 'UCIC' THEN '-' 
                                              WHEN 'UCEC' THEN '-' 
                                              WHEN 'ICEC' THEN '-' 
                                              ELSE '1' END ||
                            CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                              WHEN 'F01' THEN 'A' 
                                              WHEN 'F02' THEN 'L' 
                                              WHEN 'F03' THEN 'C' 
                                              WHEN 'V00' THEN 'V' 
                                              ELSE '9' END
                    INTO P_PART_PERIODO
                    FROM STVCAMP,STVDEPT 
                    WHERE STVDEPT_CODE = P_MODALIDAD_ALUMNO AND STVCAMP_CODE = P_COD_SEDE;
              
                    -- GET PERIODO ACTIVO 
                    SELECT SOBPTRM_TERM_CODE INTO P_PERIODO
                    FROM (
                        -- GET COD PARTE-PERIODO activo ò un PERIODO proximo valido para realizar el cambio de plan (no "00")
                        SELECT SOBPTRM_TERM_CODE FROM SOBPTRM
                        WHERE  SOBPTRM_PTRM_CODE LIKE ('' || P_PART_PERIODO || '%')
                        --AND SYSDATE BETWEEN SOBPTRM_START_DATE AND SOBPTRM_END_DATE
                        AND SYSDATE <= SOBPTRM_START_DATE
                        AND SOBPTRM_TERM_CODE NOT LIKE ('%00') --- descartar periodos "____00"
                        ORDER BY SOBPTRM_TERM_CODE ASC
                    ) WHERE ROWNUM <= 1;
                    
                    :NEW.SVRSVPR_TERM_CODE := P_PERIODO;

              END IF;
              
              V_TITUTO_SOL := 'Solicitud de Cambio de Plan Estudios  Nº '|| P_FOLIO_SOLICITUD || ' - ' || P_NOMBRE_ALUMNO || ' - ID: '|| P_ID_ALUMNO
                              || ' - ' || P_MODALIDAD_ALUMNO || ' - ' || P_NOMBRE_SEDE;

              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: “Solicitud de Rectificación - “Nombre Alumno” - “ID Alumno” - “folio_solicitud”
                  v_Params(3).param_value := V_TITUTO_SOL ;
                  v_Params(4).param_value := P_PIDM_ALUMNO;
                  v_Params(5).param_value := P_ID_ALUMNO;
                  v_Params(6).param_value := P_PERIODO;
                  v_Params(7).param_value := P_CORREO_ALUMNO;
                  v_Params(8).param_value := P_NOMBRE_ALUMNO;
                  v_Params(9).param_value := P_FOLIO_SOLICITUD;
                  v_Params(10).param_value := P_FECHA_SOLICITUD;
                  v_Params(11).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(12).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(13).param_value := P_COD_SEDE;
                  v_Params(14).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;      --        
      END IF;
      
END;

