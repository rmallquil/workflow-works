-- buscar objecto
select dbms_metadata.get_ddl('TRIGGER','TG_SPRIDEN_DESPUES_INSERT') from dual

-- Desabilitar - disable trigger
ALTER TRIGGER TG_SVASVPR_DESPUES_INSERT ENABLE;
DROP TRIGGER TG_SVASVPR_DESPUES_INSERT;





CREATE OR REPLACE TRIGGER TG_SVRSVPR_DESPUES_INSERT
BEFORE INSERT
   ON "SATURN"."SVRSVPR"
   FOR EACH ROW
/* ===================================================================================================================
  NOMBRE    : TG_SVASVPR_DESPUES_INSERT
  FECHA     : 06/10/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Generar un evento desde la creación de una nueva solicitud del tipo SOL003.

  MODIFICACIONES
  NRO     FECHA         USUARIO       MODIFICACION
  001     24/10/2016    rmallqui      SOL003 - Actualziacion del periodo por el disponible para inscripcion 
                                      (forma SFAREGS : Inscripcion de cursos alumnos)
  =================================================================================================================== */
DECLARE
      P_ID_ALUMNO               SPRIDEN.SPRIDEN_ID%TYPE;
      P_PERIODO                 SFBETRM.SFBETRM_TERM_CODE%TYPE;
      P_CORREO_ALUMNO           GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE;
      P_NOMBRE_ALUMNO           VARCHAR2(180); -- SPRIDEN.SPRIDEN_LAST_NAME, SPRIDEN.SPRIDEN_FIRST_NAME
      P_FOLIO_SOLICITUD         SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE;
      P_FECHA_SOLICITUD         SVRSVPR.SVRSVPR_ACTIVITY_DATE%TYPE;
      P_COMENTARIO_ALUMNO       SVRSVAD.SVRSVAD_ADDL_DATA_DESC%TYPE;
      P_MODALIDAD_ALUMNO        SGBSTDN.SGBSTDN_DEPT_CODE%TYPE; -- Departamento
      P_COD_SEDE                STVCAMP.STVCAMP_CODE%TYPE;  -- SGBSTDN.SGBSTDN_CAMP_CODE%TYPE;
      P_NOMBRE_SEDE             STVCAMP.STVCAMP_DESC%TYPE;
      
      P_SRVC_CODE               SVRSVPR.SVRSVPR_SRVC_CODE%TYPE;
      P_INDICADOR               NUMBER;
      P_MESSAGE                 EXCEPTION;
      
      V_PARAMS GOKPARM.T_PARAMETERLIST;
      EVENT_CODE GTVEQNM.GTVEQNM_CODE%TYPE;
BEGIN
-- 
      -- -- -- -- GET CODIGO SERVICIO -- -- -- --
      P_SRVC_CODE            :=    :NEW.SVRSVPR_SRVC_CODE;
      
      P_ID_ALUMNO            :=      :NEW.SVRSVPR_PIDM;
      P_FOLIO_SOLICITUD      :=      :NEW.SVRSVPR_PROTOCOL_SEQ_NO;
      P_FECHA_SOLICITUD      :=      :NEW.SVRSVPR_ACTIVITY_DATE;
      -- P_COMENTARIO_ALUMNO    :=      :NEW.SVRSVPR_STU_COMMENT;    -- Comentario por defecto de solicitudes
      
      
      -- GET nombres
      SELECT (SPRIDEN_FIRST_NAME || ' ' || SPRIDEN_LAST_NAME) INTO P_NOMBRE_ALUMNO FROM SPRIDEN 
      WHERE SPRIDEN_PIDM = P_ID_ALUMNO AND SPRIDEN_CHANGE_IND IS NULL;
     
     
      -- GET email
      SELECT COUNT(*) INTO P_INDICADOR FROM GOREMAL WHERE GOREMAL_PIDM = P_ID_ALUMNO and GOREMAL_STATUS_IND = 'A';
      IF P_INDICADOR = 0 THEN
           P_CORREO_ALUMNO := '';
      ELSE
          SELECT GOREMAL_EMAIL_ADDRESS INTO P_CORREO_ALUMNO FROM (
                SELECT GOREMAL_EMAIL_ADDRESS, GOREMAL_PREFERRED_IND FROM GOREMAL 
                WHERE GOREMAL_PIDM = P_ID_ALUMNO and GOREMAL_STATUS_IND = 'A' 
                ORDER BY GOREMAL_PREFERRED_IND DESC 
           )WHERE ROWNUM <= 1;
      END IF;
      
      
      -- GET email   : Comentario Configurado como DATO ADICIONAL
      -- SELECT SVRSVAD_ADDL_DATA_DESC INTO P_COMENTARIO_ALUMNO FROM SVRSVAD
      -- WHERE SVRSVAD_PROTOCOL_SEQ_NO = :NEW.SVRSVPR_PROTOCOL_SEQ_NO AND SVRSVAD_ADDL_DATA_SEQ = 2;
      P_COMENTARIO_ALUMNO := '--';
      
      
      ----------------------- GET DATOS SEGUN TIPO DE SOLICITUD ---------------
            -- Codigo evento
            -- Periodo
            -- Departamente
            -- Sede
      --------------------------------------------------------------------------
      IF P_SRVC_CODE = 'SOL003' THEN --########### RECTIF. RESAGADOS ###########
     --------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WF_SOL_REC');
              
              -- GET PERIODO disponible para inscripcion (forma SFAREGS : Inscripcion de cursos alumnos)
              SELECT COUNT(*) INTO P_INDICADOR FROM ( SELECT SFBETRM_TERM_CODE FROM SFBETRM 
                    WHERE SFBETRM_PIDM = P_ID_ALUMNO AND SFBETRM.SFBETRM_ESTS_CODE = 'EL' ORDER BY SFBETRM_TERM_CODE DESC );
              IF P_INDICADOR = 0 THEN
                    P_PERIODO := '-';
              ELSE
                    SELECT SFBETRM_TERM_CODE INTO P_PERIODO FROM (
                          -- "EL" Elejible para inscribir 
                          SELECT SFBETRM_TERM_CODE FROM SFBETRM 
                          WHERE SFBETRM_PIDM = P_ID_ALUMNO AND SFBETRM.SFBETRM_ESTS_CODE = 'EL' 
                          ORDER BY SFBETRM_TERM_CODE DESC 
                     )WHERE ROWNUM <= 1;
                     
                     :NEW.SVRSVPR_TERM_CODE := P_PERIODO;
              END IF;
                                
              -- GET DEPARTAMENTO y CAMPUS 
              SELECT COUNT(*) INTO P_INDICADOR FROM SGBSTDN WHERE SGBSTDN_PIDM = P_ID_ALUMNO;
              IF P_INDICADOR = 0 THEN
                    P_MODALIDAD_ALUMNO    := '-';
                    P_COD_SEDE            := '-';
                    P_NOMBRE_SEDE         := 'NOTHING';
              ELSE  
                    -- GET DEPARTAMENTO y CAMPUS mas reciente del alumno segun el periodo
                    SELECT SGBSTDN_DEPT_CODE, SGBSTDN_CAMP_CODE INTO P_MODALIDAD_ALUMNO, P_COD_SEDE FROM(
                          SELECT  SGBSTDN_TERM_CODE_EFF, 
                                  SGBSTDN_DEPT_CODE, 
                                  SGBSTDN_CAMP_CODE
                                  --SGBSTDN_STST_CODE   -- STVSTST : validaciones para matricula y otro
                          FROM SGBSTDN 
                          WHERE SGBSTDN_PIDM = 30539 
                          ORDER BY SGBSTDN_TERM_CODE_EFF DESC  
                    ) WHERE ROWNUM <= 1;
                    
                    -- GET nombre campus 
                    SELECT STVCAMP_DESC INTO P_NOMBRE_SEDE FROM STVCAMP WHERE STVCAMP_CODE = P_COD_SEDE;
              END IF;
              
              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: “Solicitud de Rectificación – “Nombre Alumno” – “ID Alumno” – “folio_solicitud”
                  v_Params(3).param_value := 'Solicitud de Rectificación – ' || P_NOMBRE_ALUMNO || ' - '|| P_ID_ALUMNO || ' - '|| P_FOLIO_SOLICITUD;
                  v_Params(4).param_value := P_ID_ALUMNO;
                  v_Params(5).param_value := P_PERIODO;
                  v_Params(6).param_value := P_CORREO_ALUMNO;
                  v_Params(7).param_value := P_NOMBRE_ALUMNO;
                  v_Params(8).param_value := P_FOLIO_SOLICITUD;
                  v_Params(9).param_value := P_FECHA_SOLICITUD;
                  v_Params(10).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(11).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(12).param_value := P_COD_SEDE;
                  v_Params(13).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;
      --------------------------------------------------------------------------
      ELSIF P_SRVC_CODE = 'SOL004' THEN --########### CAMBIO DE PLAN ###########
      --------------------------------------------------------------------------
              -- Check for the event definition and set the event code.
              event_code      :=    Gokevnt.F_CheckEvent('WORKFLOW','WFSOL004');
              
              -- GET ultimmo PERIODO del alumnoen el que estudio.
              SELECT COUNT(SFRSTCR_TERM_CODE) INTO P_INDICADOR FROM SFRSTCR WHERE SFRSTCR_PIDM = P_ID_ALUMNO;
              IF P_INDICADOR = 0 THEN
                    P_PERIODO    := '-';
              ELSE  
                    SELECT SFRSTCR_TERM_CODE INTO P_PERIODO FROM (
                        -- NRC ALUMNO
                        SELECT SFRSTCR_TERM_CODE,SFRSTCR_PIDM,COUNT (SFRSTCR_CRN) N_SFRSTCR_CRN
                        FROM SFRSTCR 
                        WHERE SFRSTCR_PIDM = 325180 
                        GROUP BY SFRSTCR_TERM_CODE,SFRSTCR_PIDM 
                        ORDER BY SFRSTCR_TERM_CODE DESC
                    )WHERE ROWNUM <= 1;
              END IF;
                
                
              -- GET DEPARTAMENTO y CAMPUS 
              SELECT COUNT(*) INTO P_INDICADOR FROM SGBSTDN WHERE SGBSTDN_PIDM = P_ID_ALUMNO;
              IF P_INDICADOR = 0 THEN
                    P_MODALIDAD_ALUMNO    := '-';
                    P_COD_SEDE            := '-';
                    P_NOMBRE_SEDE         := 'NOTHING';
              ELSE  
                    -- GET DEPARTAMENTO y CAMPUS mas reciente del alumno segun el periodo
                    SELECT SGBSTDN_DEPT_CODE, SGBSTDN_CAMP_CODE INTO P_MODALIDAD_ALUMNO, P_COD_SEDE FROM(
                          SELECT  SGBSTDN_TERM_CODE_EFF, 
                                  SGBSTDN_DEPT_CODE, 
                                  SGBSTDN_CAMP_CODE
                                  --SGBSTDN_STST_CODE   -- STVSTST : validaciones para matricula y otro
                          FROM SGBSTDN 
                          WHERE SGBSTDN_PIDM = P_ID_ALUMNO 
                          ORDER BY SGBSTDN_TERM_CODE_EFF DESC  
                    ) WHERE ROWNUM <= 1;
                    
                    -- GET nombre campus 
                    SELECT STVCAMP_DESC INTO P_NOMBRE_SEDE FROM STVCAMP WHERE STVCAMP_CODE = P_COD_SEDE;
              END IF;
              
              -- SET PARAMETERS
              IF (EVENT_CODE <> 'NULL') THEN
                  --------- Set the parameter values
                  v_Params(1).param_value := event_code;
                  v_Params(2).param_value := 'Banner';
                  -- Nombre del flujo: “Solicitud de Rectificación – “Nombre Alumno” – “ID Alumno” – “folio_solicitud”
                  v_Params(3).param_value := 'Solicitud de Cambio de Plan Estudios – ' || P_NOMBRE_ALUMNO || ' - '|| P_ID_ALUMNO || ' - '|| P_FOLIO_SOLICITUD;
                  v_Params(4).param_value := P_ID_ALUMNO;
                  v_Params(5).param_value := P_PERIODO;
                  v_Params(6).param_value := P_CORREO_ALUMNO;
                  v_Params(7).param_value := P_NOMBRE_ALUMNO;
                  v_Params(8).param_value := P_FOLIO_SOLICITUD;
                  v_Params(9).param_value := P_FECHA_SOLICITUD;
                  v_Params(10).param_value := P_COMENTARIO_ALUMNO;
                  v_Params(11).param_value := P_MODALIDAD_ALUMNO;
                  v_Params(12).param_value := P_COD_SEDE;
                  v_Params(13).param_value := P_NOMBRE_SEDE;
                  --------- Create the event
                  Gokparm.Send_Param_List(event_code, v_Params);
              END IF;
      --        
      END IF;
      
    
END;