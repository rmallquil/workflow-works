/*
SET SERVEROUTPUT ON
DECLARE   
     P_HTML1                 VARCHAR2(4000);
     P_HTML2                 VARCHAR2(4000);
     P_HTML3                 VARCHAR2(4000);
BEGIN    
    P_GET_ASIG_RECONOCIDAS('145916',P_HTML1,P_HTML2,P_HTML3);
    --DBMS_OUTPUT.PUT_LINE(P_HTML1|| '--' ||P_HTML2|| '--' ||P_HTML3);
    DBMS_OUTPUT.PUT_LINE(P_HTML1);
    DBMS_OUTPUT.PUT_LINE(P_HTML2);
    DBMS_OUTPUT.PUT_LINE(P_HTML3);
END;
*/

create or replace PROCEDURE P_GET_ASIG_RECONOCIDAS (
      P_PIDM          IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_HTML1         OUT VARCHAR2,
      P_HTML2         OUT VARCHAR2,
      P_HTML3         OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_ASIG_RECONOCIDAS
  FECHA     : 30/05/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene la lista de asignaturas reconocidas.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
    
    V_MESSAGE             EXCEPTION;
    V_HTML                VARCHAR2(4000);
    V_ORIGEN              VARCHAR2(30);
    V_NHTML               NUMBER := 0;
    V_CREDIT_SUM          NUMBER := 0;
    V_CREDIT_SUM_HTML     VARCHAR2(200);                    -- PARA MOSTRAR EN EL RPEORTE html LA SUMA DE CREDITOS RECONOCIDOS
    
    V_REQUEST_NO          SMRRQCM.SMRRQCM_REQUEST_NO%TYPE;
    V_CRSE_NUMB           SMRDOUS.SMRDOUS_CRSE_NUMB%TYPE;
    V_CRN                 SMRDOUS.SMRDOUS_CRN%TYPE;
    V_CRN_VAR             VARCHAR2(15);
    V_TITLE               SMRDOUS.SMRDOUS_TITLE%TYPE;
    V_CRSE_SOURCE         SMRDOUS.SMRDOUS_CRSE_SOURCE%TYPE; 
    V_CREDIT_HOURS        SMRDOUS.SMRDOUS_CREDIT_HOURS%TYPE;
    V_GRDE_CODE           SMRDOUS.SMRDOUS_GRDE_CODE%TYPE;
    
    
    -- CURSOR GET ultima ejecucion de CAP
    CURSOR C_REQUEST_NO IS
    SELECT SMRRQCM_REQUEST_NO FROM (
        SELECT SMRRQCM_REQUEST_NO FROM SMRRQCM WHERE SMRRQCM_PIDM = P_PIDM 
        ORDER BY SMRRQCM_REQUEST_NO DESC
      )WHERE ROWNUM = 1;
    
    /* GET LIST ASIGNATURAS RECONOCIDAS.
            SMRDOUS_CRSE_SOURCE:   H - History, T - Transfer, R - In-Progress, P - Planned
    */
    CURSOR C_ASIG_CAP IS
    SELECT SMRDOUS_CRSE_NUMB,SMRDOUS_CRN,SMRDOUS_TITLE, SMRDOUS_CRSE_SOURCE, SMRDOUS_CREDIT_HOURS, SMRDOUS_GRDE_CODE
    FROM (
        SELECT SMRDOUS_CRSE_NUMB,SMRDOUS_CRN,SMRDOUS_TITLE, SMRDOUS_CRSE_SOURCE, SMRDOUS_CREDIT_HOURS, SMRDOUS_GRDE_CODE
              FROM SMRDOUS 
              WHERE SMRDOUS_PIDM = P_PIDM
              AND SMRDOUS_REQUEST_NO = V_REQUEST_NO
              AND SMRDOUS_CRSE_SOURCE IN ('H','T')
              ORDER BY SMRDOUS_COMPLIANCE_ORDER
    ) WHERE ROWNUM < 80;
    
BEGIN
    
    P_HTML1 := '<tr></tr>';
    P_HTML2 := '<tr></tr>';
    P_HTML3 := '<tr></tr>';
    
    -- -- CURSOR GET ID ultima ejecucion de CAP   
    OPEN C_REQUEST_NO;
    LOOP
      FETCH C_REQUEST_NO INTO V_REQUEST_NO;
      EXIT WHEN C_REQUEST_NO%NOTFOUND;
    END LOOP;
    CLOSE C_REQUEST_NO;
 
    
    -- GET LIST ASIGNATURAS RECONOCIDAS.
    OPEN C_ASIG_CAP;
    LOOP
        FETCH C_ASIG_CAP INTO V_CRSE_NUMB, V_CRN, V_TITLE, V_CRSE_SOURCE, V_CREDIT_HOURS, V_GRDE_CODE;
        IF C_ASIG_CAP%FOUND THEN
            
            V_CREDIT_SUM := V_CREDIT_SUM + V_CREDIT_HOURS;
            
            V_HTML := '';
            /* ORIGEN:   H - History, T - Transfer, R - In-Progress, P - Planned*/
            V_ORIGEN := (CASE V_CRSE_SOURCE WHEN 'H' THEN 'Historial' WHEN 'T' THEN 'Transfer' ELSE '-' END);
            
            V_CRN_VAR := CASE WHEN NVL(TRIM(V_CRN),'-')='-' THEN '-' ELSE V_CRN END;
             
            /***********************************************************************************************************
            -- Exportar reporte de asignaturas reconocidas
            ***********************************************************************************************************/
            V_HTML := '<tr><td>' || V_CRSE_NUMB || '</td><td>' || V_CRN_VAR || '</td><td>' || V_TITLE || '</td><td>' || V_ORIGEN || '</td><td>' || V_CREDIT_HOURS || '</td><td>' || V_GRDE_CODE || '</td></tr>';
            
            IF    (LENGTH(P_HTML1) + LENGTH(V_HTML) < 4000) AND V_NHTML < 1 THEN
                      IF(P_HTML1='<tr></tr>')THEN P_HTML1:=''; END IF;
                      P_HTML1  := P_HTML1 || V_HTML;
            ELSIF (LENGTH(P_HTML2) + LENGTH(V_HTML) < 4000) AND V_NHTML < 2 THEN 
                      IF(P_HTML2='<tr></tr>')THEN P_HTML2:=''; END IF;
                      P_HTML2 := P_HTML2 || V_HTML;
                      V_NHTML  := 1;
            ELSIF (LENGTH(P_HTML3) + LENGTH(V_HTML) < 4000) AND V_NHTML < 3 THEN 
                      IF(P_HTML3='<tr></tr>')THEN P_HTML3:=''; END IF;
                      P_HTML3 := P_HTML3 || V_HTML;
                      V_NHTML  := 2;
            ELSE
                  RAISE V_MESSAGE;
            END IF;
            
        ELSE
            EXIT;
        END IF;
    END LOOP;
    CLOSE C_ASIG_CAP;

    
    V_CREDIT_SUM_HTML := '<tr style="border-top: 1px solid #202629;"><td style="" colspan="4"></td><td style="background-color: #a3a3a3;color: #202629;">'|| V_CREDIT_SUM ||'</td><td></td></tr>';
    --*************************************************************************************************************
    --  SUMA DE CREDITOS 
    IF(LENGTH(P_HTML3) + LENGTH(V_CREDIT_SUM_HTML)) < 4000 THEN 
        P_HTML3 := P_HTML3 || V_CREDIT_SUM_HTML;
    ELSE
        RAISE V_MESSAGE;
    END IF;
    
EXCEPTION
  WHEN V_MESSAGE THEN 
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| '- La cantidad de registros supera la capacidad del los parametros retornados.' );
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_ASIG_RECONOCIDAS;