create or replace PACKAGE WFK_CONTICPE AS
/*******************************************************************************
 WFK_CONTICPE:
       Conti Package body SOLICITUD de CAMBIO de PLAN ESTUDIOS
*******************************************************************************/
-- FILE NAME..: WFK_CONTICPE.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_CONTICPE
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/

PROCEDURE p_get_usuario_regacad (
                    P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
                    P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
                    P_NOMBRE              OUT VARCHAR2,
                    P_CORREO_REGACA       OUT VARCHAR2,
                    P_USUARIO_REGACA      OUT VARCHAR2,
                    P_ROL_SEDE            OUT VARCHAR2,
                    P_ERROR               OUT VARCHAR2
              );

--------------------------------------------------------------------------------

PROCEDURE P_PERIODO_VIGENTE (
                    P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
                    P_PERIODO             IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
                    P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
                    P_MODALIDAD_ALUMNO    IN SGBSTDN.SGBSTDN_DEPT_CODE%TYPE,
                    P_PERIODO_VIGENTE     OUT VARCHAR2,
                    P_ERROR               OUT VARCHAR2
              );

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
                  P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
                  P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
                  P_ERROR               OUT VARCHAR2
              );
              
--------------------------------------------------------------------------------

PROCEDURE P_ELIMINAR_ASIGNATURAS( 
                  P_ID_ALUMNO               IN SPRIDEN.SPRIDEN_PIDM%TYPE,
                  P_PERIODO                 IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
                  P_ERROR                   OUT VARCHAR2
              );
              
--------------------------------------------------------------------------------

PROCEDURE P_OBTENER_COMENTARIO_ALUMNO (
                  P_NUMERO_SOLICITUD        IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
                  P_COMENTARIO_ALUMNO       OUT VARCHAR2
              );

--------------------------------------------------------------------------------
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END WFK_CONTICPE;