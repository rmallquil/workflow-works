/*
SET SERVEROUTPUT ON
DECLARE   P_MESSAGE        VARCHAR2(4000);
BEGIN    
    P_SET_CURSO_INTROD(,'201720',,P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
END;
*/

create or replace PROCEDURE P_SET_CURSO_INTROD (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_MESSAGE             OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SET_CURSO_INTROD
  FECHA     : 02/08/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Asigna los cursos introducctorios de RM y RV con nota 11 solo en caso no los tubiera alguno.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
    
    V_STATUS_DATE_AP      SVRSVPR.SVRSVPR_STATUS_DATE%TYPE;
    
BEGIN
---
    -- Get la fecha de aprovación de la solicitud
    SELECT SVRSVPR_STATUS_DATE INTO  V_STATUS_DATE_AP
    FROM SVRSVPR 
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD 
    AND SVRSVPR_SRVS_CODE = 'AP';
    
    ----------------------------------------------------------------------------
    -- INSERT curso introductorio de RM
    INSERT INTO SORTEST ( 
          SORTEST_PIDM,         SORTEST_TESC_CODE,      SORTEST_TEST_DATE,
          SORTEST_TEST_SCORE,   SORTEST_ACTIVITY_DATE,  SORTEST_TERM_CODE_ENTRY,
          SORTEST_RELEASE_IND,  SORTEST_EQUIV_IND,      SORTEST_USER_ID,
          SORTEST_DATA_ORIGIN   ) 
    SELECT 
          P_PIDM,               'RM',                   V_STATUS_DATE_AP,
          '11.00',              SYSDATE,                P_TERM_CODE,
          'N',                  'N',                    USER,
          'WorkFlow'
    FROM DUAL
    WHERE NOT EXISTS (
        SELECT SORTEST_PIDM FROM SORTEST 
        WHERE SORTEST_PIDM = P_PIDM AND SORTEST_TESC_CODE = 'RM'
        AND SORTEST_TEST_SCORE >= '10.5'
    );
    
    ----------------------------------------------------------------------------
    -- INSERT curso introductorio de RV
    INSERT INTO SORTEST ( 
          SORTEST_PIDM,         SORTEST_TESC_CODE,      SORTEST_TEST_DATE,
          SORTEST_TEST_SCORE,   SORTEST_ACTIVITY_DATE,  SORTEST_TERM_CODE_ENTRY,
          SORTEST_RELEASE_IND,  SORTEST_EQUIV_IND,      SORTEST_USER_ID,
          SORTEST_DATA_ORIGIN   ) 
    SELECT 
          P_PIDM,               'RV',                   V_STATUS_DATE_AP,
          '11.00',              SYSDATE,                P_TERM_CODE,
          'N',                  'N',                    USER,
          'WorkFlow'
    FROM DUAL
    WHERE NOT EXISTS (
        SELECT SORTEST_PIDM FROM SORTEST 
        WHERE SORTEST_PIDM = P_PIDM AND SORTEST_TESC_CODE = 'RV'
        AND SORTEST_TEST_SCORE >= '10.5'
    );
    
    COMMIT;
--
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CURSO_INTROD;
