set serveroutput on;
declare lv_out varchar2(300 char);
begin
  szkecap.p_exec_proy(342961,'105','201710',lv_out);
  dbms_output.put_line(lv_out);
end;
--------------------------------
--------------------------------
--------------------------------


CREATE OR REPLACE PROCEDURE P_CPE_EXECPROY (
      P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
      P_MESSAGE           OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_CPE_EXECPROY
  FECHA     : 14/06/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Ejecución de PROYECCION.
  =================================================================================================================== */
AS
    -- @PARAMETERS
    LV_OUT            VARCHAR2(4000);
    V_PROGRAMA        SORLCUR.SORLCUR_PROGRAM%TYPE;
    P_EXCEPTION       EXCEPTION;
    --
BEGIN 
--
    SELECT    SORLCUR_PROGRAM
    INTO      V_PROGRAMA
    FROM (
            SELECT    SORLCUR_PROGRAM
            FROM SORLCUR        INNER JOIN SORLFOS
                  ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                  AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
            WHERE   SORLCUR_PIDM        =   P_PIDM 
                AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                AND SORLCUR_CURRENT_CDE =   'Y'
                ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
    ) WHERE ROWNUM <= 1;

    SZKECAP.P_EXEC_PROY(P_PIDM,V_PROGRAMA,P_TERM_CODE,LV_OUT);
    IF(LV_OUT <> '0')THEN
        P_MESSAGE := LV_OUT;
        RAISE P_EXCEPTION;
    END IF;
    
EXCEPTION
  WHEN P_EXCEPTION THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
  WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CPE_EXECPROY;