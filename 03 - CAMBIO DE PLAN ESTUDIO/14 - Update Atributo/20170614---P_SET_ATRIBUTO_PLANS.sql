/*
set serveroutput on
DECLARE P_ERROR varchar2(4000);
begin
  P_SET_ATRIBUTO_PLANS(551813,'201710','P015',P_ERROR);
  DBMS_OUTPUT.PUT_LINE(P_ERROR);
end;
*/

create or replace PROCEDURE P_SET_ATRIBUTO_PLANS (
      P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
      P_ATTS_CODE         IN STVATTS.STVATTS_CODE%TYPE,      -------- COD atributos
      P_ERROR             OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SET_ATRIBUTO_PLANS
  FECHA     : 14/06/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : SET ATRIBUTO de PLAN ESTUDIOS a un periodo determinado.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
    
      V_TERM_LAST_CODE      STVTERM.STVTERM_CODE%TYPE;
      V_INDICADOR           NUMBER;
      V_ATTS_CURRENT        STVATTS.STVATTS_CODE%TYPE;
      P_ROWID               GB_COMMON.INTERNAL_RECORD_ID_TYPE;
      
      -- GET el ultimo TERM(Periodo) de los atributos del estudiante - forma SGASADD
      CURSOR C_SGRSATT_TERM IS
      SELECT SGRSATT_TERM_CODE_EFF
      FROM (
          SELECT SGRSATT_TERM_CODE_EFF FROM SGRSATT 
          WHERE SGRSATT_PIDM = P_PIDM
          AND SGRSATT_TERM_CODE_EFF <= P_TERM_CODE
          ORDER BY SGRSATT_TERM_CODE_EFF DESC
      ) WHERE ROWNUM = 1;
      
      V_TERM                STVTERM.STVTERM_CODE%TYPE;
      V_ATTS_CODE           STVATTS.STVATTS_CODE%TYPE;
      
      -- CREAR - ACTUALZIAR ATRIBUTO DE PLAN ESTUDIOS
      CURSOR C_SGRSATT_SET IS
      SELECT  SGRSATT_TERM_CODE_EFF,
              SGRSATT_ATTS_CODE
        FROM SGRSATT 
        WHERE SGRSATT_PIDM =  P_PIDM
        AND SGRSATT_TERM_CODE_EFF = V_TERM_LAST_CODE
        AND P_ATTS_CODE <> V_ATTS_CURRENT;
        
BEGIN  
      
      -- GET el ultimo TERM(Periodo) de los atributos del estudiante - forma SGASADD
      OPEN C_SGRSATT_TERM;
        LOOP
          FETCH C_SGRSATT_TERM INTO V_TERM_LAST_CODE;
          EXIT WHEN C_SGRSATT_TERM%NOTFOUND;
        END LOOP;
      CLOSE C_SGRSATT_TERM;
      
      
      -- #######################################################################
      -- SET ATRIBUTO alumno - SGASADD
      IF V_TERM_LAST_CODE IS NULL THEN
            
          -- CASO QUE NO EXISTA NINGUN REGISTRO DE ATRIBUTO
          INSERT INTO SGRSATT
            (
              SGRSATT_PIDM, 
              SGRSATT_TERM_CODE_EFF,
              SGRSATT_ATTS_CODE,
              SGRSATT_ACTIVITY_DATE,
              SGRSATT_STSP_KEY_SEQUENCE
            )
          SELECT P_PIDM, P_TERM_CODE, P_ATTS_CODE, SYSDATE, NULL
          FROM DUAL
          WHERE NOT EXISTS (SELECT * FROM SGRSATT WHERE SGRSATT_PIDM = P_PIDM);
      
      ELSE
            
          -- GET ATRIBUTO de PLAN ESTUDIO mas cercano al periodo requerido - forma SGASADD
          SELECT SGRSATT_ATTS_CODE INTO V_ATTS_CURRENT 
          FROM SGRSATT 
          WHERE SGRSATT_PIDM = P_PIDM
            AND SGRSATT_TERM_CODE_EFF = V_TERM_LAST_CODE
            AND SGRSATT_ATTS_CODE IN ( 
                    SELECT STVATTS_CODE FROM STVATTS WHERE STVATTS_CODE LIKE ('P0%')
            );
      
          -- CREAR - ACTUALZIAR ATRIBUTO DE PLAN ESTUDIOS
          OPEN C_SGRSATT_SET;
            LOOP
              FETCH C_SGRSATT_SET INTO V_TERM, V_ATTS_CODE;
              IF C_SGRSATT_SET%FOUND THEN
                  IF V_TERM <> P_TERM_CODE THEN
                      
                      INSERT INTO SGRSATT
                      ( SGRSATT_PIDM, SGRSATT_TERM_CODE_EFF, SGRSATT_ATTS_CODE, SGRSATT_ACTIVITY_DATE, SGRSATT_STSP_KEY_SEQUENCE )
                      SELECT P_PIDM, P_TERM_CODE, CASE WHEN V_ATTS_CODE = V_ATTS_CURRENT THEN P_ATTS_CODE ELSE V_ATTS_CODE END, SYSDATE, NULL  
                      FROM DUAL;
                    
                  ELSE
                  
                      UPDATE SGRSATT SET SGRSATT_ATTS_CODE = P_ATTS_CODE
                      WHERE SGRSATT_PIDM = P_PIDM
                      AND SGRSATT_TERM_CODE_EFF = P_TERM_CODE
                      AND SGRSATT_ATTS_CODE = V_ATTS_CURRENT;
                      
                      EXIT;
                      
                  END IF;
              ELSE EXIT;
            END IF;
            END LOOP;
          CLOSE C_SGRSATT_SET;

      END IF;
      
      COMMIT;
      
EXCEPTION
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_ATRIBUTO_PLANS;
