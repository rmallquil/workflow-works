/*SET SERVEROUTPUT ON
DECLARE BOOL VARCHAR2(1);
BEGIN
        BOOL :=  F_RSS_CPE('928','0','0');
        --DBMS_OUTPUT.PUT_LINE(CASE WHEN BOOL = TRUE THEN 'TRUE' ELSE 'FALSE' END);
        DBMS_OUTPUT.PUT_LINE(BOOL);
END;
*/


--CREATE OR REPLACE FUNCTION F_RSS_CPE (
--    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
--    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
--    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
--) RETURN VARCHAR2
--/* ===================================================================================================================
--  NOMBRE    : F_RSS_CPE
--              "Regla Solicitud de Servicio de CAMBIO de PLAN ESTUDIO"
--  FECHA     : 07/12/16
--  AUTOR     : Mallqui Lopez, Richard Alfonso
--  OBJETIVO  : 
--  =================================================================================================================== */
-- IS

SET SERVEROUTPUT ON
DECLARE
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE := 551813;
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE;
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE;

      V_CODIGO_SOL            SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL004';
      V_PERIODOCTLG           SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';
      V_PERIODOCTLG_MAX       SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';
      V_CODE_DEPT             STVDEPT.STVDEPT_CODE%TYPE;
      V_CODE_TERM             STVTERM.STVTERM_CODE%TYPE;
      V_CODE_CAMP             STVCAMP.STVCAMP_CODE%TYPE;
      V_PROGRAM               SORLCUR.SORLCUR_PROGRAM%TYPE;
      
      V_SUB_PTRM              VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_PTRM_CODE             STVPTRM.STVPTRM_CODE%TYPE;
      V_MATRIC_FECHA          SFRRSTS.SFRRSTS_START_DATE%TYPE; --- Fecha de una semana antes de matricula
      
      V_PRIORIDAD             NUMBER;
      V_HAS_RETENCION         NUMBER;
      V_HAS_NRC               NUMBER;
      V_TERM_PREVIUS          STVTERM.STVTERM_CODE%TYPE;
      
      -- OBTENER PERIODO DE CAMPUS, DEPT, CATALOGO y la CARRERA
      CURSOR C_SORLCUR IS
      SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
      FROM(
          SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
          FROM SORLCUR 
          INNER JOIN SORLFOS
              ON SORLCUR_PIDM         = SORLFOS_PIDM 
              AND SORLCUR_SEQNO       = SORLFOS_LCUR_SEQNO
          WHERE SORLCUR_PIDM          = P_PIDM 
              AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
              AND SORLCUR_CACT_CODE   = 'ACTIVE' 
              AND SORLCUR_CURRENT_CDE = 'Y'
          ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM = 1;
      
      -- Calculando parte PERIODO (sub PTRM)
      CURSOR C_SFRRSTS_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = V_CODE_DEPT AND STVCAMP_CODE = V_CODE_CAMP;     
      
      
      /******************************************************************************************************
      GET TERM (PERIODO - Solo usando parte de periodo 1) 
           A LA FECHA QUE ESTE DENTRO DE LA FECHA DE INICIO DE MATRICULA(SFRRSTS_START_DATE) y 
           - y UNA SEMANA ANTES DE INICIO DE CLASES(SOBPTRM_START_DATE-7) -- LOS Q NO ESTUDIARON EL PERIODO ANTERIOR
           - HASTA EL INICIO DE CLASES(SOBPTRM_START_DATE) -- ALUMNO NORMAL
      */
      CURSOR C_STUDN_TERM IS
      SELECT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE, FECHA_MATRICULA FROM (
          SELECT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE, FECHA_MATRICULA FROM (
              -- Forma SOATERM - SFARSTS  ---> fechas para las partes de periodo
              SELECT DISTINCT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE, TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') FECHA_MATRICULA
              FROM SOBPTRM
              INNER JOIN SFRRSTS
                ON SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
                AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
              WHERE SFRRSTS_RSTS_CODE = 'RW'
              AND SOBPTRM_PTRM_CODE IN (V_SUB_PTRM || '1' ,  CASE WHEN V_CODE_DEPT='UVIR' OR V_CODE_DEPT='UPGT' THEN (V_SUB_PTRM||'2') ELSE '-' END ) 
              AND SOBPTRM_PTRM_CODE NOT LIKE '%0'          
              AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE-7,'dd/mm/yyyy'),'dd/mm/yyyy')  AND (SOBPTRM_START_DATE)
              ORDER BY SOBPTRM_TERM_CODE 
          ) ORDER BY SOBPTRM_TERM_CODE ,SOBPTRM_PTRM_CODE 
      ) WHERE ROWNUM <= 1;
      
      -- GET - PREVIUS TERM(Periodo)
      CURSOR C_PREVIUS_TERM IS
      SELECT PREVIUS_TERM 
        FROM (
            SELECT  STVTERM_CODE, 
                    LAG(STVTERM_CODE, 1, 0) OVER (ORDER BY STVTERM_CODE) PREVIUS_TERM, 
                    LEAD(STVTERM_CODE, 1, 0) OVER (ORDER BY STVTERM_CODE) NEXT_TERM
            FROM STVTERM ORDER BY STVTERM_CODE DESC
        ) WHERE STVTERM_CODE = V_CODE_TERM AND ROWNUM = 1;
--      
BEGIN  
--    
      OPEN C_SORLCUR;
      LOOP
        FETCH C_SORLCUR INTO V_CODE_CAMP, V_CODE_DEPT, V_PERIODOCTLG, V_PROGRAM;
        EXIT WHEN C_SORLCUR%NOTFOUND;
        END LOOP;
      CLOSE C_SORLCUR;
      
      DBMS_OUTPUT.PUT_LINE(V_CODE_CAMP);
      DBMS_OUTPUT.PUT_LINE(V_CODE_DEPT);
      DBMS_OUTPUT.PUT_LINE(V_PERIODOCTLG);
      DBMS_OUTPUT.PUT_LINE(V_PROGRAM);
      
      -- >> calculando PARTE PERIODO  --
      OPEN C_SFRRSTS_PTRM;
      LOOP
        FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
        EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
      END LOOP;
      CLOSE C_SFRRSTS_PTRM;
      
      DBMS_OUTPUT.PUT_LINE(V_SUB_PTRM);
  
      -- VERIFICAR QUE EL ALUMNO CUMPLA CON LAS RETENCIONES: '07' y/o '08' (Forma SOAHOLD)
      SELECT COUNT(*) INTO V_HAS_RETENCION FROM SPRHOLD
      WHERE SPRHOLD_PIDM = P_PIDM
      AND SPRHOLD_HLDD_CODE IN ('07','08')
      AND SPRHOLD_TO_DATE > SYSDATE;
      
      DBMS_OUTPUT.PUT_LINE(V_HAS_RETENCION);
      
      -- VALIDACION DE RESTRICCIONES
      IF /* TENER RETENCION 07 ó 08 */ (V_HAS_RETENCION = 0) OR 
         /* PERTENECER A UREG de la CARRERA 309 ó 308*/ (V_CODE_DEPT = 'UREG' AND (V_PROGRAM = '309' OR V_PROGRAM = '308')) OR
         /* PERTENECER A UPGT de la CARRERA 309 ó 318*/ (V_CODE_DEPT = 'UPGT' AND (V_PROGRAM = '309' OR V_PROGRAM = '318')) THEN
         --RETURN 'N';
         DBMS_OUTPUT.PUT_LINE('N');
      END IF;
      
      
      /******************************************************************************************************
      GET TERM (PERIODO - Solo usando parte de periodo 1) */
      OPEN C_STUDN_TERM;
      LOOP
        FETCH C_STUDN_TERM INTO V_CODE_TERM, V_PTRM_CODE, V_MATRIC_FECHA;
        EXIT WHEN C_STUDN_TERM%NOTFOUND;
        END LOOP;
      CLOSE C_STUDN_TERM;
      
      DBMS_OUTPUT.PUT_LINE(V_CODE_TERM);
      DBMS_OUTPUT.PUT_LINE(V_PTRM_CODE);
      DBMS_OUTPUT.PUT_LINE(V_MATRIC_FECHA);
      
      /******************************************************************************/
      -- PARA SEGUNDO MODULO: verificar que no tenga NRC primer modulo
      IF( V_PTRM_CODE = (V_SUB_PTRM||'2')) THEN

          SELECT COUNT(*) INTO V_HAS_NRC FROM SFRSTCR  
          WHERE SFRSTCR_TERM_CODE = V_CODE_TERM
          AND SFRSTCR_PIDM = P_PIDM
          AND SFRSTCR_PTRM_CODE = (V_SUB_PTRM||'1');
          
          DBMS_OUTPUT.PUT_LINE(V_HAS_NRC);
          
          IF  (V_HAS_NRC > 0 OR  -- No haber estudiado en el primer modulo
              SYSDATE < (V_MATRIC_FECHA) OR -- En segundo modulo: desde inicio de matricula <-> inicio de clases 
              V_CODE_TERM IS NULL) THEN
              --RETURN 'N';
              DBMS_OUTPUT.PUT_LINE('N');
          END IF;
      
      -- PARA PRIMER MODULO O UN CASO NORMAL: verificar si estudió el periodo anterior.
      ELSE
          
          OPEN C_PREVIUS_TERM;
          LOOP
              FETCH C_PREVIUS_TERM INTO V_TERM_PREVIUS;
              EXIT WHEN C_PREVIUS_TERM%NOTFOUND;
              END LOOP;
          CLOSE C_PREVIUS_TERM;
          
          DBMS_OUTPUT.PUT_LINE(V_TERM_PREVIUS);
          
          SELECT COUNT(*) INTO V_HAS_NRC FROM SFRSTCR  
          WHERE SFRSTCR_TERM_CODE = V_TERM_PREVIUS
          AND SFRSTCR_PIDM = P_PIDM;
          
          IF  (V_HAS_NRC > 0 AND SYSDATE < V_MATRIC_FECHA) OR  -- DE haber estudiado el periodo anterior: matricula <-> inicio de clases            
              V_CODE_TERM IS NULL OR V_TERM_PREVIUS IS NULL THEN
              
              DBMS_OUTPUT.PUT_LINE('N');
              --RETURN 'N';
              
          END IF;
          
      END IF;
      
      -- VALIDAR MODALIDAD Y PRIORIDAD
      WFK_OAFORM.P_CHECK_DEPT_PRIORIDAD (P_PIDM, V_CODIGO_SOL, NULL,NULL,NULL, V_PRIORIDAD);
      
      DBMS_OUTPUT.PUT_LINE(V_PRIORIDAD);
      
      
      IF(V_PERIODOCTLG < V_PERIODOCTLG_MAX) AND V_PRIORIDAD > 0 THEN
          --RETURN 'Y';
          DBMS_OUTPUT.PUT_LINE('Y');
      ELSE
          --RETURN 'N';
          DBMS_OUTPUT.PUT_LINE('N');
      END IF;
--
--END F_RSS_CPE;
END;