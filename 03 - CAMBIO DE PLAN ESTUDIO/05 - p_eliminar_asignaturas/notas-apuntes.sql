-- relacionar NUMERO_SOLICTUD - NUMEO TRANSACCION
-- Actualziar el periodo de la DEUDA SOLICTUD al periodo correcto

SELECT * FROM SPRIDEN WHERE SPRIDEN_ID = '43048428'; --PIDM 11861 -- UV
SELECT * FROM SPRIDEN WHERE SPRIDEN_ID = '72569117'; --PIDM 183294
SELECT * FROM SPRIDEN WHERE SPRIDEN_ID = '74587501'; --PIDM 211402
SELECT * FROM SPRIDEN WHERE SPRIDEN_ID = '44832442'; --PIDM 408462 
--44832442	408462
SELECT * FROM TBRACCD WHERE TBRACCD_PIDM = 408462 AND TBRACCD_DETAIL_CODE = 'RMA';
--delete from TBRACCD WHERE TBRACCD_PIDM = 408462 AND TBRACCD_DETAIL_CODE = 'RMA'; commit;


-- SFARGFE - SFTREGS CRN -- Temporary registration table, used as a workpad during the registration process.
  SELECT * FROM SFTREGS WHERE SFTREGS_PIDM = 408462; -- INFO SFTREGS
-- DegreeWorks Temporary Registration Error Message Table.
  SELECT * FROM SFTDWER WHERE SFTDWER_PIDM = 408462;  -- INFO SFTDWER
  -- TIPO DE HORARIO (OFERTA)
  SELECT * FROM STVSCHD;
  
  -- Student Course Registration Archive Table.
  SELECT * FROM SFRSTCA WHERE SFRSTCA_PIDM = 408462; -- INFO SFRSTCA 
  -- SFASLST ::: Student Course Registration Repeating Table 
  SELECT * FROM SFRSTCR WHERE SFRSTCR_PIDM = 408462; -- INFO SFRSTCR 

-- forma SGASADD : ATRIBUTO PARA DEFINIR EL PLAN QUE PETERNECE ALUMNO
  SELECT * FROM SGRSATT;
-- ELEGIBLE
  SELECT * FROM SFBETRM WHERE SFBETRM_PIDM = 211402;
-- plan estudio -- Student Study Path Enrollment. 
  SELECT * FROM SFRENSP WHERE SFRENSP_KEY_SEQNO > 1; -- info SFRENSP
-- SGRSTSP - PLAN -- lista de estados STVSTSP --- ORDER POR PERIODO (NO SE ENCONTRO OTRA LOGICA DE DETECTAR EL ACTIVO)
  SELECT *,SGRSTSP_STSP_CODE FROM SGRSTSP;
-- SGASTDN - PERIODO -- lista de estados STVSTST
  SELECT *,SGBSTDN_STST_CODE FROM SGBSTDN;


-- DEUDA/PAGO
  SELECT * FROM TBRACCD WHERE TBRACCD_PIDM = 211402;
-- Gradebook coded comment validation table 
  SELECT * FROM STVGCMT; -- INFO STVGCMT

--------------------------------------- '72569117'; --PIDM 183294
              -- Student Course Registration Archive Table.
              SELECT * FROM SFRSTCA WHERE SFRSTCA_PIDM = 183294; -- INFO SFRSTCA 
              -- SFASLST ::: Student Course Registration Repeating Table 
              SELECT * FROM SFRSTCR WHERE SFRSTCR_PIDM = 183294; -- INFO SFRSTCR 
              
              -- forma SGASADD : ATRIBUTO PARA DEFINIR EL PLAN QUE PETERNECE ALUMNO
              SELECT * FROM SGRSATT;
              -- ELEGIBLE
              SELECT * FROM SFBETRM WHERE SFBETRM_PIDM = 183294;
              
              -- SGASTDN - -- lista de estados STVSTST
              SELECT * FROM SGBSTDN WHERE SGBSTDN_PIDM = 183294; -- SGBSTDN_STST_CODE
              -- SGRSTSP - -- lista de estados STVSTSP --- ORDER POR PERIODO (NO SE ENCONTRO OTRA LOGICA DE DETECTAR EL ACTIVO)
              SELECT * FROM SGRSTSP WHERE SGRSTSP_PIDM = 183294 order by SGRSTSP_TERM_CODE_EFF DESC; -- SGRSTSP_STSP_CODE
              
              -- DEUDA/PAGO
              SELECT * FROM TBRACCD WHERE TBRACCD_PIDM = 183294;




      SELECT * FROM SGRSTSP WHERE SGRSTSP_PIDM = 402197 order by SGRSTSP_TERM_CODE_EFF DESC, SGRSTSP_KEY_SEQNO; -- SGRSTSP_STSP_CODE--1
      SELECT to_char(SGRSTSP.SGRSTSP_ACTIVITY_DATE, 'mm/dd/yyyy hh24:mi:ss') hora,SGRSTSP.* FROM SGRSTSP WHERE SGRSTSP_PIDM = 402197; --4
      SELECT * FROM SPRIDEN WHERE SPRIDEN_PIDM = 402197; --76127950
      SELECT * FROM SGBSTDN WHERE SGBSTDN_PIDM = 402197;
      SELECT * FROM SFBETRM WHERE SFBETRM_PIDM = 183294;
      SELECT * FROM SFRSTCR WHERE SFRSTCR_PIDM = 183294; -- INFO SFRSTCR 

      SELECT * FROM SFRRGFE;
      SELECT * FROM SFBETRM WHERE SFBETRM_PIDM = 211402;
      select * from tbraccd where tbraccd_pidm = 211402 and TBRACCD_TERM_CODE = '201620';

      -- Student Course Registration Archive Table.
      SELECT * FROM SFRSTCA WHERE SFRSTCA_PIDM = 386582; -- INFO SFRSTCA 
      -- SFASLST ::: Student Course Registration Repeating Table 
      SELECT * FROM SFRSTCR WHERE SFRSTCR_PIDM = 408462; -- INFO SFRSTCR 
      SELECT * FROM SFRRGFE;
      SELECT * FROM SFRENSP WHERE sfrensp_pidm = 386582 AND sfrensp_term_code = '201620';
      SELECT * FROM SFBETRM WHERE SFBETRM_PIDM = 340813;
      SELECT * FROM SFREFEE WHERE SFREFEE_PIDM = 408462;
      select * from tbraccd where tbraccd_pidm = 386582 and TBRACCD_TERM_CODE = '201620';
      --DELETE from tbraccd where tbraccd_pidm = 386582 and TBRACCD_TERM_CODE = '201620';COMMIT;


-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
SET SERVEROUTPUT ON
DECLARE     P_ID_ALUMNO                 SPRIDEN.SPRIDEN_PIDM%TYPE        := 386582  ;
            P_PERIODO                   SFBETRM.SFBETRM_TERM_CODE%TYPE   := '201620';
            
            P_FECHA_STATUS              SFBETRM.SFBETRM_ASSESSMENT_DATE%TYPE;
            P_FECHA_REFUND              SFBETRM.SFBETRM_REFUND_DATE%TYPE;
            P_KEY_SEQNO                 SGRSTSP.SGRSTSP_KEY_SEQNO%TYPE;
            SAVE_ACT_DATE_OUT           VARCHAR2(100);
            RETURN_STATUS_IN_OUT        NUMBER;
            P_INDICADOR                 NUMBER;
            P_ERROR                     varchar2(100);
BEGIN
      
      -- #######################################################################          
      -- VALIDAR - "estimaciones IN-LINE"
      -- #######################################################################          
      
        -- #######################################################################          
              -- Eliminar registros de CRN'S
              -- SFASLST ::: Student Course Registration Repeating Table 
              DELETE FROM SFRSTCR 
              WHERE SFRSTCR_PIDM = P_ID_ALUMNO
              AND SFRSTCR_TERM_CODE = P_PERIODO; 
        DBMS_OUTPUT.PUT_LINE('del curse B : ' || SQL%ROWCOUNT);
        --COMMIT;
      
                                    SELECT COUNT(*) INTO P_INDICADOR 
                                    FROM TBRACCD WHERE TBRACCD_PIDM = P_ID_ALUMNO AND TBRACCD_TERM_CODE = P_PERIODO;
                                    DBMS_OUTPUT.PUT_LINE('TBRACCD ---> ' || P_INDICADOR);

      -- #######################################################################
      -- AMORTIGUAR DEUDA
      SFKFEES.P_PROCESS_ETRM_DROP(  P_ID_ALUMNO, 
                                    P_PERIODO, 
                                    SYSDATE );
      
      
      -- #######################################################################
      -- Procesar DEUDA
      SFKFEES.P_PROCESSFEEASSESSMENT(   P_PERIODO,
                                        P_ID_ALUMNO,
                                        NULL,
                                        SYSDATE,
                                        'R',
                                        'N',                  -- create TBRACCD records
                                        'SFAREGS',    
                                        'Y',                  -- commit changes
                                        SAVE_ACT_DATE_OUT,
                                        'N',
                                        RETURN_STATUS_IN_OUT); 
                                        
                                        DBMS_OUTPUT.PUT_LINE('P_PROCESSFEEASSESSMENT : ' || SAVE_ACT_DATE_OUT || '--' || RETURN_STATUS_IN_OUT);
      
            -- #######################################################################
            -- ELIMINAR - STUDY PATH -> SFAREGS 
            DELETE FROM SFRENSP 
            WHERE SFRENSP_PIDM = P_ID_ALUMNO
            AND SFRENSP_TERM_CODE = P_PERIODO; 
        DBMS_OUTPUT.PUT_LINE('del PlanCourse : ' || SQL%ROWCOUNT);
        --COMMIT;
      
                                    SELECT COUNT(*) INTO P_INDICADOR 
                                    FROM TBRACCD WHERE TBRACCD_PIDM = P_ID_ALUMNO AND TBRACCD_TERM_CODE = P_PERIODO;
                                    DBMS_OUTPUT.PUT_LINE('TBRACCD ---> ' || P_INDICADOR);
      
             -- #######################################################################
             -- ELIMINAR - INFORMACION DE INGRESO -> SFAREGS  ||  registro que determina al alumno Elegible 
              DELETE FROM SFBETRM 
              WHERE SFBETRM_PIDM = P_ID_ALUMNO
              AND SFBETRM_TERM_CODE = P_PERIODO;  
        DBMS_OUTPUT.PUT_LINE('del header "EL" : ' || SQL%ROWCOUNT);
        --COMMIT;
 
                                    SELECT COUNT(*) INTO P_INDICADOR 
                                    FROM TBRACCD WHERE TBRACCD_PIDM = P_ID_ALUMNO AND TBRACCD_TERM_CODE = P_PERIODO;
                                    DBMS_OUTPUT.PUT_LINE('TBRACCD ---> ' || P_INDICADOR);
      
        -- #######################################################################
        -- CAMBIO ESTADO PLAN ESTUDIO  
        SELECT  SGRSTSP_KEY_SEQNO INTO P_KEY_SEQNO
        FROM SGRSTSP 
        WHERE SGRSTSP_PIDM = P_ID_ALUMNO 
        AND SGRSTSP_TERM_CODE_EFF = P_PERIODO AND ROWNUM = 1
        ORDER BY SGRSTSP_TERM_CODE_EFF DESC, SGRSTSP_KEY_SEQNO ASC;
        
        DBMS_OUTPUT.PUT_LINE(P_KEY_SEQNO);
        
        
        UPDATE SGRSTSP 
            SET SGRSTSP_STSP_CODE = 'RV' ------------------ ESTADOS STVSTSP : 'RV' - Reserva
        WHERE SGRSTSP_PIDM = P_ID_ALUMNO 
        AND SGRSTSP_TERM_CODE_EFF = P_PERIODO 
        AND SGRSTSP_KEY_SEQNO = P_KEY_SEQNO; -- P_KEY_SEQNO
          
        DBMS_OUTPUT.PUT_LINE('Actualziado ESTADO PLAN ESTUDIO ' || SQL%ROWCOUNT);
        
        -- #######################################################################
        -- CAMBIO ESTADO PERIODO  
          
        UPDATE SGBSTDN
            SET SGBSTDN_STST_CODE = 'IS'  ------------------ ESTADOS STVSTST : 'IS' - Inactivo
        WHERE SGBSTDN_PIDM = P_ID_ALUMNO
        AND SGBSTDN_TERM_CODE_EFF = P_PERIODO;
        
        DBMS_OUTPUT.PUT_LINE('Actuazliado PERIODO ' || SQL%ROWCOUNT);
        
        --
        COMMIT;
EXCEPTION
  WHEN OTHERS THEN
        --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
        P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
 END;     




SELECT  * FROM SPRIDEN WHERE SPRIDEN_PIDM = '158839'; --70230582 -- plan dos
SELECT * FROM SFTREGS;

SELECT * FROM TBRACCD WHERE TBRACCD_PIDM = 271133;
SELECT  * FROM SPRIDEN WHERE SPRIDEN_PIDM = '439219'; --74851923
SELECT  * FROM SPRIDEN WHERE SPRIDEN_PIDM = '285642'; --76135399
SELECT  * FROM SPRIDEN WHERE SPRIDEN_PIDM = '190231'; --73991074 ------
SELECT  * FROM SPRIDEN WHERE SPRIDEN_PIDM = '271133'; --60184441
SELECT  * FROM SPRIDEN WHERE SPRIDEN_PIDM = '340813'; --75383323
SELECT  * FROM SPRIDEN WHERE SPRIDEN_PIDM = '247091'; --72697938
SELECT  * FROM SPRIDEN WHERE SPRIDEN_PIDM = '386582'; --72730411

SELECT * FROM SFBETRM WHERE SFBETRM_PIDM = 340813;
SELECT * FROM SFRENSP WHERE SFRENSP_PIDM = 340813;

SELECT DISTINCT SFBETRM_PIDM FROM SFBETRM 
INNER JOIN SFRSTCR
ON sfbetrm.sfbetrm_term_code = sfrstcr.sfrstcr_term_code
AND sfbetrm.sfbetrm_pidm =  sfrstcr.sfrstcr_pidm
WHERE sfbetrm.sfbetrm_term_code = '201620';

select * FROM sfrbtch where sfrbtch_pidm = 439219;


-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
SET SERVEROUTPUT ON
DECLARE     P_ID_ALUMNO                 SPRIDEN.SPRIDEN_PIDM%TYPE        := 386582  ;
            P_PERIODO                   SFBETRM.SFBETRM_TERM_CODE%TYPE   := '201620';
            
            P_FECHA_STATUS              SFBETRM.SFBETRM_ASSESSMENT_DATE%TYPE;
            P_FECHA_REFUND              SFBETRM.SFBETRM_REFUND_DATE%TYPE;
            P_KEY_SEQNO                 SGRSTSP.SGRSTSP_KEY_SEQNO%TYPE;
            SAVE_ACT_DATE_OUT           VARCHAR2(100);
            RETURN_STATUS_IN_OUT        NUMBER;
            P_INDICADOR                 NUMBER;
BEGIN
--
      
        --::: PARTE 1 DE 2 ::::: SFAREGS - registro que determina al alumno Elegible 
        SELECT SFBETRM_ASSESSMENT_DATE,SFBETRM_REFUND_DATE INTO P_FECHA_STATUS,P_FECHA_REFUND  -- SFBETRM_ESTS_DATE
        FROM SFBETRM 
        WHERE SFBETRM_PIDM = P_ID_ALUMNO
        AND SFBETRM_TERM_CODE = P_PERIODO;
      
      -- #######################################################################
      -- quitar NRCs
          -- Student Course Registration Archive Table.
--          DELETE FROM SFRSTCA 
--          WHERE SFRSTCA_PIDM = P_ID_ALUMNO
--          AND SFRSTCA_TERM_CODE = P_PERIODO;
--          DBMS_OUTPUT.PUT_LINE('del curse A : ' || SQL%ROWCOUNT);
          
                -- Eliminar registros de CRN'S
                -- SFASLST ::: Student Course Registration Repeating Table 
                DELETE FROM SFRSTCR 
                WHERE SFRSTCR_PIDM = P_ID_ALUMNO
                AND SFRSTCR_TERM_CODE = P_PERIODO; 
          DBMS_OUTPUT.PUT_LINE('del curse B : ' || SQL%ROWCOUNT);
          COMMIT;
      
                                    SELECT COUNT(*) INTO P_INDICADOR 
                                    FROM TBRACCD WHERE TBRACCD_PIDM = P_ID_ALUMNO AND TBRACCD_TERM_CODE = P_PERIODO;
                                    DBMS_OUTPUT.PUT_LINE('TBRACCD ---> ' || P_INDICADOR);

        -- #######################################################################
        -- AMORTIGUAR DEUDA
        SFKFEES.P_PROCESS_ETRM_DROP(  P_ID_ALUMNO, 
                                      P_PERIODO, 
                                      SYSDATE );
      
      
      -- #######################################################################
      -- Procesar DEUDA
      SFKFEES.P_PROCESSFEEASSESSMENT(   P_PERIODO,
                                        P_ID_ALUMNO,
                                        NULL,
                                        SYSDATE,
                                        'R',
                                        'N',
                                        'SFAREGS',    
                                        'Y',
                                        SAVE_ACT_DATE_OUT,
                                        'N',
                                        RETURN_STATUS_IN_OUT); 
                                        
      
            -- SFAREGS - Plan de estudio de matricula y selecion de NRCs
            -- Borramos Study_path
            DELETE FROM SFRENSP 
            WHERE SFRENSP_PIDM = P_ID_ALUMNO
            AND SFRENSP_TERM_CODE = P_PERIODO; 
        DBMS_OUTPUT.PUT_LINE('del PlanCourse : ' || SQL%ROWCOUNT);
        COMMIT;
      
                                    SELECT COUNT(*) INTO P_INDICADOR 
                                    FROM TBRACCD WHERE TBRACCD_PIDM = P_ID_ALUMNO AND TBRACCD_TERM_CODE = P_PERIODO;
                                    DBMS_OUTPUT.PUT_LINE('TBRACCD ---> ' || P_INDICADOR);
      
             -- Borramos Encabezado de Periodo
             -- ::: PARTE 1 DE 2 ::::: SFAREGS - registro que determina al alumno Elegible 
              DELETE FROM SFBETRM 
              WHERE SFBETRM_PIDM = P_ID_ALUMNO
              AND SFBETRM_TERM_CODE = P_PERIODO;  
        DBMS_OUTPUT.PUT_LINE('del header "EL" : ' || SQL%ROWCOUNT);
        COMMIT;
 
                                    SELECT COUNT(*) INTO P_INDICADOR 
                                    FROM TBRACCD WHERE TBRACCD_PIDM = P_ID_ALUMNO AND TBRACCD_TERM_CODE = P_PERIODO;
                                    DBMS_OUTPUT.PUT_LINE('TBRACCD ---> ' || P_INDICADOR);

      


--             -- Borramos Encabezado de Periodo
--             -- ::: PARTE 1 DE 2 ::::: SFAREGS - registro que determina al alumno Elegible 
--              DELETE FROM SFBETRM 
--              WHERE SFBETRM_PIDM = P_ID_ALUMNO
--              AND SFBETRM_TERM_CODE = P_PERIODO;  
--        DBMS_OUTPUT.PUT_LINE('del header "EL" 2° : ' || SQL%ROWCOUNT);
--        COMMIT;
      
--      -- #######################################################################
--      -- CAMBIO ESTADO PLAN ESTUDIO  
--      SELECT  SGRSTSP_KEY_SEQNO INTO P_KEY_SEQNO
--      FROM SGRSTSP 
--      WHERE SGRSTSP_PIDM = 402197 
--      AND SGRSTSP_TERM_CODE_EFF = '201620' AND ROWNUM = 1
--      ORDER BY SGRSTSP_TERM_CODE_EFF DESC, SGRSTSP_KEY_SEQNO ASC;
--      
--      DBMS_OUTPUT.PUT_LINE(P_KEY_SEQNO);
--      
--      
--      UPDATE SGRSTSP 
--          SET SGRSTSP_STSP_CODE = 'RV' ------------------ ESTADOS STVSTSP
--      WHERE SGRSTSP_PIDM = 402197 
--      AND SGRSTSP_TERM_CODE_EFF = '201620' 
--      AND SGRSTSP_KEY_SEQNO = P_KEY_SEQNO; -- P_KEY_SEQNO
--        
--      DBMS_OUTPUT.PUT_LINE('Actualziado ESTADO PLAN ESTUDIO ' || SQL%ROWCOUNT);
--      
--      -- #######################################################################
--      -- CAMBIO ESTADO PERIODO  
--        
--      UPDATE SGBSTDN
--          SET SGBSTDN_STST_CODE = 'IS'  ------------------ ESTADOS STVSTST 
--      WHERE SGBSTDN_PIDM = 402197
--      AND SGBSTDN_TERM_CODE_EFF = '201620';
--      
--      DBMS_OUTPUT.PUT_LINE('Actuazliado PERIODO ' || SQL%ROWCOUNT);
END;





