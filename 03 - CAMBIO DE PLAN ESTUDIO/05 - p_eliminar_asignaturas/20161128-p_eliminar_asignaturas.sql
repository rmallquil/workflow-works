DECLARE P_ERROR                   VARCHAR2(100);
BEGIN
    P_ELIMINAR_ASIGNATURAS('386582','201620',P_ERROR);
    DBMS_OUTPUT.PUT_LINE('ERROR :' || P_ERROR || ' -- AFECTADOS :' || SQL%ROWCOUNT);
END;





CREATE OR REPLACE PROCEDURE P_ELIMINAR_ASIGNATURAS( 
      P_ID_ALUMNO               IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_PERIODO                 IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
      P_ERROR                   OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_ELIMINAR_ASIGNATURAS
  FECHA     : 28/11/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Eliminar los NRC's.            

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
AS
            P_KEY_SEQNO                 SGRSTSP.SGRSTSP_KEY_SEQNO%TYPE;
            SAVE_ACT_DATE_OUT           VARCHAR2(100);
            RETURN_STATUS_IN_OUT        NUMBER;            
BEGIN
      
      -- #######################################################################          
      -- VALIDAR - "estimaciones IN-LINE"
      -- #######################################################################          
      

      -- #######################################################################          
      -- Eliminar registros de CRN'S
      -- SFASLST ::: Student Course Registration Repeating Table 
      DELETE FROM SFRSTCR 
      WHERE SFRSTCR_PIDM = P_ID_ALUMNO
      AND SFRSTCR_TERM_CODE = P_PERIODO; 
        
        
        --
        COMMIT;
EXCEPTION
    WHEN OTHERS THEN
          --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_ELIMINAR_ASIGNATURAS;   