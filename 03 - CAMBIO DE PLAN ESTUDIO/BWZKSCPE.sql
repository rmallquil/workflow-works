/**********************************************************************************************/
/* BWZKSCPE.sql                                                                               */
/**********************************************************************************************/
/*                                                                                            */
/* Descripción corta: Script para generar el Paquete de automatizaci�n del proceso de         */
/*                    Solicitud de Cambio de Plan de Estudios.                                */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI                FECHA    */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creación del Código.                                        RML             04/ENE/2017 */
/*    --------------------                                                                    */
/*    Creación del paquete de Cambio de Plan de Estudios                                      */
/*    Procedure P_VERIFICAR_APTO: Verifica que el estudiante solicitante                      */
/*              cumpla las condiciones para hacer uso del WF.                                 */
/*    Procedure P_GET_USUARIO: En base a una sede y un rol, el procedimiento LOS CORREOS del  */
/*              responsable(s)de Registros Académicos de esa sede.                            */          
/*    --Paquete generico                                                                      */
/*    Procedure P_VERIFICAR_ESTADO_SOLICITUD: Valida el estado de la solicitud.               */
/*    --Paquete generico                                                                      */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud (XXXXXX).         */
/*    Procedure P_OBTENER_COMENTARIO_ALUMNO: Obtiene COMENTARIO Y TELEFONO de una solicitud.  */
/*    --Paquete generico                                                                      */
/*    Procedure P_MODIFICAR_CATALOGO: Modifica EL PERIODO CATALOGO.                           */
/*    Procedure P_MODIFICAR_RETENCION: Modifica la fecha limite de una retencion a un dia     */
/*              antes a la fecha actual.                                                      */
/*    --Paquete generico                                                                      */
/*    Procedure P_SET_ATRIBUTO_PLANS : SET ATRIBUTO de PLAN ESTUDIOS a un periodo determinado.*/
/*    --Paquete generico                                                                      */
/*    Procedure P_SET_CURSO_INTROD: Asigna los cursos introductorios de RM y RV o PR con nota */
/*              11 solo en caso no tuviera alguno.                                            */
/*    --Paquete generico                                                                      */
/*    Procedure P_CPE_CONV_ASIGN: Convalidad las asignaturas del plan 2007 al plan 2015.      */
/*    --Paquete generico                                                                      */
/*    Procedure P_CPE_EXECCAPP: Ejecución de CAPP.                                            */
/*    --Paquete generico                                                                      */
/*    Procedure P_CPE_EXECPROY: Ejecución de proyección.                                      */
/*    Procedure P_APEC_SET_DEUDA: SP - ANIDADO genera una deuda por CODIGO DETALLE --- APEC.  */
/*    --Paquete generico                                                                      */
/*    Procedure P_GET_ASIG_RECONOCIDAS: Muestra el listado de asignaturas reconocidas.        */
/*                                                                                            */
/* 2. Actualización P_CPE_CONV_ASIGN                           LAM/HGH            02/MAR/2018 */
/*    Se agrega este paquete ya que ahora el cambio de plan de estudio será una convalidación */
/* 3. Creación P_VERIFICAR_APTO                                   BFV             05/JUL/2018 */
/*    Se agrega este paquete para verificar que cumpla las condiciones requeridad de uso.     */
/*    Creación P_GET_ASIG_RECONOCIDAS                                                         */
/*    Se agrega este paquete para que muestre el listado de asignaturas reconocidas.          */
/*    Creación P_VERIFICAR_ESTADO_SOLICITUD                                                   */
/*    Valida el estado de la solicitud.                                                       */
/* 4. Actualizacion Paquete generico                              BFV             11/OCT/2018 */
/*    Se reemplaza algunos SP's para que sean consumidos desde el paquete generico.           */
/* 5. Actualizacion P_VERIFICA_APTO                               LAM             28/DIC/2018 */
/*    Se agrega el parámetro p_respuesta (confirmación del estudiante de realizar el proceso).*/
/*    Creacion P_OBTENER_RESPUESTA                                                            */
/*    Obtiene la confirmación del estudiante para la eliminación de asignaturas matriculadas. */
/*    Reemplazo de SP                                                                         */
/*    Se elimina el sp P_OBTENER_COMENTARIO, que es reemplazado por el sp P_OBTENER_RESPUESTA.*/
/* 6. Actualizacion Consulta                                      BFV             31/ENE/2019 */
/*    Se actualiza la consulta para obtener la PARTE PERIODO en todo el paquete.              */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/

-- PERMISOS DE EJECUCIÓN
/**********************************************************************************************/
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/

CREATE OR REPLACE PACKAGE BWZKSCPE AS

PROCEDURE P_VERIFICA_APTO (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_FOLIO_SOLICITUD IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_STATUS_APTO     OUT VARCHAR2,
        P_REASON          OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_USUARIO (
    P_COD_SEDE          IN STVCAMP.STVCAMP_CODE%TYPE,
    P_ROL               IN WORKFLOW.ROLE.NAME%TYPE,
    P_CORREO_REGACA     OUT VARCHAR2,
    P_ROL_SEDE          OUT VARCHAR2,
    P_ERROR             OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_ESTADO_SOLICITUD (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_STATUS_SOL          OUT VARCHAR2,
    P_ERROR               OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
    P_AN                  OUT NUMBER,
    P_ERROR               OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_INSCRIP (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_STATUS_INSCRIP  OUT VARCHAR2,
    P_MESSAGE         OUT VARCHAR2
);

   
--------------------------------------------------------------------------------

PROCEDURE P_ELIMINAR_NRC (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_MESSAGE         OUT VARCHAR2
    
);



--------------------------------------------------------------------------------

PROCEDURE P_OBTENER_RESPUESTA (
      P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_COMENTARIO          OUT VARCHAR2,
      P_TELEFONO            OUT VARCHAR2,
      P_RESPUESTA           OUT VARCHAR2,
      P_MENSAJE             OUT VARCHAR2
);

--------------------------------------------------------------------------------

/*
-- 201720 - No se utiliza
PROCEDURE P_OBTENER_PRIORIDAD (
                  P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
                  P_COD_DETALLE         IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE, ------------- APEC
                  P_PERIODO             IN SFBETRM.SFBETRM_TERM_CODE%TYPE, ------------- APEC
                  P_PRIORIDAD           OUT SVRRSRV.SVRRSRV_SEQ_NO%TYPE, ------------ Sequence number of the Service Rule
                  P_ERROR               OUT VARCHAR2 ----------------------------------- APEC
              );
*/

--------------------------------------------------------------------------------

/*
-- 201720 - No se utiliza
PROCEDURE P_GET_SIDEUDA_ALUMNO (
          P_PIDM_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
          P_CODIGO_DETALLE      IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE,
          P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE, ------ APEC
          P_DEUDA               OUT VARCHAR2
        );
*/

--------------------------------------------------------------------------------

/*
-- 201720 - No se utiliza
PROCEDURE P_ELIMINAR_RECARGO (
        P_PIDM_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_NUMERO_SOLICITUD      IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_TERM_PERIODO          IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE, ------ APEC
        P_CODIGO_DETALLE        IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE, ------ APEC
        P_ERROR                 OUT VARCHAR2
    );
*/

--------------------------------------------------------------------------------

PROCEDURE P_MODIFICAR_CATALOGO (
    P_PIDM_ALUMNO      IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PERIODO          IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
    P_ERROR            OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_MODIFICAR_RETENCION_B (
    P_PIDM                IN  SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_STVHLDD_CODE1       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE2       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE3       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE4       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_MESSAGE             OUT VARCHAR2
);

--------------------------------------------------------------------------------

/*
-- 201720 - No se utiliza
PROCEDURE P_VALIDAR_FECHA_SOLICITUD ( 
            P_CODIGO_SOLICITUD    IN SVVSRVC.SVVSRVC_CODE%TYPE,
            P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
            P_FECHA_VALIDA        OUT VARCHAR2
        );
*/

--------------------------------------------------------------------------------

PROCEDURE P_SET_ATRIBUTO_PLANS (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_ATTS_CODE         IN STVATTS.STVATTS_CODE%TYPE,      -------- COD atributos
    P_ERROR             OUT VARCHAR2
);

-------------------------------------------------------------------------------

PROCEDURE P_SET_CURSO_INTROD (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
    P_MESSAGE             OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_CPE_CONV_ASIGN(
    P_TERM_CODE   IN STVTERM.STVTERM_CODE%TYPE,
    P_CATALOGO    IN STVTERM.STVTERM_CODE%TYPE,
    P_PIDM        IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_MESSAGE     OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_CPE_EXECCAPP (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_MESSAGE           OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_CPE_EXECPROY (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_MESSAGE           OUT VARCHAR2
);

--------------------------------------------------------------------------------
-- SP Anidado
PROCEDURE P_APEC_SET_DEUDA ( 
    P_PIDM_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_COD_DETALLE           IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE,
    P_PERIODO               IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
    P_ERROR                 OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_ASIG_RECONOCIDAS (
    P_PIDM          IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_HTML1         OUT VARCHAR2,
    P_HTML2         OUT VARCHAR2,
    P_HTML3         OUT VARCHAR2
);

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END BWZKSCPE;

/**********************************************************************************************/
--  /
--  show errors 
-- 
--  SET SCAN ON
--  WHENEVER SQLERROR CONTINUE
--  DROP PUBLIC SYNONYM BWZKSCPE;
--  WHENEVER SQLERROR EXIT ROLLBACK
--  CREATE PUBLIC SYNONYM BWZKSCPE FOR BANINST1.BWZKSCPE;
--  WHENEVER SQLERROR CONTINUE
--  START gurgrtb BWZKSCPE
--  START gurgrth BWZKSCPE
--  WHENEVER SQLERROR EXIT ROLLBACK
/**********************************************************************************************/