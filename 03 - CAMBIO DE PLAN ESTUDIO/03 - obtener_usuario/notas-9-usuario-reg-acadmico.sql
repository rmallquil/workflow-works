----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------
/*
    Se hizo mencion de que los roles serian identificados por el nombre concadenado el codigo de la SEDE.
    Aun sigue pendiente algun ejemplo en WORKFLOW, ya que es el lugar donde se realizaran la cosnsulta de
    usuarios por su rol y sede.
*/

SELECT id,WORKFLOW.ROLE.* FROM WORKFLOW.ROLE;
SELECT id,organization.* FROM WORKFLOW.organization;
SELECT id,WFUSER.* FROM WORKFLOW.WFUSER;

SELECT id,org_id,role_id,user_id, ROLE_ASSIGNMENT.* FROM WORKFLOW.ROLE_ASSIGNMENT;
select Stvcamp.Stvcamp_Code from Stvcamp;



create or replace PROCEDURE p_get_usuario_regacad (
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
      P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
      P_NOMBRE              OUT VARCHAR2,
      P_CORREO_REGACA       OUT VARCHAR2,
      P_USUARIO_REGACA      OUT VARCHAR2,
      P_ROL_SEDE            OUT VARCHAR2,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_get_usuario_regaca
  FECHA     : 29/09/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : en base a una sede y un rol, el procedimiento obtenga los datos del responsable de Registro académico de esa sede.

  =================================================================================================================== */
AS
      -- @PARAMETERS
      P_ROLE_ID                 NUMBER;
      P_ORG_ID                  NUMBER;
      P_USER_ID                 NUMBER;
      P_ROLE_ASSIGNMENT_ID      NUMBER;
    
      P_SECCION_EXCEPT          VARCHAR2(50);
BEGIN 
--
      P_ROL_SEDE := P_ROL || P_COD_SEDE;
      
      -- Obtener el ROL_ID 
      P_SECCION_EXCEPT := 'ROLES';
      SELECT ID INTO P_ROLE_ID FROM WORKFLOW.ROLE WHERE NAME = P_ROL_SEDE;
      P_SECCION_EXCEPT := '';
      
      -- Obtener el ORG_ID 
      P_SECCION_EXCEPT := 'ORGRANIZACION';
      SELECT ID INTO P_ORG_ID FROM WORKFLOW.ORGANIZATION WHERE NAME = 'Root';
      P_SECCION_EXCEPT := '';
     
      -- Obtener el id usuario en la tabla que relaciona rol y usuario
      P_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
      SELECT USER_ID INTO P_USER_ID FROM WORKFLOW.ROLE_ASSIGNMENT 
      WHERE ORG_ID = P_ORG_ID AND ROLE_ID = P_ROLE_ID;
      P_SECCION_EXCEPT := '';
      
      -- Obtener Datos Usuario
      SELECT(First_Name || ' ' || Last_Name) names, Email_Address, Logon 
      INTO P_NOMBRE, P_CORREO_REGACA, P_USUARIO_REGACA
      FROM WORKFLOW.WFUSER 
      WHERE ID = P_USER_ID ;
      
      P_ERROR := 'Satisfactorio';
      
EXCEPTION
  WHEN TOO_MANY_ROWS THEN 
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              -- --DBMS_OUTPUT.PUT_LINE ();
              P_ERROR := 'A ocurrido un problema, se encontraron mas de un ROL con el mismo nombre: ' || P_ROL || P_COD_SEDE;
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_ERROR := 'A ocurrido un problema, se encontraron mas de una ORGANIZACIÒN con el mismo nombre.';
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := 'A ocurrido un problema, Se encontraron mas de un usuario con el mismo ROL.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
  WHEN NO_DATA_FOUND THEN
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              P_ERROR := 'A ocurrido un problema, NO se encontrò el nombre del ROL: ' || P_ROL || P_COD_SEDE;
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_ERROR := 'A ocurrido un problema, NO se encontrò el nombre de la ORGANIZACION.';
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := 'A ocurrido un problema, NO  se encontrò ningun usuario con esas caracteristicas.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
  WHEN OTHERS THEN
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END p_get_usuario_regacad;


----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------


DECLARE
      -- IN
      P_COD_SEDE                STVCAMP.STVCAMP_CODE%TYPE   := 'S01';
      P_ROL                     WORKFLOW.ROLE.NAME%TYPE     := 'Registro';  
      -- OUT
      P_NOMBRE                  VARCHAR2(255);
      P_CORREO_REGACA           VARCHAR2(255);
      P_USUARIO_REGACA          VARCHAR2(255);
      P_ERROR                   VARCHAR2(255);
      -- @PARAMETERS
      P_ROLE_ID                 NUMBER;
      P_ORG_ID                  NUMBER;
      P_USER_ID                 NUMBER;
      P_ROLE_ASSIGNMENT_ID      NUMBER;
      
      P_INDICADOR               NUMBER;
      P_MESSAGE                 EXCEPTION;
      P_SECCION_EXCEPT          VARCHAR2(50);
BEGIN 
--
      -- Obtener el ROL_ID 
      P_SECCION_EXCEPT := 'ROLES';
      SELECT ID INTO P_ROLE_ID FROM WORKFLOW.ROLE WHERE NAME = P_ROL || P_COD_SEDE;
      P_SECCION_EXCEPT := '';
      
      -- Obtener el ORG_ID 
      P_SECCION_EXCEPT := 'ORGRANIZACION';
      SELECT ID INTO P_ORG_ID FROM WORKFLOW.ROLE WHERE NAME = 'Root';
      P_SECCION_EXCEPT := '';
     
      -- Obtener el id usuario en la tabla que relaciona rol y usuario
      P_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
      SELECT USER_ID INTO P_USER_ID FROM WORKFLOW.ROLE_ASSIGNMENT 
      WHERE ORG_ID = P_ORG_ID AND ROLE_ID = P_ROLE_ID;
      P_SECCION_EXCEPT := '';
      
      -- Obtener Datos Usuario
      SELECT(First_Name || ' ' || Last_Name) names, Email_Address, Logon 
      INTO P_NOMBRE, P_CORREO_REGACA, P_USUARIO_REGACA
      FROM WORKFLOW.WFUSER 
      WHERE ID = P_USER_ID ;
      
      P_ERROR := 'Satisfactorio';
      
EXCEPTION
  WHEN TOO_MANY_ROWS THEN 
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              P_ERROR := 'A ocurrido un problema, se encontraron mas de un ROL con el mismo nombre: ' || P_ROL || P_COD_SEDE;
              --DBMS_OUTPUT.PUT_LINE('A ocurrido un problema, se encontraron mas de un ROL con el mismo nombre: ' || P_ROL || P_COD_SEDE);
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              DBMS_OUTPUT.PUT_LINE('A ocurrido un problema, se encontraron mas de una ORGANIZACIÒN con el mismo nombre.');
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              DBMS_OUTPUT.PUT_LINE('A ocurrido un problema, Se encontraron mas de un usuario con el mismo ROL.');
          ELSE  DBMS_OUTPUT.PUT_LINE(SQLERRM);
          END IF; 
  WHEN NO_DATA_FOUND THEN
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              DBMS_OUTPUT.PUT_LINE('A ocurrido un problema, NO se encontrarò el nombre del ROL: ' || P_ROL || P_COD_SEDE);
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              DBMS_OUTPUT.PUT_LINE('A ocurrido un problema, NO se encontrò el nombre de la ORGANIZACION.');
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              DBMS_OUTPUT.PUT_LINE('A ocurrido un problema, NO  se encontrò ningun usuario con esas caracteristicas.');
          ELSE  DBMS_OUTPUT.PUT_LINE(SQLERRM);
          END IF; 
  WHEN OTHERS THEN
          DBMS_OUTPUT.PUT_LINE('A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM);
END;


