/*
SET SERVEROUTPUT ON
DECLARE   P_TRAN_NUMBER           TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
          P_ERROR                 VARCHAR2(100);
BEGIN
    P_APEC_SET_DEUDA(241106,'SME','201620',P_ERROR);
    COMMIT;
    DBMS_OUTPUT.PUT_LINE('--' || P_ERROR);
END;
*/


CREATE OR REPLACE PROCEDURE P_APEC_SET_DEUDA ( 
        P_PIDM_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_COD_DETALLE           IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE,
        P_PERIODO               IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
        P_ERROR                 OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_APEC_SET_DEUDA
  FECHA     : 19/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : genera una deuda por CODIGO DETALLE --- APEC.

            : COMMIT - SP ANIDADO, El "COMMIT" se realiza desde el SP principal.
            
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
        
        C_SEQNO                   SORLCUR.SORLCUR_SEQNO %type;
        C_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;
        C_ALUM_CAMPUS             SORLCUR.SORLCUR_CAMP_CODE%type;
        C_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
        C_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
        C_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
        C_ALUM_PERD_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
        C_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP
        C_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE
        C_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%type;

        P_SERVICE               VARCHAR2(10);
        P_PART_PERIODO          VARCHAR2(9);
        P_APEC_CAMP             VARCHAR2(9);
        P_APEC_DEPT             VARCHAR2(9);
        P_APEC_TERM             VARCHAR2(10);
        P_APEC_IDSECCIONC       VARCHAR2(15);
        P_APEC_FECINIC          DATE;
        P_APEC_MOUNT            NUMBER;
        P_APEC_IDALUMNO         VARCHAR2(10);
        
BEGIN

      -- #######################################################################
      -- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
      SELECT    SORLCUR_SEQNO,      
                SORLCUR_LEVL_CODE,    
                SORLCUR_CAMP_CODE,        
                SORLCUR_COLL_CODE,
                SORLCUR_DEGC_CODE,  
                SORLCUR_PROGRAM,      
                SORLCUR_TERM_CODE_ADMIT,  
                SORLCUR_STYP_CODE,  
                SORLCUR_RATE_CODE,    
                SORLFOS_DEPT_CODE   
      INTO      C_SEQNO,
                C_ALUM_NIVEL, 
                C_ALUM_CAMPUS, 
                C_ALUM_ESCUELA, 
                C_ALUM_GRADO, 
                C_ALUM_PROGRAMA, 
                C_ALUM_PERD_ADM,
                C_ALUM_TIPO_ALUM,
                C_ALUM_TARIFA,
                C_ALUM_DEPARTAMENTO
      FROM (
              SELECT    SORLCUR_SEQNO,      SORLCUR_LEVL_CODE,    SORLCUR_CAMP_CODE,        SORLCUR_COLL_CODE,
                        SORLCUR_DEGC_CODE,  SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  --SORLCUR_STYP_CODE,
                        SORLCUR_STYP_CODE,  SORLCUR_RATE_CODE,    SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM_ALUMNO 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
                  ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM <= 1;
    

      -- GET CRONOGRAMA SECCIONC
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END
      INTO P_PART_PERIODO
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = C_ALUM_DEPARTAMENTO AND STVCAMP_CODE = C_ALUM_CAMPUS;
      
      SELECT CZRCAMP_CAMP_BDUCCI INTO P_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = C_ALUM_CAMPUS;-- CAMPUS 
      SELECT CZRTERM_TERM_BDUCCI INTO P_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_PERIODO ;-- PERIODO
      SELECT CZRDEPT_DEPT_BDUCCI INTO P_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = C_ALUM_DEPARTAMENTO;-- DEPARTAMENTO
      
      -- GET DATOS CTA CORRIENTE -- P_APEC_IDALUMNO, P_APEC_IDSECCIONC
      WITH 
          CTE_tblSeccionC AS (
                  -- GET SECCIONC
                  SELECT  "IDSeccionC" IDSeccionC,
                          "FecInic" FecInic
                  FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                  WHERE "IDDependencia"='UCCI'
                  AND "IDsede"    = P_APEC_CAMP
                  AND "IDPerAcad" = P_APEC_TERM
                  AND "IDEscuela" = C_ALUM_PROGRAMA
                  AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                  AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                  AND "IDSeccionC" <> '15NEX1A'
                  AND SUBSTRB("IDSeccionC",-2,2) IN (
                      -- PARTE PERIODO           
                      SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (P_PART_PERIODO || '%')
                  )
          ),
          CTE_tblPersonaAlumno AS (
                  SELECT "IDAlumno" IDAlumno 
                  FROM  dbo.tblPersonaAlumno@BDUCCI.CONTINENTAL.EDU.PE 
                  WHERE "IDPersona" IN ( 
                      SELECT "IDPersona" FROM dbo.tblPersona@BDUCCI.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM_ALUMNO
                  )
          )
      SELECT "IDAlumno", "IDSeccionC" INTO P_APEC_IDALUMNO, P_APEC_IDSECCIONC
      FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
      WHERE "IDDependencia" = 'UCCI'
      AND "IDAlumno"    IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
      AND "IDSede"      = P_APEC_CAMP
      AND "IDPerAcad"   = P_APEC_TERM
      AND "IDEscuela"   = C_ALUM_PROGRAMA
      AND "IDSeccionC"  IN ( SELECT IDSeccionC FROM CTE_tblSeccionC )
      AND "IDConcepto"  = P_COD_DETALLE;
      
      -- GET MONTO 
      SELECT "Monto" INTO P_APEC_MOUNT
      FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE 
      WHERE "IDDependencia" = 'UCCI'
      AND "IDSede"      = P_APEC_CAMP
      AND "IDPerAcad"   = P_APEC_TERM
      AND "IDSeccionC"  = P_APEC_IDSECCIONC
      AND "IDConcepto"  = P_COD_DETALLE;
      
      -- UPDATE MONTO(s)
      UPDATE dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
      SET "Cargo" = "Cargo" + P_APEC_MOUNT
      WHERE "IDDependencia" = 'UCCI'
      AND "IDAlumno"    = P_APEC_IDALUMNO
      AND "IDSede"      = P_APEC_CAMP
      AND "IDPerAcad"   = P_APEC_TERM
      AND "IDEscuela"   = C_ALUM_PROGRAMA
      AND "IDSeccionC"  = P_APEC_IDSECCIONC
      AND "IDConcepto"  = P_COD_DETALLE;
            
      -- COMMIT; -- SP ANIDADO, "COMMIT" SE REALIZA DESDE EL SP PRINCIPAL.
EXCEPTION
  WHEN OTHERS THEN
        P_ERROR := 'Error o inconsistencia detectada P_APEC_SET_DEUDA - '||SQLCODE||' -ERROR- '||SQLERRM;
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_APEC_SET_DEUDA;