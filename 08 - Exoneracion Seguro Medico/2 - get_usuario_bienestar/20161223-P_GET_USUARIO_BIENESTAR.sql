
/*
drop procedure P_OBTENER_USUARIO;
GRANT EXECUTE ON P_OBTENER_USUARIO TO wfobjects;
GRANT EXECUTE ON P_OBTENER_USUARIO TO wfauto;

set serveroutput on
DECLARE
-- OUT
p_nombre                  VARCHAR2(255);
p_correo                  VARCHAR2(255);
p_usuario                 VARCHAR2(255);
P_ROL_SEDE                VARCHAR2(255);
p_error                   VARCHAR2(255);
begin
  P_GET_USUARIO_BIENESTAR('03','adminisiones',P_NOMBRE,p_correo,p_usuario,P_ROL_SEDE,P_ERROR);
  DBMS_OUTPUT.PUT_LINE(P_NOMBRE || p_correo || p_usuario || '---' || p_error);
end;

*/

---------------------------------------------------------------------------------------------------------------------------------------------------------------------




create or replace PROCEDURE P_GET_USUARIO_BIENESTAR (
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
      P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
      P_NOMBRE              OUT VARCHAR2,
      P_CORREO              OUT VARCHAR2,
      P_USUARIO             OUT VARCHAR2,
      P_ROL_SEDE            OUT VARCHAR2,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_USUARIO_BIENESTAR
  FECHA     : 23/12/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : en base a una sede y un rol, el procedimiento obtenga los datos del responsable de UNA SEDE.

  =================================================================================================================== */
AS
      -- @PARAMETERS
      P_ROLE_ID                 NUMBER;
      P_ORG_ID                  NUMBER;
      P_USER_ID                 NUMBER;
      P_ROLE_ASSIGNMENT_ID      NUMBER;
    
      P_SECCION_EXCEPT          VARCHAR2(50);
BEGIN 
--
      P_ROL_SEDE := P_ROL || P_COD_SEDE;
      
      -- Obtener el ROL_ID 
      P_SECCION_EXCEPT := 'ROLES';
      SELECT ID INTO P_ROLE_ID FROM WORKFLOW.ROLE WHERE NAME = P_ROL_SEDE;
      P_SECCION_EXCEPT := '';
      
      -- Obtener el ORG_ID 
      P_SECCION_EXCEPT := 'ORGRANIZACION';
      SELECT ID INTO P_ORG_ID FROM WORKFLOW.ORGANIZATION WHERE NAME = 'Root';
      P_SECCION_EXCEPT := '';
     
      -- Obtener el id usuario en la tabla que relaciona rol y usuario
      P_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
      SELECT USER_ID INTO P_USER_ID FROM WORKFLOW.ROLE_ASSIGNMENT 
      WHERE ORG_ID = P_ORG_ID AND ROLE_ID = P_ROLE_ID;
      P_SECCION_EXCEPT := '';
      
      -- Obtener Datos Usuario
      SELECT(First_Name || ' ' || Last_Name) names, Email_Address, Logon 
      INTO P_NOMBRE, P_CORREO, P_USUARIO
      FROM WORKFLOW.WFUSER 
      WHERE ID = P_USER_ID ;
      
EXCEPTION
  WHEN TOO_MANY_ROWS THEN 
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              -- --DBMS_OUTPUT.PUT_LINE ();
              P_ERROR := 'A ocurrido un problema, se encontraron mas de un ROL con el mismo nombre: ' || P_ROL || P_COD_SEDE;
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_ERROR := 'A ocurrido un problema, se encontraron mas de una ORGANIZACIÒN con el mismo nombre.';
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := 'A ocurrido un problema, Se encontraron mas de un usuario con el mismo ROL.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
  WHEN NO_DATA_FOUND THEN
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              P_ERROR := 'A ocurrido un problema, NO se encontrò el nombre del ROL: ' || P_ROL || P_COD_SEDE;
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_ERROR := 'A ocurrido un problema, NO se encontrò el nombre de la ORGANIZACION.';
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := 'A ocurrido un problema, NO  se encontrò ningun usuario con esas caracteristicas.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
  WHEN OTHERS THEN
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_GET_USUARIO_BIENESTAR;

       

       
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------