create or replace PACKAGE WFK_CONTISESM AS
/*******************************************************************************
 WFK_CONTISESM:
       Conti Package SOLICITUD DE EXONERACIÓN DE SEGURO DE SALUD
*******************************************************************************/
-- FILE NAME..: WFK_CONTISESM.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_CONTISESM
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/

PROCEDURE P_GET_USUARIO_BIENESTAR (
                P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
                P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
                P_CORREO              OUT VARCHAR2,
                P_ROL_SEDE            OUT VARCHAR2,
                P_ERROR               OUT VARCHAR2
            );

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
              P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
              P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
              P_AN                  OUT NUMBER,
              P_ERROR               OUT VARCHAR2
          );
              
--------------------------------------------------------------------------------

PROCEDURE P_OBTENER_COMENTARIO_ALUMNO (
              P_NUMERO_SOLICITUD        IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
              P_COMENTARIO_ALUMNO       OUT VARCHAR2
          );

--------------------------------------------------------------------------------

PROCEDURE P_REGISTRAR_SEGURO (
              P_PIDM_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
              P_PERIODO               IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
              P_COD_ATRIBUTO          IN STVATTS.STVATTS_CODE%TYPE,       -- COD atributos
              P_CODIGO_DETALLE        IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE, --------------- APEC
              P_ERROR                 OUT VARCHAR2
          );

--------------------------------------------------------------------------------

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END WFK_CONTISESM;