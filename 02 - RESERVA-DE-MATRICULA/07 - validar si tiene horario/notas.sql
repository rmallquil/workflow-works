
-- SFARGFE - SFTREGS CRN -- Temporary registration table, used as a workpad during the registration process.
SELECT * FROM SFTREGS WHERE SFTREGS_PIDM = 284082; -- INFO SFTREGS
-- DegreeWorks Temporary Registration Error Message Table.
  SELECT * FROM SFTDWER WHERE SFTDWER_PIDM = 284082;  -- INFO SFTDWER
  -- TIPO DE HORARIO (OFERTA)
  SELECT * FROM STVSCHD;
  -- Student Course Registration Archive Table.
  SELECT * FROM SFRSTCA WHERE SFRSTCA_PIDM = 284082; -- INFO SFRSTCA 
-- SFASLST ::: Student Course Registration Repeating Table 
SELECT * FROM SFRSTCR WHERE SFRSTCR_PIDM = 284082; -- INFO SFRSTCR 
  
  
SFRSTCR_ERROR_FLAG 
This field identifies an error associated with the registration of this CRN.  
Valid values are F=Fatal, D=Do not count in enrollment, L=WaitListed, O=Override, W=Warning, X=Delete (used only by SFRSTCR POSTUPDATE DB trigger)

SELECT * FROM SFRSTCA WHERE SFRSTCA_PIDM = 284082;
SELECT * FROM SFTREGS WHERE SFTREGS_PIDM = 284082;


SET SERVEROUTPUT ON
DECLARE   P_ID_ALUMNO             SPRIDEN.SPRIDEN_PIDM%TYPE             := 284082 ;
          P_PERIODO               SFBETRM.SFBETRM_TERM_CODE%TYPE        := '201620';
          P_INDICADOR             NUMBER;
          P_HORARIO               VARCHAR2(5);
BEGIN
-- 
    -- Return si alumno NRC
    SELECT COUNT(*) INTO P_INDICADOR
    FROM SFRSTCR 
    WHERE SFRSTCR_PIDM = P_ID_ALUMNO
    AND SFRSTCR_TERM_CODE = P_PERIODO;
    
    IF P_INDICADOR > 0 THEN
      P_HORARIO   := 'TRUE';
    ELSE
      P_HORARIO   :='FALSE';
    END IF;
    
    DBMS_OUTPUT.PUT_LINE(P_HORARIO);
    
END;


/*
SET SERVEROUTPUT ON
DECLARE   P_ID_ALUMNO             SPRIDEN.SPRIDEN_PIDM%TYPE             := 284082 ;
          P_PERIODO               SFBETRM.SFBETRM_TERM_CODE%TYPE        := '201620';
          P_DEUDA                 VARCHAR2(5);
BEGIN
    P_VALIDAR_SIHORARIO(284082,'201620',P_DEUDA);
    DBMS_OUTPUT.PUT_LINE(P_DEUDA);
END;
*/

CREATE OR REPLACE PROCEDURE P_VALIDAR_SIHORARIO (
          P_ID_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
          P_PERIODO           IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
          P_DEUDA             OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_VALIDAR_HORARIO
  FECHA     : 14/11/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Retornar TRUE/FALSE si alumno tiene NRCs (Horario)

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
AS
            P_INDICADOR             NUMBER;
BEGIN
-- 
    -- COUNT alumno NRCs
    SELECT COUNT(*) INTO P_INDICADOR
    FROM SFRSTCR 
    WHERE SFRSTCR_PIDM = P_ID_ALUMNO
    AND SFRSTCR_TERM_CODE = P_PERIODO;
    
    IF P_INDICADOR > 0 THEN
      P_DEUDA   := 'TRUE';
    ELSE
      P_DEUDA   :='FALSE';
    END IF;

END P_VALIDAR_SIHORARIO;
