/**********************************************************************************************/
/* BWZKSRMT.sql                                                                               */
/**********************************************************************************************/
/*                                                                                            */
/* Descripcion corta: Script para generar el Paquete de automatizacion del proceso de         */
/*                    Solicitud de Reserva de Matricula.                                      */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI                FECHA    */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creación del Código.                                        RML             19/ENE/2017 */
/*    --------------------                                                                    */
/*    Creación del paquete de Reserva de Matrícula                                            */
/*    Procedure P_VERIFICAR_APTO: Verifica que el estudiante solicitante                      */
/*              cumpla las condiciones para hacer uso del WF.                                 */
/*    Procedure P_GET_SIDEUDA_ALUMNO: El objetivo es que en base a un ID y un código de       */
/*              detalle verifique la existencia de deuda de MATRICULA del alumno en cualquier */
/*              periodo, divisa PEN.                                                          */          
/*    Procedure P_VALIDAR_FECHA_SOLICITUD: se verifiqua que solicitud del alumno del tipo     */
/*              <P_CODIGO_SOLICITUD>( ejem SOL003), se encuentra dentro del rango de fechas   */
/*              habiles para atender la solicitud.                                            */
/*    --Paquete generico                                                                      */
/*    Procedure P_VERIFICAR_ESTADO_SOLICITUD: Valida si el estudiante anulo la solicitud.     */
/*    --Paquete generico                                                                      */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud (XXXXXX).         */
/*    Procedure P_GET_USUARIO: En base a una sede y un rol, el procedimiento LOS CORREOS      */
/*             deL(os) responsable(s) de Registros Academicos de esa sede.                    */
/*    Procedure P_ELIMINAR_NRCS: Eliminar los NRC's , STUPDY PATH, Informaciob de Ingreso.    */
/*             Vuelve a estimar sus deudas cancelando sus deudas generadas(MONTOS EN NEGATIVO)*/
/*    Procedure P_OBTENER_COMENTARIO_ALUMNO: Obtiene EL COMENTARIO Y TELEFONO de una solicitud*/ 
/*              especifica.                                                                   */
/*    Procedure P_GET_PAGOS: Obtener el monto abonado de matricula y de las cuotas.           */
/*                                                                                            */
/* 2. Actualización                                                               02/ENE/2018 */
/*    Se renombra el paquete de WFK_CONTISRM a BWZKSRMT para cumplir con el standar           */
/*    Se elimina la funcion F_GET_SIDEUDA_ALUMNO, esta validando directamenhte en el          */
/*    SP P_GET_SIDEUDA_ALUMNO.                                                                */
/*    Se elimina el SP P_VALIDAR_SIHORARIO ya que no se utiliza dentro del flujo.             */
/* 3. Creacion P_VERIFICAR_APTO                                   BFV             05/JUL/2018 */
/*    Se agrega este paquete para verificar que cumpla las condiciones requeridad de uso.     */
/*    Actualizacion P_ELIMINAR_NRCS                                                           */
/*    Se actualiza el SP para que cuando no tenga ningun NRC matriculado pase directamente.   */
/*    Creacion P_VERIFICAR_ESTADO_SOLICITUD                                                   */
/*    Se aumenta el SP para que valide si el estudiante anulo o no su solicitud.              */
/* 4. Actualización Paquete Generico                              BYF             30/ENE/2019 */
/*    Se cambia los SP de Verificar estado y cambio de estado por lo que se encuentra en el   */
/*      paquete generico.                                                                     */
/*    Actualizacion P_VERIFICAR_APTO                                                          */
/*    Se aumenta la regla para que rechace a los estudiantes que sean de Beca 18.             */
/*    Actualizacion Consulta                                                                  */
/*    Se actualiza y mejora la consulta para obtener la PARTE PERIODO en todo el paquete.     */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/
/**********************************************************************************************/
-- PERMISOS DE EJECUCIÓN
/**********************************************************************************************/
--  SET SCAN ON 
-- 
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE 
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/

CREATE OR REPLACE PACKAGE BWZKSRMT AS

PROCEDURE P_VERIFICAR_APTO (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_STATUS_APTO     OUT VARCHAR2,
        P_REASON          OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_SIDEUDA_ALUMNO (
        P_PIDM_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_CODIGO_DETALLE      IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE,
        P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE,  --------- APEC
        P_DEUDA               OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VALIDAR_FECHA_SOLICITUD ( 
        P_CODIGO_SOLICITUD    IN SVVSRVC.SVVSRVC_CODE%TYPE,
        P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_FECHA_VALIDA        OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_ESTADO_SOLICITUD (
        P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_STATUS_SOL          OUT VARCHAR2,
        P_ERROR               OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
        P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
        P_AN                  OUT NUMBER,
        P_ERROR               OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_USUARIO (
        P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
        P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
        P_CORREO              OUT VARCHAR2,
        P_ROL_SEDE            OUT VARCHAR2,
        P_ERROR               OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_ELIMINAR_NRCS ( 
        P_PIDM_ALUMNO             IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_PERIODO                 IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
        P_CODE_DEPT               IN STVDEPT.STVDEPT_CODE%TYPE,
        P_CODE_CAMP               IN STVCAMP.STVCAMP_CODE%TYPE,
        P_NUMERO_SOLICITUD        IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_ERROR                   OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_OBTENER_COMENTARIO_ALUMNO (
        P_NUMERO_SOLICITUD        IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_COMENTARIO_ALUMNO       OUT VARCHAR2,
        P_TELEFONO_ALUMNO         OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_PAGOS (
        P_PIDM_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE,  --------- APEC
        P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_ABONO_MATRIC        OUT NUMBER,
        P_ABONO_CUOTAS        OUT NUMBER
);

--------------------------------------------------------------------------------

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END BWZKSRMT;
/**********************************************************************************************/
--  /
--  show errors 
-- 
--  SET SCAN ON
--  WHENEVER SQLERROR CONTINUE
--  DROP PUBLIC SYNONYM BWZKSRMT;
--  WHENEVER SQLERROR EXIT ROLLBACK
--  CREATE PUBLIC SYNONYM BWZKSRMT FOR BANINST1.BWZKSRMT;
--  WHENEVER SQLERROR CONTINUE
--  START gurgrtb BWZKSRMT
--  START gurgrth BWZKSRMT
--  WHENEVER SQLERROR EXIT ROLLBACK
/**********************************************************************************************/