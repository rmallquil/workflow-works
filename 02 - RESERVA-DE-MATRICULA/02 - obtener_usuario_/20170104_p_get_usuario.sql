
/*
drop procedure p_get_usuario_regaca;
GRANT EXECUTE ON p_get_usuario_regaca TO wfobjects;
GRANT EXECUTE ON p_get_usuario_regaca TO wfauto;

set serveroutput on
DECLARE
-- OUT
p_nombre                  VARCHAR2(255);
p_correo                  VARCHAR2(255);
p_usuario                 VARCHAR2(255);
P_ROL_SEDE                VARCHAR2(255);
p_error                   VARCHAR2(255);
begin
  WFK_CONTISRM.P_GET_USUARIO_BIENESTAR('03','adminisiones',P_NOMBRE,p_correo,p_usuario,P_ROL_SEDE,P_ERROR);
  DBMS_OUTPUT.PUT_LINE(P_NOMBRE || p_correo || p_usuario || '---' || p_error);
end;

*/

---------------------------------------------------------------------------------------------------------------------------------------------------------------------




create or replace PROCEDURE P_GET_USUARIO (
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
      P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
      P_CORREO              OUT VARCHAR2,
      P_ROL_SEDE            OUT VARCHAR2,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_GET_USUARIO
  FECHA     : 04/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento LOS CORREOS deL(os) responsable(s) 
              de Registros Académicos de esa sede.
  =================================================================================================================== */
AS
      -- @PARAMETERS
      P_ROLE_ID                 NUMBER;
      P_ORG_ID                  NUMBER;
      P_USER_ID                 NUMBER;
      P_ROLE_ASSIGNMENT_ID      NUMBER;
      P_Email_Address           VARCHAR2(100);
    
      P_SECCION_EXCEPT          VARCHAR2(50);
      
      CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = P_ORG_ID AND ROLE_ID = P_ROLE_ID;
      
      V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
BEGIN 
--
      P_ROL_SEDE := P_ROL || P_COD_SEDE;
      
      -- Obtener el ROL_ID 
      P_SECCION_EXCEPT := 'ROLES';
      SELECT ID INTO P_ROLE_ID FROM WORKFLOW.ROLE WHERE NAME = P_ROL_SEDE;
      P_SECCION_EXCEPT := '';
      
      -- Obtener el ORG_ID 
      P_SECCION_EXCEPT := 'ORGRANIZACION';
      SELECT ID INTO P_ORG_ID FROM WORKFLOW.ORGANIZATION WHERE NAME = 'Root';
      P_SECCION_EXCEPT := '';
     
      -- Obtener los datos de usuarios que relaciona rol y usuario
      P_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
       -- #######################################################################
      OPEN C_ROLE_ASSIGNMENT;
      LOOP
          FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
          EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
                      
                      -- Obtener Datos Usuario
                      SELECT Email_Address INTO P_Email_Address FROM WORKFLOW.WFUSER WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;
          
                      P_CORREO := P_CORREO || P_Email_Address || ',';
      END LOOP;
      CLOSE C_ROLE_ASSIGNMENT;
      P_SECCION_EXCEPT := '';
      
      -- Extraer el ultimo digito en caso sea un "coma"(,)
      SELECT SUBSTR(P_CORREO,1,LENGTH(P_CORREO) -1) INTO P_CORREO
      FROM DUAL
      WHERE SUBSTR(P_CORREO,-1,1) = ',';
      
EXCEPTION
  WHEN TOO_MANY_ROWS THEN 
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              P_ERROR := '- Se encontraron mas de un ROL con el mismo nombre: ' || P_ROL || P_COD_SEDE;
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_ERROR := '- Se encontraron mas de una ORGANIZACIóN con el mismo nombre.';
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := '- Se encontraron mas de un usuario con el mismo ROL.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN NO_DATA_FOUND THEN
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              P_ERROR := '- NO se encontrò el nombre del ROL: ' || P_ROL || P_COD_SEDE;
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_ERROR := '- NO se encontrò el nombre de la ORGANIZACION.';
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := '- NO  se encontrÓ ningun usuario con esas caracteristicas.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_USUARIO;


       

       
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------