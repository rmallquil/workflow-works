create or replace PACKAGE WFK_CONTISRM AS
/*******************************************************************************
 WFK_CONTISRM:
       Conti Package body SOLICITUD RESERVA DE MATRICULA
*******************************************************************************/
-- FILE NAME..: WFK_CONTISRM.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_CONTISRM
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/

FUNCTION F_GET_SIDEUDA_ALUMNO (
                P_PIDM_ALUMNO         SPRIDEN.SPRIDEN_PIDM%TYPE,
                P_CODIGO_DETALLE      TBRACCD.TBRACCD_DETAIL_CODE%TYPE,
                P_PERIODO             SFBRGRP.SFBRGRP_TERM_CODE%TYPE  ------ APEC
              ) RETURN BOOLEAN;

--------------------------------------------------------------------------------

PROCEDURE P_GET_SIDEUDA_ALUMNO (
                P_PIDM_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
                P_CODIGO_DETALLE      IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE,
                P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE,  --------- APEC
                P_DEUDA               OUT VARCHAR2
              );

--------------------------------------------------------------------------------

PROCEDURE P_VALIDAR_FECHA_SOLICITUD ( 
                  P_CODIGO_SOLICITUD    IN SVVSRVC.SVVSRVC_CODE%TYPE,
                  P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
                  P_FECHA_VALIDA        OUT VARCHAR2
              );

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
              P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
              P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
              P_AN                  OUT NUMBER,
              P_ERROR               OUT VARCHAR2
          );

--------------------------------------------------------------------------------

PROCEDURE P_GET_USUARIO (
                  P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
                  P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
                  P_CORREO              OUT VARCHAR2,
                  P_ROL_SEDE            OUT VARCHAR2,
                  P_ERROR               OUT VARCHAR2
            );

--------------------------------------------------------------------------------

PROCEDURE P_VALIDAR_SIHORARIO (
                P_ID_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
                P_PERIODO           IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
                P_HORARIO           OUT VARCHAR2
            );

--------------------------------------------------------------------------------

PROCEDURE P_ELIMINAR_NRCS ( 
              P_PIDM_ALUMNO             IN SPRIDEN.SPRIDEN_PIDM%TYPE,
              P_PERIODO                 IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
              P_CODE_DEPT               IN STVDEPT.STVDEPT_CODE%TYPE,
              P_CODE_CAMP               IN STVCAMP.STVCAMP_CODE%TYPE,
              P_NUMERO_SOLICITUD        IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
              P_ERROR                   OUT VARCHAR2
        );

--------------------------------------------------------------------------------

PROCEDURE P_OBTENER_COMENTARIO_ALUMNO (
              P_NUMERO_SOLICITUD        IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
              P_COMENTARIO_ALUMNO       OUT VARCHAR2,
              P_TELEFONO_ALUMNO         OUT VARCHAR2
        );

--------------------------------------------------------------------------------

PROCEDURE P_GET_PAGOS (
            P_PIDM_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE,  --------- APEC
            P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
            P_ABONO_MATRIC        OUT NUMBER,
            P_ABONO_CUOTAS        OUT NUMBER
        );

--------------------------------------------------------------------------------

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END WFK_CONTISRM;