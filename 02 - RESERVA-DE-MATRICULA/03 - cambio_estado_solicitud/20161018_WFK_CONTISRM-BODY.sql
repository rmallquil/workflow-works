create or replace PACKAGE BODY WFK_CONTISRM AS
/*******************************************************************************
 WFK_CONTISRM:
       Conti Package body SOLICITUD RESERVA DE MATRICULA
*******************************************************************************/
-- FILE NAME..: WFK_CONTISRM.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_CONTISRM
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/


FUNCTION F_GET_SIDEUDA_ALUMNO
              (
                P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
                P_PERIODO             IN STVTERM.STVTERM_CODE%TYPE,
                P_CODIGO_DETALLE      IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE
              ) RETURN VARCHAR2 
              
/* ===================================================================================================================
  NOMBRE    : F_GET_SIDEUDA_ALUMNO
  FECHA     : 12//10/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : El objetivo es que en base a un ID y un c�digo de detalle verifique la existencia de deuda en la cuenta 
              corriente del alumno para el periodo correspondiente, divisa PEN.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
              
AS
      PRAGMA AUTONOMOUS_TRANSACTION;
      C_ROWS_FOUND    NUMBER;
      C_SI_DEUDA      VARCHAR2(5) :='false';
BEGIN
--
    -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
    TZKCDAA.p_calc_deuda_alumno(P_ID_ALUMNO,'PEN');
    COMMIT;
    
    -- obtener si tiene deuda
    SELECT COUNT(*)
    INTO   C_ROWS_FOUND
    FROM   TZRCDAB
    WHERE  TZRCDAB_TERM_CODE = P_PERIODO AND TZRCDAB_PIDM = P_ID_ALUMNO 
    AND TZRCDAB_DETAIL_CODE = P_CODIGO_DETALLE AND ROWNUM <= 1;
   
    IF C_ROWS_FOUND > 0 THEN
      C_SI_DEUDA := 'true';
    END IF;
      
    RETURN C_SI_DEUDA;

END f_get_sideuda_alumno;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VALIDAR_FECHA_SOLICITUD ( 
  P_CODIGO_SOLICITUD    IN SVVSRVC.SVVSRVC_CODE%TYPE,
  P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
  P_FECHA_VALIDA        OUT VARCHAR2
  )
/* ===================================================================================================================
  NOMBRE    : p_validar_fecha_solicitud
  FECHA     : 13/10/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : se verifiqua que solicitud del alumno del tipo <P_CODIGO_SOLICITUD>( ejem SOL003), 
              se encuentra dentro del rango de fechas h�biles para atender la solicitud.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */

AS
       
BEGIN
--
    
    P_FECHA_VALIDA := 'FALSE';
    
    ----------------------------------------------------------------------------
        SELECT 
                DECODE(COUNT(*),0,'FALSE','TRUE') 
        INTO P_FECHA_VALIDA 
        FROM SVRSVPR 
        INNER JOIN (
              -- get la configuracion (fechas, estados, etc) de la solicitud P_CODIGO_SOLICITUD 
               SELECT SVRRSRV_SEQ_NO, 
                      SVRRSRV_SRVC_CODE       TEMP_SRVC_CODE, 
                      SVRRSRV_INACTIVE_IND,
                      SVRRSST_RSRV_SEQ_NO , 
                      SVRRSST_SRVC_CODE , 
                      SVRRSST_SRVS_CODE       TEMP_SRVS_CODE,
                      SVRRSRV_START_DATE      TEMP_START_DATE,--- fecha finalizacion
                      SVRRSRV_END_DATE        TEMP_END_DATE,----- fecha finalizacion
                      SVRRSRV_DELIVERY_DATE , -- fecha estimada
                      SVVSRVS_LOCKED
               FROM SVRRSRV ------------------------------------- configuraci�n total de reglas
               INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
               ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
               AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
               INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
               ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
               WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD 
               AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activo
               AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 
               AND SVVSRVS_CODE IN 'AC' ------------------------- (AC)Acitvo , (RE) Rechazado
               -- AND SVRRSRV_WEB_IND = 'Y' ------------------------ Si esta disponible en WEB
        )TEMP
        ON SVRSVPR_SRVS_CODE = TEMP_SRVS_CODE
        AND SVRSVPR_SRVC_CODE =  TEMP_SRVC_CODE
        WHERE (TEMP_START_DATE < SYSDATE AND SYSDATE < TEMP_END_DATE)
        AND SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
      
    ----------------------------------------------------------------------------
--
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END P_VALIDAR_FECHA_SOLICITUD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
  P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
  P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
  P_ERROR               OUT VARCHAR2
  )
/* ===================================================================================================================
  NOMBRE    : p_cambio_estado_solicitud
  FECHA     : 17/10/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : finaliza la solicitud (XXXXXX) presentada ya sea por su atenci�n o vencimiento del tiempo de solicitud

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */

AS
       P_CODIGO_SOLICITUD          SVVSRVC.SVVSRVC_CODE%TYPE;
BEGIN
--

    P_ERROR := '';
    
    ----------------------------------------------------------------------------
        -- GET tipo de solicitud por ejemplo "SOL003" 
        SELECT  SVRSVPR_SRVC_CODE
        INTO    P_CODIGO_SOLICITUD
        FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
        
        
        -- UPDATE SOLICITUD
        UPDATE SVRSVPR
                  SET SVRSVPR_SRVS_CODE = P_ESTADO -- AN , AP , RE
        WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD
        AND SVRSVPR_SRVS_CODE = 'AC' -- Estado ACTIVO
        AND P_ESTADO IN ( 
              -- Estados de la solicitud disponibles segun las reglas y configuracion
             SELECT  SVRRSST_SRVS_CODE       TEMP_SRVS_CODE -- Estado SOL
             FROM SVRRSRV ------------------------------------- configuraci�n total de reglas
             INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
             ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
             AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
             INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
             ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
             WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
             AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
             AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 
        );
      
    ----------------------------------------------------------------------------
--
    COMMIT;
EXCEPTION
  WHEN OTHERS THEN
          --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_CAMBIO_ESTADO_SOLICITUD;  

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END WFK_CONTISRM;