/*
SET SERVEROUTPUT ON
DECLARE P_STATUS_APTO VARCHAR2(4000);
 P_REASON VARCHAR2(4000);
BEGIN
    P_VERIFICAR_APTO(216, '201820', P_STATUS_APTO, P_REASON);
    DBMS_OUTPUT.PUT_LINE(P_STATUS_APTO);
    DBMS_OUTPUT.PUT_LINE(P_REASON);
END;
*/

CREATE OR REPLACE PROCEDURE P_VERIFICAR_APTO (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_STATUS_APTO     OUT VARCHAR2,
        P_REASON          OUT VARCHAR2
)
AS
        V_CODIGO_SOL        SVRRSRV.SVRRSRV_SRVC_CODE%TYPE := 'SOL005';   
        V_RET01             INTEGER := 0; --VERIFICA RETENCIÓN 01
        V_RET02             INTEGER := 0; --VERIFICA RETENCIÓN 02
        V_SOLTRA            INTEGER := 0; --VERIFICA SOL. TRASLADO INTERNO
        V_SOLPER            INTEGER := 0; --VERIFICA SOL. PERMANENCIA DE PLAN
        V_SOLREC            INTEGER := 0; --VERIFICA SOL. RECTIFICACION DE MATRICULA
        V_SOLCAM            INTEGER := 0; --VERIFICA SOL. CAMBIO DE PLAN
        V_SOLCUO            INTEGER := 0; --VERIFICA SOL. CAMBIO DE CUOTA INICIAL
        V_SOLDIR            INTEGER := 0; --VERIFICA SOL. ASIGNTURA DIRIGIDA
        V_RESPUESTA         VARCHAR2(4000) := NULL; --RESPUESTA ANIDADA DE LAS VALIDACIONES
        V_FLAG              INTEGER := 0; --BANDERA DE VALIDACIÓN
        
        V_CODE_DEPT       STVDEPT.STVDEPT_CODE%TYPE;
        V_CODE_TERM       STVTERM.STVTERM_CODE%TYPE;
        V_CODE_CAMP       STVCAMP.STVCAMP_CODE%TYPE;
        V_PRIORIDAD       NUMBER;
        V_TERM_ADMI       NUMBER;
        P_PERIODOCTLG     SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE := '201510';
        
        V_INGRESANTE      BOOLEAN := FALSE;
        V_NCURSOS         NUMBER;
        V_FECHA_EXM       DATE;
        V_FECHA_VALIDA    BOOLEAN;
        
        P_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
        V_PART_PERIODO        VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
        
        -- Calculando parte PERIODO (sub PTRM)
        CURSOR C_SFRRSTS_PTRM IS
        SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
        FROM STVCAMP,STVDEPT 
        WHERE STVDEPT_CODE = V_CODE_DEPT AND STVCAMP_CODE = V_CODE_CAMP;
        
        -- OBTENER PERIODO por modulo de UVIR y UPGT
        CURSOR C_SFRRSTS IS
        SELECT SFRRSTS_TERM_CODE FROM (
          -- Forma SFARSTS  ---> fechas para las partes de periodo
          SELECT DISTINCT SFRRSTS_TERM_CODE 
          FROM SFRRSTS
          WHERE SFRRSTS_PTRM_CODE IN (V_PART_PERIODO || '1', V_PART_PERIODO || '2')
          AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
          AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SFRRSTS_END_DATE
          ORDER BY SFRRSTS_TERM_CODE
        ) WHERE ROWNUM <= 1;
      
BEGIN
    ---- P_VERIFICAR_APTO PARA SOLICITUD DE RESERVA

    --################################################################################################
   ---VALIDACIÓN DE RETENCIÓN 01 (DEUDA PENDIENTE)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET01
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '01'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
    
    IF V_RET01 > 0 THEN
        V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de deuda pendiente activa. ';
        V_FLAG := 1;
    END IF;
    
    --################################################################################################
   ---VALIDACIÓN DE RETENCIÓN 02 (DOCUMENTO PENDIENTE) 
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET02    
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '02'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RET02 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de documento pendiente activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de documento pendiente activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE TRASLADO INTERNO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLTRA
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL013'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
    
    IF V_SOLTRA > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RECTIFICACIÓN DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLREC
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL003'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLREC > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';
        END IF;
        V_FLAG := 1;
    END IF;
   
    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE CAMBIO DE PLAN DE ESTUDIO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLCAM
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL004'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCAM > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE PERMANENCIA DE PLAN DE ESTUDIOS ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLPER
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL015'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLPER > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Permanencia de Plan de Estudios activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Permanencia de Plan de Estudios activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE CAMBIO DE CUOTA INICIAL ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLCUO
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL007'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCUO > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Cambio de cuota inicial activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Cambio de cuota inicial activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE DIRIGIDO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLDIR
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL014'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLDIR > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Asignatura Dirigida activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

--    -- DATOS ALUMNO
--    BWZKWFFN.P_GETDATA_ALUMN(P_PIDM,V_CODE_DEPT,V_CODE_TERM,V_CODE_CAMP);
--    ------------------------------------------------------------
--
--    -- >> calculando SUB PARTE PERIODO  --
--    OPEN C_SFRRSTS_PTRM;
--    LOOP
--      FETCH C_SFRRSTS_PTRM INTO V_PART_PERIODO;
--      EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
--    END LOOP;
--    CLOSE C_SFRRSTS_PTRM;
--      
--    IF (V_CODE_DEPT = 'UPGT' OR V_CODE_DEPT = 'UVIR') THEN
--          OPEN C_SFRRSTS;
--            LOOP
--              FETCH C_SFRRSTS INTO P_CODE_TERM;
--              IF C_SFRRSTS%FOUND THEN
--                  V_CODE_TERM := P_CODE_TERM;
--              ELSE EXIT;
--            END IF;
--            END LOOP;
--          CLOSE C_SFRRSTS;
--    END IF;
--
--    -- VALIDAR MODALIDAD Y PRIORIDAD
--    BWZKWFFN.P_CHECK_DEPT_PRIORIDAD(P_PIDM, V_CODIGO_SOL, V_CODE_DEPT, V_CODE_TERM, V_CODE_CAMP, V_PRIORIDAD);
--
--    /************************************************************************************************************/
--    -- Segun regla, los que tramitan reserva el mismo periodo que ingresaron 
--    /************************************************************************************************************/
--    IF  (V_CODE_DEPT = 'UREG' AND V_PRIORIDAD = 2) OR     -- se les RESERVA las prioridades "2"
--        (V_CODE_DEPT = 'UPGT' AND V_PRIORIDAD = 4) OR     -- se les RESERVA las prioridades "4" 
--        (V_CODE_DEPT = 'UVIR' AND V_PRIORIDAD = 7)        -- se les RESERVA las prioridades "7"
--        THEN
--        -- VALIDAR QUE EL ALUMNO SEA INGRESANTE EN EL MISMO PERIODO DEL TRAMITE. (PERIODO DE INGRESO = PERIODO ACTUAL)
--        SELECT COUNT(*)
--        INTO V_TERM_ADMI
--        FROM SORLCUR
--        INNER JOIN SORLFOS
--        ON    SORLCUR_PIDM          = SORLFOS_PIDM 
--        AND   SORLCUR_SEQNO         = SORLFOS.SORLFOS_LCUR_SEQNO
--        WHERE SORLCUR_PIDM          =  P_PIDM
--            AND SORLCUR_LMOD_CODE       = 'LEARNER' /*#Estudiante*/ 
--            AND SORLCUR_CACT_CODE       = 'ACTIVE' 
--            AND SORLCUR_CURRENT_CDE     = 'Y'
--            AND SORLCUR_TERM_CODE_ADMIT = P_TERM_CODE -- VALIDACION (PERIODO DE INGRESO = PERIODO ACTUAL)
--            AND SORLFOS_DEPT_CODE       = V_CODE_DEPT;
--    ELSE
--        V_TERM_ADMI := 1;
--    END IF;
--    
--    IF (V_PRIORIDAD > 0 AND V_TERM_ADMI > 0) THEN
--        P_STATUS_APTO := 'TRUE';
--        P_REASON := 'OK';
--        RETURN;
--    END IF;

    --################################################################################################
    ---VALIDACIÓN FINAL
    --################################################################################################
    IF V_FLAG > 0 THEN
        P_STATUS_APTO := 'FALSE';
        P_REASON := V_RESPUESTA;
    ELSE
        P_STATUS_APTO := 'TRUE';
        P_REASON := 'OK';
    END IF;

EXCEPTION 
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VERIFICAR_APTO;