/**********************************************************************************************/
/* BWZKRMOD.sql                                                                               */
/**********************************************************************************************/
/*                                                                                            */
/* Descripci�n corta: Script para generar el Paquete de automatizaci�n del proceso de         */
/*                    Solicitud de Reserva de Matr�cula On Demand                             */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI             FECHA       */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creaci�n del C�digo.                                        LAM             03/ENE/2018 */
/*    --------------------                                                                    */
/*    Creaci�n del paquete de Reserva de Matr�cula On Demand                                  */
/*    Procedure P_VALID_ID: Valida si el DNI ingresado existe en la DB-BANNER.                */
/*    Procedure P_GET_INFO_STUDENT: Obtiene informaci�n del estudiante mediante el ID.        */
/*    Procedure P_VERIFICA_FECHA_DISPO: Verificar si la fecha de solicitud se encuentra dentro*/
/*              de las fecha configuradas para el proceso.                                    */
/*    Procedure P_VERIFICA_RETENCIONES: Verificar si la fecha de solicitud se encuentra dentro*/
/*              de las fecha configuradas para el proceso.                                    */
/*    Procedure P_GET_USUARIO: En base a una sede y un rol, el procedimiento LOS CORREOS      */
/*              del(os) responsable(s) de Registros Acad�micos de esa sede.                   */
/*    Procedure P_GET_SIDEUDA_ALUMNO: El objetivo es que en base a un ID y un c�digo de       */
/*              detalle verifique la existencia de deuda de MATRICULA del alumno en cualquier */
/*              periodo, divisa PEN.                                                          */
/*    Procedure P_GET_SIDEUDA_ALUMNO: Verificar si la fecha de solicitud se encuentra dentro  */
/*              de las fecha configuradas para el proceso.                                    */
/*                                                                                            */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/

/**********************************************************************************************/
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/
CREATE OR REPLACE PACKAGE BODY BWZKRMOD AS

/* ===================================================================================================================
  NOMBRE    : P_VALID_ID
  FECHA     : 04/12/2017
  AUTOR     : Arana Milla, Karina Lizbeth
  OBJETIVO  : Valida si el DNI ingresado existe en la DB-BANNER. 

  =================================================================================================================== */

PROCEDURE P_VALID_ID (
    P_ID            IN  SPRIDEN.SPRIDEN_ID%TYPE,
    P_DNI_VALID     OUT VARCHAR2
);

----------------------------------------------------------------------------------------------------------------------

PROCEDURE P_GET_INFO_STUDENT (
      P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
      P_PIDM              OUT SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_TERM_CODE         OUT SOBPTRM.SOBPTRM_TERM_CODE%TYPE, 
      P_PART_PERIODO      OUT SOBPTRM.SOBPTRM_PTRM_CODE%TYPE, 
      P_DEPT_CODE         OUT STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE         OUT STVCAMP.STVCAMP_CODE%TYPE,
      P_CAMP_DESC         OUT STVCAMP.STVCAMP_DESC%TYPE,
      P_PROGRAM           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_EMAIL             OUT GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE,
      P_NAME              OUT VARCHAR2,
      P_FECHA_SOL         OUT DATE
);

----------------------------------------------------------------------------------------------------------------------

PROCEDURE P_VERIFICA_FECHA_DISPO (
    
    P_TERM_CODE      IN SOBPTRM.SOBPTRM_TERM_CODE%TYPE,
    P_FECHA_SOL      IN VARCHAR2,
    P_PART_PERIODO   IN SOBPTRM.SOBPTRM_PTRM_CODE%TYPE,
    P_FECHA_VALIDA   OUT VARCHAR2,
    P_DESCRIP        OUT VARCHAR2 
);

----------------------------------------------------------------------------------------------------------------------

PROCEDURE P_VERIFICA_RETENCIONES (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_RET_CODE_01     IN SPRHOLD.SPRHOLD_HLDD_CODE%TYPE,
    P_RET_CODE_02     IN SPRHOLD.SPRHOLD_HLDD_CODE%TYPE,
    P_RETENCION       OUT VARCHAR2,
    P_DESCRIP         OUT VARCHAR2,
    P_MESSAGE         OUT VARCHAR2
);

----------------------------------------------------------------------------------------------------------------------

PROCEDURE P_GET_USUARIO (
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
      P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
      P_CORREO              OUT VARCHAR2,
      P_ROL_SEDE            OUT VARCHAR2,
      P_ERROR               OUT VARCHAR2
);

----------------------------------------------------------------------------------------------------------------------

PROCEDURE P_GET_SIDEUDA_ALUMNO (
                P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
                P_CODIGO_DETALLE      IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE,
                P_TERM_CODE           IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE,  --------- APEC
                P_DEUDA               OUT VARCHAR2
);              

----------------------------------------------------------------------------------------------------------------------

PROCEDURE P_ELIMINAR_NRCS ( 
      P_PIDM                    IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_TERM_CODE               IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
      P_DEPT_CODE               IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE               IN STVCAMP.STVCAMP_CODE%TYPE,
      P_FECHA_SOL               IN VARCHAR2,
      P_ERROR                   OUT VARCHAR2
);

----------------------------------------------------------------------------------------------------------------------

PROCEDURE P_GET_PAGOS (
                P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
                P_TERM_CODE           IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE,  --------- APEC
                P_FECHA_SOL           IN VARCHAR2,
                P_ABONO_MATRIC        OUT NUMBER,
                P_ABONO_CUOTAS        OUT NUMBER
);              

END BWZKRMOD;

/**********************************************************************************************/
--  /
--  show errors 
-- 
--  SET SCAN ON
--  WHENEVER SQLERROR CONTINUE
--  DROP PUBLIC SYNONYM BWZKSMCV;
--  WHENEVER SQLERROR EXIT ROLLBACK
--  CREATE PUBLIC SYNONYM BWZKSMCV FOR BANINST1.BWZKSMCV;
--  WHENEVER SQLERROR CONTINUE
--  START gurgrtb BWZKSMCV
--  START gurgrth BWZKSMCV
--  WHENEVER SQLERROR EXIT ROLLBACK
/**********************************************************************************************/
