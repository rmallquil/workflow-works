/*
SET SERVEROUTPUT ON
DECLARE BOOL BOOLEAN;
BEGIN
        BOOL :=  F_GET_SIDEUDA_ALUMNO(340813,'C00','201620');
        DBMS_OUTPUT.PUT_LINE(CASE WHEN BOOL = TRUE THEN 'TRUE' ELSE 'FALSE' END);
END;
*/

create or replace FUNCTION F_GET_SIDEUDA_ALUMNO
              (
                P_PIDM_ALUMNO         SPRIDEN.SPRIDEN_PIDM%TYPE,
                P_CODIGO_DETALLE      TBRACCD.TBRACCD_DETAIL_CODE%TYPE,
                P_PERIODO             SFBRGRP.SFBRGRP_TERM_CODE%TYPE  ------ APEC
              ) RETURN BOOLEAN 
/* ===================================================================================================================
  NOMBRE    : F_GET_SIDEUDA_ALUMNO
  FECHA     : 12/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : El objetivo es que en base a un ID y un código de detalle verifique la existencia de 
              deuda de MATRICULA del alumno en cualquier periodo, divisa PEN.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION  
  =================================================================================================================== */
AS
      PRAGMA AUTONOMOUS_TRANSACTION;
      P_INDICADOR        NUMBER := 0;
      
      -- APEC PARAMS
      P_SERVICE               VARCHAR2(10);
      P_PART_PERIODO          VARCHAR2(9);
      P_APEC_CAMP             VARCHAR2(9);
      P_APEC_DEPT             VARCHAR2(9);
      P_APEC_TERM             VARCHAR2(10);
      P_APEC_IDSECCIONC       VARCHAR2(15);
      P_APEC_FECINIC          DATE;
      P_APEC_DEUDA            NUMBER;
      P_APEC_IDALUMNO         VARCHAR2(10);
      C_SEQNO                   SORLCUR.SORLCUR_SEQNO %type;
      C_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;
      C_ALUM_CAMPUS             SORLCUR.SORLCUR_CAMP_CODE%type;
      C_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
      C_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
      C_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
      C_ALUM_PERD_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
      C_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP
      C_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE
      C_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%type;
BEGIN
--
    --########################################################################################
    --########################################################################################
    SELECT PKG_GLOBAL.GET_VAL INTO P_SERVICE FROM DUAL; -- APEC(BDUCCI) <------> BANNER
    IF P_SERVICE = 0 THEN -- 0 ---> APEC(BDUCCI)
          
          -- #######################################################################
          -- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
          SELECT    SORLCUR_SEQNO,      
                    SORLCUR_LEVL_CODE,    
                    SORLCUR_CAMP_CODE,        
                    SORLCUR_COLL_CODE,
                    SORLCUR_DEGC_CODE,  
                    SORLCUR_PROGRAM,      
                    SORLCUR_TERM_CODE_ADMIT,  
                    --SORLCUR_STYP_CODE,
                    SORLCUR_STYP_CODE,  
                    SORLCUR_RATE_CODE,    
                    SORLFOS_DEPT_CODE   
          INTO      C_SEQNO,
                    C_ALUM_NIVEL, 
                    C_ALUM_CAMPUS, 
                    C_ALUM_ESCUELA, 
                    C_ALUM_GRADO, 
                    C_ALUM_PROGRAMA, 
                    C_ALUM_PERD_ADM,
                    C_ALUM_TIPO_ALUM,
                    C_ALUM_TARIFA,
                    C_ALUM_DEPARTAMENTO
          FROM (
                  SELECT    SORLCUR_SEQNO,      SORLCUR_LEVL_CODE,    SORLCUR_CAMP_CODE,        SORLCUR_COLL_CODE,
                            SORLCUR_DEGC_CODE,  SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  --SORLCUR_STYP_CODE,
                            SORLCUR_STYP_CODE,  SORLCUR_RATE_CODE,    SORLFOS_DEPT_CODE
                  FROM SORLCUR        INNER JOIN SORLFOS
                        ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                        AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
                  WHERE   SORLCUR_PIDM        =   P_PIDM_ALUMNO 
                      AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                      -- AND SORLCUR_TERM_CODE   =   P_PERIODO 
                      AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                      AND SORLCUR_CURRENT_CDE = 'Y'
                  ORDER BY SORLCUR_SEQNO DESC 
          ) WHERE ROWNUM <= 1;
          
          -- GET CRONOGRAMA SECCIONC
          SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                            WHEN 'UPGT' THEN 'W' 
                                            WHEN 'UREG' THEN 'R' 
                                            WHEN 'UPOS' THEN '-' 
                                            WHEN 'ITEC' THEN '-' 
                                            WHEN 'UCIC' THEN '-' 
                                            WHEN 'UCEC' THEN '-' 
                                            WHEN 'ICEC' THEN '-' 
                                            ELSE '1' END ||
                          CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                            WHEN 'F01' THEN 'A' 
                                            WHEN 'F02' THEN 'L' 
                                            WHEN 'F03' THEN 'C' 
                                            WHEN 'V00' THEN 'V' 
                                            ELSE '9' END
                  INTO P_PART_PERIODO
                  FROM STVCAMP,STVDEPT 
                  WHERE STVDEPT_CODE = C_ALUM_DEPARTAMENTO AND STVCAMP_CODE = C_ALUM_CAMPUS;
          
          SELECT CZRCAMP_CAMP_BDUCCI INTO P_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = C_ALUM_CAMPUS;-- CAMPUS 
          SELECT CZRTERM_TERM_BDUCCI INTO P_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_PERIODO ;-- PERIODO
          SELECT CZRDEPT_DEPT_BDUCCI INTO P_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = C_ALUM_DEPARTAMENTO;-- DEPARTAMENTO
          
          -- GET SECCIONC 
          SELECT "IDSeccionC","FecInic" INTO P_APEC_IDSECCIONC, P_APEC_FECINIC
          FROM dbo.tblSeccionC@DBBCK.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia"='UCCI'
          AND "IDsede"    = P_APEC_CAMP
          AND "IDPerAcad" = P_APEC_TERM
          AND "IDEscuela" = C_ALUM_PROGRAMA
          AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
          AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
          AND "IDSeccionC" <> '15NEX1A'
          AND SUBSTRB("IDSeccionC",-2,2) IN (
              -- PARTE PERIODO           
              SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (P_PART_PERIODO || '%')
          );
          
          -- GET DATOS CTA CORRIENTE
          SELECT "IDAlumno","Deuda" INTO P_APEC_IDALUMNO, P_APEC_DEUDA
          FROM dbo.tblCtaCorriente@DBBCK.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia" = 'UCCI'
          AND "IDAlumno"    IN (
              SELECT "IDAlumno" FROM  dbo.tblPersonaAlumno@DBBCK.CONTINENTAL.EDU.PE 
              WHERE "IDPersona" IN ( SELECT "IDPersona" FROM dbo.tblPersona@DBBCK.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM_ALUMNO )
          )
          AND "IDSede"      = P_APEC_CAMP
          AND "IDPerAcad"   = P_APEC_TERM
          AND "IDEscuela"   = C_ALUM_PROGRAMA
          AND "IDSeccionC"  = P_APEC_IDSECCIONC
          AND "IDConcepto"  = P_CODIGO_DETALLE;
          
          P_INDICADOR := P_APEC_DEUDA;
          
          COMMIT;
          
    ELSE --**************************** 1 ---> BANNER ***************************
          
          -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
          TZKCDAA.p_calc_deuda_alumno(P_PIDM_ALUMNO,'PEN');
          COMMIT;
              
          -- GET deuda
          SELECT COUNT(TZRCDAB_AMOUNT)
          INTO   P_INDICADOR
          FROM   TZRCDAB
          WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
          AND TZRCDAB_PIDM = P_PIDM_ALUMNO 
          AND TZRCDAB_DETAIL_CODE = P_CODIGO_DETALLE;         -- CODIGO MATRICULA 'C00'
          --AND TZRCDAB_TRAN_NUMBER_PRIN = P_TRAN_NUMBER;
    
    END IF;
    
    RETURN(P_INDICADOR > 0);

END F_GET_SIDEUDA_ALUMNO;


----------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------


/*
SET SERVEROUTPUT ON
DECLARE P_DEUDA VARCHAR2(10);
BEGIN
        P_GET_SIDEUDA_ALUMNO(340813,'C00','201620',P_DEUDA);
        DBMS_OUTPUT.PUT_LINE(P_DEUDA);
END;
*/

create or replace PROCEDURE P_GET_SIDEUDA_ALUMNO (
                P_PIDM_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
                P_CODIGO_DETALLE      IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE,
                P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE,  --------- APEC
                P_DEUDA               OUT VARCHAR2
              )              
/* ===================================================================================================================
  NOMBRE    : P_GET_SIDEUDA_ALUMNO
  FECHA     : 12/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : El objetivo es que en base a un ID y un código de detalle verifique la existencia de 
              deuda de MATRICULA del alumno en cualquier periodo, divisa PEN.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION  
  =================================================================================================================== */
AS
      PRAGMA AUTONOMOUS_TRANSACTION;
      P_INDICADOR           NUMBER;
      
      -- APEC PARAMS
      P_SERVICE               VARCHAR2(10);
      P_PART_PERIODO          VARCHAR2(9);
      P_APEC_CAMP             VARCHAR2(9);
      P_APEC_DEPT             VARCHAR2(9);
      P_APEC_TERM             VARCHAR2(10);
      P_APEC_IDSECCIONC       VARCHAR2(15);
      P_APEC_FECINIC          DATE;
      P_APEC_DEUDA            NUMBER;
      P_APEC_IDALUMNO         VARCHAR2(10);
      C_SEQNO                   SORLCUR.SORLCUR_SEQNO %type;
      C_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;
      C_ALUM_CAMPUS             SORLCUR.SORLCUR_CAMP_CODE%type;
      C_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
      C_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
      C_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
      C_ALUM_PERD_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
      C_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP
      C_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE
      C_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%type;
      
BEGIN
--
    
    --########################################################################################
    --########################################################################################
    SELECT PKG_GLOBAL.GET_VAL INTO P_SERVICE FROM DUAL; -- APEC(BDUCCI) <------> BANNER
    IF P_SERVICE = 0 THEN -- 0 ---> APEC(BDUCCI)
          
          -- #######################################################################
          -- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
          SELECT    SORLCUR_SEQNO,      
                    SORLCUR_LEVL_CODE,    
                    SORLCUR_CAMP_CODE,        
                    SORLCUR_COLL_CODE,
                    SORLCUR_DEGC_CODE,  
                    SORLCUR_PROGRAM,      
                    SORLCUR_TERM_CODE_ADMIT,  
                    --SORLCUR_STYP_CODE,
                    SORLCUR_STYP_CODE,  
                    SORLCUR_RATE_CODE,    
                    SORLFOS_DEPT_CODE   
          INTO      C_SEQNO,
                    C_ALUM_NIVEL, 
                    C_ALUM_CAMPUS, 
                    C_ALUM_ESCUELA, 
                    C_ALUM_GRADO, 
                    C_ALUM_PROGRAMA, 
                    C_ALUM_PERD_ADM,
                    C_ALUM_TIPO_ALUM,
                    C_ALUM_TARIFA,
                    C_ALUM_DEPARTAMENTO
          FROM (
                  SELECT    SORLCUR_SEQNO,      SORLCUR_LEVL_CODE,    SORLCUR_CAMP_CODE,        SORLCUR_COLL_CODE,
                            SORLCUR_DEGC_CODE,  SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  --SORLCUR_STYP_CODE,
                            SORLCUR_STYP_CODE,  SORLCUR_RATE_CODE,    SORLFOS_DEPT_CODE
                  FROM SORLCUR        INNER JOIN SORLFOS
                        ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                        AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
                  WHERE   SORLCUR_PIDM        =   P_PIDM_ALUMNO 
                      AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                      -- AND SORLCUR_TERM_CODE   =   P_PERIODO 
                      AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                      AND SORLCUR_CURRENT_CDE = 'Y'
                  ORDER BY SORLCUR_SEQNO DESC 
          ) WHERE ROWNUM <= 1;
          
          -- GET CRONOGRAMA SECCIONC
          SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                            WHEN 'UPGT' THEN 'W' 
                                            WHEN 'UREG' THEN 'R' 
                                            WHEN 'UPOS' THEN '-' 
                                            WHEN 'ITEC' THEN '-' 
                                            WHEN 'UCIC' THEN '-' 
                                            WHEN 'UCEC' THEN '-' 
                                            WHEN 'ICEC' THEN '-' 
                                            ELSE '1' END ||
                          CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                            WHEN 'F01' THEN 'A' 
                                            WHEN 'F02' THEN 'L' 
                                            WHEN 'F03' THEN 'C' 
                                            WHEN 'V00' THEN 'V' 
                                            ELSE '9' END
                  INTO P_PART_PERIODO
                  FROM STVCAMP,STVDEPT 
                  WHERE STVDEPT_CODE = C_ALUM_DEPARTAMENTO AND STVCAMP_CODE = C_ALUM_CAMPUS;
          
          SELECT CZRCAMP_CAMP_BDUCCI INTO P_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = C_ALUM_CAMPUS;-- CAMPUS 
          SELECT CZRTERM_TERM_BDUCCI INTO P_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_PERIODO ;-- PERIODO
          SELECT CZRDEPT_DEPT_BDUCCI INTO P_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = C_ALUM_DEPARTAMENTO;-- DEPARTAMENTO
          
          -- GET SECCIONC 
          SELECT "IDSeccionC","FecInic" INTO P_APEC_IDSECCIONC, P_APEC_FECINIC
          FROM dbo.tblSeccionC@DBBCK.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia"='UCCI'
          AND "IDsede"    = P_APEC_CAMP
          AND "IDPerAcad" = P_APEC_TERM
          AND "IDEscuela" = C_ALUM_PROGRAMA
          AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
          AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
          AND "IDSeccionC" <> '15NEX1A'
          AND SUBSTRB("IDSeccionC",-2,2) IN (
              -- PARTE PERIODO           
              SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (P_PART_PERIODO || '%')
          );
          
          -- GET DATOS CTA CORRIENTE
          SELECT "IDAlumno","Deuda" INTO P_APEC_IDALUMNO, P_APEC_DEUDA
          FROM dbo.tblCtaCorriente@DBBCK.CONTINENTAL.EDU.PE 
          WHERE "IDDependencia" = 'UCCI'
          AND "IDAlumno"    IN (
              SELECT "IDAlumno" FROM  dbo.tblPersonaAlumno@DBBCK.CONTINENTAL.EDU.PE 
              WHERE "IDPersona" IN ( SELECT "IDPersona" FROM dbo.tblPersona@DBBCK.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM_ALUMNO )
          )
          AND "IDSede"      = P_APEC_CAMP
          AND "IDPerAcad"   = P_APEC_TERM
          AND "IDEscuela"   = C_ALUM_PROGRAMA
          AND "IDSeccionC"  = P_APEC_IDSECCIONC
          AND "IDConcepto"  = P_CODIGO_DETALLE;
          
          P_INDICADOR := P_APEC_DEUDA;
          
          COMMIT;
          
    ELSE --**************************** 1 ---> BANNER ***************************
          
          -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
          TZKCDAA.p_calc_deuda_alumno(P_PIDM_ALUMNO,'PEN');
          COMMIT;
              
          -- GET deuda
          SELECT COUNT(TZRCDAB_AMOUNT)
          INTO   P_INDICADOR
          FROM   TZRCDAB
          WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
          AND TZRCDAB_PIDM = P_PIDM_ALUMNO 
          AND TZRCDAB_DETAIL_CODE = P_CODIGO_DETALLE;         -- CODIGO MATRICULA 'C00'
          --AND TZRCDAB_TRAN_NUMBER_PRIN = P_TRAN_NUMBER;
    
    END IF;
   
    IF P_INDICADOR > 0 THEN
      P_DEUDA   := 'TRUE';
    ELSE
      P_DEUDA   :='FALSE';
    END IF;

END P_GET_SIDEUDA_ALUMNO;