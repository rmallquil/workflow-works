
create or replace PACKAGE BODY WFK_CONTISRM AS
/*******************************************************************************
 WFK_CONTISRM:
       Conti Package body SOLICITUD RESERVA DE MATRICULA
*******************************************************************************/
-- FILE NAME..: WFK_CONTISRM.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_CONTISRM
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/


FUNCTION F_GET_SIDEUDA_ALUMNO
              (
                P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
                P_PERIODO             IN STVTERM.STVTERM_CODE%TYPE,
                P_CODIGO_DETALLE      IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE
              ) RETURN VARCHAR2 
              
/* ===================================================================================================================
  NOMBRE    : F_GET_SIDEUDA_ALUMNO
  FECHA     : 12//10/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : El objetivo es que en base a un ID y un c�digo de detalle verifique la existencia de deuda en la cuenta 
              corriente del alumno para el periodo correspondiente, divisa PEN.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
              
AS
      PRAGMA AUTONOMOUS_TRANSACTION;
      C_ROWS_FOUND    NUMBER;
      C_SI_DEUDA      VARCHAR2(5) :='false';
BEGIN
--
    -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
    TZKCDAA.p_calc_deuda_alumno(P_ID_ALUMNO,'PEN');
    COMMIT;
    
    -- obtener si tiene deuda
    SELECT COUNT(*)
    INTO   C_ROWS_FOUND
    FROM   TZRCDAB
    WHERE  TZRCDAB_TERM_CODE = P_PERIODO AND TZRCDAB_PIDM = P_ID_ALUMNO 
    AND TZRCDAB_DETAIL_CODE = P_CODIGO_DETALLE AND ROWNUM <= 1;
   
    IF C_ROWS_FOUND > 0 THEN
      C_SI_DEUDA := 'true';
    END IF;
      
    RETURN C_SI_DEUDA;

END f_get_sideuda_alumno;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--





--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END WFK_CONTISRM;