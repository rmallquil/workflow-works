DECLARE P_ERROR                   VARCHAR2(100);
BEGIN
    P_ELIMINAR_NRCS('386582','201620',P_ERROR);
    DBMS_OUTPUT.PUT_LINE('ERROR :' || P_ERROR || ' -- AFECTADOS :' || SQL%ROWCOUNT);
END;





CREATE OR REPLACE PROCEDURE P_ELIMINAR_NRCS( 
      P_ID_ALUMNO               IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_PERIODO                 IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
      P_ERROR                   OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_ELIMINAR_NRCS
  FECHA     : 28/11/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Eliminar los NRC's , STUPDY PATH, Informaciob de Ingreso. Vuelve a estimar sus deudas cancelando 
              sus deudas generadas (MONTOS EN NEGATIVO)                

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
AS
            P_KEY_SEQNO                 SGRSTSP.SGRSTSP_KEY_SEQNO%TYPE;
            SAVE_ACT_DATE_OUT           VARCHAR2(100);
            RETURN_STATUS_IN_OUT        NUMBER;
BEGIN
      
      -- #######################################################################          
      -- VALIDAR - "estimaciones IN-LINE"
      -- #######################################################################          
      

      -- #######################################################################          
      -- Eliminar registros de CRN'S
      -- SFASLST ::: Student Course Registration Repeating Table 
      DELETE FROM SFRSTCR 
      WHERE SFRSTCR_PIDM = P_ID_ALUMNO
      AND SFRSTCR_TERM_CODE = P_PERIODO; 


      -- #######################################################################
      -- AMORTIGUAR DEUDA
      SFKFEES.P_PROCESS_ETRM_DROP(  P_ID_ALUMNO, 
                                    P_PERIODO, 
                                    SYSDATE );
      
      
      -- #######################################################################
      -- Procesar DEUDA
      SFKFEES.P_PROCESSFEEASSESSMENT(   P_PERIODO,
                                        P_ID_ALUMNO,
                                        NULL,
                                        SYSDATE,
                                        'R',
                                        'N',                  -- create TBRACCD records
                                        'SFAREGS',    
                                        'Y',                  -- commit changes
                                        SAVE_ACT_DATE_OUT,
                                        'N',
                                        RETURN_STATUS_IN_OUT); 
                                        
      
        -- #######################################################################
        -- ELIMINAR - STUDY PATH -> SFAREGS 
        DELETE FROM SFRENSP 
        WHERE SFRENSP_PIDM = P_ID_ALUMNO
        AND SFRENSP_TERM_CODE = P_PERIODO; 


       -- #######################################################################
       -- ELIMINAR - INFORMACION DE INGRESO -> SFAREGS  ||  registro que determina al alumno Elegible 
        DELETE FROM SFBETRM 
        WHERE SFBETRM_PIDM = P_ID_ALUMNO
        AND SFBETRM_TERM_CODE = P_PERIODO;  

      
        -- #######################################################################
        -- CAMBIO ESTADO PLAN ESTUDIO  
        SELECT  SGRSTSP_KEY_SEQNO INTO P_KEY_SEQNO
        FROM SGRSTSP 
        WHERE SGRSTSP_PIDM = P_ID_ALUMNO 
        AND SGRSTSP_TERM_CODE_EFF = P_PERIODO AND ROWNUM = 1
        ORDER BY SGRSTSP_TERM_CODE_EFF DESC, SGRSTSP_KEY_SEQNO ASC;

        
        UPDATE SGRSTSP 
            SET SGRSTSP_STSP_CODE = 'RV' ------------------ ESTADOS STVSTSP : 'RV' - Reserva
        WHERE SGRSTSP_PIDM = P_ID_ALUMNO 
        AND SGRSTSP_TERM_CODE_EFF = P_PERIODO 
        AND SGRSTSP_KEY_SEQNO = P_KEY_SEQNO; -- P_KEY_SEQNO
          
        
        -- #######################################################################
        -- CAMBIO ESTADO PERIODO  
          
        UPDATE SGBSTDN
            SET SGBSTDN_STST_CODE = 'IS'  ------------------ ESTADOS STVSTST : 'IS' - Inactivo
        WHERE SGBSTDN_PIDM = P_ID_ALUMNO
        AND SGBSTDN_TERM_CODE_EFF = P_PERIODO;
        
        
        --
        COMMIT;
EXCEPTION
    WHEN OTHERS THEN
          --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_ELIMINAR_NRCS;   