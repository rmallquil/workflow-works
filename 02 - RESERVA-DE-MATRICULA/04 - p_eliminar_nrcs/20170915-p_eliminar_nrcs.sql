/*
DECLARE P_ERROR                   VARCHAR2(100);
BEGIN
    P_ELIMINAR_NRCS('386582','201620',P_ERROR);
    DBMS_OUTPUT.PUT_LINE('ERROR :' || P_ERROR || ' -- AFECTADOS :' || SQL%ROWCOUNT);
END;
*/
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------

create or replace PROCEDURE P_ELIMINAR_NRCS ( 
      P_PIDM_ALUMNO             IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_PERIODO                 IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
      P_CODE_DEPT               IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CODE_CAMP               IN STVCAMP.STVCAMP_CODE%TYPE,
      P_NUMERO_SOLICITUD        IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_ERROR                   OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_ELIMINAR_NRCS
  FECHA     : 28/11/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Eliminar los NRC's , STUPDY PATH, Informaciob de Ingreso. Vuelve a estimar sus deudas cancelando 
              sus deudas generadas (MONTOS EN NEGATIVO)                

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  001   27/04/2016    RMALLQUI      Se adapto el proced. para reservas realizadas en el el 2° Modulo de UVIR y UPGT
  002   15/09/2017    RMALLQUI      Se agrearon 14 dias sobre la fecha final de modulo(UVIR/UPGT) para la ejecución.
  =================================================================================================================== */
AS
        P_KEY_SEQNO                 SGRSTSP.SGRSTSP_KEY_SEQNO%TYPE;
        SAVE_ACT_DATE_OUT           VARCHAR2(100);
        RETURN_STATUS_IN_OUT        NUMBER;
        P_INDICADOR                 NUMBER;
        V_SGRSTSP_REC               SGRSTSP%ROWTYPE;
        V_SGBSTDN_REC               SGBSTDN%ROWTYPE;
        
        V_NUM_NRC                   INTEGER := 0;
        
        --- ADD SGRSTSP ->  EN CASO NO EXISTA PARA EL PERIODO INDICADO
              -- cambiando el estado del plan ( FORMA SFAREGS --> plan estudio)
        CURSOR C_SGRSTSP IS
        SELECT * FROM (
            SELECT *
            FROM SGRSTSP WHERE SGRSTSP_PIDM = P_PIDM_ALUMNO
            AND SGRSTSP_KEY_SEQNO = P_KEY_SEQNO
            ORDER BY SGRSTSP_TERM_CODE_EFF DESC
         )WHERE ROWNUM = 1
         AND NOT EXISTS (
            -- validar QUE ya existe el REGISTRO
            SELECT *
            FROM SGRSTSP WHERE SGRSTSP_PIDM = P_PIDM_ALUMNO
            AND SGRSTSP_TERM_CODE_EFF = P_PERIODO
            AND SGRSTSP_KEY_SEQNO = P_KEY_SEQNO
         );
         
        -- ADD SGBSTDN - EN CASO NO EXISTA PARA EL PERIODO INDICADO
        CURSOR C_SGBSTDN IS
        SELECT * FROM (
            SELECT *
            FROM SGBSTDN WHERE SGBSTDN_PIDM = P_PIDM_ALUMNO
            ORDER BY SGBSTDN_TERM_CODE_EFF DESC
         )WHERE ROWNUM = 1
         AND NOT EXISTS (
            SELECT *
            FROM SGBSTDN WHERE SGBSTDN_PIDM = P_PIDM_ALUMNO
            AND SGBSTDN_TERM_CODE_EFF = P_PERIODO
         );

        V_FECHA_SOL           SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE;
        V_MODULO              INTEGER;
        V_PART_PERIODO        VARCHAR2(9);
BEGIN
      
        SELECT COUNT(*)
        INTO V_NUM_NRC
        FROM SFRSTCR
        WHERE SFRSTCR_PIDM = P_PIDM_ALUMNO
        AND SFRSTCR_TERM_CODE = P_PERIODO;
        
        IF V_NUM_NRC > 0 THEN
      
            -- #######################################################################          
            -- Determinar el modulo(grupo) - PARA UVIR, UPGT en caso esten en el 2� MODULO 
            -- #######################################################################  
            IF P_CODE_DEPT = 'UPGT' OR P_CODE_DEPT = 'UVIR'  THEN
                -- GET parte PERIODO
                SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                          WHEN 'UPGT' THEN 'W' 
                                          WHEN 'UREG' THEN 'R' 
                                          WHEN 'UPOS' THEN '-' 
                                          WHEN 'ITEC' THEN '-' 
                                          WHEN 'UCIC' THEN '-' 
                                          WHEN 'UCEC' THEN '-' 
                                          WHEN 'ICEC' THEN '-' 
                                          ELSE '1' END ||
                        CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                          WHEN 'F01' THEN 'A' 
                                          WHEN 'F02' THEN 'L' 
                                          WHEN 'F03' THEN 'C' 
                                          WHEN 'V00' THEN 'V' 
                                          ELSE '9' END
                INTO V_PART_PERIODO
                FROM STVCAMP,STVDEPT 
                WHERE STVDEPT_CODE = P_CODE_DEPT AND STVCAMP_CODE = P_CODE_CAMP;
            
                -- GET FECHA de la solicitud 
                SELECT  SVRSVPR_RECEPTION_DATE INTO V_FECHA_SOL
                FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
            
                -- Forma SFARSTS  ---> fechas para las partes de periodo
                SELECT MODULO INTO V_MODULO FROM(
                    SELECT CASE WHEN SFRRSTS_PTRM_CODE = V_PART_PERIODO|| '1' THEN 1 WHEN  SFRRSTS_PTRM_CODE = V_PART_PERIODO || '2' THEN 2 END MODULO 
                    FROM SFRRSTS
                    WHERE SFRRSTS_PTRM_CODE IN (V_PART_PERIODO || '1', V_PART_PERIODO || '2')
                    AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                    AND TO_DATE(TO_CHAR(V_FECHA_SOL,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND (SFRRSTS_END_DATE + 14)
                    AND SFRRSTS_TERM_CODE = P_PERIODO
                    ORDER BY 1 ASC
                ) WHERE ROWNUM = 1; 
            
                    /******************************************************************************
                    -- Desmatricular y Estimar deuda (BANNER)
                    *******************************************************************************/
            --                UPDATE SFRSTCR 
            --                SET SFRSTCR_RSTS_CODE = 'DD',           SFRSTCR_RSTS_DATE = SYSDATE,
            --                    SFRSTCR_ERROR_FLAG = 'D',           SFRSTCR_BILL_HR = 0,
            --                    SFRSTCR_WAIV_HR = 0,                SFRSTCR_CREDIT_HR = 0,
            --                    SFRSTCR_ACTIVITY_DATE = SYSDATE,    SFRSTCR_USER = 'WorkFlow',
            --                    SFRSTCR_ASSESS_ACTIVITY_DATE = SYSDATE      
            --                WHERE SFRSTCR_TERM_CODE = P_PERIODO
            --                AND SFRSTCR_PIDM = P_PIDM_ALUMNO
            --                AND SFRSTCR_RSTS_CODE IN  ('RE','RW')  -- Estados v�lidos (SFQRSTS)
            --                AND SFRSTCR_PTRM_CODE = V_PART_PERIODO || '2'
            --                AND V_MODULO = 2;
            
                    DELETE SFRSTCR 
                    WHERE SFRSTCR_TERM_CODE = P_PERIODO
                    AND SFRSTCR_PIDM = P_PIDM_ALUMNO
                    AND SFRSTCR_RSTS_CODE IN  ('RE','RW')  -- Estados validos (SFQRSTS)
                    AND SFRSTCR_PTRM_CODE = V_PART_PERIODO || '2'
                    AND V_MODULO = 2;
                    
                    COMMIT;
            ELSE
                V_MODULO := 1;
            END IF;           
            
            
            -- #######################################################################          
            -- VALIDAR - "estimaciones IN-LINE"
            -- #######################################################################          
            
            
            -- #######################################################################          
            -- Eliminar registros de CRN'S
            -- SFASLST ::: Student Course Registration Repeating Table 
            DELETE FROM SFRSTCR 
            WHERE SFRSTCR_PIDM = P_PIDM_ALUMNO
            AND SFRSTCR_TERM_CODE = P_PERIODO
            AND V_MODULO = 1; 
            
            
            -- #######################################################################
            -- AMORTIGUAR DEUDA
            SFKFEES.P_PROCESS_ETRM_DROP(  P_PIDM_ALUMNO, 
                                        P_PERIODO, 
                                        SYSDATE );
            
            
            -- #######################################################################
            -- Procesar DEUDA
            SFKFEES.P_PROCESSFEEASSESSMENT(   P_PERIODO,
                                            P_PIDM_ALUMNO,
                                            NULL,
                                            SYSDATE,
                                            'R',
                                            'N',                  -- create TBRACCD records
                                            'SFAREGS',    
                                            'Y',                  -- commit changes
                                            SAVE_ACT_DATE_OUT,
                                            'N',
                                            RETURN_STATUS_IN_OUT); 
                                            
            
            -- #######################################################################
            -- ELIMINAR - STUDY PATH -> SFAREGS 
            DELETE FROM SFRENSP 
            WHERE SFRENSP_PIDM = P_PIDM_ALUMNO
            AND SFRENSP_TERM_CODE = P_PERIODO
            AND V_MODULO = 1;
            
            
            -- #######################################################################
            -- ELIMINAR - INFORMACION DE INGRESO -> SFAREGS  ||  registro que determina al alumno Elegible 
            DELETE FROM SFBETRM 
            WHERE SFBETRM_PIDM = P_PIDM_ALUMNO
            AND SFBETRM_TERM_CODE = P_PERIODO
            AND V_MODULO = 1; 
            
            
            -- #######################################################################
            -- CAMBIO ESTADO PLAN ESTUDIO ::::  
            -- #######################################################################  
            SELECT    SORLCUR_KEY_SEQNO 
            INTO      P_KEY_SEQNO -- P_KEY_SEQNO -> se obtiene de SORLCUR_KEY_SEQNO
            FROM (
                    SELECT    SORLCUR_SEQNO,      SORLCUR_PROGRAM,      SORLCUR_TERM_CODE,
                              SORLFOS_DEPT_CODE,  SORLCUR_KEY_SEQNO
                    FROM SORLCUR        INNER JOIN SORLFOS
                          ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                          AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
                    WHERE   SORLCUR_PIDM        =   P_PIDM_ALUMNO 
                        AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                        -- AND SORLCUR_TERM_CODE   =   P_TERM_PERIODO 
                        AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                        AND SORLCUR_CURRENT_CDE = 'Y'
                    ORDER BY SORLCUR_SEQNO DESC 
            ) WHERE ROWNUM <= 1;
            
            --- cambio de plan FORMA SFAREGS --> plan estudio
            OPEN C_SGRSTSP;
            LOOP
                FETCH C_SGRSTSP INTO V_SGRSTSP_REC;
                EXIT WHEN C_SGRSTSP%NOTFOUND;
                  
                  INSERT INTO SGRSTSP (
                            SGRSTSP_PIDM,                 SGRSTSP_TERM_CODE_EFF,          SGRSTSP_KEY_SEQNO,
                            SGRSTSP_STSP_CODE,            SGRSTSP_ACTIVITY_DATE,          SGRSTSP_DATA_ORIGIN,
                            SGRSTSP_USER_ID,              SGRSTSP_FULL_PART_IND,          SGRSTSP_SESS_CODE,
                            SGRSTSP_RESD_CODE,            SGRSTSP_ORSN_CODE,              SGRSTSP_PRAC_CODE,
                            SGRSTSP_CAPL_CODE,            SGRSTSP_EDLV_CODE,              SGRSTSP_INCM_CODE,
                            SGRSTSP_EMEX_CODE,            SGRSTSP_APRN_CODE,              SGRSTSP_TRCN_CODE,
                            SGRSTSP_GAIN_CODE,            SGRSTSP_VOED_CODE,              SGRSTSP_BLCK_CODE,
                            SGRSTSP_EGOL_CODE,            SGRSTSP_BSKL_CODE,              SGRSTSP_ASTD_CODE,
                            SGRSTSP_PREV_CODE,            SGRSTSP_CAST_CODE  ) 
                  VALUES (  V_SGRSTSP_REC.SGRSTSP_PIDM,          P_PERIODO,                               P_KEY_SEQNO,
                            'RV',/*STVSTSP: 'RV'-Reserva*/       SYSDATE,                                 'WorkFlow',
                            USER,                                V_SGRSTSP_REC.SGRSTSP_FULL_PART_IND,     V_SGRSTSP_REC.SGRSTSP_SESS_CODE,
                            V_SGRSTSP_REC.SGRSTSP_RESD_CODE,     V_SGRSTSP_REC.SGRSTSP_ORSN_CODE,         V_SGRSTSP_REC.SGRSTSP_PRAC_CODE,
                            V_SGRSTSP_REC.SGRSTSP_CAPL_CODE,     V_SGRSTSP_REC.SGRSTSP_EDLV_CODE,         V_SGRSTSP_REC.SGRSTSP_INCM_CODE,
                            V_SGRSTSP_REC.SGRSTSP_EMEX_CODE,     V_SGRSTSP_REC.SGRSTSP_APRN_CODE,         V_SGRSTSP_REC.SGRSTSP_TRCN_CODE,
                            V_SGRSTSP_REC.SGRSTSP_GAIN_CODE,     V_SGRSTSP_REC.SGRSTSP_VOED_CODE,         V_SGRSTSP_REC.SGRSTSP_BLCK_CODE,
                            V_SGRSTSP_REC.SGRSTSP_EGOL_CODE,     V_SGRSTSP_REC.SGRSTSP_BSKL_CODE,         V_SGRSTSP_REC.SGRSTSP_ASTD_CODE,
                            V_SGRSTSP_REC.SGRSTSP_PREV_CODE,     V_SGRSTSP_REC.SGRSTSP_CAST_CODE  );
                  
            END LOOP;
            CLOSE C_SGRSTSP;
            
            -- PARA EL CASO QUE YA EXISTA EL PLAN PARA EL PERIODO -> Actualizar el estado
            UPDATE SGRSTSP 
                SET SGRSTSP_STSP_CODE = 'RV' ------------------ ESTADOS STVSTSP : 'RV' - Reserva
            WHERE SGRSTSP_PIDM = P_PIDM_ALUMNO 
            AND SGRSTSP_TERM_CODE_EFF = P_PERIODO 
            AND SGRSTSP_KEY_SEQNO = P_KEY_SEQNO
            AND SGRSTSP_STSP_CODE <> 'RV'; 
            
            
            -- #######################################################################
            -- CAMBIO ESTADO DEL ALUMNO PARA DICHO PERIODO 
            -- ####################################################################### 
            OPEN C_SGBSTDN;
            LOOP
                FETCH C_SGBSTDN INTO V_SGBSTDN_REC;
                EXIT WHEN C_SGBSTDN%NOTFOUND;
                
                  INSERT INTO SGBSTDN (
                            SGBSTDN_PIDM,                 SGBSTDN_TERM_CODE_EFF,          SGBSTDN_STST_CODE,
                            SGBSTDN_LEVL_CODE,            SGBSTDN_STYP_CODE,              SGBSTDN_TERM_CODE_MATRIC,
                            SGBSTDN_TERM_CODE_ADMIT,      SGBSTDN_EXP_GRAD_DATE,          SGBSTDN_CAMP_CODE,
                            SGBSTDN_FULL_PART_IND,        SGBSTDN_SESS_CODE,              SGBSTDN_RESD_CODE,
                            SGBSTDN_COLL_CODE_1,          SGBSTDN_DEGC_CODE_1,            SGBSTDN_MAJR_CODE_1,
                            SGBSTDN_MAJR_CODE_MINR_1,     SGBSTDN_MAJR_CODE_MINR_1_2,     SGBSTDN_MAJR_CODE_CONC_1,
                            SGBSTDN_MAJR_CODE_CONC_1_2,   SGBSTDN_MAJR_CODE_CONC_1_3,     SGBSTDN_COLL_CODE_2,
                            SGBSTDN_DEGC_CODE_2,          SGBSTDN_MAJR_CODE_2,            SGBSTDN_MAJR_CODE_MINR_2,
                            SGBSTDN_MAJR_CODE_MINR_2_2,   SGBSTDN_MAJR_CODE_CONC_2,       SGBSTDN_MAJR_CODE_CONC_2_2,
                            SGBSTDN_MAJR_CODE_CONC_2_3,   SGBSTDN_ORSN_CODE,              SGBSTDN_PRAC_CODE,
                            SGBSTDN_ADVR_PIDM,            SGBSTDN_GRAD_CREDIT_APPR_IND,   SGBSTDN_CAPL_CODE,
                            SGBSTDN_LEAV_CODE,            SGBSTDN_LEAV_FROM_DATE,         SGBSTDN_LEAV_TO_DATE,
                            SGBSTDN_ASTD_CODE,            SGBSTDN_TERM_CODE_ASTD,         SGBSTDN_RATE_CODE,
                            SGBSTDN_ACTIVITY_DATE,        SGBSTDN_MAJR_CODE_1_2,          SGBSTDN_MAJR_CODE_2_2,
                            SGBSTDN_EDLV_CODE,            SGBSTDN_INCM_CODE,              SGBSTDN_ADMT_CODE,
                            SGBSTDN_EMEX_CODE,            SGBSTDN_APRN_CODE,              SGBSTDN_TRCN_CODE,
                            SGBSTDN_GAIN_CODE,            SGBSTDN_VOED_CODE,              SGBSTDN_BLCK_CODE,
                            SGBSTDN_TERM_CODE_GRAD,       SGBSTDN_ACYR_CODE,              SGBSTDN_DEPT_CODE,
                            SGBSTDN_SITE_CODE,            SGBSTDN_DEPT_CODE_2,            SGBSTDN_EGOL_CODE,
                            SGBSTDN_DEGC_CODE_DUAL,       SGBSTDN_LEVL_CODE_DUAL,         SGBSTDN_DEPT_CODE_DUAL,
                            SGBSTDN_COLL_CODE_DUAL,       SGBSTDN_MAJR_CODE_DUAL,         SGBSTDN_BSKL_CODE,
                            SGBSTDN_PRIM_ROLL_IND,        SGBSTDN_PROGRAM_1,              SGBSTDN_TERM_CODE_CTLG_1,
                            SGBSTDN_DEPT_CODE_1_2,        SGBSTDN_MAJR_CODE_CONC_121,     SGBSTDN_MAJR_CODE_CONC_122,
                            SGBSTDN_MAJR_CODE_CONC_123,   SGBSTDN_SECD_ROLL_IND,          SGBSTDN_TERM_CODE_ADMIT_2,
                            SGBSTDN_ADMT_CODE_2,          SGBSTDN_PROGRAM_2,              SGBSTDN_TERM_CODE_CTLG_2,
                            SGBSTDN_LEVL_CODE_2,          SGBSTDN_CAMP_CODE_2,            SGBSTDN_DEPT_CODE_2_2,
                            SGBSTDN_MAJR_CODE_CONC_221,   SGBSTDN_MAJR_CODE_CONC_222,     SGBSTDN_MAJR_CODE_CONC_223,
                            SGBSTDN_CURR_RULE_1,          SGBSTDN_CMJR_RULE_1_1,          SGBSTDN_CCON_RULE_11_1,
                            SGBSTDN_CCON_RULE_11_2,       SGBSTDN_CCON_RULE_11_3,         SGBSTDN_CMJR_RULE_1_2,
                            SGBSTDN_CCON_RULE_12_1,       SGBSTDN_CCON_RULE_12_2,         SGBSTDN_CCON_RULE_12_3,
                            SGBSTDN_CMNR_RULE_1_1,        SGBSTDN_CMNR_RULE_1_2,          SGBSTDN_CURR_RULE_2,
                            SGBSTDN_CMJR_RULE_2_1,        SGBSTDN_CCON_RULE_21_1,         SGBSTDN_CCON_RULE_21_2,
                            SGBSTDN_CCON_RULE_21_3,       SGBSTDN_CMJR_RULE_2_2,          SGBSTDN_CCON_RULE_22_1,
                            SGBSTDN_CCON_RULE_22_2,       SGBSTDN_CCON_RULE_22_3,         SGBSTDN_CMNR_RULE_2_1,
                            SGBSTDN_CMNR_RULE_2_2,        SGBSTDN_PREV_CODE,              SGBSTDN_TERM_CODE_PREV,
                            SGBSTDN_CAST_CODE,            SGBSTDN_TERM_CODE_CAST,         SGBSTDN_DATA_ORIGIN,
                            SGBSTDN_USER_ID,              SGBSTDN_SCPC_CODE ) 
                  VALUES (  V_SGBSTDN_REC.SGBSTDN_PIDM,                 P_PERIODO,                                    'IS',/*STVSTST: 'IS'-Inactivo*/
                            V_SGBSTDN_REC.SGBSTDN_LEVL_CODE,            V_SGBSTDN_REC.SGBSTDN_STYP_CODE,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_MATRIC,
                            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT,      V_SGBSTDN_REC.SGBSTDN_EXP_GRAD_DATE,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE,
                            V_SGBSTDN_REC.SGBSTDN_FULL_PART_IND,        V_SGBSTDN_REC.SGBSTDN_SESS_CODE,              V_SGBSTDN_REC.SGBSTDN_RESD_CODE,
                            V_SGBSTDN_REC.SGBSTDN_COLL_CODE_1,          V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_1,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1,
                            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1_2,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1,
                            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_3,     V_SGBSTDN_REC.SGBSTDN_COLL_CODE_2,
                            V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2,
                            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_2,
                            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_3,   V_SGBSTDN_REC.SGBSTDN_ORSN_CODE,              V_SGBSTDN_REC.SGBSTDN_PRAC_CODE,
                            V_SGBSTDN_REC.SGBSTDN_ADVR_PIDM,            V_SGBSTDN_REC.SGBSTDN_GRAD_CREDIT_APPR_IND,   V_SGBSTDN_REC.SGBSTDN_CAPL_CODE,
                            V_SGBSTDN_REC.SGBSTDN_LEAV_CODE,            V_SGBSTDN_REC.SGBSTDN_LEAV_FROM_DATE,         V_SGBSTDN_REC.SGBSTDN_LEAV_TO_DATE,
                            V_SGBSTDN_REC.SGBSTDN_ASTD_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ASTD,         V_SGBSTDN_REC.SGBSTDN_RATE_CODE,
                            SYSDATE,                                    V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2_2,
                            V_SGBSTDN_REC.SGBSTDN_EDLV_CODE,            V_SGBSTDN_REC.SGBSTDN_INCM_CODE,              V_SGBSTDN_REC.SGBSTDN_ADMT_CODE,
                            V_SGBSTDN_REC.SGBSTDN_EMEX_CODE,            V_SGBSTDN_REC.SGBSTDN_APRN_CODE,              V_SGBSTDN_REC.SGBSTDN_TRCN_CODE,
                            V_SGBSTDN_REC.SGBSTDN_GAIN_CODE,            V_SGBSTDN_REC.SGBSTDN_VOED_CODE,              V_SGBSTDN_REC.SGBSTDN_BLCK_CODE,
                            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_GRAD,       V_SGBSTDN_REC.SGBSTDN_ACYR_CODE,              V_SGBSTDN_REC.SGBSTDN_DEPT_CODE,
                            V_SGBSTDN_REC.SGBSTDN_SITE_CODE,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2,            V_SGBSTDN_REC.SGBSTDN_EGOL_CODE,
                            V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_DUAL,
                            V_SGBSTDN_REC.SGBSTDN_COLL_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_BSKL_CODE,
                            V_SGBSTDN_REC.SGBSTDN_PRIM_ROLL_IND,        V_SGBSTDN_REC.SGBSTDN_PROGRAM_1,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CTLG_1,
                            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_1_2,        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_121,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_122,
                            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_123,   V_SGBSTDN_REC.SGBSTDN_SECD_ROLL_IND,          V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT_2,
                            V_SGBSTDN_REC.SGBSTDN_ADMT_CODE_2,          V_SGBSTDN_REC.SGBSTDN_PROGRAM_2,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CTLG_2,
                            V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_2,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE_2,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2_2,
                            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_221,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_222,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_223,
                            V_SGBSTDN_REC.SGBSTDN_CURR_RULE_1,          V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_1,          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_1,
                            V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_3,         V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_2,
                            V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_1,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_2,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_3,
                            V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_1,        V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_2,          V_SGBSTDN_REC.SGBSTDN_CURR_RULE_2,
                            V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_1,        V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_1,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_2,
                            V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_3,       V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_2,          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_1,
                            V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_3,         V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_1,
                            V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_2,        V_SGBSTDN_REC.SGBSTDN_PREV_CODE,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_PREV,
                            V_SGBSTDN_REC.SGBSTDN_CAST_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CAST,         'WorkFlow',
                            USER,                                       V_SGBSTDN_REC.SGBSTDN_SCPC_CODE );
                  
            END LOOP;
            CLOSE C_SGBSTDN;
            
            -- PARA EL CASO QUE YA EXISTA EL PLAN PARA EL PERIODO -> Actualizar el estado
            UPDATE SGBSTDN
                SET SGBSTDN_STST_CODE = 'IS'  ------------------ ESTADOS STVSTST : 'IS' - Inactivo
            WHERE SGBSTDN_PIDM = P_PIDM_ALUMNO
            AND SGBSTDN_TERM_CODE_EFF = P_PERIODO
            AND SGBSTDN_STST_CODE <> 'IS';
            
            
            --
            COMMIT; 
        END IF;
EXCEPTION
    WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_ELIMINAR_NRCS;