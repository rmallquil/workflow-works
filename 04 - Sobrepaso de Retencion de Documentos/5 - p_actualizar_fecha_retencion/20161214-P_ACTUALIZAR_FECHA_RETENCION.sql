
/*
SET SERVEROUTPUT ON
DECLARE   P_ERROR               VARCHAR2(200);
          P_FROM_DATE           SPRHOLD.SPRHOLD_FROM_DATE%TYPE;
BEGIN
    --WFK_CONTISRD.P_ACTUALIZAR_FECHA_RETENCION(20451,'02','30-Dec-2016 16:28:36',P_ERROR);
    P_ACTUALIZAR_FECHA_RETENCION(20451,'11','30-Dec-2016 16:28:36',P_ERROR);
    DBMS_OUTPUT.PUT_LINE(SQL%ROWCOUNT || ' ---- ' || P_ERROR );
    
--      SELECT TO_DATE('30-Dec-2016 16:28:36','DD-MON-YYYY HH24:MI:SS','NLS_DATE_LANGUAGE = American') 
--      INTO P_FROM_DATE
--      FROM DUAL;
--      
--      DBMS_OUTPUT.PUT_LINE(SQL%ROWCOUNT || ' ---- ' || P_FROM_DATE );
END;
*/

-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CREATE OR REPLACE PROCEDURE P_ACTUALIZAR_FECHA_RETENCION
(
      P_ID_ALUMNO           IN  SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_COD_RETENCION       IN  STVHLDD.STVHLDD_CODE%TYPE,
      P_FECHA_RETENCION     IN  VARCHAR2,
      P_ERROR               OUT VARCHAR2
)
AS
      P_INDICADOR               NUMBER;
      E_INVALID_COD_RETENCION   EXCEPTION;
      P_FROM_DATE               SPRHOLD.SPRHOLD_FROM_DATE%TYPE;
BEGIN
      
      -- CONVERTIR LA FECHA A UN FORMATO VALIDO
      --SELECT TO_DATE('30-Dec-2016 16:28:36','DD-MON-YYYY HH24:MI:SS','NLS_DATE_LANGUAGE = American') 
      SELECT TO_DATE(P_FECHA_RETENCION,'DD-MON-YYYY HH24:MI:SS','NLS_DATE_LANGUAGE = American') 
      INTO P_FROM_DATE
      FROM DUAL;

      
      -- VALIDAR codigo del TIPO DE RETENCION - STVHLDD
      SELECT COUNT(*) INTO P_INDICADOR 
      FROM STVHLDD
      WHERE STVHLDD_CODE = P_COD_RETENCION;
      
      -- INSERTAR REGISTRO DE FECHA DE RETENCION DOCUMENTOS - SOAHOLD
      IF ( P_INDICADOR = 0 ) THEN
      
            RAISE E_INVALID_COD_RETENCION;
      
      ELSE
            
            INSERT INTO SPRHOLD 
              (   SPRHOLD_PIDM,
                  SPRHOLD_HLDD_CODE,
                  SPRHOLD_USER,
                  SPRHOLD_FROM_DATE,
                  SPRHOLD_TO_DATE,
                  SPRHOLD_RELEASE_IND,
                  SPRHOLD_REASON,
                  SPRHOLD_AMOUNT_OWED,
                  SPRHOLD_ORIG_CODE,
                  SPRHOLD_ACTIVITY_DATE,
                  SPRHOLD_DATA_ORIGIN
              ) 
            VALUES 
              (   P_ID_ALUMNO,
                  P_COD_RETENCION,
                  'WORKFLOW',
                  P_FROM_DATE,
                  TO_DATE('31/12/99','DD/MM/RR'),
                  'N',
                  NULL,
                  NULL,
                  NULL,
                  P_FROM_DATE,
                  'WorkFlow');
            
            COMMIT;
      END IF;

EXCEPTION
  WHEN E_INVALID_COD_RETENCION THEN
          P_ERROR  := 'El "CÒDIGO DE RETENCION" enviado es invàlido';
  WHEN OTHERS THEN
          --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_ACTUALIZAR_FECHA_RETENCION;