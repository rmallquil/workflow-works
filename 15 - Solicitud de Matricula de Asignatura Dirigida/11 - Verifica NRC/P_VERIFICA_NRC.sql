/*
SET SERVEROUTPUT ON
DECLARE P_STATUS VARCHAR2(4000);
 P_DOCENTE VARCHAR2(4000);
 P_MESSAGE VARCHAR2(4000);
BEGIN
    P_VERIFICA_NRC(7163, '201810', 'S01', 'UREG', P_STATUS, P_DOCENTE, P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_STATUS);
    DBMS_OUTPUT.PUT_LINE(P_DOCENTE);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
END;
*/

CREATE OR REPLACE PROCEDURE P_VERIFICA_NRC (
        P_NRC_CODE          IN SSBSECT.SSBSECT_CRN%TYPE,
        P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
        P_COD_SEDE          IN STVCAMP.STVCAMP_CODE%TYPE,
        P_DEPT_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
        P_STATUS            OUT VARCHAR2,
        P_DOCENTE           OUT VARCHAR2,
        P_MESSAGE           OUT VARCHAR2
)
AS
        V_CALIF             INTEGER := 0;
        V_VIWEB             INTEGER := 0;
        V_COSTO             INTEGER := 0;
        V_CREDI             INTEGER := 0;
        V_ULTDI             INTEGER := 0;
        V_VACAN             INTEGER := 0;
        V_DOCEN             INTEGER := 0;
        V_NOM_DOCEN         VARCHAR2(4000);
        V_RESPUESTA         VARCHAR2(4000) := NULL;
        V_FLAG              INTEGER := 0;

BEGIN
   ---VALIDACIÓN DE CONDICIÓN CALIFICABLE
    SELECT COUNT(*)
    INTO V_CALIF
    FROM SSBSECT
    WHERE SSBSECT_TERM_CODE = P_TERM_CODE
    AND SSBSECT_CRN = P_NRC_CODE
    AND SSBSECT_GRADABLE_IND = 'Y';
    
    IF V_CALIF <= 0 THEN
        V_RESPUESTA := '- El NRC no tiene la condición calificable. ';
        V_FLAG := 1;
    END IF;
    
    DBMS_OUTPUT.PUT_LINE('calificable ' || V_CALIF);
    
    --VALIDACIÓN DE VISUALIZACIÓN WEB DESACTIVADA
    SELECT COUNT(*)
    INTO V_VIWEB
    FROM SSBSECT
    WHERE SSBSECT_TERM_CODE = P_TERM_CODE
    AND SSBSECT_CRN = P_NRC_CODE
    AND SSBSECT_VOICE_AVAIL = 'N';
    
    IF V_VIWEB <= 0 THEN
        V_RESPUESTA := V_RESPUESTA || '- El NRC tiene activado la visualización web. ';
        V_FLAG := 1;
    END IF;

    DBMS_OUTPUT.PUT_LINE('via web ' || V_VIWEB);

    --VALIDACIÓN DE HORAS COBRO
    SELECT COUNT(*)
    INTO V_COSTO
    FROM SSBSECT
    WHERE SSBSECT_TERM_CODE = P_TERM_CODE
    AND SSBSECT_CRN = P_NRC_CODE
    AND SSBSECT_BILL_HRS > 0;
    
    IF V_COSTO <= 0 THEN
        V_RESPUESTA := V_RESPUESTA || '- El NRC tiene horas cobro en cero. ';
        V_FLAG := 1;
    END IF;
    
    DBMS_OUTPUT.PUT_LINE('costo ' || V_COSTO);
    
    --VALIDACIÓN DE HORAS CREDITO
    SELECT COUNT(*)
    INTO V_CREDI
    FROM SSBSECT
    WHERE SSBSECT_TERM_CODE = P_TERM_CODE
    AND SSBSECT_CRN = P_NRC_CODE
    AND SSBSECT_CREDIT_HRS > 0;
    
    IF V_CREDI <= 0 THEN
        V_RESPUESTA := V_RESPUESTA || '- El NRC tiene horas crédito en cero. ';
        V_FLAG := 1;
    END IF;
    
    DBMS_OUTPUT.PUT_LINE('creditos ' || V_CREDI);
    
   ---VALIDACIÓN DE ÚLTIMO DÍGITO DE SECCIÓN
    SELECT COUNT(*)
    INTO V_ULTDI
    FROM SSBSECT
    WHERE SSBSECT_TERM_CODE = P_TERM_CODE
    AND SSBSECT_CRN = P_NRC_CODE
    AND SUBSTR(SSBSECT_SEQ_NUMB,-1,1) = 'D';
    
    IF V_ULTDI <= 0 THEN
        V_RESPUESTA := V_RESPUESTA || '- El NRC no tiene la sección correctamente configurada. ';
        V_FLAG := 1;
    END IF;
    
    DBMS_OUTPUT.PUT_LINE('ultimo digi ' || V_ULTDI);
    
   ---VALIDACIÓN DE VACANTES
    SELECT COUNT(*)
    INTO V_VACAN
    FROM SSBSECT
    WHERE SSBSECT_TERM_CODE = P_TERM_CODE
    AND SSBSECT_CRN = P_NRC_CODE
    AND SSBSECT_MAX_ENRL > 0;
    
    IF V_VACAN <= 0 THEN
        V_RESPUESTA := V_RESPUESTA || '- El NRC no tiene configurado las vacantes. ';
        V_FLAG := 1;
    END IF;
    
    DBMS_OUTPUT.PUT_LINE('vacantes ' || V_VACAN);
    
    --VALIDACIÓN DE ASIGNACIÓN DE DOCENTE
    SELECT COUNT(*)
    INTO V_DOCEN
    FROM SIRASGN
    WHERE SIRASGN_CRN = P_NRC_CODE
    AND SIRASGN_TERM_CODE = P_TERM_CODE;

    IF V_DOCEN <= 0 THEN
        V_RESPUESTA := V_RESPUESTA || '- El NRC no tiene asignado docente. ';
        V_FLAG := 1;
    END IF;
    
    DBMS_OUTPUT.PUT_LINE('docente ' || V_DOCEN);
    
    --VALIDACIÓN FINAL
    IF V_FLAG > 0 THEN
        P_STATUS := 'FALSE';
        V_NOM_DOCEN := '';
        P_MESSAGE := V_RESPUESTA;
    ELSE
        P_STATUS := 'TRUE';
        P_MESSAGE := 'OK';
        
        SELECT SPRIDEN_LAST_NAME || ' ' || SPRIDEN_FIRST_NAME
        INTO P_DOCENTE
        FROM SIRASGN
        INNER JOIN SPRIDEN ON
            SIRASGN_PIDM = SPRIDEN_PIDM
        WHERE SIRASGN_CRN = P_NRC_CODE
        AND SIRASGN_TERM_CODE = P_TERM_CODE;
    END IF;
    
EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICA_NRC;