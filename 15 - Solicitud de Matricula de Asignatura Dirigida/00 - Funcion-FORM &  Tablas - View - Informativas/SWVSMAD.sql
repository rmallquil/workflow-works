/**********************************************************************************************/
/* SWVSMAD.sql                                                                                */
/**********************************************************************************************/
/*                                                                                            */
/* Descripción corta: Script para generar la vista SWVSMAD                                    */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI             FECHA       */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creación del Código.                                        BYF           02/AGO/2018   */
/*    --------------------                                                                    */
/*    Se crea la vista SWVSMAD para Listar un max de 18 asignaturas pendientes para el        */
/*      estudiante según su proyección de asignaturas.                                        */
/* 2. Actualización de vista                                                                  */
/*    Se agrega el filtro para no mostrar actividades.                                        */
/*                                                                                            */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/ 

CREATE OR REPLACE VIEW "BANINST1".SWVSMAD ("CURSE_CODE", "CURSE_NAME") AS
/* ===================================================================================================================
  NOMBRE    : SWVSMAD
              SWView WorkFlow Solicitud Matricula de Asignatura Dirigida
  FECHA     : 02/08/2018
  AUTOR     : Flores Vilcapoma, Brian Yusef
  OBJETIVO  : Lista un max de 18 asignaturas pendientes para el estudiante según su proyección de asignaturas.
  =================================================================================================================== */
SELECT SFRREGP_SUBJ_CODE||SFRREGP_CRSE_NUMB_LOW CURSE_CODE, SCRSYLN_LONG_COURSE_TITLE CURSE_NAME
FROM (SELECT SFRREGP_SUBJ_CODE, SFRREGP_CRSE_NUMB_LOW, SCRSYLN_LONG_COURSE_TITLE
    FROM SFRREGP
INNER JOIN SCRSYLN 
    ON SCRSYLN_SUBJ_CODE = SFRREGP_SUBJ_CODE
    AND SCRSYLN_CRSE_NUMB = SFRREGP_CRSE_NUMB_LOW
WHERE SFRREGP_PIDM = BVSKOSAJ.F_GetParamValue('SOL014',999)
AND SFRREGP_TERM_CODE = (SELECT DISTINCT SFRRSTS_TERM_CODE
                            FROM SFRRSTS
                            WHERE SFRRSTS_PTRM_CODE like '%1'
                            AND SFRRSTS_RSTS_CODE = 'RW'
                            AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN SFRRSTS_START_DATE  AND SFRRSTS_END_DATE)
AND SFRREGP_Z_VISIBLE_IND = 'Y'
AND SFRREGP_SUBJ_CODE <> 'ACUC'
ORDER BY SFRREGP_AREA_PRIORITY)
WHERE ROWNUM <= 18;
