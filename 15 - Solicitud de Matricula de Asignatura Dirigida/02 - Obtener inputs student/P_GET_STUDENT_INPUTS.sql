/*
SET SERVEROUTPUT ON
DECLARE P_ATTRB_PLAN VARCHAR2(4000);
 P_ASIGNATURA_CODE VARCHAR2(4000);
 P_ASIGNATURA_DESC VARCHAR2(4000);
 P_MESSAGE VARCHAR2(4000);
BEGIN
    P_GET_STUDENT_INPUTS(123287, '201810', , P_ATTRB_PLAN, P_ASIGNATURA_CODE, P_ASIGNATURA_DESC, P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_ATTRB_PLAN);
    DBMS_OUTPUT.PUT_LINE(P_ASIGNATURA_CODE);
    DBMS_OUTPUT.PUT_LINE(P_ASIGNATURA_DESC);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
END;
*/

CREATE OR REPLACE PROCEDURE P_GET_STUDENT_INPUTS
(
    P_PIDM                 IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE            IN STVDEPT.STVDEPT_CODE%TYPE,
    P_FOLIO_SOLICITUD      IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_ATTRB_PLAN           OUT SGRSATT.SGRSATT_ATTS_CODE%TYPE,
    P_ASIGNATURA_CODE      OUT VARCHAR2,
    P_ASIGNATURA_DESC      OUT VARCHAR2,
    P_MESSAGE              OUT VARCHAR2
)
AS 
  
    V_ERROR                 EXCEPTION;
    V_ERROR2                EXCEPTION;
    V_ERROR3                EXCEPTION;
    V_ASIG                  INTEGER := 0;
    V_ASIGNATURA_NOMBRE     VARCHAR2(4000);
    V_CODIGO_SOLICITUD      SVVSRVC.SVVSRVC_CODE%TYPE;
    V_COMENTARIO            VARCHAR2(4000);
    V_CLAVE                 VARCHAR2(4000);
    V_ADDL_DATA_SEQ         SVRSVAD.SVRSVAD_ADDL_DATA_SEQ%TYPE;
    
    CURSOR C_SGRSATT IS
        SELECT SGRSATT_ATTS_CODE
        FROM (
            SELECT SGRSATT_ATTS_CODE 
            FROM SGRSATT
            WHERE SGRSATT_PIDM = P_PIDM
                AND SUBSTR(SGRSATT_ATTS_CODE,1,2) = 'P0'
                ORDER BY SGRSATT_TERM_CODE_EFF DESC
            )
        WHERE ROWNUM = 1;

    -- GET COMENTARIO de la solicitud. (comentario PERSONALZIADO)
    CURSOR C_SVRSVAD IS
        SELECT SVRSVAD_ADDL_DATA_SEQ, SVRSVAD_ADDL_DATA_CDE, SVRSVAD_ADDL_DATA_DESC
        FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
            INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
        ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
        WHERE SVRSRAD_SRVC_CODE = V_CODIGO_SOLICITUD
            AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
        ORDER BY SVRSVAD_ADDL_DATA_SEQ;

BEGIN
    --################################################################################################
    --OBTENIENDO ATRIBUTO DE PLAN
    --################################################################################################
    OPEN C_SGRSATT;
    FETCH C_SGRSATT INTO P_ATTRB_PLAN;
        IF C_SGRSATT%NOTFOUND THEN
            RAISE V_ERROR;
        END IF;
    CLOSE C_SGRSATT;

    --################################################################################################
    --OBTENIENDO CODIGO DE ASIGNATURA
    --################################################################################################

    -- GET CODIGO SOLICITUD
    SELECT SVRSVPR_SRVC_CODE 
    INTO V_CODIGO_SOLICITUD 
    FROM SVRSVPR 
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
    
    -- OBTENER CODIGO DE ASIGNATURA DE LA SOLICITUD
    OPEN C_SVRSVAD;
    LOOP
      FETCH C_SVRSVAD INTO V_ADDL_DATA_SEQ, V_CLAVE, V_COMENTARIO;
      IF C_SVRSVAD%FOUND THEN
          IF V_ADDL_DATA_SEQ = 1 THEN
              -- OBTENER CODIGO DE ASIGNATURA DE LA SOLICITUD
              P_ASIGNATURA_CODE := V_COMENTARIO;
          END IF;
      ELSE EXIT;
    END IF;
    END LOOP;
    CLOSE C_SVRSVAD;
    
    IF (P_ASIGNATURA_CODE IS NULL) THEN
      RAISE V_ERROR2;
    END IF;
    
    --################################################################################################
    --OBTENIENDO DESCRIPCION DE ASIGNATURA
    --################################################################################################
    SELECT COUNT(*)
    INTO V_ASIG
    FROM SCRSYLN
    WHERE SCRSYLN_SUBJ_CODE || SCRSYLN_CRSE_NUMB = P_ASIGNATURA_CODE;
    
    IF V_ASIG > 0 THEN
        SELECT SCRSYLN_LONG_COURSE_TITLE
        INTO V_ASIGNATURA_NOMBRE
        FROM SCRSYLN
        WHERE SCRSYLN_SUBJ_CODE || SCRSYLN_CRSE_NUMB = P_ASIGNATURA_CODE;
        
        P_ASIGNATURA_DESC := P_ATTRB_PLAN || ' - ' || P_ASIGNATURA_CODE || ' - ' || V_ASIGNATURA_NOMBRE;
        
    ELSE
        RAISE V_ERROR3;
    END IF;

    P_MESSAGE := 'OK';
    
    EXCEPTION
    WHEN V_ERROR THEN
        P_MESSAGE:='No tiene registrado atributo de plan de estudio.';
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| P_MESSAGE);
    WHEN V_ERROR2 THEN
        P_MESSAGE:='La asignatura no fue seleccionada correctamente o no existe.';
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| P_MESSAGE);
    WHEN V_ERROR3 THEN
        P_MESSAGE:='No existe registro de la asignatura solicitada.';
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| P_MESSAGE);
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_STUDENT_INPUTS;