/******************************************************************************/
/* BWZKSMAD.sql                                                               */
/******************************************************************************/
/*                                                                            */
/* Descripción corta: Script para generar el Paquete relacionado a los        */
/*                    procedimientos de Matricula de Asignatura Dirigida      */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                 INI    FECHA    */
/* ---------------------------------------------------------- --- ----------- */
/* 1. Creación del código.                                    BFV 03/JUL/2018 */
/*    Creación del paquete de Matricula de Asignatura Dirigida                */
/*                                                                            */
/*    Procedure P_GET_INFO_STUDENT: Obtiene el atributo de plan y el curso    */
/*      seleccionado para el pedido.                                          */
/*    Procedure P_VERIFICAR_APTO: Verifica que el estudiante solicitante      */
/*      cumplas las condiciones para hacer uso del WF.                        */
/*    Procedure P_OBTENER_COMENTARIO(ELIMINADO): Obtiene el comentario y el   */
/*      telefono que redacta el estudiante en el WF.                          */
/*    Procedure P_GET_USUARIO: En base a una sede y un rol, el                */
/*      procedimiento obtiene LOS CORREOS del(os) responsable(s) asignados    */
/*      a dicho ROL + SEDE (ROL_SEDE).                                        */
/*    Procedure P_GET_COORDINADOR: Obtiene el rol y el correo del coordinador */
/*      según su carrera.                                                     */
/*    Procedure P_GET_USER_PROGRAMACION: Obtiene el rol y correo del usuario  */
/*      de la oficina de programacion academica.                              */
/*    Procedure P_VERIFICAR_ESTADO_SOLICITUD: Verifica si se anulo la         */
/*      solicitud por el mismo estudiante o no.                               */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud   */
/*    Procedure P_VERIFICAR_DOCENTE: Verifica que el docente asignado este    */
/*      registrado y activo para el periodo solicitado.                       */
/*    Procedure P_VERIFICA_NRC: Verifica que el NRC creado cumpla con todas   */
/*      las caracteristicas básicas de creación.                              */
/*    Procedure P_VERIFICAR_MATRICULA: Verifica que el estudiante no este     */
/*      matriculado en la asignatura solicitada.                              */
/*    Procedure P_INSERTA_NRC: Matricula al estudiante en el NRC solicitado   */
/* 2. Actualización P_GET_INFO_STUDENT:                       LAM 20/12/2018  */
/*      Se renombra el procedure P_GET_STUDENT_INPUTS porque se agrega un     */
/*      parámetro más de salida P_COMENTARIO que se obtenía en el procedure   */
/*      P_OBTENER_COMENTARIO.                                                 */
/*    Actualización P_OBTENER_COMENTARIO:                                     */
/*      Se elimina el procedure P_GET_INFO_STUDENT ya que se obtiene en el    */
/*      procedure.                                                            */
/* 3. Actualización P_INSERTA_NRC                             BYF 15/01/2019  */
/*      Se actualiza el SP para corregir la fecha de subida a historia, y el  */
/*      código de plan de estudios.                                           */
/*    Actualización P_VERIFICA_NRC                                            */
/*      Se actualiza uno de los mensajes de error para que sea mas claro al   */
/*      de enviar la respuesta al usuario.                                    */
/*    Actualizacion P_VERIFICAR_DOCENTE                                       */
/*      Se actualiza la consulta para que obtenga el registro del SPAIDEN.    */
/*    Actualizacion Paquete generico                                          */
/*      Se actualizan los SP's de P_VERIFICAR_ESTADO_SOLICITUD,               */
/*      P_CAMBIO_ESTADO_SOLICITUD para que se usen desde el paquete generico. */
/*    --------------------                                                    */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*                                                                            */
/* FIN DEL SEGUIMIENTO                                                        */
/*                                                                            */
/******************************************************************************/ 
/**********************************************************************************************/
-- PERMISOS DE EJECUCIÓN
/**********************************************************************************************/
--  SET SCAN ON 
-- 
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE 
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/

CREATE OR REPLACE PACKAGE BANINST1.BWZKSMAD AS
/*
 BWZKSMAD:
       Paquete Web _ Desarrollo Propio _ Paquete _ Conti Solicitud Matricula ASignatura Dirigida
*/
-- FILE NAME..: BWZKSMAD.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKSMAD
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2017
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              

PROCEDURE P_GET_INFO_STUDENT(
        P_PIDM                 IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE            IN STVDEPT.STVDEPT_CODE%TYPE,
        P_FOLIO_SOLICITUD      IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_ATTRB_PLAN           OUT SGRSATT.SGRSATT_ATTS_CODE%TYPE,
        P_ASIGNATURA_CODE      OUT VARCHAR2,
        P_ASIGNATURA_DESC      OUT VARCHAR2,
        P_COMENTARIO           OUT VARCHAR2,
        P_MESSAGE              OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_APTO (
        P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
        P_ASIGNATURA_CODE   IN VARCHAR2,
        P_STATUS_APTO       OUT VARCHAR2,
        P_REASON            OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_USUARIO (
        P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
        P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
        P_CORREO_USER         OUT VARCHAR2,
        P_ROL_SEDE            OUT VARCHAR2,
        P_MESSAGE             OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_COORDINADOR (
        P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, -- sensible mayuscula 'S01'
        P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE, --sensible mayuscula 'UREG'
        P_ROL_DC              IN WORKFLOW.ROLE.NAME%TYPE, -- sensible a mayusculas 'coordinador'
        P_CORREO_COORD        OUT VARCHAR2,
        P_ROL_COORD           OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_USER_PROGRAMACION (
        P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE, --sensible mayuscula 'UREG'
        P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, -- sensible mayuscula 'S01'
        P_ROL_PROGRAMACION    IN WORKFLOW.ROLE.NAME%TYPE, -- sensible a minusculas 'progracad'
        P_CORREO_PROGR        OUT VARCHAR2,
        P_ROL_PROGR           OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_ESTADO_SOLICITUD (
        P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_STATUS_SOL          OUT VARCHAR2,
        P_MESSAGE             OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
        P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
        P_AN                  OUT NUMBER,
        P_MESSAGE             OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_DOCENTE (
        P_DOCENTE           IN VARCHAR2,
        P_TERM_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
        P_DOCENTE_DNI       OUT VARCHAR2,
        P_DOCENTE_NAME      OUT VARCHAR2,
        P_DOCENTE_EMAIL     OUT VARCHAR2,
        P_STATUS_DOC        OUT VARCHAR2,
        P_REASON_DOC        OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICA_NRC (
        P_NRC_CODE          IN SSBSECT.SSBSECT_CRN%TYPE,
        P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
        P_COD_SEDE          IN STVCAMP.STVCAMP_CODE%TYPE,
        P_DEPT_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
        P_ASIGNATURA_CODE   IN VARCHAR2,
        P_DOCENTE_DNI       IN SPRIDEN.SPRIDEN_ID%TYPE,
        P_STATUS_NRC        OUT VARCHAR2,
        P_MESSAGE           OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_MATRICULA (
        P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_ASIGNATURA_CODE   IN VARCHAR2,
        P_TERM_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
        P_STATUS            OUT VARCHAR2,
        P_REASON_MATRICULA  OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_INSERTA_NRC (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_NRC_CODE        IN SSBSECT.SSBSECT_CRN%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_MESSAGE         OUT VARCHAR2
);
    
--------------------------------------------------------------------------------

END BWZKSMAD;

/**********************************************************************************************/
--  /
--  show errors 
-- 
--  SET SCAN ON
--  WHENEVER SQLERROR CONTINUE
--  DROP PUBLIC SYNONYM BWZKSMAD;
--  WHENEVER SQLERROR EXIT ROLLBACK
--  CREATE PUBLIC SYNONYM BWZKSMAD FOR BANINST1.BWZKSMAD;
--  WHENEVER SQLERROR CONTINUE
--  START gurgrtb BWZKSMAD
--  START gurgrth BWZKSMAD
--  WHENEVER SQLERROR EXIT ROLLBACK
/**********************************************************************************************/