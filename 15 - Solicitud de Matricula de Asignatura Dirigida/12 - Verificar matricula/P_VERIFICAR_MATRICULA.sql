/*
SET SERVEROUTPUT ON
DECLARE P_STATUS VARCHAR2(4000);
 P_REASON_MATRICULA VARCHAR2(4000);
BEGIN
    P_VERIFICAR_MATRICULA(142, 'AAUC00424', '201810', P_STATUS, P_REASON_MATRICULA);
    DBMS_OUTPUT.PUT_LINE(P_STATUS);
    DBMS_OUTPUT.PUT_LINE(P_REASON_MATRICULA);
END;
*/

CREATE OR REPLACE PROCEDURE P_VERIFICAR_MATRICULA (
        P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_ASIGNATURA_CODE   IN VARCHAR2,
        P_TERM_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
        P_STATUS            OUT VARCHAR2,
        P_REASON_MATRICULA  OUT VARCHAR2
)
AS
        V_ASI_MAT           INTEGER := 0; --VALIDA LA ASIGNATURA MATRICULADA
        
BEGIN
    --################################################################################################
    -- >> VALIDACIÓN DE CURSO MATRICULADO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_ASI_MAT
    FROM SFRSTCR
    INNER JOIN SSBSECT ON
        SFRSTCR_TERM_CODE = SSBSECT_TERM_CODE
        and SFRSTCR_CRN = SSBSECT_CRN
    WHERE SFRSTCR_TERM_CODE = P_TERM_CODE
        AND SFRSTCR_PIDM = P_PIDM
        AND SFRSTCR_RSTS_CODE IN ('RW','RE')
        AND SSBSECT_SUBJ_CODE || SSBSECT_CRSE_NUMB = P_ASIGNATURA_CODE;

    IF V_ASI_MAT > 0 THEN
        P_STATUS := 'TRUE';
        P_REASON_MATRICULA := 'Ya te encuentras matriculado(a) en la asignatura solicitada. ';            
    ELSE
        P_STATUS := 'FALSE';
        P_REASON_MATRICULA := 'OK';            
    END IF;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VERIFICAR_MATRICULA;