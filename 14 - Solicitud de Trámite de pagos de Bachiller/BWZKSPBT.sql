/**********************************************************************************************/
/* BWZKSPBT.sql                                                                               */
/**********************************************************************************************/
/*                                                                                            */
/* Descripción corta: Script para generar el Paquete de automatización del proceso de         */
/*                    Solicitud  de pagos de Bachiller y Título.                              */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI             FECHA       */
/* -------------------------------------------------------------- --- ----------- ----------- */
/* 1. Creación del Código.                                        RML             28/SEP/2017 */
/*    --------------------                                                                    */
/*    Procedure P_GET_STUDENT_INPUTS: Obtiene los datos de AutoServicio ingresados            */
/*              por el estudiante concepto del idioma y si cumple con el requisito.           */
/*    --Paquete generico                                                                      */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud.                  */
/*    Procedure P_VERIFICA_CONCEPTOS_BACHILLER: Valida si se crearon los                      */
/*              conceptos para bachiller en el año(periodo) actual.                           */
/*    Procedure P_VERIFICA_CONCEPTOS_TITULO: Valida si se crearon los conceptos               */
/*              para Titulación en el año(periodo) actual.                                    */
/*    Procedure P_SET_CONCEPTO_BACHILLER: Genera cargos de Bachiller                          */
/*    Procedure P_SET_CONCEPTO_TITULO: Genera cargos de Titulación                            */
/*                                                                                            */
/* -------------------------------------------------------------------------------------------*/
/* 2. Actualización de P_GET_STUDENT_INPUTS.                      INI             FECHA       */
/*    Se agrega P_OPT_REQUISITOS, la respuesta de estudiante si   LAM             22/NOV/2017 */
/*    cumple o no con el requisito.                                                           */
/* 3. Actualizacion Paquete generico                              BYF             04/FEB/2019 */
/*    Se actualiza la consulta del SP P_CAMBIO_ESTADO_SOLICITUD para que se obtenga desde el  */
/*      paquete generico.                                                                     */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/ 
CREATE OR REPLACE PACKAGE BWZKSPBT AS

PROCEDURE P_GET_STUDENT_INPUTS (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_DIPLOMA_CODE        OUT VARCHAR2,
    P_DIPLOMA_DESC        OUT VARCHAR2,
    P_CONCEPTO_IDIOMA     OUT VARCHAR2,
    P_IDIOMA_DESC         OUT VARCHAR2,
    P_OPT_REQUISITOS      OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
    P_AN                  OUT NUMBER,
    P_ERROR               OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICA_CONCEPTOS_BACHILLER ( 
    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
    P_SECCION_CAJA_BACH     IN VARCHAR2,
    P_CONCEPTO_BACH_01      IN VARCHAR2,
    P_CONCEPTO_BACH_02      IN VARCHAR2,
    P_CONCEPTO_BACH_03      IN VARCHAR2,
    P_CONCEPTO_BACH_04      IN VARCHAR2,
    P_CONCEPTO_BACH_05      IN VARCHAR2,
    P_CONCEPTO_BACH_06      IN VARCHAR2,
    P_CONCEPTO_BACH_07      IN VARCHAR2,
    P_CONCEPTO_BACH_08      IN VARCHAR2,
    P_CONCEPTO_BACH_09      IN VARCHAR2,   
    P_PROGRAM               OUT SORLCUR.SORLCUR_PROGRAM%TYPE,     
    P_CONCEPTOS_VALIDOS     OUT VARCHAR2,
    P_MESSAGE               OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICA_CONCEPTOS_TITULO ( 
    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
    P_SECCION_CAJA_TITU     IN VARCHAR2,
    P_CONCEPTO_TITU_01      IN VARCHAR2,
    P_CONCEPTO_TITU_02      IN VARCHAR2,
    P_PROGRAM               OUT SORLCUR.SORLCUR_PROGRAM%TYPE,
    P_CONCEPTOS_VALIDOS     OUT VARCHAR2,
    P_MESSAGE               OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_SET_CONCEPTO_BACHILLER ( 
    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_FOLIO_SOLICITUD       IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,       
    P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
    P_PROGRAM               IN SORLCUR.SORLCUR_PROGRAM%TYPE,     
    P_SECCION_CAJA_BACH     IN VARCHAR2,
    P_CONCEPTO_BACH_01      IN VARCHAR2,
    P_CONCEPTO_BACH_02      IN VARCHAR2,
    P_CONCEPTO_BACH_03      IN VARCHAR2,
    P_CONCEPTO_BACH_04      IN VARCHAR2,
    P_CONCEPTO_BACH_06      IN VARCHAR2,
    P_CONCEPTO_BACH_07      IN VARCHAR2,           
    P_CONCEPTO_IDIOMA       IN VARCHAR2,
    P_MESSAGE               OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_SET_CONCEPTO_TITULO ( 
    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_FOLIO_SOLICITUD       IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,       
    P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
    P_PROGRAM               IN SORLCUR.SORLCUR_PROGRAM%TYPE,     
    P_SECCION_CAJA_TITU     IN VARCHAR2,
    P_CONCEPTO_TITU_01      IN VARCHAR2,
    P_CONCEPTO_TITU_02      IN VARCHAR2,
    P_MESSAGE               OUT VARCHAR2
);

--------------------------------------------------------------------------------

END BWZKSPBT;