/*
SET SERVEROUTPUT ON
declare P_DIPLOMA_CODE        VARCHAR2(4000);
        P_DIPLOMA_DESC        VARCHAR2(4000);
        P_CONCEPTO_IDIOMA     VARCHAR2(4000);
        P_IDIOMA_DESC         VARCHAR2(4000);
begin
    P_GET_STUDENT_INPUTS(6669,P_DIPLOMA_CODE, P_DIPLOMA_DESC,P_CONCEPTO_IDIOMA,P_IDIOMA_DESC);
    DBMS_OUTPUT.PUT_LINE(P_DIPLOMA_CODE || ' - ' || P_DIPLOMA_DESC);
    DBMS_OUTPUT.PUT_LINE(P_CONCEPTO_IDIOMA  || ' - ' || P_IDIOMA_DESC);    
end;
*/

/* ===================================================================================================================
  NOMBRE    : P_GET_STUDENT_INPUTS
  FECHA     : 28/09/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene los datos de AUTOSERVICIO  ingresados en por el alumno: Tramite BACHILLER/TITULO , concepto del Idioma
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
CREATE or replace PROCEDURE P_GET_STUDENT_INPUTS (
      P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_DIPLOMA_CODE        OUT VARCHAR2,
      P_DIPLOMA_DESC        OUT VARCHAR2,
      P_CONCEPTO_IDIOMA     OUT VARCHAR2,
      P_IDIOMA_DESC         OUT VARCHAR2
)
AS
      V_ERROR                   EXCEPTION;
      V_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
      V_ADDL_DATA_CDE           SVRSVAD.SVRSVAD_ADDL_DATA_CDE%TYPE;
      V_ADDL_DATA_SEQ           SVRSVAD.SVRSVAD_ADDL_DATA_SEQ%TYPE;
      V_ADDL_DATA_DESC          SVRSVAD.SVRSVAD_ADDL_DATA_DESC%TYPE;
      
      -- GET COMENTARIO Y TELEFONO de la solicitud. (comentario PERSONALZIADO.)
      CURSOR C_SVRSVAD IS
        SELECT SVRSVAD_ADDL_DATA_SEQ, SVRSVAD_ADDL_DATA_CDE, SVRSVAD_ADDL_DATA_DESC
        FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
        INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
        ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
        WHERE SVRSRAD_SRVC_CODE = 'SOL018'
        --AND SVRSVAD_ADDL_DATA_CDE = 'PREGUNTA'
        AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
        ORDER BY SVRSVAD_ADDL_DATA_SEQ;
BEGIN
      
      -- GET CODIGO SOLICITUD
      SELECT SVRSVPR_SRVC_CODE 
      INTO V_CODIGO_SOLICITUD 
      FROM SVRSVPR 
      WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
      
      -- OBTENER COMENTARIO Y EL TELEFONO DE LA SOLICITUD (LOS DOS ULTIMOS DATOS)
      OPEN C_SVRSVAD;
        LOOP
          FETCH C_SVRSVAD INTO V_ADDL_DATA_SEQ, V_ADDL_DATA_CDE, V_ADDL_DATA_DESC;
          IF C_SVRSVAD%FOUND THEN
              IF V_ADDL_DATA_SEQ = 1 THEN
                P_DIPLOMA_CODE  := V_ADDL_DATA_CDE;
                P_DIPLOMA_DESC  := V_ADDL_DATA_DESC;
              ELSIF V_ADDL_DATA_SEQ = 2 THEN
                P_CONCEPTO_IDIOMA   := V_ADDL_DATA_CDE;
                P_IDIOMA_DESC       := V_ADDL_DATA_DESC;
              END IF;
          ELSE EXIT;
        END IF;
        END LOOP;
      CLOSE C_SVRSVAD;

      IF P_DIPLOMA_CODE IS NULL THEN
          RAISE V_ERROR;
      END IF;
      
EXCEPTION
  WHEN V_ERROR THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontró el identificador del DIPLOMA.' );
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_STUDENT_INPUTS;
