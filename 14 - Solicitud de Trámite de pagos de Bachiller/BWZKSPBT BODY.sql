/**********************************************************************************************/
/* BWZKSPBT.sql                                                                               */
/**********************************************************************************************/
/*                                                                                            */
/* Descripción corta: Script para generar el Paquete de automatización del proceso de         */
/*                    Solicitud  de pagos de Bachiller y Título.                              */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI             FECHA       */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creación del Código.                                        RML             28/SEP/2017 */
/*    --------------------                                                                    */
/*    Procedure P_GET_STUDENT_INPUTS: Obtiene los datos de AutoServicio ingresados            */
/*              por el estudiante concepto del idioma y si cumple con el requisito.           */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud.                  */
/*    Procedure P_VERIFICA_CONCEPTOS_BACHILLER: Valida si se crearon los                      */
/*              conceptos para bachiller en el año(periodo) actual.                           */
/*    Procedure P_VERIFICA_CONCEPTOS_TITULO: Valida si se crearon los conceptos               */
/*              para Titulación en el año(periodo) actual.                                    */
/*    Procedure P_SET_CONCEPTO_BACHILLER: Genera cargos de Bachiller                          */
/*    Procedure P_SET_CONCEPTO_TITULO: Genera cargos de Titulación                            */
/*                                                                                            */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/ 

create or replace PACKAGE BODY BWZKSPBT AS

PROCEDURE P_GET_STUDENT_INPUTS (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_DIPLOMA_CODE        OUT VARCHAR2,
    P_DIPLOMA_DESC        OUT VARCHAR2,
    P_CONCEPTO_IDIOMA     OUT VARCHAR2,
    P_IDIOMA_DESC         OUT VARCHAR2,
    P_OPT_REQUISITOS      OUT VARCHAR2
)
AS
    V_ERROR                   EXCEPTION;
    V_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
    V_ADDL_DATA_CDE           SVRSVAD.SVRSVAD_ADDL_DATA_CDE%TYPE;
    V_ADDL_DATA_SEQ           SVRSVAD.SVRSVAD_ADDL_DATA_SEQ%TYPE;
    V_ADDL_DATA_DESC          SVRSVAD.SVRSVAD_ADDL_DATA_DESC%TYPE;
    
    -- GET COMENTARIO Y TELEFONO de la solicitud. (comentario PERSONALZIADO.)
    CURSOR C_SVRSVAD IS
        SELECT SVRSVAD_ADDL_DATA_SEQ, SVRSVAD_ADDL_DATA_CDE, SVRSVAD_ADDL_DATA_DESC
        FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
        INNER JOIN SVRSRAD ON --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
            SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
        WHERE SVRSRAD_SRVC_CODE = 'SOL018'
            --AND SVRSVAD_ADDL_DATA_CDE = 'PREGUNTA'
            AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
        ORDER BY SVRSVAD_ADDL_DATA_SEQ;
BEGIN
      
    -- GET CODIGO SOLICITUD
    SELECT SVRSVPR_SRVC_CODE 
        INTO V_CODIGO_SOLICITUD 
    FROM SVRSVPR 
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
    
    -- OBTENER COMENTARIO Y EL TELEFONO DE LA SOLICITUD (LOS DOS ULTIMOS DATOS)
    OPEN C_SVRSVAD;
    LOOP
    FETCH C_SVRSVAD INTO V_ADDL_DATA_SEQ, V_ADDL_DATA_CDE, V_ADDL_DATA_DESC;
        IF C_SVRSVAD%FOUND THEN
            IF V_ADDL_DATA_SEQ = 1 THEN
                P_DIPLOMA_CODE  := V_ADDL_DATA_CDE;
                P_DIPLOMA_DESC  := V_ADDL_DATA_DESC;
            ELSIF V_ADDL_DATA_SEQ = 2 THEN
                P_CONCEPTO_IDIOMA   := V_ADDL_DATA_CDE;
                P_IDIOMA_DESC       := V_ADDL_DATA_DESC;
            -- GET DE CUMPLIR CON LOS REQUISITOS
            ELSIF V_ADDL_DATA_SEQ = 3 THEN
                P_OPT_REQUISITOS  := V_ADDL_DATA_CDE;
            END IF;
        ELSE EXIT;
        END IF;
    END LOOP;
    CLOSE C_SVRSVAD;
    
    IF P_DIPLOMA_CODE IS NULL THEN
        RAISE V_ERROR;
    END IF;
      
EXCEPTION
WHEN V_ERROR THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontró el identificador del DIPLOMA.' );
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_STUDENT_INPUTS;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
    P_AN                  OUT NUMBER,
    P_ERROR               OUT VARCHAR2
)
AS
BEGIN

    BWZKPSPG.P_CAMBIO_ESTADO_SOLICITUD (P_FOLIO_SOLICITUD, P_ESTADO, P_AN, P_ERROR);  

EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CAMBIO_ESTADO_SOLICITUD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICA_CONCEPTOS_BACHILLER ( 
    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
    P_SECCION_CAJA_BACH     IN VARCHAR2,
    P_CONCEPTO_BACH_01      IN VARCHAR2,
    P_CONCEPTO_BACH_02      IN VARCHAR2,
    P_CONCEPTO_BACH_03      IN VARCHAR2,
    P_CONCEPTO_BACH_04      IN VARCHAR2,
    P_CONCEPTO_BACH_05      IN VARCHAR2,
    P_CONCEPTO_BACH_06      IN VARCHAR2,
    P_CONCEPTO_BACH_07      IN VARCHAR2,
    P_CONCEPTO_BACH_08      IN VARCHAR2,
    P_CONCEPTO_BACH_09      IN VARCHAR2,   
    P_PROGRAM               OUT SORLCUR.SORLCUR_PROGRAM%TYPE,     
    P_CONCEPTOS_VALIDOS     OUT VARCHAR2,
    P_MESSAGE               OUT VARCHAR2
)
AS
    
    V_NCONCEPTOS              NUMBER        := 0;
    V_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%TYPE;
    V_SECCION_CAJA_BACH       VARCHAR2(5);
    
    -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
    CURSOR C_SORLCUR_FOS IS
        SELECT SORLCUR_PROGRAM
        FROM (
            SELECT SORLCUR_PROGRAM
            FROM SORLCUR
            INNER JOIN SORLFOS ON
                SORLCUR_PIDM  = SORLFOS_PIDM 
                AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
            WHERE SORLCUR_PIDM = P_PIDM
                AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE = 'ACTIVE' 
                AND SORLCUR_CURRENT_CDE =   'Y'
                AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
        ) WHERE ROWNUM <= 1;
    
    -- APEC PARAMS
    V_APEC_DEPT             VARCHAR2(9);
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(10);
    V_APEC_IDSECCIONC       VARCHAR2(15);
    
BEGIN

    -- #######################################################################
    -- >> GET DATOS --
    OPEN C_SORLCUR_FOS;
    LOOP
        FETCH C_SORLCUR_FOS INTO  V_ALUM_PROGRAMA;
        EXIT WHEN C_SORLCUR_FOS%NOTFOUND;
    END LOOP;
    CLOSE C_SORLCUR_FOS;
    P_PROGRAM := V_ALUM_PROGRAMA;
            
    /*************************************************************************
                                Ejemplo: BAR - 103
    --------------------------------------------------------------------------
      Rectificación          Modalidad                        Carrera
    BA – Bachiller          R – Regular          -          xxx id Carrera
                            W – Gente Trabaja
                            V - Virtual  
    *************************************************************************/
    V_SECCION_CAJA_BACH := P_SECCION_CAJA_BACH || CASE P_DEPT_CODE WHEN 'UREG' THEN 'R' WHEN 'UVIR' THEN 'V' WHEN 'UPGT' THEN 'W' ELSE '-' END;
    
    -- IDSeccionC
    V_APEC_IDSECCIONC := V_SECCION_CAJA_BACH || ' - ' || P_PROGRAM;
        
    -- Get SEDE, sede HYO para ureg/upgt y VIR para UVIR
    V_APEC_CAMP := CASE P_DEPT_CODE WHEN 'UREG' THEN 'HYO' WHEN 'UVIR' THEN 'VIR' WHEN 'UPGT' THEN 'HYO' ELSE '-' END;
    
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
    
    SELECT  COUNT(t1."IDConcepto")
        INTO V_NCONCEPTOS
    FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE  t1 
    INNER JOIN dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE t2 ON 
        t1."IDSede"          = t2."IDsede"
        AND t1."IDPerAcad"      = t2."IDPerAcad" 
        AND t1."IDDependencia"  = t2."IDDependencia" 
        AND t1."IDSeccionC"     = t2."IDSeccionC" 
        AND t1."FecInic"        = t2."FecInic"
    WHERE t2."IDDependencia" = 'UCCI'
        AND t2."IDSeccionC"    = V_APEC_IDSECCIONC 
        AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_PSI' -- internado
        AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_ENF' -- internado
        AND t2."IDSeccionC"     <> '15NEX1A'
        AND t2."IDPerAcad"     = V_APEC_TERM
        AND T1."IDPerAcad"     = V_APEC_TERM
        AND t1."IDSede"        = V_APEC_CAMP
        AND t1."IDConcepto"    IN (P_CONCEPTO_BACH_01,P_CONCEPTO_BACH_02,P_CONCEPTO_BACH_03,P_CONCEPTO_BACH_04,P_CONCEPTO_BACH_05,P_CONCEPTO_BACH_06,P_CONCEPTO_BACH_07,P_CONCEPTO_BACH_08,P_CONCEPTO_BACH_09);    
    
    IF V_NCONCEPTOS = 9 THEN
        P_CONCEPTOS_VALIDOS := 'TRUE';
    ELSE
        P_CONCEPTOS_VALIDOS := 'FALSE';
        P_MESSAGE := 'El numero de conceptos no concuerda con los establecido.';
    END IF;
      
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICA_CONCEPTOS_BACHILLER;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICA_CONCEPTOS_TITULO ( 
        P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
        P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
        P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
        P_SECCION_CAJA_TITU     IN VARCHAR2,
        P_CONCEPTO_TITU_01      IN VARCHAR2,
        P_CONCEPTO_TITU_02      IN VARCHAR2,
        P_PROGRAM               OUT SORLCUR.SORLCUR_PROGRAM%TYPE,
        P_CONCEPTOS_VALIDOS     OUT VARCHAR2,
        P_MESSAGE               OUT VARCHAR2
)
AS
    V_NCONCEPTOS              NUMBER        := 0;
    V_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%TYPE;
    V_SECCION_CAJA_TITU       VARCHAR2(15);
    
    -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
    CURSOR C_SORLCUR_FOS IS
        SELECT SORLCUR_PROGRAM
        FROM (
                SELECT SORLCUR_PROGRAM
                FROM SORLCUR
                INNER JOIN SORLFOS ON 
                    SORLCUR_PIDM = SORLFOS_PIDM
                    AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
                WHERE   SORLCUR_PIDM        =   P_PIDM
                    AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                    AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                    AND SORLCUR_CURRENT_CDE =   'Y'
                ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
        ) WHERE ROWNUM <= 1;
    
    -- APEC PARAMS
    V_APEC_DEPT             VARCHAR2(9);
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(10);
    V_APEC_IDSECCIONC       VARCHAR2(15);
    
BEGIN

    -- #######################################################################
    -- >> GET DATOS --
    OPEN C_SORLCUR_FOS;
    LOOP
        FETCH C_SORLCUR_FOS INTO V_ALUM_PROGRAMA;
        EXIT WHEN C_SORLCUR_FOS%NOTFOUND;
    END LOOP;
    CLOSE C_SORLCUR_FOS;
    
    P_PROGRAM := V_ALUM_PROGRAMA;
    
    /*************************************************************************
                          Ejemplo: TIV - 103
    --------------------------------------------------------------------------
      Rectificación          Modalidad                        Carrera
    TI – Títutlo              R – Regular          -          xxx id Carrera         
                            W – Gente Trabaja
                            V - Virtual
    *************************************************************************/
    V_SECCION_CAJA_TITU := P_SECCION_CAJA_TITU || CASE P_DEPT_CODE WHEN 'UREG' THEN 'R' WHEN 'UVIR' THEN 'V' WHEN 'UPGT' THEN 'W' ELSE '-' END;
    -- IDSeccionC
    V_APEC_IDSECCIONC := V_SECCION_CAJA_TITU || ' - ' || P_PROGRAM;
    
    -- Get SEDE, sede HYO para ureg/upgt y VIR para UVIR
    V_APEC_CAMP := CASE P_DEPT_CODE WHEN 'UREG' THEN 'HYO' WHEN 'UVIR' THEN 'VIR' WHEN 'UPGT' THEN 'HYO' ELSE '-' END;
    
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;--
    
    SELECT COUNT(t1."IDConcepto")
        INTO V_NCONCEPTOS
    FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE  t1 
    INNER JOIN dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE t2 ON 
        t1."IDSede"          = t2."IDsede"
        AND t1."IDPerAcad"      = t2."IDPerAcad" 
        AND t1."IDDependencia"  = t2."IDDependencia" 
        AND t1."IDSeccionC"     = t2."IDSeccionC" 
        AND t1."FecInic"        = t2."FecInic"
    WHERE t2."IDDependencia" = 'UCCI'
        AND t2."IDSeccionC"    = V_APEC_IDSECCIONC 
        AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_PSI' -- internado
        AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_ENF' -- internado
        AND t2."IDSeccionC"     <> '15NEX1A'
        AND t2."IDPerAcad"     = V_APEC_TERM
        AND T1."IDPerAcad"     = V_APEC_TERM
        AND t1."IDSede"        = V_APEC_CAMP
        AND t1."IDConcepto"    IN (P_CONCEPTO_TITU_01,P_CONCEPTO_TITU_02);
    
    IF V_NCONCEPTOS = 2 THEN
        P_CONCEPTOS_VALIDOS := 'TRUE';
    ELSE
        P_CONCEPTOS_VALIDOS := 'FALSE';
        P_MESSAGE := 'El numero de conceptos no concuerda con los establecido.';
    END IF;
      
EXCEPTION
WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICA_CONCEPTOS_TITULO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_CONCEPTO_BACHILLER ( 
    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_FOLIO_SOLICITUD       IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,       
    P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
    P_PROGRAM               IN SORLCUR.SORLCUR_PROGRAM%TYPE,     
    P_SECCION_CAJA_BACH     IN VARCHAR2,
    P_CONCEPTO_BACH_01      IN VARCHAR2,
    P_CONCEPTO_BACH_02      IN VARCHAR2,
    P_CONCEPTO_BACH_03      IN VARCHAR2,
    P_CONCEPTO_BACH_04      IN VARCHAR2,
    P_CONCEPTO_BACH_06      IN VARCHAR2,
    P_CONCEPTO_BACH_07      IN VARCHAR2,           
    P_CONCEPTO_IDIOMA       IN VARCHAR2,
    P_MESSAGE               OUT VARCHAR2
)
AS
    V_SECCION_CAJA_BACH       VARCHAR2(5);
    V_RECEPTION_DATE          SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE;
    V_INDICADOR               NUMBER;
    V_EXCEP_NOTFOUND_CARGO    EXCEPTION;
    
    -- APEC PARAMS
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(10);
    V_APEC_IDSECCIONC       VARCHAR2(50);
    V_APEC_IDESCUELA        VARCHAR2(50);
    V_APEC_FECINIC          DATE;
    
BEGIN
    
    -- GET FECHA DE SOLICITUD
    SELECT SVRSVPR_RECEPTION_DATE
        INTO V_RECEPTION_DATE
    FROM SVRSVPR
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
    
    /*************************************************************************
                                Ejemplo: BAR - 103
    --------------------------------------------------------------------------
      Rectificación          Modalidad                        Carrera
    BA – Bachiller          R – Regular          -          xxx id Carrera
                            W – Gente Trabaja
                            V - Virtual  
    *************************************************************************/
    V_SECCION_CAJA_BACH := P_SECCION_CAJA_BACH || CASE P_DEPT_CODE WHEN 'UREG' THEN 'R' WHEN 'UVIR' THEN 'V' WHEN 'UPGT' THEN 'W' ELSE '-' END;
    -- IDSeccionC
    V_APEC_IDSECCIONC   := V_SECCION_CAJA_BACH || ' - ' || P_PROGRAM;
    
    -- Get SEDE, sede HYO para ureg/upgt y VIR para UVIR
    V_APEC_CAMP := CASE P_DEPT_CODE WHEN 'UREG' THEN 'HYO' WHEN 'UVIR' THEN 'VIR' WHEN 'UPGT' THEN 'HYO' ELSE '-' END;
    
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
    
    SELECT  t1."FecInic",         t2."IDEscuela"
        INTO    V_APEC_FECINIC,       V_APEC_IDESCUELA
    FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE t1
    INNER JOIN dbo.tblEscuela@BDUCCI.CONTINENTAL.EDU.PE t2 ON 
        t1."IDDependencia" = t2."IDDependencia"
        AND t1."IDEscuela" = t2."IDEscuela" 
    WHERE t1."IDDependencia" = 'UCCI'
        AND t1."IDsede"     = V_APEC_CAMP
        AND t1."IDPerAcad"  = V_APEC_TERM
        AND t1."IDSeccionC" = V_APEC_IDSECCIONC
        AND t2."IDEscuela"  = 'GYT'
        AND t2."IDTipoEsc"  = 'EXT';
    
    --**********************************************************************++***********************
    -- INSERT ALUMNO ESTADO en caso no exista el registro.
    SELECT COUNT("IDAlumno")
        INTO V_INDICADOR
    FROM dbo.tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "IDSede"      = V_APEC_CAMP 
    AND "IDAlumno"      = P_ID_ALUMNO 
    AND "IDPerAcad"     = V_APEC_TERM
    AND "IDDependencia" = 'UCCI' 
    AND "IDSeccionC"    = V_APEC_IDSECCIONC 
    AND "FecInic"       = V_APEC_FECINIC
    AND "IDEscuela"     = V_APEC_IDESCUELA;
    
    ----- INSERT
    IF (V_INDICADOR = 0) THEN
        INSERT INTO dbo.tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE 
                ("IDSede",                      "IDAlumno", 
                "IDPerAcad",                    "IDDependencia", 
                "IDSeccionC",                   "FecInic", 
                "IDPersonal",                   "IDEscuela", 
                "IDEscala",                     "IDInstitucion", 
                "FecMatricula",                 "Beca",  
                "Estado",                       "FechaEstado", 
                "MatriculaTipo",                "Carnet", 
                "PlanPago" )
        VALUES(
                V_APEC_CAMP,                    P_ID_ALUMNO, 
                V_APEC_TERM,                    'UCCI', 
                V_APEC_IDSECCIONC,              V_APEC_FECINIC, 
                'Banner', /*idpersonal*/        V_APEC_IDESCUELA,
                'X',/*idescala*/                '00000000000' , /*institucion*/ 
                V_APEC_FECINIC,                 0, /*beca*/ 
                'M', /*estado*/                 V_APEC_FECINIC, /*fechaestado*/
                'C', /*matriculatipo*/          0, /*carnet*/ 
                5 /*planpago*/ );
    END IF;
    
    
    -- *********************************************************************************************
    -- ASINGANDO el cargo a CTACORRIENTE
    SELECT COUNT("IDAlumno")
        INTO V_INDICADOR
    FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
    WHERE "IDDependencia" = 'UCCI' 
        AND "IDSede"      = V_APEC_CAMP 
        AND "IDAlumno"    = P_ID_ALUMNO
        AND "IDPerAcad"   = V_APEC_TERM 
        AND "IDSeccionC"  = V_APEC_IDSECCIONC
        AND "FecInic"     = V_APEC_FECINIC
        AND "IDConcepto"  IN (P_CONCEPTO_BACH_01,P_CONCEPTO_BACH_02,P_CONCEPTO_BACH_03,P_CONCEPTO_BACH_04,P_CONCEPTO_BACH_06,P_CONCEPTO_BACH_07,P_CONCEPTO_IDIOMA);
    
    IF (V_INDICADOR = 0) THEN
        INSERT INTO dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
              ("IDSede",            "IDAlumno",               "IDPerAcad", 
              "IDDependencia",      "IDSeccionC",             "FecInic", 
              "IDConcepto",         "FecCargo",               "Prorroga", 
              "Cargo",              "DescMora",               "IDEscuela", 
              "IDCuenta",           "IDInstitucion",          "Moneda", 
              "IDEscala",           "Beca",                   "Estado", 
              "Abono" )
        SELECT  t2."IDsede",          P_ID_ALUMNO,              V_APEC_TERM,
              'UCCI',               V_APEC_IDSECCIONC,        V_APEC_FECINIC,
              t1."IDConcepto",      V_RECEPTION_DATE,         '0',
              "Monto",              0,                        t1."IDEscuela",
              "IDCuenta",           '00000000000',            t1."Moneda",
              '',                   0,                        'M',
              0
        FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE t1
        INNER JOIN dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE t2 ON 
            t1."IDSede"          = t2."IDsede"
            AND t1."IDPerAcad"      = t2."IDPerAcad" 
            AND t1."IDDependencia"  = t2."IDDependencia" 
            AND t1."IDSeccionC"     = t2."IDSeccionC" 
            AND t1."FecInic"        = t2."FecInic"
        WHERE t2."IDDependencia"  = 'UCCI'
            AND t2."IDsede"         = V_APEC_CAMP
            AND t2."IDSeccionC"     = V_APEC_IDSECCIONC
            AND t2."IDSeccionC"     <> '15NEX1A'
            AND t2."IDPerAcad"      = V_APEC_TERM
            AND T1."IDPerAcad"      = V_APEC_TERM
            AND t2."FecInic"        = V_APEC_FECINIC
            AND t1."IDConcepto"     IN (P_CONCEPTO_BACH_01,P_CONCEPTO_BACH_02,P_CONCEPTO_BACH_03,P_CONCEPTO_BACH_04,P_CONCEPTO_BACH_06,P_CONCEPTO_BACH_07,P_CONCEPTO_IDIOMA);  
        ELSIF(V_INDICADOR > 1) THEN
            RAISE V_EXCEP_NOTFOUND_CARGO;                  
    END IF;
    
    COMMIT;
EXCEPTION
WHEN V_EXCEP_NOTFOUND_CARGO THEN
    P_MESSAGE := '- Se encontro cargos ya generados anteriormente.';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
WHEN OTHERS THEN
    ROLLBACK;
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CONCEPTO_BACHILLER;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_CONCEPTO_TITULO ( 
    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_FOLIO_SOLICITUD       IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,       
    P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
    P_PROGRAM               IN SORLCUR.SORLCUR_PROGRAM%TYPE,     
    P_SECCION_CAJA_TITU     IN VARCHAR2,
    P_CONCEPTO_TITU_01      IN VARCHAR2,
    P_CONCEPTO_TITU_02      IN VARCHAR2,
    P_MESSAGE               OUT VARCHAR2
)
AS
    V_SECCION_CAJA_TITU       VARCHAR2(5);
    V_RECEPTION_DATE          SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE;
    V_INDICADOR               NUMBER;
    V_EXCEP_NOTFOUND_CARGO    EXCEPTION;
    
    -- APEC PARAMS
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(10);
    V_APEC_IDSECCIONC       VARCHAR2(50);
    V_APEC_IDESCUELA        VARCHAR2(50);
    V_APEC_FECINIC          DATE;
BEGIN
      
    -- GET FECHA DE SOLICITUD
    SELECT SVRSVPR_RECEPTION_DATE
        INTO V_RECEPTION_DATE
    FROM SVRSVPR
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
    
    /*************************************************************************
                                Ejemplo: BAR - 103
    --------------------------------------------------------------------------
      Rectificación          Modalidad                        Carrera
    BA – Bachiller          R – Regular          -          xxx id Carrera
                            W – Gente Trabaja
                            V - Virtual  
    *************************************************************************/
    V_SECCION_CAJA_TITU := P_SECCION_CAJA_TITU || CASE P_DEPT_CODE WHEN 'UREG' THEN 'R' WHEN 'UVIR' THEN 'V' WHEN 'UPGT' THEN 'W' ELSE '-' END;
    
    -- IDSeccionC
    V_APEC_IDSECCIONC   := V_SECCION_CAJA_TITU || ' - ' || P_PROGRAM;
    
    -- Get SEDE, sede HYO para ureg/upgt y VIR para UVIR
    V_APEC_CAMP := CASE P_DEPT_CODE WHEN 'UREG' THEN 'HYO' WHEN 'UVIR' THEN 'VIR' WHEN 'UPGT' THEN 'HYO' ELSE '-' END;
    
    SELECT CZRTERM_TERM_BDUCCI
        INTO V_APEC_TERM
    FROM CZRTERM
    WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
    
    SELECT  t1."FecInic",         t2."IDEscuela"
        INTO V_APEC_FECINIC,       V_APEC_IDESCUELA
    FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE t1
    INNER JOIN dbo.tblEscuela@BDUCCI.CONTINENTAL.EDU.PE t2 ON
        t1."IDDependencia" = t2."IDDependencia"
        AND t1."IDEscuela" = t2."IDEscuela" 
    WHERE t1."IDDependencia" = 'UCCI'
        AND t1."IDsede"     = V_APEC_CAMP
        AND t1."IDPerAcad"  = V_APEC_TERM
        AND t1."IDSeccionC" = V_APEC_IDSECCIONC
        AND t2."IDEscuela"  = 'GYT'
        AND t2."IDTipoEsc"  = 'EXT';
    
    --**********************************************************************++***********************
    -- INSERT ALUMNO ESTADO en caso no exista el registro.
    SELECT COUNT("IDAlumno")
        INTO V_INDICADOR
    FROM dbo.tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE
    WHERE "IDSede"      = V_APEC_CAMP 
        AND "IDAlumno"      = P_ID_ALUMNO 
        AND "IDPerAcad"     = V_APEC_TERM
        AND "IDDependencia" = 'UCCI' 
        AND "IDSeccionC"    = V_APEC_IDSECCIONC 
        AND "FecInic"       = V_APEC_FECINIC
        AND "IDEscuela"     = V_APEC_IDESCUELA;
        
    ----- INSERT
    IF (V_INDICADOR = 0) THEN
        INSERT INTO dbo.tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE 
                ("IDSede",                      "IDAlumno", 
                "IDPerAcad",                    "IDDependencia", 
                "IDSeccionC",                   "FecInic", 
                "IDPersonal",                   "IDEscuela", 
                "IDEscala",                     "IDInstitucion", 
                "FecMatricula",                 "Beca",  
                "Estado",                       "FechaEstado", 
                "MatriculaTipo",                "Carnet", 
                "PlanPago" )
        VALUES(
                V_APEC_CAMP,                    P_ID_ALUMNO, 
                V_APEC_TERM,                    'UCCI', 
                V_APEC_IDSECCIONC,              V_APEC_FECINIC, 
                'Banner', /*idpersonal*/        V_APEC_IDESCUELA,
                'X',/*idescala*/                '00000000000' , /*institucion*/ 
                V_APEC_FECINIC,                 0, /*beca*/ 
                'M', /*estado*/                 V_APEC_FECINIC, /*fechaestado*/
                'C', /*matriculatipo*/          0, /*carnet*/ 
                5 /*planpago*/ );
    END IF;
    
    -- *********************************************************************************************
    -- ASINGANDO el cargo a CTACORRIENTE
    SELECT COUNT("IDAlumno")    
        INTO V_INDICADOR
    FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
    WHERE "IDDependencia" = 'UCCI' 
        AND "IDSede"      = V_APEC_CAMP 
        AND "IDAlumno"    = P_ID_ALUMNO
        AND "IDPerAcad"   = V_APEC_TERM 
        AND "IDSeccionC"  = V_APEC_IDSECCIONC
        AND "FecInic"     = V_APEC_FECINIC
        AND "IDConcepto"  IN (P_CONCEPTO_TITU_01,P_CONCEPTO_TITU_02);
        
    IF (V_INDICADOR = 0) THEN
        INSERT INTO dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
              ("IDSede",            "IDAlumno",               "IDPerAcad", 
              "IDDependencia",      "IDSeccionC",             "FecInic", 
              "IDConcepto",         "FecCargo",               "Prorroga", 
              "Cargo",              "DescMora",               "IDEscuela", 
              "IDCuenta",           "IDInstitucion",          "Moneda", 
              "IDEscala",           "Beca",                   "Estado", 
              "Abono" )
        SELECT  t2."IDsede",          P_ID_ALUMNO,              V_APEC_TERM,
              'UCCI',               V_APEC_IDSECCIONC,        V_APEC_FECINIC,
              t1."IDConcepto",      V_RECEPTION_DATE,         '0',
              "Monto",              0,                        t1."IDEscuela",
              "IDCuenta",           '00000000000',            t1."Moneda",
              '',                   0,                        'M',
              0
        FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE  t1 
        INNER JOIN dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE t2 ON 
            t1."IDSede"          = t2."IDsede"
            AND t1."IDPerAcad"      = t2."IDPerAcad" 
            AND t1."IDDependencia"  = t2."IDDependencia" 
            AND t1."IDSeccionC"     = t2."IDSeccionC" 
            AND t1."FecInic"        = t2."FecInic"
        WHERE t2."IDDependencia"  = 'UCCI'
            AND t2."IDsede"         = V_APEC_CAMP
            AND t2."IDSeccionC"     = V_APEC_IDSECCIONC
            AND t2."IDSeccionC"     <> '15NEX1A'
            AND t2."IDPerAcad"      = V_APEC_TERM
            AND T1."IDPerAcad"      = V_APEC_TERM
            AND t2."FecInic"        = V_APEC_FECINIC
            AND t1."IDConcepto"     IN (P_CONCEPTO_TITU_01,P_CONCEPTO_TITU_02);
    ELSIF(V_INDICADOR > 1) THEN
        RAISE V_EXCEP_NOTFOUND_CARGO;                  
    END IF;
    
    COMMIT;
EXCEPTION
WHEN V_EXCEP_NOTFOUND_CARGO THEN
    P_MESSAGE := '- Se encontro cargos ya generados anteriormente.';
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
WHEN OTHERS THEN
    ROLLBACK;
    RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CONCEPTO_TITULO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END BWZKSPBT;