create or replace PACKAGE BODY BWZKCSPBT AS
/*
 BWZKCSPBT:
       Paquete Web _ Desarrollo Propio _ Paquete _ Conti Solicitud Pago Pronto
*/
-- FILE NAME..: BWZKCSPBT.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKCSPBT
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2017
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              


/* ===================================================================================================================
  NOMBRE    : P_GET_STUDENT_INPUTS
  FECHA     : 28/09/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene los datos de AUTOSERVICIO  ingresados en por el alumno: Tramite BACHILLER/TITULO , concepto del Idioma
  
  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
PROCEDURE P_GET_STUDENT_INPUTS (
      P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_DIPLOMA_CODE        OUT VARCHAR2,
      P_DIPLOMA_DESC        OUT VARCHAR2,
      P_CONCEPTO_IDIOMA     OUT VARCHAR2,
      P_IDIOMA_DESC         OUT VARCHAR2
)
AS
      V_ERROR                   EXCEPTION;
      V_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
      V_ADDL_DATA_CDE           SVRSVAD.SVRSVAD_ADDL_DATA_CDE%TYPE;
      V_ADDL_DATA_SEQ           SVRSVAD.SVRSVAD_ADDL_DATA_SEQ%TYPE;
      V_ADDL_DATA_DESC          SVRSVAD.SVRSVAD_ADDL_DATA_DESC%TYPE;
      
      -- GET COMENTARIO Y TELEFONO de la solicitud. (comentario PERSONALZIADO.)
      CURSOR C_SVRSVAD IS
        SELECT SVRSVAD_ADDL_DATA_SEQ, SVRSVAD_ADDL_DATA_CDE, SVRSVAD_ADDL_DATA_DESC
        FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
        INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
        ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
        WHERE SVRSRAD_SRVC_CODE = 'SOL018'
        --AND SVRSVAD_ADDL_DATA_CDE = 'PREGUNTA'
        AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
        ORDER BY SVRSVAD_ADDL_DATA_SEQ;
BEGIN
      
      -- GET CODIGO SOLICITUD
      SELECT SVRSVPR_SRVC_CODE 
      INTO V_CODIGO_SOLICITUD 
      FROM SVRSVPR 
      WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
      
      -- OBTENER COMENTARIO Y EL TELEFONO DE LA SOLICITUD (LOS DOS ULTIMOS DATOS)
      OPEN C_SVRSVAD;
        LOOP
          FETCH C_SVRSVAD INTO V_ADDL_DATA_SEQ, V_ADDL_DATA_CDE, V_ADDL_DATA_DESC;
          IF C_SVRSVAD%FOUND THEN
              IF V_ADDL_DATA_SEQ = 1 THEN
                P_DIPLOMA_CODE  := V_ADDL_DATA_CDE;
                P_DIPLOMA_DESC  := V_ADDL_DATA_DESC;
              ELSIF V_ADDL_DATA_SEQ = 2 THEN
                P_CONCEPTO_IDIOMA   := V_ADDL_DATA_CDE;
                P_IDIOMA_DESC       := V_ADDL_DATA_DESC;
              END IF;
          ELSE EXIT;
        END IF;
        END LOOP;
      CLOSE C_SVRSVAD;

      IF P_DIPLOMA_CODE IS NULL THEN
          RAISE V_ERROR;
      END IF;
      
EXCEPTION
  WHEN V_ERROR THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| ' - No se encontró el identificador del DIPLOMA.' );
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_STUDENT_INPUTS;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


/* ===================================================================================================================
  NOMBRE    : p_cambio_estado_solicitud
  FECHA     : 12/12/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : cambia el estado de la solicitud (XXXXXX)

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  =================================================================================================================== */
PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
  P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
  P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
  P_AN                  OUT NUMBER,
  P_ERROR               OUT VARCHAR2
)
AS
            P_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
            P_ESTADO_PREVIOUS         SVVSRVS.SVVSRVS_CODE%TYPE;
            P_FECHA_SOL               SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE;
            P_INDICADOR               NUMBER;
            E_INVALID_ESTADO          EXCEPTION;
            V_PRIORIDAD               SVRSVPR.SVRSVPR_RSRV_SEQ_NO%TYPE;
BEGIN
--
  ----------------------------------------------------------------------------
        
        -- GET tipo de solicitud por ejemplo "SOL003" 
        SELECT  SVRSVPR_SRVC_CODE, 
                SVRSVPR_SRVS_CODE,
                SVRSVPR_RECEPTION_DATE,
                SVRSVPR_RSRV_SEQ_NO
        INTO    P_CODIGO_SOLICITUD,
                P_ESTADO_PREVIOUS,
                P_FECHA_SOL,
                V_PRIORIDAD
        FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
        
        -- SI EL ESTADO ES ANULADO (AN)
        P_AN := 0;
        IF P_ESTADO_PREVIOUS = 'AN' THEN
              P_AN := 1;
        END IF;
        
        -- VALIDAR que :
            -- El ESTADO actual sea modificable segun la configuracion.
            -- El ETSADO enviado pertenesca entre los asignados al tipo de SOLICTUD.
        SELECT COUNT(*) INTO P_INDICADOR
        FROM SVRSVPR 
        WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
        AND SVRSVPR_SRVS_CODE IN (
              -- Estados de solic. MODIFICABLES segun las reglas y configuracion
              SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
              FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
              INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
              ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
              AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
              INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
              ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
              WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
              AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
              AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 
              AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
              -- AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
        )
        AND P_ESTADO IN (
              -- Estados de solic. configurados
              SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
              FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
              INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
              ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
              AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
              INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
              ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
              WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
              AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
              AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
              -- AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
        );
        
        
        IF P_INDICADOR = 0 THEN
              RAISE E_INVALID_ESTADO;
        ELSIF (P_ESTADO_PREVIOUS <> P_ESTADO) THEN
              -- UPDATE SOLICITUD
              UPDATE SVRSVPR
                SET SVRSVPR_SRVS_CODE = P_ESTADO,  --AN , AP , RE entre otros
                SVRSVPR_ACTIVITY_DATE = SYSDATE,
                SVRSVPR_DATA_ORIGIN = 'WorkFlow'
              WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
              AND SVRSVPR_SRVS_CODE IN (
                        -- Estados de solic. MODIFICABLES segun las reglas y configuracion
                        SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
                        FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
                        INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
                        ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
                        AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
                        INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
                        ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
                        WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
                        AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
                        AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 
                        AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
                        -- AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
                )
              AND P_ESTADO IN (
                      -- Estados de solic. configurados
                      SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
                      FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
                      INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
                      ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
                      AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
                      INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
                      ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
                      WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
                      AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
                      AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
                      -- AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
              );
              
              COMMIT;
        END IF;
        
EXCEPTION
  WHEN E_INVALID_ESTADO THEN
          P_ERROR  := '- El "ESTADO" actual no es modificable ò no se encuentra entre los ESTADOS disponibles para la solicitud.';
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_ERROR );
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CAMBIO_ESTADO_SOLICITUD;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


/* ===================================================================================================================
  NOMBRE    : P_VERIFICA_CONCEPTOS_BACHILLER
  FECHA     : 28/09/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida si se crearon los conceptos para bachiller en el año(periodo) actual

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
PROCEDURE P_VERIFICA_CONCEPTOS_BACHILLER ( 
        P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
        P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
        P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
        P_SECCION_CAJA_BACH     IN VARCHAR2,
        P_CONCEPTO_BACH_01      IN VARCHAR2,
        P_CONCEPTO_BACH_02      IN VARCHAR2,
        P_CONCEPTO_BACH_03      IN VARCHAR2,
        P_CONCEPTO_BACH_04      IN VARCHAR2,
        P_CONCEPTO_BACH_05      IN VARCHAR2,
        P_CONCEPTO_BACH_06      IN VARCHAR2,
        P_CONCEPTO_BACH_07      IN VARCHAR2,
        P_CONCEPTO_BACH_08      IN VARCHAR2,
        P_CONCEPTO_BACH_09      IN VARCHAR2,   
        P_PROGRAM               OUT SORLCUR.SORLCUR_PROGRAM%TYPE,     
        P_CONCEPTOS_VALIDOS     OUT VARCHAR2,
        P_MESSAGE               OUT VARCHAR2
)
AS
    
    V_NCONCEPTOS              NUMBER        := 0;
    V_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%TYPE;
    V_SECCION_CAJA_BACH       VARCHAR2(5);
    
    -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
    CURSOR C_SORLCUR_FOS IS
    SELECT    SORLCUR_PROGRAM
    FROM (
            SELECT    SORLCUR_PROGRAM
            FROM SORLCUR        INNER JOIN SORLFOS
                  ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                  AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
            WHERE   SORLCUR_PIDM        =   P_PIDM
                AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                AND SORLCUR_CURRENT_CDE =   'Y'
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
    ) WHERE ROWNUM <= 1;
    
    -- APEC PARAMS
    V_APEC_DEPT             VARCHAR2(9);
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(10);
    V_APEC_IDSECCIONC       VARCHAR2(15);
    
BEGIN

      -- #######################################################################
      -- >> GET DATOS --
      OPEN C_SORLCUR_FOS;
      LOOP
        FETCH C_SORLCUR_FOS INTO  V_ALUM_PROGRAMA;
        EXIT WHEN C_SORLCUR_FOS%NOTFOUND;
      END LOOP;
      CLOSE C_SORLCUR_FOS;
      P_PROGRAM := V_ALUM_PROGRAMA;
            
      /*************************************************************************
                                    Ejemplo: BAR - 103
      --------------------------------------------------------------------------
          Rectificación          Modalidad                        Carrera
      BA – Bachiller            R – Regular          -          xxx id Carrera
                                W – Gente Trabaja
                                V - Virtual  
      *************************************************************************/
      V_SECCION_CAJA_BACH := P_SECCION_CAJA_BACH || CASE P_DEPT_CODE WHEN 'UREG' THEN 'R' WHEN 'UVIR' THEN 'V' WHEN 'UPGT' THEN 'W' ELSE '-' END;
      -- IDSeccionC
      V_APEC_IDSECCIONC := V_SECCION_CAJA_BACH || ' - ' || P_PROGRAM;
            
      -- Get SEDE, sede HYO para ureg/upgt y VIR para UVIR
      V_APEC_CAMP := CASE P_DEPT_CODE WHEN 'UREG' THEN 'HYO' WHEN 'UVIR' THEN 'VIR' WHEN 'UPGT' THEN 'HYO' ELSE '-' END;
      SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
      
      SELECT  COUNT(t1."IDConcepto") INTO V_NCONCEPTOS
      FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE  t1 
      INNER JOIN dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE  t2
        ON t1."IDSede"          = t2."IDsede"
        AND t1."IDPerAcad"      = t2."IDPerAcad" 
        AND t1."IDDependencia"  = t2."IDDependencia" 
        AND t1."IDSeccionC"     = t2."IDSeccionC" 
        AND t1."FecInic"        = t2."FecInic"
      WHERE t2."IDDependencia" = 'UCCI'
        AND t2."IDSeccionC"    = V_APEC_IDSECCIONC 
        AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_PSI' -- internado
        AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_ENF' -- internado
        AND t2."IDSeccionC"     <> '15NEX1A'
        AND t2."IDPerAcad"     = V_APEC_TERM
        AND T1."IDPerAcad"     = V_APEC_TERM
        AND t1."IDSede"        = V_APEC_CAMP
        AND t1."IDConcepto"    IN (P_CONCEPTO_BACH_01,P_CONCEPTO_BACH_02,P_CONCEPTO_BACH_03,P_CONCEPTO_BACH_04,P_CONCEPTO_BACH_05,P_CONCEPTO_BACH_06,P_CONCEPTO_BACH_07,P_CONCEPTO_BACH_08,P_CONCEPTO_BACH_09);    
      
      
      IF V_NCONCEPTOS = 9 THEN
        P_CONCEPTOS_VALIDOS := 'TRUE';
      ELSE
        P_CONCEPTOS_VALIDOS := 'FALSE';
        P_MESSAGE := 'El numero de conceptos no concuerda con los establecido.';
      END IF;
      
EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICA_CONCEPTOS_BACHILLER;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


/* ===================================================================================================================
  NOMBRE    : P_VERIFICA_CONCEPTOS_TITULO
  FECHA     : 28/09/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida si se crearon los conceptos para Titulación en el año(periodo) actual

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
PROCEDURE P_VERIFICA_CONCEPTOS_TITULO ( 
        P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
        P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
        P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
        P_SECCION_CAJA_TITU     IN VARCHAR2,
        P_CONCEPTO_TITU_01      IN VARCHAR2,
        P_CONCEPTO_TITU_02      IN VARCHAR2,
        P_PROGRAM               OUT SORLCUR.SORLCUR_PROGRAM%TYPE,
        P_CONCEPTOS_VALIDOS     OUT VARCHAR2,
        P_MESSAGE               OUT VARCHAR2
)
AS
    
    V_NCONCEPTOS              NUMBER        := 0;
    V_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%TYPE;
    V_SECCION_CAJA_TITU       VARCHAR2(15);
    
    -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
    CURSOR C_SORLCUR_FOS IS
    SELECT    SORLCUR_PROGRAM
    FROM (
            SELECT    SORLCUR_PROGRAM
            FROM SORLCUR        INNER JOIN SORLFOS
                  ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                  AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
            WHERE   SORLCUR_PIDM        =   P_PIDM
                AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                AND SORLCUR_CURRENT_CDE =   'Y'
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
    ) WHERE ROWNUM <= 1;
    
    -- APEC PARAMS
    V_APEC_DEPT             VARCHAR2(9);
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(10);
    V_APEC_IDSECCIONC       VARCHAR2(15);
    
BEGIN

      -- #######################################################################
      -- >> GET DATOS --
      OPEN C_SORLCUR_FOS;
      LOOP
        FETCH C_SORLCUR_FOS INTO  V_ALUM_PROGRAMA;
        EXIT WHEN C_SORLCUR_FOS%NOTFOUND;
      END LOOP;
      CLOSE C_SORLCUR_FOS;
      P_PROGRAM := V_ALUM_PROGRAMA;
      
      /*************************************************************************
                              Ejemplo: TIV - 103
      --------------------------------------------------------------------------
          Rectificación          Modalidad                        Carrera
      TI – Títutlo              R – Regular          -          xxx id Carrera         
                                W – Gente Trabaja
                                V - Virtual
      *************************************************************************/
      V_SECCION_CAJA_TITU := P_SECCION_CAJA_TITU || CASE P_DEPT_CODE WHEN 'UREG' THEN 'R' WHEN 'UVIR' THEN 'V' WHEN 'UPGT' THEN 'W' ELSE '-' END;
      -- IDSeccionC
      V_APEC_IDSECCIONC := V_SECCION_CAJA_TITU || ' - ' || P_PROGRAM;
      
      -- Get SEDE, sede HYO para ureg/upgt y VIR para UVIR
      V_APEC_CAMP := CASE P_DEPT_CODE WHEN 'UREG' THEN 'HYO' WHEN 'UVIR' THEN 'VIR' WHEN 'UPGT' THEN 'HYO' ELSE '-' END;
      SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;--
      
      SELECT  COUNT(t1."IDConcepto") INTO V_NCONCEPTOS
      FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE  t1 
      INNER JOIN dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE  t2
        ON t1."IDSede"          = t2."IDsede"
        AND t1."IDPerAcad"      = t2."IDPerAcad" 
        AND t1."IDDependencia"  = t2."IDDependencia" 
        AND t1."IDSeccionC"     = t2."IDSeccionC" 
        AND t1."FecInic"        = t2."FecInic"
      WHERE t2."IDDependencia" = 'UCCI'
        AND t2."IDSeccionC"    = V_APEC_IDSECCIONC 
        AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_PSI' -- internado
        AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_ENF' -- internado
        AND t2."IDSeccionC"     <> '15NEX1A'
        AND t2."IDPerAcad"     = V_APEC_TERM
        AND T1."IDPerAcad"     = V_APEC_TERM
        AND t1."IDSede"        = V_APEC_CAMP
        AND t1."IDConcepto"    IN (P_CONCEPTO_TITU_01,P_CONCEPTO_TITU_02);
      
      IF V_NCONCEPTOS = 2 THEN
        P_CONCEPTOS_VALIDOS := 'TRUE';
      ELSE
        P_CONCEPTOS_VALIDOS := 'FALSE';
        P_MESSAGE := 'El numero de conceptos no concuerda con los establecido.';
      END IF;
      
EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICA_CONCEPTOS_TITULO;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


/* ===================================================================================================================
  NOMBRE    : P_SET_CONCEPTO_BACHILLER
  FECHA     : 28/09/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Genera cargos de bachiller

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
PROCEDURE P_SET_CONCEPTO_BACHILLER ( 
        P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
        P_FOLIO_SOLICITUD       IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,       
        P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
        P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
        P_PROGRAM               IN SORLCUR.SORLCUR_PROGRAM%TYPE,     
        P_SECCION_CAJA_BACH     IN VARCHAR2,
        P_CONCEPTO_BACH_01      IN VARCHAR2,
        P_CONCEPTO_BACH_02      IN VARCHAR2,
        P_CONCEPTO_BACH_03      IN VARCHAR2,
        P_CONCEPTO_BACH_04      IN VARCHAR2,
        P_CONCEPTO_BACH_06      IN VARCHAR2,
        P_CONCEPTO_BACH_07      IN VARCHAR2,           
        P_CONCEPTO_IDIOMA       IN VARCHAR2,
        P_MESSAGE               OUT VARCHAR2
)
AS

    V_SECCION_CAJA_BACH       VARCHAR2(5);
    V_RECEPTION_DATE          SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE;
    V_INDICADOR               NUMBER;
    V_EXCEP_NOTFOUND_CARGO    EXCEPTION;
    
    -- APEC PARAMS
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(10);
    V_APEC_IDSECCIONC       VARCHAR2(50);
    V_APEC_IDESCUELA        VARCHAR2(50);
    V_APEC_FECINIC          DATE;
    
BEGIN
      
      -- GET FECHA DE SOLICITUD
      SELECT  SVRSVPR_RECEPTION_DATE
      INTO    V_RECEPTION_DATE
      FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
      
      /*************************************************************************
                                    Ejemplo: BAR - 103
      --------------------------------------------------------------------------
          Rectificación          Modalidad                        Carrera
      BA – Bachiller            R – Regular          -          xxx id Carrera
                                W – Gente Trabaja
                                V - Virtual  
      *************************************************************************/
      V_SECCION_CAJA_BACH := P_SECCION_CAJA_BACH || CASE P_DEPT_CODE WHEN 'UREG' THEN 'R' WHEN 'UVIR' THEN 'V' WHEN 'UPGT' THEN 'W' ELSE '-' END;
       -- IDSeccionC
      V_APEC_IDSECCIONC   := V_SECCION_CAJA_BACH || ' - ' || P_PROGRAM;
      
      -- Get SEDE, sede HYO para ureg/upgt y VIR para UVIR
      V_APEC_CAMP := CASE P_DEPT_CODE WHEN 'UREG' THEN 'HYO' WHEN 'UVIR' THEN 'VIR' WHEN 'UPGT' THEN 'HYO' ELSE '-' END;
      SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
      
      SELECT  t1."FecInic",         t2."IDEscuela"
      INTO    V_APEC_FECINIC,       V_APEC_IDESCUELA
      FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE t1
      INNER JOIN dbo.tblEscuela@BDUCCI.CONTINENTAL.EDU.PE t2
      ON t1."IDDependencia" = t2."IDDependencia"
      AND t1."IDEscuela" = t2."IDEscuela" 
      WHERE t1."IDDependencia" = 'UCCI'
      AND t1."IDsede"     = V_APEC_CAMP
      AND t1."IDPerAcad"  = V_APEC_TERM
      AND t1."IDSeccionC" = V_APEC_IDSECCIONC
      AND t2."IDEscuela"  = 'GYT'
      AND t2."IDTipoEsc"  = 'EXT';
        
      --**********************************************************************++***********************
      -- INSERT ALUMNO ESTADO en caso no exista el registro.
      SELECT COUNT("IDAlumno") INTO V_INDICADOR FROM dbo.tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE
      WHERE "IDSede"      = V_APEC_CAMP 
      AND "IDAlumno"      = P_ID_ALUMNO 
      AND "IDPerAcad"     = V_APEC_TERM
      AND "IDDependencia" = 'UCCI' 
      AND "IDSeccionC"    = V_APEC_IDSECCIONC 
      AND "FecInic"       = V_APEC_FECINIC
      AND "IDEscuela"     = V_APEC_IDESCUELA;
      ----- INSERT
      IF (V_INDICADOR = 0) THEN

            INSERT INTO dbo.tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE 
                    ("IDSede",                      "IDAlumno", 
                    "IDPerAcad",                    "IDDependencia", 
                    "IDSeccionC",                   "FecInic", 
                    "IDPersonal",                   "IDEscuela", 
                    "IDEscala",                     "IDInstitucion", 
                    "FecMatricula",                 "Beca",  
                    "Estado",                       "FechaEstado", 
                    "MatriculaTipo",                "Carnet", 
                    "PlanPago" )
            VALUES(
                    V_APEC_CAMP,                    P_ID_ALUMNO, 
                    V_APEC_TERM,                    'UCCI', 
                    V_APEC_IDSECCIONC,              V_APEC_FECINIC, 
                    'Banner', /*idpersonal*/        V_APEC_IDESCUELA,
                    'X',/*idescala*/                '00000000000' , /*institucion*/ 
                    V_APEC_FECINIC,                 0, /*beca*/ 
                    'M', /*estado*/                 V_APEC_FECINIC, /*fechaestado*/
                    'C', /*matriculatipo*/          0, /*carnet*/ 
                    5 /*planpago*/ );

      END IF;


      -- *********************************************************************************************
      -- ASINGANDO el cargo a CTACORRIENTE
      SELECT COUNT("IDAlumno") INTO V_INDICADOR FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
      WHERE "IDDependencia" = 'UCCI' 
      AND "IDSede"      = V_APEC_CAMP 
      AND "IDAlumno"    = P_ID_ALUMNO
      AND "IDPerAcad"   = V_APEC_TERM 
      AND "IDSeccionC"  = V_APEC_IDSECCIONC
      AND "FecInic"     = V_APEC_FECINIC
      AND "IDConcepto"  IN (P_CONCEPTO_BACH_01,P_CONCEPTO_BACH_02,P_CONCEPTO_BACH_03,P_CONCEPTO_BACH_04,P_CONCEPTO_BACH_06,P_CONCEPTO_BACH_07,P_CONCEPTO_IDIOMA);

      IF (V_INDICADOR = 0) THEN
            
          INSERT INTO dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
                  ("IDSede",            "IDAlumno",               "IDPerAcad", 
                  "IDDependencia",      "IDSeccionC",             "FecInic", 
                  "IDConcepto",         "FecCargo",               "Prorroga", 
                  "Cargo",              "DescMora",               "IDEscuela", 
                  "IDCuenta",           "IDInstitucion",          "Moneda", 
                  "IDEscala",           "Beca",                   "Estado", 
                  "Abono" )
          SELECT  t2."IDsede",          P_ID_ALUMNO,              V_APEC_TERM,
                  'UCCI',               V_APEC_IDSECCIONC,        V_APEC_FECINIC,
                  t1."IDConcepto",      V_RECEPTION_DATE,         '0',
                  "Monto",              0,                        t1."IDEscuela",
                  "IDCuenta",           '00000000000',            t1."Moneda",
                  '',                   0,                        'M',
                  0
          FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE  t1 
          INNER JOIN dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE  t2
            ON t1."IDSede"          = t2."IDsede"
            AND t1."IDPerAcad"      = t2."IDPerAcad" 
            AND t1."IDDependencia"  = t2."IDDependencia" 
            AND t1."IDSeccionC"     = t2."IDSeccionC" 
            AND t1."FecInic"        = t2."FecInic"
          WHERE t2."IDDependencia"  = 'UCCI'
          AND t2."IDsede"         = V_APEC_CAMP
          AND t2."IDSeccionC"     = V_APEC_IDSECCIONC
          AND t2."IDSeccionC"     <> '15NEX1A'
          AND t2."IDPerAcad"      = V_APEC_TERM
          AND T1."IDPerAcad"      = V_APEC_TERM
          AND t2."FecInic"        = V_APEC_FECINIC
          AND t1."IDConcepto"     IN (P_CONCEPTO_BACH_01,P_CONCEPTO_BACH_02,P_CONCEPTO_BACH_03,P_CONCEPTO_BACH_04,P_CONCEPTO_BACH_06,P_CONCEPTO_BACH_07,P_CONCEPTO_IDIOMA);  
          DBMS_OUTPUT.PUT_LINE('---');
      ELSIF(V_INDICADOR > 1) THEN
          RAISE V_EXCEP_NOTFOUND_CARGO;                  
      END IF;
      
      COMMIT;
EXCEPTION
  WHEN V_EXCEP_NOTFOUND_CARGO THEN
        P_MESSAGE := '- Se encontro cargos ya generados anteriormente.';
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
  WHEN OTHERS THEN
        ROLLBACK;
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CONCEPTO_BACHILLER;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


/* ===================================================================================================================
  NOMBRE    : P_SET_CONCEPTO_TITULO
  FECHA     : 28/09/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Genera cargos de Titulacion

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
PROCEDURE P_SET_CONCEPTO_TITULO ( 
        P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
        P_FOLIO_SOLICITUD       IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,       
        P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
        P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
        P_PROGRAM               IN SORLCUR.SORLCUR_PROGRAM%TYPE,     
        P_SECCION_CAJA_TITU     IN VARCHAR2,
        P_CONCEPTO_TITU_01      IN VARCHAR2,
        P_CONCEPTO_TITU_02      IN VARCHAR2,
        P_MESSAGE               OUT VARCHAR2
)
AS

    V_SECCION_CAJA_TITU       VARCHAR2(5);
    V_RECEPTION_DATE          SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE;
    V_INDICADOR               NUMBER;
    V_EXCEP_NOTFOUND_CARGO    EXCEPTION;
    
    -- APEC PARAMS
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(10);
    V_APEC_IDSECCIONC       VARCHAR2(50);
    V_APEC_IDESCUELA        VARCHAR2(50);
    V_APEC_FECINIC          DATE;
    
BEGIN
      
      -- GET FECHA DE SOLICITUD
      SELECT  SVRSVPR_RECEPTION_DATE
      INTO    V_RECEPTION_DATE
      FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
      
      /*************************************************************************
                                    Ejemplo: BAR - 103
      --------------------------------------------------------------------------
          Rectificación          Modalidad                        Carrera
      BA – Bachiller            R – Regular          -          xxx id Carrera
                                W – Gente Trabaja
                                V - Virtual  
      *************************************************************************/
      V_SECCION_CAJA_TITU := P_SECCION_CAJA_TITU || CASE P_DEPT_CODE WHEN 'UREG' THEN 'R' WHEN 'UVIR' THEN 'V' WHEN 'UPGT' THEN 'W' ELSE '-' END;
       -- IDSeccionC
      V_APEC_IDSECCIONC   := V_SECCION_CAJA_TITU || ' - ' || P_PROGRAM;
      
      -- Get SEDE, sede HYO para ureg/upgt y VIR para UVIR
      V_APEC_CAMP := CASE P_DEPT_CODE WHEN 'UREG' THEN 'HYO' WHEN 'UVIR' THEN 'VIR' WHEN 'UPGT' THEN 'HYO' ELSE '-' END;
      SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
      
      SELECT  t1."FecInic",         t2."IDEscuela"
      INTO    V_APEC_FECINIC,       V_APEC_IDESCUELA
      FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE t1
      INNER JOIN dbo.tblEscuela@BDUCCI.CONTINENTAL.EDU.PE t2
      ON t1."IDDependencia" = t2."IDDependencia"
      AND t1."IDEscuela" = t2."IDEscuela" 
      WHERE t1."IDDependencia" = 'UCCI'
      AND t1."IDsede"     = V_APEC_CAMP
      AND t1."IDPerAcad"  = V_APEC_TERM
      AND t1."IDSeccionC" = V_APEC_IDSECCIONC
      AND t2."IDEscuela"  = 'GYT'
      AND t2."IDTipoEsc"  = 'EXT';
        
      --**********************************************************************++***********************
      -- INSERT ALUMNO ESTADO en caso no exista el registro.
      SELECT COUNT("IDAlumno") INTO V_INDICADOR FROM dbo.tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE
      WHERE "IDSede"      = V_APEC_CAMP 
      AND "IDAlumno"      = P_ID_ALUMNO 
      AND "IDPerAcad"     = V_APEC_TERM
      AND "IDDependencia" = 'UCCI' 
      AND "IDSeccionC"    = V_APEC_IDSECCIONC 
      AND "FecInic"       = V_APEC_FECINIC
      AND "IDEscuela"     = V_APEC_IDESCUELA;
      ----- INSERT
      IF (V_INDICADOR = 0) THEN

            INSERT INTO dbo.tblAlumnoEstado@BDUCCI.CONTINENTAL.EDU.PE 
                    ("IDSede",                      "IDAlumno", 
                    "IDPerAcad",                    "IDDependencia", 
                    "IDSeccionC",                   "FecInic", 
                    "IDPersonal",                   "IDEscuela", 
                    "IDEscala",                     "IDInstitucion", 
                    "FecMatricula",                 "Beca",  
                    "Estado",                       "FechaEstado", 
                    "MatriculaTipo",                "Carnet", 
                    "PlanPago" )
            VALUES(
                    V_APEC_CAMP,                    P_ID_ALUMNO, 
                    V_APEC_TERM,                    'UCCI', 
                    V_APEC_IDSECCIONC,              V_APEC_FECINIC, 
                    'Banner', /*idpersonal*/        V_APEC_IDESCUELA,
                    'X',/*idescala*/                '00000000000' , /*institucion*/ 
                    V_APEC_FECINIC,                 0, /*beca*/ 
                    'M', /*estado*/                 V_APEC_FECINIC, /*fechaestado*/
                    'C', /*matriculatipo*/          0, /*carnet*/ 
                    5 /*planpago*/ );

      END IF;


      -- *********************************************************************************************
      -- ASINGANDO el cargo a CTACORRIENTE
      SELECT COUNT("IDAlumno") INTO V_INDICADOR FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
      WHERE "IDDependencia" = 'UCCI' 
      AND "IDSede"      = V_APEC_CAMP 
      AND "IDAlumno"    = P_ID_ALUMNO
      AND "IDPerAcad"   = V_APEC_TERM 
      AND "IDSeccionC"  = V_APEC_IDSECCIONC
      AND "FecInic"     = V_APEC_FECINIC
      AND "IDConcepto"  IN (P_CONCEPTO_TITU_01,P_CONCEPTO_TITU_02);

      IF (V_INDICADOR = 0) THEN
            
          INSERT INTO dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
                  ("IDSede",            "IDAlumno",               "IDPerAcad", 
                  "IDDependencia",      "IDSeccionC",             "FecInic", 
                  "IDConcepto",         "FecCargo",               "Prorroga", 
                  "Cargo",              "DescMora",               "IDEscuela", 
                  "IDCuenta",           "IDInstitucion",          "Moneda", 
                  "IDEscala",           "Beca",                   "Estado", 
                  "Abono" )
          SELECT  t2."IDsede",          P_ID_ALUMNO,              V_APEC_TERM,
                  'UCCI',               V_APEC_IDSECCIONC,        V_APEC_FECINIC,
                  t1."IDConcepto",      V_RECEPTION_DATE,         '0',
                  "Monto",              0,                        t1."IDEscuela",
                  "IDCuenta",           '00000000000',            t1."Moneda",
                  '',                   0,                        'M',
                  0
          FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE  t1 
          INNER JOIN dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE  t2
            ON t1."IDSede"          = t2."IDsede"
            AND t1."IDPerAcad"      = t2."IDPerAcad" 
            AND t1."IDDependencia"  = t2."IDDependencia" 
            AND t1."IDSeccionC"     = t2."IDSeccionC" 
            AND t1."FecInic"        = t2."FecInic"
          WHERE t2."IDDependencia"  = 'UCCI'
          AND t2."IDsede"         = V_APEC_CAMP
          AND t2."IDSeccionC"     = V_APEC_IDSECCIONC
          AND t2."IDSeccionC"     <> '15NEX1A'
          AND t2."IDPerAcad"      = V_APEC_TERM
          AND T1."IDPerAcad"      = V_APEC_TERM
          AND t2."FecInic"        = V_APEC_FECINIC
          AND t1."IDConcepto"     IN (P_CONCEPTO_TITU_01,P_CONCEPTO_TITU_02);

      ELSIF(V_INDICADOR > 1) THEN
          RAISE V_EXCEP_NOTFOUND_CARGO;                  
      END IF;
      
      COMMIT;
EXCEPTION
  WHEN V_EXCEP_NOTFOUND_CARGO THEN
        P_MESSAGE := '- Se encontro cargos ya generados anteriormente.';
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
  WHEN OTHERS THEN
        ROLLBACK;
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CONCEPTO_TITULO;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END BWZKCSPBT;