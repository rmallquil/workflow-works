/*
-- buscar objecto
select dbms_metadata.get_ddl('PROCEDURE','P_VERIFICA_CONCEPTOS_BACHILLER') from dual
drop procedure p_set_coddetalle_alumno;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfobjects;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfauto;

SET SERVEROUTPUT ON
DECLARE   P_PROGRAM               SORLCUR.SORLCUR_PROGRAM%TYPE;
          P_CONCEPTOS_VALIDOS     VARCHAR2(2000);
          P_MESSAGE               VARCHAR2(2000);
BEGIN
    P_VERIFICA_CONCEPTOS_BACHILLER(241106,'74288044','UREG','201710','BA','C01','C02','C03','C04','C05','C06','C07','C08','C09',P_PROGRAM,P_CONCEPTOS_VALIDOS,P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_PROGRAM );
    DBMS_OUTPUT.PUT_LINE(P_CONCEPTOS_VALIDOS );
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
END;
*/

/* ===================================================================================================================
  NOMBRE    : P_VERIFICA_CONCEPTOS_BACHILLER
  FECHA     : 28/09/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida si se crearon los conceptos para bachiller en el a�o(periodo) actual

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
CREATE OR REPLACE PROCEDURE P_VERIFICA_CONCEPTOS_BACHILLER ( 
        P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
        P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
        P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
        P_SECCION_CAJA_BACH     IN VARCHAR2,
        P_CONCEPTO_BACH_01      IN VARCHAR2,
        P_CONCEPTO_BACH_02      IN VARCHAR2,
        P_CONCEPTO_BACH_03      IN VARCHAR2,
        P_CONCEPTO_BACH_04      IN VARCHAR2,
        P_CONCEPTO_BACH_05      IN VARCHAR2,
        P_CONCEPTO_BACH_06      IN VARCHAR2,
        P_CONCEPTO_BACH_07      IN VARCHAR2,
        P_CONCEPTO_BACH_08      IN VARCHAR2,
        P_CONCEPTO_BACH_09      IN VARCHAR2,   
        P_PROGRAM               OUT SORLCUR.SORLCUR_PROGRAM%TYPE,     
        P_CONCEPTOS_VALIDOS     OUT VARCHAR2,
        P_MESSAGE               OUT VARCHAR2
)
AS
    
    V_NCONCEPTOS              NUMBER        := 0;
    V_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%TYPE;
    V_SECCION_CAJA_BACH       VARCHAR2(5);
    
    -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
    CURSOR C_SORLCUR_FOS IS
    SELECT    SORLCUR_PROGRAM
    FROM (
            SELECT    SORLCUR_PROGRAM
            FROM SORLCUR        INNER JOIN SORLFOS
                  ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                  AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
            WHERE   SORLCUR_PIDM        =   P_PIDM
                AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                AND SORLCUR_CURRENT_CDE =   'Y'
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
    ) WHERE ROWNUM <= 1;
    
    -- APEC PARAMS
    V_APEC_DEPT             VARCHAR2(9);
    V_APEC_CAMP             VARCHAR2(9);
    V_APEC_TERM             VARCHAR2(10);
    V_APEC_IDSECCIONC       VARCHAR2(15);
    
BEGIN

      -- #######################################################################
      -- >> GET DATOS --
      OPEN C_SORLCUR_FOS;
      LOOP
        FETCH C_SORLCUR_FOS INTO  V_ALUM_PROGRAMA;
        EXIT WHEN C_SORLCUR_FOS%NOTFOUND;
      END LOOP;
      CLOSE C_SORLCUR_FOS;
      P_PROGRAM := V_ALUM_PROGRAMA;
            
      /*************************************************************************
                                    Ejemplo: BAR - 103
      --------------------------------------------------------------------------
          Rectificaci�n          Modalidad                        Carrera
      BA � Bachiller            R � Regular          -          xxx id Carrera
                                W � Gente Trabaja
                                V - Virtual  
      *************************************************************************/
      V_SECCION_CAJA_BACH := P_SECCION_CAJA_BACH || CASE P_DEPT_CODE WHEN 'UREG' THEN 'R' WHEN 'UVIR' THEN 'V' WHEN 'UPGT' THEN 'W' ELSE '-' END;
      -- IDSeccionC
      V_APEC_IDSECCIONC := V_SECCION_CAJA_BACH || ' - ' || P_PROGRAM;
            
      -- Get SEDE, sede HYO para ureg/upgt y VIR para UVIR
      V_APEC_CAMP := CASE P_DEPT_CODE WHEN 'UREG' THEN 'HYO' WHEN 'UVIR' THEN 'VIR' WHEN 'UPGT' THEN 'HYO' ELSE '-' END;
      SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
      
      SELECT  COUNT(t1."IDConcepto") INTO V_NCONCEPTOS
      FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE  t1 
      INNER JOIN dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE  t2
        ON t1."IDSede"          = t2."IDsede"
        AND t1."IDPerAcad"      = t2."IDPerAcad" 
        AND t1."IDDependencia"  = t2."IDDependencia" 
        AND t1."IDSeccionC"     = t2."IDSeccionC" 
        AND t1."FecInic"        = t2."FecInic"
      WHERE t2."IDDependencia" = 'UCCI'
        AND t2."IDSeccionC"    = V_APEC_IDSECCIONC 
        AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_PSI' -- internado
        AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_ENF' -- internado
        AND t2."IDSeccionC"     <> '15NEX1A'
        AND t2."IDPerAcad"     = V_APEC_TERM
        AND T1."IDPerAcad"     = V_APEC_TERM
        AND t1."IDSede"        = V_APEC_CAMP
        AND t1."IDConcepto"    IN (P_CONCEPTO_BACH_01,P_CONCEPTO_BACH_02,P_CONCEPTO_BACH_03,P_CONCEPTO_BACH_04,P_CONCEPTO_BACH_05,P_CONCEPTO_BACH_06,P_CONCEPTO_BACH_07,P_CONCEPTO_BACH_08,P_CONCEPTO_BACH_09);    
      
      
      IF V_NCONCEPTOS = 9 THEN
        P_CONCEPTOS_VALIDOS := 'TRUE';
      ELSE
        P_CONCEPTOS_VALIDOS := 'FALSE';
        P_MESSAGE := 'El numero de conceptos no concuerda con los establecido.';
      END IF;
      
EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICA_CONCEPTOS_BACHILLER;