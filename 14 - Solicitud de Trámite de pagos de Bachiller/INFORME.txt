APUNTES
=================================================================================
ENALCE COMPARTIDO: https://drive.google.com/open?id=0B6g9PRwmQiqoVUswS3hSOXNCX2M

Se creo un paquete en la tarea 2: contendra en orden todas las funciones, procedimeintos creados para este fin.

	######## SOLICITUD DE TRAMITE PAGOS BACHILLER/TITULO ########
		
			- Nombre del paquete : 			BWZKCSPBT
			- Cuerpo del pakquete :			BWZKCSPBT BODY


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 00 --> carpeta 0 : TABLA INFORMATIVA - CONFIGURACION SOLICITUD DE SERVICIO
---------------------------------------------------------------------------------------------------------------------------------

	- VISTA: Seleción de trámite que quiere realizar (Bachiller/Titutlo) ..... "SZVPBTT"
	- VISTA: Seleción del idioma estudiado ................................... "SZVPBTI"

	-- FUNCION: Adicional a las reglas de servicio (EN LA CARPETA 00-WF-FUNCIONES)

			WFK_OAFORM.FUNCTION F_RSS_SPBT (
			    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
			    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
			    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
			) RETURN VARCHAR2
	

---------------------------------------------------------------------------------------------------------------------------------
TAREAS 1 --> carpeta 01 : Ttrigger
---------------------------------------------------------------------------------------------------------------------------------
	## Detecte la creación de una nueva solicitud del tipo SOL018.

	- Se elaboro triguer, Trigguer-ST_SVRSVPR_POST_INSERT_ROW

	- script para la obtencion de datos en el trigger: BWZKSVPR --> P_CONTISPBT_GETDATA
	
	- se modificò el orden de los parametros
					P_PIDM_ALUMNO	
					P_ID_ALUMNO
					P_PERIODO
					P_CORREO_ALUMNO
					P_NOMBRE_ALUMNO
					P_FOLIO_SOLICITUD
					P_FECHA_SOLICITUD
					P_COMENTARIO_ALUMNO
					P_MODALIDAD_ALUMNO
					P_COD_SEDE
					P_NOMBRE_SEDE

---------------------------------------------------------------------------------------------------------------------------------
TAREAS 2 --> carpeta 02 : Obtener Datos ingreados por el alumno
---------------------------------------------------------------------------------------------------------------------------------
		PROCEDURE P_GET_STUDENT_INPUTS (
            P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
            P_DIPLOMA_CODE        OUT VARCHAR2,
            P_DIPLOMA_DESC        OUT VARCHAR2,
            P_CONCEPTO_IDIOMA     OUT VARCHAR2,
            P_IDIOMA_DESC         OUT VARCHAR2
      	);


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 3 --> carpeta 03 : Cambio de estado solicitud
---------------------------------------------------------------------------------------------------------------------------------
		PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
		    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
		    P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
		    P_AN                  OUT NUMBER,
		    P_ERROR               OUT VARCHAR2
		);


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 4 --> carpeta 04 : Verifica si los conceptos para bachiller fueron creados
---------------------------------------------------------------------------------------------------------------------------------
		PROCEDURE P_VERIFICA_CONCEPTOS_BACHILLER ( 
		    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
		    P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
		    P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
		    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
		    P_SECCION_CAJA_BACH     IN VARCHAR2,
		    P_CONCEPTO_BACH_01      IN VARCHAR2,
		    P_CONCEPTO_BACH_02      IN VARCHAR2,
		    P_CONCEPTO_BACH_03      IN VARCHAR2,
		    P_CONCEPTO_BACH_04      IN VARCHAR2,
		    P_CONCEPTO_BACH_05      IN VARCHAR2,
		    P_CONCEPTO_BACH_06      IN VARCHAR2,
		    P_CONCEPTO_BACH_07      IN VARCHAR2,
		    P_CONCEPTO_BACH_08      IN VARCHAR2,
		    P_CONCEPTO_BACH_09      IN VARCHAR2,   
		    P_PROGRAM               OUT SORLCUR.SORLCUR_PROGRAM%TYPE,     
		    P_CONCEPTOS_VALIDOS     OUT VARCHAR2,
		    P_MESSAGE               OUT VARCHAR2
		);


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 5 --> carpeta 05 : Verifica si los conceptos para titulacion fueron creados
---------------------------------------------------------------------------------------------------------------------------------
		PROCEDURE P_VERIFICA_CONCEPTOS_TITULO ( 
		    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
		    P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
		    P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
		    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
		    P_SECCION_CAJA_TITU     IN VARCHAR2,
		    P_CONCEPTO_TITU_01      IN VARCHAR2,
		    P_CONCEPTO_TITU_02      IN VARCHAR2,
		    P_PROGRAM               OUT SORLCUR.SORLCUR_PROGRAM%TYPE,
		    P_CONCEPTOS_VALIDOS     OUT VARCHAR2,
		    P_MESSAGE               OUT VARCHAR2
		);


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 6 --> carpeta 06 : Asigna los cargos por el tramite de bachiller
---------------------------------------------------------------------------------------------------------------------------------
		PROCEDURE P_SET_CONCEPTO_BACHILLER ( 
		    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
		    P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
		    P_FOLIO_SOLICITUD       IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,       
		    P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
		    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
		    P_PROGRAM               IN SORLCUR.SORLCUR_PROGRAM%TYPE,     
		    P_SECCION_CAJA_BACH     IN VARCHAR2,
		    P_CONCEPTO_BACH_01      IN VARCHAR2,
		    P_CONCEPTO_BACH_02      IN VARCHAR2,
		    P_CONCEPTO_BACH_03      IN VARCHAR2,
		    P_CONCEPTO_BACH_04      IN VARCHAR2,
		    P_CONCEPTO_BACH_06      IN VARCHAR2,
		    P_CONCEPTO_BACH_07      IN VARCHAR2,           
		    P_CONCEPTO_IDIOMA       IN VARCHAR2,
		    P_MESSAGE               OUT VARCHAR2
		);


---------------------------------------------------------------------------------------------------------------------------------
TAREAS 7 --> carpeta 07 : Asigna los cargos por el tramite de titulacion
---------------------------------------------------------------------------------------------------------------------------------
		PROCEDURE P_SET_CONCEPTO_TITULO ( 
		    P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
		    P_ID_ALUMNO             IN SPRIDEN.SPRIDEN_ID%TYPE,
		    P_FOLIO_SOLICITUD       IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,       
		    P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
		    P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
		    P_PROGRAM               IN SORLCUR.SORLCUR_PROGRAM%TYPE,     
		    P_SECCION_CAJA_TITU     IN VARCHAR2,
		    P_CONCEPTO_TITU_01      IN VARCHAR2,
		    P_CONCEPTO_TITU_02      IN VARCHAR2,
		    P_MESSAGE               OUT VARCHAR2
		);


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

EVENTOS: