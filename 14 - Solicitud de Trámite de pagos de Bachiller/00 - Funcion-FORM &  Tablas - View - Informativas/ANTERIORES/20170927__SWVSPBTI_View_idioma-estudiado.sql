CREATE OR REPLACE VIEW "BANINST1".SWVSPBTI ("TRAMITE_ID", "TRAMITE") AS 
/* ===================================================================================================================
  NOMBRE    : SWVSTPBT
              SWView WorkFlow Solicitud tramite de PAGO de BACHILLER ó TÍTULO - IDIOMA
  FECHA     : 27/09/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso 
  OBJETIVO  : Seleción de Idioma Estudiado
  =================================================================================================================== */
SELECT 'D01' TRAMITE_ID, 'Pago por Diploma de Bachiller' TRAMITE FROM DUAL UNION
SELECT 'D02' , 'Pago por Diploma de Título' FROM DUAL;