CREATE OR REPLACE VIEW "BANINST1".SWVSPBTT ("TRAMITE_ID", "TRAMITE") AS 
/* ===================================================================================================================
  NOMBRE    : SWVSPBTT
              SWView WorkFlow Solicitud tramite de PAGO de BACHILLER ó TÍTULO - TRAMITE
  FECHA     : 27/09/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso 
  OBJETIVO  : Seleción de tramite para alumnos
  =================================================================================================================== */
SELECT 'D01' TRAMITE_ID, 'Pago por Diploma de Bachiller' TRAMITE FROM DUAL UNION
SELECT 'D02' , 'Pago por Diploma de Título' FROM DUAL;