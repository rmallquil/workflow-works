/* ===================================================================================================================
  NOMBRE    : SZVPBTT
              SZView WorkFlow solicitud tramite de PAGO de BACHILLER ó TÍTULO - TRAMITE
  FECHA     : 02/10/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso 
  OBJETIVO  : Lista codigos de tramite.
  
  MODIFICACION :
  N°    Fecha     USUARIO   Detalle
  =================================================================================================================== */
CREATE OR REPLACE VIEW "BANINST1".SZVPBTT 
(
  SZVPBTT_TRAMITE_ID, 
  SZVPBTT_TRAMITE
) AS 

SELECT 'D01' SZVPBTT_TRAMITE_ID, 'Pago por Diploma de Bachiller' SZVPBTT_TRAMITE FROM DUAL UNION
SELECT 'D02' , 'Pago por Diploma de Título' FROM DUAL;

COMMENT ON TABLE SZVPBTT IS 'SZView WorkFlow solicitud tramite de PAGO de BACHILLER ó TÍTULO - TRAMITE, Lista codigos de tramite.';
COMMENT ON COLUMN SZVPBTT.SZVPBTT_TRAMITE_ID IS 'ID codigo de TRAMITE';
COMMENT ON COLUMN SZVPBTT.SZVPBTT_TRAMITE IS 'Descripcion';
