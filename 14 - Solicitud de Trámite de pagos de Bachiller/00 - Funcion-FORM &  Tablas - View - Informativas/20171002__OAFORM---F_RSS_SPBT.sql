/*
SET SERVEROUTPUT ON
DECLARE BOOL VARCHAR2(1);
BEGIN
        BOOL :=  F_RSS_SPBT('80978','0','0');
        DBMS_OUTPUT.PUT_LINE(BOOL);
END;
*/

/* ===================================================================================================================
  NOMBRE    : F_RSS_SPBT
              "Regla Solicitud de Servicio de SOLICITUD DE TRASLADO INTERNO "
  FECHA     : 02/10/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Retorna TRUE si el alumno esta 8 ciclo o superior.             

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  =================================================================================================================== */
create or replace FUNCTION F_RSS_SPBT (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
IS
      V_CREDITOS            INTEGER;
      V_CICLO               INTEGER;

BEGIN  
--   
    -- GET CREDITOS CICLO STUDN
    P_CREDITOS_CICLO(P_PIDM,V_CREDITOS,V_CICLO);
    
    IF V_CICLO >= 8 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
--
END F_RSS_SPBT;