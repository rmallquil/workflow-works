/* ===================================================================================================================
  NOMBRE    : SZVPBTI
              SZView WorkFlow Solicitud tramite de PAGO de BACHILLER ó TÍTULO - IDIOMA
  FECHA     : 02/10/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso 
  OBJETIVO  : Lista codigos de concepto de del centro de Idioma 
  
  MODIFICACION :
  N°    Fecha     USUARIO   Detalle
  =================================================================================================================== */
CREATE OR REPLACE VIEW "BANINST1".SZVPBTI 
(
  SZVPBTI_IDIOMA_ID, 
  SZVPBTI_IDIOMA
) AS 
SELECT 'C05' SZVPBTI_TRAMITE_ID, 'Idioma Ingles' SZVPBTI_TRAMITE FROM DUAL UNION
SELECT 'C08' , 'Idioma Italiano' FROM DUAL UNION
SELECT 'C09' , 'Idioma Portugués' FROM DUAL;

COMMENT ON TABLE SZVPBTI IS 'SZView WorkFlow solicitud tramite de PAGO de BACHILLER ó TÍTULO - IDIOMA, Lista codigos de concepto de del centro de Idioma ';
COMMENT ON COLUMN SZVPBTI.SZVPBTI_IDIOMA_ID IS 'ID Concepto de IDIOMA';
COMMENT ON COLUMN SZVPBTI.SZVPBTI_IDIOMA IS 'Descripcion del ID CONCEPTO';