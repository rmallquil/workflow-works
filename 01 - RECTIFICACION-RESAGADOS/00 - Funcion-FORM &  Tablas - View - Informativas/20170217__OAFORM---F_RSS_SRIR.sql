SET SERVEROUTPUT ON
DECLARE BOOL VARCHAR2(1);
BEGIN
        BOOL :=  WFK_OAFORM.F_RSS_SRIR('428093','0','0');
        --DBMS_OUTPUT.PUT_LINE(CASE WHEN BOOL = TRUE THEN 'TRUE' ELSE 'FALSE' END);
        DBMS_OUTPUT.PUT_LINE(BOOL);
END;


FUNCTION F_RSS_SRIR (
    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE
) RETURN VARCHAR2
/* ===================================================================================================================
  NOMBRE    : F_RSS_SRIR
              "Regla Solicitud de Servicio de SOLICITUD RECTIFICACIÓN DE INSCRIPCIÓN REZAGADOS"
  FECHA     : 20/02/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : 

  NRO     FECHA         USUARIO       MODIFICACION
  001     09/02/2017    RMALLQUI      Agrego validacion solo para matriculados (CON NRC)   
  002     26/04/2017    RMALLQUI      Se agrego validacion para el segundo modulo de UVIR y UPGT
  =================================================================================================================== */
 IS
      P_CODIGO_SOL          SVRRSRV.SVRRSRV_SRVC_CODE%TYPE      := 'SOL003';
      V_PRIORIDAD           NUMBER;
      P_INDICADOR           NUMBER;
      V_CODE_DEPT           STVDEPT.STVDEPT_CODE%TYPE; 
      V_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
      V_CODE_CAMP           STVCAMP.STVCAMP_CODE%TYPE;

      P_CODE_TERM           STVTERM.STVTERM_CODE%TYPE;
      V_PART_PERIODO        SOBPTRM.SOBPTRM_PTRM_CODE%TYPE; --------------- Parte-de-Periodo
      -- OBTENER PERIODO por modulo de UVIR y UPGT
      CURSOR C_SFRRSTS IS
      SELECT SFRRSTS_TERM_CODE FROM (
          -- Forma SFARSTS  ---> fechas para las partes de periodo
          SELECT DISTINCT SFRRSTS_TERM_CODE 
          FROM SFRRSTS
          WHERE SFRRSTS_PTRM_CODE IN (V_PART_PERIODO || '1', V_PART_PERIODO || '2')
          AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
          AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND SFRRSTS_END_DATE
          ORDER BY SFRRSTS_TERM_CODE DESC
      ) WHERE ROWNUM <= 1;    
BEGIN  
--    
    -- DATOS ALUMNO
    P_GETDATA_ALUMN(P_PIDM,V_CODE_DEPT,V_CODE_TERM,V_CODE_CAMP);

    /************************************************************************************************************/
    -- Segun regla para UPGT y UVIR, los que tramitan reserva el mismo periodo que ingresaron 
    /************************************************************************************************************/
    IF  (V_CODE_TERM IS NULL) AND  (V_CODE_DEPT = 'UPGT' OR V_CODE_DEPT = 'UVIR') THEN
        -- GET parte PERIODO para poder obtener despues el PERIODO ACTIVO
        SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                  WHEN 'UPGT' THEN 'W' 
                                  WHEN 'UREG' THEN 'R' 
                                  WHEN 'UPOS' THEN '-' 
                                  WHEN 'ITEC' THEN '-' 
                                  WHEN 'UCIC' THEN '-' 
                                  WHEN 'UCEC' THEN '-' 
                                  WHEN 'ICEC' THEN '-' 
                                  ELSE '1' END ||
                CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                  WHEN 'F01' THEN 'A' 
                                  WHEN 'F02' THEN 'L' 
                                  WHEN 'F03' THEN 'C' 
                                  WHEN 'V00' THEN 'V' 
                                  ELSE '9' END
        INTO V_PART_PERIODO
        FROM STVCAMP,STVDEPT 
        WHERE STVDEPT_CODE = V_CODE_DEPT AND STVCAMP_CODE = V_CODE_CAMP;

        OPEN C_SFRRSTS;
          LOOP
            FETCH C_SFRRSTS INTO P_CODE_TERM;
            IF C_SFRRSTS%FOUND THEN
                V_CODE_TERM := P_CODE_TERM;
            ELSE EXIT;
          END IF;
          END LOOP;
        CLOSE C_SFRRSTS;

    END IF;

    -- VALIDAR MODALIDAD Y PRIORIDAD
    P_CHECK_DEPT_PRIORIDAD (P_PIDM, P_CODIGO_SOL, V_CODE_DEPT, NULL, NULL, V_PRIORIDAD);

    -- VERIFICAR QUE YA ESTE MATRICULADO
    ---A VERIFICAR QUE TENGA NRC's
      SELECT COUNT(*) INTO P_INDICADOR FROM SFRSTCR 
      WHERE SFRSTCR_PIDM = P_PIDM
      AND SFRSTCR_TERM_CODE = V_CODE_TERM; 
    
    IF V_PRIORIDAD > 0 AND P_INDICADOR > 0 THEN
        RETURN 'Y';
    END IF;
        RETURN 'N';
--
END F_RSS_SRIR;