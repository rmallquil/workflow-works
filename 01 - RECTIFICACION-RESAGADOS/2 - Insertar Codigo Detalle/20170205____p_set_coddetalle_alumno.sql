/*
drop procedure p_set_coddetalle_alumno;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfobjects;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfauto;

SET SERVEROUTPUT ON
DECLARE   P_TRAN_NUMBER           TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
          P_ERROR                 VARCHAR2(100);
BEGIN
    P_SET_CODDETALLE_ALUMNO(241106,'REC','201620',P_TRAN_NUMBER,P_ERROR);
    DBMS_OUTPUT.PUT_LINE(P_TRAN_NUMBER  || '--' || P_ERROR);
END;

*/
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------


CREATE OR REPLACE PROCEDURE P_SET_CODDETALLE_ALUMNO( 
        P_PIDM_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_COD_DETALLE           IN SFRRGFE.SFRRGFE_DETL_CODE%TYPE,
        P_PERIODO               IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
        P_TRAN_NUMBER           OUT TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
        P_ERROR                 OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SET_CODDETALLE_ALUMNO
  FECHA     : 19/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : genera una deuda por CODIGO DETALLE.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
        NOM_SERVICIO                VARCHAR2(30);
        
        C_SEQNO                   SORLCUR.SORLCUR_SEQNO %type;
        C_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;
        C_ALUM_CAMPUS             SORLCUR.SORLCUR_CAMP_CODE%type;
        C_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
        C_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
        C_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
        C_ALUM_PERD_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
        C_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP
        C_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE
        C_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%type;

        C_CARGO_MINIMO          SFRRGFE.SFRRGFE_MIN_CHARGE%TYPE;
        P_ROWID                 GB_COMMON.INTERNAL_RECORD_ID_TYPE;

        -- APEC PARAMS
        P_PART_PERIODO          VARCHAR2(9);
        P_APEC_CAMP             VARCHAR2(9);
        P_APEC_DEPT             VARCHAR2(9);
        P_APEC_TERM             VARCHAR2(10);
        P_APEC_IDSECCIONC       VARCHAR2(15);
        P_APEC_FECINIC          DATE;
        P_APEC_MOUNT          NUMBER;
        P_APEC_IDALUMNO       VARCHAR2(10);
BEGIN

      -- #######################################################################
      -- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
      SELECT    SORLCUR_SEQNO,      
                SORLCUR_LEVL_CODE,    
                SORLCUR_CAMP_CODE,        
                SORLCUR_COLL_CODE,
                SORLCUR_DEGC_CODE,  
                SORLCUR_PROGRAM,      
                SORLCUR_TERM_CODE_ADMIT,  
                --SORLCUR_STYP_CODE,
                SORLCUR_STYP_CODE,  
                SORLCUR_RATE_CODE,    
                SORLFOS_DEPT_CODE   
      INTO      C_SEQNO,
                C_ALUM_NIVEL, 
                C_ALUM_CAMPUS, 
                C_ALUM_ESCUELA, 
                C_ALUM_GRADO, 
                C_ALUM_PROGRAMA, 
                C_ALUM_PERD_ADM,
                C_ALUM_TIPO_ALUM,
                C_ALUM_TARIFA,
                C_ALUM_DEPARTAMENTO
      FROM (
              SELECT    SORLCUR_SEQNO,      SORLCUR_LEVL_CODE,    SORLCUR_CAMP_CODE,        SORLCUR_COLL_CODE,
                        SORLCUR_DEGC_CODE,  SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  --SORLCUR_STYP_CODE,
                        SORLCUR_STYP_CODE,  SORLCUR_RATE_CODE,    SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM_ALUMNO 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  -- AND SORLCUR_TERM_CODE   =   P_PERIODO 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
              ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM <= 1;
      

      ---#################################----- APEC -----#######################################
      --########################################################################################
      -- PKG_GLOBAL.GET_VAL: (0) APEC(BDUCCI) <------> (1) BANNER
      IF PKG_GLOBAL.GET_VAL = 0  THEN -- 0 ---> APEC(BDUCCI)

            -- GET CRONOGRAMA SECCIONC
            SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                              WHEN 'UPGT' THEN 'W' 
                                              WHEN 'UREG' THEN 'R' 
                                              WHEN 'UPOS' THEN '-' 
                                              WHEN 'ITEC' THEN '-' 
                                              WHEN 'UCIC' THEN '-' 
                                              WHEN 'UCEC' THEN '-' 
                                              WHEN 'ICEC' THEN '-' 
                                              ELSE '1' END ||
                            CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                              WHEN 'F01' THEN 'A' 
                                              WHEN 'F02' THEN 'L' 
                                              WHEN 'F03' THEN 'C' 
                                              WHEN 'V00' THEN 'V' 
                                              ELSE '9' END
                    INTO P_PART_PERIODO
                    FROM STVCAMP,STVDEPT 
                    WHERE STVDEPT_CODE = C_ALUM_DEPARTAMENTO AND STVCAMP_CODE = C_ALUM_CAMPUS;
            
            SELECT CZRCAMP_CAMP_BDUCCI INTO P_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = C_ALUM_CAMPUS;-- CAMPUS 
            SELECT CZRTERM_TERM_BDUCCI INTO P_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_PERIODO ;-- PERIODO
            SELECT CZRDEPT_DEPT_BDUCCI INTO P_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = C_ALUM_DEPARTAMENTO;-- DEPARTAMENTO

            -- GET DATOS CTA CORRIENTE -- P_APEC_IDALUMNO
            WITH 
                CTE_tblSeccionC AS (
                        -- GET SECCIONC
                        SELECT  "IDSeccionC" IDSeccionC,
                                "FecInic" FecInic
                        FROM dbo.tblSeccionC@DBBCK.CONTINENTAL.EDU.PE 
                        WHERE "IDDependencia"='UCCI'
                        AND "IDsede"    = P_APEC_CAMP
                        AND "IDPerAcad" = P_APEC_TERM
                        AND "IDEscuela" = C_ALUM_PROGRAMA
                        AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                        AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                        AND "IDSeccionC" <> '15NEX1A'
                        AND SUBSTRB("IDSeccionC",-2,2) IN (
                            -- PARTE PERIODO           
                            SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (P_PART_PERIODO || '%')
                        )
                ),
                CTE_tblPersonaAlumno AS (
                        SELECT "IDAlumno" IDAlumno 
                        FROM  dbo.tblPersonaAlumno@DBBCK.CONTINENTAL.EDU.PE 
                        WHERE "IDPersona" IN ( 
                            SELECT "IDPersona" FROM dbo.tblPersona@DBBCK.CONTINENTAL.EDU.PE WHERE "IDPersonaN" = P_PIDM_ALUMNO
                        )
                )
            SELECT "IDAlumno", "IDSeccionC" INTO P_APEC_IDALUMNO, P_APEC_IDSECCIONC
            FROM dbo.tblCtaCorriente@DBBCK.CONTINENTAL.EDU.PE 
            WHERE "IDDependencia" = 'UCCI'
            AND "IDAlumno"    IN ( SELECT IDAlumno FROM CTE_tblPersonaAlumno )
            AND "IDSede"      = P_APEC_CAMP
            AND "IDPerAcad"   = P_APEC_TERM
            AND "IDEscuela"   = C_ALUM_PROGRAMA
            AND "IDSeccionC"  IN ( SELECT IDSeccionC FROM CTE_tblSeccionC )
            AND "IDConcepto"  = P_COD_DETALLE;
            
            -- GET MONTO 
            SELECT "Monto" INTO P_APEC_MOUNT
            FROM dbo.tblConceptos@DBBCK.CONTINENTAL.EDU.PE 
            WHERE "IDDependencia" = 'UCCI'
            AND "IDSede"      = P_APEC_CAMP
            AND "IDPerAcad"   = P_APEC_TERM
            AND "IDSeccionC"  = P_APEC_IDSECCIONC
            AND "IDConcepto"  = P_COD_DETALLE;

            -- UPDATE MONTO(s)
            UPDATE dbo.tblCtaCorriente@DBBCK.CONTINENTAL.EDU.PE 
            SET "Cargo" = "Cargo" + P_APEC_MOUNT
            WHERE "IDDependencia" = 'UCCI'
            AND "IDAlumno"    = P_APEC_IDALUMNO
            AND "IDSede"      = P_APEC_CAMP
            AND "IDPerAcad"   = P_APEC_TERM
            AND "IDEscuela"   = C_ALUM_PROGRAMA
            AND "IDSeccionC"  = P_APEC_IDSECCIONC
            AND "IDConcepto"  = P_COD_DETALLE;
            
            P_TRAN_NUMBER := 0;

      ELSE -- 1 ---> BANNER

              -- GET - Codigo detalle y tambien VALIDA que si se encuentre configurado correctamente.
              SELECT SFRRGFE_MIN_CHARGE INTO C_CARGO_MINIMO
              FROM SFRRGFE 
              WHERE SFRRGFE_TERM_CODE = P_PERIODO AND SFRRGFE_DETL_CODE = P_COD_DETALLE AND SFRRGFE_TYPE = 'STUDENT'
              AND NVL(NVL(SFRRGFE_LEVL_CODE, c_alum_nivel),'-')           = NVL(NVL(c_alum_nivel, SFRRGFE_LEVL_CODE),'-')--------------------- Nivel
              AND NVL(NVL(SFRRGFE_CAMP_CODE, c_alum_campus),'-')          = NVL(NVL(c_alum_campus, SFRRGFE_CAMP_CODE),'-')  ----------------- Campus (sede)
              AND NVL(NVL(SFRRGFE_COLL_CODE, c_alum_escuela),'-')         = NVL(NVL(c_alum_escuela, SFRRGFE_COLL_CODE),'-') --------------- Escuela 
              AND NVL(NVL(SFRRGFE_DEGC_CODE, c_alum_grado),'-')           = NVL(NVL(c_alum_grado, SFRRGFE_DEGC_CODE),'-') ----------- Grado
              AND NVL(NVL(SFRRGFE_PROGRAM, c_alum_programa),'-')          = NVL(NVL(c_alum_programa, SFRRGFE_PROGRAM),'-') ---------- Programa
              AND NVL(NVL(SFRRGFE_TERM_CODE_ADMIT, c_alum_perd_adm),'-')  = NVL(NVL(c_alum_perd_adm, SFRRGFE_TERM_CODE_ADMIT),'-') --------- Periodo Admicion
              -- SFRRGFE_PRIM_SEC_CDE -- Curriculums (prim, secundario, cualquiera)
              -- SFRRGFE_LFST_CODE -- Tipo Campo Estudio (MAJOR, ...)
              -- SFRRGFE_MAJR_CODE -- Codigo Campo Estudio (Carrera)
              AND NVL(NVL(SFRRGFE_DEPT_CODE, c_alum_departamento),'-')    = NVL(NVL(c_alum_departamento, SFRRGFE_DEPT_CODE),'-') ------------ Departamento
              -- SFRRGFE_LFST_PRIM_SEC_CDE -- Campo Estudio   (prim, secundario, cualquiera)
              AND NVL(NVL(SFRRGFE_STYP_CODE_CURRIC, c_alum_tipo_alum),'-') = NVL(NVL(c_alum_tipo_alum, SFRRGFE_STYP_CODE_CURRIC),'-') ------ Tipo Alumno Curriculum
              AND NVL(NVL(SFRRGFE_RATE_CODE_CURRIC, c_alum_tarifa),'-')   = NVL(NVL(c_alum_tarifa, SFRRGFE_RATE_CODE_CURRIC),'-'); ------- Trf Curriculum (Escala)

              -- GET - Descripcion de CODIGO DETALLE  
              SELECT TBBDETC_DESC INTO NOM_SERVICIO FROM TBBDETC WHERE TBBDETC_DETAIL_CODE = P_COD_DETALLE;
            
              -- #######################################################################
              -- GENERAR DEUDA
              TB_RECEIVABLE.p_create ( p_pidm                 =>  P_PIDM_ALUMNO,        -- PIDEM ALUMNO
                                       p_term_code            =>  P_PERIODO,          -- DETALLE
                                       p_detail_code          =>  P_COD_DETALLE,      -- CODIGO DETALLE 
                                       p_user                 =>  USER,               -- USUARIO
                                       p_entry_date           =>  SYSDATE,     
                                       p_amount               =>  C_CARGO_MINIMO,
                                       p_effective_date       =>  SYSDATE,
                                       p_bill_date            =>  NULL,    
                                       p_due_date             =>  NULL,    
                                       p_desc                 =>  NOM_SERVICIO,    
                                       p_receipt_number       =>  NULL,     -- numero de recibo que se muestra en la forma TVAAREV
                                       p_tran_number_paid     =>  NULL,     -- numero de transaccion que ser� pagara
                                       p_crossref_pidm        =>  NULL,    
                                       p_crossref_number      =>  NULL,    
                                       p_crossref_detail_code =>  NULL,     
                                       p_srce_code            =>  'T',    -- defaul 'T', pero p_processfeeassessment crea un 'R' Modulo registro  
                                       p_acct_feed_ind        =>  'Y',
                                       p_session_number       =>  0,    
                                       p_cshr_end_date        =>  NULL,    
                                       p_crn                  =>  NULL,
                                       p_crossref_srce_code   =>  NULL,
                                       p_loc_mdt              =>  NULL,
                                       p_loc_mdt_seq          =>  NULL,    
                                       p_rate                 =>  NULL,    
                                       p_units                =>  NULL,     
                                       p_document_number      =>  NULL,    
                                       p_trans_date           =>  NULL,    
                                       p_payment_id           =>  NULL,    
                                       p_invoice_number       =>  NULL,    
                                       p_statement_date       =>  NULL,    
                                       p_inv_number_paid      =>  NULL,    
                                       p_curr_code            =>  NULL,    
                                       p_exchange_diff        =>  NULL,    
                                       p_foreign_amount       =>  NULL,    
                                       p_late_dcat_code       =>  NULL,    
                                       p_atyp_code            =>  NULL,    
                                       p_atyp_seqno           =>  NULL,    
                                       p_card_type_vr         =>  NULL,    
                                       p_card_exp_date_vr     =>  NULL,     
                                       p_card_auth_number_vr  =>  NULL,    
                                       p_crossref_dcat_code   =>  NULL,    
                                       p_orig_chg_ind         =>  NULL,    
                                       p_ccrd_code            =>  NULL,    
                                       p_merchant_id          =>  NULL,    
                                       p_data_origin          =>  'WorkFlow',    
                                       p_override_hold        =>  'N',     
                                       p_tran_number_out      =>  P_TRAN_NUMBER, 
                                       p_rowid_out            =>  P_ROWID);
      
      END IF;


      COMMIT;
EXCEPTION
  WHEN OTHERS THEN
        ROLLBACK;
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CODDETALLE_ALUMNO;