/*
drop procedure p_set_coddetalle_alumno;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfobjects;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfauto;

set serveroutput on
DECLARE p_error varchar2(64);
begin
  p_set_coddetalle_alumno('179316','201710','EXM',p_error);
  DBMS_OUTPUT.PUT_LINE(p_error);
end;

*/
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

create or replace PROCEDURE p_set_coddetalle_alumno (
  p_id_alumno       IN TBRACCD.TBRACCD_PIDM%TYPE, 
  p_periodo         IN TBRACCD.TBRACCD_TERM_CODE%TYPE , 
  p_codigo_detalle  IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
  p_error           OUT varchar2
  )
/* ===================================================================================================================
  NOMBRE    : p_set_coddetalle_alumno
  FECHA     : 24/08/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Inserta un c�digo de detalle a la cuenta corriente del alumno para un periodo correspondiente.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */

AS
        -- p_id_alumno               TBRACCD.TBRACCD_PIDM%TYPE;
        c_codigo_detalle_dec      TBBDETC.TBBDETC_DESC%TYPE;
        c_user                    TBRACCD.TBRACCD_USER%TYPE;
        c_tran_number             TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
        
        c_alum_nivel              SORLCUR.SORLCUR_LEVL_CODE%TYPE;
        c_alum_campus             SORLCUR.SORLCUR_CAMP_CODE%TYPE;
        c_alum_escuela            SORLCUR.SORLCUR_COLL_CODE%TYPE;
        c_alum_grado              SORLCUR.SORLCUR_DEGC_CODE%TYPE;
        c_alum_programa           SORLCUR.SORLCUR_PROGRAM%TYPE;
        c_alum_perd_adm           SORLCUR.SORLCUR_TERM_CODE_ADMIT%TYPE;
        c_alum_tipo_alum          SORLCUR.SORLCUR_STYP_CODE%TYPE;           -- STVSTYP
        c_alum_tarifa             SORLCUR.SORLCUR_RATE_CODE%TYPE;           -- STVRATE
        c_alum_departamento       SORLFOS.SORLFOS_DEPT_CODE%TYPE;
        
        c_nivel                   SFRRGFE.SFRRGFE_LEVL_CODE%TYPE;           -- STVLEVL
        c_campus                  SFRRGFE.SFRRGFE_CAMP_CODE%TYPE;           -- STVCAMP
        c_escuela                 SFRRGFE.SFRRGFE_COLL_CODE%TYPE;           -- STVCOLL
        c_grado                   SFRRGFE.SFRRGFE_DEGC_CODE%TYPE;           -- STVDEGC
        c_programa                SFRRGFE.SFRRGFE_PROGRAM%TYPE;             -- consultar relacion con SFRRGFE_MAJR_CODE-STVMAJR
        c_perd_adm                SFRRGFE.SFRRGFE_TERM_CODE_ADMIT%TYPE;     -- STVTERM
        --c_curriculums             SFRRGFE.SFRRGFE_PRIM_SEC_CDE%TYPE;
        c_tipo_camp_studio        SFRRGFE.SFRRGFE_LFST_CODE%TYPE;           -- GTVLFST
        c_cod_camp_studio         SFRRGFE.SFRRGFE_MAJR_CODE%TYPE;                   -- STVMAJR
        c_depto                   SFRRGFE.SFRRGFE_DEPT_CODE%TYPE;           -- STVDEPT
        --c_camp_studio             SFRRGFE.SFRRGFE_LFST_PRIM_SEC_CDE%TYPE; 
        c_tipo_alumn_curriculm    SFRRGFE.SFRRGFE_STYP_CODE_CURRIC%TYPE;    -- STVSTYP
        c_trf_curriculm           SFRRGFE.SFRRGFE_RATE_CODE_CURRIC%TYPE;    -- STVRATE
        c_cargo_minimo            SFRRGFE.SFRRGFE_MIN_CHARGE%TYPE;
        
        /* FORMAS */
        c_form                    varchar2(16);
BEGIN
--
    ----------------------------------------------------------------------------
        c_form := 'SGASTDN';
        /* NOTA: probabilidad de encontrar doble curricula activa para el alumno - saldria error */
        SELECT 
            SORLCUR_LEVL_CODE,
            SORLCUR_CAMP_CODE,
            SORLCUR_COLL_CODE,
            SORLCUR_DEGC_CODE,
            SORLCUR_PROGRAM,
            SORLCUR_TERM_CODE_ADMIT,
            --SORLCUR_STYP_CODE,
            SORLCUR_STYP_CODE,
            SORLCUR_RATE_CODE,
            SORLFOS_DEPT_CODE
        INTO 
            c_alum_nivel, c_alum_campus, c_alum_escuela, c_alum_grado, c_alum_programa, c_alum_perd_adm,/*c_alum_tipo_alum,*/
            c_alum_tipo_alum,c_alum_tarifa,c_alum_departamento
        FROM SORLCUR 
        INNER JOIN SORLFOS
          ON SORLCUR_PIDM =  SORLFOS_PIDM AND SORLCUR_SEQNO = SORLFOS.SORLFOS_LCUR_SEQNO
        WHERE SORLCUR_PIDM = p_id_alumno AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
        AND SORLCUR_TERM_CODE = p_periodo AND SORLCUR_CACT_CODE = 'ACTIVE';
    ----------------------------------------------------------------------------
        c_form := 'SFARGFE';
        --DBMS_OUTPUT.PUT_LINE(c_alum_nivel ||' '|| c_alum_campus  ||' '||  c_alum_escuela  ||' '||  c_alum_grado  ||' '||  c_alum_programa  ||' '||  c_alum_perd_adm  ||' '||  c_alum_tipo_alum ||' '|| c_alum_tarifa  ||' - '||  c_alum_departamento);
--        SELECT 
--            SFRRGFE_LEVL_CODE,
--            SFRRGFE_CAMP_CODE,
--            SFRRGFE_COLL_CODE,
--            SFRRGFE_DEGC_CODE,
--            SFRRGFE_PROGRAM,
--            SFRRGFE_TERM_CODE_ADMIT,
--            --SFRRGFE_PRIM_SEC_CDE,
--            SFRRGFE_LFST_CODE,
--            SFRRGFE_MAJR_CODE,
--            SFRRGFE_DEPT_CODE,
--            --SFRRGFE_LFST_PRIM_SEC_CDE,
--            SFRRGFE_STYP_CODE_CURRIC,
--            SFRRGFE_RATE_CODE_CURRIC
--        INTO  c_nivel ,c_campus ,c_escuela ,c_grado ,c_programa ,c_perd_adm /*,c_curriculums*/,c_tipo_camp_studio
--              ,c_cod_camp_studio ,c_depto /*,c_camp_studio ,c_tipo_alumn_curriculm ,c_trf_curriculm */
        SELECT SFRRGFE_MIN_CHARGE INTO c_cargo_minimo
         FROM SFRRGFE 
        WHERE SFRRGFE_TERM_CODE = p_periodo AND SFRRGFE_DETL_CODE = p_codigo_detalle AND SFRRGFE_TYPE = 'STUDENT'
        AND NVL(SFRRGFE_LEVL_CODE, c_alum_nivel) = NVL(c_alum_nivel, SFRRGFE_LEVL_CODE) 
        AND NVL(SFRRGFE_CAMP_CODE, c_alum_campus) = NVL(c_alum_campus, SFRRGFE_CAMP_CODE) 
        AND NVL(SFRRGFE_COLL_CODE, c_alum_escuela) = NVL(c_alum_escuela, SFRRGFE_COLL_CODE) 
        AND NVL(SFRRGFE_DEGC_CODE, c_alum_grado) = NVL(c_alum_grado, SFRRGFE_DEGC_CODE) 
        AND NVL(SFRRGFE_PROGRAM, c_alum_programa) = NVL(c_alum_programa, SFRRGFE_PROGRAM) 
        AND NVL(SFRRGFE_TERM_CODE_ADMIT, c_alum_perd_adm) = NVL(c_alum_perd_adm, SFRRGFE_TERM_CODE_ADMIT) 
        AND NVL(SFRRGFE_DEPT_CODE, c_alum_departamento) = NVL(c_alum_departamento, SFRRGFE_DEPT_CODE)
        AND NVL(SFRRGFE_STYP_CODE_CURRIC, c_alum_tipo_alum) = NVL(c_alum_tipo_alum, SFRRGFE_STYP_CODE_CURRIC)
        AND NVL(SFRRGFE_RATE_CODE_CURRIC, c_alum_tarifa) = NVL(c_alum_tarifa, SFRRGFE_RATE_CODE_CURRIC);
    ----------------------------------------------------------------------------
      
      SELECT TBBDETC_DESC INTO c_codigo_detalle_dec  FROM TBBDETC  WHERE TBBDETC_DETAIL_CODE = p_codigo_detalle AND TBBDETC_TYPE_IND = 'C';
      SELECT user INTO c_user FROM dual;
      SELECT TBRACCD_TRAN_NUMBER + 1 INTO c_tran_number FROM (
                        SELECT TBRACCD_TRAN_NUMBER FROM TBRACCD
                        WHERE TBRACCD_PIDM = p_id_alumno ORDER BY TBRACCD_TRAN_NUMBER DESC
                      )WHERE ROWNUM<= 1;
    
    ----------------------------------------------------------------------------
      INSERT INTO TBRACCD (
        TBRACCD_PIDM ,TBRACCD_TRAN_NUMBER ,TBRACCD_TERM_CODE ,TBRACCD_DETAIL_CODE ,TBRACCD_USER ,TBRACCD_ENTRY_DATE,
        TBRACCD_AMOUNT ,TBRACCD_BALANCE ,TBRACCD_EFFECTIVE_DATE ,TBRACCD_DESC ,TBRACCD_SRCE_CODE,TBRACCD_ACCT_FEED_IND,
        TBRACCD_ACTIVITY_DATE ,TBRACCD_SESSION_NUMBER,TBRACCD_TRANS_DATE
      )
      SELECT
            p_id_alumno ,c_tran_number ,p_periodo ,p_codigo_detalle ,c_user ,SYSDATE ,c_cargo_minimo,0 ,SYSDATE ,c_codigo_detalle_dec ,'T' ,'Y' ,SYSDATE,0,SYSDATE
      FROM DUAL
      WHERE EXISTS (
          --SELECT TBBDETC_DESC FROM TBBDETC  WHERE TBBDETC_DETAIL_CODE = p_codigo_detalle
           SELECT SFRRGFE_MIN_CHARGE 
             FROM SFRRGFE 
            WHERE SFRRGFE_TERM_CODE = p_periodo AND SFRRGFE_DETL_CODE =    AND SFRRGFE_TYPE = 'STUDENT'
            AND NVL(SFRRGFE_LEVL_CODE, c_alum_nivel) = NVL(c_alum_nivel, SFRRGFE_LEVL_CODE) 
            AND NVL(SFRRGFE_CAMP_CODE, c_alum_campus) = NVL(c_alum_campus, SFRRGFE_CAMP_CODE) 
            AND NVL(SFRRGFE_COLL_CODE, c_alum_escuela) = NVL(c_alum_escuela, SFRRGFE_COLL_CODE) 
            AND NVL(SFRRGFE_DEGC_CODE, c_alum_grado) = NVL(c_alum_grado, SFRRGFE_DEGC_CODE) 
            AND NVL(SFRRGFE_PROGRAM, c_alum_programa) = NVL(c_alum_programa, SFRRGFE_PROGRAM) 
            AND NVL(SFRRGFE_TERM_CODE_ADMIT, c_alum_perd_adm) = NVL(c_alum_perd_adm, SFRRGFE_TERM_CODE_ADMIT) 
            AND NVL(SFRRGFE_DEPT_CODE, c_alum_departamento) = NVL(c_alum_departamento, SFRRGFE_DEPT_CODE)
            AND NVL(SFRRGFE_STYP_CODE_CURRIC, c_alum_tipo_alum) = NVL(c_alum_tipo_alum, SFRRGFE_STYP_CODE_CURRIC)
            AND NVL(SFRRGFE_RATE_CODE_CURRIC, c_alum_tarifa) = NVL(c_alum_tarifa, SFRRGFE_RATE_CODE_CURRIC)
      );
      
      p_error := 'Correcto';
    ----------------------------------------------------------------------------

--
    COMMIT;
EXCEPTION
  WHEN TOO_MANY_ROWS
           THEN DBMS_OUTPUT.PUT_LINE('Se detecto mas de un REGISTRO (curriculm activo o codigos de detalles) para el alumno, por lo que no se completo lo solicitado. : ' || c_form );
  WHEN NO_DATA_FOUND
           THEN DBMS_OUTPUT.PUT_LINE ('NO se detectaron REGISTROS (curriculm activo o codigos de detalles) para el alumno, por lo que no se completo lo solicitado. : '  || c_form );
  WHEN OTHERS THEN
          p_error := SUBSTR(SQLERRM, 1, 64);
          raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;

----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------
