/*
drop procedure p_set_coddetalle_alumno;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfobjects;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfauto;

set serveroutput on
DECLARE p_error varchar2(64);
begin
  p_set_coddetalle_alumno('179316','201710','CR1',p_error);
  DBMS_OUTPUT.PUT_LINE(p_error);
end;

*/
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

create or replace PROCEDURE p_set_coddetalle_alumno (
  p_id_alumno       IN TBRACCD.TBRACCD_PIDM%TYPE, 
  p_periodo         IN TBRACCD.TBRACCD_TERM_CODE%TYPE , 
  p_codigo_detalle  IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
  p_error           OUT varchar2
  )
/* ===================================================================================================================
	NOMBRE		: sp_set_coddetalle_alumno
	FECHA		  : 24/08/16
	AUTOR		  : Mallqui Lopez, Richard Alfonso
	OBJETIVO	: Inserta un c�digo de detalle a la cuenta corriente del alumno para un periodo correspondiente.

	MODIFICACIONES
	NRO 	FECHA		USUARIO		MODIFICACION

	=================================================================================================================== */

AS
        -- p_id_alumno               TBRACCD.TBRACCD_PIDM%TYPE;
        c_codigo_detalle_dec      TBBDETC.TBBDETC_DESC%TYPE;
        c_user                    TBRACCD.TBRACCD_USER%TYPE;
        c_tran_number             TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
BEGIN
--
      -- SELECT SPRIDEN_PIDM INTO c_pidm FROM SPRIDEN WHERE SPRIDEN_ID = p_id_alumno AND SPRIDEN_CHANGE_IND IS NULL;
      SELECT TBBDETC_DESC INTO c_codigo_detalle_dec  FROM TBBDETC  WHERE TBBDETC_DETAIL_CODE = p_codigo_detalle;
      SELECT user INTO c_user FROM dual;
      SELECT TBRACCD_TRAN_NUMBER + 1 INTO c_tran_number FROM (
                        SELECT TBRACCD_TRAN_NUMBER FROM TBRACCD
                        WHERE TBRACCD_PIDM = p_id_alumno ORDER BY TBRACCD_TRAN_NUMBER DESC
                      )WHERE ROWNUM<= 1;
    --
      INSERT INTO TBRACCD (
      TBRACCD_PIDM ,TBRACCD_TRAN_NUMBER ,TBRACCD_TERM_CODE ,TBRACCD_DETAIL_CODE ,TBRACCD_USER ,TBRACCD_ENTRY_DATE,
      TBRACCD_AMOUNT ,TBRACCD_BALANCE ,TBRACCD_EFFECTIVE_DATE ,TBRACCD_DESC ,TBRACCD_SRCE_CODE,TBRACCD_ACCT_FEED_IND 
      ,TBRACCD_ACTIVITY_DATE ,TBRACCD_SESSION_NUMBER,TBRACCD_TRANS_DATE)
      SELECT
            p_id_alumno ,c_tran_number ,p_periodo ,p_codigo_detalle ,c_user ,SYSDATE ,0,0 ,SYSDATE ,c_codigo_detalle_dec ,'T' ,'Y' ,SYSDATE,0,SYSDATE
      FROM DUAL
      WHERE NOT EXISTS (
          SELECT * FROM TBRACCD WHERE TBRACCD_PIDM = p_id_alumno 
          AND  TBRACCD_DETAIL_CODE = p_codigo_detalle AND  TBRACCD_TERM_CODE = p_periodo
      ) AND EXISTS (
          SELECT TBBDETC_DESC FROM TBBDETC  WHERE TBBDETC_DETAIL_CODE = p_codigo_detalle
      );
      
      p_error := 'Correcto';
    --

--
    COMMIT;
EXCEPTION
WHEN OTHERS THEN
    p_error := SUBSTR(SQLERRM, 1, 64);
    raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;

----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------
