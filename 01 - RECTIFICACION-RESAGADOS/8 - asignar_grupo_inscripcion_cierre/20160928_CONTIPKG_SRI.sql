create or replace PACKAGE CONTIPKG_SRI AS
/*
 CONTIPKG_SRI:
       Conti Package SOLICITUD RECTIFICACION DE INSCRIPCION
*/
-- FILE NAME..: CONTIPKG_SRI.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: CONTIPKG_SRI
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              

PROCEDURE p_set_coddetalle_alumno (
  p_id_alumno       IN TBRACCD.TBRACCD_PIDM%TYPE, 
  p_periodo         IN TBRACCD.TBRACCD_TERM_CODE%TYPE , 
  p_codigo_detalle  IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
  p_error           OUT varchar2
  );

--------------------------------------------------------------------------------

FUNCTION f_get_sideuda_alumno
              (
                P_PIDM IN TBRACCD.TBRACCD_PIDM%TYPE, -- PIDM ALUMNO
                P_TERM_CODE IN STVTERM.STVTERM_CODE%TYPE,
                P_COD_DETALLE IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE
              ) RETURN VARCHAR2;

--------------------------------------------------------------------------------

PROCEDURE p_validar_fecha_solicitud 
              (
                p_id_alumno         IN SPRIDEN.SPRIDEN_PIDM%TYPE, 
                p_codigo_solicitud  IN SVVSRVC.SVVSRVC_CODE%TYPE,
                p_fecha_valida      OUT varchar2
              );
              
--------------------------------------------------------------------------------

PROCEDURE p_del_coddetalle_alumno 
              (
                  p_id_alumno        IN TBRACCD.TBRACCD_PIDM%TYPE,
                  p_periodo          IN TBRACCD.TBRACCD_TERM_CODE%TYPE,
                  p_codigo_detalle   IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE,
                  p_error            OUT varchar2
              );

--------------------------------------------------------------------------------

PROCEDURE p_cambio_estado_solicitud 
              (
                  -- p_id_alumno           IN SPRIDEN.SPRIDEN_PIDM%TYPE, 
                  p_motivo              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
                  p_numero_solicitud    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
                  p_error               OUT varchar2
              );

--------------------------------------------------------------------------------

PROCEDURE p_set_gr_inscripc_abierto 
              (
                  P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
                  P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE ,
                  P_MODALIDAD_ALUMNO    IN STVDEPT.STVDEPT_CODE%TYPE , 
                  P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE ,
                  P_ERROR               OUT VARCHAR2
              );

--------------------------------------------------------------------------------

PROCEDURE p_set_gr_inscripc_cierre 
              (
                  P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
                  P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE ,
                  P_MODALIDAD_ALUMNO    IN STVDEPT.STVDEPT_CODE%TYPE , 
                  P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE ,
                  P_ERROR               OUT VARCHAR2
              );

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--    

END CONTIPKG_SRI;