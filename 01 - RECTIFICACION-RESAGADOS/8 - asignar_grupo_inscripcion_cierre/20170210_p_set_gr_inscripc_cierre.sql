
/*
drop procedure p_set_gr_inscripc_cierre;
GRANT EXECUTE ON p_set_gr_inscripc_cierre TO wfobjects;
GRANT EXECUTE ON p_set_gr_inscripc_cierre TO wfauto;

set serveroutput on
DECLARE p_error varchar2(64);
begin
  p_set_gr_inscripc_cierre('30691','201610','UREG','S01',p_error);
  DBMS_OUTPUT.PUT_LINE(p_error);
end;

*/
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE p_set_gr_inscripc_cierre (
      P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
      P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE ,
      P_MODALIDAD_ALUMNO    IN STVDEPT.STVDEPT_CODE%TYPE , 
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE ,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : p_set_gr_inscripc_cierre
  FECHA     : 28/09/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : asignar el código de grupo de cierre luego de la rectificación de inscripción a asignaturas en base al id, la modalidad y a la sede del estudiante.

                          Ejemplo: 98-98WF01
------------------------------------------------------------------------------
    Rectificación               Modalidad                 Sede            
98-98 – Rectificación     R – Regular                   S01 – Huancayo
                          W – Gente que trabaja         F01 – Arequipa 
                          V – Virtual                   F02 – Lima  
                                                        F03 – Cusco
                                                        V00 – Virtual

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
AS
      P_RPGRP               SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      P_RPGRP_NEW           SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      P_MODALIDAD_SEDE      VARCHAR2(4);
      P_MODALIDAD_SEDE2     VARCHAR2(4);
      
      P_MESSAGE             EXCEPTION;
      P_MESSAGE2            EXCEPTION;
      P_MESSAGE3            EXCEPTION;
      P_INDICADOR           NUMBER := 0;
      P_ROW_UPDATE          NUMBER := 0;
      P_COD_RECTF           VARCHAR2(15) := '98-98';
BEGIN
--
      
      P_INDICADOR := 1;      
      SELECT SFBRGRP_RGRP_CODE INTO P_RPGRP
      FROM SFBRGRP 
      WHERE SFBRGRP_PIDM = P_ID_ALUMNO
      AND SFBRGRP_TERM_CODE = P_PERIODO;
      
      -- OBTENER CADENA DE MODALIDAD Y SEDE  del CODIGO DE GRUPO  
      P_MODALIDAD_SEDE := SUBSTR(P_RPGRP,6,4);
      
      -- VALIDACION : CODIGO grupo insc. y datos proporcionados
      SELECT CASE P_MODALIDAD_ALUMNO 
                WHEN 'UVIR' THEN 'V' --#UC-SEMI PRESENCIAL (EV)
                WHEN 'UPGT' THEN 'W' --#UC-SEMI PRESENCIAL (GT)
                WHEN 'UREG' THEN 'R' --#UC-PRESENCIAL
                ELSE '' END INTO P_MODALIDAD_SEDE2 FROM DUAL;
            
      IF (P_MODALIDAD_SEDE != (P_MODALIDAD_SEDE2 || P_COD_SEDE )) THEN
              RAISE P_MESSAGE2;
      ELSIF (P_RPGRP = (P_COD_RECTF || P_MODALIDAD_SEDE)) THEN
              RAISE P_MESSAGE3;
      END IF;
    
      
      -- CODIGO DEL GRUPO DE RECTIFICACIÒN : 98-98<cod_modadlidad><cod_sede>
      SELECT CONCAT(P_COD_RECTF,P_MODALIDAD_SEDE) INTO P_RPGRP_NEW FROM DUAL;
      
      -- Validar que exista el codigo del grupo de inscripcion
      P_INDICADOR := 2;    
      SELECT SFBWCTL_RGRP_CODE 
      INTO P_RPGRP_NEW
      FROM SFBWCTL
      WHERE SFBWCTL_RGRP_CODE  = P_RPGRP_NEW
      AND  SFBWCTL_TERM_CODE = P_PERIODO;
      
      -- ASIGNAR GRUPO DE RECTIFICACIÒN
      UPDATE SFBRGRP 
        -- SELECT SFBRGRP_TERM_CODE, SFBRGRP_PIDM, SFBRGRP.* 
        SET SFBRGRP_RGRP_CODE   = P_RPGRP_NEW,
            SFBRGRP_USER        = 'WorkFlow',
            SFBRGRP_ACTIVITY_DATE = SYSDATE
      WHERE SFBRGRP_PIDM = P_ID_ALUMNO
      AND SFBRGRP_TERM_CODE = P_PERIODO
      AND EXISTS (
            ------ LA DEFINICION DE LA VISTA SFVRGRP
            SELECT SFBWCTL.SFBWCTL_TERM_CODE,
              SFBWCTL.SFBWCTL_RGRP_CODE,
              SFBWCTL.SFBWCTL_PRIORITY,
              SFRWCTL.SFRWCTL_BEGIN_DATE,
              SFRWCTL.SFRWCTL_END_DATE,
                    SFRWCTL.SFRWCTL_HOUR_BEGIN,
                    SFRWCTL.SFRWCTL_HOUR_END
              FROM SFRWCTL, SFBWCTL
                    WHERE SFRWCTL.SFRWCTL_TERM_CODE = SFBWCTL.SFBWCTL_TERM_CODE
                    AND   SFRWCTL.SFRWCTL_PRIORITY  = SFBWCTL.SFBWCTL_PRIORITY
                    AND   SFBWCTL.SFBWCTL_RGRP_CODE = P_RPGRP_NEW
                    AND   SFRWCTL.SFRWCTL_TERM_CODE = P_PERIODO
      );
      P_ROW_UPDATE := P_ROW_UPDATE + SQL%ROWCOUNT;
      --DBMS_OUTPUT.PUT_LINE(P_ROW_UPDATE);
      IF (P_ROW_UPDATE > 1) THEN
            ROLLBACK;
            RAISE P_MESSAGE;
      END IF;


      COMMIT;
EXCEPTION
  WHEN P_MESSAGE THEN
          P_ERROR := 'Se detecto que posee DOBLE grupo de inscripcion: ' || P_RPGRP_NEW ||  '. Acerarce a oficinas para solucionar su caso en particular, Gracias.';          
  WHEN P_MESSAGE2 THEN
          P_ERROR := 'El departamento y la sede no coinciden con el grupo de rectificacion';          
  WHEN P_MESSAGE3 THEN
          P_ERROR := 'Advertencia, UD. ya se encuentra en el grupo de inscripcion para su rectificaciòn. No procedio lo solicitado.';          
  WHEN TOO_MANY_ROWS THEN 
          IF (P_INDICADOR = 2) THEN
              P_ERROR := 'Se encontraron mas de un grupo de CIERRE para su inscripcion: ' || P_RPGRP_NEW || ' . Acerarce a oficinas para solucionar su caso en particular, Gracias.';
          ELSIF (P_INDICADOR = 1) THEN
              P_ERROR := 'Se encontraron mas de un grupo ABIERTO para su inscripción: ' || P_RPGRP_NEW || ' . Acerarce a oficinas para solucionar su caso en particular, Gracias.';
          ELSE  P_ERROR := SQLERRM;
          END IF;       
  WHEN NO_DATA_FOUND THEN
           IF (P_INDICADOR = 2) THEN
              P_ERROR := 'No se encontró el codigo del grupo de CIERRE: ' || P_RPGRP_NEW || ' . Acerarce a oficinas para solucionar su caso en particular, Gracias.';
          ELSIF (P_INDICADOR = 1) THEN
              P_ERROR := 'No se encontró el grupo ABIERTO para su inscripción: ' || P_RPGRP_NEW || ' . Acerarce a oficinas para solucionar su caso en particular, Gracias.';
          ELSE  P_ERROR := SQLERRM;
          END IF;  
  WHEN OTHERS THEN
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END p_set_gr_inscripc_cierre;

       
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------