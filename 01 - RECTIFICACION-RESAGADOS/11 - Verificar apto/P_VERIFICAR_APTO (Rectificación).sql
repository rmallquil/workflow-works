/*
SET SERVEROUTPUT ON
DECLARE P_STATUS_APTO VARCHAR2(4000);
 P_REASON VARCHAR2(4000);
BEGIN
    P_VERIFICAR_APTO(247923, '201810', P_STATUS_APTO, P_REASON);
    DBMS_OUTPUT.PUT_LINE(P_STATUS_APTO);
    DBMS_OUTPUT.PUT_LINE(P_REASON);
END;
*/

CREATE OR REPLACE PROCEDURE P_VERIFICAR_APTO (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_STATUS_APTO     OUT VARCHAR2,
        P_REASON          OUT VARCHAR2
)
AS
        V_MATRI             INTEGER := 0; --VALIDA MATRICULA
        V_SOLTRA            INTEGER := 0; --VALIDA SOL. TRASLADO INTERNO
        V_SOLRES            INTEGER := 0; --VALIDA SOL. RESERVA
        V_SOLPER            INTEGER := 0; --VALIDA SOL. PERMANENCIA
        V_SOLCAM            INTEGER := 0; --VALIDA SOL. CAMBIO DE PLAN
        V_SOLCUO            INTEGER := 0; --VALIDA SOL. MODIFICACIÓN DE CUOTA INICIAL
        V_RESPUESTA         VARCHAR2(4000) := NULL;
        V_FLAG              INTEGER := 0;
BEGIN
    ---P_VERIFICAR_APTO PARA SOLICITUD DE RECTIFICACIÓN
    
    --################################################################################################
    -- VERIFICAR QUE YA ESTE MATRICULADO (VERIFICAR QUE TENGA NRC's)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_MATRI 
    FROM SFRSTCR 
    WHERE SFRSTCR_TERM_CODE = P_TERM_CODE
        AND SFRSTCR_RSTS_CODE in ('RW','RE','DD')
        AND SFRSTCR_PIDM = P_PIDM;
        
    IF V_MATRI > 0 THEN 
        V_RESPUESTA := '<br />' || '<br />' || '- Tiene matrícula en el periodo solicitado. ';
        V_FLAG := 1;
    END IF;
    
    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE TRASLADO INTERNO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLTRA
    FROM SVRSVPR1
        WHERE SVRSVPR1_TERM_CODE = P_TERM_CODE
        AND SVRSVPR1_SRVC_CODE = 'SOL013'
        AND SVRSVPR1_SRVS_CODE in ('AC','AP')
        AND SVRSVPR1_PIDM = P_PIDM;
    
    IF V_SOLTRA > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE CAMBIO DE PLAN DE ESTUDIO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLCAM
    FROM SVRSVPR1
        WHERE SVRSVPR1_PIDM = P_PIDM
        AND SVRSVPR1_TERM_CODE = P_TERM_CODE
        AND SVRSVPR1_SRVC_CODE = 'SOL004'
        AND SVRSVPR1_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCAM > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Cambio de Plan de Estudio activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RESERVA DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLRES
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL005'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLRES > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        END IF;
        V_FLAG := 1;
    END IF;
    
    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE PERMANENCIA DE PLAN DE ESTUDIOS ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLPER
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL015'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLPER > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Permanencia de Plan de Estudio activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Permanencia de Plan de Estudio activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE MODIFICACIÓN DE CUOTA INICIAL ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLCUO
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL007'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCUO > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Modificación de cuota inicial activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Modificación de cuota inicial activa. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN FINAL
    --################################################################################################
    IF V_FLAG > 0 THEN
        P_STATUS_APTO := 'FALSE';
        P_REASON := V_RESPUESTA;
    ELSE
        P_STATUS_APTO := 'TRUE';
        P_REASON := 'OK';
    END IF;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VERIFICAR_APTO;