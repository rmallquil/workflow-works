
-------------------------------------------------------------------------------------------------------------

/*
drop procedure p_validar_fecha_solicitud;
GRANT EXECUTE ON p_validar_fecha_solicitud TO wfobjects;
GRANT EXECUTE ON p_validar_fecha_solicitud TO wfauto;

set serveroutput on
DECLARE p_fecha_valida varchar2(64);
begin
  p_validar_fecha_solicitud('30539','SOL003',p_fecha_valida);
  DBMS_OUTPUT.PUT_LINE(p_fecha_valida);
end;

*/



create or replace PROCEDURE p_validar_fecha_solicitud (
  p_id_alumno         IN SPRIDEN.SPRIDEN_PIDM%TYPE, 
  p_codigo_solicitud  IN SVVSRVC.SVVSRVC_CODE%TYPE,
  p_fecha_valida      OUT varchar2
  )
/* ===================================================================================================================
  NOMBRE    : p_validar_fecha_solicitud
  FECHA     : 19/09/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : se verifiqua que solicitud del tipo SOL003, se encuentra dentro del rango de fechas hábiles para atender la solicitud.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */

AS
       
BEGIN
--
    
    p_fecha_valida := 'FALSE';
    
    ----------------------------------------------------------------------------
        SELECT 
                DECODE(COUNT(*),0,'FALSE','TRUE') 
        INTO p_fecha_valida 
        FROM SVRSVPR 
        INNER JOIN (
               SELECT SVRRSRV_SEQ_NO, 
                      SVRRSRV_SRVC_CODE       TEMP_SRVC_CODE, 
                      SVRRSRV_INACTIVE_IND,
                      SVRRSST_RSRV_SEQ_NO , 
                      SVRRSST_SRVC_CODE , 
                      SVRRSST_SRVS_CODE       TEMP_SRVS_CODE,
                      SVRRSRV_END_DATE        TEMP_END_DATE , -- fecha finalizacion
                      SVRRSRV_DELIVERY_DATE , -- fecha estimada
                      SVVSRVS_LOCKED
               FROM SVRRSRV
               INNER JOIN SVRRSST
               ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
               AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
               INNER JOIN SVVSRVS -- total de estados de servicios
               ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
               WHERE SVRRSRV_SRVC_CODE = p_codigo_solicitud 
               AND SVRRSRV_INACTIVE_IND = 'Y' -- Activo
               AND SVVSRVS_LOCKED <> 'L' -- (L)LOCKET , (U)UNLOCKET 
               AND SVVSRVS_CODE IN 'AC' -- (AC)Acitvo , (RE) Rechazado
        )TEMP
        ON SVRSVPR_SRVS_CODE = TEMP_SRVS_CODE
        AND SVRSVPR_SRVC_CODE =  TEMP_SRVC_CODE
        WHERE  TEMP_END_DATE < SYSDATE 
        AND SVRSVPR_PIDM = p_id_alumno;
      
    ----------------------------------------------------------------------------

--
    COMMIT;
EXCEPTION
  WHEN TOO_MANY_ROWS
           THEN DBMS_OUTPUT.PUT_LINE('Se detecto mas de un REGISTRO (curriculm activo o codigos de detalles) para el alumno, por lo que no se completo lo solicitado.');
  WHEN NO_DATA_FOUND
           THEN DBMS_OUTPUT.PUT_LINE ('NO se detectaron REGISTROS (curriculm activo o codigos de detalles) para el alumno, por lo que no se completo lo solicitado.');
  WHEN OTHERS THEN
          raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END p_validar_fecha_solicitud;
