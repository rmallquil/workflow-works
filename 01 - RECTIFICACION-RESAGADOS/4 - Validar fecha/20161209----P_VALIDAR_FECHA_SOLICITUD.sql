/*
drop procedure p_validar_fecha_solicitud;
GRANT EXECUTE ON p_validar_fecha_solicitud TO wfobjects;
GRANT EXECUTE ON p_validar_fecha_solicitud TO wfauto;

set serveroutput on
DECLARE p_fecha_valida varchar2(64);
        p_comentario varchar2(64);
begin
  p_validar_fecha_solicitud('SOL003','52',p_fecha_valida, p_comentario);
  DBMS_OUTPUT.PUT_LINE(p_fecha_valida || ' --- ' || p_comentario );
end;

*/


create or replace PROCEDURE P_VALIDAR_FECHA_SOLICITUD ( 
  P_CODIGO_SOLICITUD    IN SVVSRVC.SVVSRVC_CODE%TYPE,
  P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
  P_FECHA_VALIDA        OUT VARCHAR2,
  P_COMENTARIO_ALUMNO   OUT VARCHAR2
  )
/* ===================================================================================================================
  NOMBRE    : p_validar_fecha_solicitud
  FECHA     : 13/10/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : se verifiqua que solicitud del alumno del tipo <P_CODIGO_SOLICITUD>( ejem SOL003), 
              se encuentra dentro del rango de fechas hábiles para atender la solicitud.

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  01    19/10/2016    rmallqui      Se agrego la obtencion del comentario personalizado de la solictud.
  =================================================================================================================== */

AS
       
BEGIN
--
    
    P_FECHA_VALIDA := 'FALSE';
    
    ----------------------------------------------------------------------------
        SELECT 
                DECODE(COUNT(*),0,'FALSE','TRUE') 
        INTO P_FECHA_VALIDA 
        FROM SVRSVPR 
        INNER JOIN (
              -- get la configuracion (fechas, estados, etc) de la solicitud P_CODIGO_SOLICITUD 
               SELECT SVRRSRV_SEQ_NO, 
                      SVRRSRV_SRVC_CODE       TEMP_SRVC_CODE, 
                      SVRRSRV_INACTIVE_IND,
                      SVRRSST_RSRV_SEQ_NO , 
                      SVRRSST_SRVC_CODE , 
                      SVRRSST_SRVS_CODE       TEMP_SRVS_CODE,
                      SVRRSRV_START_DATE      TEMP_START_DATE,--- fecha finalizacion
                      SVRRSRV_END_DATE        TEMP_END_DATE,----- fecha finalizacion
                      SVRRSRV_DELIVERY_DATE , -- fecha estimada
                      SVVSRVS_LOCKED
               FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
               INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
               ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
               AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
               INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
               ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
               WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD 
               AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activo
               AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 
               AND SVVSRVS_CODE IN 'AC' ------------------------- (AC)Acitvo , (RE) Rechazado
               -- AND SVRRSRV_WEB_IND = 'Y' ------------------------ Si esta disponible en WEB
        )TEMP
        ON SVRSVPR_SRVS_CODE = TEMP_SRVS_CODE
        AND SVRSVPR_SRVC_CODE =  TEMP_SRVC_CODE
        WHERE (TEMP_START_DATE < SYSDATE AND SYSDATE < TEMP_END_DATE)
        AND SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
        
        -- GET COMENTARIO de la solicitud. (comentario PERSONALZIADO.)
        SELECT SVRSVAD_ADDL_DATA_DESC 
        INTO P_COMENTARIO_ALUMNO 
        FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
        INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
        ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
        WHERE SVRSRAD_SRVC_CODE = P_CODIGO_SOLICITUD
        AND UPPER(SVRSRAD_ADDL_DATA_TITLE) = UPPER('Ingrese un comentario')
        AND SVRSVAD_ADDL_DATA_CDE = 'PREGUNTA'
        AND SVRSVAD_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD ;

      
    ----------------------------------------------------------------------------
--
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END P_VALIDAR_FECHA_SOLICITUD;