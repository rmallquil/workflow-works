/*
SET SERVEROUTPUT ON
declare comentario varchar2(1200);
        BEC varchar2(2);
begin
    P_OBTENER_COMENTARIO_ALUMNO(382, '201710',comentario,BEC); -- 381 380
    DBMS_OUTPUT.PUT_LINE(comentario || ' --- ' || BEC);
end;
*/

CREATE or replace PROCEDURE P_OBTENER_COMENTARIO_ALUMNO (
      P_NUMERO_SOLICITUD        IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_PERIODO                 IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
      P_COMENTARIO_ALUMNO       OUT VARCHAR2,
      P_BEC18                   OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_OBTENER_COMENTARIO_ALUMNO
  FECHA     : 03/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento LOS CORREOS deL(os) responsable(s) 
              de Registros Académicos de esa sede.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  001   03/03/2017    RMALLQUI    Se agrego parametro de retorno indicando si el alumno es de BEC18
  =================================================================================================================== */
AS
      P_CODIGO_SOLICITUD      SVVSRVC.SVVSRVC_CODE%TYPE;
      P_PIDM                  SPRIDEN.SPRIDEN_PIDM%TYPE;
BEGIN
      
      -- GET CODIGO SOLICITUD
      SELECT SVRSVPR_SRVC_CODE      , SVRSVPR_PIDM
      INTO P_CODIGO_SOLICITUD       , P_PIDM
      FROM SVRSVPR 
      WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
      
      -- GET COMENTARIO de la solicitud. (comentario PERSONALZIADO.)
      SELECT SVRSVAD_ADDL_DATA_DESC INTO P_COMENTARIO_ALUMNO 
      FROM (
            SELECT SVRSVAD_ADDL_DATA_DESC
            FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
            INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
            ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
            WHERE SVRSRAD_SRVC_CODE = P_CODIGO_SOLICITUD
            AND SVRSVAD_ADDL_DATA_CDE = 'PREGUNTA'
            AND SVRSVAD_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD
            ORDER BY SVRSVAD_ADDL_DATA_SEQ DESC
      ) WHERE ROWNUM = 1;

      -- get si alumno BEC18
      SELECT CASE WHEN DAT = 0 THEN 'No' ELSE 'Si' END  
      INTO P_BEC18 FROM 
      (
            SELECT COUNT(*) DAT FROM SGRSATT 
            WHERE SGRSATT_PIDM = P_PIDM
            AND SGRSATT_ATTS_CODE = 'BE18'
            AND SGRSATT_TERM_CODE_EFF = P_PERIODO
      ) WHERE ROWNUM <= 1;

END P_OBTENER_COMENTARIO_ALUMNO;