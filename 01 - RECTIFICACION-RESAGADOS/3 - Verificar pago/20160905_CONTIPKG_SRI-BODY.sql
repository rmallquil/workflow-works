create or replace PACKAGE BODY CONTIPKG_SRI AS
/*
 CONTIPKG_SRI:
       Conti Package body SOLICITUD RECTIFICACION DE INSCRIPCION
*/
-- FILE NAME..: CONTIPKG_SRI.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: CONTIPKG_SRI
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              


PROCEDURE p_set_coddetalle_alumno (
  p_id_alumno       IN TBRACCD.TBRACCD_PIDM%TYPE, 
  p_periodo         IN TBRACCD.TBRACCD_TERM_CODE%TYPE , 
  p_codigo_detalle  IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
  p_error           OUT varchar2
  )
/* ===================================================================================================================
  NOMBRE    : p_set_coddetalle_alumno
  FECHA     : 24/08/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Inserta un c�digo de detalle a la cuenta corriente del alumno para un periodo correspondiente.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */

AS
        -- p_id_alumno               TBRACCD.TBRACCD_PIDM%TYPE;
        c_codigo_detalle_dec      TBBDETC.TBBDETC_DESC%TYPE;
        c_user                    TBRACCD.TBRACCD_USER%TYPE;
        c_tran_number             TBRACCD.TBRACCD_TRAN_NUMBER%TYPE;
        
        c_alum_nivel              SORLCUR.SORLCUR_LEVL_CODE%TYPE;
        c_alum_campus             SORLCUR.SORLCUR_CAMP_CODE%TYPE;
        c_alum_escuela            SORLCUR.SORLCUR_COLL_CODE%TYPE;
        c_alum_grado              SORLCUR.SORLCUR_DEGC_CODE%TYPE;
        c_alum_programa           SORLCUR.SORLCUR_PROGRAM%TYPE;
        c_alum_perd_adm           SORLCUR.SORLCUR_TERM_CODE_ADMIT%TYPE;
        c_alum_tipo_alum          SORLCUR.SORLCUR_STYP_CODE%TYPE;           -- STVSTYP
        c_alum_tarifa             SORLCUR.SORLCUR_RATE_CODE%TYPE;           -- STVRATE
        c_alum_departamento       SORLFOS.SORLFOS_DEPT_CODE%TYPE;
        
        c_nivel                   SFRRGFE.SFRRGFE_LEVL_CODE%TYPE;           -- STVLEVL
        c_campus                  SFRRGFE.SFRRGFE_CAMP_CODE%TYPE;           -- STVCAMP
        c_escuela                 SFRRGFE.SFRRGFE_COLL_CODE%TYPE;           -- STVCOLL
        c_grado                   SFRRGFE.SFRRGFE_DEGC_CODE%TYPE;           -- STVDEGC
        c_programa                SFRRGFE.SFRRGFE_PROGRAM%TYPE;             -- consultar relacion con SFRRGFE_MAJR_CODE-STVMAJR
        c_perd_adm                SFRRGFE.SFRRGFE_TERM_CODE_ADMIT%TYPE;     -- STVTERM
        --c_curriculums             SFRRGFE.SFRRGFE_PRIM_SEC_CDE%TYPE;
        c_tipo_camp_studio        SFRRGFE.SFRRGFE_LFST_CODE%TYPE;           -- GTVLFST
        c_cod_camp_studio         SFRRGFE.SFRRGFE_MAJR_CODE%TYPE;                   -- STVMAJR
        c_depto                   SFRRGFE.SFRRGFE_DEPT_CODE%TYPE;           -- STVDEPT
        --c_camp_studio             SFRRGFE.SFRRGFE_LFST_PRIM_SEC_CDE%TYPE; 
        c_tipo_alumn_curriculm    SFRRGFE.SFRRGFE_STYP_CODE_CURRIC%TYPE;    -- STVSTYP
        c_trf_curriculm           SFRRGFE.SFRRGFE_RATE_CODE_CURRIC%TYPE;    -- STVRATE
        c_cargo_minimo            SFRRGFE.SFRRGFE_MIN_CHARGE%TYPE;
        
        /* FORMAS */
        c_form                    varchar2(16);
BEGIN
--
    ----------------------------------------------------------------------------
        c_form := 'SGASTDN';
        /* NOTA: probabilidad de encontrar doble curricula activa para el alumno - saldria error */
        SELECT 
            SORLCUR_LEVL_CODE,
            SORLCUR_CAMP_CODE,
            SORLCUR_COLL_CODE,
            SORLCUR_DEGC_CODE,
            SORLCUR_PROGRAM,
            SORLCUR_TERM_CODE_ADMIT,
            --SORLCUR_STYP_CODE,
            SORLCUR_STYP_CODE,
            SORLCUR_RATE_CODE,
            SORLFOS_DEPT_CODE
        INTO 
            c_alum_nivel, c_alum_campus, c_alum_escuela, c_alum_grado, c_alum_programa, c_alum_perd_adm,/*c_alum_tipo_alum,*/
            c_alum_tipo_alum,c_alum_tarifa,c_alum_departamento
        FROM SORLCUR 
        INNER JOIN SORLFOS
          ON SORLCUR_PIDM =  SORLFOS_PIDM AND SORLCUR_SEQNO = SORLFOS.SORLFOS_LCUR_SEQNO
        WHERE SORLCUR_PIDM = p_id_alumno AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
        AND SORLCUR_TERM_CODE = p_periodo AND SORLCUR_CACT_CODE = 'ACTIVE';
    ----------------------------------------------------------------------------
        c_form := 'SFARGFE';
        --DBMS_OUTPUT.PUT_LINE(c_alum_nivel ||' '|| c_alum_campus  ||' '||  c_alum_escuela  ||' '||  c_alum_grado  ||' '||  c_alum_programa  ||' '||  c_alum_perd_adm  ||' '||  c_alum_tipo_alum ||' '|| c_alum_tarifa  ||' - '||  c_alum_departamento);
--        SELECT 
--            SFRRGFE_LEVL_CODE,
--            SFRRGFE_CAMP_CODE,
--            SFRRGFE_COLL_CODE,
--            SFRRGFE_DEGC_CODE,
--            SFRRGFE_PROGRAM,
--            SFRRGFE_TERM_CODE_ADMIT,
--            --SFRRGFE_PRIM_SEC_CDE,
--            SFRRGFE_LFST_CODE,
--            SFRRGFE_MAJR_CODE,
--            SFRRGFE_DEPT_CODE,
--            --SFRRGFE_LFST_PRIM_SEC_CDE,
--            SFRRGFE_STYP_CODE_CURRIC,
--            SFRRGFE_RATE_CODE_CURRIC
--        INTO  c_nivel ,c_campus ,c_escuela ,c_grado ,c_programa ,c_perd_adm /*,c_curriculums*/,c_tipo_camp_studio
--              ,c_cod_camp_studio ,c_depto /*,c_camp_studio ,c_tipo_alumn_curriculm ,c_trf_curriculm */
        SELECT SFRRGFE_MIN_CHARGE INTO c_cargo_minimo
         FROM SFRRGFE 
        WHERE SFRRGFE_TERM_CODE = p_periodo AND SFRRGFE_DETL_CODE = p_codigo_detalle AND SFRRGFE_TYPE = 'STUDENT'
        AND NVL(SFRRGFE_LEVL_CODE, c_alum_nivel) = NVL(c_alum_nivel, SFRRGFE_LEVL_CODE) 
        AND NVL(SFRRGFE_CAMP_CODE, c_alum_campus) = NVL(c_alum_campus, SFRRGFE_CAMP_CODE) 
        AND NVL(SFRRGFE_COLL_CODE, c_alum_escuela) = NVL(c_alum_escuela, SFRRGFE_COLL_CODE) 
        AND NVL(SFRRGFE_DEGC_CODE, c_alum_grado) = NVL(c_alum_grado, SFRRGFE_DEGC_CODE) 
        AND NVL(SFRRGFE_PROGRAM, c_alum_programa) = NVL(c_alum_programa, SFRRGFE_PROGRAM) 
        AND NVL(SFRRGFE_TERM_CODE_ADMIT, c_alum_perd_adm) = NVL(c_alum_perd_adm, SFRRGFE_TERM_CODE_ADMIT) 
        AND NVL(SFRRGFE_DEPT_CODE, c_alum_departamento) = NVL(c_alum_departamento, SFRRGFE_DEPT_CODE)
        AND NVL(SFRRGFE_STYP_CODE_CURRIC, c_alum_tipo_alum) = NVL(c_alum_tipo_alum, SFRRGFE_STYP_CODE_CURRIC)
        AND NVL(SFRRGFE_RATE_CODE_CURRIC, c_alum_tarifa) = NVL(c_alum_tarifa, SFRRGFE_RATE_CODE_CURRIC);
    ----------------------------------------------------------------------------
      
      SELECT TBBDETC_DESC INTO c_codigo_detalle_dec  FROM TBBDETC  WHERE TBBDETC_DETAIL_CODE = p_codigo_detalle;
      SELECT user INTO c_user FROM dual;
      SELECT TBRACCD_TRAN_NUMBER + 1 INTO c_tran_number FROM (
                        SELECT TBRACCD_TRAN_NUMBER FROM TBRACCD
                        WHERE TBRACCD_PIDM = p_id_alumno ORDER BY TBRACCD_TRAN_NUMBER DESC
                      )WHERE ROWNUM<= 1;
    
    ----------------------------------------------------------------------------
      INSERT INTO TBRACCD (
        TBRACCD_PIDM ,TBRACCD_TRAN_NUMBER ,TBRACCD_TERM_CODE ,TBRACCD_DETAIL_CODE ,TBRACCD_USER ,TBRACCD_ENTRY_DATE,
        TBRACCD_AMOUNT ,TBRACCD_BALANCE ,TBRACCD_EFFECTIVE_DATE ,TBRACCD_DESC ,TBRACCD_SRCE_CODE,TBRACCD_ACCT_FEED_IND,
        TBRACCD_ACTIVITY_DATE ,TBRACCD_SESSION_NUMBER,TBRACCD_TRANS_DATE
      )
      SELECT
            p_id_alumno ,c_tran_number ,p_periodo ,p_codigo_detalle ,c_user ,SYSDATE ,c_cargo_minimo,0 ,SYSDATE ,c_codigo_detalle_dec ,'T' ,'Y' ,SYSDATE,0,SYSDATE
      FROM DUAL
      WHERE EXISTS (
          --SELECT TBBDETC_DESC FROM TBBDETC  WHERE TBBDETC_DETAIL_CODE = p_codigo_detalle
           SELECT SFRRGFE_MIN_CHARGE 
             FROM SFRRGFE 
            WHERE SFRRGFE_TERM_CODE = p_periodo AND SFRRGFE_DETL_CODE = p_codigo_detalle AND SFRRGFE_TYPE = 'STUDENT'
            AND NVL(SFRRGFE_LEVL_CODE, c_alum_nivel) = NVL(c_alum_nivel, SFRRGFE_LEVL_CODE) 
            AND NVL(SFRRGFE_CAMP_CODE, c_alum_campus) = NVL(c_alum_campus, SFRRGFE_CAMP_CODE) 
            AND NVL(SFRRGFE_COLL_CODE, c_alum_escuela) = NVL(c_alum_escuela, SFRRGFE_COLL_CODE) 
            AND NVL(SFRRGFE_DEGC_CODE, c_alum_grado) = NVL(c_alum_grado, SFRRGFE_DEGC_CODE) 
            AND NVL(SFRRGFE_PROGRAM, c_alum_programa) = NVL(c_alum_programa, SFRRGFE_PROGRAM) 
            AND NVL(SFRRGFE_TERM_CODE_ADMIT, c_alum_perd_adm) = NVL(c_alum_perd_adm, SFRRGFE_TERM_CODE_ADMIT) 
            AND NVL(SFRRGFE_DEPT_CODE, c_alum_departamento) = NVL(c_alum_departamento, SFRRGFE_DEPT_CODE)
            AND NVL(SFRRGFE_STYP_CODE_CURRIC, c_alum_tipo_alum) = NVL(c_alum_tipo_alum, SFRRGFE_STYP_CODE_CURRIC)
            AND NVL(SFRRGFE_RATE_CODE_CURRIC, c_alum_tarifa) = NVL(c_alum_tarifa, SFRRGFE_RATE_CODE_CURRIC)
      );
      
      p_error := 'Correcto';
    ----------------------------------------------------------------------------

--
    COMMIT;
EXCEPTION
  WHEN TOO_MANY_ROWS
           THEN DBMS_OUTPUT.PUT_LINE('Se detecto mas de un REGISTRO (curriculm activo o codigos de detalles) para el alumno, por lo que no se completo lo solicitado. : ' || c_form );
  WHEN NO_DATA_FOUND
           THEN DBMS_OUTPUT.PUT_LINE ('NO se detectaron REGISTROS (curriculm activo o codigos de detalles) para el alumno, por lo que no se completo lo solicitado. : '  || c_form );
  WHEN OTHERS THEN
          p_error := SUBSTR(SQLERRM, 1, 64);
          raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;


--****************************************************************************--
--****************************************************************************--


FUNCTION f_get_sideuda_alumno
							(
								P_PIDM IN TBRACCD.TBRACCD_PIDM%TYPE, -- PIDM ALUMNO
								P_TERM_CODE IN STVTERM.STVTERM_CODE%TYPE,
                P_COD_DETALLE IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE
							) RETURN BOOLEAN

/* ===================================================================================================================
	NOMBRE		: f_get_sideuda_alumno
	FECHA		  : 05//09/16
	AUTOR		  : Mallqui Lopez, Richard Alfonso
	OBJETIVO	: Verifica la existencia de deuda en la cuenta corriente del alumno para un codigo detalle y  
              periodo correspondiente

	MODIFICACIONES
	NRO 	FECHA		USUARIO		MODIFICACION

	=================================================================================================================== */

AS

    P_DIVISA        GUBINST.GUBINST_BASE_CURR_CODE%TYPE := 'PEN'; -- TIPO MONEDA 'PEN' - DOLAR 
    C_ROWS_FOUND    NUMBER;
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
   v_PIDM                NUMBER  (8);
   v_TERM_CODE           VARCHAR2(6);
   v_DETAIL_CODE         VARCHAR2(4);
   v_prin_detail_code    VARCHAR2(4);
   v_int_detail_code     VARCHAR2(4);
   v_ENTRY_DATE          DATE; -- FECHA DE GENERACI�N DE DEUDA
   v_EFFECTIVE_DATE      DATE; -- FECHA EN LA QUE DEBER�A PAGAR EL CONCEPTO
   v_ACTIVITY_DATE       DATE;
   v_TRANS_DATE          DATE;  
   v_DIAS_VENCIDOS       NUMBER ;
   v_TRAN_NUMBER_P       NUMBER(8);
   v_TRAN_NUMBER_I       NUMBER(8);
   v_TRAN_NUMBER_MAX     NUMBER(8);
   v_AMOUNT_P            NUMBER(16,2);
   v_AMOUNT_I            NUMBER(16,2);  
   v_AMOUNT_TOTAL        NUMBER(16,2);
   v_MULTA               NUMBER(16,2);
   v_COBRANZA            NUMBER(16,2);
   v_TRAN_ORIGINAL       NUMBER(8);
   v_REF_NUMBER          NUMBER(8);
   v_INSTALLMENT_PLAN    VARCHAR2(2);  
   v_tiene_cargo_multa   varchar2(1); 
   v_BaseCurr            GUBINST.GUBINST_BASE_CURR_CODE%TYPE;
   v_convrate            GVRCURR.GVRCURR_CONV_RATE%TYPE;
   v_rate_eff_date       GVRCURR.GVRCURR_RATE_EFF_DATE%TYPE;
   DUMMY_TAZA_CAMBIO       NUMBER(16,2);
   v_AMOUNT_CAPITAL_DIV2   NUMBER(16,2);
   v_AMOUNT_INTERESES_DIV2 NUMBER(16,2);
   v_AMOUNT_TOTAL_DIV2     NUMBER(16,2);
   v_TERM_CODE_ORG         VARCHAR2(6);          -- 6.2 [MC:1.6] EANC 11/MAR/2008
   INSTALLMENT_PLAN    TBRISTL.TBRISTL_INSTALLMENT_PLAN%TYPE;
   REF_NUMBER  TBRISTL.TBRISTL_REF_NUMBER%TYPE;
   intanual          NUMBER(8);
   v_total_multa     NUMBER (8);
   interes_val       VARCHAR2(30);
   multa_val         VARCHAR2(30);
   v_Gasto_Cobranza  NUMBER (8);
   

-----------------------------------------------------------------------------------


      CURSOR PTI_PLAN_ALUMNO_C IS    
         SELECT TBRISTL_INSTALLMENT_PLAN,
                TBRISTL_PRIN_DETAIL_CODE ,
                TBRISTL_INT_DETAIL_CODE  ,
                TBRISTL_REF_NUMBER                    -- 01-FEB-2008
           FROM tbristl
          WHERE tbristl_pidm      = v_PIDM ;

      -- FECHAS DE CARGO
      CURSOR PTI_FECHA_CARGOS_C IS
          SELECT UNIQUE TBRACCD_EFFECTIVE_DATE ,             -- 6.2 [MC:1.20] EANC 18-JUN-2008
                TBRACCD_TRANS_DATE     ,
                TBRACCD_TERM_CODE                           -- 6.2 [MC:1.6] EANC 11/MAR/2008
          FROM TBRACCD ,
                TBBDETC                                     -- 6.2 [MC:1.20] EANC 18-JUN-2008
          WHERE TBRACCD_PIDM        = v_PIDM
            AND TBRACCD_BALANCE    > 0 -- CAMBIO REALIZADO POR FHUAMANA - WS ASCBANC || ORGINAL <> 0
            AND TBRACCD_SRCE_CODE   = 'I'
            AND TBRACCD_CROSSREF_NUMBER = v_REF_NUMBER    -- 01-FEB-2008
            AND tbraccd_tran_number_paid is null          -- 6.2 [MC:1.2] EANC 11/FEB/2008
            AND TBRACCD_MERCHANT_ID IS NULL       -- 7.3 [LASC:7.3]
            AND tbbdetc_detail_code = TBRACCD_DETAIL_CODE 
            AND tbbdetc_TYPE_IND = 'C' -- son cargos
            AND ( 
                  TBRACCD_DETAIL_CODE IN (  SELECT TVRDCTX_DETC_CODE
                                            FROM TVRDCTX
                                            WHERE TVRDCTX_DETC_CODE = TBRACCD_DETAIL_CODE
                                              AND TVRDCTX_CURR_CODE = P_DIVISA
                                            )
                  OR (TBRACCD_DETAIL_CODE NOT IN ( SELECT TVRDCTX_DETC_CODE
                                                   FROM TVRDCTX
                                                   WHERE TVRDCTX_DETC_CODE = TBRACCD_DETAIL_CODE
                                                     AND TVRDCTX_CURR_CODE != P_DIVISA
                                                      )
                       AND P_DIVISA = ( SELECT GUBINST_BASE_CURR_CODE
                                                   FROM GUBINST
                                               )
                     )
                );
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
      /*NO SON CARGOS DE INSENMENT PLANT*/
      CURSOR PTI_CARGOS_NO_I_C IS
         SELECT TBRACCD_DETAIL_CODE    ,
                TBRACCD_TRAN_NUMBER    ,
                TBRACCD_ENTRY_DATE     ,
                TBRACCD_BALANCE        ,--------------------------------
                TBRACCD_EFFECTIVE_DATE ,
                TBRACCD_TERM_CODE                        -- 6.2 [MC:1.6] EANC 11/MAR/2008
           FROM TBRACCD ,
                TBBDETC
          WHERE TBRACCD_PIDM        = v_PIDM
            AND TBRACCD_BALANCE    > 0 -- CAMBIO REALIZADO POR FHUAMANA - WS ASCBANC || ORGINAL <> 0
            AND TBRACCD_SRCE_CODE  <> 'I'
            AND TBBDETC_DETAIL_CODE = TBRACCD_DETAIL_CODE -- C�DIGO DE CARGO
            AND tbbdetc_type_ind    = 'C'
            AND tbraccd_tran_number_paid is null         -- 6.2 [MC:1.2] EANC 11/FEB/2008
            AND TBRACCD_MERCHANT_ID IS NULL       -- 7.3 [LASC:7.3]
            AND (    
                  TBRACCD_DETAIL_CODE IN ( SELECT TVRDCTX_DETC_CODE
                                                FROM TVRDCTX
                                               WHERE TVRDCTX_DETC_CODE = TBRACCD_DETAIL_CODE
                                                 AND TVRDCTX_CURR_CODE = P_DIVISA
                                            )
                  OR (     TBRACCD_DETAIL_CODE NOT IN ( SELECT TVRDCTX_DETC_CODE
                                                          FROM TVRDCTX
                                                         WHERE TVRDCTX_DETC_CODE = TBRACCD_DETAIL_CODE
                                                           AND TVRDCTX_CURR_CODE != P_DIVISA
                                                      )
                       AND P_DIVISA = ( SELECT GUBINST_BASE_CURR_CODE
                                                   FROM GUBINST
                                               )
                     )
                );
--------------------------------------------------------------------------------
    /*IMPLEMENTADO EL PLAN DE PAGOS*/
--------------------------------------------------------------------------------
      CURSOR PTI_TRAN_NUM_P_C IS
         SELECT TBRACCD_TRAN_NUMBER ,
                TBRACCD_BALANCE -------------------------------------------
           FROM TBRACCD
          WHERE TBRACCD_PIDM            = v_PIDM
            AND TBRACCD_BALANCE        <> 0
            AND TBRACCD_EFFECTIVE_DATE  = v_EFFECTIVE_DATE
            AND TBRACCD_TRANS_DATE      = v_TRANS_DATE   -- 6.2 [MC:1.19]
            AND TBRACCD_SRCE_CODE       = 'I'
            AND TBRACCD_CROSSREF_NUMBER = v_REF_NUMBER    -- 6.2 [MC:1.16]
            AND TBRACCD_DETAIL_CODE     = v_prin_detail_code
            AND tbraccd_tran_number_paid is null       -- 6.2 [MC:1.2] EANC 11/FEB/2008
            AND TBRACCD_MERCHANT_ID IS NULL       -- 7.3 [LASC:7.3]
            AND (    TBRACCD_DETAIL_CODE IN ( SELECT TVRDCTX_DETC_CODE
                                              FROM TVRDCTX
                                             WHERE TVRDCTX_DETC_CODE = TBRACCD_DETAIL_CODE
                                             AND TVRDCTX_CURR_CODE = P_DIVISA
                                            )

                  OR (     TBRACCD_DETAIL_CODE NOT IN ( SELECT TVRDCTX_DETC_CODE
                                                          FROM TVRDCTX
                                                         WHERE TVRDCTX_DETC_CODE = TBRACCD_DETAIL_CODE
                                                           AND TVRDCTX_CURR_CODE != P_DIVISA
                                                      )
                       AND P_DIVISA = ( SELECT GUBINST_BASE_CURR_CODE
                                                  FROM GUBINST
                                               )
                     )
                );
-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------
      CURSOR PTI_TRAN_NUM_I_C IS
       SELECT TBRACCD_TRAN_NUMBER ,
                TBRACCD_BALANCE --------------------------------
           FROM TBRACCD
          WHERE TBRACCD_PIDM            = v_PIDM
            AND TBRACCD_BALANCE        <> 0
            AND TBRACCD_EFFECTIVE_DATE  = v_EFFECTIVE_DATE
            AND TBRACCD_TRANS_DATE      = v_TRANS_DATE   -- 6.2 [MC:1.19]           
            AND TBRACCD_SRCE_CODE       = 'I'
            AND TBRACCD_DETAIL_CODE     = v_int_detail_code
            AND TBRACCD_CROSSREF_NUMBER = v_REF_NUMBER    -- 6.2 [MC:1.16]           
            AND tbraccd_tran_number_paid is null       -- 6.2 [MC:1.2] EANC 11/FEB/2008
            AND TBRACCD_MERCHANT_ID IS NULL       -- 7.3 [LASC:7.3]
            AND (    TBRACCD_DETAIL_CODE IN ( SELECT TVRDCTX_DETC_CODE
                                                FROM TVRDCTX
                                               WHERE TVRDCTX_DETC_CODE = TBRACCD_DETAIL_CODE
                                                 AND TVRDCTX_CURR_CODE = P_DIVISA
                                            )
                  OR (     TBRACCD_DETAIL_CODE NOT IN ( SELECT TVRDCTX_DETC_CODE
                                                          FROM TVRDCTX
                                                         WHERE TVRDCTX_DETC_CODE = TBRACCD_DETAIL_CODE
                                                           AND TVRDCTX_CURR_CODE != P_DIVISA
                                                      )
                       AND P_DIVISA = ( SELECT GUBINST_BASE_CURR_CODE
                                                   FROM GUBINST
                                               )
                     )
                );
----------------------------------------------------------------------------
/*VERIFICAR SI APLICA*/
----------------------------------------------------------------------------
      CURSOR PTI_MULTA_NO_I_C IS
         SELECT 'Y'
           FROM TBBDETC -- C�DIGOS DE DETALLE
          WHERE TBBDETC_DETAIL_CODE = V_DETAIL_CODE
            AND TBBDETC_TAXT_CODE   = ( SELECT GTVSDAX_EXTERNAL_CODE
                                          FROM GTVSDAX
                                         WHERE GTVSDAX_INTERNAL_CODE_GROUP = 'ARSYSCHILE'
                                           AND GTVSDAX_INTERNAL_CODE = 'CMNOINST');
--------------------------------------------------------------------------------
/*DEFINE LA MONEDA BASE*/
--------------------------------------------------------------------------------
      CURSOR GET_BASE_CURR_C IS
       SELECT GUBINST_BASE_CURR_CODE
       FROM GUBINST; 
--------------------------------------------------------------------------------
/*
TASA DE CONVERSI�N  
  DE MONEDA 1 A MONEDA 2
*/
--------------------------------------------------------------------------------
      CURSOR GET_CONV_RATE_C IS
        SELECT GVRCURR_CONV_RATE,
            GVRCURR_RATE_EFF_DATE
         FROM GVRCURR
        WHERE GVRCURR_CURR_CODE = P_DIVISA
            AND TRUNC(GVRCURR_RATE_EFF_DATE  ) <= TRUNC(sysdate) 
            AND TRUNC(GVRCURR_RATE_NCHG_DATE ) >  TRUNC(sysdate) ;
--------------------------------------------------------------------------------
/*CUENTA LA CANTIDAD DE D�AS PASADOS A LA FECHA DE PAGO*/
--------------------------------------------------------------------------------
      CURSOR GET_DIAS_VENCIDOS_C IS
        SELECT TRUNC (SYSDATE) - TRUNC (v_EFFECTIVE_DATE)
         FROM DUAL ;
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
      CURSOR GET_MAX_TRAN_NUMBER_C IS           
         SELECT NVL (  MAX ( TZRCDAB_TRAN_NUMBER ) ,  0 )
           FROM TZRCDAB
          WHERE TZRCDAB_PIDM = v_PIDM;          
BEGIN
     
     ----BORRANDO LOS DATOS DEL ALUMNO POR SESSION
     DELETE FROM TZRCDAB 
            WHERE TZRCDAB_PIDM       = P_PIDM
              AND TZRCDAB_SESSION_ID = USERENV('SESSIONID'); 
      
     v_PIDM       := P_PIDM ;
     v_TERM_CODE  := P_TERM_CODE ;
     -- Obtiene la moneda base definida en el sistema
     OPEN GET_BASE_CURR_C;
     FETCH GET_BASE_CURR_C INTO v_BaseCurr;
     CLOSE GET_BASE_CURR_C;
     IF P_DIVISA <> v_BaseCurr THEN
        OPEN GET_CONV_RATE_C ;
        FETCH GET_CONV_RATE_C  INTO v_convrate, v_rate_eff_date;
        CLOSE GET_CONV_RATE_C;      
        DUMMY_TAZA_CAMBIO := v_convrate ;
     END IF;
     OPEN  PTI_PLAN_ALUMNO_C ;
     LOOP  -- BUSCA LOS PLANES DEL ALUMNO
        FETCH PTI_PLAN_ALUMNO_C INTO v_INSTALLMENT_PLAN,
                                     v_prin_detail_code,
                                     v_int_detail_code ,
                                     v_REF_NUMBER      ;  -- 01-FEB-2008
        IF PTI_PLAN_ALUMNO_C%FOUND THEN
           INSTALLMENT_PLAN := v_INSTALLMENT_PLAN ;
           REF_NUMBER       := v_REF_NUMBER;
           OPEN GET_MAX_TRAN_NUMBER_C;
           FETCH GET_MAX_TRAN_NUMBER_C INTO v_TRAN_NUMBER_MAX ;
           CLOSE GET_MAX_TRAN_NUMBER_C;                
           OPEN PTI_FECHA_CARGOS_C ;
           LOOP
              --Se obtienen las fechas del registro del codigo PRINCIPAL
              FETCH PTI_FECHA_CARGOS_C INTO v_EFFECTIVE_DATE ,
                                            v_TRANS_DATE     ,
                                            v_TERM_CODE_ORG  ;     
              IF PTI_FECHA_CARGOS_C%FOUND THEN
                 v_TRAN_NUMBER_MAX := v_TRAN_NUMBER_MAX  + 1 ;
                 --Se obtiene la cantidad y el tran number del codigo PRINCIPAL
                 OPEN  PTI_TRAN_NUM_P_C;
                 FETCH PTI_TRAN_NUM_P_C INTO v_TRAN_NUMBER_P, v_AMOUNT_P ;
                 IF PTI_TRAN_NUM_P_C%NOTFOUND THEN
                    v_TRAN_NUMBER_P := null;
                    v_AMOUNT_P      := 0;
                 END IF; 
                 CLOSE PTI_TRAN_NUM_P_C;  
                 --Se obtiene la cantidad y el tran number del codigo de INTERESES
                 OPEN  PTI_TRAN_NUM_I_C;
                 FETCH PTI_TRAN_NUM_I_C INTO v_TRAN_NUMBER_I, v_AMOUNT_I ;
                 IF PTI_TRAN_NUM_I_C%NOTFOUND THEN
                    v_TRAN_NUMBER_I := null;
                    v_AMOUNT_I      := 0;
                 END IF;
                 CLOSE PTI_TRAN_NUM_I_C;
                 v_AMOUNT_TOTAL := v_AMOUNT_P + v_AMOUNT_I ;
                 v_MULTA := 0;
                 v_COBRANZA := 0;  
                 IF P_DIVISA <> v_BaseCurr THEN
                    v_AMOUNT_TOTAL_DIV2 := v_AMOUNT_TOTAL ;
                    v_AMOUNT_TOTAL      := v_AMOUNT_TOTAL * v_convrate ;
                 END IF;
                 OPEN GET_DIAS_VENCIDOS_C;
                 FETCH GET_DIAS_VENCIDOS_C INTO v_DIAS_VENCIDOS;
                 CLOSE GET_DIAS_VENCIDOS_C;    
                 IF v_DIAS_VENCIDOS > 0 THEN
                      /*
                        CALCULANDO LA MORA
                        
                        EN LUGAR DE USAR LA FUNCION: 
                          v_MULTA    := TVKCLML.F_CALCMULTA  ( v_AMOUNT_TOTAL , interes_val , sysdate , v_DIAS_VENCIDOS );--
                          ESTO POR QUE COMPARA:
                                        COLOMBIATYPE CON TIPOCOLOMBIA LO CUAL GENERA INCONGRUENCIAS
                      */                 
                      interes_val := gokeacc.f_getgtvsdaxextcode ('INTERES','DOCSVENTA'); 
                        IF NVL(interes_val, '0') IN ('0', 'ACTUALIZAME') THEN
                          intanual := 0;
                        ELSE
                          intanual := TO_NUMBER(interes_val);
                        END IF;
                        IF intanual = 0 THEN
                          v_total_multa := 0;
                        ELSE
                          v_total_multa := v_AMOUNT_TOTAL * (intanual/100/360) * v_DIAS_VENCIDOS;
                        END IF ;
                    
                    v_MULTA := v_total_multa;
                    
                    /*
                        CALCULANDO LA MULTA
                        EN LUGAR DE USAR LA FUNCION: 
                          v_MULTA    := TVKCLML.F_CALCMULTA  ( v_AMOUNT_TOTAL , interes_val , sysdate , v_DIAS_VENCIDOS );--
                          ESTO POR QUE COMPARA:
                          COLOMBIATYPE CON TIPOCOLOMBIA LO CUAL GENERA INCONGRUENCIAS
                      */                 
                          multa_val := gokeacc.f_getgtvsdaxextcode ('MULTA','DOCSVENTA');
                          IF NVL(multa_val, '0') IN ('0', 'ACTUALIZAME') THEN
                            v_Gasto_Cobranza := 0;
                          ELSE
                             v_Gasto_Cobranza := TO_NUMBER(multa_val);
                          END IF;
                          v_COBRANZA:= ROUND(v_Gasto_Cobranza, tvkrpts.f_get_currency_dec( tvkrpts.f_get_curr_currency ) ) ;
                          
                    
                    
                    
                 END IF;          
               IF (v_AMOUNT_TOTAL > 0) THEN   -- 7.3 [LASC:7.3]
                -- SE MUESTRA EN LA FORMA TVACAJA
                 INSERT INTO TZRCDAB ( TZRCDAB_SESSION_ID       , TZRCDAB_PIDM             , TZRCDAB_TRAN_NUMBER      ,
                                       TZRCDAB_TERM_CODE        , TZRCDAB_DETAIL_CODE      , TZRCDAB_AMOUNT           ,
                                       TZRCDAB_EFFECTIVE_DATE   , TZRCDAB_MULTA_MORA       , TZRCDAB_COBRANZA         ,
                                       TZRCDAB_CHECK_PAGO       , TZRCDAB_CAPITAL          , TZRCDAB_INTERESES        ,
                                       TZRCDAB_TRAN_NUMBER_PRIN , TZRCDAB_TRAN_NUMBER_INIT , TZRCDAB_DOCUMENT_NUMBER  ,
                                       TZRCDAB_BANCO            , TZRCDAB_PLAZA            , TZRCDAB_CUENTA           ,
                                       TZRCDAB_TRAN_MULTA       , TZRCDAB_TRAN_COBRANZA    , TZRCDAB_TRAN_PAGO        ,
                                       TZRCDAB_USER             , TZRCDAB_ACTIVITY_DATE    , TZRCDAB_DETAIL_CODE_ORG  ,
                                       TZRCDAB_DIV2_AMOUNT
                                     )

                              VALUES ( USERENV('SESSIONID') , v_PIDM            , v_TRAN_NUMBER_MAX ,
                                       v_TERM_CODE_ORG      , 'PAGO'            , v_AMOUNT_TOTAL    ,
                                       v_EFFECTIVE_DATE     , v_MULTA           , v_COBRANZA        ,
                                       'N'                  , v_AMOUNT_P        , v_AMOUNT_I        ,
                                       v_TRAN_NUMBER_P      ,  v_TRAN_NUMBER_I  , NULL              ,
                                       NULL                 , NULL              , NULL              ,
                                       NULL                 , NULL              , NULL              ,
                                       'ASCBANC'            , sysdate           , v_prin_detail_code,
                                       v_AMOUNT_TOTAL_DIV2
                                     ) ;
               END IF; -- 7.3 [LASC:7.3]                                    
              ELSE
                 EXIT;
              END IF ;
           END LOOP; -- TERMINA LOOP DE FECHAS
           CLOSE PTI_FECHA_CARGOS_C ;
           ELSE
           EXIT ; -- CLOSE PTI_PLAN_ALUMNO_C ;
        END IF; -- TERMINA IF PTI_PLAN_ALUMNO_C
     END LOOP; -- TERMINA LOOP PTI_PLAN_ALUMNO_C
     CLOSE PTI_PLAN_ALUMNO_C ;    
     v_AMOUNT_P := 0;
     v_AMOUNT_I := 0;
     v_MULTA := 0;
     v_COBRANZA := 0;
     OPEN PTI_CARGOS_NO_I_C;
     LOOP
        --Se obtienen los registros con adeudo que no sean I
        FETCH PTI_CARGOS_NO_I_C INTO v_DETAIL_CODE    ,
                                     v_TRAN_ORIGINAL  ,
                                     v_ENTRY_DATE     ,
                                     v_AMOUNT_TOTAL   ,
                                     v_EFFECTIVE_DATE ,
                                     v_TERM_CODE_ORG  ;
        IF PTI_CARGOS_NO_I_C%FOUND THEN
           IF P_DIVISA <> v_BaseCurr THEN
              v_AMOUNT_TOTAL_DIV2     := v_AMOUNT_TOTAL ;
              v_AMOUNT_CAPITAL_DIV2   := v_AMOUNT_P ;
              v_AMOUNT_INTERESES_DIV2 := v_AMOUNT_I ;
              v_AMOUNT_TOTAL  := v_AMOUNT_TOTAL * v_convrate ;
           END IF;
           OPEN PTI_MULTA_NO_I_C ;
           FETCH PTI_MULTA_NO_I_C into v_tiene_cargo_multa;
           IF PTI_MULTA_NO_I_C%FOUND THEN
              v_MULTA := 0;
              v_COBRANZA := 0;
           OPEN GET_DIAS_VENCIDOS_C;
           FETCH GET_DIAS_VENCIDOS_C INTO v_DIAS_VENCIDOS;
           CLOSE GET_DIAS_VENCIDOS_C;                
           IF v_DIAS_VENCIDOS > 0 THEN
                 v_MULTA    := TVKCLML.F_CALCMULTA  ( v_AMOUNT_TOTAL , v_AMOUNT_TOTAL , sysdate , v_DIAS_VENCIDOS );
                 v_COBRANZA := TVKCLML.F_CALCGASTOS ( v_AMOUNT_TOTAL , v_AMOUNT_TOTAL , sysdate , v_DIAS_VENCIDOS );
              END IF;
           ELSE
              v_MULTA := 0;
              v_COBRANZA := 0;
           END IF ;
           CLOSE PTI_MULTA_NO_I_C ;
           OPEN GET_MAX_TRAN_NUMBER_C;
           FETCH GET_MAX_TRAN_NUMBER_C INTO v_TRAN_NUMBER_MAX ;
           CLOSE GET_MAX_TRAN_NUMBER_C;                

           v_TRAN_NUMBER_MAX := v_TRAN_NUMBER_MAX  + 1 ;
           IF (v_AMOUNT_TOTAL > 0) THEN   -- 7.3 [LASC:7.3]
             INSERT INTO TZRCDAB ( TZRCDAB_SESSION_ID       , TZRCDAB_PIDM             , TZRCDAB_TRAN_NUMBER      ,
                                 TZRCDAB_TERM_CODE        , TZRCDAB_DETAIL_CODE      , TZRCDAB_AMOUNT           ,
                                 TZRCDAB_MULTA_MORA       , TZRCDAB_COBRANZA         , TZRCDAB_TRAN_ORIGINAL    ,
                                 TZRCDAB_EFFECTIVE_DATE   , TZRCDAB_USER             , TZRCDAB_ACTIVITY_DATE    ,
                                 TZRCDAB_TRAN_NUMBER_PRIN , TZRCDAB_DETAIL_CODE_ORG  , TZRCDAB_DIV2_AMOUNT,
                                 TZRCDAB_CAPITAL          , TZRCDAB_INTERESES
                               )
                        VALUES ( USERENV('SESSIONID')  , v_PIDM            , v_TRAN_NUMBER_MAX ,
                                 v_TERM_CODE_ORG       , v_DETAIL_CODE     , v_AMOUNT_TOTAL    ,
                                 v_MULTA               , v_COBRANZA        , v_TRAN_ORIGINAL   ,
                                 v_EFFECTIVE_DATE      , 'ASCBANC'         , sysdate,
                                 v_TRAN_ORIGINAL       , v_DETAIL_CODE     ,v_AMOUNT_TOTAL_DIV2,
                                 v_AMOUNT_CAPITAL_DIV2 , v_AMOUNT_INTERESES_DIV2
                               ) ;
          END IF;  -- 7.3 [LASC:7.3]                           
        ELSE
           EXIT;
        END IF ;
     END LOOP; -- TERMINA LOOP DE CARGOS NO I
     CLOSE PTI_CARGOS_NO_I_C;
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    
      SELECT COUNT(*)
      INTO   C_ROWS_FOUND
      FROM   TZRCDAB
      WHERE  TZRCDAB_TERM_CODE = P_TERM_CODE AND TZRCDAB_PIDM = P_PIDM 
      AND TZRCDAB_DETAIL_CODE = P_COD_DETALLE AND ROWNUM <= 1;
    
      IF C_ROWS_FOUND = 0 THEN
        RETURN(FALSE);
        --DBMS_OUTPUT.PUT_LINE('FALSE');
      ELSE
        RETURN(TRUE);
        --DBMS_OUTPUT.PUT_LINE('TRUE');
      END IF;

COMMIT;
END ;


--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END CONTIPKG_SRI;

