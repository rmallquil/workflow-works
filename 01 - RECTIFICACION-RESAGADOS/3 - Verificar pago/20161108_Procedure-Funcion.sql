
-- prueba FUNCION
SET SERVEROUTPUT ON
DECLARE BOOL BOOLEAN;
BEGIN
        BOOL :=  F_GET_SIDEUDA_ALUMNO(325180,'3');
        DBMS_OUTPUT.PUT_LINE(CASE WHEN BOOL = TRUE THEN 'TRUE' ELSE 'FALSE' END);
END;

----------------------------------------------------------------------------------------------------------------------
create or replace FUNCTION F_GET_SIDEUDA_ALUMNO
              (
                P_ID_ALUMNO           SPRIDEN.SPRIDEN_ID%TYPE,
                P_TRAN_NUMBER         TBRACCD.TBRACCD_TRAN_NUMBER%TYPE
              ) RETURN BOOLEAN 
              
/* ===================================================================================================================
  NOMBRE    : F_GET_SIDEUDA_ALUMNO
  FECHA     : 08//11/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : El objetivo es que en base a un ID y un código de detalle verifique la existencia de deuda generada por 
              la solicitud del alumno para el periodo correspondiente, divisa PEN.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  
  =================================================================================================================== */
              
AS
      PRAGMA AUTONOMOUS_TRANSACTION;
      P_INDICADOR        NUMBER := 0;
BEGIN
--
    -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
    TZKCDAA.p_calc_deuda_alumno(P_ID_ALUMNO,'PEN');
    COMMIT;
        
    -- GET deuda
    SELECT COUNT(TZRCDAB_AMOUNT)
    INTO   P_INDICADOR
    FROM   TZRCDAB
    WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
    AND TZRCDAB_PIDM = P_ID_ALUMNO 
    --AND TZRCDAB_DETAIL_CODE = P_CODIGO_DETALLE
    AND TZRCDAB_TRAN_NUMBER_PRIN = P_TRAN_NUMBER;
      
    RETURN(P_INDICADOR > 0);

END F_GET_SIDEUDA_ALUMNO;

----------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------

                                      SET SERVEROUTPUT ON
                                      DECLARE P_DEUDA VARCHAR2(10);
                                      BEGIN
                                              P_GET_SIDEUDA_ALUMNO(325180,'35',P_DEUDA);
                                              DBMS_OUTPUT.PUT_LINE(P_DEUDA);
                                      END;
----------------------------------------------------------------------------------------------------------------------
create or replace PROCEDURE P_GET_SIDEUDA_ALUMNO
              (
                P_ID_ALUMNO           IN SPRIDEN.SPRIDEN_ID%TYPE,
                P_TRAN_NUMBER         IN TBRACCD.TBRACCD_TRAN_NUMBER%TYPE,
                P_DEUDA               OUT VARCHAR2
              )
              
/* ===================================================================================================================
  NOMBRE    : P_GET_SIDEUDA_ALUMNO
  FECHA     : 08//11/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : El objetivo es que en base a un ID y un código de detalle verifique la existencia de deuda generada por 
              la solicitud del alumno para el periodo correspondiente, divisa PEN.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  
  =================================================================================================================== */
              
AS
      PRAGMA AUTONOMOUS_TRANSACTION;
      P_INDICADOR           NUMBER;
BEGIN
--
    
    -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
    TZKCDAA.p_calc_deuda_alumno(P_ID_ALUMNO,'PEN');
    COMMIT;
        
    -- GET deuda
    SELECT COUNT(TZRCDAB_AMOUNT)
    INTO   P_INDICADOR
    FROM   TZRCDAB
    WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
    AND TZRCDAB_PIDM = P_ID_ALUMNO 
    --AND TZRCDAB_DETAIL_CODE = P_CODIGO_DETALLE
    AND TZRCDAB_TRAN_NUMBER_PRIN = P_TRAN_NUMBER;
   
    IF P_INDICADOR > 0 THEN
      P_DEUDA   := 'TRUE';
    ELSE
      P_DEUDA   :='FALSE';
    END IF;

END P_GET_SIDEUDA_ALUMNO;

