create or replace FUNCTION f_get_sideuda_alumno
							(
								P_PIDM IN TBRACCD.TBRACCD_PIDM%TYPE, -- PIDM ALUMNO
								P_TERM_CODE IN STVTERM.STVTERM_CODE%TYPE,
                P_COD_DETALLE IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE
							) RETURN VARCHAR2 
              
/* ===================================================================================================================
	NOMBRE		: f_get_sideuda_alumno
	FECHA		  : 07//09/16
	AUTOR		  : Mallqui Lopez, Richard Alfonso
	OBJETIVO	: Verifica la existencia de deuda en la cuenta corriente del alumno para un codigo detalle y  
              periodo correspondiente, divisa PEN.

	MODIFICACIONES
	NRO 	FECHA		USUARIO		MODIFICACION

	=================================================================================================================== */
              
AS
PRAGMA AUTONOMOUS_TRANSACTION;

      C_ROWS_FOUND    NUMBER;
      C_SI_DEUDA      VARCHAR2(5) :='false';
BEGIN

    TZKCDAA.p_calc_deuda_alumno(P_PIDM,'PEN');
    COMMIT;
    
    SELECT COUNT(*)
    INTO   C_ROWS_FOUND
    FROM   TZRCDAB
    WHERE  TZRCDAB_TERM_CODE = P_TERM_CODE AND TZRCDAB_PIDM = P_PIDM 
    AND TZRCDAB_DETAIL_CODE = P_COD_DETALLE AND ROWNUM <= 1;
  
    IF C_ROWS_FOUND > 0 THEN
      C_SI_DEUDA := 'true';
    END IF;
      
    RETURN C_SI_DEUDA;

END f_get_sideuda_alumno;