/*SET SERVEROUTPUT ON
DECLARE
   P_FOLIO_SOLICITUD       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE;
   P_COMENTARIO_ALUMNO     VARCHAR2(4000);
   P_TIPO_RECTIFICACION    VARCHAR2(1);
   P_BEC18                 VARCHAR2(40);
BEGIN
   P_GET_STUDENT_INPUTS('7677',P_COMENTARIO_ALUMNO,P_TIPO_RECTIFICACION,P_BEC18);
   DBMS_OUTPUT.PUT_LINE(P_COMENTARIO_ALUMNO || '---' || P_TIPO_RECTIFICACION || '---' || P_BEC18);
END
*/

/* ===================================================================================================================
  NOMBRE    : P_GET_STUDENT_INPUTS
  FECHA     : 27/12/2017
  AUTOR     : Arana Milla, Karina Lizbeth
  OBJETIVO  : Con el n�mero de folio, obtener el PIDM, comentario y tipo de rectificaci�n de matr�cula (con pago o 
              sin pago).

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */
  
CREATE OR REPLACE PROCEDURE P_GET_STUDENT_INPUTS(

   P_FOLIO_SOLICITUD       IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
   P_COMENTARIO_ALUMNO     OUT VARCHAR2,
   P_TIPO_RECTIFICACION    OUT VARCHAR2,
   P_BEC18                 OUT VARCHAR2
  
)

AS
  V_PIDM                  SPRIDEN.SPRIDEN_PIDM%TYPE;
  V_ADDL_DATA_CDE         SVRSVAD.SVRSVAD_ADDL_DATA_CDE%TYPE;
  V_ADDL_DATA_SEQ         SVRSVAD.SVRSVAD_ADDL_DATA_SEQ%TYPE;
  V_ADDL_DATA_DESC        SVRSVAD.SVRSVAD_ADDL_DATA_DESC%TYPE;

  CURSOR C_SVRSVAD IS
        SELECT SVRSVAD_ADDL_DATA_SEQ,SVRSVAD_ADDL_DATA_DESC, SVRSVAD_ADDL_DATA_CDE 
        FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
             INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
             ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
        WHERE SVRSRAD_SRVC_CODE = 'SOL003'
              AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
              ORDER BY SVRSVAD_ADDL_DATA_SEQ;
      
BEGIN

      OPEN C_SVRSVAD;
        LOOP
          FETCH C_SVRSVAD INTO V_ADDL_DATA_SEQ, V_ADDL_DATA_DESC, V_ADDL_DATA_CDE;
          IF C_SVRSVAD%FOUND THEN
              IF V_ADDL_DATA_SEQ = 1 THEN
                P_COMENTARIO_ALUMNO  := V_ADDL_DATA_DESC;
              ELSIF V_ADDL_DATA_SEQ = 2 THEN
                P_TIPO_RECTIFICACION := V_ADDL_DATA_CDE;
              END IF;
          ELSE EXIT;
        END IF;
        END LOOP;
      CLOSE C_SVRSVAD;
      
      SELECT SVRSVPR_PIDM INTO V_PIDM
      FROM SVRSVPR
      WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;
      
      -- get si alumno BEC18
      SELECT CASE WHEN DAT = 0 THEN 'No' ELSE 'Si' END  
      INTO P_BEC18 FROM 
      (
            SELECT COUNT(*) DAT FROM SGRSATT 
            WHERE SGRSATT_PIDM = V_PIDM
            AND SGRSATT_ATTS_CODE = 'BE18'
            AND SGRSATT_TERM_CODE_EFF IN (
                SELECT MAX(SGRSATT_TERM_CODE_EFF) FROM SGRSATT
                WHERE SGRSATT_PIDM = V_PIDM
            )
      ) WHERE ROWNUM <= 1;
      
    EXCEPTION
        WHEN OTHERS THEN
             RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);  
END P_GET_STUDENT_INPUTS;