/*
SET SERVEROUTPUT ON
DECLARE
   P_PIDM       SPRIDEN.SPRIDEN_PIDM%TYPE;
   P_PROGRAM    SOBCURR.SOBCURR_PROGRAM%TYPE;
   P_CANT_CRED  SMBPOGN.SMBPOGN_ACT_CREDITS_OVERALL%TYPE;
   P_MESSAGE    VARCHAR2(4000);
BEGIN
   P_GET_CRED_APROB('545718','114',P_CANT_CRED,P_MESSAGE);
   DBMS_OUTPUT.PUT_LINE(P_PIDM||'---'||P_PROGRAM||'---'||P_CANT_CRED||'---'||P_MESSAGE);
END;
*/



CREATE OR REPLACE PROCEDURE P_GET_CRED_APROB(
   P_PIDM       IN SPRIDEN.SPRIDEN_PIDM%TYPE,
   P_PROGRAM    IN SOBCURR.SOBCURR_PROGRAM%TYPE,
   P_CANT_CRED  OUT SMBPOGN.SMBPOGN_ACT_CREDITS_OVERALL%TYPE,
   P_MESSAGE    OUT VARCHAR2
)
AS

/* ===================================================================================================================
  NOMBRE    : P_GET_CRED_APROB
  FECHA     : 19/01/2018
  AUTOR     : Arana Milla, Karina Lizbeth
  OBJETIVO  : Obtener la cantidad de cr�ditos aprobados por el estudiante.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */

   CURSOR C_SMBPOGN IS
   SELECT SMBPOGN_ACT_CREDITS_OVERALL
     FROM (SELECT SMBPOGN_ACT_CREDITS_OVERALL 
           FROM SMBPOGN
           WHERE SMBPOGN_PIDM = P_PIDM
                 AND SMBPOGN_PROGRAM = P_PROGRAM
           ORDER BY SMBPOGN_TERM_CODE_EFF DESC, SMBPOGN_REQUEST_NO DESC
          )WHERE ROWNUM = 1;
   
BEGIN
   
   OPEN C_SMBPOGN;
     FETCH C_SMBPOGN INTO P_CANT_CRED;
       IF C_SMBPOGN%NOTFOUND THEN
          P_MESSAGE := 'No tiene registro de cr�ditos aprobados';
       END IF;
   CLOSE C_SMBPOGN;
   
   EXCEPTION
     WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
   
END P_GET_CRED_APROB;