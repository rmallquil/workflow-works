create or replace PACKAGE BWZKCSREI AS
/*******************************************************************************
 BWZKCSREI:
       Paquete Web _ Desarrollo Propio _ Paquete _ Conti SOLICITUD DE REINCORPORACIÒN
*******************************************************************************/
-- FILE NAME..: BWZKCSREI.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKCSREI
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/

PROCEDURE P_GET_INFO_STUDENT (
            P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
            P_PIDM              OUT SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_DEPT_CODE         OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE         OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC         OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_PROGRAM           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
            P_EMAIL             OUT GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE,
            P_NAME              OUT VARCHAR2      
      );

--------------------------------------------------------------------------------

PROCEDURE P_REGLAS_VALIDARDATA (
            P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_DEPT_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
            P_TERM_NEW          IN STVTERM.STVTERM_CODE%TYPE,
            P_DATOS_VALIDOS     OUT NUMBER
      );
              
--------------------------------------------------------------------------------

PROCEDURE P_CAMBIAR_ESTADO_ALUMNO( 
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_TERM_CODE           IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
            P_ERROR               OUT VARCHAR2
      );

--------------------------------------------------------------------------------

PROCEDURE P_CREATE_CTACORRIENTE (
            P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
            P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_DEPT_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
            P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
            P_PROGRAM           IN SOBCURR.SOBCURR_PROGRAM%TYPE
      );

--------------------------------------------------------------------------------

------------ APEC -----------------
PROCEDURE P_SET_CARGO_CONCEPTO ( 
              P_ID                    IN SPRIDEN.SPRIDEN_ID%TYPE,
              P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
              P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
              P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
              P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
              P_APEC_CONCEPTO         IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
              P_PROGRAM               IN SOBCURR.SOBCURR_PROGRAM%TYPE,
              P_MESSAGE               OUT VARCHAR2
      );

--------------------------------------------------------------------------------

PROCEDURE P_SET_GRP_INSCRIPC_ABIERTO (
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
            P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
            P_ERROR               OUT VARCHAR2
      );

--------------------------------------------------------------------------------

PROCEDURE P_STI_EXECCAPP (
            P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_PROGRAM           IN SOBCURR.SOBCURR_PROGRAM%TYPE,
            P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
            P_MESSAGE           OUT VARCHAR2
      );

--------------------------------------------------------------------------------

PROCEDURE P_STI_EXECPROY (
            P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_PROGRAM           IN SOBCURR.SOBCURR_PROGRAM%TYPE,
            P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
            P_MESSAGE           OUT VARCHAR2
      );

--------------------------------------------------------------------------------

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END BWZKCSREI;