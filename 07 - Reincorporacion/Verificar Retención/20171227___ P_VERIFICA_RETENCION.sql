/*
SET SERVEROUTPUT ON
DECLARE
   P_PIDM        SPRIDEN.SPRIDEN_PIDM%TYPE;
   P_RET_CODE    SPRHOLD.SPRHOLD_HLDD_CODE%TYPE;
   P_RETENCION   VARCHAR2(15); 
   P_MESSAGE     VARCHAR2(4000);
BEGIN
   P_VERIFICA_RETENCION('107001','02',P_RETENCION,P_MESSAGE);
   DBMS_OUTPUT.PUT_LINE(P_RETENCION);
END;
*/

/* ===================================================================================================================
  NOMBRE    : P_VERIFICA_RETENCION
  FECHA     : 27/12/2017
  AUTOR     : Arana Milla, Karina Lizbeth
  OBJETIVO  : Verificar si el estudiante tiene activa la retención de documentos
  
  MODIFICACIONES
  NRO     FECHA         USUARIO       MODIFICACION   
  =================================================================================================================== */

CREATE OR REPLACE PROCEDURE P_VERIFICA_RETENCION(
   P_PIDM        IN SPRIDEN.SPRIDEN_PIDM%TYPE,
   P_RET_CODE    IN SPRHOLD.SPRHOLD_HLDD_CODE%TYPE,
   P_RETENCION   OUT VARCHAR2, 
   P_MESSAGE     OUT VARCHAR2
) 
AS

   V_INDICADOR_RET   NUMBER;
   V_ERROR           EXCEPTION;
   
BEGIN
  
  SELECT COUNT(*) INTO V_INDICADOR_RET 
  FROM SPRHOLD
  WHERE SPRHOLD_HLDD_CODE = P_RET_CODE
        AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SPRHOLD_FROM_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') AND TO_DATE(TO_CHAR(SPRHOLD_TO_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')
        AND SPRHOLD_PIDM = P_PIDM;
        
  IF V_INDICADOR_RET = 0 THEN
    P_RETENCION:= 'FALSE';
    
  ELSIF V_INDICADOR_RET >= 1 THEN
    P_RETENCION:= 'TRUE';
    
  ELSE
    RAISE V_ERROR;
  END IF;
  
  EXCEPTION
      WHEN V_ERROR THEN
          P_MESSAGE  := '- No data found.';
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
      WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VERIFICA_RETENCION;