CREATE OR REPLACE PROCEDURE P_SET_GRP_OPEN (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
      P_TERM_CODE           IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE ,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE , 
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE ,
      P_MESSAGE             OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : p_set_grupo_inscripcion_abierto
  FECHA     : 27/09/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Asignar el c�digo de grupo de inscripci�n para la rectificaci�n en base al id, la modalidad y a la sede del estudiante.

                          Ejemplo: 99-99WF01
------------------------------------------------------------------------------
    Rectificaci�n               Modalidad                 Sede            
99-99 � Rectificaci�n     R � Regular                   S01 � Huancayo
                          W � Gente que trabaja         F01 � Arequipa 
                          V � Virtual                   F02 � Lima  
                                                        F03 � Cusco
                                                        V00 � Virtual

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
AS
      P_RPGRP               SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      P_RPGRP_NEW           SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      P_MODALIDAD_SEDE      VARCHAR2(4);
      P_MODALIDAD_SEDE2     VARCHAR2(4);
      P_INDICADOR_TERM      SFBRGRP.SFBRGRP_TERM_CODE%TYPE;
      
      V_MESSAGE             EXCEPTION;
      V_MESSAGE2            EXCEPTION;
      V_MESSAGE3            EXCEPTION;
      P_INDICADOR           NUMBER := 0;
      P_ROW_UPDATE          NUMBER := 0;
      P_COD_GROUP           VARCHAR2(15); --:= '00-20'; --->>> EN PERIODO "00" ES "00-20"
                                                     --->>> EN CICLO REGULAR ES "99-99"
      CURSOR C_SFBRGRP_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM                        
                    FROM STVCAMP,STVDEPT 
                    WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;                                                     
      
BEGIN
--
      P_INDICADOR_TERM := SUBSTR(P_TERM_CODE,5,1);
      
      IF P_INDICADOR_TERM = '0' THEN
         P_COD_GROUP := '00-20';
      ELSE
         P_COD_GROUP := '99-99';
      END IF;
      
      OPEN C_SFBRGRP_PTRM;
           LOOP
               FETCH C_SFBRGRP_PTRM INTO P_MODALIDAD_SEDE;
                     EXIT WHEN C_SFBRGRP_PTRM%NOTFOUND;
                     END LOOP;
      CLOSE C_SFBRGRP_PTRM;
      
      SELECT COUNT(*) INTO P_INDICADOR
      FROM SFBRGRP
      WHERE SFBRGRP_PIDM = P_PIDM
            AND SFBRGRP_TERM_CODE = P_TERM_CODE;
            
      IF P_INDICADOR = 0 THEN
      
         INSERT INTO SFBRGRP (SFBRGRP_TERM_CODE,          SFBRGRP_PIDM,           SFBRGRP_RGRP_CODE,                 SFBRGRP_USER, 
                              SFBRGRP_ACTIVITY_DATE,      SFBRGRP_USER_ID,        SFBRGRP_DATA_ORIGIN)
                      VALUES (P_TERM_CODE,                P_PIDM,                 (P_COD_GROUP||P_MODALIDAD_SEDE),   'WorkFlow',   
                              SYSDATE,                    USER,                   'WFAUTO');
         COMMIT;
      ELSE
         
----------------------------------------------------------------------------------------------------------------------------------------      
      --P_INDICADOR := 1;      
      SELECT SFBRGRP_RGRP_CODE INTO P_RPGRP
      FROM SFBRGRP 
      WHERE SFBRGRP_PIDM = P_PIDM
      AND SFBRGRP_TERM_CODE = P_TERM_CODE;
      
      -- OBTENER CADENA DE MODALIDAD Y SEDE  del CODIGO DE GRUPO  
      P_MODALIDAD_SEDE := SUBSTR(P_RPGRP,6,4);
      
      -- VALIDACION : CODIGO grupo insc. y datos proporcionados
      SELECT CASE P_DEPT_CODE 
                WHEN 'UVIR' THEN 'V' --#UC-SEMI PRESENCIAL (EV)
                WHEN 'UPGT' THEN 'W' --#UC-SEMI PRESENCIAL (GT)
                WHEN 'UREG' THEN 'R' --#UC-PRESENCIAL
                ELSE '' END INTO P_MODALIDAD_SEDE2 FROM DUAL;
            
      IF (P_MODALIDAD_SEDE != (P_MODALIDAD_SEDE2 || P_CAMP_CODE )) THEN
              RAISE V_MESSAGE2;
      ELSIF (P_RPGRP = (P_COD_GROUP || P_MODALIDAD_SEDE)) THEN
              RAISE V_MESSAGE3;
      END IF;
    
      
      -- CODIGO DEL GRUPO DE RECTIFICACI�N : 99-99<cod_modadlidad><cod_sede>
      SELECT CONCAT(P_COD_GROUP,P_MODALIDAD_SEDE) INTO P_RPGRP_NEW FROM DUAL;
      
      -- Validar que exista el codigo del grupo de inscripcion
      P_INDICADOR := 2;    
      SELECT SFBWCTL_RGRP_CODE 
      INTO P_RPGRP_NEW
      FROM SFBWCTL
      WHERE SFBWCTL_RGRP_CODE  = P_RPGRP_NEW
      AND  SFBWCTL_TERM_CODE = P_TERM_CODE;
      
      -- ASIGNAR GRUPO DE RECTIFICACI�N
      UPDATE SFBRGRP 
        -- SELECT SFBRGRP_TERM_CODE, SFBRGRP_PIDM, SFBRGRP.* 
        SET SFBRGRP_RGRP_CODE     = P_RPGRP_NEW,
            SFBRGRP_DATA_ORIGIN   = 'WorkFlow',
            SFBRGRP_ACTIVITY_DATE = SYSDATE
      WHERE SFBRGRP_PIDM = P_PIDM
      AND SFBRGRP_TERM_CODE = P_TERM_CODE
      AND EXISTS (
            ------ LA DEFINICION DE LA VISTA SFVRGRP
            SELECT SFBWCTL.SFBWCTL_TERM_CODE,
              SFBWCTL.SFBWCTL_RGRP_CODE,
              SFBWCTL.SFBWCTL_PRIORITY,
              SFRWCTL.SFRWCTL_BEGIN_DATE,
              SFRWCTL.SFRWCTL_END_DATE,
                    SFRWCTL.SFRWCTL_HOUR_BEGIN,
                    SFRWCTL.SFRWCTL_HOUR_END
              FROM SFRWCTL, SFBWCTL
                    WHERE SFRWCTL.SFRWCTL_TERM_CODE = SFBWCTL.SFBWCTL_TERM_CODE
                    AND   SFRWCTL.SFRWCTL_PRIORITY  = SFBWCTL.SFBWCTL_PRIORITY
                    AND   SFBWCTL.SFBWCTL_RGRP_CODE = P_RPGRP_NEW
                    AND   SFRWCTL.SFRWCTL_TERM_CODE = P_TERM_CODE
      );
      P_ROW_UPDATE := P_ROW_UPDATE + SQL%ROWCOUNT;
      --DBMS_OUTPUT.PUT_LINE(P_ROW_UPDATE);
      IF (P_ROW_UPDATE > 1) THEN
            ROLLBACK;
            RAISE V_MESSAGE;
      END IF;

      COMMIT;
EXCEPTION
  WHEN V_MESSAGE THEN
          P_MESSAGE := 'Se detecto que posee DOBLE grupo de inscripci�n: ' || P_RPGRP_NEW;
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
  WHEN V_MESSAGE2 THEN
          P_MESSAGE := 'El departamento y la sede no coinciden con el grupo de rectificaci�n';
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
  WHEN V_MESSAGE3 THEN
          P_MESSAGE := 'Advertencia, Ya se encuentra en el grupo de inscripci�n para su rectificaci�n. No procedio lo solicitado.';
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
  WHEN TOO_MANY_ROWS THEN 
          IF (P_INDICADOR = 2) THEN
              P_MESSAGE := 'Se encontraron mas de un grupo ABIERTO de inscripci�n: ' || P_RPGRP_NEW;
          ELSIF (P_INDICADOR = 1) THEN
              P_MESSAGE := 'Se encontraron mas de un grupo INICIAL de inscripci�n: ' || P_RPGRP_NEW;
          ELSE  P_MESSAGE := SQLERRM;
          END IF;     
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );  
  WHEN NO_DATA_FOUND THEN
           IF (P_INDICADOR = 2) THEN
              P_MESSAGE := 'No se encontr� el grupo ABIERTO de inscripci�n: ' || P_RPGRP_NEW;
          ELSIF (P_INDICADOR = 1) THEN
              P_MESSAGE := 'No se encontr� tu grupo INICIAL de inscripci�n: ' || P_RPGRP_NEW;
          ELSE  P_MESSAGE := SQLERRM;
          END IF;  
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );  
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_GRP_OPEN;

SELECT * FROM SPRIDEN
WHERE SPRIDEN_PIDM = '131128';


