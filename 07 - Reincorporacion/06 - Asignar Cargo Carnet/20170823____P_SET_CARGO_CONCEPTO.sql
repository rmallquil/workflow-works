/*
-- buscar objecto
select dbms_metadata.get_ddl('PROCEDURE','P_SET_CARGO_CONCEPTO') from dual
drop procedure P_SET_CARGO_CONCEPTO;
GRANT EXECUTE ON P_SET_CARGO_CONCEPTO TO wfobjects;
GRANT EXECUTE ON P_SET_CARGO_CONCEPTO TO wfauto;

SET SERVEROUTPUT ON
DECLARE   P_MESSAGE                 VARCHAR2(4000);
BEGIN
    P_SET_CARGO_CONCEPTO('43317482',326495,'UPGT','S01','201720','CAR','110',P_MESSAGE);
    DBMS_OUTPUT.PUT_LINE(P_MESSAGE);
END;
*/

------------ APEC -----------------
CREATE OR REPLACE PROCEDURE P_SET_CARGO_CONCEPTO ( 
        P_ID                    IN SPRIDEN.SPRIDEN_ID%TYPE,
        P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
        P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
        P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
        P_APEC_CONCEPTO         IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
        P_PROGRAM               IN SOBCURR.SOBCURR_PROGRAM%TYPE,
        P_MESSAGE               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SET_CARGO_CONCEPTO
  FECHA     : 23/08/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : APEC-BDUCCI: Asigna cargo al estudiante con el CONCEPTO.
              Valida que solo tenga el cargo una vez al año.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
          -- @PARAMETERS
      V_APEC_CAMP             VARCHAR2(9);
      V_APEC_TERM             VARCHAR2(9);
      V_APEC_DEPT             VARCHAR2(9);
      V_APEC_SECCIONC         VARCHAR2(20);
      V_PART_PERIODO          VARCHAR2(9);
      V_CARGO_CARNET          NUMBER := 0;
    
BEGIN
--    
      --**************************************************************************************************************
      -- GET CRONOGRAMA SECCIONC
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END
      INTO V_PART_PERIODO
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;        
      
      SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
      SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
      SELECT CZRDEPT_DEPT_BDUCCI INTO V_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO


      -- Vlidar que No tenga asignado el cargo en el año
      SELECT SUM("Cargo") INTO V_CARGO_CARNET
      FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
      WHERE "IDDependencia"         = 'UCCI'
      AND "IDAlumno"                = P_ID
      AND SUBSTRB("IDPerAcad",1,4)  = SUBSTRB(V_APEC_TERM,1,4)
      AND "IDConcepto"              = 'CAR'; -- C00 Concepto Matricul

      IF V_CARGO_CARNET = 0 OR  V_CARGO_CARNET IS NULL THEN

            --Get SECCIONC
            WITH 
                CTE_tblSeccionC AS (
                        -- GET SECCIONC
                        SELECT  "IDSeccionC" IDSeccionC,
                                "FecInic" FecInic
                        FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                        WHERE "IDDependencia"='UCCI'
                        AND "IDsede"    = V_APEC_CAMP
                        AND "IDPerAcad" = V_APEC_TERM
                        AND "IDEscuela" = P_PROGRAM
                        AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                        AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                        AND "IDSeccionC" <> '15NEX1A'
                        AND SUBSTRB("IDSeccionC",-2,2) IN (
                            -- PARTE PERIODO           
                            SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (V_PART_PERIODO || '%')
                        )
                )
            SELECT "IDSeccionC" INTO V_APEC_SECCIONC
            FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
            WHERE "IDDependencia" = 'UCCI'
            AND "IDAlumno"    = P_ID
              AND "IDSede"      = V_APEC_CAMP
              AND "IDPerAcad"   = V_APEC_TERM
              AND "IDEscuela"   = P_PROGRAM
              AND "IDSeccionC"  IN (SELECT IDSeccionC FROM CTE_tblSeccionC)
              AND "IDConcepto" = 'C00'; -- C00 Concepto Matricula


            /*******
               -- Get CARGO de CARNET "CAR"
            ********/
            SELECT  "Monto"
            INTO    V_CARGO_CARNET
            FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE  t1 
            INNER JOIN dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE  t2
              ON t1."IDSede"          = t2."IDsede"
              AND t1."IDPerAcad"      = t2."IDPerAcad" 
              AND t1."IDDependencia"  = t2."IDDependencia" 
              AND t1."IDSeccionC"     = t2."IDSeccionC" 
              AND t1."FecInic"        = t2."FecInic"
            WHERE t2."IDDependencia" = 'UCCI'
              AND t2."IDSeccionC"    = V_APEC_SECCIONC
              AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_PSI' -- internado
              AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_ENF' -- internado
              AND t2."IDSeccionC"     <> '15NEX1A'
              AND t2."IDPerAcad"     = V_APEC_TERM
              AND T1."IDPerAcad"     = V_APEC_TERM
              AND t1."IDConcepto"    = P_APEC_CONCEPTO;

            /**********************************************************************************************
                -- ASINGANDO el cargo CARNET a CTACORRIENTE
            ********/
            UPDATE dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
                SET "Cargo" = "Cargo" + V_CARGO_CARNET  
            WHERE "IDDependencia" = 'UCCI' 
            AND "IDAlumno"    = P_ID
            AND "IDSede"      = V_APEC_CAMP 
            AND "IDPerAcad"   = V_APEC_TERM 
            AND "IDEscuela"   = P_PROGRAM
            AND "IDSeccionC"  = V_APEC_SECCIONC
            AND "IDConcepto"  = P_APEC_CONCEPTO; 

            COMMIT;
            
      END IF;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CARGO_CONCEPTO;