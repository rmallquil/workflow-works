

/*
drop procedure P_SET_GR_INSCRIPC_ABIERTO;
GRANT EXECUTE ON P_SET_GR_INSCRIPC_ABIERTO TO wfobjects;
GRANT EXECUTE ON P_SET_GR_INSCRIPC_ABIERTO TO wfauto;

set serveroutput on
DECLARE p_error varchar2(64);
begin
  P_SET_GR_INSCRIPC_ABIERTO('30691','201610','UREG','S01',p_error);
  DBMS_OUTPUT.PUT_LINE(p_error);
end;

*/
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE P_SET_GR_INSCRIPC_ABIERTO (
      P_PIDM_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
      P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE ,
      P_MODALIDAD_ALUMNO    IN STVDEPT.STVDEPT_CODE%TYPE , 
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE ,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SET_GR_INSCRIPC_ABIERTO
  FECHA     : 05/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Asignar el código de grupo de inscripción del estudiante.

                          Ejemplo: 99-99WF01
------------------------------------------------------------------------------
    Rectificación               Modalidad                 Sede            
99-99 – Rectificación     R – Regular                   S01 – Huancayo
                          W – Gente que trabaja         F01 – Arequipa 
                          V – Virtual                   F02 – Lima  
                                                        F03 – Cusco
                                                        V00 – Virtual

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
AS
      P_RPGRP               SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      P_RPGRP_NEW           SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      P_MODALIDAD_SEDE      VARCHAR2(4);
      P_MODALIDAD_SEDE2     VARCHAR2(4);
      
      P_MESSAGE             EXCEPTION;
      P_MESSAGE2            EXCEPTION;
      P_MESSAGE3            EXCEPTION;
      P_INDICADOR           NUMBER := 0;
      P_ROW_UPDATE          NUMBER := 0;
      P_COD_RECTF           VARCHAR2(15) := '99-99';
BEGIN
--
      
      -- VALIDACION : CODIGO grupo insc. y datos proporcionados
      SELECT CASE P_MODALIDAD_ALUMNO 
                WHEN 'UVIR' THEN 'V' --#UC-SEMI PRESENCIAL (EV)
                WHEN 'UPGT' THEN 'W' --#UC-SEMI PRESENCIAL (GT)
                WHEN 'UREG' THEN 'R' --#UC-PRESENCIAL
                ELSE '' END INTO P_MODALIDAD_SEDE2 FROM DUAL;
      
      -- OBTENER CADENA DE MODALIDAD Y SEDE  del CODIGO DE GRUPO  
      P_MODALIDAD_SEDE := (P_MODALIDAD_SEDE2 || P_COD_SEDE);
      
      -- CODIGO DEL GRUPO DE RECTIFICACIÒN : 99-99<cod_modadlidad><cod_sede>
      SELECT CONCAT(P_COD_RECTF,P_MODALIDAD_SEDE) INTO P_RPGRP_NEW FROM DUAL;
      
      -- Validar que exista el codigo del grupo de inscripcion
      SELECT COUNT(SFBWCTL_RGRP_CODE) INTO P_INDICADOR
      FROM SFBWCTL
      WHERE SFBWCTL_RGRP_CODE  = P_RPGRP_NEW
      AND  SFBWCTL_TERM_CODE = P_PERIODO;
      
      -- ASIGNAR GRUPO DE INSCRIPCION - forma SFARGRP
      IF(P_INDICADOR = 1) THEN
            INSERT INTO SFBRGRP (
                  SFBRGRP_TERM_CODE, 
                  SFBRGRP_PIDM, 
                  SFBRGRP_RGRP_CODE, 
                  SFBRGRP_USER, 
                  SFBRGRP_ACTIVITY_DATE
              )
            SELECT P_PERIODO, P_PIDM_ALUMNO, P_RPGRP_NEW, USER, SYSDATE 
            FROM DUAL
            WHERE EXISTS (
                  ------ LA DEFINICION DE LA VISTA SFVRGRP
                  SELECT SFBWCTL.SFBWCTL_TERM_CODE,
                  SFBWCTL.SFBWCTL_RGRP_CODE,
                  SFBWCTL.SFBWCTL_PRIORITY,
                  SFRWCTL.SFRWCTL_BEGIN_DATE,
                  SFRWCTL.SFRWCTL_END_DATE,
                        SFRWCTL.SFRWCTL_HOUR_BEGIN,
                        SFRWCTL.SFRWCTL_HOUR_END
                  FROM SFRWCTL, SFBWCTL
                        WHERE SFRWCTL.SFRWCTL_TERM_CODE = SFBWCTL.SFBWCTL_TERM_CODE
                        AND   SFRWCTL.SFRWCTL_PRIORITY  = SFBWCTL.SFBWCTL_PRIORITY
                        AND   SFBWCTL.SFBWCTL_RGRP_CODE = P_RPGRP_NEW
                        AND   SFRWCTL.SFRWCTL_TERM_CODE = P_PERIODO
            );
      ELSE
          RAISE P_MESSAGE;
      END IF;

      COMMIT;

EXCEPTION
  WHEN P_MESSAGE THEN
          P_ERROR := 'Se detecto un inconveniente con el grupo de inscripción: ' || P_RPGRP_NEW || ' verificar la forma SFARCTL.';
  WHEN OTHERS THEN
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_SET_GR_INSCRIPC_ABIERTO;

       
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------