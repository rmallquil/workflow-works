/*SET SERVEROUTPUT ON
DECLARE
  P_PIDM        SPRIDEN.SPRIDEN_PIDM%TYPE;
  P_TERM_CODE   STVTERM.STVTERM_CODE%TYPE;
  P_MESSAGE     VARCHAR2(4000);
BEGIN
 P_SET_REINCORPORACION('118196','201800',P_MESSAGE);
END;
*/
-- PIDM'S A PROBAR
-- 563400 -- PERIODO 201800 CREADO
-- 570738 -- PARA INSERTAR PERIODO NUEVO --- ya cambiado
-- 430465

/* ===================================================================================================================
  NOMBRE    : P_SET_REINCORPORACION
  FECHA     : 27/12/2017
  AUTOR     : Arana Milla, Karina Lizbeth
  OBJETIVO  : Insertar el registro de nuevo periodo a reincorporar
  
  MODIFICACIONES
  NRO     FECHA         USUARIO       MODIFICACION   
  =================================================================================================================== */

CREATE OR REPLACE PROCEDURE P_SET_REINCORPORACION(
  P_PIDM        IN	SPRIDEN.SPRIDEN_PIDM%TYPE,
  P_TERM_CODE   IN	STVTERM.STVTERM_CODE%TYPE,
  P_MESSAGE     OUT	VARCHAR2
)

AS

    V_SGBSTDN_REC           SGBSTDN%ROWTYPE;
    V_SGRSTSP_REC           SGRSTSP%ROWTYPE;
    V_STSP_CODE             SGRSTSP.SGRSTSP_STSP_CODE%TYPE;
    V_STST_CODE             SGBSTDN.SGBSTDN_STST_CODE%TYPE; 
    V_STYP_CODE             SGBSTDN.SGBSTDN_STYP_CODE%TYPE;
     
    -- GET FORMA SGASTDN
      CURSOR C_SGBSTDN IS
      SELECT * FROM (
          SELECT *
          FROM SGBSTDN WHERE SGBSTDN_PIDM = P_PIDM
          ORDER BY SGBSTDN_TERM_CODE_EFF DESC
       ) WHERE ROWNUM = 1
       AND NOT EXISTS (
          SELECT *
          FROM SGBSTDN WHERE SGBSTDN_PIDM = P_PIDM
          AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE);
            
    -- GET estatus alumno de BANNER-->PLAN ESTUDIO
      CURSOR C_SGRSTSP IS
      SELECT * FROM (
              SELECT * 
              FROM SGRSTSP 
              WHERE SGRSTSP_PIDM = P_PIDM
              ORDER BY SGRSTSP_TERM_CODE_EFF DESC
        ) WHERE ROWNUM = 1;
          
BEGIN
  
      -- INSERT  ------> SGBSTDN -
      
      OPEN C_SGBSTDN;
          FETCH C_SGBSTDN INTO V_SGBSTDN_REC;
          IF C_SGBSTDN%FOUND THEN
           INSERT INTO SGBSTDN (
                      SGBSTDN_PIDM,                 SGBSTDN_TERM_CODE_EFF,          SGBSTDN_STST_CODE,
                      SGBSTDN_LEVL_CODE,            SGBSTDN_STYP_CODE,              SGBSTDN_TERM_CODE_MATRIC,
                      SGBSTDN_TERM_CODE_ADMIT,      SGBSTDN_EXP_GRAD_DATE,          SGBSTDN_CAMP_CODE,
                      SGBSTDN_FULL_PART_IND,        SGBSTDN_SESS_CODE,              SGBSTDN_RESD_CODE,
                      SGBSTDN_COLL_CODE_1,          SGBSTDN_DEGC_CODE_1,            SGBSTDN_MAJR_CODE_1,
                      SGBSTDN_MAJR_CODE_MINR_1,     SGBSTDN_MAJR_CODE_MINR_1_2,     SGBSTDN_MAJR_CODE_CONC_1,
                      SGBSTDN_MAJR_CODE_CONC_1_2,   SGBSTDN_MAJR_CODE_CONC_1_3,     SGBSTDN_COLL_CODE_2,
                      SGBSTDN_DEGC_CODE_2,          SGBSTDN_MAJR_CODE_2,            SGBSTDN_MAJR_CODE_MINR_2,
                      SGBSTDN_MAJR_CODE_MINR_2_2,   SGBSTDN_MAJR_CODE_CONC_2,       SGBSTDN_MAJR_CODE_CONC_2_2,
                      SGBSTDN_MAJR_CODE_CONC_2_3,   SGBSTDN_ORSN_CODE,              SGBSTDN_PRAC_CODE,
                      SGBSTDN_ADVR_PIDM,            SGBSTDN_GRAD_CREDIT_APPR_IND,   SGBSTDN_CAPL_CODE,
                      SGBSTDN_LEAV_CODE,            SGBSTDN_LEAV_FROM_DATE,         SGBSTDN_LEAV_TO_DATE,
                      SGBSTDN_ASTD_CODE,            SGBSTDN_TERM_CODE_ASTD,         SGBSTDN_RATE_CODE,
                      SGBSTDN_ACTIVITY_DATE,        SGBSTDN_MAJR_CODE_1_2,          SGBSTDN_MAJR_CODE_2_2,
                      SGBSTDN_EDLV_CODE,            SGBSTDN_INCM_CODE,              SGBSTDN_ADMT_CODE,
                      SGBSTDN_EMEX_CODE,            SGBSTDN_APRN_CODE,              SGBSTDN_TRCN_CODE,
                      SGBSTDN_GAIN_CODE,            SGBSTDN_VOED_CODE,              SGBSTDN_BLCK_CODE,
                      SGBSTDN_TERM_CODE_GRAD,       SGBSTDN_ACYR_CODE,              SGBSTDN_DEPT_CODE,
                      SGBSTDN_SITE_CODE,            SGBSTDN_DEPT_CODE_2,            SGBSTDN_EGOL_CODE,
                      SGBSTDN_DEGC_CODE_DUAL,       SGBSTDN_LEVL_CODE_DUAL,         SGBSTDN_DEPT_CODE_DUAL,
                      SGBSTDN_COLL_CODE_DUAL,       SGBSTDN_MAJR_CODE_DUAL,         SGBSTDN_BSKL_CODE,
                      SGBSTDN_PRIM_ROLL_IND,        SGBSTDN_PROGRAM_1,              SGBSTDN_TERM_CODE_CTLG_1,
                      SGBSTDN_DEPT_CODE_1_2,        SGBSTDN_MAJR_CODE_CONC_121,     SGBSTDN_MAJR_CODE_CONC_122,
                      SGBSTDN_MAJR_CODE_CONC_123,   SGBSTDN_SECD_ROLL_IND,          SGBSTDN_TERM_CODE_ADMIT_2,
                      SGBSTDN_ADMT_CODE_2,          SGBSTDN_PROGRAM_2,              SGBSTDN_TERM_CODE_CTLG_2,
                      SGBSTDN_LEVL_CODE_2,          SGBSTDN_CAMP_CODE_2,            SGBSTDN_DEPT_CODE_2_2,
                      SGBSTDN_MAJR_CODE_CONC_221,   SGBSTDN_MAJR_CODE_CONC_222,     SGBSTDN_MAJR_CODE_CONC_223,
                      SGBSTDN_CURR_RULE_1,          SGBSTDN_CMJR_RULE_1_1,          SGBSTDN_CCON_RULE_11_1,
                      SGBSTDN_CCON_RULE_11_2,       SGBSTDN_CCON_RULE_11_3,         SGBSTDN_CMJR_RULE_1_2,
                      SGBSTDN_CCON_RULE_12_1,       SGBSTDN_CCON_RULE_12_2,         SGBSTDN_CCON_RULE_12_3,
                      SGBSTDN_CMNR_RULE_1_1,        SGBSTDN_CMNR_RULE_1_2,          SGBSTDN_CURR_RULE_2,
                      SGBSTDN_CMJR_RULE_2_1,        SGBSTDN_CCON_RULE_21_1,         SGBSTDN_CCON_RULE_21_2,
                      SGBSTDN_CCON_RULE_21_3,       SGBSTDN_CMJR_RULE_2_2,          SGBSTDN_CCON_RULE_22_1,
                      SGBSTDN_CCON_RULE_22_2,       SGBSTDN_CCON_RULE_22_3,         SGBSTDN_CMNR_RULE_2_1,
                      SGBSTDN_CMNR_RULE_2_2,        SGBSTDN_PREV_CODE,              SGBSTDN_TERM_CODE_PREV,
                      SGBSTDN_CAST_CODE,            SGBSTDN_TERM_CODE_CAST,         SGBSTDN_DATA_ORIGIN,
                      SGBSTDN_USER_ID,              SGBSTDN_SCPC_CODE ) 
            VALUES (  V_SGBSTDN_REC.SGBSTDN_PIDM,                 P_TERM_CODE,                                  'AS',
                      V_SGBSTDN_REC.SGBSTDN_LEVL_CODE,            'R',                                          V_SGBSTDN_REC.SGBSTDN_TERM_CODE_MATRIC,
                      V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT,      V_SGBSTDN_REC.SGBSTDN_EXP_GRAD_DATE,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE,
                      V_SGBSTDN_REC.SGBSTDN_FULL_PART_IND,        V_SGBSTDN_REC.SGBSTDN_SESS_CODE,              V_SGBSTDN_REC.SGBSTDN_RESD_CODE,
                      V_SGBSTDN_REC.SGBSTDN_COLL_CODE_1,          V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_1,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1,
                      V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1_2,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1,
                      V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_3,     V_SGBSTDN_REC.SGBSTDN_COLL_CODE_2,
                      V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2,
                      V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_2,
                      V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_3,   V_SGBSTDN_REC.SGBSTDN_ORSN_CODE,              V_SGBSTDN_REC.SGBSTDN_PRAC_CODE,
                      V_SGBSTDN_REC.SGBSTDN_ADVR_PIDM,            V_SGBSTDN_REC.SGBSTDN_GRAD_CREDIT_APPR_IND,   V_SGBSTDN_REC.SGBSTDN_CAPL_CODE,
                      V_SGBSTDN_REC.SGBSTDN_LEAV_CODE,            V_SGBSTDN_REC.SGBSTDN_LEAV_FROM_DATE,         V_SGBSTDN_REC.SGBSTDN_LEAV_TO_DATE,
                      V_SGBSTDN_REC.SGBSTDN_ASTD_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ASTD,         V_SGBSTDN_REC.SGBSTDN_RATE_CODE,
                      SYSDATE,                                    V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2_2,
                      V_SGBSTDN_REC.SGBSTDN_EDLV_CODE,            V_SGBSTDN_REC.SGBSTDN_INCM_CODE,              V_SGBSTDN_REC.SGBSTDN_ADMT_CODE,
                      V_SGBSTDN_REC.SGBSTDN_EMEX_CODE,            V_SGBSTDN_REC.SGBSTDN_APRN_CODE,              V_SGBSTDN_REC.SGBSTDN_TRCN_CODE,
                      V_SGBSTDN_REC.SGBSTDN_GAIN_CODE,            V_SGBSTDN_REC.SGBSTDN_VOED_CODE,              V_SGBSTDN_REC.SGBSTDN_BLCK_CODE,
                      V_SGBSTDN_REC.SGBSTDN_TERM_CODE_GRAD,       V_SGBSTDN_REC.SGBSTDN_ACYR_CODE,              V_SGBSTDN_REC.SGBSTDN_DEPT_CODE,
                      V_SGBSTDN_REC.SGBSTDN_SITE_CODE,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2,            V_SGBSTDN_REC.SGBSTDN_EGOL_CODE,
                      V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_DUAL,
                      V_SGBSTDN_REC.SGBSTDN_COLL_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_BSKL_CODE,
                      V_SGBSTDN_REC.SGBSTDN_PRIM_ROLL_IND,        V_SGBSTDN_REC.SGBSTDN_PROGRAM_1,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CTLG_1,
                      V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_1_2,        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_121,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_122,
                      V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_123,   V_SGBSTDN_REC.SGBSTDN_SECD_ROLL_IND,          V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT_2,
                      V_SGBSTDN_REC.SGBSTDN_ADMT_CODE_2,          V_SGBSTDN_REC.SGBSTDN_PROGRAM_2,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CTLG_2,
                      V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_2,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE_2,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2_2,
                      V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_221,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_222,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_223,
                      V_SGBSTDN_REC.SGBSTDN_CURR_RULE_1,          V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_1,          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_1,
                      V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_3,         V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_2,
                      V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_1,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_2,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_3,
                      V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_1,        V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_2,          V_SGBSTDN_REC.SGBSTDN_CURR_RULE_2,
                      V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_1,        V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_1,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_2,
                      V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_3,       V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_2,          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_1,
                      V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_3,         V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_1,
                      V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_2,        V_SGBSTDN_REC.SGBSTDN_PREV_CODE,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_PREV,
                      V_SGBSTDN_REC.SGBSTDN_CAST_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CAST,         'WorkFlow',
                      USER,                                       V_SGBSTDN_REC.SGBSTDN_SCPC_CODE );
                      
                      COMMIT;
                      
            ELSE
            
             SELECT SGBSTDN_STST_CODE, SGBSTDN_STYP_CODE INTO V_STST_CODE, V_STYP_CODE
             FROM SGBSTDN
             WHERE SGBSTDN_PIDM = P_PIDM
                   AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE;
                
                IF V_STST_CODE = 'IS' AND V_STYP_CODE <> 'R' THEN
                   
                   UPDATE SGBSTDN
                   SET SGBSTDN_STST_CODE = 'AS',
                       SGBSTDN_STYP_CODE = 'R',
                       SGBSTDN_ACTIVITY_DATE = SYSDATE,
                       SGBSTDN_DATA_ORIGIN = 'WorkFlow',
                       SGBSTDN_USER_ID = USER
                   WHERE SGBSTDN_PIDM = P_PIDM
                         AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE;
                      
                   COMMIT;
                
                ELSIF V_STST_CODE = 'AS' AND V_STYP_CODE <> 'R' THEN
                   
                   UPDATE SGBSTDN
                   SET SGBSTDN_STYP_CODE = 'R',
                       SGBSTDN_ACTIVITY_DATE = SYSDATE,
                       SGBSTDN_DATA_ORIGIN = 'WorkFlow',
                       SGBSTDN_USER_ID = USER
                   WHERE SGBSTDN_PIDM = P_PIDM
                         AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE;
                      
                   COMMIT;
                 
                 ELSIF V_STST_CODE = 'IS' AND V_STYP_CODE = 'R' THEN
                 
                    UPDATE SGBSTDN
                    SET SGBSTDN_STST_CODE = 'AS',
                       SGBSTDN_ACTIVITY_DATE = SYSDATE,
                       SGBSTDN_DATA_ORIGIN = 'WorkFlow',
                       SGBSTDN_USER_ID = USER
                    WHERE SGBSTDN_PIDM = P_PIDM
                          AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE;
                      
                    COMMIT;
                 END IF;
            END IF;       
      CLOSE C_SGBSTDN;
  
      OPEN C_SGRSTSP;
           FETCH C_SGRSTSP INTO V_SGRSTSP_REC;
           IF C_SGRSTSP%FOUND THEN
           INSERT INTO SGRSTSP (
                      SGRSTSP_PIDM,       SGRSTSP_TERM_CODE_EFF,  SGRSTSP_KEY_SEQNO,
                      SGRSTSP_STSP_CODE,  SGRSTSP_ACTIVITY_DATE,  SGRSTSP_DATA_ORIGIN,
                      SGRSTSP_USER_ID,    SGRSTSP_FULL_PART_IND,  SGRSTSP_SESS_CODE,
                      SGRSTSP_RESD_CODE,  SGRSTSP_ORSN_CODE,      SGRSTSP_PRAC_CODE,
                      SGRSTSP_CAPL_CODE,  SGRSTSP_EDLV_CODE,      SGRSTSP_INCM_CODE,
                      SGRSTSP_EMEX_CODE,  SGRSTSP_APRN_CODE,      SGRSTSP_TRCN_CODE,
                      SGRSTSP_GAIN_CODE,  SGRSTSP_VOED_CODE,      SGRSTSP_BLCK_CODE,
                      SGRSTSP_EGOL_CODE,  SGRSTSP_BSKL_CODE,      SGRSTSP_ASTD_CODE,
                      SGRSTSP_PREV_CODE,  SGRSTSP_CAST_CODE ) 
              SELECT  V_SGRSTSP_REC.SGRSTSP_PIDM,       P_TERM_CODE,                          V_SGRSTSP_REC.SGRSTSP_KEY_SEQNO,
                      'AS',                             SYSDATE,                              'WFAUTO',
                      USER,                             V_SGRSTSP_REC.SGRSTSP_FULL_PART_IND,  V_SGRSTSP_REC.SGRSTSP_SESS_CODE,
                      V_SGRSTSP_REC.SGRSTSP_RESD_CODE,  V_SGRSTSP_REC.SGRSTSP_ORSN_CODE,      V_SGRSTSP_REC.SGRSTSP_PRAC_CODE,
                      V_SGRSTSP_REC.SGRSTSP_CAPL_CODE,  V_SGRSTSP_REC.SGRSTSP_EDLV_CODE,      V_SGRSTSP_REC.SGRSTSP_INCM_CODE,
                      V_SGRSTSP_REC.SGRSTSP_EMEX_CODE,  V_SGRSTSP_REC.SGRSTSP_APRN_CODE,      V_SGRSTSP_REC.SGRSTSP_TRCN_CODE,
                      V_SGRSTSP_REC.SGRSTSP_GAIN_CODE,  V_SGRSTSP_REC.SGRSTSP_VOED_CODE,      V_SGRSTSP_REC.SGRSTSP_BLCK_CODE,
                      V_SGRSTSP_REC.SGRSTSP_EGOL_CODE,  V_SGRSTSP_REC.SGRSTSP_BSKL_CODE,      V_SGRSTSP_REC.SGRSTSP_ASTD_CODE,
                      V_SGRSTSP_REC.SGRSTSP_PREV_CODE,  V_SGRSTSP_REC.SGRSTSP_CAST_CODE
              FROM DUAL;
           COMMIT;
         
           ELSE
           
             SELECT SGRSTSP_STSP_CODE INTO V_STSP_CODE FROM SGRSTSP 
             WHERE SGRSTSP_PIDM = P_PIDM
                  AND SGRSTSP_TERM_CODE_EFF = P_TERM_CODE;
           
             IF V_STSP_CODE <> 'AS' THEN

                UPDATE SGRSTSP
                SET SGRSTSP_STSP_CODE = 'AS'
                WHERE SGRSTSP_PIDM = P_PIDM
                      AND SGRSTSP_TERM_CODE_EFF = P_TERM_CODE;
                
                COMMIT;
             END IF;
           END IF; 
      CLOSE C_SGRSTSP;         

      EXCEPTION
         WHEN OTHERS THEN
             P_MESSAGE := 'Error o inconsistencia detectada -'||SQLCODE||' -ERROR- '||SQLERRM;
             RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);          
END P_SET_REINCORPORACION;