create or replace PACKAGE BODY BWZKCSREI AS
/*******************************************************************************
 BWZKCSREI:
       Paquete Web _ Desarrollo Propio _ Paquete _ Conti SOLICITUD DE REINCORPORACIÒN
*******************************************************************************/
-- FILE NAME..: BWZKCSREI.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKCSREI
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/


PROCEDURE P_GET_INFO_STUDENT (
      P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
      P_PIDM              OUT SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_DEPT_CODE         OUT STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE         OUT STVCAMP.STVCAMP_CODE%TYPE,
      P_CAMP_DESC         OUT STVCAMP.STVCAMP_DESC%TYPE,
      P_PROGRAM           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_EMAIL             OUT GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE,
      P_NAME              OUT VARCHAR2      
)
/* ===================================================================================================================
  NOMBRE    : P_GET_INFO_STUDENT
  FECHA     : 05/12/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene información de un usuario(estudiante) 

  =================================================================================================================== */
AS
      -- @PARAMETERS
      V_INDICADOR         NUMBER;

      -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
      CURSOR C_CAMPDEPT IS
      SELECT    SORLCUR_CAMP_CODE,
                SORLCUR_PROGRAM,
                SORLFOS_DEPT_CODE,
                STVCAMP_DESC
      FROM (
              SELECT    SORLCUR_CAMP_CODE, SORLCUR_PROGRAM, SORLFOS_DEPT_CODE, STVCAMP_DESC
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              INNER JOIN STVCAMP
                    ON SORLCUR_CAMP_CODE = STVCAMP_CODE
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE =   'Y'
              ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM = 1;

BEGIN 
      
      -----------------------------------------------------------------------------
      -- GET nombres , ID
      SELECT (SPRIDEN_FIRST_NAME || ' ' || SPRIDEN_LAST_NAME), SPRIDEN_PIDM INTO P_NAME, P_PIDM  FROM SPRIDEN 
      WHERE SPRIDEN_ID = P_ID AND SPRIDEN_CHANGE_IND IS NULL;


      -----------------------------------------------------------------------------
      -- >> GET DEPARTAMENTO y CAMPUS --
      OPEN C_CAMPDEPT;
      LOOP
        FETCH C_CAMPDEPT INTO P_CAMP_CODE,P_PROGRAM,P_DEPT_CODE,P_CAMP_DESC ;
        EXIT WHEN C_CAMPDEPT%NOTFOUND;
      END LOOP;
      CLOSE C_CAMPDEPT;


      -----------------------------------------------------------------------------
      -- GET email
      SELECT COUNT(*) INTO V_INDICADOR FROM GOREMAL WHERE GOREMAL_PIDM = P_PIDM and GOREMAL_STATUS_IND = 'A';
      IF V_INDICADOR = 0 THEN
           P_EMAIL := '-';
      ELSE
              SELECT COUNT(*) INTO V_INDICADOR FROM GOREMAL WHERE GOREMAL_PIDM = P_PIDM and GOREMAL_STATUS_IND = 'A' 
              AND GOREMAL_EMAIL_ADDRESS LIKE ('%@continental.edu.pe');            
              IF (V_INDICADOR > 0) THEN
                  SELECT GOREMAL_EMAIL_ADDRESS INTO P_EMAIL FROM (
                    SELECT GOREMAL_EMAIL_ADDRESS, GOREMAL_PREFERRED_IND FROM GOREMAL 
                    WHERE GOREMAL_PIDM = P_PIDM and GOREMAL_STATUS_IND = 'A' AND GOREMAL_EMAIL_ADDRESS LIKE ('%@continental.edu.pe')
                    ORDER BY GOREMAL_PREFERRED_IND DESC
                  )WHERE ROWNUM <= 1;
              ELSE
                  SELECT GOREMAL_EMAIL_ADDRESS INTO P_EMAIL FROM (
                      SELECT GOREMAL_EMAIL_ADDRESS, GOREMAL_PREFERRED_IND FROM GOREMAL
                      WHERE GOREMAL_PIDM = P_PIDM and GOREMAL_STATUS_IND = 'A'
                      ORDER BY GOREMAL_PREFERRED_IND DESC
                  )WHERE ROWNUM <= 1;
              END IF;
      END IF;
      
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_INFO_STUDENT;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_REGLAS_VALIDARDATA (
      P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_DEPT_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_NEW          IN STVTERM.STVTERM_CODE%TYPE,
      P_DATOS_VALIDOS     OUT NUMBER
)
/* ===================================================================================================================
  NOMBRE    : P_GET_INFO_STUDENT
  FECHA     : 15/08/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida los datos segun als reglas establecidad para al FLUJO de REINCORPORACIÓN

  =================================================================================================================== */
AS
      -- @PARAMETERS
      V_INDICADOR         NUMBER;
      V_STST_CODE         SGBSTDN.SGBSTDN_STST_CODE%TYPE;
      V_STSP_CODE         SGRSTSP.SGRSTSP_STSP_CODE%TYPE;
      V_TERM_OLD          STVTERM.STVTERM_CODE%TYPE;
      V_SUB_PTRM          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_TERM_VALIDO       NUMBER; -- (0 / 1) Si el periodo ingreasado es valido

      -- GET estatus alumno FORMA GASTDN
      CURSOR C_SGBSTDN_STST IS
      SELECT SGBSTDN_TERM_CODE_EFF, SGBSTDN_STST_CODE FROM (
            SELECT SGBSTDN_TERM_CODE_EFF, SGBSTDN_STST_CODE 
            FROM SGBSTDN 
            WHERE SGBSTDN_PIDM = P_PIDM ORDER BY SGBSTDN_TERM_CODE_EFF DESC
      ) WHERE ROWNUM = 1;
      
      -- GET estatus alumno de BANNER-->PLAN ESTUDIO
      CURSOR C_SGRSTSP_STSP IS
      SELECT SGRSTSP_STSP_CODE FROM(
          SELECT SGRSTSP_STSP_CODE
          FROM SGRSTSP 
          WHERE SGRSTSP_PIDM = P_PIDM
          AND SGRSTSP_TERM_CODE_EFF = V_TERM_OLD
          ORDER BY SGRSTSP_KEY_SEQNO DESC
      ) WHERE ROWNUM = 1;
      
      -- Calculando parte PERIODO (sub PTRM)
      CURSOR C_SFRRSTS_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;
      
BEGIN 
      
      P_DATOS_VALIDOS := 0;
      
      -----------------------------------------------------------------------------
      -- >> GET ESTADIO DEL ALUMNO --
      OPEN C_SGBSTDN_STST;
      LOOP
        FETCH C_SGBSTDN_STST INTO V_TERM_OLD, V_STST_CODE;
        EXIT WHEN C_SGBSTDN_STST%NOTFOUND;
      END LOOP;
      CLOSE C_SGBSTDN_STST;

      -----------------------------------------------------------------------------
      -- GET estatus alumno de BANNER-->PLAN ESTUDIO
      OPEN C_SGRSTSP_STSP;
      LOOP
        FETCH C_SGRSTSP_STSP INTO V_STSP_CODE;
        EXIT WHEN C_SGRSTSP_STSP%NOTFOUND;
      END LOOP;
      CLOSE C_SGRSTSP_STSP;


      -- >> calculando PARTE PERIODO  --
      OPEN C_SFRRSTS_PTRM;
      LOOP
        FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
        EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
      END LOOP;
      CLOSE C_SFRRSTS_PTRM;      
      
      /******************************************************************************************************
        GET TERM (PERIODO - Solo usando parte de periodo 1) 
             DESDE INICIO DE MATRICULA(SFRRSTS_START_DATE)
             - HASTA FINALIZAR EL PERIODO DE CLASES(SOBPTRM_END_DATE)
      */
      SELECT COUNT(SOBPTRM_TERM_CODE) INTO V_TERM_VALIDO FROM (
            -- Forma SOATERM - SFARSTS  ---> fechas para las partes de periodo
            SELECT DISTINCT SOBPTRM_TERM_CODE, SOBPTRM_PTRM_CODE
            FROM SOBPTRM
            INNER JOIN SFRRSTS
              ON SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
              AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
            WHERE SFRRSTS_RSTS_CODE = 'RW'
            AND SOBPTRM_PTRM_CODE = (V_SUB_PTRM || '1') -- Solo parte de periodo '%1'
            -- AND SOBPTRM_PTRM_CODE NOT LIKE '%0'         -- VERANO
            AND SOBPTRM_TERM_CODE = P_TERM_NEW
            AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')  AND (SOBPTRM_END_DATE)
      ) WHERE ROWNUM <= 1;
      

      /******************************************************************************************************/
      -- CHECK REGLAS
      IF (
            (V_STST_CODE = 'IS' AND (V_STSP_CODE = 'AB' OR V_STSP_CODE = 'RV' OR V_STSP_CODE = 'AS'))
            OR  
            (V_STST_CODE = 'AS' AND (V_STSP_CODE = 'AB' OR V_STSP_CODE = 'RV'))
          )
          AND V_TERM_VALIDO > 0 THEN
                P_DATOS_VALIDOS := 1;
      END IF;
  

  
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_REGLAS_VALIDARDATA;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_CAMBIAR_ESTADO_ALUMNO( 
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_TERM_CODE           IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_CAMBIAR_ESTADO_ALUMNO
  FECHA     : 10/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Cambia los estados de un alumno(forma SGASTDN) : Plan Estudio, Status Alumno, Tipo Alumno
                - Plan Estudio    :  'RE' - Reincorporado
                - Status Alumno   :  'AS' - Activo
                - Tipo Alumno     :  'R' - UC Reincorporado

  MODIFICACIONES
  NRO     FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */
AS
        V_SGRSTSP_KEY_SEQNO     SGRSTSP.SGRSTSP_KEY_SEQNO%TYPE;
        V_SGRSTSP_REC           SGRSTSP%ROWTYPE;
        V_SGBSTDN_REC           SGBSTDN%ROWTYPE;
        V_SORLCUR_REC           SORLCUR%ROWTYPE;
        V_SORLFOS_REC           SORLFOS%ROWTYPE;
        V_SORLCUR_SEQNO_OLD     SORLCUR.SORLCUR_SEQNO%TYPE;
        V_SORLCUR_SEQNO_INC     SORLCUR.SORLCUR_SEQNO%TYPE;
        V_SORLCUR_SEQNO_NEW     SORLCUR.SORLCUR_SEQNO%TYPE;
        V_SORLFOS_SEQNO         SORLFOS.SORLFOS_SEQNO%TYPE;
        V_CURRICULA             EXCEPTION;
        
        /*----------------------------------------------------------------------------------
        -- INSERTAR: PLAN ESTUDIO (SGRSTSP)   
              - No confundir con PERIODO CATALOGO.
              - Nuevo Registro para la Evidencia que alumno realizó Reincorporación
        */
        CURSOR C_SGRSTSP IS
        SELECT * FROM (
            --Ultimo registro cercano al P_TERM_CODE
            SELECT *  FROM SGRSTSP 
            WHERE SGRSTSP_PIDM = P_PIDM
            AND SGRSTSP_TERM_CODE_EFF <= P_TERM_CODE
            ORDER BY SGRSTSP_KEY_SEQNO DESC
        ) WHERE ROWNUM = 1;
        
        
        -- INSERTAR:  SGASTDN 
        CURSOR C_SGBSTDN IS
        SELECT * FROM (
            SELECT *
            FROM SGBSTDN WHERE SGBSTDN_PIDM = P_PIDM
            ORDER BY SGBSTDN_TERM_CODE_EFF DESC
         )WHERE ROWNUM = 1
         AND NOT EXISTS (
            SELECT *
            FROM SGBSTDN WHERE SGBSTDN_PIDM = P_PIDM
            AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE
         );
        

        -- INSERTAR:  CURRICULA Parte-1
        CURSOR C_SORLCUR IS
        SELECT * FROM (
            SELECT SORLCUR.*
            FROM SORLCUR        INNER JOIN SORLFOS
                  ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                  AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
            WHERE   SORLCUR_PIDM = P_PIDM
            AND SORLFOS_CSTS_CODE IN ('INPROGRESS','STUDYPATH') -- Estados disponibles en CURRICULUM(hasta el momento)
            AND SORLCUR_LMOD_CODE   = 'LEARNER'
            AND SORLCUR_CACT_CODE   = 'ACTIVE' 
            AND SORLCUR_CURRENT_CDE = 'Y' --------------------- CURRENT CURRICULUM
            AND SORLFOS_CURRENT_CDE = 'Y' --------------------- CURRENT CURRICULUM
            --AND SORLCUR_ROLL_IND  = 'Y' -- Indicador Y / N si el currículo del alumno pasaa a historia académica
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
        )WHERE ROWNUM = 1;
        
        -- INSERTAR:  CURRICULA Parte-2
        CURSOR C_SORLFOS IS
        SELECT * FROM (
            SELECT * FROM SORLFOS 
            WHERE SORLFOS_PIDM = P_PIDM
            AND SORLFOS_LCUR_SEQNO = V_SORLCUR_SEQNO_OLD
            AND SORLFOS_CACT_CODE = 'ACTIVE'
            AND SORLFOS_CSTS_CODE IN ('INPROGRESS','STUDYPATH')
            AND SORLFOS_CURRENT_CDE = 'Y' --------------------- CURRENT CURRICULUM
        )WHERE ROWNUM = 1;

BEGIN
        
        /* #######################################################################
        -- INSERT  ------> PLAN ESTUDIO (SGRSTSP)   
              - No confundir con PERIODO CATALOGO.
              - Nuevo Registro para la Evidencia que alumno realizó Reincorporación
        */
        OPEN C_SGRSTSP;
        LOOP
            FETCH C_SGRSTSP INTO V_SGRSTSP_REC;
            EXIT WHEN C_SGRSTSP%NOTFOUND;
            
            -- GET SGRSTSP_KEY_SEQNO NUEVO
            SELECT SGRSTSP_KEY_SEQNO + 1 INTO V_SGRSTSP_KEY_SEQNO FROM (
                SELECT SGRSTSP_KEY_SEQNO  FROM SGRSTSP 
                WHERE SGRSTSP_PIDM = P_PIDM
                ORDER BY SGRSTSP_KEY_SEQNO DESC
            ) WHERE ROWNUM = 1;

            
            INSERT INTO SGRSTSP (
                    SGRSTSP_PIDM,       SGRSTSP_TERM_CODE_EFF,  SGRSTSP_KEY_SEQNO,
                    SGRSTSP_STSP_CODE,  SGRSTSP_ACTIVITY_DATE,  SGRSTSP_DATA_ORIGIN,
                    SGRSTSP_USER_ID,    SGRSTSP_FULL_PART_IND,  SGRSTSP_SESS_CODE,
                    SGRSTSP_RESD_CODE,  SGRSTSP_ORSN_CODE,      SGRSTSP_PRAC_CODE,
                    SGRSTSP_CAPL_CODE,  SGRSTSP_EDLV_CODE,      SGRSTSP_INCM_CODE,
                    SGRSTSP_EMEX_CODE,  SGRSTSP_APRN_CODE,      SGRSTSP_TRCN_CODE,
                    SGRSTSP_GAIN_CODE,  SGRSTSP_VOED_CODE,      SGRSTSP_BLCK_CODE,
                    SGRSTSP_EGOL_CODE,  SGRSTSP_BSKL_CODE,      SGRSTSP_ASTD_CODE,
                    SGRSTSP_PREV_CODE,  SGRSTSP_CAST_CODE ) 
            SELECT 
                    V_SGRSTSP_REC.SGRSTSP_PIDM,       P_TERM_CODE,                          V_SGRSTSP_KEY_SEQNO,
                    'AS',                             SYSDATE,                              'WorkFlow',
                    USER,                             V_SGRSTSP_REC.SGRSTSP_FULL_PART_IND,  V_SGRSTSP_REC.SGRSTSP_SESS_CODE,
                    V_SGRSTSP_REC.SGRSTSP_RESD_CODE,  V_SGRSTSP_REC.SGRSTSP_ORSN_CODE,      V_SGRSTSP_REC.SGRSTSP_PRAC_CODE,
                    V_SGRSTSP_REC.SGRSTSP_CAPL_CODE,  V_SGRSTSP_REC.SGRSTSP_EDLV_CODE,      V_SGRSTSP_REC.SGRSTSP_INCM_CODE,
                    V_SGRSTSP_REC.SGRSTSP_EMEX_CODE,  V_SGRSTSP_REC.SGRSTSP_APRN_CODE,      V_SGRSTSP_REC.SGRSTSP_TRCN_CODE,
                    V_SGRSTSP_REC.SGRSTSP_GAIN_CODE,  V_SGRSTSP_REC.SGRSTSP_VOED_CODE,      V_SGRSTSP_REC.SGRSTSP_BLCK_CODE,
                    V_SGRSTSP_REC.SGRSTSP_EGOL_CODE,  V_SGRSTSP_REC.SGRSTSP_BSKL_CODE,      V_SGRSTSP_REC.SGRSTSP_ASTD_CODE,
                    V_SGRSTSP_REC.SGRSTSP_PREV_CODE,  V_SGRSTSP_REC.SGRSTSP_CAST_CODE
            FROM DUAL;              
        END LOOP;
        CLOSE C_SGRSTSP;
              
              
        /* #######################################################################
        -- INSERT ó UPDATE ------> SGBSTDN -
            -- ACTUALIZA registro para dicho periodo ó lo INSERTA cuando no existe
        */
        
        -- UPDATE SGASTDN 
        UPDATE SGBSTDN
        SET  SGBSTDN_STST_CODE = 'AS'  ------------------ ESTADOS STVSTST : 'AS' - Activo
            ,SGBSTDN_STYP_CODE = 'R'   ------------------ ESTADOS STVSTYP : 'C' - Regular
            ,SGBSTDN_ACTIVITY_DATE = SYSDATE
            ,SGBSTDN_DATA_ORIGIN = 'WorkFlow'
            ,SGBSTDN_USER_ID = USER
        WHERE SGBSTDN_PIDM = P_PIDM
        AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE;

        -- INSERT SGASTDN
        OPEN C_SGBSTDN;
        LOOP
            FETCH C_SGBSTDN INTO V_SGBSTDN_REC;
            EXIT WHEN C_SGBSTDN%NOTFOUND;
            
              INSERT INTO SGBSTDN (
                        SGBSTDN_PIDM,                 SGBSTDN_TERM_CODE_EFF,          SGBSTDN_STST_CODE,
                        SGBSTDN_LEVL_CODE,            SGBSTDN_STYP_CODE,              SGBSTDN_TERM_CODE_MATRIC,
                        SGBSTDN_TERM_CODE_ADMIT,      SGBSTDN_EXP_GRAD_DATE,          SGBSTDN_CAMP_CODE,
                        SGBSTDN_FULL_PART_IND,        SGBSTDN_SESS_CODE,              SGBSTDN_RESD_CODE,
                        SGBSTDN_COLL_CODE_1,          SGBSTDN_DEGC_CODE_1,            SGBSTDN_MAJR_CODE_1,
                        SGBSTDN_MAJR_CODE_MINR_1,     SGBSTDN_MAJR_CODE_MINR_1_2,     SGBSTDN_MAJR_CODE_CONC_1,
                        SGBSTDN_MAJR_CODE_CONC_1_2,   SGBSTDN_MAJR_CODE_CONC_1_3,     SGBSTDN_COLL_CODE_2,
                        SGBSTDN_DEGC_CODE_2,          SGBSTDN_MAJR_CODE_2,            SGBSTDN_MAJR_CODE_MINR_2,
                        SGBSTDN_MAJR_CODE_MINR_2_2,   SGBSTDN_MAJR_CODE_CONC_2,       SGBSTDN_MAJR_CODE_CONC_2_2,
                        SGBSTDN_MAJR_CODE_CONC_2_3,   SGBSTDN_ORSN_CODE,              SGBSTDN_PRAC_CODE,
                        SGBSTDN_ADVR_PIDM,            SGBSTDN_GRAD_CREDIT_APPR_IND,   SGBSTDN_CAPL_CODE,
                        SGBSTDN_LEAV_CODE,            SGBSTDN_LEAV_FROM_DATE,         SGBSTDN_LEAV_TO_DATE,
                        SGBSTDN_ASTD_CODE,            SGBSTDN_TERM_CODE_ASTD,         SGBSTDN_RATE_CODE,
                        SGBSTDN_ACTIVITY_DATE,        SGBSTDN_MAJR_CODE_1_2,          SGBSTDN_MAJR_CODE_2_2,
                        SGBSTDN_EDLV_CODE,            SGBSTDN_INCM_CODE,              SGBSTDN_ADMT_CODE,
                        SGBSTDN_EMEX_CODE,            SGBSTDN_APRN_CODE,              SGBSTDN_TRCN_CODE,
                        SGBSTDN_GAIN_CODE,            SGBSTDN_VOED_CODE,              SGBSTDN_BLCK_CODE,
                        SGBSTDN_TERM_CODE_GRAD,       SGBSTDN_ACYR_CODE,              SGBSTDN_DEPT_CODE,
                        SGBSTDN_SITE_CODE,            SGBSTDN_DEPT_CODE_2,            SGBSTDN_EGOL_CODE,
                        SGBSTDN_DEGC_CODE_DUAL,       SGBSTDN_LEVL_CODE_DUAL,         SGBSTDN_DEPT_CODE_DUAL,
                        SGBSTDN_COLL_CODE_DUAL,       SGBSTDN_MAJR_CODE_DUAL,         SGBSTDN_BSKL_CODE,
                        SGBSTDN_PRIM_ROLL_IND,        SGBSTDN_PROGRAM_1,              SGBSTDN_TERM_CODE_CTLG_1,
                        SGBSTDN_DEPT_CODE_1_2,        SGBSTDN_MAJR_CODE_CONC_121,     SGBSTDN_MAJR_CODE_CONC_122,
                        SGBSTDN_MAJR_CODE_CONC_123,   SGBSTDN_SECD_ROLL_IND,          SGBSTDN_TERM_CODE_ADMIT_2,
                        SGBSTDN_ADMT_CODE_2,          SGBSTDN_PROGRAM_2,              SGBSTDN_TERM_CODE_CTLG_2,
                        SGBSTDN_LEVL_CODE_2,          SGBSTDN_CAMP_CODE_2,            SGBSTDN_DEPT_CODE_2_2,
                        SGBSTDN_MAJR_CODE_CONC_221,   SGBSTDN_MAJR_CODE_CONC_222,     SGBSTDN_MAJR_CODE_CONC_223,
                        SGBSTDN_CURR_RULE_1,          SGBSTDN_CMJR_RULE_1_1,          SGBSTDN_CCON_RULE_11_1,
                        SGBSTDN_CCON_RULE_11_2,       SGBSTDN_CCON_RULE_11_3,         SGBSTDN_CMJR_RULE_1_2,
                        SGBSTDN_CCON_RULE_12_1,       SGBSTDN_CCON_RULE_12_2,         SGBSTDN_CCON_RULE_12_3,
                        SGBSTDN_CMNR_RULE_1_1,        SGBSTDN_CMNR_RULE_1_2,          SGBSTDN_CURR_RULE_2,
                        SGBSTDN_CMJR_RULE_2_1,        SGBSTDN_CCON_RULE_21_1,         SGBSTDN_CCON_RULE_21_2,
                        SGBSTDN_CCON_RULE_21_3,       SGBSTDN_CMJR_RULE_2_2,          SGBSTDN_CCON_RULE_22_1,
                        SGBSTDN_CCON_RULE_22_2,       SGBSTDN_CCON_RULE_22_3,         SGBSTDN_CMNR_RULE_2_1,
                        SGBSTDN_CMNR_RULE_2_2,        SGBSTDN_PREV_CODE,              SGBSTDN_TERM_CODE_PREV,
                        SGBSTDN_CAST_CODE,            SGBSTDN_TERM_CODE_CAST,         SGBSTDN_DATA_ORIGIN,
                        SGBSTDN_USER_ID,              SGBSTDN_SCPC_CODE ) 
              VALUES (  V_SGBSTDN_REC.SGBSTDN_PIDM,                 P_TERM_CODE,                                  'AS', /*AS-Activo*/ 
                        V_SGBSTDN_REC.SGBSTDN_LEVL_CODE,            'R',/*R-Reincorporado*/                       V_SGBSTDN_REC.SGBSTDN_TERM_CODE_MATRIC,
                        V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT,      V_SGBSTDN_REC.SGBSTDN_EXP_GRAD_DATE,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE,
                        V_SGBSTDN_REC.SGBSTDN_FULL_PART_IND,        V_SGBSTDN_REC.SGBSTDN_SESS_CODE,              V_SGBSTDN_REC.SGBSTDN_RESD_CODE,
                        V_SGBSTDN_REC.SGBSTDN_COLL_CODE_1,          V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_1,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1,
                        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1_2,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1,
                        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_3,     V_SGBSTDN_REC.SGBSTDN_COLL_CODE_2,
                        V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2,
                        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_2,
                        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_3,   V_SGBSTDN_REC.SGBSTDN_ORSN_CODE,              V_SGBSTDN_REC.SGBSTDN_PRAC_CODE,
                        V_SGBSTDN_REC.SGBSTDN_ADVR_PIDM,            V_SGBSTDN_REC.SGBSTDN_GRAD_CREDIT_APPR_IND,   V_SGBSTDN_REC.SGBSTDN_CAPL_CODE,
                        V_SGBSTDN_REC.SGBSTDN_LEAV_CODE,            V_SGBSTDN_REC.SGBSTDN_LEAV_FROM_DATE,         V_SGBSTDN_REC.SGBSTDN_LEAV_TO_DATE,
                        V_SGBSTDN_REC.SGBSTDN_ASTD_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ASTD,         V_SGBSTDN_REC.SGBSTDN_RATE_CODE,
                        SYSDATE,                                    V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2_2,
                        V_SGBSTDN_REC.SGBSTDN_EDLV_CODE,            V_SGBSTDN_REC.SGBSTDN_INCM_CODE,              V_SGBSTDN_REC.SGBSTDN_ADMT_CODE,
                        V_SGBSTDN_REC.SGBSTDN_EMEX_CODE,            V_SGBSTDN_REC.SGBSTDN_APRN_CODE,              V_SGBSTDN_REC.SGBSTDN_TRCN_CODE,
                        V_SGBSTDN_REC.SGBSTDN_GAIN_CODE,            V_SGBSTDN_REC.SGBSTDN_VOED_CODE,              V_SGBSTDN_REC.SGBSTDN_BLCK_CODE,
                        V_SGBSTDN_REC.SGBSTDN_TERM_CODE_GRAD,       V_SGBSTDN_REC.SGBSTDN_ACYR_CODE,              V_SGBSTDN_REC.SGBSTDN_DEPT_CODE,
                        V_SGBSTDN_REC.SGBSTDN_SITE_CODE,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2,            V_SGBSTDN_REC.SGBSTDN_EGOL_CODE,
                        V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_DUAL,
                        V_SGBSTDN_REC.SGBSTDN_COLL_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_BSKL_CODE,
                        V_SGBSTDN_REC.SGBSTDN_PRIM_ROLL_IND,        V_SGBSTDN_REC.SGBSTDN_PROGRAM_1,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CTLG_1,
                        V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_1_2,        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_121,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_122,
                        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_123,   V_SGBSTDN_REC.SGBSTDN_SECD_ROLL_IND,          V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT_2,
                        V_SGBSTDN_REC.SGBSTDN_ADMT_CODE_2,          V_SGBSTDN_REC.SGBSTDN_PROGRAM_2,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CTLG_2,
                        V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_2,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE_2,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2_2,
                        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_221,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_222,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_223,
                        V_SGBSTDN_REC.SGBSTDN_CURR_RULE_1,          V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_1,          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_1,
                        V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_3,         V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_2,
                        V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_1,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_2,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_3,
                        V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_1,        V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_2,          V_SGBSTDN_REC.SGBSTDN_CURR_RULE_2,
                        V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_1,        V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_1,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_2,
                        V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_3,       V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_2,          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_1,
                        V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_3,         V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_1,
                        V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_2,        V_SGBSTDN_REC.SGBSTDN_PREV_CODE,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_PREV,
                        V_SGBSTDN_REC.SGBSTDN_CAST_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CAST,         'WorkFlow',
                        USER,                                       V_SGBSTDN_REC.SGBSTDN_SCPC_CODE );
        END LOOP;
        CLOSE C_SGBSTDN;
              

        -- #######################################################################
        -- INSERT  ------> SORLCUR -
        OPEN C_SORLCUR;
        LOOP
            FETCH C_SORLCUR INTO V_SORLCUR_REC;
            EXIT WHEN C_SORLCUR%NOTFOUND;    
            

            -- GET SORLCUR_SEQNO DEL REGISTRO INACTIVO 
            SELECT MAX(SORLCUR_SEQNO + 1) INTO V_SORLCUR_SEQNO_INC  FROM SORLCUR WHERE SORLCUR_PIDM = V_SORLCUR_REC.SORLCUR_PIDM;
            
            -- GET SORLCUR_SEQNO NUEVO 
            SELECT MAX(SORLCUR_SEQNO + 2) INTO V_SORLCUR_SEQNO_NEW  FROM SORLCUR WHERE SORLCUR_PIDM = V_SORLCUR_REC.SORLCUR_PIDM;
            
            -- GET SORLCUR_SEQNO ANTERIOR 
            V_SORLCUR_SEQNO_OLD := V_SORLCUR_REC.SORLCUR_SEQNO;

            -- INACTIVE --- SORLCUR - (1 DE 2)
            INSERT INTO SORLCUR (
                        SORLCUR_PIDM,             SORLCUR_SEQNO,                SORLCUR_LMOD_CODE,
                        SORLCUR_TERM_CODE,        SORLCUR_KEY_SEQNO,            SORLCUR_PRIORITY_NO,
                        SORLCUR_ROLL_IND,         SORLCUR_CACT_CODE,            SORLCUR_USER_ID,
                        SORLCUR_DATA_ORIGIN,      SORLCUR_ACTIVITY_DATE,        SORLCUR_LEVL_CODE,
                        SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,            SORLCUR_TERM_CODE_CTLG,
                        SORLCUR_TERM_CODE_END,    SORLCUR_TERM_CODE_MATRIC,     SORLCUR_TERM_CODE_ADMIT,
                        SORLCUR_ADMT_CODE,        SORLCUR_CAMP_CODE,            SORLCUR_PROGRAM,
                        SORLCUR_START_DATE,       SORLCUR_END_DATE,             SORLCUR_CURR_RULE,
                        SORLCUR_ROLLED_SEQNO,     SORLCUR_STYP_CODE,            SORLCUR_RATE_CODE,
                        SORLCUR_LEAV_CODE,        SORLCUR_LEAV_FROM_DATE,       SORLCUR_LEAV_TO_DATE,
                        SORLCUR_EXP_GRAD_DATE,    SORLCUR_TERM_CODE_GRAD,       SORLCUR_ACYR_CODE,
                        SORLCUR_SITE_CODE,        SORLCUR_APPL_SEQNO,           SORLCUR_APPL_KEY_SEQNO,
                        SORLCUR_USER_ID_UPDATE,   SORLCUR_ACTIVITY_DATE_UPDATE, SORLCUR_GAPP_SEQNO,
                        SORLCUR_CURRENT_CDE ) 
            VALUES (    V_SORLCUR_REC.SORLCUR_PIDM,             V_SORLCUR_SEQNO_INC,                        V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                        P_TERM_CODE,                            V_SORLCUR_REC.SORLCUR_KEY_SEQNO,            V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                        V_SORLCUR_REC.SORLCUR_ROLL_IND,         'INACTIVE',/*SORLCUR_CACT_CODE*/            USER,
                        V_SORLCUR_REC.SORLCUR_DATA_ORIGIN,      SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                        V_SORLCUR_REC.SORLCUR_COLL_CODE,        V_SORLCUR_REC.SORLCUR_DEGC_CODE,            V_SORLCUR_REC.SORLCUR_TERM_CODE_CTLG,
                        P_TERM_CODE,                            V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                        V_SORLCUR_REC.SORLCUR_ADMT_CODE,        V_SORLCUR_REC.SORLCUR_CAMP_CODE,            V_SORLCUR_REC.SORLCUR_PROGRAM,
                        V_SORLCUR_REC.SORLCUR_START_DATE,       V_SORLCUR_REC.SORLCUR_END_DATE,             V_SORLCUR_REC.SORLCUR_CURR_RULE,
                        V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,     V_SORLCUR_REC.SORLCUR_STYP_CODE,            V_SORLCUR_REC.SORLCUR_RATE_CODE,
                        V_SORLCUR_REC.SORLCUR_LEAV_CODE,        V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                        V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,    V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                        V_SORLCUR_REC.SORLCUR_SITE_CODE,        V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                        USER,                                   SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                        NULL );
            
            -- ACTIVE --- SORLCUR - (2 DE 2)
            INSERT INTO SORLCUR (
                          SORLCUR_PIDM,             SORLCUR_SEQNO,                SORLCUR_LMOD_CODE,
                          SORLCUR_TERM_CODE,        SORLCUR_KEY_SEQNO,            SORLCUR_PRIORITY_NO,
                          SORLCUR_ROLL_IND,         SORLCUR_CACT_CODE,            SORLCUR_USER_ID,
                          SORLCUR_DATA_ORIGIN,      SORLCUR_ACTIVITY_DATE,        SORLCUR_LEVL_CODE,
                          SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,            SORLCUR_TERM_CODE_CTLG,
                          SORLCUR_TERM_CODE_END,    SORLCUR_TERM_CODE_MATRIC,     SORLCUR_TERM_CODE_ADMIT,
                          SORLCUR_ADMT_CODE,        SORLCUR_CAMP_CODE,            SORLCUR_PROGRAM,
                          SORLCUR_START_DATE,       SORLCUR_END_DATE,             SORLCUR_CURR_RULE,
                          SORLCUR_ROLLED_SEQNO,     SORLCUR_STYP_CODE,            SORLCUR_RATE_CODE,
                          SORLCUR_LEAV_CODE,        SORLCUR_LEAV_FROM_DATE,       SORLCUR_LEAV_TO_DATE,
                          SORLCUR_EXP_GRAD_DATE,    SORLCUR_TERM_CODE_GRAD,       SORLCUR_ACYR_CODE,
                          SORLCUR_SITE_CODE,        SORLCUR_APPL_SEQNO,           SORLCUR_APPL_KEY_SEQNO,
                          SORLCUR_USER_ID_UPDATE,   SORLCUR_ACTIVITY_DATE_UPDATE, SORLCUR_GAPP_SEQNO,
                          SORLCUR_CURRENT_CDE ) 
              VALUES (    V_SORLCUR_REC.SORLCUR_PIDM,             V_SORLCUR_SEQNO_NEW,                        V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                          P_TERM_CODE,                            V_SGRSTSP_KEY_SEQNO,/*SGRSTSP*/             V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                          V_SORLCUR_REC.SORLCUR_ROLL_IND,         V_SORLCUR_REC.SORLCUR_CACT_CODE,            USER,
                          'WorkFlow',                             SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                          V_SORLCUR_REC.SORLCUR_COLL_CODE,        V_SORLCUR_REC.SORLCUR_DEGC_CODE,            V_SORLCUR_REC.SORLCUR_TERM_CODE_CTLG,
                          P_TERM_CODE,                            V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                          V_SORLCUR_REC.SORLCUR_ADMT_CODE,        V_SORLCUR_REC.SORLCUR_CAMP_CODE,            V_SORLCUR_REC.SORLCUR_PROGRAM,
                          V_SORLCUR_REC.SORLCUR_START_DATE,       V_SORLCUR_REC.SORLCUR_END_DATE,             V_SORLCUR_REC.SORLCUR_CURR_RULE,
                          V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,     V_SORLCUR_REC.SORLCUR_STYP_CODE,            V_SORLCUR_REC.SORLCUR_RATE_CODE,
                          V_SORLCUR_REC.SORLCUR_LEAV_CODE,        V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                          V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,    V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                          V_SORLCUR_REC.SORLCUR_SITE_CODE,        V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                          USER,                                   SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                          'Y' );
            
            -- UPDATE TERM_CODE_END(vigencia curriculum) 
            UPDATE SORLCUR 
            SET   SORLCUR_TERM_CODE_END = P_TERM_CODE, 
                  SORLCUR_ACTIVITY_DATE_UPDATE = SYSDATE
            WHERE   SORLCUR_PIDM = P_PIDM 
            AND     SORLCUR_SEQNO = V_SORLCUR_SEQNO_OLD;
            
            -- UPDATE SORLCUR_CURRENT_CDE(curriculum activo)  -- Solo 1 curricula activa por PERIODO
            UPDATE SORLCUR 
            SET   SORLCUR_CURRENT_CDE = NULL
            WHERE   SORLCUR_PIDM = P_PIDM 
            AND     SORLCUR_TERM_CODE_END = P_TERM_CODE -- En caso el registro sea del mismo Periodo
            AND     SORLCUR_SEQNO = V_SORLCUR_SEQNO_OLD;    
            
        END LOOP;
        CLOSE C_SORLCUR;
              

        -- #######################################################################
        -- INSERT ------> SORLFOS -
        OPEN C_SORLFOS;
        LOOP
              FETCH C_SORLFOS INTO V_SORLFOS_REC;
              EXIT WHEN C_SORLFOS%NOTFOUND;  

              V_SORLFOS_SEQNO := V_SORLFOS_REC.SORLFOS_SEQNO;

              -- INSERT --- SORLFOS - (1 DE 2) 
              INSERT INTO SORLFOS (
                          SORLFOS_PIDM,                   SORLFOS_LCUR_SEQNO,           SORLFOS_SEQNO,
                          SORLFOS_LFST_CODE,              SORLFOS_TERM_CODE,            SORLFOS_PRIORITY_NO,
                          SORLFOS_CSTS_CODE,              SORLFOS_CACT_CODE,            SORLFOS_DATA_ORIGIN,
                          SORLFOS_USER_ID,                SORLFOS_ACTIVITY_DATE,        SORLFOS_MAJR_CODE,
                          SORLFOS_TERM_CODE_CTLG,         SORLFOS_TERM_CODE_END,        SORLFOS_DEPT_CODE,
                          SORLFOS_MAJR_CODE_ATTACH,       SORLFOS_LFOS_RULE,            SORLFOS_CONC_ATTACH_RULE,
                          SORLFOS_START_DATE,             SORLFOS_END_DATE,             SORLFOS_TMST_CODE,
                          SORLFOS_ROLLED_SEQNO,           SORLFOS_USER_ID_UPDATE,       SORLFOS_ACTIVITY_DATE_UPDATE,
                          SORLFOS_CURRENT_CDE) 
              VALUES (    V_SORLFOS_REC.SORLFOS_PIDM,             V_SORLCUR_SEQNO_INC,                      1,/*V_SORLFOS_REC.SORLFOS_SEQNO*/
                          V_SORLFOS_REC.SORLFOS_LFST_CODE,        P_TERM_CODE,                              V_SORLFOS_REC.SORLFOS_PRIORITY_NO,
                          'CHANGED'/*SORLFOS_CSTS_CODE*/,         'INACTIVE'/*SORLFOS_CACT_CODE*/,          V_SORLFOS_REC.SORLFOS_DATA_ORIGIN,
                          USER,                                   SYSDATE,                                  V_SORLFOS_REC.SORLFOS_MAJR_CODE,
                          V_SORLFOS_REC.SORLFOS_TERM_CODE_CTLG,   V_SORLFOS_REC.SORLFOS_TERM_CODE_END,      V_SORLFOS_REC.SORLFOS_DEPT_CODE,
                          V_SORLFOS_REC.SORLFOS_MAJR_CODE_ATTACH, V_SORLFOS_REC.SORLFOS_LFOS_RULE,          V_SORLFOS_REC.SORLFOS_CONC_ATTACH_RULE,
                          V_SORLFOS_REC.SORLFOS_START_DATE,       V_SORLFOS_REC.SORLFOS_END_DATE,           V_SORLFOS_REC.SORLFOS_TMST_CODE,
                          V_SORLFOS_REC.SORLFOS_ROLLED_SEQNO,     USER,                                     SYSDATE,
                          NULL /*SORLFOS_CURRENT_CDE*/);
        
              -- INSERT --- SORLFOS - (2 DE 2)
              INSERT INTO SORLFOS (
                          SORLFOS_PIDM,                   SORLFOS_LCUR_SEQNO,           SORLFOS_SEQNO,
                          SORLFOS_LFST_CODE,              SORLFOS_TERM_CODE,            SORLFOS_PRIORITY_NO,
                          SORLFOS_CSTS_CODE,              SORLFOS_CACT_CODE,            SORLFOS_DATA_ORIGIN,
                          SORLFOS_USER_ID,                SORLFOS_ACTIVITY_DATE,        SORLFOS_MAJR_CODE,
                          SORLFOS_TERM_CODE_CTLG,         SORLFOS_TERM_CODE_END,        SORLFOS_DEPT_CODE,
                          SORLFOS_MAJR_CODE_ATTACH,       SORLFOS_LFOS_RULE,            SORLFOS_CONC_ATTACH_RULE,
                          SORLFOS_START_DATE,             SORLFOS_END_DATE,             SORLFOS_TMST_CODE,
                          SORLFOS_ROLLED_SEQNO,           SORLFOS_USER_ID_UPDATE,       SORLFOS_ACTIVITY_DATE_UPDATE,
                          SORLFOS_CURRENT_CDE) 
              VALUES (    V_SORLFOS_REC.SORLFOS_PIDM,                 V_SORLCUR_SEQNO_NEW,                      1,/*V_SORLFOS_REC.SORLFOS_SEQNO*/
                          V_SORLFOS_REC.SORLFOS_LFST_CODE,            P_TERM_CODE,                              V_SORLFOS_REC.SORLFOS_PRIORITY_NO,
                          'INPROGRESS'/*SORLFOS_CSTS_CODE*/,          V_SORLFOS_REC.SORLFOS_CACT_CODE,          'WorkFlow',
                          USER,                                       SYSDATE,                                  V_SORLFOS_REC.SORLFOS_MAJR_CODE,
                          V_SORLFOS_REC.SORLFOS_TERM_CODE_CTLG,       V_SORLFOS_REC.SORLFOS_TERM_CODE_END,      V_SORLFOS_REC.SORLFOS_DEPT_CODE,
                          V_SORLFOS_REC.SORLFOS_MAJR_CODE_ATTACH,     V_SORLFOS_REC.SORLFOS_LFOS_RULE,          V_SORLFOS_REC.SORLFOS_CONC_ATTACH_RULE,
                          V_SORLFOS_REC.SORLFOS_START_DATE,           V_SORLFOS_REC.SORLFOS_END_DATE,           V_SORLFOS_REC.SORLFOS_TMST_CODE,
                          V_SORLFOS_REC.SORLFOS_ROLLED_SEQNO,         USER,                                     SYSDATE,
                          'Y');
        
        END LOOP;
        CLOSE C_SORLFOS;

        -- UPDATE SORLFOS_CURRENT_CDE(curriculum activo) PARA registros del mismo PERIODO.
        UPDATE SORLFOS 
        SET   SORLFOS_CURRENT_CDE = NULL
        WHERE   SORLFOS_PIDM = P_PIDM
        AND     SORLFOS_LCUR_SEQNO = V_SORLCUR_SEQNO_OLD 
        AND     SORLFOS_CSTS_CODE IN ('INPROGRESS','STUDYPATH');
      
      ------------------------------------------------------------------------------------
      
      --##################################################################################
      -- Validar la creación de la CURRICULA (SORLCUR y SORLFOS)
      IF (V_SORLCUR_SEQNO_OLD IS NULL OR V_SORLFOS_SEQNO IS NULL) THEN
              RAISE V_CURRICULA;
      END IF;

      --
      COMMIT;

EXCEPTION
    WHEN V_CURRICULA THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| '- La Currícula (SORLCUR y SORLFOS) no fue creada: No data fount.' );
    WHEN OTHERS THEN
          --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_CAMBIAR_ESTADO_ALUMNO;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_CREATE_CTACORRIENTE (
      P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
      P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_DEPT_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
      P_PROGRAM           IN SOBCURR.SOBCURR_PROGRAM%TYPE
)
/* ===================================================================================================================
  NOMBRE    : P_CREATE_CTACORRIENTE
  FECHA     : 23/08/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Asigna escala y crea la CTA Corriente de un alumno
              Valida Cargo de CARNET para poder asignarle.

MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  001   08/11/2017    KARANA        Se agregó el filtro de NuevoPlan=0
  =================================================================================================================== */
AS
      -- @PARAMETERS
      V_APEC_CAMP             VARCHAR2(9);
      V_APEC_TERM             VARCHAR2(9);
      V_APEC_DEPT             VARCHAR2(9);
      V_APEC_SECCIONC         VARCHAR2(20);
      V_NUM_CTACORRIENTE      NUMBER;
      V_PART_PERIODO          VARCHAR2(9);
      V_RESULT                INTEGER;

      V_ESCALA                VARCHAR2(9);
      V_CRONOGRAMA            VARCHAR2(9);
      V_OBSERVACION           VARCHAR(255) := 'Asignación de escala - Reincorporación WORKFLOW';
BEGIN 
--
        --**************************************************************************************************************
        -- GET CRONOGRAMA SECCIONC
        SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                  WHEN 'UPGT' THEN 'W' 
                                  WHEN 'UREG' THEN 'R' 
                                  WHEN 'UPOS' THEN '-' 
                                  WHEN 'ITEC' THEN '-' 
                                  WHEN 'UCIC' THEN '-' 
                                  WHEN 'UCEC' THEN '-' 
                                  WHEN 'ICEC' THEN '-' 
                                  ELSE '1' END ||
                CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                  WHEN 'F01' THEN 'A' 
                                  WHEN 'F02' THEN 'L' 
                                  WHEN 'F03' THEN 'C' 
                                  WHEN 'V00' THEN 'V' 
                                  ELSE '9' END
        INTO V_PART_PERIODO
        FROM STVCAMP,STVDEPT 
        WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;        
        
        SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
        SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
        SELECT CZRDEPT_DEPT_BDUCCI INTO V_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO
        
        -- Validar si el estuidnate tiene CTA CORRIENTE. como C00(CONCPETO MATRICULA)
        WITH 
            CTE_tblSeccionC AS (
                    -- GET SECCIONC
                    SELECT  "IDSeccionC" IDSeccionC,
                            "FecInic" FecInic
                    FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                    WHERE "IDDependencia"='UCCI'
                    AND "IDsede"    = V_APEC_CAMP
                    AND "IDPerAcad" = V_APEC_TERM
                    AND "IDEscuela" = P_PROGRAM
                    AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                    AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                    AND "IDSeccionC" <> '15NEX1A'
                    AND SUBSTRB("IDSeccionC",-2,2) IN (
                        -- PARTE PERIODO           
                        SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (V_PART_PERIODO || '%')
                    )
            )
        SELECT COUNT("IDAlumno") INTO V_NUM_CTACORRIENTE
        FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDependencia" = 'UCCI'
        AND "IDAlumno"    = P_ID
        AND "IDSede"      = V_APEC_CAMP
        AND "IDPerAcad"   = V_APEC_TERM
        AND "IDEscuela"   = P_PROGRAM
        AND "IDSeccionC"  = V_APEC_SECCIONC
        AND "IDConcepto" = 'C00'; -- C00 Concepto Matricul


        -- Get ESCALA y CRONOGRAMA
        SELECT "Cronograma", "Reincorporacion" REINCORPORACION INTO V_CRONOGRAMA, V_ESCALA
        FROM tblCronogramas@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDepartamento" = P_DEPT_CODE
        AND "IDCampus" = P_CAMP_CODE
        AND "Activo" = 1
        AND "Pronabec" <> 1
        AND "NuevoPlan" = 0;


        -----------------------------------------------------------------------------------------------
        --- Asignacion de escala y Creacion CTA CORRIENTE.
        IF(V_NUM_CTACORRIENTE = 0) THEN
              ---------------------------------------
              /* OBSERVACIÒN: Para pruebas acceder al SCRIPT y comentar y descomentar la conexcion de "AT BANNER" --> "AT MIGR"
                    dentro del SP : sp_ActualizarMontoPagarBanner(YA INCLUIDO en el SP dbo.sp_AsignarEscala_CtaCorriente)
              */
              ---------------------------------------
              V_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
                    'dbo.sp_AsignarEscala_CtaCorriente "'
                    || 'UCCI' ||'" , "'|| P_ID ||'" , "'|| V_APEC_TERM ||'" , "'|| V_APEC_DEPT ||'" , "'|| V_APEC_CAMP ||'" , "'|| V_ESCALA ||'" , "'|| V_CRONOGRAMA ||'" , "'|| V_OBSERVACION ||'" , "'|| USER ||'"' 
              );
        END IF;
        
        COMMIT;
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CREATE_CTACORRIENTE;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


------------ APEC -----------------
PROCEDURE P_SET_CARGO_CONCEPTO ( 
        P_ID                    IN SPRIDEN.SPRIDEN_ID%TYPE,
        P_PIDM                  IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_DEPT_CODE             IN STVDEPT.STVDEPT_CODE%TYPE,
        P_CAMP_CODE             IN STVCAMP.STVCAMP_CODE%TYPE,
        P_TERM_CODE             IN STVTERM.STVTERM_CODE%TYPE,
        P_APEC_CONCEPTO         IN TBBDETC.TBBDETC_DETAIL_CODE%TYPE,
        P_PROGRAM               IN SOBCURR.SOBCURR_PROGRAM%TYPE,
        P_MESSAGE               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SET_CARGO_CONCEPTO
  FECHA     : 23/08/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : APEC-BDUCCI: Asigna cargo al estudiante con el CONCEPTO.
              Valida que solo tenga el cargo una vez al año.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
          -- @PARAMETERS
      V_APEC_CAMP             VARCHAR2(9);
      V_APEC_TERM             VARCHAR2(9);
      V_APEC_DEPT             VARCHAR2(9);
      V_APEC_SECCIONC         VARCHAR2(20);
      V_PART_PERIODO          VARCHAR2(9);
      V_CARGO_CARNET          NUMBER := 0;
    
BEGIN
--    
      --**************************************************************************************************************
      -- GET CRONOGRAMA SECCIONC
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END
      INTO V_PART_PERIODO
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;        
      
      SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
      SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
      SELECT CZRDEPT_DEPT_BDUCCI INTO V_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO

      
      --Get SECCIONC
      WITH 
          CTE_tblSeccionC AS (
                  -- GET SECCIONC
                  SELECT  "IDSeccionC" IDSeccionC,
                          "FecInic" FecInic
                  FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                  WHERE "IDDependencia"='UCCI'
                  AND "IDsede"    = V_APEC_CAMP
                  AND "IDPerAcad" = V_APEC_TERM
                  AND "IDEscuela" = P_PROGRAM
                  AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                  AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                  AND "IDSeccionC" <> '15NEX1A'
                  AND SUBSTRB("IDSeccionC",-2,2) IN (
                      -- PARTE PERIODO           
                      SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (V_PART_PERIODO || '%')
                  )
          )
      SELECT "IDSeccionC" INTO V_APEC_SECCIONC
      FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
      WHERE "IDDependencia" = 'UCCI'
      AND "IDAlumno"    = P_ID
        AND "IDSede"      = V_APEC_CAMP
        AND "IDPerAcad"   = V_APEC_TERM
        AND "IDEscuela"   = P_PROGRAM
        AND "IDSeccionC"  IN (SELECT IDSeccionC FROM CTE_tblSeccionC)
        AND "IDConcepto" = 'C00'; -- C00 Concepto Matricula


      /*******
         -- Get CARGO de CARNET "CAR"
      ********/
      SELECT  "Monto"
      INTO    V_CARGO_CARNET
      FROM dbo.tblConceptos@BDUCCI.CONTINENTAL.EDU.PE  t1 
      INNER JOIN dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE  t2
        ON t1."IDSede"          = t2."IDsede"
        AND t1."IDPerAcad"      = t2."IDPerAcad" 
        AND t1."IDDependencia"  = t2."IDDependencia" 
        AND t1."IDSeccionC"     = t2."IDSeccionC" 
        AND t1."FecInic"        = t2."FecInic"
      WHERE t2."IDDependencia" = 'UCCI'
        AND t2."IDSeccionC"    = V_APEC_SECCIONC
        AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_PSI' -- internado
        AND SUBSTRB(t2."IDSeccionC",1,7) <> 'INT_ENF' -- internado
        AND t2."IDSeccionC"     <> '15NEX1A'
        AND t2."IDPerAcad"     = V_APEC_TERM
        AND T1."IDPerAcad"     = V_APEC_TERM
        AND t1."IDConcepto"    = P_APEC_CONCEPTO;

      /**********************************************************************************************
          -- ASINGANDO el cargo CARNET a CTACORRIENTE
      ********/
      UPDATE dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
          SET "Cargo" = "Cargo" + V_CARGO_CARNET  
      WHERE "IDDependencia" = 'UCCI' 
      AND "IDAlumno"    = P_ID
      AND "IDSede"      = V_APEC_CAMP 
      AND "IDPerAcad"   = V_APEC_TERM 
      AND "IDEscuela"   = P_PROGRAM
      AND "IDSeccionC"  = V_APEC_SECCIONC
      AND "IDConcepto"  = P_APEC_CONCEPTO; 

      COMMIT;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_SET_CARGO_CONCEPTO;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_SET_GRP_INSCRIPC_ABIERTO (
      P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
      P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SET_GRP_INSCRIPC_ABIERTO
  FECHA     : 05/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Asignar el código de grupo de inscripción del estudiante.

                          Ejemplo: 99-99WF01
------------------------------------------------------------------------------
    Rectificación               Modalidad                 Sede            
99-99 – Rectificación     R – Regular                   S01 – Huancayo
                          W – Gente que trabaja         F01 – Arequipa 
                          V – Virtual                   F02 – Lima  
                                                        F03 – Cusco
                                                        V00 – Virtual

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
AS
      V_RPGRP_NEW           SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      V_RPGRP_OLD           SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      V_DEPT_CAMP           VARCHAR2(4);
      
      V_INDICADOR           NUMBER := 0;
      V_COD_RECTF           VARCHAR2(15) := '99-99';
      V_ROW_UPDATE          NUMBER := 0;
      V_GRP_INSCRIPCION     EXCEPTION;
      
      -- Si tiene GRUPO INSCRIPCION
      CURSOR C_SFBRGRP_RGRP IS
      SELECT SFBRGRP_RGRP_CODE
      FROM SFBRGRP 
      WHERE SFBRGRP_PIDM = P_PIDM
      AND SFBRGRP_TERM_CODE = P_TERM_CODE;
      
BEGIN
-- 
      ------------------------------------------------------------
      -- Si tiene GRUPO INSCRIPCION
      OPEN C_SFBRGRP_RGRP;
      LOOP
        FETCH C_SFBRGRP_RGRP INTO V_RPGRP_OLD;
        EXIT WHEN C_SFBRGRP_RGRP%NOTFOUND;
      END LOOP;
      CLOSE C_SFBRGRP_RGRP;
      
      
      -- #######################################################################
      -- VALIDACION : CODIGO grupo insc. y datos proporcionados
      SELECT CASE P_DEPT_CODE 
                WHEN 'UVIR' THEN 'V' --#UC-SEMI PRESENCIAL (EV)
                WHEN 'UPGT' THEN 'W' --#UC-SEMI PRESENCIAL (GT)
                WHEN 'UREG' THEN 'R' --#UC-PRESENCIAL
                ELSE '' END 
            || P_CAMP_CODE INTO V_DEPT_CAMP FROM DUAL;
      -- CODIGO DEL GRUPO DE RECTIFICACIÒN : 99-99<cod_modadlidad><cod_sede>
      SELECT CONCAT(V_COD_RECTF,V_DEPT_CAMP) INTO V_RPGRP_NEW FROM DUAL;
      
      
      IF V_RPGRP_OLD IS NULL THEN
            
            -- ASIGNAR GRUPO DE INSCRIPCION - forma SFARGRP
            INSERT INTO SFBRGRP (
                  SFBRGRP_TERM_CODE, 
                  SFBRGRP_PIDM, 
                  SFBRGRP_RGRP_CODE, 
                  SFBRGRP_USER, 
                  SFBRGRP_ACTIVITY_DATE,
                  SFBRGRP_DATA_ORIGIN
              )
            SELECT P_TERM_CODE, P_PIDM, V_RPGRP_NEW, USER, SYSDATE , 'WorkFlow'
            FROM DUAL
            WHERE EXISTS (
                  ------ LA DEFINICION DE LA VISTA SFVRGRP
                  SELECT SFBWCTL.SFBWCTL_TERM_CODE,
                  SFBWCTL.SFBWCTL_RGRP_CODE,
                  SFBWCTL.SFBWCTL_PRIORITY,
                  SFRWCTL.SFRWCTL_BEGIN_DATE,
                  SFRWCTL.SFRWCTL_END_DATE,
                        SFRWCTL.SFRWCTL_HOUR_BEGIN,
                        SFRWCTL.SFRWCTL_HOUR_END
                  FROM SFRWCTL, SFBWCTL
                        WHERE SFRWCTL.SFRWCTL_TERM_CODE = SFBWCTL.SFBWCTL_TERM_CODE
                        AND   SFRWCTL.SFRWCTL_PRIORITY  = SFBWCTL.SFBWCTL_PRIORITY
                        AND   SFBWCTL.SFBWCTL_RGRP_CODE = V_RPGRP_NEW
                        AND   SFRWCTL.SFRWCTL_TERM_CODE = P_TERM_CODE
            ) AND ROWNUM = 1;
            V_ROW_UPDATE := SQL%ROWCOUNT;
            
      ELSE
            
            -- ASIGNAR GRUPO DE RECTIFICACIÓN
            UPDATE SFBRGRP 
              SET SFBRGRP_RGRP_CODE     = V_RPGRP_NEW,
                  SFBRGRP_USER          = USER,
                  SFBRGRP_ACTIVITY_DATE = SYSDATE
            WHERE SFBRGRP_PIDM = P_PIDM
            AND SFBRGRP_TERM_CODE = P_TERM_CODE
            AND EXISTS (
                  ------ LA DEFINICION DE LA VISTA SFVRGRP
                  SELECT SFBWCTL.SFBWCTL_TERM_CODE,
                    SFBWCTL.SFBWCTL_RGRP_CODE,
                    SFBWCTL.SFBWCTL_PRIORITY,
                    SFRWCTL.SFRWCTL_BEGIN_DATE,
                    SFRWCTL.SFRWCTL_END_DATE,
                          SFRWCTL.SFRWCTL_HOUR_BEGIN,
                          SFRWCTL.SFRWCTL_HOUR_END
                    FROM SFRWCTL, SFBWCTL
                          WHERE SFRWCTL.SFRWCTL_TERM_CODE = SFBWCTL.SFBWCTL_TERM_CODE
                          AND   SFRWCTL.SFRWCTL_PRIORITY  = SFBWCTL.SFBWCTL_PRIORITY
                          AND   SFBWCTL.SFBWCTL_RGRP_CODE = V_RPGRP_NEW
                          AND   SFRWCTL.SFRWCTL_TERM_CODE = P_TERM_CODE
            );
            V_ROW_UPDATE := SQL%ROWCOUNT;
           
      END IF;
      
      IF V_ROW_UPDATE <> 1 THEN
            RAISE V_GRP_INSCRIPCION;
      END IF;

      COMMIT;

EXCEPTION
  WHEN V_GRP_INSCRIPCION THEN
          P_ERROR := '- No se logró asignar grupo de inscripción, "No data found".';
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||SQLERRM );
  WHEN OTHERS THEN
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||' -ERROR- '||SQLERRM );
END P_SET_GRP_INSCRIPC_ABIERTO;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_STI_EXECCAPP (
      P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_PROGRAM           IN SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
      P_MESSAGE           OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_STI_EXECCAPP
  FECHA     : 08/06/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Ejecución de CAPP.
  =================================================================================================================== */
AS
    -- @PARAMETERS
    LV_OUT            VARCHAR2(4000);
    P_EXCEPTION       EXCEPTION;
    --
BEGIN 
--
    SZKECAP.P_EXEC_CAPP(P_PIDM,P_PROGRAM,P_TERM_CODE,LV_OUT);
    IF(LV_OUT <> '0')THEN
        P_MESSAGE := LV_OUT;
        RAISE P_EXCEPTION;
    END IF;
    
EXCEPTION
  WHEN P_EXCEPTION THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
  WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_STI_EXECCAPP;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_STI_EXECPROY (
      P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_PROGRAM           IN SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
      P_MESSAGE           OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_STI_EXECPROY
  FECHA     : 08/06/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Ejecución de PROYECCION.
  =================================================================================================================== */
AS
    -- @PARAMETERS
    LV_OUT            VARCHAR2(4000);
    P_EXCEPTION       EXCEPTION;
    --
BEGIN 
--
    SZKECAP.P_EXEC_PROY(P_PIDM, P_PROGRAM, P_TERM_CODE, LV_OUT);
    IF(LV_OUT <> '0')THEN
        P_MESSAGE := LV_OUT;
        RAISE P_EXCEPTION;
    END IF;
    
EXCEPTION
  WHEN P_EXCEPTION THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE|| P_MESSAGE );
  WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_STI_EXECPROY;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--                   
END BWZKCSREI;