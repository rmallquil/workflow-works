/*
SET SERVEROUTPUT ON
DECLARE P_FECHA_VALIDA VARCHAR2(10);
BEGIN
    P_CREATE_CTACORRIENTE('43317482',326495,'UPGT','S01','201720','110');
END;
*/


CREATE OR REPLACE PROCEDURE P_CREATE_CTACORRIENTE (
      P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
      P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_DEPT_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
      P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
      P_PROGRAM           IN SOBCURR.SOBCURR_PROGRAM%TYPE
)
/* ===================================================================================================================
  NOMBRE    : P_CREATE_CTACORRIENTE
  FECHA     : 23/08/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Asigna escala y crea la CTA Corriente de un alumno
              Valida Cargo de CARNET para poder asignarle.

MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  =================================================================================================================== */
AS
      -- @PARAMETERS
      V_APEC_CAMP             VARCHAR2(9);
      V_APEC_TERM             VARCHAR2(9);
      V_APEC_DEPT             VARCHAR2(9);
      V_APEC_SECCIONC         VARCHAR2(20);
      V_NUM_CTACORRIENTE      NUMBER;
      V_PART_PERIODO          VARCHAR2(9);
      V_RESULT                INTEGER;

      V_ESCALA                VARCHAR2(9);
      V_CRONOGRAMA            VARCHAR2(9);
      V_OBSERVACION           VARCHAR(255) := 'Asignación de escala - Reincorporación WORKFLOW';
BEGIN 
--
        --**************************************************************************************************************
        -- GET CRONOGRAMA SECCIONC
        SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                  WHEN 'UPGT' THEN 'W' 
                                  WHEN 'UREG' THEN 'R' 
                                  WHEN 'UPOS' THEN '-' 
                                  WHEN 'ITEC' THEN '-' 
                                  WHEN 'UCIC' THEN '-' 
                                  WHEN 'UCEC' THEN '-' 
                                  WHEN 'ICEC' THEN '-' 
                                  ELSE '1' END ||
                CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                  WHEN 'F01' THEN 'A' 
                                  WHEN 'F02' THEN 'L' 
                                  WHEN 'F03' THEN 'C' 
                                  WHEN 'V00' THEN 'V' 
                                  ELSE '9' END
        INTO V_PART_PERIODO
        FROM STVCAMP,STVDEPT 
        WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;        
        
        SELECT CZRCAMP_CAMP_BDUCCI INTO V_APEC_CAMP FROM CZRCAMP WHERE CZRCAMP_CODE = P_CAMP_CODE;-- CAMPUS 
        SELECT CZRTERM_TERM_BDUCCI INTO V_APEC_TERM FROM CZRTERM WHERE CZRTERM_CODE = P_TERM_CODE;-- PERIODO
        SELECT CZRDEPT_DEPT_BDUCCI INTO V_APEC_DEPT FROM CZRDEPT WHERE CZRDEPT_CODE = P_DEPT_CODE;-- DEPARTAMENTO
        
        -- Validar si el estuidnate tiene CTA CORRIENTE. como C00(CONCPETO MATRICULA)
        WITH 
            CTE_tblSeccionC AS (
                    -- GET SECCIONC
                    SELECT  "IDSeccionC" IDSeccionC,
                            "FecInic" FecInic
                    FROM dbo.tblSeccionC@BDUCCI.CONTINENTAL.EDU.PE 
                    WHERE "IDDependencia"='UCCI'
                    AND "IDsede"    = V_APEC_CAMP
                    AND "IDPerAcad" = V_APEC_TERM
                    AND "IDEscuela" = P_PROGRAM
                    AND SUBSTRB("IDSeccionC",1,7) <> 'INT_PSI' -- internado
                    AND SUBSTRB("IDSeccionC",1,7) <> 'INT_ENF' -- internado
                    AND "IDSeccionC" <> '15NEX1A'
                    AND SUBSTRB("IDSeccionC",-2,2) IN (
                        -- PARTE PERIODO           
                        SELECT CZRPTRM_PTRM_BDUCCI FROM CZRPTRM WHERE CZRPTRM_CODE LIKE (V_PART_PERIODO || '%')
                    )
            )
        SELECT COUNT("IDAlumno") INTO V_NUM_CTACORRIENTE
        FROM dbo.tblCtaCorriente@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDependencia" = 'UCCI'
        AND "IDAlumno"    = P_ID
        AND "IDSede"      = V_APEC_CAMP
        AND "IDPerAcad"   = V_APEC_TERM
        AND "IDEscuela"   = P_PROGRAM
        AND "IDSeccionC"  = V_APEC_SECCIONC
        AND "IDConcepto" = 'C00'; -- C00 Concepto Matricul


        -- Get ESCALA y CRONOGRAMA
        SELECT "Cronograma", "Reincorporacion" REINCORPORACION INTO V_CRONOGRAMA, V_ESCALA
        FROM tblCronogramas@BDUCCI.CONTINENTAL.EDU.PE 
        WHERE "IDDepartamento" = P_DEPT_CODE
        AND "IDCampus" = P_CAMP_CODE
        AND "Activo" = 1
        AND "Pronabec" <> 1;


        -----------------------------------------------------------------------------------------------
        --- Asignacion de escala y Creacion CTA CORRIENTE.
        IF(V_NUM_CTACORRIENTE = 0) THEN
              ---------------------------------------
              /* OBSERVACIÒN: Para pruebas acceder al SCRIPT y comentar y descomentar la conexcion de "AT BANNER" --> "AT MIGR"
                    dentro del SP : sp_ActualizarMontoPagarBanner(YA INCLUIDO en el SP dbo.sp_AsignarEscala_CtaCorriente)
              */
              ---------------------------------------
              V_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
                    'dbo.sp_AsignarEscala_CtaCorriente "'
                    || 'UCCI' ||'" , "'|| P_ID ||'" , "'|| V_APEC_TERM ||'" , "'|| V_APEC_DEPT ||'" , "'|| V_APEC_CAMP ||'" , "'|| V_ESCALA ||'" , "'|| V_CRONOGRAMA ||'" , "'|| V_OBSERVACION ||'" , "'|| USER ||'"' 
              );
        END IF;
        
        COMMIT;
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CREATE_CTACORRIENTE;