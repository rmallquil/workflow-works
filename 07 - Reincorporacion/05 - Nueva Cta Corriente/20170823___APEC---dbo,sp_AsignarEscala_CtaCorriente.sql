/**************************************************************
        SP de BDUCCI ---------------> APEC
**************************************************************/

/* PERMISOS - IMPORTANTE */ 
--        grant execute on [dbo].[sp_AsignarEscala_CtaCorriente]  to [SSLSCDataCBanner]

/**************************************************************/

USE [BDUCCI]
GO
/* ===================================================================================================================
NOMBRE    : [dbo].[sp_AsignarEscala_CtaCorriente]
FECHA   : 17/08/2017
AUTOR   : Richard Mallqui Lopez 
OBJETIVO  : Asignar Escala y crea Cuenta Corriente, 
        **Basado en el script proporcionado por BFLORESV BNE.sp_Escala_Traslado

MODIFICACIONES
NRO   FECHA           USUARIO     MODIFICACION
001   23/07/2017      rmallqui    Ahroa los parametros de cronograma y escaala deben ser enviados, adiconalmente 0 / 1 para asignarle cargo de CARNET
=================================================================================================================== */
CREATE PROCEDURE [dbo].[sp_AsignarEscala_CtaCorriente]
  @c_IDDependencia    VARCHAR(100),
  @c_IDAlumno         VARCHAR(100),
  @c_IDPerAcad        VARCHAR(100),
  @c_IDEscuelaADM     VARCHAR(100),
  @c_IDSede           VARCHAR(100),
  @c_IDEscala         VARCHAR(2),
  @c_Cronograma       VARCHAR(3),
  @c_Observacion      VARCHAR(255),
  @c_Personal         VARCHAR(50)
AS
     
BEGIN 
-----   
  
  SET NOCOUNT ON;
  SET FMTONLY OFF
  BEGIN TRY
    BEGIN TRANSACTION
      
      IF @c_Cronograma IS NULL OR @c_IDEscala IS NULL BEGIN
        RAISERROR (15600,-1,-1,'Inconsistencia detectada: No se encontro la escala y/o cronograma.'); 
      END;

      IF NOT EXISTS (SELECT IDAlumno FROM tblAlumnoEscala 
              where IDAlumno = @c_IDAlumno 
                and IDSede = @c_IDSede 
                and Cronograma  = @c_Cronograma 
                and IDPersonal  = @c_Personal
                and Observacion = @c_Observacion)
      BEGIN
          --ASIGNACIÓN DE ESCALA
          INSERT INTO tblAlumnoEscala (
              IDSede,         IDDependencia,  IDAlumno,         IDPerAcad,
              IDSeccionC,     FecInic,        FechaEscala,      IDEscala,
              Beca,           Cronograma,     IDEscala1,        IDEscala2, 
              IDEscala3,      IDEscala4,      IDEscala5,        IDEscala6,
              Beca1,          Beca2,          Beca3,            Beca4,
              Beca5,          Beca6,          Fechavalidez,     IDPersonal, 
              Observacion,    IDConcepto,     Reincorporacion,  Variacion,
              ModPens,        NroCred,        Descuento,        MontoPagar,
              MasCredNormal,  Ultimo
          )
          SELECT DISTINCT TOP 1 
              @c_IDSede,        @c_IDDependencia,     @c_IDAlumno,    @c_IDPerAcad,
              'XESCALA',        '01/01/1980',         GETDATE(),      @c_IDEscala,
              0,                @c_Cronograma,        @c_IDEscala,    @c_IDEscala, 
              @c_IDEscala,      @c_IDEscala,          @c_IDEscala,    @c_IDEscala,
              0,                0,                    0,              0,
              0,                0,                    GETDATE(),      @c_Personal,
              @c_Observacion,   'C00',                '0',            NULL, 
              NULL,             NULL,                 NULL,           NULL, 
              NULL,             '0'
      END

      exec [dbo].[sp_ActualizarMontoPagarBanner] @c_IDDependencia,@c_IDSede,@c_IDPerAcad,@c_IDAlumno
  
      exec [BNE].[sp_Actualizar_Registro_SME_Alumnoestado] @c_IDAlumno,@c_IDPerAcad,@c_IDSede,@c_IDDependencia

      COMMIT TRANSACTION

  END TRY
  BEGIN CATCH

      --INSTRUCCIONES EN CASO DE ERRORES
      DECLARE @ErrorMessage NVARCHAR(4000);  
      DECLARE @ErrorSeverity INT;  
      DECLARE @ErrorState INT;  

      SELECT   
        @ErrorMessage = ERROR_MESSAGE(),  
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorState = ERROR_STATE();  

      -- Use RAISERROR inside the CATCH block to return error  
      -- information about the original error that caused  
      -- execution to jump to the CATCH block.  
      RAISERROR (
        @ErrorMessage, -- Message text.  
        @ErrorSeverity, -- Severity.  
        @ErrorState -- State.  
        );  
    
      ROLLBACK TRANSACTION

  END CATCH
END

GO 

grant execute on [dbo].[sp_AsignarEscala_CtaCorriente]  to [SSLSCDataCBanner]