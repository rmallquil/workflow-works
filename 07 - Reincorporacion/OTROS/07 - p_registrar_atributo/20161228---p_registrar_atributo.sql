/*
drop procedure p_set_coddetalle_alumno;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfobjects;
GRANT EXECUTE ON p_set_coddetalle_alumno TO wfauto;

SET SERVEROUTPUT ON
DECLARE   P_ERROR                 VARCHAR2(100);
BEGIN
    -- P_REGISTAR_ATRIBUTO ( PIDM , COD_ATRIBUTO , PERIODO , ERROR )
    P_REGISTAR_ATRIBUTO(183294,'201620','SME',P_ERROR);
    DBMS_OUTPUT.PUT_LINE( '--' || P_ERROR);
END;

*/

CREATE OR REPLACE PROCEDURE P_REGISTAR_ATRIBUTO (
      P_PIDM_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_PERIODO               IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
      P_COD_ATRIBUTO          IN STVATTS.STVATTS_CODE%TYPE,       -- COD atributos
      P_ERROR                 OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_REGISTAR_ATRIBUTO
  FECHA     : 28/12/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida y Agrega un atributo relacionado.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS

        
        P_PERIODO_NEXT              SFBETRM.SFBETRM_TERM_CODE%TYPE;
        NOM_SERVICIO                VARCHAR2(30);
        
        C_SEQNO                   SORLCUR.SORLCUR_SEQNO %type;
        C_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;
        C_ALUM_CAMPUS             SORLCUR.SORLCUR_CAMP_CODE%type;
        C_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
        C_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
        C_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
        C_ALUM_PERD_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
        C_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP
        C_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE
        C_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%type;
              
        P_COD_DETALLE           SFRRGFE.SFRRGFE_DETL_CODE%TYPE;
        P_INDICADOR             NUMBER;
        C_CARGO_MINIMO          SFRRGFE.SFRRGFE_MIN_CHARGE%TYPE;
        P_ROWID                 GB_COMMON.INTERNAL_RECORD_ID_TYPE;
        
        SAVE_ACT_DATE_OUT       VARCHAR2(100);
        RETURN_STATUS_IN_OUT    NUMBER;
BEGIN  
  
      -- GET - NEXT Periodo
      SELECT NEXT_TERM INTO P_PERIODO_NEXT
      FROM (
        SELECT STVTERM_CODE, LEAD(STVTERM_CODE, 1, 0) OVER (ORDER BY STVTERM_CODE) NEXT_TERM
        FROM STVTERM ORDER BY STVTERM_CODE DESC
      ) WHERE STVTERM_CODE = P_PERIODO;
    

      -- #######################################################################
      -- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
      SELECT    SORLCUR_SEQNO,      
                SORLCUR_LEVL_CODE,    
                SORLCUR_CAMP_CODE,        
                SORLCUR_COLL_CODE,
                SORLCUR_DEGC_CODE,  
                SORLCUR_PROGRAM,      
                SORLCUR_TERM_CODE_ADMIT,  
                --SORLCUR_STYP_CODE,
                SORLCUR_STYP_CODE,  
                SORLCUR_RATE_CODE,    
                SORLFOS_DEPT_CODE   
      INTO      C_SEQNO,
                C_ALUM_NIVEL, 
                C_ALUM_CAMPUS, 
                C_ALUM_ESCUELA, 
                C_ALUM_GRADO, 
                C_ALUM_PROGRAMA, 
                C_ALUM_PERD_ADM,
                C_ALUM_TIPO_ALUM,
                C_ALUM_TARIFA,
                C_ALUM_DEPARTAMENTO
      FROM (
              SELECT    SORLCUR_SEQNO,      SORLCUR_LEVL_CODE,    SORLCUR_CAMP_CODE,        SORLCUR_COLL_CODE,
                        SORLCUR_DEGC_CODE,  SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  --SORLCUR_STYP_CODE,
                        SORLCUR_STYP_CODE,  SORLCUR_RATE_CODE,    SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM_ALUMNO 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_TERM_CODE   =   P_PERIODO 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
              ORDER BY SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM <= 1;

   
      -- GET - Codigo detalle y tambien VALIDA que si se encuentre configurado correctamente.
      SELECT SFRRGFE_DETL_CODE, SFRRGFE_MIN_CHARGE INTO P_COD_DETALLE, C_CARGO_MINIMO
      FROM SFRRGFE 
      WHERE SFRRGFE_TERM_CODE = P_PERIODO AND SFRRGFE_ATTS_CODE = P_COD_ATRIBUTO 
      AND SFRRGFE_TYPE = 'STUDENT'
      AND NVL(NVL(SFRRGFE_LEVL_CODE, c_alum_nivel),'-')           = NVL(NVL(c_alum_nivel, SFRRGFE_LEVL_CODE),'-')---------------- Nivel
      AND NVL(NVL(SFRRGFE_CAMP_CODE, c_alum_campus),'-')          = NVL(NVL(c_alum_campus, SFRRGFE_CAMP_CODE),'-')  ------------- Campus (sede)
      AND NVL(NVL(SFRRGFE_COLL_CODE, c_alum_escuela),'-')         = NVL(NVL(c_alum_escuela, SFRRGFE_COLL_CODE),'-') ------------- Escuela 
      AND NVL(NVL(SFRRGFE_DEGC_CODE, c_alum_grado),'-')           = NVL(NVL(c_alum_grado, SFRRGFE_DEGC_CODE),'-') --------------- Grado
      AND NVL(NVL(SFRRGFE_PROGRAM, c_alum_programa),'-')          = NVL(NVL(c_alum_programa, SFRRGFE_PROGRAM),'-') -------------- Programa
      AND NVL(NVL(SFRRGFE_TERM_CODE_ADMIT, c_alum_perd_adm),'-')  = NVL(NVL(c_alum_perd_adm, SFRRGFE_TERM_CODE_ADMIT),'-') ------ Periodo Admicion
      -- SFRRGFE_PRIM_SEC_CDE -- Curriculums (prim, secundario, cualquiera)
      -- SFRRGFE_LFST_CODE -- Tipo Campo Estudio (MAJOR, ...)
      -- SFRRGFE_MAJR_CODE -- Codigo Campo Estudio (Carrera)
      AND NVL(NVL(SFRRGFE_DEPT_CODE, c_alum_departamento),'-')    = NVL(NVL(c_alum_departamento, SFRRGFE_DEPT_CODE),'-') -------- Departamento
      -- SFRRGFE_LFST_PRIM_SEC_CDE -- Campo Estudio   (prim, secundario, cualquiera)
      AND NVL(NVL(SFRRGFE_STYP_CODE_CURRIC, c_alum_tipo_alum),'-') = NVL(NVL(c_alum_tipo_alum, SFRRGFE_STYP_CODE_CURRIC),'-') --- Tipo Alumno Curriculum
      AND NVL(NVL(SFRRGFE_RATE_CODE_CURRIC, c_alum_tarifa),'-')   = NVL(NVL(c_alum_tarifa, SFRRGFE_RATE_CODE_CURRIC),'-'); ------ Trf Curriculum (Escala)
      
      -- GET - Descripcion de CODIGO DETALLE  
      SELECT TBBDETC_DESC INTO NOM_SERVICIO FROM TBBDETC WHERE TBBDETC_DETAIL_CODE = P_COD_DETALLE;


      -- #######################################################################
      -- SET ATRIBUTO alumno
      INSERT INTO SGRSATT
        (
          SGRSATT_PIDM, 
          SGRSATT_TERM_CODE_EFF,
          SGRSATT_ATTS_CODE,
          SGRSATT_ACTIVITY_DATE,
          SGRSATT_STSP_KEY_SEQUENCE
        )
      SELECT P_PIDM_ALUMNO, P_PERIODO, P_COD_ATRIBUTO, SYSDATE, NULL
      FROM DUAL
      WHERE NOT EXISTS (SELECT * FROM SGRSATT 
                    WHERE SGRSATT_PIDM = P_PIDM_ALUMNO AND SGRSATT_TERM_CODE_EFF = P_PERIODO 
                    AND (SGRSATT_ATTS_CODE = P_COD_ATRIBUTO OR SGRSATT_ATTS_CODE IS NULL));
      P_INDICADOR := SQL%ROWCOUNT;  
      -- ACTUALIZAR en caso exista NULL con el ATRIBUTO para el periodo.
      IF P_INDICADOR = 0  THEN
          UPDATE SGRSATT SET SGRSATT_ATTS_CODE = P_COD_ATRIBUTO
          WHERE SGRSATT_PIDM = P_PIDM_ALUMNO AND SGRSATT_TERM_CODE_EFF = P_PERIODO 
          AND SGRSATT_ATTS_CODE IS NULL 
          AND NOT EXISTS (SELECT SGRSATT_PIDM FROM SGRSATT 
                    WHERE SGRSATT_PIDM = P_PIDM_ALUMNO AND SGRSATT_TERM_CODE_EFF = P_PERIODO
                    AND SGRSATT_ATTS_CODE = P_COD_ATRIBUTO);
      END IF;
   
      -- SET - ATRIBUTO FINALIZAR (LIMITAR VALIDES del atributo A UN SOLO PERIODO)
      INSERT INTO SGRSATT
        ( 
          SGRSATT_PIDM, 
          SGRSATT_TERM_CODE_EFF,
          SGRSATT_ATTS_CODE,
          SGRSATT_ACTIVITY_DATE,
          SGRSATT_STSP_KEY_SEQUENCE
        )
      SELECT P_PIDM_ALUMNO, P_PERIODO_NEXT, NULL, SYSDATE, NULL
      FROM DUAL
      WHERE NOT EXISTS (SELECT * FROM SGRSATT 
                        WHERE SGRSATT_PIDM = P_PIDM_ALUMNO 
                        AND SGRSATT_TERM_CODE_EFF = P_PERIODO_NEXT
                    );
      P_INDICADOR := SQL%ROWCOUNT;

      -- #######################################################################
      -- Procesar DEUDA - Volviendo a estimar la deuda devido al cambio de TARIFA.
      SFKFEES.p_processfeeassessment (  P_PERIODO,
                                        P_PIDM_ALUMNO,
                                        SYSDATE,      -- assessment effective date(Evaluación de la fecha efectiva)
                                        SYSDATE,  -- refund by total refund date(El reembolso por fecha total del reembolso)
                                        'R',          -- use regular assessment rules(utilizar las reglas de evaluación periódica)
                                        'Y',          -- create TBRACCD records
                                        'SFAREGS',    -- where assessment originated from
                                        'Y',          -- commit changes
                                        SAVE_ACT_DATE_OUT,    -- OUT -- save_act_date
                                        'N',          -- do not ignore SFRFMAX rules
                                        RETURN_STATUS_IN_OUT );   -- OUT -- return_status

        COMMIT;
EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
        --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
        P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_REGISTAR_ATRIBUTO;

