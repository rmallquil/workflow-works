
/*
SET SERVEROUTPUT ON
DECLARE   P_MONTO_REEMBOLSO     TBRACCD.TBRACCD_AMOUNT%TYPE;
          P_PERIODO_REEMBOLSO   TBRACCD.TBRACCD_TERM_CODE%TYPE;
          P_ERROR               VARCHAR2(300);
BEGIN
    P_OBTENER_REEMBOLSO(407099,'RFND',P_MONTO_REEMBOLSO,P_PERIODO_REEMBOLSO,P_ERROR);
    DBMS_OUTPUT.PUT_LINE(P_MONTO_REEMBOLSO || ' ---- ' ||  P_PERIODO_REEMBOLSO || ' ---- ' || P_ERROR);
END;
*/




CREATE OR REPLACE PROCEDURE P_OBTENER_REEMBOLSO (
  P_PIDM_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
  P_CODIGO_DETALLE      IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE,
  P_MONTO_REEMBOLSO     OUT TBRACCD.TBRACCD_AMOUNT%TYPE,
  P_PERIODO_REEMBOLSO   OUT TBRACCD.TBRACCD_TERM_CODE%TYPE,
  P_ERROR               OUT VARCHAR2
)              
/* ===================================================================================================================
  NOMBRE    : P_OBTENER_REEMBOLSO
  FECHA     : 05/01/201
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene el CARGO a un alumno por CODIGO DETALLE.
              ** No valida si se realizo movimientos ya que es un cargo especial estatico
                 ejemplo: "RFND"- Reembolso.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION  
  =================================================================================================================== */
AS
      PRAGMA AUTONOMOUS_TRANSACTION;
      P_INDICADOR           NUMBER;
BEGIN
--
  
    -- GET deuda
    SELECT COUNT(TBRACCD_AMOUNT) 
    INTO P_INDICADOR
    FROM (
        SELECT TBRACCD_AMOUNT FROM TBRACCD 
        WHERE TBRACCD_PIDM = P_PIDM_ALUMNO  AND TBRACCD_DETAIL_CODE = P_CODIGO_DETALLE
         ORDER BY TBRACCD_TERM_CODE DESC
    ) WHERE ROWNUM = 1;
    
    IF P_INDICADOR = 0 THEN
          P_MONTO_REEMBOLSO   := 0;
          P_PERIODO_REEMBOLSO := '-';
    ELSE
          SELECT TBRACCD_TERM_CODE, TBRACCD_AMOUNT
          INTO P_PERIODO_REEMBOLSO, P_MONTO_REEMBOLSO
          FROM (
                  SELECT TBRACCD_TERM_CODE, TBRACCD_AMOUNT 
                  FROM  TBRACCD 
                  WHERE TBRACCD_PIDM = P_PIDM_ALUMNO  
                  AND TBRACCD_DETAIL_CODE = P_CODIGO_DETALLE
                  ORDER BY TBRACCD_TERM_CODE DESC
          ) WHERE ROWNUM = 1;
    END IF;
    
EXCEPTION
  WHEN OTHERS THEN
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_OBTENER_REEMBOLSO;
