-- 72252413
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SET SERVEROUTPUT ON
declare     P_PIDM_ALUMNO         SPRIDEN.SPRIDEN_PIDM%TYPE := '263373';
            P_CODIGO_DETALLE      TBRACCD.TBRACCD_DETAIL_CODE%TYPE := 'RFND';
            P_MONTO_REEMBOLSO     TBRACCD.TBRACCD_AMOUNT%TYPE;
begin 
    --
    
    -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
    TZKCDAA.p_calc_deuda_alumno(406509,'PEN');
    COMMIT;
        
    -- GET deuda
    SELECT TZRCDAB_AMOUNT
    INTO   P_MONTO_REEMBOLSO
    FROM   TZRCDAB
    WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
    AND TZRCDAB_PIDM = '406509' 
    AND TZRCDAB_DETAIL_CODE = P_CODIGO_DETALLE;
    
    DBMS_OUTPUT.PUT_LINE(P_MONTO_REEMBOLSO);
end;


    SELECT *
    FROM   TZRCDAB
    WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
    AND TZRCDAB_PIDM = '406509'  
    AND TZRCDAB_DETAIL_CODE = 'RFND';


/*
SET SERVEROUTPUT ON
DECLARE   P_MONTO_REEMBOLSO     TBRACCD.TBRACCD_AMOUNT%TYPE;
          P_ERROR               VARCHAR2(300);
BEGIN
    P_OBTENER_REEMBOLSO(406509,'RFND',P_MONTO_REEMBOLSO,P_ERROR);
    DBMS_OUTPUT.PUT_LINE(P_MONTO_REEMBOLSO || ' ---- ' || P_ERROR);
END;
*/




CREATE OR REPLACE PROCEDURE P_OBTENER_REEMBOLSO (
  P_PIDM_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
  P_CODIGO_DETALLE      IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE,
  P_MONTO_REEMBOLSO     OUT TBRACCD.TBRACCD_AMOUNT%TYPE,
  P_ERROR               OUT VARCHAR2
)              
/* ===================================================================================================================
  NOMBRE    : P_OBTENER_REEMBOLSO
  FECHA     : 27/12/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene el CARGO de un alumno por CODIGO DETALLE.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION  
  =================================================================================================================== */
AS
      PRAGMA AUTONOMOUS_TRANSACTION;
      P_INDICADOR           NUMBER;
BEGIN
--
    
    -- API CUENTAS POR COBRAR, replicada para realizar consultas (package completo)
    TZKCDAA.p_calc_deuda_alumno(P_PIDM_ALUMNO,'PEN');
    COMMIT;
        
    -- GET deuda
    SELECT COUNT(TZRCDAB_AMOUNT)
    INTO   P_INDICADOR
    FROM   TZRCDAB
    WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
    AND TZRCDAB_PIDM = P_PIDM_ALUMNO 
    AND TZRCDAB_DETAIL_CODE = P_CODIGO_DETALLE;
    
    IF P_INDICADOR = 0 THEN
      P_MONTO_REEMBOLSO   := 0;
    ELSE
      SELECT TZRCDAB_AMOUNT
      INTO   P_MONTO_REEMBOLSO
      FROM   TZRCDAB
      WHERE  TZRCDAB_SESSION_ID = USERENV('SESSIONID')
      AND TZRCDAB_PIDM = P_PIDM_ALUMNO 
      AND TZRCDAB_DETAIL_CODE = P_CODIGO_DETALLE;
    END IF;

EXCEPTION
  WHEN OTHERS THEN
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_OBTENER_REEMBOLSO;
