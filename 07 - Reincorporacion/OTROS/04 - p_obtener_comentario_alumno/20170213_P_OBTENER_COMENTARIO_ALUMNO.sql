/*
SET SERVEROUTPUT ON
declare comentario varchar2(200);
comentario1 varchar2(200);
comentario2 varchar2(200);
comentario3 varchar2(200);
begin
    P_OBTENER_COMENTARIO_ALUMNO(119,comentario,comentario1,comentario2,comentario3);
    DBMS_OUTPUT.PUT_LINE(comentario || ' - ' || comentario1 || ' - ' || comentario2  || ' - ' || comentario3 );
end;
*/

CREATE OR REPLACE PROCEDURE P_OBTENER_COMENTARIO_ALUMNO (
      P_NUMERO_SOLICITUD        IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_COMENTARIO_ALUMNO       OUT VARCHAR2,
      P_DESC_CAMP               OUT VARCHAR2,
      P_DESC_DEPT               OUT VARCHAR2,
      P_DESC_PROG               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_OBTENER_COMENTARIO_ALUMNO
  FECHA     : 00/00/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obteiene el comentario ingresado por el alumno 
              (se adicionò el campus, departamento y el programa para que sea selecionado por el alumno)

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION  
  001   13/02/2017    rmallqui    adicionalmente se va a obtener la campus, departamento y el programa ingresados por el alumno.
  =================================================================================================================== */
AS
      P_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
      V_INDICADOR               INTEGER;
BEGIN
      
      -- GET CODIGO SOLICITUD
      SELECT SVRSVPR_SRVC_CODE 
      INTO P_CODIGO_SOLICITUD 
      FROM SVRSVPR 
      WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
      
      -- GET COMENTARIO de la solicitud. (comentario PERSONALZIADO.)
      SELECT SVRSVAD_ADDL_DATA_DESC INTO P_COMENTARIO_ALUMNO 
      FROM (
            SELECT SVRSVAD_ADDL_DATA_DESC
            FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
            INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
            ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
            WHERE SVRSRAD_SRVC_CODE = P_CODIGO_SOLICITUD
            AND SVRSVAD_ADDL_DATA_CDE = 'PREGUNTA'
            AND SVRSVAD_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD
            ORDER BY SVRSVAD_ADDL_DATA_SEQ DESC
      ) WHERE ROWNUM = 1;
      
      -- GET SEDE
      SELECT COUNT(*) INTO V_INDICADOR FROM SVRSVAD 
      WHERE SVRSVAD_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD AND SVRSVAD_ADDL_DATA_SEQ = 2;
      IF (V_INDICADOR = 0) THEN
            P_DESC_CAMP := '-';
      ELSE
          SELECT SVRSVAD_ADDL_DATA_DESC INTO P_DESC_CAMP FROM SVRSVAD 
          WHERE SVRSVAD_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD AND SVRSVAD_ADDL_DATA_SEQ = 2;
      END IF;
      
      -- GET DEPARTAMENTO
      SELECT COUNT(*) INTO V_INDICADOR FROM SVRSVAD 
      WHERE SVRSVAD_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD AND SVRSVAD_ADDL_DATA_SEQ = 3;
      IF (V_INDICADOR = 0) THEN
            P_DESC_DEPT := '-';
      ELSE
          SELECT SVRSVAD_ADDL_DATA_DESC INTO P_DESC_DEPT FROM SVRSVAD 
          WHERE SVRSVAD_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD AND SVRSVAD_ADDL_DATA_SEQ = 3;
      END IF;
      
      -- GET PROGRAMA
      SELECT COUNT(*) INTO V_INDICADOR FROM SVRSVAD 
      WHERE SVRSVAD_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD AND SVRSVAD_ADDL_DATA_SEQ = 4;
      IF (V_INDICADOR = 0) THEN
            P_DESC_PROG := '-';
      ELSE
          SELECT SVRSVAD_ADDL_DATA_DESC INTO P_DESC_PROG FROM SVRSVAD 
          WHERE SVRSVAD_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD AND SVRSVAD_ADDL_DATA_SEQ = 4;
      END IF;

END P_OBTENER_COMENTARIO_ALUMNO;