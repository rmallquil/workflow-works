create or replace PACKAGE WFK_CONTISR AS
/*******************************************************************************
 WFK_CONTISR:
       Conti Package body SOLICITUD DE REINCORPORACIÒN
*******************************************************************************/
-- FILE NAME..: WFK_CONTISR.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_CONTISR
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/

PROCEDURE P_OBTENER_USUARIO (
                P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
                P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
                P_CORREO              OUT VARCHAR2,
                P_ROL_SEDE            OUT VARCHAR2,
                P_ERROR               OUT VARCHAR2
            );

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
              P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
              P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
              P_AN                  OUT NUMBER,
              P_ERROR               OUT VARCHAR2
          );
              
--------------------------------------------------------------------------------

PROCEDURE P_OBTENER_COMENTARIO_ALUMNO (
                  P_NUMERO_SOLICITUD        IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
                  P_COMENTARIO_ALUMNO       OUT VARCHAR2,
                  P_DESC_CAMP               OUT VARCHAR2,
                  P_DESC_DEPT               OUT VARCHAR2,
                  P_DESC_PROG               OUT VARCHAR2
            );

--------------------------------------------------------------------------------

PROCEDURE P_OBTENER_REEMBOLSO (
            P_PIDM_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_CODIGO_DETALLE      IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE,
            P_MONTO_REEMBOLSO     OUT TBRACCD.TBRACCD_AMOUNT%TYPE,
            P_PERIODO_REEMBOLSO   OUT TBRACCD.TBRACCD_TERM_CODE%TYPE,
            P_ERROR               OUT VARCHAR2
        );           

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIAR_ESTATUS( 
              P_PIDM_ALUMNO             IN SPRIDEN.SPRIDEN_PIDM%TYPE,
              P_PERIODO                 IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
              P_ERROR                   OUT VARCHAR2
        );

--------------------------------------------------------------------------------

PROCEDURE P_REGISTAR_ATRIBUTO (
            P_PIDM_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_PERIODO               IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
            P_COD_ATRIBUTO          IN STVATTS.STVATTS_CODE%TYPE,       -- COD atributos
            P_ERROR                 OUT VARCHAR2
        );

--------------------------------------------------------------------------------

PROCEDURE P_UPDATE_CTA_CORRIENTE (
            P_PIDM_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_PERIODO               IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
            P_ERROR                 OUT VARCHAR2
        );

--------------------------------------------------------------------------------

PROCEDURE P_SET_GR_INSCRIPC_ABIERTO (
            P_PIDM_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
            P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE ,
            P_MODALIDAD_ALUMNO    IN STVDEPT.STVDEPT_CODE%TYPE , 
            P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE ,
            P_ERROR               OUT VARCHAR2
      );

--------------------------------------------------------------------------------

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END WFK_CONTISR;