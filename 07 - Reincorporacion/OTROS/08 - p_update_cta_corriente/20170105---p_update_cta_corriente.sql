create or replace PROCEDURE P_UPDATE_CTA_CORRIENTE (
    P_PIDM_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PERIODO               IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
    P_ERROR                 OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_UPDATE_CTA_CORRIENTE
  FECHA     : 05/01/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Vuelve a estimar las cuentas de un alumno.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
        SAVE_ACT_DATE_OUT       VARCHAR2(100);
        RETURN_STATUS_IN_OUT    NUMBER;
BEGIN
      -- #######################################################################
      -- Procesar DEUDA - Volviendo a estimar la deuda devido al cambio de TARIFA.
      SFKFEES.p_processfeeassessment (  P_PERIODO,
                                        P_PIDM_ALUMNO,
                                        SYSDATE,      -- assessment effective date(Evaluación de la fecha efectiva)
                                        SYSDATE,  -- refund by total refund date(El reembolso por fecha total del reembolso)
                                        'R',          -- use regular assessment rules(utilizar las reglas de evaluación periódica)
                                        'Y',          -- create TBRACCD records
                                        'SFAREGS',    -- where assessment originated from
                                        'Y',          -- commit changes
                                        SAVE_ACT_DATE_OUT,    -- OUT -- save_act_date
                                        'N',          -- do not ignore SFRFMAX rules
                                        RETURN_STATUS_IN_OUT );   -- OUT -- return_status

        COMMIT;
EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
        --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
        P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_UPDATE_CTA_CORRIENTE;