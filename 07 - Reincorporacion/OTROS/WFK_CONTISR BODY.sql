create or replace PACKAGE BODY WFK_CONTISR AS
/*******************************************************************************
 WFK_CONTISR:
       Conti Package body SOLICITUD DE REINCORPORACIÒN
*******************************************************************************/
-- FILE NAME..: WFK_CONTISR.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_CONTISR
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/


PROCEDURE P_OBTENER_USUARIO (
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE,
      P_ROL                 IN WORKFLOW.ROLE.NAME%TYPE,
      P_CORREO              OUT VARCHAR2,
      P_ROL_SEDE            OUT VARCHAR2,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_OBTENER_USUARIO
  FECHA     : 04/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : En base a una sede y un rol, el procedimiento LOS CORREOS deL(os) responsable(s) 
              de Registros Académicos de esa sede.
  =================================================================================================================== */
AS
      -- @PARAMETERS
      P_ROLE_ID                 NUMBER;
      P_ORG_ID                  NUMBER;
      P_USER_ID                 NUMBER;
      P_ROLE_ASSIGNMENT_ID      NUMBER;
      P_Email_Address           VARCHAR2(100);
    
      P_SECCION_EXCEPT          VARCHAR2(50);
      
      CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = P_ORG_ID AND ROLE_ID = P_ROLE_ID;
      
      V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
BEGIN 
--
      P_ROL_SEDE := P_ROL || P_COD_SEDE;
      
      -- Obtener el ROL_ID 
      P_SECCION_EXCEPT := 'ROLES';
      SELECT ID INTO P_ROLE_ID FROM WORKFLOW.ROLE WHERE NAME = P_ROL_SEDE;
      P_SECCION_EXCEPT := '';
      
      -- Obtener el ORG_ID 
      P_SECCION_EXCEPT := 'ORGRANIZACION';
      SELECT ID INTO P_ORG_ID FROM WORKFLOW.ORGANIZATION WHERE NAME = 'Root';
      P_SECCION_EXCEPT := '';
     
      -- Obtener los datos de usuarios que relaciona rol y usuario
      P_SECCION_EXCEPT := 'ROLE_ASSIGNMENT';
       -- #######################################################################
      OPEN C_ROLE_ASSIGNMENT;
      LOOP
          FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
          EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
                      
                      -- Obtener Datos Usuario
                      SELECT Email_Address INTO P_Email_Address FROM WORKFLOW.WFUSER WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;
          
                      P_CORREO := P_CORREO || P_Email_Address || ',';
      END LOOP;
      CLOSE C_ROLE_ASSIGNMENT;
      P_SECCION_EXCEPT := '';
      
      -- Extraer el ultimo digito en caso sea un "coma"(,)
      SELECT SUBSTR(P_CORREO,1,LENGTH(P_CORREO) -1) INTO P_CORREO
      FROM DUAL
      WHERE SUBSTR(P_CORREO,-1,1) = ',';
      
EXCEPTION
  WHEN TOO_MANY_ROWS THEN 
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              -- --DBMS_OUTPUT.PUT_LINE ();
              P_ERROR := 'A ocurrido un problema, se encontraron mas de un ROL con el mismo nombre: ' || P_ROL || P_COD_SEDE;
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_ERROR := 'A ocurrido un problema, se encontraron mas de una ORGANIZACIÒN con el mismo nombre.';
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := 'A ocurrido un problema, Se encontraron mas de un usuario con el mismo ROL.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
  WHEN NO_DATA_FOUND THEN
          IF ( P_SECCION_EXCEPT = 'ROLES') THEN
              P_ERROR := 'A ocurrido un problema, NO se encontrò el nombre del ROL: ' || P_ROL || P_COD_SEDE;
          ELSIF (P_SECCION_EXCEPT = 'ORGRANIZACION') THEN
              P_ERROR := 'A ocurrido un problema, NO se encontrò el nombre de la ORGANIZACION.';
          ELSIF (P_SECCION_EXCEPT = 'ROLE_ASSIGNMENT') THEN
              P_ERROR := 'A ocurrido un problema, NO  se encontrò ningun usuario con esas caracteristicas.';
          ELSE  P_ERROR := SQLERRM;
          END IF; 
  WHEN OTHERS THEN
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_OBTENER_USUARIO;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
  P_NUMERO_SOLICITUD    IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
  P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
  P_AN                  OUT NUMBER,
  P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : p_cambio_estado_solicitud
  FECHA     : 12/12/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : cambia el estado de la solicitud (XXXXXX)

  MODIFICACIONES
  NRO   FECHA         USUARIO       MODIFICACION
  001   10/03/2016    RMALLQUI      Se agrego el parametro P_AN paravalidar si la solicitud este ANULADO.
  =================================================================================================================== */
AS
            P_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
            P_ESTADO_PREVIOUS         SVVSRVS.SVVSRVS_CODE%TYPE;
            P_FECHA_SOL               SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE;
            P_INDICADOR               NUMBER;
            E_INVALID_ESTADO          EXCEPTION;
            V_PRIORIDAD               SVRSVPR.SVRSVPR_RSRV_SEQ_NO%TYPE;
BEGIN
--
  ----------------------------------------------------------------------------
        
        -- GET tipo de solicitud por ejemplo "SOL003" 
        SELECT  SVRSVPR_SRVC_CODE, 
                SVRSVPR_SRVS_CODE,
                SVRSVPR_RECEPTION_DATE,
                SVRSVPR_RSRV_SEQ_NO
        INTO    P_CODIGO_SOLICITUD,
                P_ESTADO_PREVIOUS,
                P_FECHA_SOL,
                V_PRIORIDAD
        FROM SVRSVPR WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
        
        -- SI EL ESTADO ES ANULADO (AN)
        P_AN := 0;
        IF P_ESTADO_PREVIOUS = 'AN' THEN
              P_AN := 1;
        END IF;
        
        -- VALIDAR que :
            -- El ESTADO actual sea modificable segun la configuracion.
            -- El ETSADO enviado pertenesca entre los asignados al tipo de SOLICTUD.
        SELECT COUNT(*) INTO P_INDICADOR
        FROM SVRSVPR 
        WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD
        AND SVRSVPR_SRVS_CODE IN (
              -- Estados de solic. MODIFICABLES segun las reglas y configuracion
              SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
              FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
              INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
              ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
              AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
              INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
              ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
              WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
              AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
              AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 
              AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
              AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
        )
        AND P_ESTADO IN (
              -- Estados de solic. configurados
              SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
              FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
              INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
              ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
              AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
              INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
              ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
              WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
              AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
              AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
              AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
        );
        
        
        IF P_INDICADOR = 0 THEN
              RAISE E_INVALID_ESTADO;
        ELSIF (P_ESTADO_PREVIOUS <> P_ESTADO) THEN
              -- UPDATE SOLICITUD
              UPDATE SVRSVPR
                SET SVRSVPR_SRVS_CODE = P_ESTADO,  --AN , AP , RE entre otros
                --SVRSVPR_ACCD_TRAN_NUMBER = P_TRAN_NUMBER_OLD,
                --SVRSVPR_BILLING_IND = 'N', -- CASE WHEN P_ESTADO = 'AP' THEN 'Y' ELSE 'N' END,
                SVRSVPR_ACTIVITY_DATE = SYSDATE,
                SVRSVPR_DATA_ORIGIN = 'WorkFlow'
              WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD
              AND SVRSVPR_SRVS_CODE IN (
                        -- Estados de solic. MODIFICABLES segun las reglas y configuracion
                        SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
                        FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
                        INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
                        ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
                        AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
                        INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
                        ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
                        WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
                        AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
                        AND SVVSRVS_LOCKED <> 'L' ------------------------ (L)LOCKET , (U)UNLOCKET 
                        AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
                        AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
                )
              AND P_ESTADO IN (
                      -- Estados de solic. configurados
                      SELECT  SVRRSST_SRVS_CODE ------------------------ Estado SOL
                      FROM SVRRSRV ------------------------------------- configuraciòn total de reglas
                      INNER JOIN SVRRSST ------------------------------- subformulario de reglas -- estados
                      ON SVRRSRV_SRVC_CODE = SVRRSST_SRVC_CODE
                      AND SVRRSRV_SEQ_NO = SVRRSST_RSRV_SEQ_NO
                      INNER JOIN SVVSRVS ------------------------------- total de estados de servicios
                      ON SVRRSST_SRVS_CODE = SVVSRVS_CODE
                      WHERE SVRRSRV_SRVC_CODE = P_CODIGO_SOLICITUD
                      AND SVRRSRV_INACTIVE_IND = 'Y' ------------------- Activado
                      AND SVRRSST_RSRV_SEQ_NO = V_PRIORIDAD
                      AND P_FECHA_SOL BETWEEN SVRRSRV_START_DATE AND SVRRSRV_END_DATE
              );
              
              COMMIT;
        END IF;
        
EXCEPTION
  WHEN E_INVALID_ESTADO THEN
          P_ERROR  := 'El "ESTADO" actual no es modificable ò no se encuentra entre los ESTADOS disponibles para la solicitud.';
  WHEN OTHERS THEN
          --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_CAMBIO_ESTADO_SOLICITUD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_OBTENER_COMENTARIO_ALUMNO (
      P_NUMERO_SOLICITUD        IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_COMENTARIO_ALUMNO       OUT VARCHAR2,
      P_DESC_CAMP               OUT VARCHAR2,
      P_DESC_DEPT               OUT VARCHAR2,
      P_DESC_PROG               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_OBTENER_COMENTARIO_ALUMNO
  FECHA     : 00/00/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obteiene el comentario ingresado por el alumno 
              (se adicionò el campus, departamento y el programa para que sea selecionado por el alumno)

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION  
  001   13/02/2017    rmallqui    adicionalmente se va a obtener la campus, departamento y el programa ingresados por el alumno.
  =================================================================================================================== */
AS
      P_CODIGO_SOLICITUD        SVVSRVC.SVVSRVC_CODE%TYPE;
      V_INDICADOR               INTEGER;
BEGIN
      
      -- GET CODIGO SOLICITUD
      SELECT SVRSVPR_SRVC_CODE 
      INTO P_CODIGO_SOLICITUD 
      FROM SVRSVPR 
      WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD;
      
      -- GET COMENTARIO de la solicitud. (comentario PERSONALZIADO.)
      SELECT SVRSVAD_ADDL_DATA_DESC INTO P_COMENTARIO_ALUMNO 
      FROM (
            SELECT SVRSVAD_ADDL_DATA_DESC
            FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
            INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
            ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
            WHERE SVRSRAD_SRVC_CODE = P_CODIGO_SOLICITUD
            AND SVRSVAD_ADDL_DATA_CDE = 'PREGUNTA'
            AND SVRSVAD_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD
            ORDER BY SVRSVAD_ADDL_DATA_SEQ DESC
      ) WHERE ROWNUM = 1;
      
      -- GET SEDE
      SELECT COUNT(*) INTO V_INDICADOR FROM SVRSVAD 
      WHERE SVRSVAD_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD AND SVRSVAD_ADDL_DATA_SEQ = 2;
      IF (V_INDICADOR = 0) THEN
            P_DESC_CAMP := '-';
      ELSE
          SELECT SVRSVAD_ADDL_DATA_DESC INTO P_DESC_CAMP FROM SVRSVAD 
          WHERE SVRSVAD_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD AND SVRSVAD_ADDL_DATA_SEQ = 2;
      END IF;
      
      -- GET DEPARTAMENTO
      SELECT COUNT(*) INTO V_INDICADOR FROM SVRSVAD 
      WHERE SVRSVAD_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD AND SVRSVAD_ADDL_DATA_SEQ = 3;
      IF (V_INDICADOR = 0) THEN
            P_DESC_DEPT := '-';
      ELSE
          SELECT SVRSVAD_ADDL_DATA_DESC INTO P_DESC_DEPT FROM SVRSVAD 
          WHERE SVRSVAD_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD AND SVRSVAD_ADDL_DATA_SEQ = 3;
      END IF;
      
      -- GET PROGRAMA
      SELECT COUNT(*) INTO V_INDICADOR FROM SVRSVAD 
      WHERE SVRSVAD_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD AND SVRSVAD_ADDL_DATA_SEQ = 4;
      IF (V_INDICADOR = 0) THEN
            P_DESC_PROG := '-';
      ELSE
          SELECT SVRSVAD_ADDL_DATA_DESC INTO P_DESC_PROG FROM SVRSVAD 
          WHERE SVRSVAD_PROTOCOL_SEQ_NO = P_NUMERO_SOLICITUD AND SVRSVAD_ADDL_DATA_SEQ = 4;
      END IF;

END P_OBTENER_COMENTARIO_ALUMNO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_OBTENER_REEMBOLSO (
  P_PIDM_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
  P_CODIGO_DETALLE      IN TBRACCD.TBRACCD_DETAIL_CODE%TYPE,
  P_MONTO_REEMBOLSO     OUT TBRACCD.TBRACCD_AMOUNT%TYPE,
  P_PERIODO_REEMBOLSO   OUT TBRACCD.TBRACCD_TERM_CODE%TYPE,
  P_ERROR               OUT VARCHAR2
)              
/* ===================================================================================================================
  NOMBRE    : P_OBTENER_REEMBOLSO
  FECHA     : 05/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene el CARGO a un alumno por CODIGO DETALLE.
              ** No valida si se realizo movimientos ya que es un cargo especial estatico
                 ejemplo: "RFND"- Reembolso.

  MODIFICACIONES
  NRO   FECHA         USUARIO     MODIFICACION  
  =================================================================================================================== */
AS
      PRAGMA AUTONOMOUS_TRANSACTION;
      P_INDICADOR           NUMBER;
BEGIN
--
  
    -- GET deuda
    SELECT COUNT(TBRACCD_AMOUNT) 
    INTO P_INDICADOR
    FROM (
        SELECT TBRACCD_AMOUNT FROM TBRACCD 
        WHERE TBRACCD_PIDM = P_PIDM_ALUMNO  AND TBRACCD_DETAIL_CODE = P_CODIGO_DETALLE
         ORDER BY TBRACCD_TERM_CODE DESC
    ) WHERE ROWNUM = 1;
    
    IF P_INDICADOR = 0 THEN
          P_MONTO_REEMBOLSO   := 0;
          P_PERIODO_REEMBOLSO := '-';
    ELSE
          SELECT TBRACCD_TERM_CODE, TBRACCD_AMOUNT
          INTO P_PERIODO_REEMBOLSO, P_MONTO_REEMBOLSO
          FROM (
                  SELECT TBRACCD_TERM_CODE, TBRACCD_AMOUNT 
                  FROM  TBRACCD 
                  WHERE TBRACCD_PIDM = P_PIDM_ALUMNO  
                  AND TBRACCD_DETAIL_CODE = P_CODIGO_DETALLE
                  ORDER BY TBRACCD_TERM_CODE DESC
          ) WHERE ROWNUM = 1;
    END IF;
    
EXCEPTION
  WHEN OTHERS THEN
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_OBTENER_REEMBOLSO;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_CAMBIAR_ESTATUS( 
      P_PIDM_ALUMNO             IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_PERIODO                 IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
      P_ERROR                   OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_CAMBIAR_ESTATUS
  FECHA     : 10/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Cambia los estados de un alumno(forma SGASTDN) : Plan Estudio, Status Alumno, Tipo Alumno
                - Plan Estudio    :  'RE' - Reincorporado
                - Status Alumno   :  'AS' - Activo
                - Tipo Alumno     :  'R' - UC Reincorporado

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
        P_KEY_SEQNO             SGRSTSP.SGRSTSP_KEY_SEQNO%TYPE;
        P_PERIODO_LAST          SFBETRM.SFBETRM_TERM_CODE%TYPE;
        
        -- AGREGAR NUEVO REGISTRO
        CURSOR C_SGRSTSP IS
        SELECT * FROM SGRSTSP 
          WHERE SGRSTSP_PIDM = P_PIDM_ALUMNO 
          AND SGRSTSP_TERM_CODE_EFF = P_PERIODO_LAST 
          AND SGRSTSP_KEY_SEQNO = P_KEY_SEQNO;
        
        -- AGREGAR NUEVO REGISTRO
        CURSOR C_SGBSTDN IS
        SELECT * FROM (
            SELECT *
            FROM SGBSTDN WHERE SGBSTDN_PIDM = P_PIDM_ALUMNO
            ORDER BY SGBSTDN_TERM_CODE_EFF DESC
         )WHERE ROWNUM = 1
         AND NOT EXISTS (
            SELECT *
            FROM SGBSTDN WHERE SGBSTDN_PIDM = P_PIDM_ALUMNO
            AND SGBSTDN_TERM_CODE_EFF = P_PERIODO
         );
        
        -- AGREGAR NUEVO REGISTRO
        CURSOR C_SORLCUR IS
        SELECT * FROM (
            SELECT * FROM SORLCUR 
            WHERE SORLCUR_PIDM = P_PIDM_ALUMNO
            AND SORLCUR_LMOD_CODE = 'LEARNER'
            AND SORLCUR_CURRENT_CDE = 'Y' --------------------- CURRENT CURRICULUM
            ORDER BY SORLCUR_SEQNO DESC
        )WHERE ROWNUM = 1;
        
        V_SGRSTSP_REC           SGRSTSP%ROWTYPE;
        V_SGBSTDN_REC           SGBSTDN%ROWTYPE;
        V_SORLCUR_REC           SORLCUR%ROWTYPE;
        P_SORLCUR_SEQNO_OLD     SORLCUR.SORLCUR_SEQNO%TYPE;
        P_SORLCUR_SEQNO_NEW     SORLCUR.SORLCUR_SEQNO%TYPE;
        P_SGRSTSP_KEY_SEQNO     SGRSTSP.SGRSTSP_KEY_SEQNO%TYPE;
        P_INDICADOR             NUMBER;
BEGIN
      
        -- DETERMINAR si existe ya un PlanEstudio para dicho periodo.
        SELECT COUNT(*) INTO P_INDICADOR
        FROM SGRSTSP 
        WHERE SGRSTSP_PIDM = P_PIDM_ALUMNO
        AND SGRSTSP_TERM_CODE_EFF = P_PERIODO;
        
        ----------------------------------------------------------------------------------
        
        IF P_INDICADOR = 0  THEN
        
              -- #######################################################################
              -- CAMBIO ESTADO PLAN ESTUDIO  --- FORMA SGASTDN        
              
              P_SGRSTSP_KEY_SEQNO := 1;
              
              -- Get lasT periodo
              SELECT MAX(SGRSTSP_TERM_CODE_EFF) INTO P_PERIODO_LAST
              FROM SGRSTSP 
              WHERE SGRSTSP_PIDM = P_PIDM_ALUMNO
              AND SGRSTSP_TERM_CODE_EFF < P_PERIODO;
              -- GET 
              SELECT  MAX(SGRSTSP_KEY_SEQNO) INTO P_KEY_SEQNO 
              FROM SGRSTSP 
              WHERE SGRSTSP_PIDM = P_PIDM_ALUMNO 
              AND SGRSTSP_TERM_CODE_EFF = P_PERIODO_LAST;
              
              -- #######################################################################
              -- INSERT  ------> PLAN ESTUDIO -
              OPEN C_SGRSTSP;
              LOOP
                  FETCH C_SGRSTSP INTO V_SGRSTSP_REC;
                  EXIT WHEN C_SGRSTSP%NOTFOUND;
                  
                  INSERT INTO SGRSTSP (
                          SGRSTSP_PIDM,       SGRSTSP_TERM_CODE_EFF,  SGRSTSP_KEY_SEQNO,
                          SGRSTSP_STSP_CODE,  SGRSTSP_ACTIVITY_DATE,  SGRSTSP_DATA_ORIGIN,
                          SGRSTSP_USER_ID,    SGRSTSP_FULL_PART_IND,  SGRSTSP_SESS_CODE,
                          SGRSTSP_RESD_CODE,  SGRSTSP_ORSN_CODE,      SGRSTSP_PRAC_CODE,
                          SGRSTSP_CAPL_CODE,  SGRSTSP_EDLV_CODE,      SGRSTSP_INCM_CODE,
                          SGRSTSP_EMEX_CODE,  SGRSTSP_APRN_CODE,      SGRSTSP_TRCN_CODE,
                          SGRSTSP_GAIN_CODE,  SGRSTSP_VOED_CODE,      SGRSTSP_BLCK_CODE,
                          SGRSTSP_EGOL_CODE,  SGRSTSP_BSKL_CODE,      SGRSTSP_ASTD_CODE,
                          SGRSTSP_PREV_CODE,  SGRSTSP_CAST_CODE ) 
                  SELECT 
                          V_SGRSTSP_REC.SGRSTSP_PIDM,       P_PERIODO,                            P_SGRSTSP_KEY_SEQNO,
                          'AS',                             SYSDATE,                              V_SGRSTSP_REC.SGRSTSP_DATA_ORIGIN,
                          USER,                             V_SGRSTSP_REC.SGRSTSP_FULL_PART_IND,  V_SGRSTSP_REC.SGRSTSP_SESS_CODE,
                          V_SGRSTSP_REC.SGRSTSP_RESD_CODE,  V_SGRSTSP_REC.SGRSTSP_ORSN_CODE,      V_SGRSTSP_REC.SGRSTSP_PRAC_CODE,
                          V_SGRSTSP_REC.SGRSTSP_CAPL_CODE,  V_SGRSTSP_REC.SGRSTSP_EDLV_CODE,      V_SGRSTSP_REC.SGRSTSP_INCM_CODE,
                          V_SGRSTSP_REC.SGRSTSP_EMEX_CODE,  V_SGRSTSP_REC.SGRSTSP_APRN_CODE,      V_SGRSTSP_REC.SGRSTSP_TRCN_CODE,
                          V_SGRSTSP_REC.SGRSTSP_GAIN_CODE,  V_SGRSTSP_REC.SGRSTSP_VOED_CODE,      V_SGRSTSP_REC.SGRSTSP_BLCK_CODE,
                          V_SGRSTSP_REC.SGRSTSP_EGOL_CODE,  V_SGRSTSP_REC.SGRSTSP_BSKL_CODE,      V_SGRSTSP_REC.SGRSTSP_ASTD_CODE,
                          V_SGRSTSP_REC.SGRSTSP_PREV_CODE,  V_SGRSTSP_REC.SGRSTSP_CAST_CODE
                  FROM DUAL;
                  
              END LOOP;
              CLOSE C_SGRSTSP;
              
              -- #######################################################################
              -- INSERT  ------> SGBSTDN -
              OPEN C_SGBSTDN;
              LOOP
                  FETCH C_SGBSTDN INTO V_SGBSTDN_REC;
                  EXIT WHEN C_SGBSTDN%NOTFOUND;
                  
                    INSERT INTO SGBSTDN (
                              SGBSTDN_PIDM,                 SGBSTDN_TERM_CODE_EFF,          SGBSTDN_STST_CODE,
                              SGBSTDN_LEVL_CODE,            SGBSTDN_STYP_CODE,              SGBSTDN_TERM_CODE_MATRIC,
                              SGBSTDN_TERM_CODE_ADMIT,      SGBSTDN_EXP_GRAD_DATE,          SGBSTDN_CAMP_CODE,
                              SGBSTDN_FULL_PART_IND,        SGBSTDN_SESS_CODE,              SGBSTDN_RESD_CODE,
                              SGBSTDN_COLL_CODE_1,          SGBSTDN_DEGC_CODE_1,            SGBSTDN_MAJR_CODE_1,
                              SGBSTDN_MAJR_CODE_MINR_1,     SGBSTDN_MAJR_CODE_MINR_1_2,     SGBSTDN_MAJR_CODE_CONC_1,
                              SGBSTDN_MAJR_CODE_CONC_1_2,   SGBSTDN_MAJR_CODE_CONC_1_3,     SGBSTDN_COLL_CODE_2,
                              SGBSTDN_DEGC_CODE_2,          SGBSTDN_MAJR_CODE_2,            SGBSTDN_MAJR_CODE_MINR_2,
                              SGBSTDN_MAJR_CODE_MINR_2_2,   SGBSTDN_MAJR_CODE_CONC_2,       SGBSTDN_MAJR_CODE_CONC_2_2,
                              SGBSTDN_MAJR_CODE_CONC_2_3,   SGBSTDN_ORSN_CODE,              SGBSTDN_PRAC_CODE,
                              SGBSTDN_ADVR_PIDM,            SGBSTDN_GRAD_CREDIT_APPR_IND,   SGBSTDN_CAPL_CODE,
                              SGBSTDN_LEAV_CODE,            SGBSTDN_LEAV_FROM_DATE,         SGBSTDN_LEAV_TO_DATE,
                              SGBSTDN_ASTD_CODE,            SGBSTDN_TERM_CODE_ASTD,         SGBSTDN_RATE_CODE,
                              SGBSTDN_ACTIVITY_DATE,        SGBSTDN_MAJR_CODE_1_2,          SGBSTDN_MAJR_CODE_2_2,
                              SGBSTDN_EDLV_CODE,            SGBSTDN_INCM_CODE,              SGBSTDN_ADMT_CODE,
                              SGBSTDN_EMEX_CODE,            SGBSTDN_APRN_CODE,              SGBSTDN_TRCN_CODE,
                              SGBSTDN_GAIN_CODE,            SGBSTDN_VOED_CODE,              SGBSTDN_BLCK_CODE,
                              SGBSTDN_TERM_CODE_GRAD,       SGBSTDN_ACYR_CODE,              SGBSTDN_DEPT_CODE,
                              SGBSTDN_SITE_CODE,            SGBSTDN_DEPT_CODE_2,            SGBSTDN_EGOL_CODE,
                              SGBSTDN_DEGC_CODE_DUAL,       SGBSTDN_LEVL_CODE_DUAL,         SGBSTDN_DEPT_CODE_DUAL,
                              SGBSTDN_COLL_CODE_DUAL,       SGBSTDN_MAJR_CODE_DUAL,         SGBSTDN_BSKL_CODE,
                              SGBSTDN_PRIM_ROLL_IND,        SGBSTDN_PROGRAM_1,              SGBSTDN_TERM_CODE_CTLG_1,
                              SGBSTDN_DEPT_CODE_1_2,        SGBSTDN_MAJR_CODE_CONC_121,     SGBSTDN_MAJR_CODE_CONC_122,
                              SGBSTDN_MAJR_CODE_CONC_123,   SGBSTDN_SECD_ROLL_IND,          SGBSTDN_TERM_CODE_ADMIT_2,
                              SGBSTDN_ADMT_CODE_2,          SGBSTDN_PROGRAM_2,              SGBSTDN_TERM_CODE_CTLG_2,
                              SGBSTDN_LEVL_CODE_2,          SGBSTDN_CAMP_CODE_2,            SGBSTDN_DEPT_CODE_2_2,
                              SGBSTDN_MAJR_CODE_CONC_221,   SGBSTDN_MAJR_CODE_CONC_222,     SGBSTDN_MAJR_CODE_CONC_223,
                              SGBSTDN_CURR_RULE_1,          SGBSTDN_CMJR_RULE_1_1,          SGBSTDN_CCON_RULE_11_1,
                              SGBSTDN_CCON_RULE_11_2,       SGBSTDN_CCON_RULE_11_3,         SGBSTDN_CMJR_RULE_1_2,
                              SGBSTDN_CCON_RULE_12_1,       SGBSTDN_CCON_RULE_12_2,         SGBSTDN_CCON_RULE_12_3,
                              SGBSTDN_CMNR_RULE_1_1,        SGBSTDN_CMNR_RULE_1_2,          SGBSTDN_CURR_RULE_2,
                              SGBSTDN_CMJR_RULE_2_1,        SGBSTDN_CCON_RULE_21_1,         SGBSTDN_CCON_RULE_21_2,
                              SGBSTDN_CCON_RULE_21_3,       SGBSTDN_CMJR_RULE_2_2,          SGBSTDN_CCON_RULE_22_1,
                              SGBSTDN_CCON_RULE_22_2,       SGBSTDN_CCON_RULE_22_3,         SGBSTDN_CMNR_RULE_2_1,
                              SGBSTDN_CMNR_RULE_2_2,        SGBSTDN_PREV_CODE,              SGBSTDN_TERM_CODE_PREV,
                              SGBSTDN_CAST_CODE,            SGBSTDN_TERM_CODE_CAST,         SGBSTDN_DATA_ORIGIN,
                              SGBSTDN_USER_ID,              SGBSTDN_SCPC_CODE ) 
                    VALUES (  V_SGBSTDN_REC.SGBSTDN_PIDM,                 P_PERIODO,                                    'AS', /*AS-Activo*/ 
                              V_SGBSTDN_REC.SGBSTDN_LEVL_CODE,            'R',/*R-Reincorporado*/                       V_SGBSTDN_REC.SGBSTDN_TERM_CODE_MATRIC,
                              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT,      V_SGBSTDN_REC.SGBSTDN_EXP_GRAD_DATE,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE,
                              V_SGBSTDN_REC.SGBSTDN_FULL_PART_IND,        V_SGBSTDN_REC.SGBSTDN_SESS_CODE,              V_SGBSTDN_REC.SGBSTDN_RESD_CODE,
                              V_SGBSTDN_REC.SGBSTDN_COLL_CODE_1,          V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_1,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1,
                              V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_1_2,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1,
                              V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_1_3,     V_SGBSTDN_REC.SGBSTDN_COLL_CODE_2,
                              V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2,            V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2,
                              V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_MINR_2_2,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_2,
                              V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_2_3,   V_SGBSTDN_REC.SGBSTDN_ORSN_CODE,              V_SGBSTDN_REC.SGBSTDN_PRAC_CODE,
                              V_SGBSTDN_REC.SGBSTDN_ADVR_PIDM,            V_SGBSTDN_REC.SGBSTDN_GRAD_CREDIT_APPR_IND,   V_SGBSTDN_REC.SGBSTDN_CAPL_CODE,
                              V_SGBSTDN_REC.SGBSTDN_LEAV_CODE,            V_SGBSTDN_REC.SGBSTDN_LEAV_FROM_DATE,         V_SGBSTDN_REC.SGBSTDN_LEAV_TO_DATE,
                              V_SGBSTDN_REC.SGBSTDN_ASTD_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ASTD,         V_SGBSTDN_REC.SGBSTDN_RATE_CODE,
                              SYSDATE,                                    V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_1_2,          V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_2_2,
                              V_SGBSTDN_REC.SGBSTDN_EDLV_CODE,            V_SGBSTDN_REC.SGBSTDN_INCM_CODE,              V_SGBSTDN_REC.SGBSTDN_ADMT_CODE,
                              V_SGBSTDN_REC.SGBSTDN_EMEX_CODE,            V_SGBSTDN_REC.SGBSTDN_APRN_CODE,              V_SGBSTDN_REC.SGBSTDN_TRCN_CODE,
                              V_SGBSTDN_REC.SGBSTDN_GAIN_CODE,            V_SGBSTDN_REC.SGBSTDN_VOED_CODE,              V_SGBSTDN_REC.SGBSTDN_BLCK_CODE,
                              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_GRAD,       V_SGBSTDN_REC.SGBSTDN_ACYR_CODE,              V_SGBSTDN_REC.SGBSTDN_DEPT_CODE,
                              V_SGBSTDN_REC.SGBSTDN_SITE_CODE,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2,            V_SGBSTDN_REC.SGBSTDN_EGOL_CODE,
                              V_SGBSTDN_REC.SGBSTDN_DEGC_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_DUAL,
                              V_SGBSTDN_REC.SGBSTDN_COLL_CODE_DUAL,       V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_DUAL,         V_SGBSTDN_REC.SGBSTDN_BSKL_CODE,
                              V_SGBSTDN_REC.SGBSTDN_PRIM_ROLL_IND,        V_SGBSTDN_REC.SGBSTDN_PROGRAM_1,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CTLG_1,
                              V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_1_2,        V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_121,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_122,
                              V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_123,   V_SGBSTDN_REC.SGBSTDN_SECD_ROLL_IND,          V_SGBSTDN_REC.SGBSTDN_TERM_CODE_ADMIT_2,
                              V_SGBSTDN_REC.SGBSTDN_ADMT_CODE_2,          V_SGBSTDN_REC.SGBSTDN_PROGRAM_2,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CTLG_2,
                              V_SGBSTDN_REC.SGBSTDN_LEVL_CODE_2,          V_SGBSTDN_REC.SGBSTDN_CAMP_CODE_2,            V_SGBSTDN_REC.SGBSTDN_DEPT_CODE_2_2,
                              V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_221,   V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_222,     V_SGBSTDN_REC.SGBSTDN_MAJR_CODE_CONC_223,
                              V_SGBSTDN_REC.SGBSTDN_CURR_RULE_1,          V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_1,          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_1,
                              V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_11_3,         V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_1_2,
                              V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_1,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_2,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_12_3,
                              V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_1,        V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_1_2,          V_SGBSTDN_REC.SGBSTDN_CURR_RULE_2,
                              V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_1,        V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_1,         V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_2,
                              V_SGBSTDN_REC.SGBSTDN_CCON_RULE_21_3,       V_SGBSTDN_REC.SGBSTDN_CMJR_RULE_2_2,          V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_1,
                              V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_2,       V_SGBSTDN_REC.SGBSTDN_CCON_RULE_22_3,         V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_1,
                              V_SGBSTDN_REC.SGBSTDN_CMNR_RULE_2_2,        V_SGBSTDN_REC.SGBSTDN_PREV_CODE,              V_SGBSTDN_REC.SGBSTDN_TERM_CODE_PREV,
                              V_SGBSTDN_REC.SGBSTDN_CAST_CODE,            V_SGBSTDN_REC.SGBSTDN_TERM_CODE_CAST,         'WorkFlow',
                              USER,                                       V_SGBSTDN_REC.SGBSTDN_SCPC_CODE );
              END LOOP;
              CLOSE C_SGBSTDN;
              
              -- #######################################################################
              -- INSERT  ------> SORLCUR -
              OPEN C_SORLCUR;
              LOOP
                  FETCH C_SORLCUR INTO V_SORLCUR_REC;
                  EXIT WHEN C_SORLCUR%NOTFOUND;    
                  
                  -- GET SORLCUR_SEQNO NUEVO 
                  SELECT MAX(SORLCUR_SEQNO) + 1 INTO P_SORLCUR_SEQNO_NEW FROM SORLCUR
                  WHERE SORLCUR_PIDM = P_PIDM_ALUMNO;
                  -- GET SORLCUR_SEQNO ANTERIOR 
                  P_SORLCUR_SEQNO_OLD := V_SORLCUR_REC.SORLCUR_SEQNO;
                  
                  INSERT INTO SORLCUR (
                              SORLCUR_PIDM,             SORLCUR_SEQNO,                SORLCUR_LMOD_CODE,
                              SORLCUR_TERM_CODE,        SORLCUR_KEY_SEQNO,            SORLCUR_PRIORITY_NO,
                              SORLCUR_ROLL_IND,         SORLCUR_CACT_CODE,            SORLCUR_USER_ID,
                              SORLCUR_DATA_ORIGIN,      SORLCUR_ACTIVITY_DATE,        SORLCUR_LEVL_CODE,
                              SORLCUR_COLL_CODE,        SORLCUR_DEGC_CODE,            SORLCUR_TERM_CODE_CTLG,
                              SORLCUR_TERM_CODE_END,    SORLCUR_TERM_CODE_MATRIC,     SORLCUR_TERM_CODE_ADMIT,
                              SORLCUR_ADMT_CODE,        SORLCUR_CAMP_CODE,            SORLCUR_PROGRAM,
                              SORLCUR_START_DATE,       SORLCUR_END_DATE,             SORLCUR_CURR_RULE,
                              SORLCUR_ROLLED_SEQNO,     SORLCUR_STYP_CODE,            SORLCUR_RATE_CODE,
                              SORLCUR_LEAV_CODE,        SORLCUR_LEAV_FROM_DATE,       SORLCUR_LEAV_TO_DATE,
                              SORLCUR_EXP_GRAD_DATE,    SORLCUR_TERM_CODE_GRAD,       SORLCUR_ACYR_CODE,
                              SORLCUR_SITE_CODE,        SORLCUR_APPL_SEQNO,           SORLCUR_APPL_KEY_SEQNO,
                              SORLCUR_USER_ID_UPDATE,   SORLCUR_ACTIVITY_DATE_UPDATE, SORLCUR_GAPP_SEQNO,
                              SORLCUR_CURRENT_CDE ) 
                  VALUES (    V_SORLCUR_REC.SORLCUR_PIDM,             P_SORLCUR_SEQNO_NEW,                        V_SORLCUR_REC.SORLCUR_LMOD_CODE,
                              P_PERIODO,                              P_SGRSTSP_KEY_SEQNO,/*SGRSTSP*/             V_SORLCUR_REC.SORLCUR_PRIORITY_NO,
                              V_SORLCUR_REC.SORLCUR_ROLL_IND,         V_SORLCUR_REC.SORLCUR_CACT_CODE,            USER,
                              'WorkFlow',                             SYSDATE,                                    V_SORLCUR_REC.SORLCUR_LEVL_CODE,
                              V_SORLCUR_REC.SORLCUR_COLL_CODE,        V_SORLCUR_REC.SORLCUR_DEGC_CODE,            V_SORLCUR_REC.SORLCUR_TERM_CODE_CTLG,
                              NULL,                                   V_SORLCUR_REC.SORLCUR_TERM_CODE_MATRIC,     V_SORLCUR_REC.SORLCUR_TERM_CODE_ADMIT,
                              V_SORLCUR_REC.SORLCUR_ADMT_CODE,        V_SORLCUR_REC.SORLCUR_CAMP_CODE,            V_SORLCUR_REC.SORLCUR_PROGRAM,
                              V_SORLCUR_REC.SORLCUR_START_DATE,       V_SORLCUR_REC.SORLCUR_END_DATE,             V_SORLCUR_REC.SORLCUR_CURR_RULE,
                              V_SORLCUR_REC.SORLCUR_ROLLED_SEQNO,     V_SORLCUR_REC.SORLCUR_STYP_CODE,            V_SORLCUR_REC.SORLCUR_RATE_CODE,
                              V_SORLCUR_REC.SORLCUR_LEAV_CODE,        V_SORLCUR_REC.SORLCUR_LEAV_FROM_DATE,       V_SORLCUR_REC.SORLCUR_LEAV_TO_DATE,
                              V_SORLCUR_REC.SORLCUR_EXP_GRAD_DATE,    V_SORLCUR_REC.SORLCUR_TERM_CODE_GRAD,       V_SORLCUR_REC.SORLCUR_ACYR_CODE,
                              V_SORLCUR_REC.SORLCUR_SITE_CODE,        V_SORLCUR_REC.SORLCUR_APPL_SEQNO,           V_SORLCUR_REC.SORLCUR_APPL_KEY_SEQNO,
                              USER,                                   SYSDATE,                                    V_SORLCUR_REC.SORLCUR_GAPP_SEQNO,
                              V_SORLCUR_REC.SORLCUR_CURRENT_CDE );
      
                  
                  -- UPDATE TERM_CODE_END(vigencia curriculum) 
                  UPDATE SORLCUR 
                  SET   SORLCUR_TERM_CODE_END = P_PERIODO, 
                        SORLCUR_ACTIVITY_DATE_UPDATE = SYSDATE
                  WHERE   SORLCUR_PIDM = P_PIDM_ALUMNO 
                  AND     SORLCUR_LMOD_CODE = 'LEARNER'
                  AND     SORLCUR_SEQNO = P_SORLCUR_SEQNO_OLD;
                  
                  -- UPDATE SORLCUR_CURRENT_CDE(curriculum activo)  PARA registros del mismo PERIODO.
                  UPDATE SORLCUR 
                  SET   SORLCUR_CURRENT_CDE = NULL
                  WHERE   SORLCUR_PIDM = P_PIDM_ALUMNO 
                  AND     SORLCUR_LMOD_CODE = 'LEARNER'
                  AND     SORLCUR_TERM_CODE_END = P_PERIODO
                  AND     SORLCUR_SEQNO = P_SORLCUR_SEQNO_OLD;    
                  
              END LOOP;
              CLOSE C_SORLCUR;
              
              -- #######################################################################
              -- INSERT --- SORLFOS -
              INSERT INTO SORLFOS (
                    SORLFOS_PIDM,             SORLFOS_LCUR_SEQNO,         SORLFOS_SEQNO,
                    SORLFOS_LFST_CODE,        SORLFOS_TERM_CODE,          SORLFOS_PRIORITY_NO,
                    SORLFOS_CSTS_CODE,        SORLFOS_CACT_CODE,          SORLFOS_DATA_ORIGIN,
                    SORLFOS_USER_ID,          SORLFOS_ACTIVITY_DATE,      SORLFOS_MAJR_CODE,
                    SORLFOS_TERM_CODE_CTLG,   SORLFOS_TERM_CODE_END,      SORLFOS_DEPT_CODE,
                    SORLFOS_MAJR_CODE_ATTACH, SORLFOS_LFOS_RULE,          SORLFOS_CONC_ATTACH_RULE,
                    SORLFOS_START_DATE,       SORLFOS_END_DATE,           SORLFOS_TMST_CODE,
                    SORLFOS_ROLLED_SEQNO,     SORLFOS_USER_ID_UPDATE,     SORLFOS_ACTIVITY_DATE_UPDATE,
                    SORLFOS_CURRENT_CDE) 
              SELECT 
                    V_SORLFOS_REC.SORLFOS_PIDM,             P_SORLCUR_SEQNO_NEW,                      1,/*V_SORLFOS_REC.SORLFOS_SEQNO*/
                    V_SORLFOS_REC.SORLFOS_LFST_CODE,        P_PERIODO,                                V_SORLFOS_REC.SORLFOS_PRIORITY_NO,
                    V_SORLFOS_REC.SORLFOS_CSTS_CODE,        V_SORLFOS_REC.SORLFOS_CACT_CODE,          'WorkFlow',
                    USER,                                   SYSDATE,                                  V_SORLFOS_REC.SORLFOS_MAJR_CODE,
                    V_SORLFOS_REC.SORLFOS_TERM_CODE_CTLG,   V_SORLFOS_REC.SORLFOS_TERM_CODE_END,      V_SORLFOS_REC.SORLFOS_DEPT_CODE,
                    V_SORLFOS_REC.SORLFOS_MAJR_CODE_ATTACH, V_SORLFOS_REC.SORLFOS_LFOS_RULE,          V_SORLFOS_REC.SORLFOS_CONC_ATTACH_RULE,
                    V_SORLFOS_REC.SORLFOS_START_DATE,       V_SORLFOS_REC.SORLFOS_END_DATE,           V_SORLFOS_REC.SORLFOS_TMST_CODE,
                    V_SORLFOS_REC.SORLFOS_ROLLED_SEQNO,     USER,                                     SYSDATE,
                    'Y'
              FROM SORLFOS V_SORLFOS_REC
              WHERE SORLFOS_PIDM = P_PIDM_ALUMNO
              AND SORLFOS_LCUR_SEQNO = P_SORLCUR_SEQNO_OLD;
               
              -- UPDATE SORLFOS_CURRENT_CDE(curriculum activo) PARA registros del mismo PERIODO.
              UPDATE SORLFOS 
              SET   SORLFOS_CURRENT_CDE = NULL
              WHERE   SORLFOS_PIDM = P_PIDM_ALUMNO
              AND     SORLFOS_LCUR_SEQNO = P_SORLCUR_SEQNO_OLD
              AND     SORLFOS_TERM_CODE = P_PERIODO
              AND     SORLFOS_CSTS_CODE = 'INPROGRESS';
        
        ----------------------------------------------------------------------------------
        ELSE
        ----------------------------------------------------------------------------------
            
              -- #######################################################################
              -- CAMBIO ESTADO PLAN ESTUDIO  --- FORMA SGASTDN  
              
              -- UPDATE SGASTDN
              UPDATE SGBSTDN
              SET  SGBSTDN_STST_CODE = 'AS'  ------------------ ESTADOS STVSTST : 'AS' - Activo
                  ,SGBSTDN_STYP_CODE = 'R'   ------------------ ESTADOS STVSTYP : 'R' - UC Reincorporado
              WHERE SGBSTDN_PIDM = P_PIDM_ALUMNO
              AND SGBSTDN_TERM_CODE_EFF = P_PERIODO;
              
              -- UPDATE SGRSTSP -- PLAN ESTUDIO
              UPDATE SGRSTSP
              SET SGRSTSP_STSP_CODE = 'AS'
              WHERE SGRSTSP_PIDM = P_PIDM_ALUMNO
              AND SGRSTSP_TERM_CODE_EFF = P_PERIODO
              AND SGRSTSP_STSP_CODE <> 'AS';            
            
        END IF;
        ------------------------------------------------------------------------------------
        
        --
        COMMIT;
EXCEPTION
    WHEN OTHERS THEN
          --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_CAMBIAR_ESTATUS;    


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_REGISTAR_ATRIBUTO (
      P_PIDM_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_PERIODO               IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
      P_COD_ATRIBUTO          IN STVATTS.STVATTS_CODE%TYPE,       -- COD atributos
      P_ERROR                 OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_REGISTAR_ATRIBUTO
  FECHA     : 28/12/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Valida y Agrega un atributo relacionado y vuelve a estimar deudas.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
        P_PERIODO_NEXT              SFBETRM.SFBETRM_TERM_CODE%TYPE;
        NOM_SERVICIO                VARCHAR2(30);
        
        C_SEQNO                   SORLCUR.SORLCUR_SEQNO %type;
        C_ALUM_NIVEL              SORLCUR.SORLCUR_LEVL_CODE%type;
        C_ALUM_CAMPUS             SORLCUR.SORLCUR_CAMP_CODE%type;
        C_ALUM_ESCUELA            SORLCUR.SORLCUR_COLL_CODE%type;
        C_ALUM_GRADO              SORLCUR.SORLCUR_DEGC_CODE%type;
        C_ALUM_PROGRAMA           SORLCUR.SORLCUR_PROGRAM%type;
        C_ALUM_PERD_ADM           SORLCUR.SORLCUR_TERM_CODE_ADMIT%type;
        C_ALUM_TIPO_ALUM          SORLCUR.SORLCUR_STYP_CODE%type;           -- STVSTYP
        C_ALUM_TARIFA             SORLCUR.SORLCUR_RATE_CODE%type;           -- STVRATE
        C_ALUM_DEPARTAMENTO       SORLFOS.SORLFOS_DEPT_CODE%type;
              
        P_COD_DETALLE           SFRRGFE.SFRRGFE_DETL_CODE%TYPE;
        P_INDICADOR             NUMBER;
        C_CARGO_MINIMO          SFRRGFE.SFRRGFE_MIN_CHARGE%TYPE;
        
        SAVE_ACT_DATE_OUT       VARCHAR2(100);
        RETURN_STATUS_IN_OUT    NUMBER;
BEGIN  
  
      -- GET - NEXT Periodo
      SELECT NEXT_TERM INTO P_PERIODO_NEXT
      FROM (
        SELECT STVTERM_CODE, LEAD(STVTERM_CODE, 1, 0) OVER (ORDER BY STVTERM_CODE) NEXT_TERM
        FROM STVTERM ORDER BY STVTERM_CODE DESC
      ) WHERE STVTERM_CODE = P_PERIODO;
    

      -- #######################################################################
      -- GET datos de alumno para VALIDAR y OBTENER el CODIGO DETALLE
      SELECT    SORLCUR_SEQNO,      
                SORLCUR_LEVL_CODE,    
                SORLCUR_CAMP_CODE,        
                SORLCUR_COLL_CODE,
                SORLCUR_DEGC_CODE,  
                SORLCUR_PROGRAM,      
                SORLCUR_TERM_CODE_ADMIT,  
                --SORLCUR_STYP_CODE,
                SORLCUR_STYP_CODE,  
                SORLCUR_RATE_CODE,    
                SORLFOS_DEPT_CODE   
      INTO      C_SEQNO,
                C_ALUM_NIVEL, 
                C_ALUM_CAMPUS, 
                C_ALUM_ESCUELA, 
                C_ALUM_GRADO, 
                C_ALUM_PROGRAMA, 
                C_ALUM_PERD_ADM,
                C_ALUM_TIPO_ALUM,
                C_ALUM_TARIFA,
                C_ALUM_DEPARTAMENTO
      FROM (
              SELECT    SORLCUR_SEQNO,      SORLCUR_LEVL_CODE,    SORLCUR_CAMP_CODE,        SORLCUR_COLL_CODE,
                        SORLCUR_DEGC_CODE,  SORLCUR_PROGRAM,      SORLCUR_TERM_CODE_ADMIT,  --SORLCUR_STYP_CODE,
                        SORLCUR_STYP_CODE,  SORLCUR_RATE_CODE,    SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS.SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM_ALUMNO 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  -- AND SORLCUR_TERM_CODE   =   P_PERIODO 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
              ORDER BY SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM <= 1;

   
      -- GET - Codigo detalle y tambien VALIDA que si se encuentre configurado correctamente.
      SELECT SFRRGFE_DETL_CODE, SFRRGFE_MIN_CHARGE INTO P_COD_DETALLE, C_CARGO_MINIMO
      FROM SFRRGFE 
      WHERE SFRRGFE_TERM_CODE = P_PERIODO AND SFRRGFE_ATTS_CODE = P_COD_ATRIBUTO 
      AND SFRRGFE_TYPE = 'STUDENT'
      AND NVL(NVL(SFRRGFE_LEVL_CODE, c_alum_nivel),'-')           = NVL(NVL(c_alum_nivel, SFRRGFE_LEVL_CODE),'-')---------------- Nivel
      AND NVL(NVL(SFRRGFE_CAMP_CODE, c_alum_campus),'-')          = NVL(NVL(c_alum_campus, SFRRGFE_CAMP_CODE),'-')  ------------- Campus (sede)
      AND NVL(NVL(SFRRGFE_COLL_CODE, c_alum_escuela),'-')         = NVL(NVL(c_alum_escuela, SFRRGFE_COLL_CODE),'-') ------------- Escuela 
      AND NVL(NVL(SFRRGFE_DEGC_CODE, c_alum_grado),'-')           = NVL(NVL(c_alum_grado, SFRRGFE_DEGC_CODE),'-') --------------- Grado
      AND NVL(NVL(SFRRGFE_PROGRAM, c_alum_programa),'-')          = NVL(NVL(c_alum_programa, SFRRGFE_PROGRAM),'-') -------------- Programa
      AND NVL(NVL(SFRRGFE_TERM_CODE_ADMIT, c_alum_perd_adm),'-')  = NVL(NVL(c_alum_perd_adm, SFRRGFE_TERM_CODE_ADMIT),'-') ------ Periodo Admicion
      -- SFRRGFE_PRIM_SEC_CDE -- Curriculums (prim, secundario, cualquiera)
      -- SFRRGFE_LFST_CODE -- Tipo Campo Estudio (MAJOR, ...)
      -- SFRRGFE_MAJR_CODE -- Codigo Campo Estudio (Carrera)
      AND NVL(NVL(SFRRGFE_DEPT_CODE, c_alum_departamento),'-')    = NVL(NVL(c_alum_departamento, SFRRGFE_DEPT_CODE),'-') -------- Departamento
      -- SFRRGFE_LFST_PRIM_SEC_CDE -- Campo Estudio   (prim, secundario, cualquiera)
      AND NVL(NVL(SFRRGFE_STYP_CODE_CURRIC, c_alum_tipo_alum),'-') = NVL(NVL(c_alum_tipo_alum, SFRRGFE_STYP_CODE_CURRIC),'-') --- Tipo Alumno Curriculum
      AND NVL(NVL(SFRRGFE_RATE_CODE_CURRIC, c_alum_tarifa),'-')   = NVL(NVL(c_alum_tarifa, SFRRGFE_RATE_CODE_CURRIC),'-'); ------ Trf Curriculum (Escala)
      
      -- GET - Descripcion de CODIGO DETALLE  
      SELECT TBBDETC_DESC INTO NOM_SERVICIO FROM TBBDETC WHERE TBBDETC_DETAIL_CODE = P_COD_DETALLE;


      -- #######################################################################
      -- SET ATRIBUTO alumno
      INSERT INTO SGRSATT
        (
          SGRSATT_PIDM, 
          SGRSATT_TERM_CODE_EFF,
          SGRSATT_ATTS_CODE,
          SGRSATT_ACTIVITY_DATE,
          SGRSATT_STSP_KEY_SEQUENCE
        )
      SELECT P_PIDM_ALUMNO, P_PERIODO, P_COD_ATRIBUTO, SYSDATE, NULL
      FROM DUAL
      WHERE NOT EXISTS (SELECT * FROM SGRSATT 
                    WHERE SGRSATT_PIDM = P_PIDM_ALUMNO AND SGRSATT_TERM_CODE_EFF = P_PERIODO 
                    AND (SGRSATT_ATTS_CODE = P_COD_ATRIBUTO OR SGRSATT_ATTS_CODE IS NULL));
      P_INDICADOR := SQL%ROWCOUNT;  
      -- ACTUALIZAR en caso exista NULL con el ATRIBUTO para el periodo.
      IF P_INDICADOR = 0  THEN
          UPDATE SGRSATT SET SGRSATT_ATTS_CODE = P_COD_ATRIBUTO
          WHERE SGRSATT_PIDM = P_PIDM_ALUMNO AND SGRSATT_TERM_CODE_EFF = P_PERIODO 
          AND SGRSATT_ATTS_CODE IS NULL 
          AND NOT EXISTS (SELECT SGRSATT_PIDM FROM SGRSATT 
                    WHERE SGRSATT_PIDM = P_PIDM_ALUMNO AND SGRSATT_TERM_CODE_EFF = P_PERIODO
                    AND SGRSATT_ATTS_CODE = P_COD_ATRIBUTO);
      END IF;
   
      -- SET - ATRIBUTO FINALIZAR (LIMITAR VALIDES del atributo A UN SOLO PERIODO)
      INSERT INTO SGRSATT
        ( 
          SGRSATT_PIDM, 
          SGRSATT_TERM_CODE_EFF,
          SGRSATT_ATTS_CODE,
          SGRSATT_ACTIVITY_DATE,
          SGRSATT_STSP_KEY_SEQUENCE
        )
      SELECT P_PIDM_ALUMNO, P_PERIODO_NEXT, NULL, SYSDATE, NULL
      FROM DUAL
      WHERE NOT EXISTS (SELECT * FROM SGRSATT 
                        WHERE SGRSATT_PIDM = P_PIDM_ALUMNO 
                        AND SGRSATT_TERM_CODE_EFF = P_PERIODO_NEXT
                    );
      P_INDICADOR := SQL%ROWCOUNT;

            -- #######################################################################
      -- Procesar DEUDA - Volviendo a estimar la deuda devido al cambio de TARIFA.
      SFKFEES.p_processfeeassessment (  P_PERIODO,
                                        P_PIDM_ALUMNO,
                                        SYSDATE,      -- assessment effective date(Evaluación de la fecha efectiva)
                                        SYSDATE,  -- refund by total refund date(El reembolso por fecha total del reembolso)
                                        'R',          -- use regular assessment rules(utilizar las reglas de evaluación periódica)
                                        'Y',          -- create TBRACCD records
                                        'SFAREGS',    -- where assessment originated from
                                        'Y',          -- commit changes
                                        SAVE_ACT_DATE_OUT,    -- OUT -- save_act_date
                                        'N',          -- do not ignore SFRFMAX rules
                                        RETURN_STATUS_IN_OUT );   -- OUT -- return_status

      COMMIT;
EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
        --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
        P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_REGISTAR_ATRIBUTO;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_UPDATE_CTA_CORRIENTE (
    P_PIDM_ALUMNO           IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PERIODO               IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
    P_ERROR                 OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_UPDATE_CTA_CORRIENTE
  FECHA     : 05/01/2016
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Vuelve a estimar las cuentas de un alumno.

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION
  =================================================================================================================== */
AS
        SAVE_ACT_DATE_OUT       VARCHAR2(100);
        RETURN_STATUS_IN_OUT    NUMBER;
BEGIN
      -- #######################################################################
      -- Procesar DEUDA - Volviendo a estimar la deuda devido al cambio de TARIFA.
      SFKFEES.p_processfeeassessment (  P_PERIODO,
                                        P_PIDM_ALUMNO,
                                        SYSDATE,      -- assessment effective date(Evaluación de la fecha efectiva)
                                        SYSDATE,  -- refund by total refund date(El reembolso por fecha total del reembolso)
                                        'R',          -- use regular assessment rules(utilizar las reglas de evaluación periódica)
                                        'Y',          -- create TBRACCD records
                                        'SFAREGS',    -- where assessment originated from
                                        'Y',          -- commit changes
                                        SAVE_ACT_DATE_OUT,    -- OUT -- save_act_date
                                        'N',          -- do not ignore SFRFMAX rules
                                        RETURN_STATUS_IN_OUT );   -- OUT -- return_status

        COMMIT;
EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
        --raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
        P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_UPDATE_CTA_CORRIENTE;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_SET_GR_INSCRIPC_ABIERTO (
      P_PIDM_ALUMNO         IN SPRIDEN.SPRIDEN_PIDM%TYPE ,
      P_PERIODO             IN SFBRGRP.SFBRGRP_TERM_CODE%TYPE ,
      P_MODALIDAD_ALUMNO    IN STVDEPT.STVDEPT_CODE%TYPE , 
      P_COD_SEDE            IN STVCAMP.STVCAMP_CODE%TYPE ,
      P_ERROR               OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_SET_GR_INSCRIPC_ABIERTO
  FECHA     : 05/01/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Asignar el código de grupo de inscripción del estudiante.

                          Ejemplo: 99-99WF01
------------------------------------------------------------------------------
    Rectificación               Modalidad                 Sede            
99-99 – Rectificación     R – Regular                   S01 – Huancayo
                          W – Gente que trabaja         F01 – Arequipa 
                          V – Virtual                   F02 – Lima  
                                                        F03 – Cusco
                                                        V00 – Virtual

  MODIFICACIONES
  NRO   FECHA   USUARIO   MODIFICACION

  =================================================================================================================== */
AS
      P_RPGRP               SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      P_RPGRP_NEW           SFBRGRP.SFBRGRP_RGRP_CODE%TYPE;
      P_MODALIDAD_SEDE      VARCHAR2(4);
      P_MODALIDAD_SEDE2     VARCHAR2(4);
      
      P_MESSAGE             EXCEPTION;
      P_MESSAGE2            EXCEPTION;
      P_MESSAGE3            EXCEPTION;
      P_INDICADOR           NUMBER := 0;
      P_ROW_UPDATE          NUMBER := 0;
      P_COD_RECTF           VARCHAR2(15) := '99-99';
BEGIN
--
      
      -- VALIDACION : CODIGO grupo insc. y datos proporcionados
      SELECT CASE P_MODALIDAD_ALUMNO 
                WHEN 'UVIR' THEN 'V' --#UC-SEMI PRESENCIAL (EV)
                WHEN 'UPGT' THEN 'W' --#UC-SEMI PRESENCIAL (GT)
                WHEN 'UREG' THEN 'R' --#UC-PRESENCIAL
                ELSE '' END INTO P_MODALIDAD_SEDE2 FROM DUAL;
      
      -- OBTENER CADENA DE MODALIDAD Y SEDE  del CODIGO DE GRUPO  
      P_MODALIDAD_SEDE := (P_MODALIDAD_SEDE2 || P_COD_SEDE);
      
      -- CODIGO DEL GRUPO DE RECTIFICACIÒN : 99-99<cod_modadlidad><cod_sede>
      SELECT CONCAT(P_COD_RECTF,P_MODALIDAD_SEDE) INTO P_RPGRP_NEW FROM DUAL;
      
      -- Validar que exista el codigo del grupo de inscripcion
      SELECT COUNT(SFBWCTL_RGRP_CODE) INTO P_INDICADOR
      FROM SFBWCTL
      WHERE SFBWCTL_RGRP_CODE  = P_RPGRP_NEW
      AND  SFBWCTL_TERM_CODE = P_PERIODO;
      
      -- ASIGNAR GRUPO DE INSCRIPCION - forma SFARGRP
      IF(P_INDICADOR = 1) THEN
            INSERT INTO SFBRGRP (
                  SFBRGRP_TERM_CODE, 
                  SFBRGRP_PIDM, 
                  SFBRGRP_RGRP_CODE, 
                  SFBRGRP_USER, 
                  SFBRGRP_ACTIVITY_DATE
              )
            SELECT P_PERIODO, P_PIDM_ALUMNO, P_RPGRP_NEW, USER, SYSDATE 
            FROM DUAL
            WHERE EXISTS (
                  ------ LA DEFINICION DE LA VISTA SFVRGRP
                  SELECT SFBWCTL.SFBWCTL_TERM_CODE,
                  SFBWCTL.SFBWCTL_RGRP_CODE,
                  SFBWCTL.SFBWCTL_PRIORITY,
                  SFRWCTL.SFRWCTL_BEGIN_DATE,
                  SFRWCTL.SFRWCTL_END_DATE,
                        SFRWCTL.SFRWCTL_HOUR_BEGIN,
                        SFRWCTL.SFRWCTL_HOUR_END
                  FROM SFRWCTL, SFBWCTL
                        WHERE SFRWCTL.SFRWCTL_TERM_CODE = SFBWCTL.SFBWCTL_TERM_CODE
                        AND   SFRWCTL.SFRWCTL_PRIORITY  = SFBWCTL.SFBWCTL_PRIORITY
                        AND   SFBWCTL.SFBWCTL_RGRP_CODE = P_RPGRP_NEW
                        AND   SFRWCTL.SFRWCTL_TERM_CODE = P_PERIODO
            );
      ELSE
          RAISE P_MESSAGE;
      END IF;

      COMMIT;

EXCEPTION
  WHEN P_MESSAGE THEN
          P_ERROR := 'Se detecto un inconveniente con el grupo de inscripción: ' || P_RPGRP_NEW || ' verificar la forma SFARCTL.';
  WHEN OTHERS THEN
          P_ERROR := 'A ocurrido un error  - '||SQLCODE||' -ERROR- '||SQLERRM;
END P_SET_GR_INSCRIPC_ABIERTO;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--



--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--                   
END WFK_CONTISR;