/**********************************************************************++
-- PROCEDIMIENTO PARA AGREGARLO AL PAQUETE QUE USA EL TRIGGER -->  BWKSVPR  
/**********************************************************************/
    

--## SOLICITUD DE REINCORPORACIÓN (CONTISRE) ## 
CREATE OR REPLACE PROCEDURE P_CONTISREI_GETDATA( 
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
)
/* ===================================================================================================================
  NOMBRE    : P_CONTISRE_GETDATA
  FECHA     : 14/08/2017
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtener los datos dept, camp, nombre camp y periodo de un PIDM

  MODIFICACIONES
  NRO     FECHA         USUARIO     MODIFICACION
  =================================================================================================================== */
AS
      
      V_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_1          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_2          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_INDICADOR           NUMBER;

      -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
      CURSOR C_CAMPDEPT IS
      SELECT    SORLCUR_CAMP_CODE,
                SORLFOS_DEPT_CODE,
                STVCAMP_DESC
      FROM (
              SELECT    SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, STVCAMP_DESC
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              INNER JOIN STVCAMP
                    ON SORLCUR_CAMP_CODE = STVCAMP_CODE
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE =   'Y'
              ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM = 1;

      -- Calculando parte PERIODO (sub PTRM)
      CURSOR C_SFRRSTS_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;
      
BEGIN

      P_DEPT_CODE  := '-';
      P_CAMP_CODE  := '-';
      P_CAMP_DESC  := '-';
      P_TERM_CODE  := '-';
      

      -- >> GET DEPARTAMENTO y CAMPUS --
      OPEN C_CAMPDEPT;
      LOOP
        FETCH C_CAMPDEPT INTO P_CAMP_CODE,P_DEPT_CODE,P_CAMP_DESC ;
        EXIT WHEN C_CAMPDEPT%NOTFOUND;
      END LOOP;
      CLOSE C_CAMPDEPT;

     
      -- >> calculando PARTE PERIODO  --
      OPEN C_SFRRSTS_PTRM;
      LOOP
        FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
        EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
      END LOOP;
      CLOSE C_SFRRSTS_PTRM;


      -- PARTE DE PERIODOS
      V_SUB_PTRM_1 := V_SUB_PTRM || '1';
      V_SUB_PTRM_2 := V_SUB_PTRM || CASE WHEN (P_DEPT_CODE ='UVIR' OR P_DEPT_CODE ='UPGT') THEN '2' ELSE '-' END;
 

       /******************************************************************************************************
        GET TERM (PERIODO  ACTIVO - Solo usando parte de periodo 1)  ó PERIODO MAS PROXIMO del alumno,
      */
      SELECT COUNT(SFRRSTS_TERM_CODE) INTO V_INDICADOR FROM SFRRSTS WHERE SFRRSTS_PTRM_CODE LIKE (V_SUB_PTRM || '1'  ) 
      AND SFRRSTS_RSTS_CODE = 'RW' AND P_RECEPTION_DATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE;
      IF V_INDICADOR = 0 THEN

              SELECT SOBPTRM_TERM_CODE INTO V_INDICADOR FROM SOBPTRM
              WHERE  SOBPTRM_PTRM_CODE LIKE (V_SUB_PTRM || '%') AND SYSDATE <= SOBPTRM_START_DATE;
              IF V_INDICADOR = 0 THEN
                    P_TERM_CODE := '-';
              ELSE
                    -- GET PERIODO ACTIVO mas cercano 
                    SELECT SOBPTRM_TERM_CODE INTO P_TERM_CODE
                    FROM (
                        -- GET COD PARTE-PERIODO activo ò un PERIODO proximo valido para realizar la reincorporacion
                        SELECT SOBPTRM_TERM_CODE FROM SOBPTRM
                        WHERE  SOBPTRM_PTRM_CODE LIKE (V_SUB_PTRM || '%')
                        AND SYSDATE <= SOBPTRM_START_DATE
                        ORDER BY SOBPTRM_TERM_CODE ASC
                    ) WHERE ROWNUM <= 1;
              END IF;

      ELSE
            SELECT SFRRSTS_TERM_CODE INTO  P_TERM_CODE FROM (
                -- Forma SFARSTS  ---> fechas para las partes de periodo
                SELECT DISTINCT SFRRSTS_TERM_CODE 
                FROM SFRRSTS
                WHERE SFRRSTS_PTRM_CODE LIKE (V_SUB_PTRM || '1'  )
                AND SFRRSTS_RSTS_CODE = 'RW' -- inscrito por web
                AND P_RECEPTION_DATE BETWEEN SFRRSTS_START_DATE AND SFRRSTS_END_DATE
                ORDER BY SFRRSTS_TERM_CODE DESC
            ) WHERE ROWNUM <= 1;
      END IF;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CONTISREI_GETDATA;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
