SET SERVEROUTPUT ON
DECLARE
    P_PIDM        SPRIDEN.SPRIDEN_PIDM%TYPE;
    P_TERM_CODE   STVTERM.STVTERM_CODE%TYPE;
    P_MESSAGE     VARCHAR2(4000);
BEGIN
P_SET_PERIODO_CAT ('202294','201810',P_MESSAGE);
END;


--UPDATE SORLCUR
--SET SORLCUR_TERM_CODE_CTLG = '201800'
--WHERE SORLCUR_PIDM = P_PIDM
--      AND SORLCUR_LMOD_CODE = 'LEARNER'
--      AND SORLCUR_CACT_CODE = 'ACTIVE'
--      AND SORLCUR_CURRENT_CDE = 'Y'
--      AND SORLCUR_TERM_CODE_END IS NULL;
--      
--
--UPDATE SORLFOS
--SET SORLFOS_TERM_CODE_CTLG = '201800'
--WHERE SORLFOS_PIDM = P_PIDM     
--      AND SORLFOS_CACT_CODE = 'ACTIVE'
--      AND SORLFOS_CURRENT_CDE = 'Y'
--      AND SORLFOS_TERM_CODE = P_TERM_CODE;



CREATE OR REPLACE PROCEDURE P_SET_PERIODO_CAT (
          P_PIDM         IN SPRIDEN.SPRIDEN_PIDM%TYPE,
          P_TERM_CODE    IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
          P_MESSAGE      OUT VARCHAR2
  )
  /* ===================================================================================================================
    NOMBRE    : P_SET_PERIODO_CAT
    FECHA     : 18/03/2018
    AUTOR     : Arana Milla, Karina Lizbeth
    OBJETIVO  : Actualiza el periodo de cat�logo en las tablas SGBSTDN, SORLCUR y SORLFOS.
    =================================================================================================================== */
  AS
       
  BEGIN 
        -- UPDATE PER CATALOGO - SGBSTDN    
        UPDATE SGBSTDN
        SET SGBSTDN_TERM_CODE_CTLG_1 = '201800'
        WHERE SGBSTDN_PIDM = P_PIDM
              AND SGBSTDN_TERM_CODE_EFF = P_TERM_CODE
              AND SGBSTDN_STYP_CODE = 'R';
        COMMIT;
        
        -- UPDATE PER CATALOGO - SORLCUR
        UPDATE SORLCUR
        SET SORLCUR_TERM_CODE_CTLG = '201800'
        WHERE SORLCUR_PIDM = P_PIDM
              AND SORLCUR_LMOD_CODE = 'LEARNER'
              AND SORLCUR_CACT_CODE = 'ACTIVE'
              AND SORLCUR_CURRENT_CDE = 'Y'
              AND SORLCUR_TERM_CODE_END IS NULL
              AND SORLCUR_TERM_CODE = P_TERM_CODE;
        COMMIT;
        
        -- UPDATE PER CATALOGO - SORLFOS
        UPDATE SORLFOS
        SET SORLFOS_TERM_CODE_CTLG = '201800'
        WHERE SORLFOS_PIDM = P_PIDM     
              AND SORLFOS_CACT_CODE = 'ACTIVE'
              AND SORLFOS_CURRENT_CDE = 'Y'
              AND SORLFOS_TERM_CODE = P_TERM_CODE;
        COMMIT;

  EXCEPTION
  WHEN OTHERS THEN
        P_MESSAGE := 'Error o inconsistencia detectada -'||SQLCODE||' -ERROR- '||SQLERRM;
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
  END P_SET_PERIODO_CAT;