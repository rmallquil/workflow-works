/*
drop procedure P_GET_INFO_STUDENT;
GRANT EXECUTE ON P_OBTENER_USUARIO TO wfobjects;
GRANT EXECUTE ON P_OBTENER_USUARIO TO wfauto;

set serveroutput on
DECLARE
P_PIDM              SPRIDEN.SPRIDEN_PIDM%TYPE;
P_DEPT_CODE         STVDEPT.STVDEPT_CODE%TYPE;
P_CAMP_CODE         STVCAMP.STVCAMP_CODE%TYPE;
P_CAMP_DESC         STVCAMP.STVCAMP_DESC%TYPE;
P_EMAIL             GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE;
P_NAME              VARCHAR2(4000);
begin
  P_GET_INFO_STUDENT('70163334',P_PIDM,P_DEPT_CODE,P_CAMP_CODE,P_CAMP_DESC,P_EMAIL,P_NAME);
  DBMS_OUTPUT.PUT_LINE(P_PIDM || '---' || P_DEPT_CODE || '---' ||  P_CAMP_CODE || '---' || P_CAMP_DESC || '---' || P_EMAIL || '---' || P_NAME);
end;
*/

create or replace PROCEDURE P_GET_INFO_STUDENT (
      P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
      P_PIDM              OUT SPRIDEN.SPRIDEN_PIDM%TYPE,
      P_DEPT_CODE         OUT STVDEPT.STVDEPT_CODE%TYPE,
      P_CAMP_CODE         OUT STVCAMP.STVCAMP_CODE%TYPE,
      P_CAMP_DESC         OUT STVCAMP.STVCAMP_DESC%TYPE,
      P_PROGRAM           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
      P_EMAIL             OUT GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE,
      P_NAME              OUT VARCHAR2      
)
/* ===================================================================================================================
  NOMBRE    : P_GET_INFO_STUDENT
  FECHA     : 05/12/16
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : Obtiene información de un usuario(estudiante) 

  =================================================================================================================== */
AS
      -- @PARAMETERS
      V_INDICADOR         NUMBER;

      -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
      CURSOR C_CAMPDEPT IS
      SELECT    SORLCUR_CAMP_CODE,
                SORLCUR_PROGRAM,
                SORLFOS_DEPT_CODE,
                STVCAMP_DESC
      FROM (
              SELECT    SORLCUR_CAMP_CODE, SORLCUR_PROGRAM, SORLFOS_DEPT_CODE, STVCAMP_DESC
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              INNER JOIN STVCAMP
                    ON SORLCUR_CAMP_CODE = STVCAMP_CODE
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE =   'Y'
              ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM = 1;

BEGIN 
      
      -----------------------------------------------------------------------------
      -- GET nombres , ID
      SELECT (SPRIDEN_FIRST_NAME || ' ' || SPRIDEN_LAST_NAME), SPRIDEN_PIDM INTO P_NAME, P_PIDM  FROM SPRIDEN 
      WHERE SPRIDEN_ID = P_ID AND SPRIDEN_CHANGE_IND IS NULL;


      -----------------------------------------------------------------------------
      -- >> GET DEPARTAMENTO y CAMPUS --
      OPEN C_CAMPDEPT;
      LOOP
        FETCH C_CAMPDEPT INTO P_CAMP_CODE,P_PROGRAM,P_DEPT_CODE,P_CAMP_DESC ;
        EXIT WHEN C_CAMPDEPT%NOTFOUND;
      END LOOP;
      CLOSE C_CAMPDEPT;


      -----------------------------------------------------------------------------
      -- GET email
      SELECT COUNT(*) INTO V_INDICADOR FROM GOREMAL WHERE GOREMAL_PIDM = P_PIDM and GOREMAL_STATUS_IND = 'A';
      IF V_INDICADOR = 0 THEN
           P_EMAIL := '-';
      ELSE
              SELECT COUNT(*) INTO V_INDICADOR FROM GOREMAL WHERE GOREMAL_PIDM = P_PIDM and GOREMAL_STATUS_IND = 'A' 
              AND GOREMAL_EMAIL_ADDRESS LIKE ('%@continental.edu.pe');            
              IF (V_INDICADOR > 0) THEN
                  SELECT GOREMAL_EMAIL_ADDRESS INTO P_EMAIL FROM (
                    SELECT GOREMAL_EMAIL_ADDRESS, GOREMAL_PREFERRED_IND FROM GOREMAL 
                    WHERE GOREMAL_PIDM = P_PIDM and GOREMAL_STATUS_IND = 'A' AND GOREMAL_EMAIL_ADDRESS LIKE ('%@continental.edu.pe')
                    ORDER BY GOREMAL_PREFERRED_IND DESC
                  )WHERE ROWNUM <= 1;
              ELSE
                  SELECT GOREMAL_EMAIL_ADDRESS INTO P_EMAIL FROM (
                      SELECT GOREMAL_EMAIL_ADDRESS, GOREMAL_PREFERRED_IND FROM GOREMAL
                      WHERE GOREMAL_PIDM = P_PIDM and GOREMAL_STATUS_IND = 'A'
                      ORDER BY GOREMAL_PREFERRED_IND DESC
                  )WHERE ROWNUM <= 1;
              END IF;
      END IF;
      
EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_INFO_STUDENT;

       
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------