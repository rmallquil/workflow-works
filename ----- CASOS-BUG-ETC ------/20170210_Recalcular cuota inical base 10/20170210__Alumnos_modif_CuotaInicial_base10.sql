/*
  Alumnos que se solicitaron llevar menos creditos y se les modific``o su cuota inicial
  pero que inicialmente se realizo sobre 11 creaditos y que actualmente se modifico para q sea sobre 10.
*/
SET SERVEROUTPUT ON;
DECLARE P_RESULT INTEGER;
BEGIN
    -- ACTUALIZAR MONTO PAGAR (CREDITOS 11)
    P_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
          'dbo.sp_updateCuotaInicial "'
          || 'UCCI' ||'" , "'|| 'HYO' ||'" , "'|| '2017-1' ||'" , "'|| '45776314' ||'" , "'|| '10' ||'"' 
    );
    DBMS_OUTPUT.PUT_LINE(P_RESULT);
    
    P_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
          'dbo.sp_updateCuotaInicial "'
          || 'UCCI' ||'" , "'|| 'HYO' ||'" , "'|| '2017-1' ||'" , "'|| '71655520' ||'" , "'|| '10' ||'"' 
    );
    DBMS_OUTPUT.PUT_LINE(P_RESULT);
    
    P_RESULT := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@BDUCCI.CONTINENTAL.EDU.PE (
          'dbo.sp_updateCuotaInicial "'
          || 'UCCI' ||'" , "'|| 'HYO' ||'" , "'|| '2017-1' ||'" , "'|| '72809174' ||'" , "'|| '10' ||'"' 
    );
    DBMS_OUTPUT.PUT_LINE(P_RESULT);
    
    COMMIT;
END;