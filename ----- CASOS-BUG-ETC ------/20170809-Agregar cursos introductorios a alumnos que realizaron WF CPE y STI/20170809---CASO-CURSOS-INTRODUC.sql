

/* ===================================================================================================================
NOMBRE    : -
FECHA   : 08/08/2017
AUTOR   : Richard Mallqui Lopez 
OBJETIVO  : agrega cursos introductorios para aquellos alumnos que realizaron Solicitud de Traslado Interno y Cambio de Plan
            pero que en su momento no estaba considerado éste punto.

MODIFICACIONES
NRO   FECHA   USUARIO   MODIFICACION
=================================================================================================================== */
SET SERVEROUTPUT ON
DECLARE
      V_PIDM      SPRIDEN.SPRIDEN_PIDM%TYPE;
      CURSOR C_SORTEST IS
        --SELECT DISTINCT SVRSVPR_PIDM, SPRIDEN_ID, (CASE SVRSVPR_SRVC_CODE WHEN 'SOL004' THEN 'CAMBIO DE PLAN' WHEN 'SOL013' THEN 'TRASLADO INTERNO' ELSE '-' END)  WF
        SELECT DISTINCT SVRSVPR_PIDM
        FROM SVRSVPR
        INNER JOIN SPRIDEN
        ON SVRSVPR_PIDM = SPRIDEN_PIDM
        WHERE SPRIDEN_CHANGE_IND IS NULL
        AND SVRSVPR_SRVC_CODE IN ('SOL004','SOL0013')
        AND SVRSVPR_RECEPTION_DATE > '23/07/2017'
        AND SVRSVPR_SRVS_CODE = 'CO'
        AND SVRSVPR_PIDM NOT IN (
              SELECT DISTINCT SORTEST_PIDM FROM (
                    SELECT SORTEST_PIDM, COUNT(SORTEST_TESC_CODE) N_RM_RV FROM (
                          SELECT DISTINCT SORTEST_PIDM,SORTEST_TESC_CODE FROM SORTEST WHERE SORTEST_TESC_CODE = 'RM' AND SORTEST_TEST_SCORE >= '10.5'
                          UNION
                          SELECT DISTINCT SORTEST_PIDM,SORTEST_TESC_CODE FROM SORTEST WHERE SORTEST_TESC_CODE = 'RV' AND SORTEST_TEST_SCORE >= '10.5'
                    ) GROUP BY SORTEST_PIDM
              ) WHERE N_RM_RV >= 2
        );
BEGIN
      -- GET los PIDMs de alumnos que completaron Traslado Interno u Cambio de Plan y les falta los cursos introductorios.
      OPEN C_SORTEST;
        LOOP
          FETCH C_SORTEST INTO V_PIDM;
          EXIT WHEN C_SORTEST%NOTFOUND;
          
            ----------------------------------------------------------------------------
            -- INSERT curso introductorio de RM
            INSERT INTO SORTEST ( 
                  SORTEST_PIDM,         SORTEST_TESC_CODE,      SORTEST_TEST_DATE,
                  SORTEST_TEST_SCORE,   SORTEST_ACTIVITY_DATE,  SORTEST_TERM_CODE_ENTRY,
                  SORTEST_RELEASE_IND,  SORTEST_EQUIV_IND,      SORTEST_USER_ID,
                  SORTEST_DATA_ORIGIN   ) 
            SELECT 
                  V_PIDM,               'RM',                   SYSDATE,
                  '11.00',              SYSDATE,                '201720',
                  'N',                  'N',                    USER,
                  'WorkFlow'
            FROM DUAL
            WHERE NOT EXISTS (
                SELECT * FROM SORTEST 
                WHERE SORTEST_PIDM = V_PIDM AND SORTEST_TESC_CODE = 'RM'
                AND SORTEST_TEST_SCORE >= '10.5'
            );
            
            ----------------------------------------------------------------------------
            -- INSERT curso introductorio de RV
            INSERT INTO SORTEST ( 
                  SORTEST_PIDM,         SORTEST_TESC_CODE,      SORTEST_TEST_DATE,
                  SORTEST_TEST_SCORE,   SORTEST_ACTIVITY_DATE,  SORTEST_TERM_CODE_ENTRY,
                  SORTEST_RELEASE_IND,  SORTEST_EQUIV_IND,      SORTEST_USER_ID,
                  SORTEST_DATA_ORIGIN   ) 
            SELECT 
                  V_PIDM,               'RV',                   SYSDATE,
                  '11.00',              SYSDATE,                '201720',
                  'N',                  'N',                    USER,
                  'WorkFlow'
            FROM DUAL
            WHERE NOT EXISTS (
                SELECT SORTEST_PIDM FROM SORTEST 
                WHERE SORTEST_PIDM = V_PIDM AND SORTEST_TESC_CODE = 'RV'
                AND SORTEST_TEST_SCORE >= '10.5'
            );
          
        END LOOP;
      CLOSE C_SORTEST;
      --------------------------------------------------------------------------
      COMMIT;
END;
