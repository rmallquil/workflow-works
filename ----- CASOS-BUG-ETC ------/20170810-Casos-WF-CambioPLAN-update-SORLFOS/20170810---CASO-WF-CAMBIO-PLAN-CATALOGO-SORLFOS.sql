

/* ===================================================================================================================
NOMBRE    : -
FECHA   : 10/08/2017
AUTOR   : Richard Mallqui Lopez 
OBJETIVO  : Se corrige los alumnos a los cuales no se actualizo el EL PERIODO DE CATALOGO (sgastdn->curricula->campo de estudio)

MODIFICACIONES
NRO   FECHA   USUARIO   MODIFICACION
=================================================================================================================== */

SET SERVEROUTPUT ON
DECLARE
    V_LCUR_PIDM                   SPRIDEN.SPRIDEN_PIDM%TYPE;
    V_LCUR_SEQNO_MAX              SORLCUR.SORLCUR_SEQNO%TYPE;
    V_SORLFOS_SEQNO               SORLFOS.SORLFOS_SEQNO%TYPE;
    V_LCUR_TERM_CODE_CTLG         SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE;
    V_SORLFOS_TERM_CODE_CTLG      SORLFOS.SORLFOS_TERM_CODE_CTLG%TYPE;
    V_INDICADOR                   NUMBER := 0;
    
    CURSOR C_CTLG_LFOS IS
          SELECT  LCUR_PIDM, LCUR_SEQNO_MAX, SORLFOS_SEQNO, LCUR_TERM_CODE_CTLG, SORLFOS_TERM_CODE_CTLG FROM (
                  SELECT LCUR_PIDM, LCUR_TERM_MAX, LCUR_SEQNO_MAX, SORLCUR_TERM_CODE_CTLG LCUR_TERM_CODE_CTLG FROM (
                          SELECT LCUR_PIDM, LCUR_TERM_MAX, MAX(SORLCUR_SEQNO) LCUR_SEQNO_MAX FROM (
                                  SELECT SORLCUR_PIDM LCUR_PIDM, MAX(SORLCUR_TERM_CODE) LCUR_TERM_MAX
                                  FROM SORLCUR WHERE SORLCUR_CACT_CODE   =   'ACTIVE' 
                                  AND SORLCUR_CURRENT_CDE =   'Y'
                                  AND SORLCUR_PIDM IN (
                                          SELECT SVRSVPR_PIDM
                                          FROM SVRSVPR WHERE SVRSVPR_SRVC_CODE IN ('SOL004')
                                          AND SVRSVPR_RECEPTION_DATE > '23/07/2017'
                                          AND SVRSVPR_SRVS_CODE = 'CO')
                                  GROUP BY SORLCUR_PIDM
                          )T1 INNER JOIN SORLCUR
                          ON T1.LCUR_PIDM = SORLCUR.SORLCUR_PIDM
                          AND T1.LCUR_TERM_MAX = SORLCUR.SORLCUR_TERM_CODE
                          GROUP BY LCUR_PIDM, LCUR_TERM_MAX
                  ) INNER JOIN SORLCUR
                  ON LCUR_PIDM = SORLCUR_PIDM
                  AND LCUR_TERM_MAX = SORLCUR_TERM_CODE
                  AND LCUR_SEQNO_MAX = SORLCUR_SEQNO
          )T3 INNER JOIN SORLFOS T_LFOS
          ON LCUR_PIDM = T_LFOS.SORLFOS_PIDM
          AND LCUR_SEQNO_MAX = T_LFOS.SORLFOS_LCUR_SEQNO
          WHERE T_LFOS.SORLFOS_TERM_CODE_CTLG <> LCUR_TERM_CODE_CTLG
          AND T_LFOS.SORLFOS_TERM_CODE_CTLG < '201710';

BEGIN

      -- #######################################################################
      -- INSERT  ------> SORLCUR -
      OPEN C_CTLG_LFOS;
      LOOP
          FETCH C_CTLG_LFOS INTO V_LCUR_PIDM, V_LCUR_SEQNO_MAX, V_SORLFOS_SEQNO, V_LCUR_TERM_CODE_CTLG, V_SORLFOS_TERM_CODE_CTLG;
          EXIT WHEN C_CTLG_LFOS%NOTFOUND;
          
          UPDATE SORLFOS SET  SORLFOS_TERM_CODE_CTLG = V_LCUR_TERM_CODE_CTLG,
                              SORLFOS_ACTIVITY_DATE = SYSDATE,
                              SORLFOS_USER_ID = 'WFAUTO',
                              SORLFOS_ACTIVITY_DATE_UPDATE = SYSDATE,
                              SORLFOS_USER_ID_UPDATE = 'WFAUTO'
          WHERE SORLFOS_PIDM = V_LCUR_PIDM
          AND SORLFOS_LCUR_SEQNO = V_LCUR_SEQNO_MAX
          AND SORLFOS_SEQNO = V_SORLFOS_SEQNO
          AND SORLFOS_TERM_CODE_CTLG = V_SORLFOS_TERM_CODE_CTLG;
          
          V_INDICADOR := V_INDICADOR + SQL%ROWCOUNT;

          END LOOP;
      CLOSE C_CTLG_LFOS;
      
      
      DBMS_OUTPUT.PUT_LINE('N° Rows fectados  --> ' || V_INDICADOR);
      
      -- 
      COMMIT;
      --
END;
