/*
  Alumnos INGRESANTES 201710 que solicitaron SOLICITUD DE RESERVA fuera de fecha:
  Regulacion de sus periodo a los cuales se les realiza Reserva de Matrícula
*/
SET SERVEROUTPUT ON
DECLARE P_ERROR                   VARCHAR2(100);
BEGIN
    -- Caso del alumno ID: 76140396
    WFK_CONTISRM.P_ELIMINAR_NRCS(200571,'201710',P_ERROR);
    DBMS_OUTPUT.PUT_LINE('ERROR :' || P_ERROR || ' -- AFECTADOS :' || SQL%ROWCOUNT);
END;