create or replace PACKAGE WFK_CONTI_TESTFUNCION AS
/*******************************************************************************
 WFK_CONTI_TESTFUNCION:
       Conti Package --------------------
*******************************************************************************/
-- FILE NAME..: WFK_CONTI_TESTFUNCION.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_CONTI_TESTFUNCION
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/

      FUNCTION F_TESTFUNCION RETURN BOOLEAN ;

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END WFK_CONTI_TESTFUNCION;

SHOW ERRORS
 
SET SCAN ON
WHENEVER SQLERROR CONTINUE
DROP PUBLIC SYNONYM WFK_CONTI_TESTFUNCION;
WHENEVER SQLERROR EXIT ROLLBACK
WHENEVER SQLERROR CONTINUE
START gurgrtb WFK_CONTI_TESTFUNCION
START gurgrth WFK_CONTI_TESTFUNCION
WHENEVER SQLERROR EXIT ROLLBACK
CREATE PUBLIC SYNONYM WFK_CONTI_TESTFUNCION FOR WFK_CONTI_TESTFUNCION;
