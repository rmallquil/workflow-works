
create or replace PACKAGE BODY WFK_CONTI_TESTFUNCION AS
/*******************************************************************************
 WFK_CONTI_TESTFUNCION:
       Conti Package --------------------
*******************************************************************************/
-- FILE NAME..: WFK_CONTI_TESTFUNCION.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: WFK_CONTI_TESTFUNCION
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2016
 /******************************************************************************
  DESCRIPTION:
              -
  DESCRIPTION END 
*******************************************************************************/

    FUNCTION F_TESTFUNCION RETURN BOOLEAN 
    AS
        P_INDICADOR        NUMBER := 0;
    BEGIN  
        RETURN(P_INDICADOR > 0);
    END F_TESTFUNCION;
    
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--                   
END WFK_CONTI_TESTFUNCION;