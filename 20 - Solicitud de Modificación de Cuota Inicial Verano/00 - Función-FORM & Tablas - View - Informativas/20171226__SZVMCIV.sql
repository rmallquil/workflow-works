/**********************************************************************************************/
/* SZVMCIV.sql                                                                               */
/**********************************************************************************************/
/*                                                                                            */
/* Descripción corta: Script para generar la vista SZVMCIV                                    */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI             FECHA       */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creación del Código.                                        LAM           06/DIC/2017   */
/*    --------------------                                                                    */
/*    Se crea la vista SZVMCIV para elegir los créditos aptos a inscripción para el ciclo     */
/*    verano entre el rango de 1 a 4 créditos para el DEPT UREG.                              */
/*                                                                                            */
/* 2. Actualización.                                                                          */
/*    --------------                                                                          */
/*                                                                LAM           26/DIC/2017   */
/*    Se agrega las opciones de créditos a mostrar en un rango de 1 - 6 para los DEPT's UPGT  */
/*    y UVIR.                                                                                 */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/ 

  CREATE OR REPLACE VIEW "BANINST1"."SZVMCIV" (CONFIRM_CRED_ID, CONFIRM_CRED_DESC) AS 
  WITH cteSZVMCIV AS (		
			SELECT 'UREG' SZVMCIV_DEPT_CODE, 1 SZVMCIV_CRED_ID, '1 crédito' SZVMCIV_CRED_DESC FROM DUAL UNION
            SELECT 'UREG', 2, '2 créditos' FROM DUAL UNION
            SELECT 'UREG', 3, '3 créditos' FROM DUAL UNION
            SELECT 'UREG', 4, '4 - 12 créditos' FROM DUAL UNION
            SELECT 'UPGT', 1, '1 crédito' FROM DUAL UNION
            SELECT 'UPGT', 2, '2 créditos' FROM DUAL UNION
            SELECT 'UPGT', 3, '3 créditos' FROM DUAL UNION
            SELECT 'UPGT', 4, '4 créditos' FROM DUAL UNION
            SELECT 'UPGT', 5, '5 créditos' FROM DUAL UNION
            SELECT 'UPGT', 6, '6 - 12 créditos' FROM DUAL UNION
			SELECT 'UVIR', 1, '1 crédito' FROM DUAL UNION
            SELECT 'UVIR', 2, '2 créditos' FROM DUAL UNION
            SELECT 'UVIR', 3, '3 créditos' FROM DUAL UNION
            SELECT 'UVIR', 4, '4 créditos' FROM DUAL UNION
            SELECT 'UVIR', 5, '5 créditos' FROM DUAL UNION
            SELECT 'UVIR', 6, '6 - 12 créditos' FROM DUAL)
		SELECT SZVMCIV_CRED_ID, SZVMCIV_CRED_DESC
			FROM cteSZVMCIV
			WHERE SZVMCIV_DEPT_CODE = (SELECT SORLFOS_DEPT_CODE
											FROM (SELECT SORLFOS_DEPT_CODE
													FROM SORLCUR        
									            INNER JOIN SORLFOS
									                ON SORLCUR_PIDM = SORLFOS_PIDM 
									                    AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
									            WHERE SORLCUR_PIDM = BVSKOSAJ.F_GetParamValue('SOL020',999)
									                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
									                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
									                  AND SORLCUR_CURRENT_CDE =   'Y'
									            ORDER BY SORLCUR_TERM_CODE DESC,SORLCUR_SEQNO DESC)
											WHERE ROWNUM <= 1);