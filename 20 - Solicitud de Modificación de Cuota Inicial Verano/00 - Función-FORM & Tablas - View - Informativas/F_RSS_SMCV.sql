
/**********************************************************************************************/
/* F_RSS_SMCV.sql                                                                             */
/**********************************************************************************************/
/*                                                                                            */
/* Descripción corta: Script para generar la función F_RSS_SMCV                               */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI             FECHA       */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creación del Código.                                        LAM           06/DIC/2017   */
/*    --------------------                                                                    */
/*    Se crea la función F_RSS_SMCV para validar que el estudiante no sea cachimbo, sea de la */                              
/*    modalidad presencial (UREG) y no tenga asignaturas inscritas en el periodo requerido.   */
/*                                                                                            */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/ 

CREATE OR REPLACE FUNCTION F_RSS_SMCV (

    P_PIDM            SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_SRVC_CODE       SVRRSRV.SVRRSRV_SRVC_CODE%TYPE,
    P_SRVC_RULE       SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE

) RETURN VARCHAR2
 IS
    
    --P_CODIGO_SOL         SVRRSRV.SVRRSRV_SRVC_CODE%TYPE := 'SOL020';
    V_CODE_DEPT          STVDEPT.STVDEPT_CODE%TYPE;
    P_TERM_CODE          STVTERM.STVTERM_CODE%TYPE;
    V_TERM_CODE          STVTERM.STVTERM_CODE%TYPE;
    V_TERM_CODE_ADMIT    STVTERM.STVTERM_CODE%TYPE;
    V_INDICADOR          NUMBER;
    
    -- GET DEPARTMENTO ('UREG','UPGT','UVIR')
    CURSOR GET_DEPT IS
        SELECT    SORLFOS_DEPT_CODE  
        FROM (
              SELECT  SORLFOS_DEPT_CODE
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              WHERE   SORLCUR_PIDM        =   P_PIDM
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE =   'Y'
              ORDER BY SORLCUR_TERM_CODE DESC,SORLCUR_SEQNO DESC 
      ) WHERE ROWNUM = 1;
    
    -- GET PERIODO DE INGRESO
    CURSOR GET_TERM IS
        SELECT    SORLCUR_TERM_CODE_ADMIT  
        FROM (SELECT SORLCUR_TERM_CODE_ADMIT
              FROM SORLFOS
                    INNER JOIN SORLCUR
                    ON SORLCUR_PIDM = SORLFOS_PIDM
                    AND SORLCUR_TERM_CODE_CTLG = SORLFOS_TERM_CODE_CTLG
                    AND SORLCUR_CACT_CODE = SORLFOS_CACT_CODE
                    AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
                    WHERE SORLCUR_LMOD_CODE = 'LEARNER'
                    AND SORLCUR_CACT_CODE = 'ACTIVE'
                    AND SORLFOS_CSTS_CODE = 'INPROGRESS'
                    AND SORLCUR_CURRENT_CDE = 'Y'
                    --AND SORLFOS_TERM_CODE_CTLG = SORLCUR_TERM_CODE_ADMIT
                    AND SORLCUR_PIDM = P_PIDM
              ORDER BY SORLCUR_TERM_CODE_ADMIT DESC 
      ) WHERE ROWNUM = 1;
    
    -- GET PERIODO ACTIVO
    CURSOR C_SOBPTRM IS
      SELECT SOBPTRM_TERM_CODE FROM (
          -- Forma SOATERM - SFARSTS  ---> fechas para las partes de periodo
          SELECT DISTINCT SOBPTRM_TERM_CODE
          FROM SOBPTRM
          INNER JOIN SFRRSTS
            ON SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
            AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
          WHERE SFRRSTS_RSTS_CODE = 'RW'
          AND SUBSTR(SOBPTRM_PTRM_CODE,3,1) = 1 -- Solo parte de periodo '%1'          
          AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')  AND TO_DATE(TO_CHAR(SFRRSTS_END_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')
          ORDER BY SOBPTRM_TERM_CODE DESC
      ) WHERE ROWNUM <= 1;   

 BEGIN
 
    OPEN GET_DEPT;
    LOOP
        FETCH GET_DEPT INTO V_CODE_DEPT;
        EXIT WHEN GET_DEPT%NOTFOUND;
    END LOOP;
    CLOSE GET_DEPT;

    OPEN GET_TERM;
    LOOP
        FETCH GET_TERM INTO V_TERM_CODE_ADMIT;
        EXIT WHEN GET_TERM%NOTFOUND;
    END LOOP;
    CLOSE GET_TERM;

     -- >> Obteniendo PERIODO ACTIVO  --
    P_TERM_CODE := NULL;
    OPEN C_SOBPTRM;
      LOOP
        FETCH C_SOBPTRM INTO V_TERM_CODE;
        IF C_SOBPTRM%FOUND THEN
            P_TERM_CODE := V_TERM_CODE;
        ELSE EXIT;
      END IF;
      END LOOP;
    CLOSE C_SOBPTRM;
    
    -- VERIFICA SI TIENE REGISTRO DE INSCRIPCIÓN EN LA FORMA SFAREGS 
    SELECT COUNT(SFTREGS_PIDM) INTO V_INDICADOR FROM SFTREGS
    WHERE SFTREGS_TERM_CODE = P_TERM_CODE
          AND SFTREGS_PIDM = P_PIDM;

    IF SUBSTR(V_TERM_CODE_ADMIT,1,4) <> SUBSTR(P_TERM_CODE,1,4) AND V_CODE_DEPT = 'UREG' AND V_INDICADOR = 0 THEN
       RETURN 'Y';
    END IF;
       RETURN 'N';

    EXCEPTION
    WHEN OTHERS THEN
        RETURN 'N';
  
END F_RSS_SMCV;