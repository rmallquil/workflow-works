/**********************************************************************************************/
/* BWZKSMCV.sql                                                                               */
/**********************************************************************************************/
/*                                                                                            */
/* Descripción corta: Script para generar el Paquete de automatización del proceso de         */
/*                    Solicitud de Modificación de Cuota Inicial Verano                       */
/*                                                                                            */
/**********************************************************************************************/
/*                                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                     INI             FECHA       */
/* -------------------------------------------------------------- ---- ---------- ----------- */
/* 1. Creación del Codigo.                                        LAM             06/DIC/2017 */
/*    --------------------                                                                    */
/*      Creación del paquete de Modificación de Cuota Inicial - Verano                        */
/*    Procedure P_GET_STUDENT_INPUTS: Obtiene los datos de AutoServicio ingresados            */
/*              por el estudiante concepto del idioma y si cumple con el requisito.           */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud.                  */
/*    Procedure P_VERIFICAR_CTA_CTE: Verifica si tiene cta cte creada en el periodo académico */
/*              requerido                                                                     */
/*    Procedure P_SET_HRS_MAX: Actualiza las horas máximas(créditos) en el SFAREGS            */
/*    Procedure P_SET_RECALCULO_CUOTA: Actualizar el monto de la C01 en base a los créditos   */
/*                solicitados                                                                 */
/* 2. Actualizacion P_VERIFICAR_CTA_CTE                           LAM             17/DIC/2019 */
/*      Se actualiza el mensaje cuando no encuentra la cuenta corriente ("No tiene cuenta     */
/*      corriente creada.")                                                                   */
/* 3. Actualizacion Consulta                                      BYF             06/FEB/2019 */
/*      Se actualiza la consulta para obtener el campo PARTE PERIODO de todo el paquete.      */
/*                                                                                            */
/* -------------------------------------------------------------------------------------------*/
/* FIN DEL SEGUIMIENTO                                                                        */
/*                                                                                            */
/**********************************************************************************************/

/**********************************************************************************************/
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/

CREATE OR REPLACE PACKAGE BWZKSMCV AS

PROCEDURE P_GET_STUDENT_INPUTS (

      P_FOLIO_SOLICITUD      IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_CREDITOS_CODE        OUT SVRSVAD.SVRSVAD_ADDL_DATA_CDE%TYPE,
      P_CREDITOS_DESC        OUT SVRSVAD.SVRSVAD_ADDL_DATA_DESC%TYPE    
 );

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
      P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
      P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
      P_AN                  OUT NUMBER,
      P_ERROR               OUT VARCHAR2
 );
              
--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_CTA_CTE (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO       IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_TERM_CODE       IN STVTERM.STVTERM_CODE%TYPE,
    P_COD_SEDE        IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CTA_CTE         OUT VARCHAR2,
    P_DESCRIP_CTA_CTE OUT VARCHAR2,
    P_ERROR           OUT VARCHAR2
);

--------------------------------------------------------------------------------
PROCEDURE P_SET_HRS_MAX (

    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVTERM.STVTERM_CODE%TYPE,
    P_CREDITOS_CODE   IN SVRSVAD.SVRSVAD_ADDL_DATA_CDE%TYPE,
    P_MESSAGE         OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_SET_RECALCULO_CUOTA (

    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVTERM.STVTERM_CODE%TYPE,
    P_CREDITOS_CODE   IN SVRSVAD.SVRSVAD_ADDL_DATA_CDE%TYPE,
    P_MESSAGE         OUT VARCHAR2
);
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--     
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END BWZKSMCV;

/**********************************************************************************************/
--  /
--  show errors 
-- 
--  SET SCAN ON
--  WHENEVER SQLERROR CONTINUE
--  DROP PUBLIC SYNONYM BWZKSMCV;
--  WHENEVER SQLERROR EXIT ROLLBACK
--  CREATE PUBLIC SYNONYM BWZKSMCV FOR BANINST1.BWZKSMCV;
--  WHENEVER SQLERROR CONTINUE
--  START gurgrtb BWZKSMCV
--  START gurgrth BWZKSMCV
--  WHENEVER SQLERROR EXIT ROLLBACK
/**********************************************************************************************/