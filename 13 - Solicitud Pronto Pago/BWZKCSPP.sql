/* ---------------------------------------------------------------------------------------------
-- buscar objecto
select dbms_metadata.get_ddl('PACKAGE','BWZKCSPP') from dual
--------------------------------------------------------------------------------------------- */

create or replace PACKAGE BWZKCSPP AS
/*
 BWZKCSPP:
       Paquete Web _ Desarrollo Propio _ Paquete _ Conti Solicitud Pronto Pago
*/
-- FILE NAME..: BWZKCSPP.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKCSPP
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2017
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              


PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
              P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
              P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
              P_AN                  OUT NUMBER,
              P_ERROR               OUT VARCHAR2
      );

--------------------------------------------------------------------------------

PROCEDURE P_GET_DATOSROLE_DPP (
            P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, -- Case sensitive
            P_ROLE_CODE           IN WORKFLOW.ROLE.NAME%TYPE, -- Case sensitive
            P_ROLE_SEDE           OUT VARCHAR2,
            P_ROLE_EMAILS         OUT VARCHAR2,
            P_ERROR               OUT VARCHAR2
      );

--------------------------------------------------------------------------------

PROCEDURE P_GET_STUDENT_INPUTS (
            P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
            P_NCOUTAS             OUT VARCHAR2,
            P_NCOUTAS_DESC        OUT VARCHAR2
      );

--------------------------------------------------------------------------------


--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--    

END BWZKCSPP;