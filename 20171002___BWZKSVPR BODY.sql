/******************************************************************************/
/* BWZKSVPR BODY.sql                                                          */
/******************************************************************************/
/*                                                                            */
/* Descripción corta: Script para generar el Paquete de procedimientos usados */
/*  en los triggers de inicio de WorkFlow                                     */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                 INI    FECHA    */
/* ---------------------------------------------------------- --- ----------- */
/* 1. Creación del Código.                                    RML 24/NOV/2017 */
/* Se cambia el nombre del procedimiento WFK_OAFORM POR BWZKWFFN              */
/*  Procedure P_CONTISTI_GETDATA: Obtener los datos dept, camp, nombre camp y */
/*    periodo de un PIDM.                                                     */
/*  Procedure P_CONTISPGA_GETDATA: Obtener los datos dept, camp, nombre camp  */
/*    y periodo de un PIDM                                                    */
/*  Procedure P_CONTISPP_GETDATA: Obtener los datos dept, camp, nombre camp   */
/*    y periodo de un PIDM                                                    */
/*  Procedure P_CONTISPBT_GETDATA: Obtener los datos dept, camp, nombre camp  */
/*    y periodo de un PIDM                                                    */
/*  Procedure P_CONTISDPC_GETDATA: Obtener los datos dept, camp, nombre camp  */
/*    y periodo de un PIDM.                                                   */
/*  Procedure P_CONTISMCV_GETDATA: Obtener los datos dept, camp, nombre camp  */
/*    y periodo de un PIDM.                                                   */
/*  Procedure P_CONTISPPE_GETDATA: Obtener los datos dept, camp, nombre camp  */
/*    y periodo de un PIDM.                                                   */
/* -------------------------------------------------------------------------- */
/*                                                                            */
/* FIN DEL SEGUIMIENTO                                                        */
/*                                                                            */
/******************************************************************************/ 

/**********************************************************************/
-- PERMISOS DE EJECUCIÓN
/**********************************************************************/
-- CREATE OR REPLACE PUBLIC SYNONYM "BWZKSVPR" FOR "BANINST1"."BWZKSVPR";
-- GRANT EXECUTE ON BANINST1.BWZKSVPR TO SATURN;
--------------------------------------------------------------------------
--------------------------------------------------------------------------


CREATE OR REPLACE PACKAGE BODY BWZKSVPR AS
        
--## SOLICITUD DE TRASLADO INTERNO (CONTISTI) ## 
PROCEDURE P_CONTISTI_GETDATA( 
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
)
AS
      
      V_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_1          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_2          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo

      -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
      CURSOR C_CAMPDEPT IS
      SELECT    SORLCUR_CAMP_CODE,
                SORLFOS_DEPT_CODE,
                STVCAMP_DESC
      FROM (
              SELECT    SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, STVCAMP_DESC
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              INNER JOIN STVCAMP
                    ON SORLCUR_CAMP_CODE = STVCAMP_CODE
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
                  AND SORLCUR_TERM_CODE_END IS NULL
              ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM = 1;

      -- Calculando parte PERIODO (sub PTRM)
      CURSOR C_SFRRSTS_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;
      
      /******************************************************************************************************
        GET TERM (PERIODO - Solo usando parte de periodo 1) 
             A LA FECHA QUE ESTE DENTRO DE LA FECHA DE INICIO DE MATRICULA(SFRRSTS_START_DATE) y 
             y una semana despues de INICIO DE CLASES(SOBPTRM_START_DATE)
      */
      CURSOR C_STUDN_TERM IS
      SELECT SOBPTRM_TERM_CODE FROM (
          -- Forma SOATERM - SFARSTS  ---> fechas para las partes de periodo
          SELECT DISTINCT SOBPTRM_TERM_CODE 
          FROM SOBPTRM
          INNER JOIN SFRRSTS
            ON SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
            AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
          WHERE SFRRSTS_RSTS_CODE = 'RW'
          AND SOBPTRM_PTRM_CODE = V_SUB_PTRM_1 -- Solo parte de periodo '%1'
          AND SOBPTRM_PTRM_CODE NOT LIKE '%0'          
          AND TO_DATE(TO_CHAR(P_RECEPTION_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')  AND (SOBPTRM_START_DATE + 7)
          ORDER BY SOBPTRM_TERM_CODE DESC
      ) WHERE ROWNUM <= 1;
     
BEGIN

      P_DEPT_CODE  := '-';
      P_CAMP_CODE  := '-';
      P_CAMP_DESC  := '-';
      P_TERM_CODE  := '-';
      

      -- >> GET DEPARTAMENTO y CAMPUS --
      OPEN C_CAMPDEPT;
      LOOP
        FETCH C_CAMPDEPT INTO P_CAMP_CODE,P_DEPT_CODE,P_CAMP_DESC ;
        EXIT WHEN C_CAMPDEPT%NOTFOUND;
      END LOOP;
      CLOSE C_CAMPDEPT;

     
      -- >> calculando PARTE PERIODO  --
      OPEN C_SFRRSTS_PTRM;
      LOOP
        FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
        EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
      END LOOP;
      CLOSE C_SFRRSTS_PTRM;


      -- PARTE DE PERIODOS
      V_SUB_PTRM_1 := V_SUB_PTRM || '1';
      V_SUB_PTRM_2 := V_SUB_PTRM || CASE WHEN (P_DEPT_CODE ='UVIR' OR P_DEPT_CODE ='UPGT') THEN '2' ELSE '-' END;
 

     /******************************************************************************************************
        GET TERM (PERIODO - Solo usando parte de periodo 1) 
             A LA FECHA QUE ESTE DENTRO DE LA FECHA DE INICIO DE MATRICULA(SFRRSTS_START_DATE) y 
             y UNA SEMANA ANTES DE INICIO DE CLASES(SOBPTRM_START_DATE-7)
      */
      OPEN C_STUDN_TERM;
      LOOP
        FETCH C_STUDN_TERM INTO P_TERM_CODE;
        EXIT WHEN C_STUDN_TERM%NOTFOUND;
      END LOOP;
      CLOSE C_STUDN_TERM;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CONTISTI_GETDATA;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

--## SOLICITUD DE PRORROGA GASTO ADMINISTRATIVO (CONTISPGA) ## 
PROCEDURE P_CONTISPGA_GETDATA( 
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
)
AS
      
      V_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_1          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_2          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo

      -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
      CURSOR C_CAMPDEPT IS
      SELECT    SORLCUR_CAMP_CODE,
                SORLFOS_DEPT_CODE,
                STVCAMP_DESC
      FROM (
              SELECT    SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, STVCAMP_DESC
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              INNER JOIN STVCAMP
                    ON SORLCUR_CAMP_CODE = STVCAMP_CODE
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE =   'Y'
                  AND SORLCUR_TERM_CODE_END IS NULL
              ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM = 1;

      -- Calculando parte PERIODO (sub PTRM)
      CURSOR C_SFRRSTS_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;
      
      /******************************************************************************************************
        GET TERM (PERIODO - Solo usando parte de periodo 1) 
             Las prorrogas se realzan dentro del periodo academico(inia a fin de clases)
      */
      CURSOR C_STUDN_TERM IS
      SELECT SOBPTRM_TERM_CODE FROM (
          SELECT DISTINCT SOBPTRM_TERM_CODE 
          FROM SOBPTRM 
          WHERE SOBPTRM_PTRM_CODE = V_SUB_PTRM_1 -- Solo parte de periodo '%1'
          AND SOBPTRM_PTRM_CODE NOT LIKE '%0'          
          AND TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN SOBPTRM_START_DATE  AND SOBPTRM_END_DATE
          ORDER BY SOBPTRM_TERM_CODE DESC
      ) WHERE ROWNUM <= 1;
     
BEGIN

      P_DEPT_CODE  := '-';
      P_CAMP_CODE  := '-';
      P_CAMP_DESC  := '-';
      P_TERM_CODE  := '-';
      

      -- >> GET DEPARTAMENTO y CAMPUS --
      OPEN C_CAMPDEPT;
      LOOP
        FETCH C_CAMPDEPT INTO P_CAMP_CODE,P_DEPT_CODE,P_CAMP_DESC ;
        EXIT WHEN C_CAMPDEPT%NOTFOUND;
      END LOOP;
      CLOSE C_CAMPDEPT;

     
      -- >> calculando PARTE PERIODO  --
      OPEN C_SFRRSTS_PTRM;
      LOOP
        FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
        EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
      END LOOP;
      CLOSE C_SFRRSTS_PTRM;


      -- PARTE DE PERIODOS
      V_SUB_PTRM_1 := V_SUB_PTRM || '1';
      V_SUB_PTRM_2 := V_SUB_PTRM || CASE WHEN (P_DEPT_CODE ='UVIR' OR P_DEPT_CODE ='UPGT') THEN '2' ELSE '-' END;
 

     /******************************************************************************************************
        GET TERM (PERIODO - Solo usando parte de periodo 1) 
             Las prorrogas se realzan dentro del periodo academico(inicia a fin de clases)
      */
      OPEN C_STUDN_TERM;
      LOOP
        FETCH C_STUDN_TERM INTO P_TERM_CODE;
        EXIT WHEN C_STUDN_TERM%NOTFOUND;
      END LOOP;
      CLOSE C_STUDN_TERM;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CONTISPGA_GETDATA;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

--## SOLICITUD DE PRONTO PAGO (CONTISPP) ## 
PROCEDURE P_CONTISPP_GETDATA( 
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
)
AS
      
      V_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_1          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_2          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo

      -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
      CURSOR C_CAMPDEPT IS
      SELECT    SORLCUR_CAMP_CODE,
                SORLFOS_DEPT_CODE,
                STVCAMP_DESC
      FROM (
              SELECT    SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, STVCAMP_DESC
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              INNER JOIN STVCAMP
                    ON SORLCUR_CAMP_CODE = STVCAMP_CODE
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE =   'Y'
                  AND SORLCUR_TERM_CODE_END IS NULL
              ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM = 1;

      -- Calculando parte PERIODO (sub PTRM)
      CURSOR C_SFRRSTS_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;
      
       /******************************************************************************************************
        GET TERM (PERIODO - Solo usando parte de periodo 1) 
             A LA FECHA QUE ESTE DENTRO DE LA FECHA DE INICIO DE MATRICULA(SFRRSTS_START_DATE) y 
             y UNA SEMANA ANTES DE INICIO DE CLASES(SOBPTRM_START_DATE + 7)
      */
      CURSOR C_STUDN_TERM IS
      SELECT SOBPTRM_TERM_CODE FROM (
          -- Forma SOATERM - SFARSTS  ---> fechas para las partes de periodo
          SELECT DISTINCT SOBPTRM_TERM_CODE 
          FROM SOBPTRM
          INNER JOIN SFRRSTS
            ON SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
            AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
          WHERE SFRRSTS_RSTS_CODE = 'RW'
          AND SOBPTRM_PTRM_CODE = V_SUB_PTRM_1 -- Solo parte de periodo '%1'
          AND SOBPTRM_PTRM_CODE NOT LIKE '%0'          
          AND TO_DATE(TO_CHAR(P_RECEPTION_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')  AND (SOBPTRM_START_DATE + 7)
          ORDER BY SOBPTRM_TERM_CODE DESC
      ) WHERE ROWNUM <= 1;
     
BEGIN

      P_DEPT_CODE  := '-';
      P_CAMP_CODE  := '-';
      P_CAMP_DESC  := '-';
      P_TERM_CODE  := '-';
      

      -- >> GET DEPARTAMENTO y CAMPUS --
      OPEN C_CAMPDEPT;
      LOOP
        FETCH C_CAMPDEPT INTO P_CAMP_CODE,P_DEPT_CODE,P_CAMP_DESC ;
        EXIT WHEN C_CAMPDEPT%NOTFOUND;
      END LOOP;
      CLOSE C_CAMPDEPT;

     
      -- >> calculando PARTE PERIODO  --
      OPEN C_SFRRSTS_PTRM;
      LOOP
        FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
        EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
      END LOOP;
      CLOSE C_SFRRSTS_PTRM;


      -- PARTE DE PERIODOS
      V_SUB_PTRM_1 := V_SUB_PTRM || '1';
      V_SUB_PTRM_2 := V_SUB_PTRM || CASE WHEN (P_DEPT_CODE ='UVIR' OR P_DEPT_CODE ='UPGT') THEN '2' ELSE '-' END;
 

       /******************************************************************************************************
        GET TERM (PERIODO - Solo usando parte de periodo 1) 
             A LA FECHA QUE ESTE DENTRO DE LA FECHA DE INICIO DE MATRICULA(SFRRSTS_START_DATE) y 
             y UNA SEMANA ANTES DE INICIO DE CLASES(SOBPTRM_START_DATE + 7)
      */
      OPEN C_STUDN_TERM;
      LOOP
        FETCH C_STUDN_TERM INTO P_TERM_CODE;
        EXIT WHEN C_STUDN_TERM%NOTFOUND;
      END LOOP;
      CLOSE C_STUDN_TERM;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CONTISPP_GETDATA;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


--## SOLICITUD DE PAGO TRAMITE BACHILLER y TITULACION (CONTISPBT) ## 
PROCEDURE P_CONTISPBT_GETDATA( 
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
)
AS
      
      V_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_1          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_2          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo

      -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
      CURSOR C_CAMPDEPT IS
      SELECT    SORLCUR_CAMP_CODE,
                SORLFOS_DEPT_CODE,
                STVCAMP_DESC
      FROM (
              SELECT    SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, STVCAMP_DESC
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              INNER JOIN STVCAMP
                    ON SORLCUR_CAMP_CODE = STVCAMP_CODE
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
                  AND SORLCUR_TERM_CODE_END IS NULL
              ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM = 1;

BEGIN

      P_DEPT_CODE  := '-';
      P_CAMP_CODE  := '-';
      P_CAMP_DESC  := '-';
      P_TERM_CODE  := '-';
      

      -- >> GET DEPARTAMENTO y CAMPUS --
      OPEN C_CAMPDEPT;
      LOOP
        FETCH C_CAMPDEPT INTO P_CAMP_CODE,P_DEPT_CODE,P_CAMP_DESC ;
        EXIT WHEN C_CAMPDEPT%NOTFOUND;
      END LOOP;
      CLOSE C_CAMPDEPT;

     --> Get PERIODO, El periodo es anual para el pago del tramite de bachiller y titulacion
      P_TERM_CODE := TO_CHAR(SYSDATE, 'YYYY') || '10';

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CONTISPBT_GETDATA;


--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--         

--## SOLICITUD DE DESCUENTO POR CONVENIO (CONTISDPC) ## 
PROCEDURE P_CONTISDPC_GETDATA( 
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
)
AS
      
      V_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_1          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_2          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_APEC_DEPT           VARCHAR2(10);
      V_ING_TERM            VARCHAR2(10);
      V_CONT_ING            NUMBER;

      -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
      CURSOR C_CAMPDEPT IS
      SELECT    SORLCUR_CAMP_CODE,
                SORLFOS_DEPT_CODE,
                STVCAMP_DESC
      FROM (
              SELECT    SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, STVCAMP_DESC
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              INNER JOIN STVCAMP
                    ON SORLCUR_CAMP_CODE = STVCAMP_CODE
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
                  AND SORLCUR_TERM_CODE_END IS NULL
              ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM = 1;

      -- Calculando parte PERIODO (sub PTRM)
      CURSOR C_SFRRSTS_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;

      /******************************************************************************************************
        GET TERM (PERIODO - Solo usando parte de periodo 1) 
      */
      CURSOR C_STUDN_TERM IS
      SELECT SOBPTRM_TERM_CODE FROM (
          SELECT DISTINCT SOBPTRM_TERM_CODE 
          FROM SOBPTRM
          INNER JOIN SFRRSTS
            ON SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
            AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
          WHERE SFRRSTS_RSTS_CODE = 'RW'     
          AND SOBPTRM_PTRM_CODE = V_SUB_PTRM_1 -- Solo parte de periodo '%1'
          AND SOBPTRM_PTRM_CODE NOT LIKE '%0'          
          AND TO_DATE(TO_CHAR(P_RECEPTION_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN (TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') - 7)  AND (TO_DATE(TO_CHAR(SOBPTRM_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') + 15)
          ORDER BY SOBPTRM_TERM_CODE DESC
      ) WHERE ROWNUM <= 1;

BEGIN

      P_DEPT_CODE  := '-';
      P_CAMP_CODE  := '-';
      P_CAMP_DESC  := '-';
      P_TERM_CODE  := '-';

      -- >> GET DEPARTAMENTO y CAMPUS --
      OPEN C_CAMPDEPT;
      LOOP
        FETCH C_CAMPDEPT INTO P_CAMP_CODE,P_DEPT_CODE,P_CAMP_DESC ;
        EXIT WHEN C_CAMPDEPT%NOTFOUND;
      END LOOP;
      CLOSE C_CAMPDEPT;

      -- >> calculando PARTE PERIODO  --
      OPEN C_SFRRSTS_PTRM;
      LOOP
        FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
        EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
      END LOOP;
      CLOSE C_SFRRSTS_PTRM;


      -- PARTE DE PERIODOS
      V_SUB_PTRM_1 := V_SUB_PTRM || '1';
      V_SUB_PTRM_2 := V_SUB_PTRM || CASE WHEN (P_DEPT_CODE ='UVIR' OR P_DEPT_CODE ='UPGT') THEN '2' ELSE '-' END;
 
     /******************************************************************************************************
        GET TERM (PERIODO - Solo usando parte de periodo 1) 
             Las prorrogas se realzan dentro del periodo academico(inicia a fin de clases)
      */
        SELECT CZRDEPT_DEPT_BDUCCI
        INTO V_APEC_DEPT 
        FROM CZRDEPT 
        WHERE CZRDEPT_CODE = P_DEPT_CODE;-- MODALIDAD 
        
        ----OBTENEMOS EL ULTIMO PERIODO ACTIVO DE INGRESANTES
        SELECT
        MAX(TRIM("IDPerAcad")) INTO V_ING_TERM
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
        AND "Ingresante" = '1'
        AND "Renuncia" = '0'
        AND "IDEscuelaADM" in ('ADM','ADG','ADV')
        AND "IDExamen" < TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy');
    
        ----VERIFICAR SI ES INGRESANTE O NO
        SELECT COUNT("IDAlumno") INTO V_CONT_ING
        FROM tblPostulante@BDUCCI.CONTINENTAL.EDU.PE
        WHERE "IDDependencia" = 'UCCI'
        AND "Ingresante" = '1'
        AND "Renuncia" = '0'
        AND "IDEscuelaADM" = V_APEC_DEPT
        AND "IDPerAcad" = V_ING_TERM
        AND "IDAlumno" = (SELECT SPRIDEN_ID FROM SPRIDEN WHERE SPRIDEN_PIDM = P_PIDM);
        
        IF V_CONT_ING > 0 THEN
            SELECT CZRTERM_CODE
            INTO P_TERM_CODE 
            FROM CZRTERM
            WHERE CZRTERM_TERM_BDUCCI = V_ING_TERM;-- MODALIDAD 
        ELSE
            OPEN C_STUDN_TERM;
            LOOP
            FETCH C_STUDN_TERM INTO P_TERM_CODE;
            EXIT WHEN C_STUDN_TERM%NOTFOUND;
            END LOOP;
            CLOSE C_STUDN_TERM;
        END IF;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CONTISDPC_GETDATA;

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              

--## SOLICITUD DE MODIFICACI�N DE CUOTA INICIAL DE VERANO (CONTISMCV) ##

PROCEDURE P_CONTISMCV_GETDATA( 
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
)
AS
      
      V_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_1          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_2          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo

      -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
      CURSOR C_CAMPDEPT IS
      SELECT    SORLCUR_CAMP_CODE,
                SORLFOS_DEPT_CODE,
                STVCAMP_DESC
      FROM (
              SELECT    SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, STVCAMP_DESC
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              INNER JOIN STVCAMP
                    ON SORLCUR_CAMP_CODE = STVCAMP_CODE
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
              ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM = 1;

      -- Calculando parte PERIODO (sub PTRM)
      CURSOR C_SFRRSTS_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;
      
      /******************************************************************************************************
        GET TERM (PERIODO - Solo usando parte de periodo 1) 
             A LA FECHA QUE ESTE DENTRO DE LA FECHA DE INICIO DE MATRICULA(SFRRSTS_START_DATE) y 
             y una semana despues de INICIO DE CLASES(SOBPTRM_START_DATE)
      */
      CURSOR C_STUDN_TERM IS
      SELECT SOBPTRM_TERM_CODE FROM (
          -- Forma SOATERM - SFARSTS  ---> fechas para las partes de periodo
          SELECT DISTINCT SOBPTRM_TERM_CODE 
          FROM SOBPTRM
          INNER JOIN SFRRSTS
            ON SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
            AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
          WHERE SFRRSTS_RSTS_CODE = 'RW'
          AND SOBPTRM_PTRM_CODE = V_SUB_PTRM_1 -- Solo parte de periodo '%1'
          --AND SOBPTRM_PTRM_CODE NOT LIKE '%0'          
          AND TO_DATE(TO_CHAR(P_RECEPTION_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')  AND (TO_DATE(TO_CHAR(SOBPTRM_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')+15)
          ORDER BY SOBPTRM_TERM_CODE DESC
      ) WHERE ROWNUM <= 1;
     
BEGIN

      P_DEPT_CODE  := '-';
      P_CAMP_CODE  := '-';
      P_CAMP_DESC  := '-';
      P_TERM_CODE  := '-';
      

      -- >> GET DEPARTAMENTO y CAMPUS --
      OPEN C_CAMPDEPT;
      LOOP
        FETCH C_CAMPDEPT INTO P_CAMP_CODE,P_DEPT_CODE,P_CAMP_DESC ;
        EXIT WHEN C_CAMPDEPT%NOTFOUND;
      END LOOP;
      CLOSE C_CAMPDEPT;

     
      -- >> calculando PARTE PERIODO  --
      OPEN C_SFRRSTS_PTRM;
      LOOP
        FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
        EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
      END LOOP;
      CLOSE C_SFRRSTS_PTRM;


      -- PARTE DE PERIODOS
      V_SUB_PTRM_1 := V_SUB_PTRM || '1';
      V_SUB_PTRM_2 := V_SUB_PTRM || CASE WHEN (P_DEPT_CODE ='UVIR' OR P_DEPT_CODE ='UPGT') THEN '2' ELSE '-' END;
 

     /******************************************************************************************************
        GET TERM (PERIODO - Solo usando parte de periodo 1) 
             A LA FECHA QUE ESTE DENTRO DE LA FECHA DE INICIO DE MATRICULA(SFRRSTS_START_DATE) y 
             y UNA SEMANA ANTES DE INICIO DE CLASES(SOBPTRM_START_DATE-7)
      */
      OPEN C_STUDN_TERM;
      LOOP
        FETCH C_STUDN_TERM INTO P_TERM_CODE;
        EXIT WHEN C_STUDN_TERM%NOTFOUND;
      END LOOP;
      CLOSE C_STUDN_TERM;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CONTISMCV_GETDATA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CONTISPPE_GETDATA (
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
)
AS
      
      V_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_1          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_2          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo

      -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
      CURSOR C_CAMPDEPT IS
      SELECT    SORLCUR_CAMP_CODE,
                SORLFOS_DEPT_CODE,
                STVCAMP_DESC
      FROM (
              SELECT    SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, STVCAMP_DESC
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              INNER JOIN STVCAMP
                    ON SORLCUR_CAMP_CODE = STVCAMP_CODE
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
                  AND SORLCUR_TERM_CODE_END IS NULL
              ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM = 1;

      -- Calculando parte PERIODO (sub PTRM)
      CURSOR C_SFRRSTS_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;
      
      /******************************************************************************************************
        GET TERM (PERIODO - Solo usando parte de periodo 1) 
             A LA FECHA QUE ESTE DENTRO DE LA FECHA DE INICIO DE MATRICULA(SFRRSTS_START_DATE) y 
             y una semana despues de INICIO DE CLASES(SOBPTRM_START_DATE)
      */
      CURSOR C_STUDN_TERM IS
      SELECT SOBPTRM_TERM_CODE FROM (
          -- Forma SOATERM - SFARSTS  ---> fechas para las partes de periodo
          SELECT DISTINCT SOBPTRM_TERM_CODE 
          FROM SOBPTRM
          INNER JOIN SFRRSTS
            ON SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
            AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
          WHERE SFRRSTS_RSTS_CODE = 'RW'
          AND SOBPTRM_PTRM_CODE = V_SUB_PTRM_1 -- Solo parte de periodo '%1'
          --AND SOBPTRM_PTRM_CODE NOT LIKE '%0'          
          AND TO_DATE(TO_CHAR(P_RECEPTION_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')  AND (TO_DATE(TO_CHAR(SFRRSTS_END_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') + 7)
          ORDER BY SOBPTRM_TERM_CODE DESC
      ) WHERE ROWNUM <= 1;
     
BEGIN

      P_DEPT_CODE  := '-';
      P_CAMP_CODE  := '-';
      P_CAMP_DESC  := '-';
      P_TERM_CODE  := '-';
      

      -- >> GET DEPARTAMENTO y CAMPUS --
      OPEN C_CAMPDEPT;
      LOOP
        FETCH C_CAMPDEPT INTO P_CAMP_CODE,P_DEPT_CODE,P_CAMP_DESC ;
        EXIT WHEN C_CAMPDEPT%NOTFOUND;
      END LOOP;
      CLOSE C_CAMPDEPT;

     
      -- >> calculando PARTE PERIODO  --
      OPEN C_SFRRSTS_PTRM;
      LOOP
        FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
        EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
      END LOOP;
      CLOSE C_SFRRSTS_PTRM;


      -- PARTE DE PERIODOS
      V_SUB_PTRM_1 := V_SUB_PTRM || '1';
      V_SUB_PTRM_2 := V_SUB_PTRM || CASE WHEN (P_DEPT_CODE ='UVIR' OR P_DEPT_CODE ='UPGT') THEN '2' ELSE '-' END;
 

     /******************************************************************************************************
        GET TERM (PERIODO - Solo usando parte de periodo 1) 
             A LA FECHA QUE ESTE DENTRO DE LA FECHA DE INICIO DE MATRICULA(SFRRSTS_START_DATE) y 
             y UNA SEMANA DESPU�S DEL (SOBPTRM_START_DATE-7)
      */
      OPEN C_STUDN_TERM;
      LOOP
        FETCH C_STUDN_TERM INTO P_TERM_CODE;
        EXIT WHEN C_STUDN_TERM%NOTFOUND;
      END LOOP;
      CLOSE C_STUDN_TERM;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CONTISPPE_GETDATA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--


PROCEDURE P_CONTISMAD_GETDATA (
            P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
            P_RECEPTION_DATE      IN SVRSVPR.SVRSVPR_RECEPTION_DATE%TYPE,
            P_DEPT_CODE           OUT STVDEPT.STVDEPT_CODE%TYPE,
            P_CAMP_CODE           OUT STVCAMP.STVCAMP_CODE%TYPE,
            P_CAMP_DESC           OUT STVCAMP.STVCAMP_DESC%TYPE,
            P_TERM_CODE           OUT STVTERM.STVTERM_CODE%TYPE
)
AS
      
      V_SUB_PTRM            VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_1          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo
      V_SUB_PTRM_2          VARCHAR2(5) := NULL; --------------- Parte-de-Periodo

      -- CURSOR GET CAMP, CAMP DESC, DEPARTAMENTO
      CURSOR C_CAMPDEPT IS
      SELECT    SORLCUR_CAMP_CODE,
                SORLFOS_DEPT_CODE,
                STVCAMP_DESC
      FROM (
              SELECT    SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, STVCAMP_DESC
              FROM SORLCUR        INNER JOIN SORLFOS
                    ON    SORLCUR_PIDM  =   SORLFOS_PIDM 
                    AND   SORLCUR_SEQNO =   SORLFOS_LCUR_SEQNO
              INNER JOIN STVCAMP
                    ON SORLCUR_CAMP_CODE = STVCAMP_CODE
              WHERE   SORLCUR_PIDM        =   P_PIDM 
                  AND SORLCUR_LMOD_CODE   =   'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE   =   'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
                  AND SORLCUR_TERM_CODE_END IS NULL
              ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
      ) WHERE ROWNUM = 1;

      -- Calculando parte PERIODO (sub PTRM)
      CURSOR C_SFRRSTS_PTRM IS
      SELECT CASE STVDEPT_CODE  WHEN 'UVIR' THEN 'V' 
                                WHEN 'UPGT' THEN 'W' 
                                WHEN 'UREG' THEN 'R' 
                                WHEN 'UPOS' THEN '-' 
                                WHEN 'ITEC' THEN '-' 
                                WHEN 'UCIC' THEN '-' 
                                WHEN 'UCEC' THEN '-' 
                                WHEN 'ICEC' THEN '-' 
                                ELSE '1' END ||
              CASE STVCAMP_CODE WHEN 'S01' THEN 'H' 
                                WHEN 'F01' THEN 'A' 
                                WHEN 'F02' THEN 'L' 
                                WHEN 'F03' THEN 'C' 
                                WHEN 'V00' THEN 'V' 
                                ELSE '9' END SUBPTRM
      FROM STVCAMP,STVDEPT 
      WHERE STVDEPT_CODE = P_DEPT_CODE AND STVCAMP_CODE = P_CAMP_CODE;
      
      /******************************************************************************************************
        GET TERM (PERIODO - Solo usando parte de periodo 1) 
             A LA FECHA QUE ESTE DENTRO DE LA FECHA DE INICIO DE MATRICULA(SFRRSTS_START_DATE) y 
             y una semana despues de INICIO DE CLASES(SOBPTRM_START_DATE)
      */
      CURSOR C_STUDN_TERM IS
      SELECT SOBPTRM_TERM_CODE FROM (
          -- Forma SOATERM - SFARSTS  ---> fechas para las partes de periodo
          SELECT DISTINCT SOBPTRM_TERM_CODE 
          FROM SOBPTRM
          INNER JOIN SFRRSTS
            ON SOBPTRM_TERM_CODE = SFRRSTS_TERM_CODE
            AND SOBPTRM_PTRM_CODE = SFRRSTS_PTRM_CODE
          WHERE SFRRSTS_RSTS_CODE = 'RW'
          AND SOBPTRM_PTRM_CODE = V_SUB_PTRM_1 -- Solo parte de periodo '%1'
          --AND SOBPTRM_PTRM_CODE NOT LIKE '%0'          
          AND TO_DATE(TO_CHAR(P_RECEPTION_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE(TO_CHAR(SFRRSTS_START_DATE,'dd/mm/yyyy'),'dd/mm/yyyy')  AND (TO_DATE(TO_CHAR(SFRRSTS_END_DATE,'dd/mm/yyyy'),'dd/mm/yyyy') + 7)
          ORDER BY SOBPTRM_TERM_CODE DESC
      ) WHERE ROWNUM <= 1;
     
BEGIN

      P_DEPT_CODE  := '-';
      P_CAMP_CODE  := '-';
      P_CAMP_DESC  := '-';
      P_TERM_CODE  := '-';
      

      -- >> GET DEPARTAMENTO y CAMPUS --
      OPEN C_CAMPDEPT;
      LOOP
        FETCH C_CAMPDEPT INTO P_CAMP_CODE,P_DEPT_CODE,P_CAMP_DESC ;
        EXIT WHEN C_CAMPDEPT%NOTFOUND;
      END LOOP;
      CLOSE C_CAMPDEPT;

     
      -- >> calculando PARTE PERIODO  --
      OPEN C_SFRRSTS_PTRM;
      LOOP
        FETCH C_SFRRSTS_PTRM INTO V_SUB_PTRM;
        EXIT WHEN C_SFRRSTS_PTRM%NOTFOUND;
      END LOOP;
      CLOSE C_SFRRSTS_PTRM;


      -- PARTE DE PERIODOS
      V_SUB_PTRM_1 := V_SUB_PTRM || '1';
      V_SUB_PTRM_2 := V_SUB_PTRM || CASE WHEN (P_DEPT_CODE ='UVIR' OR P_DEPT_CODE ='UPGT') THEN '2' ELSE '-' END;
 

     /******************************************************************************************************
        GET TERM (PERIODO - Solo usando parte de periodo 1) 
             A LA FECHA QUE ESTE DENTRO DE LA FECHA DE INICIO DE MATRICULA(SFRRSTS_START_DATE) y 
             y UNA SEMANA DESPU�S DEL (SOBPTRM_START_DATE-7)
      */
      OPEN C_STUDN_TERM;
      LOOP
        FETCH C_STUDN_TERM INTO P_TERM_CODE;
        EXIT WHEN C_STUDN_TERM%NOTFOUND;
      END LOOP;
      CLOSE C_STUDN_TERM;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CONTISMAD_GETDATA;
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END BWZKSVPR;