/******************************************************************************/
/* BWZKPSPG.sql                                                               */
/******************************************************************************/
/*                                                                            */
/* Descripción corta: Script para el Paquete de procedimientos genericos      */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                 INI    FECHA    */
/* ---------------------------------------------------------- --- ----------- */
/* 1. Creación del código.                                    BFV 25/SEP/2018 */
/*    Creación del paquete de procedimientos genéricos.                       */
/*                                                                            */
/*    Procedure P_VALID_ID: Verifica si el DNI es correcto y existe.          */
/*    Procedure P_GET_INFO_STUDENT: Obtiene información base del estudiante   */
/*      para casos de WF On Demand                                            */
/*    Procedure P_VERIFICA_FECHA_DISPO: Verifica si las fechas para solicitud */
/*      permite que la solicitud este disponible.                             */
/*    Procedure P_VERIFICAR_MATRICULA: Verifica que el estudiante presente    */
/*      matricula en el periodo solicitado.                                   */
/*    Procedure P_MODIFICAR_RETENCION_A: Actualiza el registro a 3 retenciones*/
/*    Procedure P_MODIFICAR_RETENCION_B: Actualiza el registro a 4 retenciones*/
/*    Procedure P_VERIFICAR_CTA_CTE: Verifica que tenga cuenta corriente      */
/*    Procedure P_SET_RECALCULO_CUOTA: Recalculo de las cuotas segun pedido   */
/*    Procedure P_EXECCAPP: Ejecutar CAPP desde sistema                       */
/*    Procedure P_EXECPROY: Ejecutar Proyección desde sistema                 */
/*    Procedure P_MODIFICAR_CATALOGO: Modifica el periodo catalogo            */
/*    Procedure P_VERIFICAR_ESTADO_SOLICITUD: Verifica el estado de la        */
/*      solicitud para que continue el proceso.                               */
/*    Procedure P_CAMBIO_ESTADO_SOLICITUD: Cambia el estado de la solicitud   */
/*      para que continue el proceso y no pueda anularse la solicitud.        */
/*    Procedure P_SET_CURSO_INTROD: Registrar cursos introductorios           */
/*    Proceudre P_ELIMINAR_NRC: Elimina la matricula del estudiante.          */
/*    Procedure P_CONV_ASIGN: Realiza el proceso de convalidacion             */
/*    Procedure P_SET_ATRIBUTO_PLAN: Registra el atributo de plan             */
/*    Procedure P_GET_ASIG_RECONOCIDAS: Reporte de asignaturas reconocidas.   */
/*    Procedure P_VALIDAR_CONVA: Valida si el estudiante tiene registro de    */
/*      convalidaciones o no.                                                 */
/* 2. Actualizacion P_VERIFICAR_MATRICULA                     BFV 06/NOV/2018 */
/*    Se actualiza la consulta para que considere una revisión mas amplia y   */
/*      no limitada a una asignatura en especifico.                           */
/*    Agregado P_VALIDAR_CONVA                                                */
/*    Se aumenta la consulta para validar si el estudiante tiene convalidacion*/
/*      en ese periodo.                                                       */
/*    Agregado P_VERIFICAR_PAGOS_CTA_DUP                                      */
/*    Valida si el estudiante tiene mas de una cuenta corriente, elimina o    */
/*      notifica de la anulación de otra y solo deja activa la cuenta actual. */
/*    Agregado P_REGISTRAR_CAMBIO_ADM                                         */
/*    Registra en la base de datos el cambio realizado por el usuario en su   */
/*      postulacion.                                                          */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*                                                                            */
/* FIN DEL SEGUIMIENTO                                                        */
/*                                                                            */
/******************************************************************************/ 
/******************************************************************************/
-- PERMISOS DE EJECUCIÓN
/******************************************************************************/
--  SET SCAN ON 
-- 
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE 
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/******************************************************************************/

CREATE OR REPLACE PACKAGE BANINST1.BWZKPSPG AS
/*
 BWZKPSPG:
       Paquete Web _ Desarrollo Propio _ Paquete _ Paquete Generico
*/
-- FILE NAME..: BWZKPSPG.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKPSPG
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2017
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              

PROCEDURE P_VALID_ID (
    P_ID            IN  SPRIDEN.SPRIDEN_ID%TYPE,
    P_DNI_VALID     OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_INFO_STUDENT (
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_PIDM              OUT SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         OUT SOBPTRM.SOBPTRM_TERM_CODE%TYPE, 
    P_PART_PERIODO      OUT SOBPTRM.SOBPTRM_PTRM_CODE%TYPE, 
    P_DEPT_CODE         OUT STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         OUT STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_DESC         OUT STVCAMP.STVCAMP_DESC%TYPE,
    P_PROGRAM           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_EMAIL             OUT GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE,
    P_NAME              OUT VARCHAR2,
    P_FECHA_SOL         OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICA_FECHA_DISPO (
    P_TERM_CODE      IN SOBPTRM.SOBPTRM_TERM_CODE%TYPE,
    P_FECHA_SOL      IN VARCHAR2,
    P_PART_PERIODO   IN SOBPTRM.SOBPTRM_PTRM_CODE%TYPE,
    P_FECHA_VALIDA   OUT VARCHAR2,
    P_MESSAGE        OUT VARCHAR2 
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_MATRICULA (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_STATUS_MAT      OUT VARCHAR2,
    P_MESSAGE         OUT VARCHAR2

);

--------------------------------------------------------------------------------

PROCEDURE P_MODIFICAR_RETENCION_A (
    P_PIDM                IN  SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_STVHLDD_CODE1       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE2       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE3       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_MESSAGE             OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_MODIFICAR_RETENCION_B (
    P_PIDM                IN  SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_STVHLDD_CODE1       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE2       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE3       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_STVHLDD_CODE4       IN  STVHLDD.STVHLDD_CODE%TYPE,
    P_MESSAGE             OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_CTA_CTE (
    P_PIDM			    IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID_ALUMNO			IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE			IN STVDEPT.STVDEPT_CODE%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_COD_SEDE          IN STVCAMP.STVCAMP_CODE%TYPE,
    P_CTA_CTE			OUT VARCHAR2,
    P_DESCRIP_CTA_CTE	OUT VARCHAR2,
    P_MESSAGE   		OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_SET_RECALCULO_CUOTA (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVTERM.STVTERM_CODE%TYPE,
    P_CREDITOS_CODE   IN SVRSVAD.SVRSVAD_ADDL_DATA_CDE%TYPE,
    P_MESSAGE         OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_EXECCAPP (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_MESSAGE           OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_EXECPROY (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_MESSAGE           OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_MODIFICAR_CATALOGO (
    P_PIDM_ALUMNO      IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_PERIODO          IN SFBETRM.SFBETRM_TERM_CODE%TYPE,
    P_MESSAGE          OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_ESTADO_SOLICITUD (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_STATUS_SOL          OUT VARCHAR2,
    P_MESSAGE             OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
    P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
    P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
    P_AN                  OUT NUMBER,
    P_MESSAGE             OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_SET_CURSO_INTROD (
    P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE           IN STVTERM.STVTERM_CODE%TYPE,
    P_FECHA_SOL           IN VARCHAR2,
    P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
    P_MESSAGE             OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_ELIMINAR_NRC (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_MESSAGE         OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_CONV_ASIGN(
    P_TERM_CODE   IN STVTERM.STVTERM_CODE%TYPE,
    P_CATALOGO    IN STVTERM.STVTERM_CODE%TYPE,
    P_PIDM        IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_MESSAGE     OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_SET_ATRIBUTO_PLAN (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_ATTS_CODE         IN STVATTS.STVATTS_CODE%TYPE,-------- COD atributos
    P_MESSAGE           OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_ASIG_RECONOCIDAS (
    P_PIDM          IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_HTML1         OUT VARCHAR2,
    P_HTML2         OUT VARCHAR2,
    P_HTML3         OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VALIDAR_CONVA (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
    P_STATUS_CONVA      OUT VARCHAR2,
    P_MESSAGE           OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_PAGOS_CTA_DUP (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE			IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_PROGRAM           IN SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_NEW_PLAN          IN VARCHAR2,
    P_CAMBIO_ADM        IN VARCHAR2,
    P_STATUS_CTA        OUT VARCHAR2,
    P_CTA_DESC			OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_REGISTRAR_CAMBIO_ADM (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_CAMBIO_ADM        IN VARCHAR2,
    P_DEPT_CODE         IN VARCHAR2,
    P_DEPT_CODE_NEW     IN VARCHAR2,
    P_CAMP_CODE         IN VARCHAR2,
    P_CAMP_CODE_NEW     IN VARCHAR2,
    P_MOD_ADM           IN VARCHAR2,
    P_MOD_ADM_NEW       IN VARCHAR2,
    P_PROGRAM           IN VARCHAR2,
    P_PROGRAM_NEW       IN VARCHAR2,
    P_MESSAGE			OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_INFO_INGRESANTE (
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_PIDM              OUT SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         OUT SOBPTRM.SOBPTRM_TERM_CODE%TYPE, 
    P_DEPT_CODE         OUT STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         OUT STVCAMP.STVCAMP_CODE%TYPE,
    P_CAMP_DESC         OUT STVCAMP.STVCAMP_DESC%TYPE,
    P_PROGRAM           OUT SOBCURR.SOBCURR_PROGRAM%TYPE,
    P_PROGRAM_DESC      OUT SMRPRLE.SMRPRLE_PROGRAM_DESC%TYPE,
    P_MOD_ADM           OUT VARCHAR2,
    P_MOD_ADM_DESC      OUT VARCHAR2,
    P_CATALOGO          OUT VARCHAR2,
    P_EMAIL             OUT GOREMAL.GOREMAL_EMAIL_ADDRESS%TYPE,
    P_NAME              OUT VARCHAR2,
    P_FECHA_SOL         OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_BENEFICIO_PREVIO (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_ID                IN SPRIDEN.SPRIDEN_ID%TYPE,
    P_DEPT_CODE			IN STVDEPT.STVDEPT_CODE%TYPE,
    P_CAMP_CODE         IN STVCAMP.STVCAMP_CODE%TYPE,
    P_TERM_CODE	        IN STVTERM.STVTERM_CODE%TYPE,
    P_CAMBIO_ADM        IN VARCHAR2,
    P_STATUS_BEN        OUT VARCHAR2,
    P_MESSAGE			OUT VARCHAR2
);

END BWZKPSPG;

/******************************************************************************/
--  /
--  show errors 
-- 
--  SET SCAN ON
--  WHENEVER SQLERROR CONTINUE
--  DROP PUBLIC SYNONYM BWZKPSPG;
--  WHENEVER SQLERROR EXIT ROLLBACK
--  CREATE PUBLIC SYNONYM BWZKPSPG FOR BANINST1.BWZKPSPG;
--  WHENEVER SQLERROR CONTINUE
--  START gurgrtb BWZKPSPG
--  START gurgrth BWZKPSPG
--  WHENEVER SQLERROR EXIT ROLLBACK
/******************************************************************************/