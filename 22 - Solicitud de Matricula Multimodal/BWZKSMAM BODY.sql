/*********************************************************************************/
/* BWZKSMAM BODY.sql                                                             */
/*********************************************************************************/
/*                                                                               */
/* Descripción corta: Script para generar el Paquete relacionado a los           */
/*                    procedimientos de Matricula Multimodal                     */
/*                                                                               */
/*****************************************************************************   */
/*                                                                               */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                 INI    FECHA       */
/* ---------------------------------------------------------- --- -----------    */
/* 1. Creación del código.                                 BFV/LAM 03/JUL/2018   */
/*    Creación del paquete de Matricula Multimodal                               */
/*                                                                               */
/*    Procedure P_GET_STUDENT_INPUTS: Obtiene la informacion del DNI registrado. */
/*    Procedure P_VERIFICAR_APTO: Valida si el estudiante cumple las             */
/*      condiciones necesarias para acceder al flujo.                            */
/*    Procedure P_VERIFICA_FECHA_DISPO: Confirma si la solicitud esta dentro     */
/*      de las fechas programadas.                                               */
/*    Procedure P_VERIFICAR_NRC_MULTI: Verifica si el NRC creado tiene las       */
/*      caracteristicas basicas de un NRC multimodal.                            */
/*    Procedure P_INSERT_SOBREPASOS: Inserta los sobrepasos para la matricula    */
/*      multimodal.                                                              */
/*    Procedure P_INSERT_NRC_MULTI: Registra el NRC multimodal al estudiante     */
/*                                                                               */
/* --------------------------------------------------------------------------    */
/*                                                                               */
/* FIN DEL SEGUIMIENTO                                                           */
/*                                                                               */
/*********************************************************************************/
-- Creación BANINST1 Y PERMISOS
/**********************************************************************/
--     CREATE OR REPLACE PUBLIC SYNONYM "BWZKSMAM" FOR "BANINST1"."BWZKSMAM";
--     GRANT EXECUTE ON BANINST1.BWZKSMAM TO SATURN;
--------------------------------------------------------------------------
--------------------------------------------------------------------------

CREATE OR REPLACE PACKAGE BODY BANINST1.BWZKSMAM AS
/*
  BWZKSMAM:
       Paquete Web _ Desarrollo Propio _ Paquete _ Matricula Multimodal
*/
-- FILE NAME..: BWZKSMAM.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKSMAM
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2017
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_STUDENT_INPUTS(
        P_PIDM                 IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE            IN STVDEPT.STVDEPT_CODE%TYPE,
        P_FOLIO_SOLICITUD      IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_ATTRB_PLAN           OUT SGRSATT.SGRSATT_ATTS_CODE%TYPE,
        P_ASIGNATURA_CODE      OUT VARCHAR2,
        P_ASIGNATURA_DESC      OUT VARCHAR2,
        P_PLAN_ASIGNATURA      OUT VARCHAR2,
        P_DEPT_CODE_DEST       OUT VARCHAR2,
        P_DEPT_DESC_DEST       OUT VARCHAR2,
        P_COMENTARIO           OUT VARCHAR2,
        P_TELEFONO             OUT VARCHAR2,
        P_MESSAGE              OUT VARCHAR2
)
AS 

    V_ERROR                 EXCEPTION;
    V_ERROR2                EXCEPTION;
    V_ERROR3                EXCEPTION;
    V_ASIG                  INTEGER := 0;
    V_ASIGNATURA_NOMBRE     VARCHAR2(4000);
    V_CODIGO_SOLICITUD      SVVSRVC.SVVSRVC_CODE%TYPE;
    V_CLAVE                 VARCHAR2(4000);
    V_DESCRIPTION           VARCHAR2(4000);
    V_ADDL_DATA_SEQ         SVRSVAD.SVRSVAD_ADDL_DATA_SEQ%TYPE;

    CURSOR C_SGRSATT IS
        SELECT SGRSATT_ATTS_CODE
        FROM (
            SELECT SGRSATT_ATTS_CODE 
            FROM SGRSATT
            WHERE SGRSATT_PIDM = P_PIDM
                AND SUBSTR(SGRSATT_ATTS_CODE,1,2) = 'P0'
                ORDER BY SGRSATT_TERM_CODE_EFF DESC
            )
        WHERE ROWNUM = 1;

    -- GET COMENTARIO de la solicitud. (comentario PERSONALZIADO)
    CURSOR C_SVRSVAD IS
        SELECT SVRSVAD_ADDL_DATA_SEQ, SVRSVAD_ADDL_DATA_CDE, SVRSVAD_ADDL_DATA_DESC
        FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
            INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
        ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
        WHERE SVRSRAD_SRVC_CODE = V_CODIGO_SOLICITUD
            AND SVRSVAD_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD
        ORDER BY SVRSVAD_ADDL_DATA_SEQ;

BEGIN
    --################################################################################################
    --OBTENIENDO ATRIBUTO DE PLAN
    --################################################################################################
    OPEN C_SGRSATT;
    FETCH C_SGRSATT INTO P_ATTRB_PLAN;
        IF C_SGRSATT%NOTFOUND THEN
            RAISE V_ERROR;
        END IF;
    CLOSE C_SGRSATT;

    --################################################################################################
    --OBTENIENDO CODIGO DE ASIGNATURA
    --################################################################################################

    -- GET CODIGO SOLICITUD
    SELECT SVRSVPR_SRVC_CODE 
    INTO V_CODIGO_SOLICITUD 
    FROM SVRSVPR 
    WHERE SVRSVPR_PROTOCOL_SEQ_NO = P_FOLIO_SOLICITUD;

    -- OBTENER CODIGO DE ASIGNATURA DE LA SOLICITUD
    OPEN C_SVRSVAD;
    LOOP
      FETCH C_SVRSVAD INTO V_ADDL_DATA_SEQ, V_CLAVE, V_DESCRIPTION;
      IF C_SVRSVAD%FOUND THEN
          IF V_ADDL_DATA_SEQ = 1 THEN
              -- OBTENER CODIGO Y DESCRIPCION DE ASIGNATURA DE LA SOLICITUD
              P_ASIGNATURA_CODE := V_CLAVE;
              P_ASIGNATURA_DESC := V_DESCRIPTION;
          ELSIF V_ADDL_DATA_SEQ = 2 THEN
              -- GET DEPARTAMENTO DE DESTINO
              P_DEPT_CODE_DEST := V_CLAVE;
              P_DEPT_DESC_DEST := V_DESCRIPTION;
          ELSIF V_ADDL_DATA_SEQ = 3 THEN
              -- GET RAZÓN DE LA SOLICITUD
              P_COMENTARIO := V_DESCRIPTION;
          ELSE
              -- GET TELEFONO DE ESTUDIANTE
              P_TELEFONO := V_DESCRIPTION;
          END IF;
      ELSE EXIT;
    END IF;
    END LOOP;
    CLOSE C_SVRSVAD;

    IF (P_ASIGNATURA_CODE IS NULL) THEN
      RAISE V_ERROR2;
    END IF;

    --################################################################################################
    --OBTENIENDO DESCRIPCION DE ASIGNATURA
    --################################################################################################
    SELECT COUNT(*)
    INTO V_ASIG
    FROM SCRSYLN
    WHERE SCRSYLN_SUBJ_CODE || SCRSYLN_CRSE_NUMB = P_ASIGNATURA_CODE;

    IF V_ASIG > 0 THEN
       P_PLAN_ASIGNATURA := P_ATTRB_PLAN || ' - ' || P_ASIGNATURA_CODE || ' - ' || P_ASIGNATURA_DESC;

    ELSE
        RAISE V_ERROR3;
    END IF;

    P_MESSAGE := 'OK';

    EXCEPTION
    WHEN V_ERROR THEN
        P_MESSAGE:='No tiene registrado atributo de plan de estudio.';
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| P_MESSAGE);
    WHEN V_ERROR2 THEN
        P_MESSAGE:='La asignatura no fue seleccionada correctamente o no existe.';
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| P_MESSAGE);
    WHEN V_ERROR3 THEN
        P_MESSAGE:='No existe registro de la asignatura solicitada.';
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| P_MESSAGE);
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_STUDENT_INPUTS;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_APTO (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
    P_ASIGNATURA_CODE   IN VARCHAR2,
    P_STATUS_APTO       OUT VARCHAR2,
    P_REASON            OUT VARCHAR2
)
AS
        V_RET01             INTEGER := 0; --VALIDA RETENCIÓN 01
        V_RET02             INTEGER := 0; --VALIDA RETENCIÓN 02
        V_RET04             INTEGER := 0; --VALIDA RETENCIÓN 04
        V_RET07             INTEGER := 0; --VALIDA RETENCIÓN 07
        V_RETPER            INTEGER := 0; --VALIDA RETENCIÓN 08 - 16
        V_SOLTRA            INTEGER := 0; --VALIDA SOLICITUD TRASLADO INTERNO
        V_SOLRES            INTEGER := 0; --VALIDA SOLICITUD RESERVA
        V_SOLCAM            INTEGER := 0; --VALIDA SOLICITUD CAMBIO DE PLAN
        V_SOLCUO            INTEGER := 0; --VALIDA SOLICITUD CAMBIO CUOTA INICIAL
        V_SOLPER            INTEGER := 0; --VALIDA SOLICITUD PERMANENCIA
        V_SOLDIR            INTEGER := 0; --VALIDA SOLICITUD DIRIGIDO
        V_SOLMUL            INTEGER := 0; --VALIDA SOLICITUD MULTIMODAL
        V_ASI_MAT           INTEGER := 0; --VALIDA SI TIENE LA ASIGNATURA MATRICULADA
        V_RESPUESTA         VARCHAR2(4000) := NULL;
        V_MESSAGE           VARCHAR2(4000) := NULL;
        V_FLAG              INTEGER := 0;

        
        V_CODE_DEPT         STVDEPT.STVDEPT_CODE%TYPE;
        V_CODE_TERM         STVTERM.STVTERM_CODE%TYPE;
        V_CODE_CAMP         STVCAMP.STVCAMP_CODE%TYPE;
        V_PROGRAM           SORLCUR.SORLCUR_PROGRAM%TYPE;
        V_TERMCTLG          SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE;
        
        V_PRIORIDAD         NUMBER;
        V_HAS_RETENCION     NUMBER;
        V_HAS_NRC           NUMBER;
        
    --################################################################################################
    -- OBTENER PERIODO DE CATALOGO, CAMPUS, DEPT y CARRERA
    --################################################################################################
    CURSOR C_SORLCUR IS
        SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
        FROM(
            SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
            FROM SORLCUR 
            INNER JOIN SORLFOS
                ON SORLCUR_PIDM         = SORLFOS_PIDM 
                AND SORLCUR_SEQNO       = SORLFOS_LCUR_SEQNO
            WHERE SORLCUR_PIDM          = P_PIDM 
                AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y'
                AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
        ) WHERE ROWNUM = 1;
        
BEGIN
    ---P_VERIFICAR_APTO PARA SOLICITUD DE CAMBIO DE PLAN
    
    --################################################################################################
    -->> Obtener SEDE, DEPT, TERM CATALOGO Y PROGRAMA (CARRERA)
    --################################################################################################
    OPEN C_SORLCUR;
    LOOP
        FETCH C_SORLCUR INTO V_CODE_CAMP, V_CODE_DEPT, V_TERMCTLG, V_PROGRAM;
        EXIT WHEN C_SORLCUR%NOTFOUND;
        END LOOP;
    CLOSE C_SORLCUR;
    
    --################################################################################################
    ---VALIDACIÓN DE RETENCIÓN 01 (DEUDA PENDIENTE)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET01
    FROM SPRHOLD
        WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '01'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
    
    IF V_RET01 > 0 THEN
        V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de deuda pendiente activa. ';            
        V_FLAG := 1;
    END IF;

    --################################################################################################   
    ---VALIDACIÓN DE RETENCIÓN 02 (DOCUMENTO PENDIENTE)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET02    
    FROM SPRHOLD
    WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '02'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RET02 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de documento pendiente activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de documento pendiente activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################   
    ---VALIDACIÓN DE RETENCIÓN 04 (PAGO DE MATRICULA Y PRIMERA CUOTA)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET04
    FROM SPRHOLD
    WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '04'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RET04 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de pago de matricula y primera cuota activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de pago de matricula y primera cuota activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################   
    ---VALIDACIÓN DE RETENCIÓN 07 (CAMBIO DE PLAN DE ESTUDIO)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RET07    
    FROM SPRHOLD
    WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE = '07'
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RET07 > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de cambio de plan activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de cambio de plan activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################   
    ---VALIDACIÓN DE RETENCIÓN 08 - 16 (PERMANENCIA DE PLAN)
    --################################################################################################
    SELECT COUNT(*)
    INTO V_RETPER
    FROM SPRHOLD
    WHERE SPRHOLD_PIDM = P_PIDM
        AND SPRHOLD_HLDD_CODE IN ('08','16')
        --AND TO_DATE(SYSDATE,'DD/MM/YYYY HH:MI:SS') BETWEEN TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YYYY HH:MI:SS') AND TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YYYY HH:MI:SS');
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') >= TO_DATE(SPRHOLD_FROM_DATE,'DD/MM/YY HH:MI:SS')
        AND TO_DATE(SYSDATE,'DD/MM/YY HH:MI:SS') <= TO_DATE(SPRHOLD_TO_DATE,'DD/MM/YY HH:MI:SS');
        
    IF V_RETPER > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene retención de Permanencia de plan de estudios activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene retención de Permanencia de plan de estudios activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE RESERVA DE MATRICULA ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLRES
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL005'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLRES > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Reserva de Matricula activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE CAMBIO DE PLAN DE ESTUDIOS ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLCAM
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL004'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLCAM > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Rectificación de Matricula activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE TRASLADO INTERNO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLTRA
    FROM SVRSVPR
    WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL013'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
    
    IF V_SOLTRA > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Traslado Interno activa. ';            
        END IF;
        V_FLAG := 1;
    END IF;
    
    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE ASIGNATURA DIRIGIDA EN PARALELO CON LA MISMA ASIGNATURA
    --################################################################################################
    SELECT COUNT(SVRSVAD_ADDL_DATA_CDE)
    INTO V_SOLDIR
    FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
            INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
            ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
            INNER JOIN SVRSVPR
            ON SVRSRAD_SRVC_CODE = SVRSVPR_SRVC_CODE
            AND SVRSVAD_PROTOCOL_SEQ_NO = SVRSVPR_PROTOCOL_SEQ_NO
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSRAD_SRVC_CODE = 'SOL014'
        AND SVRSVAD_ADDL_DATA_CDE = P_ASIGNATURA_CODE
        AND SVRSVPR_SRVS_CODE IN ('AC','AP');
      
      IF V_SOLDIR > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Matrícula de Asignatura Dirigida de la misma asignatura activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Matrícula de Asignatura Dirigida de la misma asignatura activa. ';
        END IF;
        V_FLAG := 1;
      END IF;
  

    --################################################################################################
    ---VALIDACIÓN DE SOLICITUD DE PERMANENCIA DE PLAN DE ESTUDIOS ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_SOLPER
    FROM SVRSVPR
        WHERE SVRSVPR_PIDM = P_PIDM
        AND SVRSVPR_TERM_CODE = P_TERM_CODE
        AND SVRSVPR_SRVC_CODE = 'SOL015'
        AND SVRSVPR_SRVS_CODE in ('AC','AP');
        
    IF V_SOLPER > 0 THEN 
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Permanencia de Plan de Estudio activa. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Permanencia de Plan de Estudio activa. ';
        END IF;
        V_FLAG := 1;
    END IF;
    
--    --################################################################################################
--    ---VALIDACIÓN DE SOLICITUD DE ASIGNATURA MULTIMODAL EN PARALELO CON LA MISMA ASIGNATURA
--    --################################################################################################
--    SELECT COUNT(SVRSVAD_ADDL_DATA_CDE)
--    INTO V_SOLMUL
--    FROM SVRSVAD --------------------------------------- SVASVPR datos adicionales de SOL que alumno ingresa
--            INNER JOIN SVRSRAD --------------------------------- SVASRAD Datos adicionales de Reglas Servicio
--            ON SVRSVAD_ADDL_DATA_SEQ = SVRSRAD_ADDL_DATA_SEQ
--            INNER JOIN SVRSVPR
--            ON SVRSRAD_SRVC_CODE = SVRSVPR_SRVC_CODE
--            AND SVRSVAD_PROTOCOL_SEQ_NO = SVRSVPR_PROTOCOL_SEQ_NO
--        WHERE SVRSVPR_PIDM = P_PIDM
--        AND SVRSVPR_TERM_CODE = P_TERM_CODE
--        AND SVRSRAD_SRVC_CODE = 'SOL016'
--        AND SVRSVAD_ADDL_DATA_CDE = P_ASIGNATURA_CODE
--        AND SVRSVPR_SRVS_CODE IN ('AC','AP');
--      
--      IF V_SOLMUL > 0 THEN 
--        IF V_FLAG = 1 THEN
--            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Tiene solicitud de Matrícula de Asignatura Multimodal de la misma asignatura activa. ';
--        ELSE
--            V_RESPUESTA := '<br />' || '<br />' || '- Tiene solicitud de Matrícula de Asignatura Multimodal de la misma asignatura activa. ';
--        END IF;
--        V_FLAG := 1;
--      END IF;
      
    --################################################################################################
    -- >> VALIDACIÓN DE CURSO MATRICULADO
    --################################################################################################

    SELECT COUNT(*)
    INTO V_ASI_MAT
    FROM SFRSTCR
    INNER JOIN SSBSECT ON
        SFRSTCR_TERM_CODE = SSBSECT_TERM_CODE
        and SFRSTCR_CRN = SSBSECT_CRN
    WHERE SFRSTCR_TERM_CODE = P_TERM_CODE
        AND SFRSTCR_PIDM = P_PIDM
        AND SFRSTCR_RSTS_CODE IN ('RW','RE')
        AND SSBSECT_SUBJ_CODE || SSBSECT_CRSE_NUMB = P_ASIGNATURA_CODE;

    IF V_ASI_MAT > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- Ya esta matriculado en la asignatura solicitada. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- Ya esta matriculado en la asignatura solicitada. ';            
        END IF;
        V_FLAG := 1;
    END IF;
    
    ---VALIDACIÓN FINAL
    --################################################################################################
    IF V_FLAG > 0 THEN
        P_STATUS_APTO := 'FALSE';
        P_REASON := V_RESPUESTA;
    ELSE
        P_STATUS_APTO := 'TRUE';
        P_REASON := 'OK';
    END IF;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_VERIFICAR_APTO;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_ESTADO_SOLICITUD (
        P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_STATUS_SOL          OUT VARCHAR2,
        P_MESSAGE             OUT VARCHAR2
)
AS
BEGIN

   BWZKPSPG.P_VERIFICAR_ESTADO_SOLICITUD (P_FOLIO_SOLICITUD, P_STATUS_SOL, P_MESSAGE);

EXCEPTION
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_ESTADO_SOLICITUD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
        P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
        P_AN                  OUT NUMBER,
        P_MESSAGE             OUT VARCHAR2
)
AS
BEGIN

BWZKPSPG.P_CAMBIO_ESTADO_SOLICITUD (P_FOLIO_SOLICITUD, P_ESTADO, P_AN, P_MESSAGE);

EXCEPTION
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_CAMBIO_ESTADO_SOLICITUD;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_COORDINADOR (
        P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, -- sensible mayuscula 'S01'
        P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE, --sensible mayuscula 'UREG'
        P_ROL_DC              IN WORKFLOW.ROLE.NAME%TYPE, -- sensible a mayusculas 'coordinador'
        P_CORREO_COORD        OUT VARCHAR2,
        P_ROL_COORD           OUT VARCHAR2
)
AS
    -- @PARAMETERS
    V_ROLE_ID                 NUMBER;
    V_ORG_ID                  NUMBER;
    V_INDICADOR               NUMBER;
    V_Email_Address           VARCHAR2(500);
    V_EXEPTION                EXCEPTION;
    
    V_CODE_DEPT      STVDEPT.STVDEPT_CODE%TYPE;
    V_CODE_CAMP      STVCAMP.STVCAMP_CODE%TYPE;
    V_PROGRAM        SORLCUR.SORLCUR_PROGRAM%TYPE;
    V_TERMCTLG       SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE;
    
    CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * 
        FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = V_ORG_ID 
        AND ROLE_ID = V_ROLE_ID;
      
    V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
      
    --################################################################################################
    -- OBTENER PERIODO DE CATALOGO, CAMPUS, DEPT y CARRERA
    --################################################################################################
    CURSOR C_SORLCUR IS
        SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
        FROM(
            SELECT SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE, SORLCUR_TERM_CODE_CTLG, SORLCUR_PROGRAM
            FROM SORLCUR 
            INNER JOIN SORLFOS
                ON SORLCUR_PIDM         = SORLFOS_PIDM 
                AND SORLCUR_SEQNO       = SORLFOS_LCUR_SEQNO
            WHERE SORLCUR_PIDM          = P_PIDM 
                AND SORLCUR_LMOD_CODE   = 'LEARNER' /*#Estudiante*/ 
                AND SORLCUR_CACT_CODE   = 'ACTIVE' 
                AND SORLCUR_CURRENT_CDE = 'Y'
                AND SORLCUR_TERM_CODE_END IS NULL
            ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC 
        ) WHERE ROWNUM = 1;
        
BEGIN 
        --################################################################################################
        -->> Obtener SEDE, DEPT, TERM CATALOGO Y PROGRAMA (CARRERA)
        --################################################################################################
        OPEN C_SORLCUR;
        LOOP
            FETCH C_SORLCUR INTO V_CODE_CAMP, V_CODE_DEPT, V_TERMCTLG, V_PROGRAM;
            EXIT WHEN C_SORLCUR%NOTFOUND;
            END LOOP;
        CLOSE C_SORLCUR;

        IF P_DEPT_CODE = 'UREG' THEN
            P_ROL_COORD := P_ROL_DC || P_DEPT_CODE || V_PROGRAM || P_CAMP_CODE;
        ELSE
            P_ROL_COORD := P_ROL_DC || P_DEPT_CODE || '000' || P_CAMP_CODE;
        END IF;
        
        -- Obtener el ROL_ID 
        SELECT ID 
        INTO V_ROLE_ID 
        FROM WORKFLOW.ROLE 
        WHERE NAME = P_ROL_COORD;
        
        -- Obtener el ORG_ID 
        SELECT ID 
        INTO V_ORG_ID 
        FROM WORKFLOW.ORGANIZATION 
        WHERE NAME = 'Root';
        
        --Validar que exista el ROL
        SELECT COUNT(*)
        INTO V_INDICADOR 
        FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = V_ORG_ID 
        AND ROLE_ID = V_ROLE_ID;
        
        IF V_INDICADOR = 0 THEN
          RAISE V_EXEPTION;
        END if;
        
        -- #######################################################################
        OPEN C_ROLE_ASSIGNMENT;
        LOOP
          FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
          EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
                      
              -- Obtener Datos Usuario
              SELECT Email_Address 
              INTO V_Email_Address 
              FROM WORKFLOW.WFUSER 
              WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;
  
              P_CORREO_COORD := P_CORREO_COORD || V_Email_Address || ',';
              
        END LOOP;
        CLOSE C_ROLE_ASSIGNMENT;
        
        -- Extraer el ultimo digito en caso sea un "coma"(,)
        SELECT SUBSTR(P_CORREO_COORD,1,LENGTH(P_CORREO_COORD) -1)
        INTO P_CORREO_COORD
        FROM DUAL
        WHERE SUBSTR(P_CORREO_COORD,-1,1) = ',';

EXCEPTION
  WHEN V_EXEPTION THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| 'No se encontro el ROL: ' || P_ROL_COORD);
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_COORDINADOR;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_GET_USER_PROGRAMACION (
        P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
        P_DEPT_CODE_DEST      IN STVDEPT.STVDEPT_CODE%TYPE, --sensible mayuscula 'UREG'
        P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, -- sensible mayuscula 'S01'
        P_ROL_PROGRAMACION    IN WORKFLOW.ROLE.NAME%TYPE, -- sensible a minusculas 'progracad'
        P_CORREO_PROGR        OUT VARCHAR2,
        P_ROL_PROGR           OUT VARCHAR2
)
AS
    -- @PARAMETERS
    V_ROLE_ID                   NUMBER;
    V_ORG_ID                    NUMBER;
    V_INDICADOR                 NUMBER;
    V_Email_Address             VARCHAR2(500);
    V_EXEPTION                  EXCEPTION;
    
    CURSOR C_ROLE_ASSIGNMENT IS
        SELECT * 
        FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = V_ORG_ID 
        AND ROLE_ID = V_ROLE_ID;
      
    V_ROLE_ASSIGNMENT_REC       WORKFLOW.ROLE_ASSIGNMENT%ROWTYPE;
        
BEGIN 
        
        IF P_DEPT_CODE = 'UVIR' THEN
            P_ROL_PROGR := P_ROL_PROGRAMACION || P_DEPT_CODE || '000';
          ELSIF P_DEPT_CODE_DEST = 'UVIR' THEN
            P_ROL_PROGR := P_ROL_PROGRAMACION || P_DEPT_CODE_DEST || '000';
          ELSE
            P_ROL_PROGR := P_ROL_PROGRAMACION || P_DEPT_CODE_DEST || P_CAMP_CODE;
        END IF;

        -- Obtener el ROL_ID 
        SELECT ID 
        INTO V_ROLE_ID 
        FROM WORKFLOW.ROLE 
        WHERE NAME = P_ROL_PROGR;

        -- Obtener el ORG_ID 
        SELECT ID 
        INTO V_ORG_ID 
        FROM WORKFLOW.ORGANIZATION 
        WHERE NAME = 'Root';

        --Validar que exista el ROL
        SELECT COUNT(*)
        INTO V_INDICADOR 
        FROM WORKFLOW.ROLE_ASSIGNMENT 
        WHERE ORG_ID = V_ORG_ID 
        AND ROLE_ID = V_ROLE_ID;

        IF V_INDICADOR = 0 THEN
          RAISE V_EXEPTION;
        END if;

        -- #######################################################################
        OPEN C_ROLE_ASSIGNMENT;
        LOOP
          FETCH C_ROLE_ASSIGNMENT INTO V_ROLE_ASSIGNMENT_REC;
          EXIT WHEN C_ROLE_ASSIGNMENT%NOTFOUND;
                      
              -- Obtener Datos Usuario
              SELECT Email_Address 
              INTO V_Email_Address 
              FROM WORKFLOW.WFUSER 
              WHERE ID = V_ROLE_ASSIGNMENT_REC.USER_ID ;

              P_CORREO_PROGR := P_CORREO_PROGR || V_Email_Address || ',';
              
        END LOOP;
        CLOSE C_ROLE_ASSIGNMENT;
        
        -- Extraer el ultimo digito en caso sea un "coma"(,)
        SELECT SUBSTR(P_CORREO_PROGR,1,LENGTH(P_CORREO_PROGR) -1)
        INTO P_CORREO_PROGR
        FROM DUAL
        WHERE SUBSTR(P_CORREO_PROGR,-1,1) = ',';

EXCEPTION
  WHEN V_EXEPTION THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| 'No se encontro el ROL ' || P_ROL_PROGR);
  WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_GET_USER_PROGRAMACION;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICA_NRC_MULTI (
        P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_NRC_CODE          IN SSBSECT.SSBSECT_CRN%TYPE,
        P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
        P_CAMP_SEDE         IN STVCAMP.STVCAMP_CODE%TYPE,
        P_DEPT_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
        P_ASIGNATURA_CODE   IN VARCHAR2,
        P_FECHA_INICIO      OUT VARCHAR2,
        P_FECHA_FIN         OUT VARCHAR2,
        P_STATUS_NRC        OUT VARCHAR2,
        P_REASON_NRC        OUT VARCHAR2
)
AS
        V_NRC_ACTIVO        INTEGER := 0; --NRC ACTIVO
        V_NRC_ASIG          INTEGER := 0; --ASIGNATURA ASIGNADA AL NRC
        V_RESPUESTA         VARCHAR2(4000) := NULL;
        V_EXISTENCIA_NRC    INTEGER := 0;
        V_CONFLICTO_HORARIO INTEGER := 0;
        V_NRC_MULTI         INTEGER := 0;
        V_VACANTES_MULTI    INTEGER := 0;
        V_FLAG              INTEGER := 0;

BEGIN
    --################################################################################################
    --VALIDACIÓN DEL INPUT DE NRC
    --################################################################################################

    IF P_NRC_CODE IS NULL OR P_NRC_CODE = '' THEN
        V_RESPUESTA := '<br />' || '<br />' || '- No ha registrado el código del NRC. ';
        
        P_STATUS_NRC := 'FALSE';
        P_REASON_NRC := V_RESPUESTA;
        RETURN;
    END IF;

    --################################################################################################
    --VALIDACIÓN DE ESTADO ACTIVO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_NRC_ACTIVO
    FROM SSBSECT
    WHERE SSBSECT_TERM_CODE = P_TERM_CODE
    AND SSBSECT_CRN = P_NRC_CODE
    AND SSBSECT_SSTS_CODE = 'A';
    
    IF V_NRC_ACTIVO <= 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- El NRC no está activo. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- El NRC no está activo. ';
        END IF;
        V_FLAG := 1;
    END IF;
    
    --################################################################################################
    ---VALIDACIÓN DE ASIGNATURA ASIGNADA AL NRC
    --################################################################################################

    SELECT COUNT(*)
    INTO V_NRC_ASIG
    FROM SFRREGP
    WHERE SFRREGP_PIDM = P_PIDM
    AND SFRREGP_TERM_CODE = P_TERM_CODE
    AND SFRREGP_SUBJ_CODE || SFRREGP_CRSE_NUMB_LOW = P_ASIGNATURA_CODE
    AND SFRREGP_Z_VISIBLE_IND = 'Y';
    
    IF V_NRC_ASIG <= 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- La asignatura no esta disponible o no pertenece al plan de estudios del estudiante. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- La asignatura no esta disponible o no pertenece al plan de estudios del estudiante. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÓN DE EXISTENCIA DE NRC MULTIMODAL
    --################################################################################################
    SELECT COUNT(*)
    INTO V_EXISTENCIA_NRC
    FROM SSBSECT 
    WHERE SSBSECT_TERM_CODE = P_TERM_CODE 
    and SSBSECT_CRN = P_NRC_CODE
    and SSBSECT_SSTS_CODE = 'A' 
    and SSBSECT_SEQ_NUMB <> '0';
    
    IF V_EXISTENCIA_NRC <= 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- El NRC no existe o es semilla. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- El NRC no existeo es semilla. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÃ“N DE CONFLICTO DE HORARIO DEL ESTUDIANTE
    --################################################################################################
    SELECT COUNT(*)
    INTO V_CONFLICTO_HORARIO
    FROM SVQ_SSRMEET_TIMECONFLICT
    WHERE SSRMEET_TERM_CODE = P_TERM_CODE
    AND SSRMEET_CRN = P_NRC_CODE
    AND SSRMEET_CRN_CONFLICT IN 
        (SELECT SFRSTCR_CRN 
            FROM SFRSTCR 
            WHERE SFRSTCR_TERM_CODE = P_TERM_CODE 
            AND SFRSTCR_PIDM = P_PIDM
    );
    
    IF V_CONFLICTO_HORARIO > 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- El NRC tiene cruce de horario con lo matriculado por el estudiante. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- El NRC tiene cruce de horario con lo matriculado por el estudiante. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÃ“N DE NRC MULTIMODAL
    --################################################################################################
    SELECT COUNT(*)
    INTO V_NRC_MULTI
    FROM SSRRESV
    WHERE SSRRESV_TERM_CODE = P_TERM_CODE
    AND SSRRESV_CRN = P_NRC_CODE
    AND SSRRESV_DEPT_CODE <> P_DEPT_CODE;

    IF V_NRC_MULTI <= 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- El NRC no es multimodal. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- El NRC no es multimodal. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    ---VALIDACIÃ“N DE VACANTES MULTIMODAL
    --################################################################################################

    IF V_NRC_MULTI > 0 THEN
        SELECT COUNT(*)
        INTO V_VACANTES_MULTI
        FROM SSRRESV
        WHERE SSRRESV_TERM_CODE = P_TERM_CODE 
        AND SSRRESV_CRN = P_NRC_CODE
        AND SSRRESV_DEPT_CODE <> P_DEPT_CODE
        AND SSRRESV_LEVL_CODE IS NOT NULL 
        AND SSRRESV_SEATS_AVAIL > 0;
    ELSE
        SELECT COUNT(*)
        INTO V_VACANTES_MULTI
        FROM SSBSECT
        WHERE SSBSECT_TERM_CODE = P_TERM_CODE
        AND SSBSECT_CRN = P_NRC_CODE 
        AND SSBSECT_SEATS_AVAIL > 0;
    END IF;

    IF V_VACANTES_MULTI <= 0 THEN
        IF V_FLAG = 1 THEN
            V_RESPUESTA := V_RESPUESTA || '<br />' || '- El NRC no tiene vacantes. ';
        ELSE
            V_RESPUESTA := '<br />' || '<br />' || '- El NRC no tiene vacantes. ';
        END IF;
        V_FLAG := 1;
    END IF;

    --################################################################################################
    --VALIDACIÃ“N FINAL
    --################################################################################################
    IF V_FLAG > 0 THEN
        P_STATUS_NRC := 'FALSE';
        P_FECHA_INICIO := '';
        P_FECHA_FIN := '';
        P_REASON_NRC := V_RESPUESTA;
    ELSE
        SELECT DISTINCT TO_DATE(SSBSECT_PTRM_START_DATE), TO_DATE(SSBSECT_PTRM_END_DATE)
        INTO P_FECHA_INICIO, P_FECHA_FIN
        FROM SSBSECT 
        WHERE SSBSECT_TERM_CODE = P_TERM_CODE 
        and SSBSECT_CRN = P_NRC_CODE;
        
        P_STATUS_NRC := 'TRUE';
        P_REASON_NRC := 'OK';
    END IF;
    
EXCEPTION
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICA_NRC_MULTI;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_VERIFICAR_MATRICULA (
        P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_ASIGNATURA_CODE   IN VARCHAR2,
        P_TERM_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
        P_STATUS            OUT VARCHAR2,
        P_REASON_MATRICULA  OUT VARCHAR2
)
AS
        V_ASI_MAT           INTEGER := 0; --VALIDA LA ASIGNATURA MATRICULADA
        
BEGIN
    --################################################################################################
    -- >> VALIDACIÓN DE CURSO MATRICULADO
    --################################################################################################
    SELECT COUNT(*)
    INTO V_ASI_MAT
    FROM SFRSTCR
    INNER JOIN SSBSECT ON
        SFRSTCR_TERM_CODE = SSBSECT_TERM_CODE
        and SFRSTCR_CRN = SSBSECT_CRN
    WHERE SFRSTCR_TERM_CODE = P_TERM_CODE
        AND SFRSTCR_PIDM = P_PIDM
        AND SFRSTCR_RSTS_CODE IN ('RW','RE')
        AND SSBSECT_SUBJ_CODE || SSBSECT_CRSE_NUMB = P_ASIGNATURA_CODE;

    IF V_ASI_MAT > 0 THEN
        P_STATUS := 'TRUE';
        P_REASON_MATRICULA := 'Ya te encuentras matriculado(a) en la asignatura solicitada. ';            
    ELSE
        P_STATUS := 'FALSE';
        P_REASON_MATRICULA := 'OK';            
    END IF;

EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_VERIFICAR_MATRICULA;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_INSERT_SOBREPASOS (
      P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE, 
      P_TERM_CODE       IN STVTERM.STVTERM_CODE%TYPE,
      P_CAMP_CODE       IN STVCAMP.STVCAMP_CODE%TYPE,
      P_NRC_CODE        IN SSBSECT.SSBSECT_CRN%TYPE,
      P_DEPT_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
      P_MESSAGE         OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_INSERT_SOBREPASOS
  FECHA     : 11/07/2018
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : El objetivo es insertar los sobrepasos 07 "Departamento/Modalidad" y 03 "Campus", haciendo una 
              verificación de si tiene insertado el registro. (los códigos de los sobrepasos trabajarán 
              en variables locales)   
  =================================================================================================================== */
AS
      V_PTRM_DEPT       VARCHAR2(9); -- GTVINSM
      V_CAMP_CODE       STVCAMP.STVCAMP_CODE%TYPE;
      V_IF_ROVR         NUMBER;
      
BEGIN 

      -- VALIDAR QUE NRC TENGA UNA DISTINTA MODALIDAD ALUMNO<
      SELECT CASE SUBSTR(SSBSECT_PTRM_CODE,1,1) WHEN 'V' THEN 'UVIR' 
                                                WHEN 'R' THEN 'UREG' 
                                                WHEN 'W' THEN 'UPGT'
                                                ELSE '-' END, 
            SSBSECT_CAMP_CODE 
      INTO  V_PTRM_DEPT,  
            V_CAMP_CODE
      FROM SSBSECT 
      WHERE SSBSECT_TERM_CODE = P_TERM_CODE 
      AND   SSBSECT_CRN = P_NRC_CODE;
      
      -- INSERTAR  SOBREPASO SFRSRPO --- 03 DEPT(STVROVR)
      IF (V_PTRM_DEPT <> P_DEPT_CODE) THEN
          INSERT INTO SFRSRPO (
            SFRSRPO_TERM_CODE,          SFRSRPO_PIDM,           SFRSRPO_ROVR_CODE,
            SFRSRPO_SUBJ_CODE,          SFRSRPO_CRSE_NUMB,      SFRSRPO_ACTIVITY_DATE,
            SFRSRPO_SEQ_NUMB,           SFRSRPO_CRN,            SFRSRPO_USER
          )
          SELECT 
            SSBSECT_TERM_CODE,          P_PIDM,                 '03',
            SSBSECT_SUBJ_CODE,          SSBSECT_CRSE_NUMB,      SYSDATE,
            SSBSECT_SEQ_NUMB,           SSBSECT_CRN,            'WFAUTO'
          FROM SSBSECT 
          WHERE SSBSECT_TERM_CODE = P_TERM_CODE 
          AND SSBSECT_CRN = P_NRC_CODE
          AND NOT EXISTS(
              SELECT * FROM SFRSRPO WHERE SFRSRPO_TERM_CODE = P_TERM_CODE AND SFRSRPO_PIDM = P_PIDM AND SFRSRPO_CRN = P_NRC_CODE AND SFRSRPO_ROVR_CODE = '03'
          );
      END IF;

      
      -- INSERTAR  SOBREPASO SFRSRPO --- 07 CAMPUS (STVROVR)
      IF (V_CAMP_CODE <> P_CAMP_CODE) THEN
          INSERT INTO SFRSRPO (
            SFRSRPO_TERM_CODE,          SFRSRPO_PIDM,           SFRSRPO_ROVR_CODE,
            SFRSRPO_SUBJ_CODE,          SFRSRPO_CRSE_NUMB,      SFRSRPO_ACTIVITY_DATE,
            SFRSRPO_SEQ_NUMB,           SFRSRPO_CRN,            SFRSRPO_USER
          )
          SELECT 
            SSBSECT_TERM_CODE,          P_PIDM,                 '07',
            SSBSECT_SUBJ_CODE,          SSBSECT_CRSE_NUMB,      SYSDATE,
            SSBSECT_SEQ_NUMB,           SSBSECT_CRN,            'WFAUTO'
          FROM SSBSECT 
          WHERE SSBSECT_TERM_CODE = P_TERM_CODE 
          AND SSBSECT_CRN = P_NRC_CODE
          AND NOT EXISTS(
              SELECT * FROM SFRSRPO WHERE SFRSRPO_TERM_CODE = P_TERM_CODE AND SFRSRPO_PIDM = P_PIDM AND SFRSRPO_CRN = P_NRC_CODE AND SFRSRPO_ROVR_CODE = '07'
          );
      END IF;

      --
      COMMIT; 
EXCEPTION
    WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_INSERT_SOBREPASOS;
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

PROCEDURE P_INSERT_NRC_MULTI (
        P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_NRC_CODE        IN SSBSECT.SSBSECT_CRN%TYPE,
        P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
        P_MESSAGE         OUT VARCHAR2
)
AS
        V_HORMA             INTEGER := 0; --VERIFICACIÓN DE HORAS MAX
        V_MATRI             INTEGER := 0; --VERIFICACIÓN DE MATRICULA
        V_STATU             INTEGER := 0; --VERIFICACIÓN DE ESTATUS ELEGIBLE
        V_SEQNC             INTEGER := 0;
        V_SEQ_PLAN          INTEGER := 0;
        V_PTRM              VARCHAR2(100);
        V_DEPT              VARCHAR2(100);
        V_CAMP              VARCHAR2(100);
        V_CRED              INTEGER := 0;
        V_CAMP_ALUM         VARCHAR2(100);
        V_DEPT_ALUM         VARCHAR2(100);
        V_CAMP_OVER         VARCHAR2(10) := 'N';
        V_DEPT_OVER         VARCHAR2(10) := 'N';
BEGIN

        SELECT SORLCUR_KEY_SEQNO, SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE
        INTO V_SEQ_PLAN, V_CAMP_ALUM, V_DEPT_ALUM
        FROM (
              SELECT SORLCUR_KEY_SEQNO, SORLCUR_CAMP_CODE, SORLFOS_DEPT_CODE
              FROM SORLCUR
              INNER JOIN SORLFOS ON
                    SORLCUR_PIDM = SORLFOS_PIDM 
                    AND SORLCUR_SEQNO = SORLFOS_LCUR_SEQNO
              WHERE SORLCUR_PIDM = P_PIDM 
                  AND SORLCUR_LMOD_CODE = 'LEARNER' /*#Estudiante*/ 
                  AND SORLCUR_CACT_CODE = 'ACTIVE' 
                  AND SORLCUR_CURRENT_CDE = 'Y'
                  AND SORLCUR_TERM_CODE_END IS NULL
                  ORDER BY SORLCUR_TERM_CODE DESC, SORLCUR_SEQNO DESC
        ) WHERE ROWNUM = 1;

    --################################################################################################
    ---VERIFICAR HORAS MAX DE MATRICULA EN EL PERIODO 
    --################################################################################################
        SELECT COUNT(*)
        INTO V_HORMA
        FROM SFBETRM
        WHERE SFBETRM_PIDM = P_PIDM
        AND SFBETRM_TERM_CODE = P_TERM_CODE;
        
        IF V_HORMA = 0 THEN
            INSERT INTO SFBETRM (
                            SFBETRM_TERM_CODE,          SFBETRM_PIDM,               SFBETRM_ESTS_CODE,      SFBETRM_ESTS_DATE,      SFBETRM_MHRS_OVER,
                            SFBETRM_AR_IND,             SFBETRM_ASSESSMENT_DATE,    SFBETRM_ADD_DATE,       SFBETRM_ACTIVITY_DATE,  SFBETRM_TMST_CODE,      
                            SFBETRM_TMST_DATE,          SFBETRM_TMST_MAINT_IND,     SFBETRM_USER,           SFBETRM_REFUND_DATE,    SFBETRM_DATA_ORIGIN,
                            SFBETRM_INITIAL_REG_DATE,   SFBETRM_MIN_HRS,            SFBETRM_MINH_SRCE_CDE,  SFBETRM_MAXH_SRCE_CDE)
    
                        VALUES (
                            P_TERM_CODE,                P_PIDM,                     'EL',                   SYSDATE,                27,
                            'N',                        SYSDATE,                    SYSDATE,                SYSDATE,                'CI',
                            SYSDATE,                    'S',                        'WFAUTO',               SYSDATE,                'BANNER',
                            SYSDATE,                    0,                          'M',                    'U');
            COMMIT;
        END IF;
    --################################################################################################
    ---VERIFICAR STATUS ELEGIBLE
    --################################################################################################
        SELECT COUNT(*)
        INTO V_STATU
        FROM SFRENSP
        WHERE SFRENSP_PIDM = P_PIDM
        AND SFRENSP_TERM_CODE = P_TERM_CODE;
        
        IF V_STATU = 0 THEN
            INSERT INTO SFRENSP (
                            SFRENSP_TERM_CODE,      SFRENSP_PIDM,           SFRENSP_KEY_SEQNO,      SFRENSP_ESTS_CODE,      SFRENSP_ESTS_DATE, 
                            SFRENSP_ADD_DATE,       SFRENSP_ACTIVITY_DATE,  SFRENSP_USER,           SFRENSP_DATA_ORIGIN)
                        VALUES (
                            P_TERM_CODE,            P_PIDM,                  V_SEQ_PLAN,            'EL',                   SYSDATE,
                            SYSDATE,                SYSDATE,                'WFAUTO',               'BANNER');
            COMMIT;
        END IF;

    --################################################################################################
    ---VERIFICAR MATRICULA EN EL PERIODO
    --################################################################################################
        SELECT COUNT(*)
        INTO V_MATRI
        FROM SFRSTCR
        WHERE SFRSTCR_PIDM = P_PIDM
        AND SFRSTCR_TERM_CODE = P_TERM_CODE
        AND SFRSTCR_CRN = P_NRC_CODE;
        
        IF V_MATRI = 0 THEN
            
            SELECT NVL(MAX(SFRSTCR_REG_SEQ),0)
            INTO V_SEQNC
            FROM SFRSTCR
            WHERE SFRSTCR_TERM_CODE = P_TERM_CODE
            AND SFRSTCR_CRN = P_NRC_CODE;
            
            SELECT DISTINCT SSBSECT_PTRM_CODE, SSBSECT_CAMP_CODE, SSBSECT_CREDIT_HRS
            INTO V_PTRM, V_CAMP, V_CRED
            FROM SSBSECT
            WHERE SSBSECT_TERM_CODE = P_TERM_CODE
            AND SSBSECT_CRN = P_NRC_CODE;
        
            IF V_CAMP <> V_CAMP_ALUM THEN
                V_CAMP_OVER := 'Y';
            END IF;
            
            SELECT CASE SUBSTR(STVPTRM_CODE,1,1)
                WHEN 'V' THEN 'UVIR'
                WHEN 'W' THEN 'UPGT'
                WHEN 'R' THEN 'UREG'
                END
                INTO V_DEPT
            FROM STVPTRM
            WHERE STVPTRM_CODE = V_PTRM;
            
            IF V_DEPT <> V_DEPT_ALUM THEN
                V_DEPT_OVER := 'Y';
            END IF;
        
            INSERT INTO SFRSTCR (
                SFRSTCR_TERM_CODE,              SFRSTCR_PIDM,           SFRSTCR_CRN,                        SFRSTCR_CLASS_SORT_KEY,     SFRSTCR_REG_SEQ,
                SFRSTCR_PTRM_CODE,              SFRSTCR_RSTS_CODE,      SFRSTCR_RSTS_DATE,                  SFRSTCR_ERROR_FLAG,         SFRSTCR_MESSAGE,
                SFRSTCR_BILL_HR,                SFRSTCR_WAIV_HR,        SFRSTCR_CREDIT_HR,                  SFRSTCR_BILL_HR_HOLD,       SFRSTCR_CREDIT_HR_HOLD,
                SFRSTCR_GMOD_CODE,              SFRSTCR_GRDE_CODE,      SFRSTCR_GRDE_CODE_MID,              SFRSTCR_GRDE_DATE,          SFRSTCR_DUPL_OVER,
                SFRSTCR_LINK_OVER,              SFRSTCR_CORQ_OVER,      SFRSTCR_PREQ_OVER,                  SFRSTCR_TIME_OVER,          SFRSTCR_CAPC_OVER,
                SFRSTCR_LEVL_OVER,              SFRSTCR_COLL_OVER,      SFRSTCR_MAJR_OVER,                  SFRSTCR_CLAS_OVER,          SFRSTCR_APPR_OVER,
                SFRSTCR_APPR_RECEIVED_IND,      SFRSTCR_ADD_DATE,       SFRSTCR_ACTIVITY_DATE,              SFRSTCR_LEVL_CODE,          SFRSTCR_CAMP_CODE,
                SFRSTCR_RESERVED_KEY,           SFRSTCR_ATTEND_HR,      SFRSTCR_REPT_OVER,                  SFRSTCR_RPTH_OVER,          SFRSTCR_TEST_OVER,
                SFRSTCR_CAMP_OVER,              SFRSTCR_USER,           SFRSTCR_DEGC_OVER,                  SFRSTCR_PROG_OVER,          SFRSTCR_LAST_ATTEND,
                SFRSTCR_GCMT_CODE,              SFRSTCR_DATA_ORIGIN,    SFRSTCR_ASSESS_ACTIVITY_DATE,       SFRSTCR_DEPT_OVER,          SFRSTCR_ATTS_OVER,
                SFRSTCR_CHRT_OVER,              SFRSTCR_RMSG_CDE,       SFRSTCR_WL_PRIORITY,                SFRSTCR_WL_PRIORITY_ORIG,   SFRSTCR_GRDE_CODE_INCMP_FINAL,
                SFRSTCR_INCOMPLETE_EXT_DATE,    SFRSTCR_MEXC_OVER,      SFRSTCR_STSP_KEY_SEQUENCE,          SFRSTCR_BRDH_SEQ_NUM,       SFRSTCR_BLCK_CODE,
                SFRSTCR_STRH_SEQNO,             SFRSTCR_STRD_SEQNO,     SFRSTCR_VERSION,                    SFRSTCR_USER_ID,            SFRSTCR_VPDI_CODE,          
                SFRSTCR_SESSIONID,              SFRSTCR_CURRENT_TIME)
            VALUES (
                P_TERM_CODE,                    P_PIDM,                 P_NRC_CODE,                         NULL,                       V_SEQNC+1, 
                V_PTRM,                         'RE',                   SYSDATE,                            'O',                        NULL, 
                V_CRED,                         V_CRED,                 V_CRED,                             V_CRED,                     V_CRED,
                'S',                            NULL,                   NULL,                               NULL,                       'N', 
                'N',                            'N',                    'N',                                'N',                        'N',
                'N',                            'N',                    'N',                                'N',                        'N', 
                NULL,                           SYSDATE,                SYSDATE,                            'PG',                       V_CAMP,
                NULL,                           NULL,                   'N',                                'N',                        NULL, 
                V_CAMP_OVER,                    'WFAUTO',               'N',                                'N',                        NULL, 
                NULL,                           NULL,                   SYSDATE,                            V_DEPT_OVER,                'N', 
                'N',                            NULL,                   NULL,                               NULL,                       NULL, 
                NULL,                           'N',                    V_SEQ_PLAN,                         NULL,                       NULL, 
                NULL,                           NULL,                   NULL,                               NULL,                       NULL,
                32533092,                       CAST(SYSDATE AS TIMESTAMP(9)));
            COMMIT;
            
            UPDATE SSBSECT SET
            SSBSECT_ENRL = SSBSECT_ENRL + 1
            WHERE SSBSECT_CRN = P_NRC_CODE
            AND SSBSECT_TERM_CODE = P_TERM_CODE;
            COMMIT;
            
            UPDATE SSBSECT SET
            SSBSECT_SEATS_AVAIL = SSBSECT_MAX_ENRL - SSBSECT_ENRL
            WHERE SSBSECT_CRN = P_NRC_CODE
            AND SSBSECT_TERM_CODE = P_TERM_CODE;
            COMMIT;
            
            UPDATE SSRRESV SET
            SSRRESV_ENRL = SSRRESV_ENRL + 1
            WHERE SSRRESV_TERM_CODE = P_TERM_CODE
            AND SSRRESV_CRN = P_NRC_CODE
            AND SSRRESV_DEPT_CODE = V_DEPT_ALUM;
            
            UPDATE SSRRESV SET
            SSRRESV_SEATS_AVAIL = SSRRESV_MAX_ENRL - SSRRESV_ENRL
            WHERE SSRRESV_TERM_CODE = P_TERM_CODE
            AND SSRRESV_CRN = P_NRC_CODE
            AND SSRRESV_DEPT_CODE = -V_DEPT_ALUM;
            
            P_MESSAGE := 'Se registró la asignatura al estudiante.';
        ELSE
            P_MESSAGE := 'El estudiante ya está matriculado en la asignatura solicitada.';
        END IF;
        
EXCEPTION
  WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);

END P_INSERT_NRC_MULTI;

--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--
--*****************************************************************************************************************************+********--

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              
END BWZKSMAM;

/**********************************************************************************************/
--/
--show errors
--
--SET SCAN ON
/**********************************************************************************************/