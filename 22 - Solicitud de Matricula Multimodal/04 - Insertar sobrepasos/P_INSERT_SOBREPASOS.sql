CREATE OR REPLACE PROCEDURE P_INSERT_SOBREPASOS (
      P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE, 
      P_TERM_CODE       IN STVTERM.STVTERM_CODE%TYPE,
      P_CAMP_CODE       IN STVCAMP.STVCAMP_CODE%TYPE,
      P_NRC_CODE        IN SSBSECT.SSBSECT_CRN%TYPE,
      P_DEPT_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
      P_MESSAGE         OUT VARCHAR2
)
/* ===================================================================================================================
  NOMBRE    : P_INSERT_SOBREPASOS
  FECHA     : 11/07/2018
  AUTOR     : Mallqui Lopez, Richard Alfonso
  OBJETIVO  : El objetivo es insertar los sobrepasos 07 "Departamento/Modalidad" y 03 "Campus", haciendo una 
              verificación de si tiene insertado el registro. (los códigos de los sobrepasos trabajarán 
              en variables locales)   
  =================================================================================================================== */
AS
      V_PTRM_DEPT       VARCHAR2(9); -- GTVINSM
      V_CAMP_CODE       STVCAMP.STVCAMP_CODE%TYPE;
      V_IF_ROVR         NUMBER;
      
BEGIN 

      -- VALIDAR QUE NRC TENGA UNA DISTINTA MODALIDAD ALUMNO<
      SELECT CASE SUBSTR(SSBSECT_PTRM_CODE,1,1) WHEN 'V' THEN 'UVIR' 
                                                WHEN 'R' THEN 'UREG' 
                                                WHEN 'W' THEN 'UPGT'
                                                ELSE '-' END, 
            SSBSECT_CAMP_CODE 
      INTO  V_PTRM_DEPT,  
            V_CAMP_CODE
      FROM SSBSECT 
      WHERE SSBSECT_TERM_CODE = P_TERM_CODE 
      AND   SSBSECT_CRN = P_NRC_CODE;
      
      -- INSERTAR  SOBREPASO SFRSRPO --- 03 DEPT(STVROVR)
      IF (V_PTRM_DEPT <> P_DEPT_CODE) THEN
          INSERT INTO SFRSRPO (
            SFRSRPO_TERM_CODE,          SFRSRPO_PIDM,           SFRSRPO_ROVR_CODE,
            SFRSRPO_SUBJ_CODE,          SFRSRPO_CRSE_NUMB,      SFRSRPO_ACTIVITY_DATE,
            SFRSRPO_SEQ_NUMB,           SFRSRPO_CRN,            SFRSRPO_USER
          )
          SELECT 
            SSBSECT_TERM_CODE,          P_PIDM,                 '03',
            SSBSECT_SUBJ_CODE,          SSBSECT_CRSE_NUMB,      SYSDATE,
            SSBSECT_SEQ_NUMB,           SSBSECT_CRN,            'WFAUTO'
          FROM SSBSECT 
          WHERE SSBSECT_TERM_CODE = P_TERM_CODE 
          AND SSBSECT_CRN = P_NRC_CODE
          AND NOT EXISTS(
              SELECT * FROM SFRSRPO WHERE SFRSRPO_TERM_CODE = P_TERM_CODE AND SFRSRPO_PIDM = P_PIDM AND SFRSRPO_CRN = P_NRC_CODE AND SFRSRPO_ROVR_CODE = '03'
          );
      END IF;

      
      -- INSERTAR  SOBREPASO SFRSRPO --- 07 CAMPUS (STVROVR)
      IF (V_CAMP_CODE <> P_CAMP_CODE) THEN
          INSERT INTO SFRSRPO (
            SFRSRPO_TERM_CODE,          SFRSRPO_PIDM,           SFRSRPO_ROVR_CODE,
            SFRSRPO_SUBJ_CODE,          SFRSRPO_CRSE_NUMB,      SFRSRPO_ACTIVITY_DATE,
            SFRSRPO_SEQ_NUMB,           SFRSRPO_CRN,            SFRSRPO_USER
          )
          SELECT 
            SSBSECT_TERM_CODE,          P_PIDM,                 '07',
            SSBSECT_SUBJ_CODE,          SSBSECT_CRSE_NUMB,      SYSDATE,
            SSBSECT_SEQ_NUMB,           SSBSECT_CRN,            'WFAUTO'
          FROM SSBSECT 
          WHERE SSBSECT_TERM_CODE = P_TERM_CODE 
          AND SSBSECT_CRN = P_NRC_CODE
          AND NOT EXISTS(
              SELECT * FROM SFRSRPO WHERE SFRSRPO_TERM_CODE = P_TERM_CODE AND SFRSRPO_PIDM = P_PIDM AND SFRSRPO_CRN = P_NRC_CODE AND SFRSRPO_ROVR_CODE = '07'
          );
      END IF;

      --
      COMMIT; 
EXCEPTION
    WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR(-20001,'Error o inconsistencia detectada -'||SQLCODE||'-ERROR- '|| SQLERRM);
END P_INSERT_SOBREPASOS;