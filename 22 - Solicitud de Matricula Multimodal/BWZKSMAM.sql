/******************************************************************************/
/* BWZKSMAM.sql                                                               */
/******************************************************************************/
/*                                                                            */
/* Descripción corta: Script para generar el Paquete relacionado a los        */
/*                    procedimientos de Matricula Multimodal                  */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* SEGUIMIENTO: 1.0 [Universidad Continental]                 INI    FECHA    */
/* ---------------------------------------------------------- --- ----------- */
/* 1. Creación del código.                                 BFV/LAM 03/JUL/2018*/
/*    Creación del paquete de Matricula Multimodal                            */
/*                                                                            */
/*    Procedure P_VALID_ID: Verifica si el DNI es correcto y existe.          */
/*    Procedure P_GET_INFO_STUDENT: Obtiene la informacion del DNI registrado */
/*    Procedure P_VERIFICAR_APTO: Valida si el estudiante cumple las          */
/*      condiciones necesarias para acceder al flujo.                         */
/*    Procedure P_VERIFICA_FECHA_DISPO: Confirma si la solicitud esta dentro  */
/*      de las fechas programadas.                                            */
/*    Procedure P_VERIFICAR_NRC_MULTI: Verifica si el NRC creado tiene las    */
/*      caracteristicas basicas de un NRC multimodal.                         */
/*    Procedure P_INSERT_SOBREPASOS: Inserta los sobrepasos para la matricula */
/*      multimodal.                                                           */
/*    Procedure P_INSERT_NRC_MULTI: Registra el NRC multimodal al estudiante  */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*                                                                            */
/* FIN DEL SEGUIMIENTO                                                        */
/*                                                                            */
/******************************************************************************/ 
/**********************************************************************************************/
-- PERMISOS DE EJECUCIÓN
/**********************************************************************************************/
--  SET SCAN ON 
-- 
--  CONNECT baninst1/&&baninst1_password;
--  WHENEVER SQLERROR CONTINUE 
-- 
--  SET SCAN OFF
--  SET ECHO OFF
/**********************************************************************************************/

CREATE OR REPLACE PACKAGE BANINST1.BWZKSMAM AS
/*
 BWZKSMAM:
       Paquete Web _ Desarrollo Propio _ Paquete _ Conti Solicitud Matricula Multimodal
*/
-- FILE NAME..: BWZKSMAM.sql
-- RELEASE....: 0.1 [U. CONTINENTAL 1.0]
-- OBJECT NAME: BWZKSMAM
-- PRODUCT....: WF (WorkFlow)
-- COPYRIGHT..: Copyright Copyright UNIVERSIDAD CONTINENTAL 2017
 /*
  DESCRIPTION:
              -
  DESCRIPTION END */

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--              

PROCEDURE P_GET_STUDENT_INPUTS(
        P_PIDM                 IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_TERM_CODE            IN STVDEPT.STVDEPT_CODE%TYPE,
        P_FOLIO_SOLICITUD      IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_ATTRB_PLAN           OUT SGRSATT.SGRSATT_ATTS_CODE%TYPE,
        P_ASIGNATURA_CODE      OUT VARCHAR2,
        P_ASIGNATURA_DESC      OUT VARCHAR2,
        P_PLAN_ASIGNATURA      OUT VARCHAR2,
        P_DEPT_CODE_DEST       OUT VARCHAR2,
        P_DEPT_DESC_DEST       OUT VARCHAR2,
        P_COMENTARIO           OUT VARCHAR2,
        P_TELEFONO             OUT VARCHAR2,
        P_MESSAGE              OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_APTO (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_TERM_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
    P_ASIGNATURA_CODE   IN VARCHAR2,
    P_STATUS_APTO       OUT VARCHAR2,
    P_REASON            OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_ESTADO_SOLICITUD (
        P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_STATUS_SOL          OUT VARCHAR2,
        P_MESSAGE             OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_CAMBIO_ESTADO_SOLICITUD (
        P_FOLIO_SOLICITUD     IN SVRSVPR.SVRSVPR_PROTOCOL_SEQ_NO%TYPE,
        P_ESTADO              IN SVVSRVS.SVVSRVS_CODE%TYPE, 
        P_AN                  OUT NUMBER,
        P_MESSAGE             OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_COORDINADOR (
        P_PIDM                IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, -- sensible mayuscula 'S01'
        P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE, --sensible mayuscula 'UREG'
        P_ROL_DC              IN WORKFLOW.ROLE.NAME%TYPE, -- sensible a mayusculas 'coordinador'
        P_CORREO_COORD        OUT VARCHAR2,
        P_ROL_COORD           OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_GET_USER_PROGRAMACION (
        P_DEPT_CODE           IN STVDEPT.STVDEPT_CODE%TYPE,
        P_DEPT_CODE_DEST      IN STVDEPT.STVDEPT_CODE%TYPE, --sensible mayuscula 'UREG'
        P_CAMP_CODE           IN STVCAMP.STVCAMP_CODE%TYPE, -- sensible mayuscula 'S01'
        P_ROL_PROGRAMACION    IN WORKFLOW.ROLE.NAME%TYPE, -- sensible a minusculas 'progracad'
        P_CORREO_PROGR        OUT VARCHAR2,
        P_ROL_PROGR           OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICA_NRC_MULTI (
    P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_NRC_CODE          IN SSBSECT.SSBSECT_CRN%TYPE,
    P_TERM_CODE         IN STVTERM.STVTERM_CODE%TYPE,
    P_CAMP_SEDE         IN STVCAMP.STVCAMP_CODE%TYPE,
    P_DEPT_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
    P_ASIGNATURA_CODE   IN VARCHAR2,
    P_FECHA_INICIO      OUT VARCHAR2,
    P_FECHA_FIN         OUT VARCHAR2,
    P_STATUS_NRC        OUT VARCHAR2,
    P_REASON_NRC        OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_VERIFICAR_MATRICULA (
        P_PIDM              IN SPRIDEN.SPRIDEN_PIDM%TYPE,
        P_ASIGNATURA_CODE   IN VARCHAR2,
        P_TERM_CODE         IN STVDEPT.STVDEPT_CODE%TYPE,
        P_STATUS            OUT VARCHAR2,
        P_REASON_MATRICULA  OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_INSERT_SOBREPASOS (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE, 
    P_TERM_CODE       IN STVTERM.STVTERM_CODE%TYPE,
    P_CAMP_CODE       IN STVCAMP.STVCAMP_CODE%TYPE,
    P_NRC_CODE        IN SSBSECT.SSBSECT_CRN%TYPE,
    P_DEPT_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_MESSAGE         OUT VARCHAR2
);

--------------------------------------------------------------------------------

PROCEDURE P_INSERT_NRC_MULTI (
    P_PIDM            IN SPRIDEN.SPRIDEN_PIDM%TYPE,
    P_NRC_CODE        IN SSBSECT.SSBSECT_CRN%TYPE,
    P_TERM_CODE       IN STVDEPT.STVDEPT_CODE%TYPE,
    P_MESSAGE         OUT VARCHAR2
);

--------------------------------------------------------------------------------

END BWZKSMAM;

/**********************************************************************************************/
--  /
--  show errors 
-- 
--  SET SCAN ON
--  WHENEVER SQLERROR CONTINUE
--  DROP PUBLIC SYNONYM BWZKSMAM;
--  WHENEVER SQLERROR EXIT ROLLBACK
--  CREATE PUBLIC SYNONYM BWZKSMAM FOR BANINST1.BWZKSMAM;
--  WHENEVER SQLERROR CONTINUE
--  START gurgrtb BWZKSMAM
--  START gurgrth BWZKSMAM
--  WHENEVER SQLERROR EXIT ROLLBACK
/**********************************************************************************************/